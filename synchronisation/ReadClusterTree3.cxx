
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TGraphErrors.h"
#include "TVectorD.h"
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

typedef unsigned int uint;
typedef vector<double> vdouble;
typedef vector<int> vint;
typedef vector<string> vstring;

constexpr uint NLAD{2};
constexpr uint NBOARD{3};
constexpr uint NBRANCH{7};
constexpr bool nov2021BT{false};
constexpr bool june2022BT{true};

TTree *ClustersTree[NBOARD];

std::unordered_map<int, vdouble *> integral_map, cog_map, sovern_map;
std::unordered_map<int, std::vector<std::vector<double>> *> sovernPerChannel_map;
std::unordered_map<int, vint *> length_map;
std::unordered_map<int, uint> eventNumber_map;
std::unordered_map<int, ULong64_t> frameCounter_map;
std::vector<std::array<TBranch *, NBRANCH>> branches;

vdouble detectorPos;
vdouble detectorPosErr = {0.05, 0.05, 0.05, 0.05};
vdouble cogPosErr = {5., 5., 5., 5.};

enum runType {cosmics, beamtest, june2022swapped, reversed_beamtest, april2023swapped};

TCanvas *CreateCanvas(uint board, string name, int width = 800, int height = 600, int divx = 1, int divy = 2)
{

    string fname{name + Form("_%d", board)};
    TCanvas *c{new TCanvas(fname.c_str(), fname.c_str(), width, height)};
    if (divx and divy)
        c->Divide(divx, divy);
    return c;
}

Double_t langaufun(Double_t *x, Double_t *par)
{
    // From the root tutorials.
    // Fit parameters:
    // par[0]=Width (scale) parameter of Landau density
    // par[1]=Most Probable (MP, location) parameter of Landau density
    // par[2]=Total area (integral -inf to inf, normalization constant)
    // par[3]=Width (sigma) of convoluted Gaussian function
    //
    // In the Landau distribution (represented by the CERNLIB approximation),
    // the maximum is located at x=-0.22278298 with the location parameter=0.
    // This shift is corrected within this function, so that the actual
    // maximum is identical to the MP parameter.

    // Numeric constants
    Double_t invsq2pi = 0.3989422804014; // (2 pi)^(-1/2)
    Double_t mpshift = -0.22278298;      // Landau maximum location

    // Control constants
    Double_t np = 100.0; // number of convolution steps
    Double_t sc = 5.0;   // convolution extends to +-sc Gaussian sigmas

    // Variables
    Double_t xx;
    Double_t mpc;
    Double_t fland;
    Double_t sum = 0.0;
    Double_t xlow, xupp;
    Double_t step;
    Double_t i;

    // MP shift correction
    mpc = par[1] - mpshift * par[0];

    // Range of convolution integral
    xlow = x[0] - sc * par[3];
    xupp = x[0] + sc * par[3];

    step = (xupp - xlow) / np;

    // Convolution integral of Landau and Gaussian by sum
    for (i = 1.0; i <= np / 2; i++)
    {
        xx = xlow + (i - .5) * step;
        fland = TMath::Landau(xx, mpc, par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0], xx, par[3]);

        xx = xupp - (i - .5) * step;
        fland = TMath::Landau(xx, mpc, par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0], xx, par[3]);
    }

    return (par[2] * step * sum * invsq2pi / par[3]);
}

void ReadClusterTree(string filename = "/Users/azzarell/cernbox/PAN/miniPAN/Data/StripX/BeamTest202111/beam_202111072346/board5_202111072346_clustered.root", uint board = 0)
{

    TFile *cluFile{new TFile(filename.c_str())};

    if (cluFile == 0)
    {
        cout << "could not open file " << filename << endl;
        return;
    }

    TTree *clusters_tree{(TTree *)(cluFile->Get("clusters_tree"))};
    ClustersTree[board] = clusters_tree;

    if (clusters_tree == 0)
    {
        cout << "tree clusters_tree not found" << endl;
        return;
    }
    else
        cout << "clusters_tree has " << clusters_tree->GetEntries() << " entries" << endl;

    // vdouble tmp_double_vec, tmp_double_vec2;
    // vint tmp_int_vec;

    for (uint lad{0}; lad < NLAD; lad++)
    {
        cout << "getting branches for ladder " << lad << endl;

        uint index{lad + NLAD * board};

        // integral_map.at(lad) = tmp_double_vec;
        // length_map.at(lad) = tmp_int_vec;
        // cog_map.at(lad) = tmp_double_vec;
        //(branches.at(lad)).at(0) = clusters_tree->Branch(("integral" + to_string(lad)).c_str(), &integral_map.at(lad));
        //(branches.at(lad)).at(1) = clusters_tree->Branch(("length" + to_string(lad)).c_str(), &length_map.at(lad));
        //(branches.at(lad)).at(2) = clusters_tree->Branch(("cog" + to_string(lad)).c_str(), &cog_map.at(lad));
        clusters_tree->SetBranchAddress(Form("integral%d", lad), &integral_map.at(index), &((branches.at(index)).at(0)));
        clusters_tree->SetBranchAddress(Form("cog%d", lad), &cog_map.at(index), &((branches.at(index)).at(2)));
        clusters_tree->SetBranchAddress(Form("length%d", lad), &length_map.at(index), &((branches.at(index)).at(1)));
        clusters_tree->SetBranchAddress(Form("sovern%d", lad), &sovern_map.at(index), &((branches.at(index)).at(3)));
        clusters_tree->SetBranchAddress(Form("sovernPerChannel%d", lad), &sovernPerChannel_map.at(index), &((branches.at(index)).at(4)));
        try {clusters_tree->SetBranchAddress(Form("eventNumber%d", lad), &eventNumber_map.at(index), &((branches.at(index)).at(5)));}
        catch (...) {continue;}
        try {clusters_tree->SetBranchAddress(Form("frameCounter%d", lad), &frameCounter_map.at(index), &((branches.at(index)).at(6)));}
        catch (...) {continue;}
    }

    // branch check
    for (uint lad{0}; lad < NLAD; lad++)
    {
        uint index{lad + NLAD * board};
        for (uint br{0}; br < branches.at(lad).size(); br++) {
            if (branches.at(index).at(br)) cout << "ladder " << lad << " branch " << br << " has name " << branches.at(index).at(br)->GetName() << endl;
        }
    }

    TH1F *hcog[NLAD], *hint[NLAD], *hlen[NLAD];
    TH2F *h2cog, *h2int_vs_cog[NLAD];

    for (uint lad{0}; lad < NLAD; lad++)
    {
        hcog[lad] = new TH1F(Form("hcog_board%d_lad%d", board, lad), Form("center of gravity, board %d, ladder %d", board, lad), 4096, 0, 2048);
        hint[lad] = new TH1F(Form("hint_board%d_lad%d", board, lad), Form("integral, board %d, ladder %d", board, lad), 800, 0, 200);
        hlen[lad] = new TH1F(Form("hlen_board%d_lad%d", board, lad), Form("length, board %d, ladder %d", board, lad), 20, 0, 20);

        h2int_vs_cog[lad] = new TH2F(Form("h2int_vs_cog_board%d_lad%d", board, lad), Form("integral vs cog, board %d, ladder %d", board, lad), 4096, 0, 2048, 800, 0, 200);
    }
    h2cog = new TH2F(Form("h2cog_board%d", board), Form("cog lad1 vs cog lad0, board %d", board), 4096, 0, 2048, 4096, 0, 2048);

    for (uint entry{0}; entry < clusters_tree->GetEntries(); entry++)
    {

        clusters_tree->GetEntry(entry);

        size_t len0{integral_map.at(0 + NLAD * board)->size()};
        size_t len1{integral_map.at(1 + NLAD * board)->size()};

        // cout << len0 << "  " << len1 << endl;
        /*if (len0 == 0)
        {
            cout << "Entry " << entry << endl;
            cout << " number of clusters lad 0: " << (integral_map.at(0))->size() << " " << (cog_map.at(0))->size() << " " << (length_map.at(0))->size() << endl;
            cout << " number of clusters lad 1: " << (integral_map.at(1))->size() << " " << (cog_map.at(1))->size() << " " << (length_map.at(1))->size() << endl;
            cout << endl;
        }
        */
        /*
        if (len0 != len1)
        {
            cout << "len0=" << len0 << "    len1=" << len1 << endl;
        }
        */

        if (len0 == 1 and len1 == 1) // one cluster per sensor
        {

            for (uint lad{0}; lad < NLAD; lad++)
            {
                uint index{lad + NLAD * board};
                hcog[lad]->Fill(cog_map.at(index)->at(0));
                hint[lad]->Fill(integral_map.at(index)->at(0));
                hlen[lad]->Fill(length_map.at(index)->at(0));
                h2int_vs_cog[lad]->Fill(cog_map.at(index)->at(0), integral_map.at(index)->at(0));
            }
            h2cog->Fill(cog_map.at(0 + NLAD * board)->at(0), cog_map.at(1 + NLAD * board)->at(0));
        }
    }

    TCanvas *chint{CreateCanvas(board, "chint")};
    TCanvas *chlen{CreateCanvas(board, "chlen")};
    TCanvas *chcog{CreateCanvas(board, "chcog")};
    TCanvas *ch2cog{CreateCanvas(board, "ch2cog", 800, 600, 0, 0)};
    TCanvas *ch2int_vs_cog{CreateCanvas(board, "ch2int_vs_cog")};

    // ch2cog->Divide(1, 2);

    TF1 *flandau = new TF1("flandau", "landau");
    flandau->SetRange(20, 60);

    TF1 *flangau = new TF1("flangau", langaufun, 15, 60, 4);
    flangau->SetParameters(5, 30, 100, 5);
    flangau->SetParNames("Width", "MP", "Area", "GSigma");
    flangau->SetLineColor(kRed);
    flangau->SetLineStyle(1);

    for (uint lad{0}; lad < NLAD; lad++)
    {

        chint->cd(lad + 1);
        hint[lad]->Draw();

        hint[lad]->Fit(flandau, "N");
        flangau->SetParameter(1, flandau->GetParameter(1));
        flangau->SetParameter(2, flandau->GetParameter(2));
        flangau->SetRange(flandau->GetParameter(1) - 10, flandau->GetParameter(1) + 50);
        hint[lad]->Fit(flangau, "RM");

        chcog->cd(lad + 1);
        hcog[lad]->Draw();

        chlen->cd(lad + 1);
        hlen[lad]->Draw();

        ch2int_vs_cog->cd(lad + 1);
        h2int_vs_cog[lad]->Draw("colz");
    }

    ch2cog->cd();
    h2cog->Draw("colz");
    /////////

    chint->Update();
    chcog->Update();
    chlen->Update();
    ch2int_vs_cog->Update();
    ch2cog->Update();

    /////////
}

uint findMinNumberOfEntries(TTree** tree) {
    uint minValue = 999999;
    for (uint i = 0; i < NBOARD; ++i) {
        if (tree[i]->GetEntries() < minValue) minValue = tree[i]->GetEntries();
    }
    return minValue;
}

std::vector<uint> findSyncedEntryNumber(const std::vector<uint>& eventNumbersVecBase, std::vector<bool>& entriesStatus, uint entry, uint minNumberOfEntries) {
    std::vector<uint> entriesVec(NBOARD);
    std::vector<uint> eventNumbersVec;
    for (uint board{0}; board < NBOARD; board++) {
        ClustersTree[board]->GetEntry(entry);
        entriesVec.at(board) = entry;
        entriesStatus.at(board) = false;
        uint index = board * 2;
        eventNumbersVec.push_back(eventNumber_map.at(index) - eventNumbersVecBase.at(board));
        // std::cout << "eventNumbersVec[" << board << "]: " << eventNumbersVec.at(board) << std::endl;
    }
    
    uint largestEntry = *std::max_element(eventNumbersVec.begin(), eventNumbersVec.end());
    std::vector<bool>::iterator result;
    for (uint i{entry}; i <= largestEntry; i++) {
        for (uint board{0}; board < NBOARD; board++) {
            if (i > minNumberOfEntries) break;
            uint index = board * 2;
            ClustersTree[board]->GetEntry(i);
            if (eventNumber_map.at(index) - eventNumbersVecBase.at(board) == largestEntry) {
                entriesVec.at(board) = i;
                entriesStatus.at(board) = true;
            }
        }
        result = std::find_if(entriesStatus.begin(), entriesStatus.end(), [](bool i){ return !i; });
        if (result != std::end(entriesStatus)) {
			if (i == largestEntry) {
				std::cout<<"entry "<< i << std::endl;
				std::cout << "largestEntry: " << largestEntry << std::endl;
				for (uint board{0}; board < NBOARD; board++) {
					std::cout << "entriesStatus[" << board << "]: " << entriesStatus.at(board) << std::endl;
					std::cout << "entriesVec[" << board << "]: " << entriesVec.at(board) << std::endl;
				}
			}
            continue;
        } else {
            break;
        }
    }
    
    return entriesVec;
}

double findAverageValue(TVectorD& vec) {
    double avgValue{0.};
    for (
         int i{0}; i < vec.GetNrows(); ++i) avgValue += vec[i];
    avgValue /= vec.GetNrows();
    return avgValue;
}

double estimateCurvature(TVectorD& yCoord, TVectorD& zCoord) {
    double curv{0.};
    if (TMath::Abs(yCoord[0] - yCoord[1]) > TMath::Abs(yCoord[2] - yCoord[3])) {
        curv = (yCoord[0] - yCoord[1]) / ((zCoord[0] - zCoord[1]) * (zCoord[0] + zCoord[1] - 2 * findAverageValue(zCoord)));
    }
    else {
        curv = (yCoord[2] - yCoord[3]) / ((zCoord[2] - zCoord[3]) * (zCoord[2] + zCoord[3] - 2 * findAverageValue(zCoord)));
    }
    return curv;
}

void ReadClusterTree3(string path = "/Users/azzarell/cernbox/PAN/miniPAN/Data/StripX/BeamTest202111/beam_202111072346/", string suffix = "202111072346_clustered.root", string outfile = "synced_clusters.root", string type = "beamtest")
{
    gStyle->SetOptFit(1);
    vstring vlist(NBOARD, "");
    vlist.at(0) = path + "board0_" + suffix;
    for (std::size_t i = 1; i < vlist.size(); i++) {
        vlist.at(i) = path + "board" + std::to_string(2 * i + 1) + "_" + suffix;
    }
    
    runType runMode;
    if (type == "cosmics") runMode = cosmics;
    else if (type == "beamtest") runMode = beamtest;
    else if (type == "reversed_beamtest") runMode = reversed_beamtest;
    else if (type == "april2023swapped") runMode = april2023swapped;
    else runMode = june2022swapped;
    
    if (nov2021BT) detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81 cm // November 2021 beamtest
    if (june2022BT) detectorPos = {-6.838, -6.038, -0.302, 0.498, 6.038, 6.838}; // 0, 8.0, 65.36, 73.36, 128.76, 136.76 mm // June 2022 beamtest

    std::array<TBranch *, NBRANCH> temp_arr;
    branches.assign(NLAD * NBOARD, temp_arr);
    
    // initialize common synchronised tree
    TFile *syncOutFile = TFile::Open(outfile.c_str(), "recreate");
    TTree *syncTree = new TTree("sync_clusters_tree", "Synchronised clusters tree for all boards");
    
    vdouble vdTemp;
    vint viTemp;
    
    for (uint board{0}; board < NBOARD; board++)
    {
        cout << "initialization for board " << board << endl;
        for (uint lad{0}; lad < NLAD; lad++)
        {
            cout << "intialization for ladder " << lad << endl;
            uint index{lad + NLAD * board};
            integral_map[index] = new vdouble;
            length_map[index] = new vint;
            cog_map[index] = new vdouble;
            sovern_map[index] = new vdouble;
            sovernPerChannel_map[index] = new std::vector<std::vector<double>>;
            eventNumber_map[index] = 0;
            frameCounter_map[index] = 0;
            if (runMode == beamtest) {
                syncTree->Branch(Form("integral%d", index), integral_map.at(index));
                syncTree->Branch(Form("length%d", index), length_map.at(index));
                syncTree->Branch(Form("cog%d", index), cog_map.at(index));
                syncTree->Branch(Form("sovern%d", index), sovern_map.at(index));
                syncTree->Branch(Form("sovernPerChannel%d", index), sovernPerChannel_map.at(index));
                syncTree->Branch(Form("eventNumber%d", index), &eventNumber_map.at(index));
                syncTree->Branch(Form("frameCounter%d", index), &frameCounter_map.at(index));
            }
            else {
                uint index1{0};
                if (runMode == reversed_beamtest) {
                    index1 = NLAD * (NBOARD - board) - 1 - lad;
                }
                else if (runMode == april2023swapped) {
                    index1 = NLAD * (NBOARD - board) - 1 - lad;
                    if (board == 0 && lad == 0) index1 = 1;
                    if (board == 2 && lad == 0) index1 = 5;
                }
                else {
                    index1 = lad + NLAD * (NBOARD - board - 1);
                    if (runMode == june2022swapped) {
                        if (board == 0) index1 = index;
                        else if (board == 1) index1 = index + 2;
                        else index1 = index - 2;
                    }
                }
                syncTree->Branch(Form("integral%d", index1), integral_map.at(index));
                syncTree->Branch(Form("length%d", index1), length_map.at(index));
                syncTree->Branch(Form("cog%d", index1), cog_map.at(index));
                syncTree->Branch(Form("sovern%d", index1), sovern_map.at(index));
                syncTree->Branch(Form("sovernPerChannel%d", index1), sovernPerChannel_map.at(index));
                syncTree->Branch(Form("eventNumber%d", index1), &eventNumber_map.at(index));
                syncTree->Branch(Form("frameCounter%d", index1), &frameCounter_map.at(index));
            }
        }
    }

    for (uint board{0}; board < NBOARD; board++)
        ReadClusterTree(vlist.at(board), board);

    // now let's try to see if we can associate 2 boards together

    // we cycle on board0, and compare with board1

    TH2F *h2corr{new TH2F("h2corr", "h2corr;cog_lad0_board0;cog_lad0_board1", 2048, 0, 2048, 2048, 0, 2048)};
    // TH2F *h2diff{new TH2F("h2diff", "h2diff;event;abs(cog difference) boards 0-1", 40000, 0, 40000, 1000, 0, 1000)};
    TH2F *h2corr2{new TH2F("h2corr2", "h2corr2;cog_lad0_board0;cog_lad0_board2", 2048, 0, 2048, 2048, 0, 2048)};
    // TH2F *h2diff2{new TH2F("h2diff2", "h2diff2;event;abs(cog difference) board 0-2", 40000, 0, 40000, 1000, 0, 1000)};
    TGraph *grdiff{new TGraph()};
    grdiff->SetMarkerStyle(24);
    grdiff->SetMarkerColor(kRed);
    grdiff->SetMarkerSize(0.3);

    TGraph *grdiff2{new TGraph()};
    grdiff2->SetMarkerStyle(24);
    grdiff2->SetMarkerColor(kRed);
    grdiff2->SetMarkerSize(0.3);
    
    TGraph *grdiffClock1{new TGraph()};
    grdiffClock1->SetMarkerStyle(24);
    grdiffClock1->SetLineColor(kRed);
    grdiffClock1->SetLineWidth(2);
    
    TGraph *grdiffClock2{new TGraph()};
    grdiffClock2->SetMarkerStyle(24);
    grdiffClock2->SetLineColor(kRed);
    grdiffClock2->SetLineWidth(2);
    
    TGraph *grdiffClock3{new TGraph()};
    grdiffClock3->SetMarkerStyle(24);
    grdiffClock3->SetLineColor(kRed);
    grdiffClock3->SetLineWidth(2);
    
    std::vector<uint> eventNumbersVecBase;
    std::vector<ULong64_t> clockCounterVecBase;
    std::vector<bool> eventNumberStatus;
    std::vector<std::size_t> lengths(NBOARD * NLAD);
    Long64_t diff1{0}, diff2{0}, diff3{0};
    
    for (uint board{0}; board < NBOARD; board++) ClustersTree[board]->GetEntry(0);
    
    for (uint board{0}; board < NBOARD; board++)
    {
        uint index = board * 2;
        eventNumbersVecBase.push_back(eventNumber_map.at(index));
        clockCounterVecBase.push_back(frameCounter_map.at(index));
        std::cout << "eventNumbersVecBase[" << board << "]: " << eventNumbersVecBase.at(board) << std::endl;
        std::cout << "clockCounterVecBase[" << board << "]: " << clockCounterVecBase.at(board) << std::endl;
        eventNumberStatus.push_back(false);
    }
    
    uint minNumberOfEntries = findMinNumberOfEntries(ClustersTree);
    
    std::cout << "minNumberOfEntries: " << minNumberOfEntries << std::endl;
    
    std::vector<uint> entriesVec;
    bool syncStatus{true};
    
    TGraphErrors* gr{nullptr};
    TF1 *fitFunc{nullptr};
    TVectorD *yCoord{nullptr}, *zCoord{nullptr}, *yErr{nullptr}, *zErr{nullptr};
    yCoord = new TVectorD(4);
    zCoord = new TVectorD(4);
    zErr = new TVectorD(4, &detectorPosErr[0]);
    yErr = new TVectorD(4, &cogPosErr[0]);
    
    // if (suffix.length() > 27) suffix.erase(suffix.begin(), suffix.begin()+12);

    for (uint entry0{1}; entry0 < minNumberOfEntries; entry0++)
    {
        entriesVec.clear();
        entriesVec.shrink_to_fit();
        syncStatus = true;
        eventNumberStatus.clear();
        eventNumberStatus.shrink_to_fit();
        eventNumberStatus.assign(NBOARD, false);
        
        entriesVec = findSyncedEntryNumber(eventNumbersVecBase, eventNumberStatus, entry0, minNumberOfEntries);
        
        for (uint board{0}; board < NBOARD; board++) {
            if (!eventNumberStatus.at(board)) syncStatus = false;
        }
        
        if (!syncStatus) continue;
        
        if (suffix == "202111072346_clustered.root") {
            if (entry0 > 1979 and entry0 < 15795) // 1489 15273
                entriesVec.at(1) = entry0 + 1;
            if (entry0 > 15795 and entry0 < 19190) // 18710
                entriesVec.at(1) = entry0 + 1;
            if (entry0 >= 19190)
                entriesVec.at(1) = entry0 + 2;
            if (entry0 > 31800) // 31401
                entriesVec.at(1) = entry0 + 3;

            if (entry0 > 1370 and entry0 < 15795) // 882 15273
                entriesVec.at(0) = entry0 + 1;
            if (entry0 >= 15795)
                entriesVec.at(0) = entry0 + 2;
        }
        
        if (suffix == "202111080545_clustered.root") {
            if (entry0 > 4710 and entry0 < 14046) { // 4606 13949
                if (entry0 < 9790) entriesVec.at(2) = entry0 - 1; // 9697
                else entriesVec.at(2) = entry0 + 1;
            }

            if (entry0 > 4710) { // 4606
                if (entry0 >= 9790 and entry0 < 14046)
                    entriesVec.at(1) = entry0 + 1;
                else if (entry0 < 9790)
                    entriesVec.at(1) = entry0 - 1;
                else if (entry0 > 20713) // 20621
                    entriesVec.at(1) = entry0 + 6;
                else entriesVec.at(1) = entry0 + 5;
            }
        }
        
        if (suffix == "beam_40V_800us_busy_07082022_2010_clustered.root") {
            if (entry0 > 119490 and entry0 < 222040) {
                entriesVec.at(0) = entry0 + 1;
            }
            if (entry0 > 227878) entriesVec.at(2) = entry0 - 1;
        }
        
        if (suffix == "beam_40V_800us_busy_07082022_1610_clustered.root") {
            if (entry0 > 156418 and entry0 < 181964) {
                entriesVec.at(1) = entry0 - 1; // correct
            }
            if (entry0 > 181963 and entry0 < 221529) {
                entriesVec.at(1) = entry0 - 4;
            }
            if (entry0 > 271514 and entry0 < 340300) {
                entriesVec.at(1) = entry0 - 3;
            }
            if (entry0 > 78024 and entry0 < 156527) {
                entriesVec.at(2) = entry0 + 4; // + 3
            }
            if (entry0 > 221453 and entry0 < 271515) {
                entriesVec.at(2) = entry0 + 1; // correct
            }
            if (entry0 > 271514 and entry0 < 340300) {
                entriesVec.at(2) = entry0 + 2; // correct
            }
            if (entry0 > 340299) {
                entriesVec.at(2) = entry0 + 1; // correct
            }
            
        }
        
        if (suffix == "beam_40V_800us_busy_02102022_1810_clustered.root") {
			if (entry0 > 0) {
				entriesVec.at(2) = entry0 + 2;
			}
		}
        
        if (suffix == "beam_40V_800us_busy_02102022_0325_clustered.root") {
            if (entry0 > 0) {
                entriesVec.at(0) = entry0 + 1;
                entriesVec.at(1) = entry0 + 1;
            }
        }
        
        if (suffix == "test_cosmics_19032023_2150_clustered.root") {
            if (entry0 > 0) {
                entriesVec.at(0) = entry0 + 4;
                entriesVec.at(2) = entry0 + 5;
            }
        }
        
        for (uint board{0}; board < NBOARD; board++) {
            ClustersTree[board]->GetEntry(entriesVec.at(board));
        }
        
        for (std::size_t i = 0; i < lengths.size(); ++i) lengths.at(i) = integral_map.at(i)->size();

        if (lengths.at(0) == 1 and lengths.at(1) == 1 and lengths.at(2) == 1 and lengths.at(3) == 1)
        {
            
            for (int i{0}; i < yCoord->GetNrows(); ++i) {
                (*yCoord)[i] = cog_map.at(i)->at(0);
                (*zCoord)[i] = detectorPos.at(i);
                if (june2022BT && i % 2 == 0) {
                    (*yCoord)[i] = 2047 - cog_map.at(i)->at(0);
                }
            }
            
            fitFunc = new TF1("fitFunc","[0] * (x - [1]) * (x - [1]) + [2]", detectorPos.front(), detectorPos.at(3));
            fitFunc->SetParameter(0, estimateCurvature(*yCoord, *zCoord));
            fitFunc->SetParameter(1, findAverageValue(*zCoord));
            fitFunc->SetParameter(2, findAverageValue(*yCoord));
            // fitFunc->SetParLimits(0, -10.0 * estimateCurvature(*yCoord, *zCoord), 10.0 * estimateCurvature(*yCoord, *zCoord));
            // fitFunc->SetParLimits(1, detectorPos.front(), detectorPos.at(3));
            // fitFunc->SetParLimits(2, 0., 2048.);
            gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
            TFitResultPtr r = gr->Fit(fitFunc, "QRS EX0");
            // cout << "entry " << entry0 << endl;
            if (june2022BT) {
                h2corr->Fill(2047 - cog_map.at(0)->at(0), 2047 - cog_map.at(2)->at(0));
            }
            else h2corr->Fill(cog_map.at(0)->at(0), cog_map.at(2)->at(0));
            // h2diff->Fill(entry0, TMath::Abs(cog_map.at(0)->at(0) - cog_map.at(2)->at(0)) + TMath::Abs(cog_map.at(1)->at(0) - cog_map.at(3)->at(0)));
            if (june2022BT) {
                grdiff->SetPoint(grdiff->GetN(), entry0, (TMath::Abs(2047 - cog_map.at(0)->at(0) - fitFunc->Eval((*zCoord)[0])) + TMath::Abs(fitFunc->Eval((*zCoord)[2]) - 2047 + cog_map.at(2)->at(0)) + TMath::Abs(fitFunc->Eval((*zCoord)[1]) - cog_map.at(1)->at(0)) + TMath::Abs(fitFunc->Eval((*zCoord)[3]) - cog_map.at(3)->at(0))) / 4.);
                diff1 = (Long64_t)(frameCounter_map.at(2) - clockCounterVecBase.at(1)) - (Long64_t)(frameCounter_map.at(0) - clockCounterVecBase.at(0));
                grdiffClock1->SetPoint(grdiffClock1->GetN(), entry0, diff1);
                
            }
            else grdiff->SetPoint(grdiff->GetN(), entry0, (TMath::Abs(cog_map.at(0)->at(0) - fitFunc->Eval((*zCoord)[0])) + TMath::Abs(fitFunc->Eval((*zCoord)[2]) - cog_map.at(2)->at(0)) + TMath::Abs(fitFunc->Eval((*zCoord)[1]) - cog_map.at(1)->at(0)) + TMath::Abs(fitFunc->Eval((*zCoord)[3]) - cog_map.at(3)->at(0))) / 4.);
        }
        // from this we see that most probably the last event to be in coincidence between board 0 and 1 is 22597.
        if (NBOARD > 2) {
            if (lengths.at(0) == 1 and lengths.at(1) == 1 and lengths.at(4) == 1 and lengths.at(5) == 1)
            {
                for (int i{0}; i < yCoord->GetNrows(); ++i) {
                    if (i < 2) {
                        (*yCoord)[i] = cog_map.at(i)->at(0);
                        (*zCoord)[i] = detectorPos.at(i);
                        if (june2022BT && i % 2 == 0) {
                            (*yCoord)[i] = 2047 - cog_map.at(i)->at(0);
                        }
                    }
                    else {
                        (*yCoord)[i] = cog_map.at(i + 2)->at(0);
                        (*zCoord)[i] = detectorPos.at(i + 2);
                        if (june2022BT && i % 2 == 0) {
                            (*yCoord)[i] = 2047 - cog_map.at(i + 2)->at(0);
                        }
                    }
                }
                
                fitFunc = new TF1("fitFunc","[0] * (x - [1]) * (x - [1]) + [2]", detectorPos.front(), detectorPos.back());
                fitFunc->SetParameter(0, estimateCurvature(*yCoord, *zCoord));
                fitFunc->SetParameter(1, findAverageValue(*zCoord));
                fitFunc->SetParameter(2, findAverageValue(*yCoord));
                // fitFunc->SetParLimits(0, -10.0 * estimateCurvature(*yCoord, *zCoord), 10.0 * estimateCurvature(*yCoord, *zCoord));
                // fitFunc->SetParLimits(1, detectorPos.front(), detectorPos.back());
                // fitFunc->SetParLimits(2, 0., 2048.);
                gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
                TFitResultPtr r = gr->Fit(fitFunc, "QRS EX0");
                
                if (june2022BT) {
                    h2corr2->Fill(2047 - cog_map.at(0)->at(0), 2047 - cog_map.at(4)->at(0));
                }
                else h2corr2->Fill(cog_map.at(0)->at(0), cog_map.at(4)->at(0));
                // h2diff2->Fill(entry0, TMath::Abs(cog_map.at(0)->at(0) - cog_map.at(4)->at(0)) + TMath::Abs(cog_map.at(1)->at(0) - cog_map.at(5)->at(0)));
                if (june2022BT) {
                    grdiff2->SetPoint(grdiff2->GetN(), entry0, (TMath::Abs(2047 - cog_map.at(0)->at(0) - fitFunc->Eval((*zCoord)[0])) + TMath::Abs(fitFunc->Eval((*zCoord)[2]) - 2047 + cog_map.at(4)->at(0)) + TMath::Abs(fitFunc->Eval((*zCoord)[1]) - cog_map.at(1)->at(0)) + TMath::Abs(fitFunc->Eval((*zCoord)[3]) - cog_map.at(5)->at(0))) / 4.);
                    diff2 = (Long64_t)(frameCounter_map.at(4) - clockCounterVecBase.at(2)) - (Long64_t)(frameCounter_map.at(0) - clockCounterVecBase.at(0));
                    diff3 = (Long64_t)(frameCounter_map.at(4) - clockCounterVecBase.at(2)) - (Long64_t)(frameCounter_map.at(2) - clockCounterVecBase.at(1));
                    grdiffClock2->SetPoint(grdiffClock2->GetN(), entry0, diff2);
                    grdiffClock3->SetPoint(grdiffClock3->GetN(), entry0, diff3);
                }
                else grdiff2->SetPoint(grdiff2->GetN(), entry0, (TMath::Abs(cog_map.at(0)->at(0) - fitFunc->Eval((*zCoord)[0])) + TMath::Abs(fitFunc->Eval((*zCoord)[2]) - cog_map.at(4)->at(0)) + TMath::Abs(fitFunc->Eval((*zCoord)[1]) - cog_map.at(1)->at(0)) + TMath::Abs(fitFunc->Eval((*zCoord)[3]) - cog_map.at(5)->at(0))) / 4.);
            }
        }
        
        syncTree->Fill();
        
        for (uint board{0}; board < NBOARD; board++)
        {
            for (uint lad{0}; lad < NLAD; lad++)
            {
                uint index{lad + NLAD * board};
                integral_map.at(index)->clear();
                length_map.at(index)->clear();
                cog_map.at(index)->clear();
                sovern_map.at(index)->clear();
                sovernPerChannel_map.at(index)->clear();
            }
        }
    }

    TCanvas *c{new TCanvas("c", "c", 800, 600)};
    c->Divide(1, 2);
    c->cd(1);
    h2corr->Draw();
    c->cd(2);
    // h2diff->Draw();
    grdiff->Draw("pla");
    c->Update();

    TCanvas *c2{new TCanvas("c2", "c2", 800, 600)};
    c2->Divide(1, 2);
    c2->cd(1);
    h2corr2->Draw();
    c2->cd(2);
    // h2diff2->Draw();
    grdiff2->Draw("pla");
    c2->Update();
    
    TCanvas *c3{new TCanvas("c3", "c3", 800, 600)};
    c3->Divide(1, 3);
    c3->cd(1);
    grdiffClock1->SetTitle("Clock counter 1 - 0");
    grdiffClock1->GetXaxis()->SetTitle("event number");
    grdiffClock1->Draw("la");
    c3->cd(2);
    grdiffClock2->SetTitle("Clock counter 2 - 0");
    grdiffClock2->GetXaxis()->SetTitle("event number");
    grdiffClock2->Draw("la");
    c3->cd(3);
    grdiffClock3->SetTitle("Clock counter 2 - 1");
    grdiffClock3->GetXaxis()->SetTitle("event number");
    grdiffClock3->Draw("la");
    c3->Update();

    for (uint board{0}; board < NBOARD; board++) cout << ClustersTree[board]->GetEntries() << endl;
    
    syncOutFile->cd();
    syncTree->Write();
    syncOutFile->Close();
    
    delete syncOutFile;
}
