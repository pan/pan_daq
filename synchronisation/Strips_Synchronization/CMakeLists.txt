cmake_minimum_required(VERSION 3.17)
project(GPIOSync)

# Find Boost libraries
find_package(Boost REQUIRED COMPONENTS program_options)

# Find ROOT libraries
find_package(ROOT REQUIRED)

# Add the executable
add_executable(ReadClusterTree3 ${CMAKE_CURRENT_SOURCE_DIR}/Strips_Synchronization/ReadClusterTree3.cxx)

# Include Boost and ROOT directories
target_include_directories(ReadClusterTree3 PRIVATE ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

# Link Boost and ROOT libraries
target_link_libraries(ReadClusterTree3 PRIVATE ${Boost_LIBRARIES} ${ROOT_LIBRARIES})

# Apply compiler flags
target_compile_options(ReadClusterTree3 PRIVATE -fPIC -g -O2 -Wall -pthread -std=c++17 -m64)

# Specify the installation directory for the executables
install(TARGETS ReadClusterTree3 DESTINATION
	${CMAKE_CURRENT_SOURCE_DIR}/bin)