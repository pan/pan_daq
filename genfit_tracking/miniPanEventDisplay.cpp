//
//  miniPanEventDisplay.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 19.01.22.
//

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TVectorD.h"
#include "TFitResult.h"
#include "TEveManager.h"
#include "TGeoManager.h"
#include "TVector3.h"
#include "TDatabasePDG.h"
#include "TMath.h"

#include <iostream>
#include <numeric>
#include <string>
#include <algorithm>
#include <cmath>
#include <utility>
#include <vector>
#include <array>
#include <unordered_map>

#include "miniPanMagField.hpp"
#include "RectangularFinitePlane.h"
#include "StripXFinitePlane.hpp"
#include "ConstField.h"
#include "Exception.h"
#include "FieldManager.h"
#include "KalmanFitterRefTrack.h"
#include "KalmanFitter.h"
#include "AbsKalmanFitter.h"
#include "KalmanFitterInfo.h"
#include "FitStatus.h"
#include "MeasuredStateOnPlane.h"
#include "StateOnPlane.h"
#include "SharedPlanePtr.h"
#include "Track.h"
#include "TrackPoint.h"
#include "DetPlane.h"

#include "MaterialEffects.h"
#include "AbsTrackRep.h"
#include "RKTrackRep.h"
#include "TGeoMaterialInterface.h"

#include "EventDisplay.h"

#include "AbsMeasurement.h"
#include "PlanarMeasurement.h"
#include "MeasurementOnPlane.h"

typedef unsigned int uint;
typedef vector<double> vdouble;
typedef vector<int> vint;
typedef vector<string> vstring;

constexpr std::size_t NLAD{2};
constexpr std::size_t NBOARD{3};
constexpr std::size_t NBRANCH{5};
constexpr std::size_t NBRANCHPIX{3};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double conversionCoefPixel{0.0055}; // cm / ch
constexpr float detectorResolution(0.0025); // resolution of scd detectors in cm
constexpr float stripLength(5.12); // scd strip length in cm
float pTruth(10.0); // beamtest energy 10 GeV/c pi-
constexpr bool nov2021BT{false};
constexpr bool june2022BT{true};

vdouble detectorPos;
vdouble detectorPosErr = {0.01, 0.01, 0.01, 0.01, 0.01, 0.01};
std::array<vdouble, NLAD * NBOARD> deltas;
// genfit::AbsFinitePlane* finiteStripX = new genfit::StripXFinitePlane(2.55, 2.56);
genfit::AbsFinitePlane* finiteStripX = new genfit::RectangularFinitePlane(-2.55, 2.55, -2.56, 2.56);
vdouble detectorPosPixel = {-7.865, 7.865};

vector<vdouble> alignPars = { // deltaY, deltaZ, thetaX
    {7.5977900e-06, -6.8999600e-07, 8.30209e-05},
    {-3.7525000e-05, 9.1912700e-07, -0.000340924},
    {0.00022215100, -4.5594400e-08, 0.00020294},
    {-0.00017130100, 8.1104300e-08, 0.000157194},
    {-5.7136400e-06, -1.0651500e-06, 5.19773e-05},
    {-4.5760600e-05, 7.2106000e-07, 0.000498967}
    // {7.88065e-04, -0.193191e-04, 1.49739e-06},
    // {-29.9637e-04, 0.138847e-04, -1.20184e-05},
    // {16.8955e-04, -0.22272e-04, 2.54002e-06},
    // {11.3012e-04, 0.391145e-04, 1.06067e-05},
    // {21.4351e-04, -0.172721e-04, -7.14252e-07},
    // {-26.0258e-04, 0.0270422e-04, -1.69074e-05}
};

vdouble transformVectorsDouble(std::unordered_map<int, vdouble *> &vec, std::unordered_map<int, vdouble *> &vsoverns, std::size_t j, bool errorsFlag = false, bool enablePixels = false) {
    vdouble outVec;
    double alpha{0.0};
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (errorsFlag) {
            alpha = sqrt(1.5 + vec[i]->at(j));
            if (enablePixels) {
                outVec.push_back(conversionCoef / sqrt(12.0));
            }
            else {
                if (vec[i]->at(j) == 1) outVec.push_back(conversionCoef / sqrt(12.0));
                else outVec.push_back(alpha * conversionCoef / vsoverns[i]->at(j)); // due to CoG calculation
            }
        }
        else {
            outVec.push_back((vec[i]->at(j) - 1023.5) * conversionCoef);
        }
    }
    
    return outVec;
}

vdouble transformVectorsInt(std::unordered_map<int, vint *> &vec, std::unordered_map<int, vdouble *> &vsoverns, std::size_t j, bool errorsFlag = false, bool enablePixels = false) {
    
    std::unordered_map<int, vdouble *> convVec;
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        convVec[i] = new vdouble(vec[i]->begin(), vec[i]->end());
    }
    
    return transformVectorsDouble(convVec, vsoverns, j, errorsFlag, enablePixels);
}

void updateVcogs(std::unordered_map<int, vdouble *> &vec, std::unordered_map<int, vdouble *> &vecAligned, std::size_t j, bool enablePixels = false) {
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (i % 2 == 0 && !enablePixels) vecAligned[i]->push_back(2047.0 - vec[i]->at(j));
        else vecAligned[i]->push_back(vec[i]->at(j));
    }
}

vdouble updateCoGs(std::unordered_map<int, vdouble *> &vec) {
    vdouble newCoGs(NLAD * NBOARD);
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        newCoGs[i] = (vec[i]->at(0) - 1023.5) * conversionCoef + alignPars.at(i)[0] - detectorPos[i] * alignPars.at(i)[2];
        newCoGs[i] *= 1.0;
    }
    
    return newCoGs;
}

vdouble updateDetPos(std::unordered_map<int, vdouble *> &vec) {
    vdouble newDetPos(NLAD * NBOARD);
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        newDetPos[i] = detectorPos[i] + alignPars.at(i)[1] + (1.0) * (vec[i]->at(0) - 1023.5) * conversionCoef * alignPars.at(i)[2];
    }
    return newDetPos;
}

TVectorD removeElement(std::size_t i, TVectorD * vecOrig) {
    TVectorD outVec, tempVecFirst(*vecOrig), tempVecSecond(*vecOrig);
    
    tempVecFirst.ResizeTo(vecOrig->GetLwb(), i-1);
    tempVecSecond.ResizeTo(i+1,vecOrig->GetUpb());
    outVec.ResizeTo(vecOrig->GetNrows() - 1);
    outVec.SetSub(0, tempVecFirst);
    outVec.SetSub(tempVecFirst.GetNrows(), tempVecSecond);
    
    return outVec;
}

bool notSingle(std::size_t n) {
    return (n != 1);
}

bool notTwo(std::size_t n) {
    return (n != 2);
}

bool notThree(std::size_t n) {
    return (n != 3);
}

bool notFour(std::size_t n) {
    return (n != 4);
}

bool fivePlus(std::size_t n) {
    return (n < 5);
}

bool performFitting(const TVectorD &zCoord, const TVectorD &yCoord, const TVectorD &zErr, const TVectorD &yErr, const TVectorD &xPixel, const TVectorD &yPixel, const TVectorD &xPixelErr, const TVectorD &yPixelErr, bool enablePixels, bool useAlignedGeom, genfit::Track* &fitTrack, genfit::KalmanFitterRefTrack* &fitter) {
    
    int detId(0); // detector ID
    int planeId(0);
    int hitId(0); // hit ID
    
    genfit::PlanarMeasurement* measurement = nullptr;
    //*
    TMatrixDSym hitCov(2);
    hitCov.UnitMatrix();
    hitCov(1,1) = detectorResolution*detectorResolution / 12; // include misalignment factor?
    hitCov(0,0) = stripLength * stripLength / 4; // 12 - classic approach

    TVectorD hitCoords(2);
    hitCoords[0] = 0.;
    hitCoords[1] = 0.;
    //*/
    /*
    TMatrixDSym hitCov(1);
    hitCov.UnitMatrix();
    hitCov *= detectorResolution*detectorResolution / 12;

    TVectorD hitCoords(1);
    hitCoords[0] = 0;
    //*/
    TVector3 detPlaneN, detPlaneU, detPlaneV, detPlane0;
    genfit::AbsTrackRep* cardRep;
    genfit::FitStatus* fitStatus = nullptr;
    
    if (enablePixels) {
        for (std::size_t i{0}; i < NLAD * (NBOARD + 1); ++i) {
            detPlaneN.SetXYZ(0,0,1);
            detPlaneN.SetMag(1);
            if (i == 0) {
                hitCoords[0] = xPixel[i];
                detPlane0.SetXYZ(0, 0, detectorPosPixel.front());
                hitCov(0,0) = xPixelErr[i] * xPixelErr[i];
                hitCoords[1] = yPixel[i];
                hitCov(1,1) = yPixelErr[i] * yPixelErr[i];
            }
            else if (i == NLAD * (NBOARD + 1) - 1) {
                hitCoords[0] = xPixel[1];
                detPlane0.SetXYZ(0, 0, detectorPosPixel.back());
                hitCov(0,0) = xPixelErr[1];
                hitCoords[1] = yPixel[1];
                hitCov(1,1) = yPixelErr[1] * yPixelErr[1];
            }
            else {
                hitCoords[0] = 0.;
                detPlane0.SetXYZ(0, 0, zCoord[i - 1]);
                hitCov(0,0) = stripLength * stripLength / 4;
                hitCoords[1] = yCoord[i - 1];
                hitCov(1,1) = yErr[i - 1] * yErr[i - 1] + detectorResolution * detectorResolution * 16 / 25;
            }
            std::cout << "hitCoords[0]: " << hitCoords[0] << std::endl;
            std::cout << "hitCoords[1]: " << hitCoords[1] << std::endl;
            detPlaneU = detPlaneN.Orthogonal();
            detPlaneU.SetMag(1);
            detPlaneV = detPlaneN.Cross(detPlaneU);
            detPlaneV.SetMag(1);
            detPlaneU.SetXYZ(fabs(detPlaneU.X()), fabs(detPlaneU.Y()), fabs(detPlaneU.Z()));
            detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
            measurement = new genfit::PlanarMeasurement(hitCoords, hitCov, ++detId, hitId, nullptr);
            measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finiteStripX->clone())), planeId);
            // measurement->setStripV();
            fitTrack->insertPoint(new genfit::TrackPoint(measurement->clone(), fitTrack));
            delete measurement;
            measurement = nullptr;
        }
    }
    else {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            detPlaneN.SetXYZ(0,0,1);
            detPlaneN.SetMag(1);
            hitCoords[0] = 0.0;
            if (useAlignedGeom) {
                detPlaneN.RotateX(TMath::DegToRad() * alignPars.at(i)[2]);
                detPlane0.SetXYZ(0, 0, detectorPos[i] + alignPars.at(i)[1]);
                hitCoords[1] = yCoord[i] / TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]);
                hitCov(1,1) = yErr[i] * yErr[i] / (TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]) * TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]));
                // hitCoords[0] = yCoord[i] / TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]);
                // hitCov(0,0) = yErr[i] * yErr[i] / (TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]) * TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]));
            }
            else {
                detPlane0.SetXYZ(0, 0, zCoord[i]);
                hitCoords[1] = yCoord[i];
                // hitCov(1,1) = yErr[i] * yErr[i];
                hitCov(1,1) = yErr[i] * yErr[i] + detectorResolution * detectorResolution * 16 / 25; // 16 / 25 default; 5 / 25 for 1 GeV/c
                // hitCoords[0] = yCoord[i];
                // hitCov(0,0) = yErr[i] * yErr[i];
            }
            detPlaneU = detPlaneN.Orthogonal();
            detPlaneU.SetMag(1);
            detPlaneV = detPlaneN.Cross(detPlaneU);
            detPlaneV.SetMag(1);
            if (useAlignedGeom) {
                detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
            }
            else {
                detPlaneU.SetXYZ(fabs(detPlaneU.X()), fabs(detPlaneU.Y()), fabs(detPlaneU.Z()));
                detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
            }
            measurement = new genfit::PlanarMeasurement(hitCoords, hitCov, ++detId, hitId, nullptr);
            if (!useAlignedGeom) measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finiteStripX->clone())), planeId);
            else measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneV, detPlaneU, finiteStripX->clone())), planeId);
            // measurement->setStripV();
            fitTrack->insertPoint(new genfit::TrackPoint(measurement->clone(), fitTrack));
            delete measurement;
            measurement = nullptr;
        }
    }
    
    if (fitTrack->getNumPoints() == 0) return false;
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track before fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    // do the fit
    try {
        fitter->processTrack(fitTrack);
    }
    catch (genfit::Exception& e) {
        std::cout << "genfit failed to fit track" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track after fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->determineCardinalRep();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with cardinal rep determination after fit" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->getFittedState();
        // print fit result
        // (fitTrack->getFittedState()).Print();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with fittedstate" << std:: endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    cardRep = fitTrack->getCardinalRep();
    
    try {
        fitStatus = fitTrack->getFitStatus(cardRep);
        std::cout << "N Failed Points: " << fitStatus->getNFailedPoints() << std::endl;
        if (!fitStatus->isFitted()) return false;
        auto nmeas = fitStatus->getNdf();
        if (nmeas < 1) return false;
        auto chi2Value  = fitStatus->getChi2()/nmeas;
        std::cout << "N measurements: " << nmeas << std::endl;
        std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
        if (chi2Value < 0) return false;
        if (!fitStatus->isFitConverged()) {
            try {
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->determineCardinalRep();
                fitTrack->getFittedState();
                cardRep = fitTrack->getCardinalRep();
                fitStatus = fitTrack->getFitStatus(cardRep);
                nmeas = fitStatus->getNdf();
                if (nmeas < 1) return false;
                chi2Value  = fitStatus->getChi2()/nmeas;
                std::cout << "N measurements: " << nmeas << std::endl;
                std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
                if (chi2Value < 0) return false;
            } catch (genfit::Exception& e) {
                std::cerr << "Exception" << std::endl;
                std::cerr << e.what();
                throw e;
            }
        }
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with Fit Status" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    delete measurement;
    measurement = nullptr;
    
    return true;
}

bool multipleClusterLength(std::unordered_map<int, vint *> vlengths) {
    bool result{true};
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (vlengths.at(i)->front() == 1) result = false;
    }

    return result;
}

void TracksFitting(std::size_t nAlignIter = 10, std::string inputFile = "synced_clusters.root", std::string outputFile = "tracking_histos_aligned_genfit.root", std::string geomFile = "MiniPANGeom.root", bool useAlignedGeom = false, bool beamTestData = true, bool useFieldMap = true, bool enablePixels = false, bool excludeSingleStripClusters = false) {
    
    gStyle->SetOptFit(1);
    
    std::unordered_map<int, vdouble *> vintegrals, vcogs, vsoverns, vcogsAligned, vquadX, vquadY;
    std::unordered_map<int, std::vector<std::vector<double>> *> vsovernsPerChannel;
    std::unordered_map<int, vint *> vlengths, vpixelSizes;
    
    if (nov2021BT) detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81 cm // November 2021 beamtest
    if (june2022BT) detectorPos = {-7.006, -6.206, -0.470, 0.330, 5.870, 6.670}; // 0, 8.0, 65.36, 73.36, 128.76, 136.76 mm // June 2022 beamtest
    
    const genfit::eFitterType fitterId = genfit::RefKalman;
    const int nIter = 20; // max number of iterations
    const double dPVal = 1.E-3; // convergence criterion
    int pdg;
    if (!beamTestData) pdg = 13; // particle pdg code for cosmics (mu-)
    else {
        pdg = -211; // pi- during beamtest
        if (june2022BT) {
            pdg = 11; // e-
            pTruth = 0.5; // 1 GeV/c, 0.5 GeV/c
            if (enablePixels) {
                pdg = 2212; // proton
                pTruth = 180; // 180 GeV;
            }
        }
    }
    genfit::KalmanFitterRefTrack* fitter = 0;
    fitter = new genfit::KalmanFitterRefTrack(nIter, dPVal);
    fitter->setMaxIterations(nIter);
    //fitter->useSquareRootFormalism();
    
    auto geoMan = new TGeoManager("Geometry", "miniPAN geometry");
    geoMan->Import(geomFile.c_str());
    
    if (!useFieldMap) {
        genfit::FieldManager::getInstance()->init(new genfit::ConstField(0., -0.00065, 0.)); // in kGauss 1 T = 10 kG here we set the mag field of the Earth
        // genfit::FieldManager::getInstance()->init(new genfit::ConstField(-4.0, 0., 0.));
    } else {
        if (nov2021BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("n48","2-magnets-30mm-N48.txt"));
        if (june2022BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("june2022","magnet_map_june2022.txt"));
        genfit::FieldManager::getInstance()->useCache(true, 8);
    }
    
    genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
    
    const double charge = TDatabasePDG::Instance()->GetParticle(pdg)->Charge()/(3.);
    
    genfit::EventDisplay* display = genfit::EventDisplay::getInstance();
    display->reset();
    
    TGraphErrors *gr;
    
    TFile *inFile = TFile::Open(inputFile.c_str(), "read");
    TTree *clustersTree = (TTree *) inFile->Get("sync_clusters_tree");
    if (enablePixels) clustersTree = (TTree *) inFile->Get("CoincidentClusters");
    
    std::vector<std::array<TBranch *, NBRANCH>> branches;
    std::vector<std::array<TBranch *, NBRANCHPIX>> branchesPixel;
    std::array<TBranch *, NBRANCH> temp_arr;
    std::array<TBranch *, NBRANCHPIX> temp_arr_pix;
    branches.assign(NLAD * NBOARD, temp_arr);
    branchesPixel.assign(NLAD, temp_arr_pix);
    
    const auto &nEntries = clustersTree->GetEntries();
    
    if (!enablePixels) {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            vintegrals[i] = new vdouble;
            vcogs[i] = new vdouble;
            vsoverns[i] = new vdouble;
            vsovernsPerChannel[i] = new std::vector<std::vector<double>>;
            vlengths[i] = new vint;
            vcogsAligned[i] = new vdouble;
            clustersTree->SetBranchAddress(Form("integral%lu", i), &vintegrals.at(i), &((branches.at(i)).at(0)));
            clustersTree->SetBranchAddress(Form("cog%lu", i), &vcogs.at(i), &((branches.at(i)).at(2)));
            clustersTree->SetBranchAddress(Form("sovern%lu", i), &vsoverns.at(i), &((branches.at(i)).at(3)));
            clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", i), &vsovernsPerChannel.at(i), &((branches.at(i)).at(4)));
            clustersTree->SetBranchAddress(Form("length%lu", i), &vlengths.at(i), &((branches.at(i)).at(1)));
        }
    }
    else {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            vintegrals[i] = new vdouble;
            vcogs[i] = new vdouble;
            vsoverns[i] = new vdouble;
            vsovernsPerChannel[i] = new std::vector<std::vector<double>>;
            vlengths[i] = new vint;
            vcogsAligned[i] = new vdouble;
            clustersTree->SetBranchAddress(Form("stripx%lu_integral", i + 1), &vintegrals.at(i), &((branches.at(i)).at(0)));
            clustersTree->SetBranchAddress(Form("stripx%lu_cog", i + 1), &vcogs.at(i), &((branches.at(i)).at(2)));
            // clustersTree->SetBranchAddress(Form("sovern%lu", i), &vsoverns.at(i), &((branches.at(i)).at(3)));
            // clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", i), &vsovernsPerChannel.at(i), &((branches.at(i)).at(4)));
            clustersTree->SetBranchAddress(Form("stripx%lu_len", i + 1), &vlengths.at(i), &((branches.at(i)).at(1)));
            if (i < NLAD) {
                vquadX[i] = new vdouble;
                vquadY[i] = new vdouble;
                vpixelSizes[i] = new vint;
                clustersTree->SetBranchAddress(Form("quad%lu_x", i + 1), &vquadX.at(i), &((branchesPixel.at(i)).at(0)));
                clustersTree->SetBranchAddress(Form("quad%lu_y", i + 1), &vquadY.at(i), &((branchesPixel.at(i)).at(1)));
                clustersTree->SetBranchAddress(Form("quad%lu_size", i + 1), &vpixelSizes.at(i), &((branchesPixel.at(i)).at(2)));
            }
        }
    }
        
    // branch check
    
    if (enablePixels) {
        for (std::size_t i{0}; i < NLAD; ++i) {
            for (uint br{0}; br < branchesPixel.at(i).size(); br++) {
                cout << "ladder " << i << " branch " << br << " has name " << branchesPixel.at(i).at(br)->GetName() << endl;
            }
        }
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            for (uint br{0}; br < 3; br++) {
                cout << "ladder " << i << " branch " << br << " has name " << branches.at(i).at(br)->GetName() << endl;
            }
        }
    }
    else {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            for (uint br{0}; br < branches.at(i).size(); br++) {
                cout << "ladder " << i << " branch " << br << " has name " << branches.at(i).at(br)->GetName() << endl;
            }
        }
    }
    //*
    std::array<std::vector<double>, NLAD * NBOARD> arrHistos;
    TFile *outFile = TFile::Open(outputFile.c_str(), "recreate");
    TTree *tracksTree = new TTree("tracks_tree", "Fitted tracks");
    //*/
    float chi2, pvalue, rmse_x, rmse_y, rmse_z, momReco, momX, momY, momZ, momErr;
    float xInitDir, yInitDir, zInitDir, momInit, p0, p1;
    int hnmeas, hPDG, hConvTracks;
    UInt_t conversionDepth;
    std::vector<float> xErrors, yErrors, zErrors;
    vint lengthOut;
    vdouble integralOut;
    std::vector<std::vector<double>> sovernsPerChannelOut;
    TMatrixDSym covMatrix, correlMatrix;
    //*
    auto chi2br = tracksTree->Branch("chi2br", &chi2, "chi2/F");
    auto pvaluebr = tracksTree->Branch("pvaluebr", &pvalue, "pvalue/F");
    auto p0br = tracksTree->Branch("p0br", &p0, "p0/F");
    auto p1br = tracksTree->Branch("p1br", &p1, "p1/F");
    auto covMatrixbr = tracksTree->Branch("covMatrixbr", &covMatrix);
    auto corelMatrixbr = tracksTree->Branch("correlMatrixbr", &correlMatrix);
    auto rmse_ybr = tracksTree->Branch("rmse_ybr", &rmse_y, "rmse_y/F");
    //auto xdirbr = tracksTree->Branch("xdirbr", &xdir, "xdir/F");
    //auto ydirbr = tracksTree->Branch("ydirbr", &ydir, "ydir/F");
    //auto zdirbr = tracksTree->Branch("zdirbr", &zdir, "zdir/F");
    auto hnmeasbr = tracksTree->Branch("hnmeasbr", &hnmeas, "hnmeas/I");
    auto yerrbr = tracksTree->Branch("yerrbr", &yErrors);
    auto momRecobr = tracksTree->Branch("momRecobr", &momReco, "momReco/F");
    auto momXbr = tracksTree->Branch("momXbr", &momX, "momX/F");
    auto momYbr = tracksTree->Branch("momYbr", &momY, "momY/F");
    auto momZbr = tracksTree->Branch("momZbr", &momZ, "momZ/F");
    auto momErrbr = tracksTree->Branch("momErrbr", &momErr, "momErr/F");
    auto hPDGbr = tracksTree->Branch("hPDGbr", &hPDG, "hPDG/I");
    auto lengthbr = tracksTree->Branch("clusterlengthbr", &lengthOut);
    auto integralbr = tracksTree->Branch("clusterintegralbr", &integralOut);
    auto sovernsbr = tracksTree->Branch("sovernsbr", &sovernsPerChannelOut);
    // auto hConvTracksbr = tracksTree->Branch("hConvTracksbr", &hConvTracks, "hConvTracks/I");
    //*/
    TCanvas *c = nullptr;
    std::size_t subCanvasIt{1};
    TF1 *fitFunc = new TF1("fitFunc","pol1(0)", detectorPos.front(), detectorPos.back());
    TVectorD *yCoord, *zCoord, *yErr, *zErr;
    TVectorD *xPixel, *yPixel, *xPixelErr, *yPixelErr;
    zCoord = new TVectorD(NLAD * NBOARD, &detectorPos[0]);
    zErr = new TVectorD(NLAD * NBOARD, &detectorPosErr[0]);
    xPixel = new TVectorD(NLAD);
    yPixel = new TVectorD(NLAD);
    xPixelErr = new TVectorD(NLAD);
    yPixelErr = new TVectorD(NLAD);
    TVectorD chi2array;
    double chi2Sum{0};
    
    unsigned int N1event{0}, N2events{0}, N3events{0}, N4events{0}, N5plusevents{0};
    unsigned int N1GoodEvent{0};
    unsigned int Nnot0Layer{0}, Nnot1Layer{0}, Nnot2Layer{0}, Nnot3Layer{0}, Nnot4Layer{0}, Nnot5Layer{0};
    
    vdouble deltaY(NBOARD * NLAD), deltaZ(NBOARD * NLAD), thetaX(NBOARD * NLAD);
    vdouble gradDeltaY(NBOARD * NLAD), gradDeltaZ(NBOARD * NLAD), gradThetaX(NBOARD * NLAD);
    double gamma{1.0}, reducingFactor{0.5}, epsilon{0.5};
    unsigned int patience{10}, badChi2Iter{0}, goodChi2Iter{0};
    
    genfit::Track* fitTrack(nullptr);
    TVector3 pos(0, 0, detectorPos.front());
    TVector3 mom;
    if (!beamTestData) mom = TVector3(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
    else mom = TVector3(0, 0, pTruth); // 10 GeV/c
    TMatrixDSym covM(6);
    for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution;
    for (int i = 3; i < 6; ++i) covM(i,i) = pow(detectorResolution / 6 / sqrt(3), 2); // 6 is set arbitrary ~ nTrackpoints
    
    genfit::MeasuredStateOnPlane fittedState;
    genfit::FitStatus* fitStatus = nullptr;
    TVector3 fittedTrackDir, primaryPartDir;
    genfit::TrackPoint* trackPoint = nullptr;
    genfit::AbsMeasurement* rawMeas = nullptr;
    genfit::KalmanFitterInfo* fitterInfo = nullptr;
    genfit::MeasurementOnPlane residual;
    genfit::AbsTrackRep* cardRep = nullptr;
    unsigned int nTrackPoints;
    int detId;
    TVectorD scdState(2);
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        
        delete fitTrack;
        fitTrack = nullptr;
        
        clustersTree->GetEntry(event);
        std::array<std::size_t, NBOARD * NLAD> len;
        std::array<std::size_t, NBOARD * NLAD>::iterator it;
        
        for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
        it = std::find_if(len.begin(), len.end(), notTwo);
        if (it == len.end()) N2events++;
        it = std::find_if(len.begin(), len.end(), notThree);
        if (it == len.end()) N3events++;
        it = std::find_if(len.begin(), len.end(), notFour);
        if (it == len.end()) N4events++;
        it = std::find_if(len.begin(), len.end(), fivePlus);
        if (it == len.end()) N5plusevents++;
        
        it = std::find_if(len.begin() + 1, len.end(), notSingle);
        if (it == len.end()) Nnot0Layer++;
        
        if (len[0] == 1) {
            it = std::find_if(len.begin() + 2, len.end(), notSingle);
            if (it == len.end()) Nnot1Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 2, notSingle);
        if (it == len.begin() + 2) {
            it = std::find_if(len.begin() + 3, len.end(), notSingle);
            if (it == len.end()) Nnot2Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 3, notSingle);
        if (it == len.begin() + 3) {
            it = std::find_if(len.begin() + 4, len.end(), notSingle);
            if (it == len.end()) Nnot3Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 4, notSingle);
        if (it == len.begin() + 4) {
            if (len[5] == 1) Nnot4Layer++;
        }
        
        it = std::find_if(len.begin(), len.end() - 1, notSingle);
        if (it == len.end() - 1) Nnot5Layer++;
        
        it = std::find_if(len.begin(), len.end(), notSingle);
        
        if (it == len.end() && vpixelSizes[0]->size() == 1 && vpixelSizes[1]->size() == 1) {
            
            if (!multipleClusterLength(vlengths) and excludeSingleStripClusters) continue;
            
            N1event++;
            
            std::cout << "event: " << event << std::endl;
            
            if (june2022BT) updateVcogs(vcogs, vcogsAligned, 0, enablePixels);
            else {
                
                
                for (std::size_t i{0}; i < NLAD * NBOARD; ++i) vcogsAligned[i]->push_back(vcogs[i]->at(0));
            }
            
            if (enablePixels) {
                for (std::size_t i{0}; i < NLAD; ++i) {
                    (*yPixel)[i] = (vquadX[i]->at(0) - 255.5) * conversionCoefPixel;
                    (*xPixel)[i] = (vquadY[i]->at(0) - 255.5) * conversionCoefPixel;
                    (*xPixelErr)[i] = conversionCoefPixel / sqrt(12. * vpixelSizes[i]->at(0));
                    (*yPixelErr)[i] = conversionCoefPixel / sqrt(12. * vpixelSizes[i]->at(0));
                }
            }

            fitFunc->SetParameter(0, (vcogsAligned[0]->at(0) - 1023.5) * conversionCoef);
            fitFunc->SetParameter(1, ((-1.0) * vcogsAligned[0]->at(0) + vcogsAligned[NBOARD * NLAD - 1]->at(0)) * conversionCoef / (detectorPos.back() - detectorPos.front()));
            fitFunc->SetParLimits(0, -1024.0 * conversionCoef, 1024.0 * conversionCoef);
            fitFunc->SetParLimits(1, -2048.0 * conversionCoef / (detectorPos.back() - detectorPos.front()), 2048.0 * conversionCoef / (detectorPos.back() - detectorPos.front()));
            
            yErr = new TVectorD(NLAD * NBOARD, &transformVectorsInt(vlengths, vsoverns, 0, true, enablePixels)[0]);
            
            if (useAlignedGeom) {
                yCoord = new TVectorD(NLAD * NBOARD, &updateCoGs(vcogsAligned)[0]);
                zCoord = new TVectorD(NLAD * NBOARD, &updateDetPos(vcogsAligned)[0]);
            } else {
                yCoord = new TVectorD(NLAD * NBOARD, &transformVectorsDouble(vcogsAligned, vsoverns, 0)[0]);
            }
            
            double sign(1.);
            genfit::AbsTrackRep* rep = new genfit::RKTrackRep(sign*pdg);
            genfit::MeasuredStateOnPlane stateRef(rep);
            if (!beamTestData) mom.SetXYZ(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
            else mom.SetXYZ(0, 0, pTruth); // 10 GeV/c
            if (!useAlignedGeom) pos.SetXYZ(0, 0, detectorPos.front());
            else pos.SetXYZ(0, 0, (*zCoord)[0]);
            if (enablePixels) pos.SetXYZ(0, 0, detectorPosPixel.front());
            for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution;
            for (int i = 3; i < 6; ++i) covM(i,i) = pow(detectorResolution / 6 / sqrt(3), 2); // 6 is set arbitrary ~ nTrackpoints
            rep->setPosMomCov(stateRef, pos, mom, covM);
            
            //second rep
            genfit::AbsTrackRep* secondRep = nullptr;
            if (!beamTestData) {
                sign = -1.;
                secondRep = new genfit::RKTrackRep(sign*pdg);
                genfit::MeasuredStateOnPlane stateSecondRef(secondRep);
                mom.SetXYZ(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
                if (!useAlignedGeom) pos.SetXYZ(0, 0, detectorPos.front());
                else pos.SetXYZ(0, 0, (*zCoord)[0]);
                for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution;
                for (int i = 3; i < 6; ++i) covM(i,i) = pow(detectorResolution / 6 / sqrt(3), 2); // 6 is set arbitrary ~ nTrackpoints
                secondRep->setPosMomCov(stateSecondRef, pos, mom, covM);
            }
                
            // create track
            TVectorD seedState(6);
            TMatrixDSym seedCov(6);
            rep->get6DStateCov(stateRef, seedState, seedCov);
            fitTrack = new genfit::Track(rep, seedState, seedCov); //initialized with smeared rep
            if (!beamTestData) fitTrack->addTrackRep(secondRep);
            
            std::cout << "after fitTrack definition: " << std::endl;
            
            std::cout << "before performFitting: " << std::endl;
            
            bool fitResult = performFitting(*zCoord, *yCoord, *zErr, *yErr, *xPixel, *yPixel, *xPixelErr, *yPixelErr, enablePixels, useAlignedGeom, fitTrack, fitter);
            
            std::cout << "after performFitting: " << std::endl;
            
            gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
            TFitResultPtr r = gr->Fit(fitFunc, "QRS");
            cardRep = fitTrack->getCardinalRep();
            
            fitStatus = fitTrack->getFitStatus(cardRep);
            chi2 = fitStatus->getChi2() / fitStatus->getNdf();
            pvalue = fitStatus->getPVal();
            if (pvalue > 1.0E-33) chi2Sum += fitStatus->getChi2();
            
            // correlMatrix.ResizeTo(r->GetCorrelationMatrix());
            
            // correlMatrix = r->GetCorrelationMatrix();
            p0 = r->Parameter(0);
            p1 = r->Parameter(1);
            // r->Print("V");
            /*
            if (event % 100 == 0) {
                if (!c) {
                    c = new TCanvas("c", "A fitted track", 1280, 800);
                    c->Divide(5,2);
                }
                if (subCanvasIt < 11) {
                    c->cd(subCanvasIt);
                    gr->SetTitle(Form("Event %lu", event));
                    gr->GetXaxis()->SetTitle("Layer position Z, cm");
                    gr->GetYaxis()->SetTitle("Channel number");
                    gr->Draw("AP");
                    fitFunc->Draw("same");
                    c->Update();
                    subCanvasIt++;
                }
            }*/
            
            hnmeas = yCoord->GetNrows();
            
            yErrors.clear();
            yErrors.shrink_to_fit();
            lengthOut.clear();
            lengthOut.shrink_to_fit();
            integralOut.clear();
            integralOut.shrink_to_fit();
            sovernsPerChannelOut.clear();
            sovernsPerChannelOut.shrink_to_fit();
            
            std::cout << "before residual calculation: " << std::endl;
            
            nTrackPoints = fitTrack->getNumPoints();
            
            std::cout << "nTrackPoints: " << nTrackPoints << std::endl;
            
            if (fitResult && fitStatus->isFitConverged()) {
                
                fittedState = fitTrack->getFittedState();
                covMatrix.ResizeTo(fittedState.getCov());
                covMatrix = fittedState.getCov();
                
                for (std::size_t i = 0; i < nTrackPoints; ++i) {
                    trackPoint = fitTrack->getPointWithMeasurementAndFitterInfo(i, cardRep);
                    rawMeas = trackPoint->getRawMeasurement();
                    fittedState = fitTrack->getFittedState(i, cardRep);
                    detId = rawMeas->getDetId();
                    fitterInfo = trackPoint->getKalmanFitterInfo();
                    residual = fitterInfo->getResidual();
                    scdState = residual.getState();
                    yErrors.push_back(scdState[1]);
                    if (i < NLAD * NBOARD) lengthOut.push_back((vlengths.at(i))->front());
                    if (!enablePixels) integralOut.push_back((vintegrals.at(i))->front());
                    if (!enablePixels) sovernsPerChannelOut.push_back((vsovernsPerChannel.at(i))->front());
                }
            
                // evaluateUnbiasedResiduals(yErrors, yCoord, yErr, zCoord, zErr, fitFunc);
                
                if (pvalue > 1.0E-33) N1GoodEvent++;
                
                rmse_y = TMath::RMS(yErrors.begin(), yErrors.end());
                
                momReco = fittedState.getMomMag();
                momErr = (momReco - pTruth) / pTruth;
                momX = (fittedState.getMom()).X();
                momY = (fittedState.getMom()).Y();
                momZ = (fittedState.getMom()).Z();
                
                hPDG = fittedState.getPDG();
                
                tracksTree->Fill();
                
                display->addEvent(fitTrack);
            }
            
            for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
                vcogsAligned[i]->clear();
                vcogsAligned[i]->shrink_to_fit();
            }
            
            std::cout << "after residual calculation: " << std::endl;

        }
    }
    
    chi2array.ResizeTo(1);
    chi2array[0] = chi2Sum / N1GoodEvent;
    chi2Sum = 0.;
    
    outFile->cd();
    tracksTree->Write();
    outFile->Close();
    
    inFile->Close();
    
    std::cout << "Total number of events: " << nEntries << std::endl;
    std::cout << "Number of events with 1 cluster in every plane: " << N1event << std::endl;
    std::cout << "Number of events with 2 clusters in every plane: " << N2events << std::endl;
    std::cout << "Number of events with 3 clusters in every plane: " << N3events << std::endl;
    std::cout << "Number of events with 4 clusters in every plane: " << N4events << std::endl;
    std::cout << "Number of events with 5+ clusters in every plane: " << N5plusevents << std::endl;
    
    std::cout << "Efficiency, layer 0: " << N1event * 100.0 / Nnot0Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 1: " << N1event * 100.0 / Nnot1Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 2: " << N1event * 100.0 / Nnot2Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 3: " << N1event * 100.0 / Nnot3Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 4: " << N1event * 100.0 / Nnot4Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 5: " << N1event * 100.0 / Nnot5Layer << " %" << std::endl;
    
    double totalEff = (N1event * 1.0 / Nnot0Layer) * (N1event * 1.0 / Nnot1Layer) * (N1event * 1.0 / Nnot2Layer) * (N1event * 1.0 / Nnot3Layer) * (N1event * 1.0 / Nnot4Layer) * (N1event * 1.0 / Nnot5Layer);
    
    std::cout << "Tracking efficiency: " << totalEff * 100.0 << " %" << std::endl;
    
    delete fitter;
    
    display->setOptions("ABDEFGHMPT"); // G show geometry
    display->open();
}
