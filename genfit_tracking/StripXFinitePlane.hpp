//
//  StripXFinitePlane.hpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 18.02.22.
//

#ifndef StripXFinitePlane_hpp
#define StripXFinitePlane_hpp

#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include "TObject.h"
#include "AbsFinitePlane.h"

namespace genfit {

class StripXFinitePlane : public AbsFinitePlane {
private:
    // Private Methods -----------------
    vector<double> dimensions_; // half-size in each dimension: u, v
protected:
    // protect from calling copy c'tor or assignment operator from outside the class. Use #clone() if you want a copy!
    StripXFinitePlane(const StripXFinitePlane&);
    StripXFinitePlane& operator=(const StripXFinitePlane&);

    ClassDef(StripXFinitePlane, 1);
public:
    StripXFinitePlane();
    StripXFinitePlane(double halfDimU, double halfDimV);
    StripXFinitePlane(const vector<double>& dim);

    virtual ~StripXFinitePlane();

    //! Returns whether a u,v point is in the active plane or not.
    bool isInActive(double u, double v) const;

    //! Deep copy ctor for polymorphic class.
    StripXFinitePlane* clone() const;

    void Print(const Option_t* = "") const;
};

}

#endif /* StripXFinitePlane_hpp */
