//
//  main.cpp
//  MiniPan_event_display
//
//  Created by Daniil Sukhonos on 13.02.24.
//

#include <boost/program_options.hpp>
#include <unordered_map>

#include "GFGbl.h"
#include "FieldManager.h"
#include "ConstField.h"
#include "MaterialEffects.h"
#include "TGeoMaterialInterface.h"
#include "EventDisplay.h"
#include "GblFitterInfo.h"
#include "PlanarMeasurement.h"
#include "RectangularFinitePlane.h"
#include "SharedPlanePtr.h"
#include "KalmanFitterInfo.h"
#include "KalmanFitterRefTrack.h"

#include "TGeoManager.h"
#include "TDatabasePDG.h"
#include "TGraphErrors.h"
#include "TRandom3.h"
#include "TH1D.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TROOT.h"

#include "miniPanMagField.hpp"

#define SHOW_TRACKS

namespace po = boost::program_options;

constexpr std::size_t nDetectors{6};
constexpr std::size_t nPixels{2};
constexpr std::size_t NBRANCH{5};
constexpr std::size_t NBRANCHPIX{5};
constexpr std::size_t nDetectorsY{3};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double conversionCoefY{0.04}; // cm / ch
constexpr double conversionCoefPixel{0.0055}; // cm / ch
constexpr float detectorResolution{0.0025}; // resolution of scd detectors in cm
constexpr float stripLength{5.12}; // stripx strip length in cm
constexpr double pixelLength{2.8215};
constexpr double pixelLengthY{2.8215};
float pTruth{1.0}; // beamtest energy 10 GeV/c pi-
double mass{0.0};
constexpr bool nov2021BT{false};
constexpr bool june2022BT{false};
constexpr bool sep2022BT{false};
constexpr bool nov2022BT{false};
constexpr bool apr2023BT{false};
constexpr bool summer2023BT{true};

std::vector<double> detectorPos, detectorPosY;
std::vector<double> detectorPosErr, detectorPosErrY;
// genfit::AbsFinitePlane* finiteStripX = new genfit::StripXFinitePlane(2.55, 2.56);
std::unique_ptr<genfit::AbsFinitePlane> finiteStripX = std::make_unique<genfit::RectangularFinitePlane>(-2.57, 2.57, -2.57, 2.57);
std::unique_ptr<genfit::AbsFinitePlane> finitePixel = std::make_unique<genfit::RectangularFinitePlane>(-1.414, 1.414, -1.414, 1.414);
std::vector<double> detectorPosPixel = {-7.8695, 7.8845};

TRandom3 r{42};

bool multipleClusterLength(const std::unordered_map<std::size_t, std::vector<int>*> &vlengths) {
    bool result{true};
    
    for (std::size_t i{0}; i < vlengths.size(); ++i) {
        if (vlengths.at(i)->front() == 1) result = false;
    }

    return result;
}

void correctStripPositions(const std::unordered_map<std::size_t, std::vector<double>*> &vec, std::unordered_map<std::size_t, std::vector<double>*> &vecAligned, bool generatedDataFlag, std::size_t j) {
    for (std::size_t i{0}; i < vecAligned.size(); ++i) {
        if (generatedDataFlag) vecAligned.at(i)->push_back(vec.at(i)->at(j));
        else {
            if (vec.at(i)->at(j) >= 0 and vec.at(i)->at(j) < 256) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) - 1);
                if (vec.at(i)->at(j) < (0 + 64)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (0 + 128)) vecAligned.at(i)->back() -= 0;
                else if (vec.at(i)->at(j) < (0 + 192)) vecAligned.at(i)->back() += 1;
                else if (vec.at(i)->at(j) < (0 + 256)) vecAligned.at(i)->back() += 2;
            }
            else if (vec.at(i)->at(j) >= 256 and vec.at(i)->at(j) < 512) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) + 1);
                if (vec.at(i)->at(j) < (256 + 64)) vecAligned.at(i)->back() -= 2;
                else if (vec.at(i)->at(j) < (256 + 128)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (256 + 192)) vecAligned.at(i)->back() += 0;
                else if (vec.at(i)->at(j) < (256 + 256)) vecAligned.at(i)->back() += 1;
            }
            else if (vec.at(i)->at(j) >= 512 and vec.at(i)->at(j) < 768) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) - 1);
                if (vec.at(i)->at(j) < (512 + 64)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (512 + 128)) vecAligned.at(i)->back() -= 0;
                else if (vec.at(i)->at(j) < (512 + 192)) vecAligned.at(i)->back() += 1;
                else if (vec.at(i)->at(j) < (512 + 256)) vecAligned.at(i)->back() += 2;
            }
            else if (vec.at(i)->at(j) >= 768 and vec.at(i)->at(j) < 1024) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) + 1);
                if (vec.at(i)->at(j) < (768 + 64)) vecAligned.at(i)->back() -= 2;
                else if (vec.at(i)->at(j) < (768 + 128)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (768 + 192)) vecAligned.at(i)->back() += 0;
                else if (vec.at(i)->at(j) < (768 + 256)) vecAligned.at(i)->back() += 1;
            }
            else if (vec.at(i)->at(j) >= 1024 and vec.at(i)->at(j) < 1280) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) - 1);
                if (vec.at(i)->at(j) < (1024 + 64)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (1024 + 128)) vecAligned.at(i)->back() -= 0;
                else if (vec.at(i)->at(j) < (1024 + 192)) vecAligned.at(i)->back() += 1;
                else if (vec.at(i)->at(j) < (1024 + 256)) vecAligned.at(i)->back() += 2;
            }
            else if (vec.at(i)->at(j) >= 1280 and vec.at(i)->at(j) < 1536) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) + 1);
                if (vec.at(i)->at(j) < (1280 + 64)) vecAligned.at(i)->back() -= 2;
                else if (vec.at(i)->at(j) < (1280 + 128)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (1280 + 192)) vecAligned.at(i)->back() += 0;
                else if (vec.at(i)->at(j) < (1280 + 256)) vecAligned.at(i)->back() += 1;
            }
            else if (vec.at(i)->at(j) >= 1536 and vec.at(i)->at(j) < 1792) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) - 1);
                if (vec.at(i)->at(j) < (1536 + 64)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (1536 + 128)) vecAligned.at(i)->back() -= 0;
                else if (vec.at(i)->at(j) < (1536 + 192)) vecAligned.at(i)->back() += 1;
                else if (vec.at(i)->at(j) < (1536 + 256)) vecAligned.at(i)->back() += 2;
            }
            else if (vec.at(i)->at(j) >= 1792 and vec.at(i)->at(j) < 2048) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) + 1);
                if (vec.at(i)->at(j) < (1792 + 64)) vecAligned.at(i)->back() -= 2;
                else if (vec.at(i)->at(j) < (1792 + 128)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (1792 + 192)) vecAligned.at(i)->back() += 0;
                else if (vec.at(i)->at(j) < (1792 + 256)) vecAligned.at(i)->back() += 1;
            }
        }
    }
}

void updateVcogs(std::unordered_map<std::size_t, std::vector<double> *> &vecAligned, std::size_t j, bool stripY) {
    for (std::size_t i{0}; i < vecAligned.size(); ++i) {
        if (stripY) {
            if (i == 1) vecAligned[i]->at(j) = 127. - vecAligned[i]->at(j);
        }
        else {
            if (i % 2 == 0) vecAligned[i]->at(j) = 2047. - vecAligned[i]->at(j);
        }
    }
}

double GetStripYLength(double strip)
{
    constexpr double L0{5.12325};

    double l{TMath::Sqrt(L0 * L0 - 4 * strip * strip)};

    return l;
}

template <typename T>
TVectorD* transformVectors(const std::unordered_map<std::size_t, std::vector<T>*> &vec, const std::unordered_map<std::size_t, std::vector<double>*> &vsoverns, std::size_t j, std::shared_ptr<TVectorD> Pixel = nullptr, bool enablePixels = false, bool enableStripY = false) {
    
    TVectorD* outVec = new TVectorD(3);
    double alpha{0};
    
    if (enablePixels && enableStripY) {
        outVec->ResizeTo(nDetectors + nDetectorsY + nPixels);
        for (std::size_t i{0}; i < outVec->GetNrows(); ++i) {
            (*outVec)[i] = 0;
            if (std::is_integral<T>::value) (*outVec)[i] = stripLength / sqrt(12.0);
            if (vec.size() == nDetectors) {
                if (i > 1 && i < 5) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 2)->at(j));
                        if (vec.at(i - 2)->at(j) == 1) (*outVec)[i] = conversionCoef / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoef / vsoverns.at(i - 2)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 2)->at(j) - 1023.5) * conversionCoef;
                }
                else if (i > 5 && i < 9) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 3)->at(j));
                        if (vec.at(i - 3)->at(j) == 1) (*outVec)[i] = conversionCoef / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoef / vsoverns.at(i - 3)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 3)->at(j) - 1023.5) * conversionCoef;
                }
            }
            else {
                if (i == 1) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 1)->at(j));
                        if (vec.at(i - 1)->at(j) == 1) (*outVec)[i] = conversionCoefY / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoefY / vsoverns.at(i - 1)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 1)->at(j) - 63.5) * conversionCoefY;
                }
                else if (i == 5) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 4)->at(j));
                        if (vec.at(i - 4)->at(j) == 1) (*outVec)[i] = conversionCoefY / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoefY / vsoverns.at(i - 4)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 4)->at(j) - 63.5) * conversionCoefY;
                }
                else if (i == 9) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 7)->at(j));
                        if (vec.at(i - 7)->at(j) == 1) (*outVec)[i] = conversionCoefY / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoefY / vsoverns.at(i - 7)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 7)->at(j) - 63.5) * conversionCoefY;
                }
            }
            if (i == 0) (*outVec)[i] = (*Pixel)[0];
            else if (i == outVec->GetNrows() - 1) (*outVec)[i] = (*Pixel)[1];
        }
    }
    else if (!enablePixels && enableStripY) {
        outVec->ResizeTo(nDetectors + nDetectorsY);
        for (std::size_t i{0}; i < outVec->GetNrows(); ++i) {
            (*outVec)[i] = 0;
            if (std::is_integral<T>::value) (*outVec)[i] = stripLength / sqrt(12.0);
            if (vec.size() == nDetectors) {
                if (i > 0 && i < 4) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 1)->at(j));
                        if (vec.at(i - 1)->at(j) == 1) (*outVec)[i] = conversionCoef / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoef / vsoverns.at(i - 1)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 1)->at(j) - 1023.5) * conversionCoef;
                }
                else if (i > 4 && i < 8) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 2)->at(j));
                        if (vec.at(i - 2)->at(j) == 1) (*outVec)[i] = conversionCoef / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoef / vsoverns.at(i - 2)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 2)->at(j) - 1023.5) * conversionCoef;
                }
            }
            else {
                if (i == 0) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i)->at(j));
                        if (vec.at(i)->at(j) == 1) (*outVec)[i] = conversionCoefY / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoefY / vsoverns.at(i)->at(j); // due to CoG calculation
                    }
                    else outVec[i] = (vec.at(i)->at(j) - 63.5) * conversionCoefY;
                }
                else if (i == 4) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 3)->at(j));
                        if (vec.at(i - 3)->at(j) == 1) (*outVec)[i] = conversionCoefY / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoefY / vsoverns.at(i - 3)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 3)->at(j) - 63.5) * conversionCoefY;
                }
                else if (i == 8) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 6)->at(j));
                        if (vec.at(i - 6)->at(j) == 1) (*outVec)[i] = conversionCoefY / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoefY / vsoverns.at(i - 6)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 6)->at(j) - 63.5) * conversionCoefY;
                }
            }
        }
    }
    else if (enablePixels && !enableStripY) {
        outVec->ResizeTo(nDetectors + nPixels);
        for (std::size_t i{0}; i < outVec->GetNrows(); ++i) {
            (*outVec)[i] = 0;
            if (std::is_integral<T>::value) (*outVec)[i] = stripLength / sqrt(12.0);
            if (vec.size() == nDetectors) {
                if (i > 0 && i < outVec->GetNrows() - 1) {
                    if (std::is_integral<T>::value) {
                        alpha = sqrt(1.5 + vec.at(i - 1)->at(j));
                        if (vec.at(i - 1)->at(j) == 1) (*outVec)[i] = conversionCoef / sqrt(12.0);
                        else (*outVec)[i] = alpha * conversionCoef / vsoverns.at(i - 1)->at(j); // due to CoG calculation
                    }
                    else (*outVec)[i] = (vec.at(i - 1)->at(j) - 1023.5) * conversionCoef;
                }
            }
            if (i == 0) (*outVec)[i] = (*Pixel)[0];
            else if (i == outVec->GetNrows() - 1) (*outVec)[i] = (*Pixel)[1];
        }
    }
    else {
        outVec->ResizeTo(nDetectors);
        for (std::size_t i{0}; i < outVec->GetNrows(); ++i) {
            (*outVec)[i] = 0;
            if (std::is_integral<T>::value) (*outVec)[i] = stripLength / sqrt(12.0);
            if (vec.size() == nDetectors) {
                if (std::is_integral<T>::value) {
                    alpha = sqrt(1.5 + vec.at(i)->at(j));
                    if (vec.at(i)->at(j) == 1) (*outVec)[i] = conversionCoef / sqrt(12.0);
                    else (*outVec)[i] = alpha * conversionCoef / vsoverns.at(i)->at(j); // due to CoG calculation
                }
                else (*outVec)[i] = (vec.at(i)->at(j) - 1023.5) * conversionCoef;
            }
        }
    }
    
    return outVec;
}

bool performFitting(const TVectorD &zCoord, const TVectorD &yCoord, const TVectorD &zErr, const TVectorD &yErr, const TVectorD &xCoord, const TVectorD &xErr, genfit::Track* fitTrack, std::shared_ptr<genfit::KalmanFitterRefTrack> fitter, bool enablePixels = false) {
    
    int detId{0}; // detector ID
    int planeId{0};
    int hitId{0}; // hit ID
    
    genfit::PlanarMeasurement* measurement{nullptr};
    //*
    TMatrixDSym hitCov(2);
    hitCov.UnitMatrix();
    hitCov(1, 1) = detectorResolution * detectorResolution / 12; // include misalignment factor?
    hitCov(0, 0) = stripLength * stripLength / 12; // 12 - classic approach

    TVectorD hitCoords(2);
    hitCoords[0] = 0.;
    hitCoords[1] = 0.;
    
    TVector3 detPlaneN, detPlaneU, detPlaneV, detPlane0;
    genfit::AbsTrackRep* cardRep{nullptr};
    genfit::FitStatus* fitStatus{nullptr};
    
    for (std::size_t i{0}; i < zCoord.GetNrows(); ++i) {
        detPlaneN.SetXYZ(0, 0, 1);
        detPlaneN.SetMag(1);
        detPlane0.SetXYZ(0, 0, zCoord[i]);
        
        hitCoords[1] = xCoord[i];
        hitCoords[0] = yCoord[i];
        hitCov(0, 0) = yErr[i] * yErr[i] * 3.;
        hitCov(1, 1) = xErr[i] * xErr[i] * 3.;
        
        detPlaneU = detPlaneN.Orthogonal();
        detPlaneU.SetMag(1);
        detPlaneV = detPlaneN.Cross(detPlaneU);
        detPlaneV.SetMag(1);
        detPlaneU.SetXYZ(fabs(detPlaneU.X()), fabs(detPlaneU.Y()), fabs(detPlaneU.Z()));
        detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
        
        measurement = new genfit::PlanarMeasurement(hitCoords, hitCov, ++detId, hitId, nullptr);
        if (enablePixels && (i == 0 || i == zCoord.GetNrows() - 1)) {
            measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finitePixel->clone())), ++planeId);
        }
        else measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finiteStripX->clone())), ++planeId);
        // measurement->setStripV();
        fitTrack->insertPoint(new genfit::TrackPoint(measurement->clone(), fitTrack));
        delete measurement;
        measurement = nullptr;
    }
    //}
    
    if (fitTrack->getNumPoints() == 0) return false;
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track before fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    // do the fit
    try {
        fitter->processTrack(fitTrack);
    }
    catch (genfit::Exception& e) {
        std::cout << "genfit failed to fit track" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track after fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->determineCardinalRep();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with cardinal rep determination after fit" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->getFittedState();
        // print fit result
        // (fitTrack->getFittedState()).Print();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with fittedstate" << std:: endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    cardRep = fitTrack->getCardinalRep();
    
    try {
        fitStatus = fitTrack->getFitStatus(cardRep);
        std::cout << "N Failed Points: " << fitStatus->getNFailedPoints() << std::endl;
        if (!fitStatus->isFitted()) return false;
        auto nmeas = fitStatus->getNdf();
        if (nmeas < 1) return false;
        auto chi2Value  = fitStatus->getChi2()/nmeas;
        std::cout << "N measurements: " << nmeas << std::endl;
        std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
        if (chi2Value < 0) return false;
        if (!fitStatus->isFitConverged()) {
            try {
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->determineCardinalRep();
                fitTrack->getFittedState();
                cardRep = fitTrack->getCardinalRep();
                fitStatus = fitTrack->getFitStatus(cardRep);
                nmeas = fitStatus->getNdf();
                if (nmeas < 1) return false;
                chi2Value  = fitStatus->getChi2()/nmeas;
                std::cout << "N measurements: " << nmeas << std::endl;
                std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
                if (chi2Value < 0) return false;
            } catch (genfit::Exception& e) {
                std::cout << "Double refit failed" << std::endl;
                std::cerr << "Exception" << std::endl;
                std::cerr << e.what();
                throw e;
            }
        }
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with Fit Status" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    return true;
}


int main(int argc, char * argv[]) {
    
    std::string inputFile, geomFile, pathToFieldMap;
    bool beamTestData, useFieldMap, enablePixels, enableStripY, excludeSingleStripClusters, enableSecondRep, enableThirdRep;
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "Produce help message")
        ("input-file,i", po::value<std::string>(&inputFile)->required(), "Input file name")
        ("geom-file,g", po::value<std::string>(&geomFile)->required(), "ROOT geometry file name")
        ("beamtest,e", po::value<bool>(&beamTestData)->default_value(true), "An input file is beamtest data")
        ("field-map,f", po::value<bool>(&useFieldMap)->default_value(true), "Use a simulated field map instead of a const field value")
        ("path-to-map,m", po::value<std::string>(&pathToFieldMap)->default_value("/Users/dasukhon/Documents/Documents_newMac/pan_daq/alignment/GBL_Millepede/GBL_Millepede/"), "Path to field map files")
        ("enable-second-rep,r", po::bool_switch(&enableSecondRep)->default_value(false), "Add second particle representation for better fitting")
        ("enable-third-rep,t", po::bool_switch(&enableThirdRep)->default_value(false), "Add third particle representation for better fitting")
        ("enable-pixels,p", po::bool_switch(&enablePixels)->default_value(false), "An input file contains pixels data as well")
        ("enable-stripy,y", po::bool_switch(&enableStripY)->default_value(false), "An input file contains StripY data as well");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    try {
        po::notify(vm);
    }
    catch (const po::required_option& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        std::cerr << desc << std::endl;
        return 1;
    }
    
    gROOT->SetBatch(kFALSE);
    TApplication* myApp = new TApplication("myApp", &argc, argv);
    
    std::unordered_map<std::size_t, std::vector<double> *> vintegrals, vcogs, vsoverns, vcogsAligned, vquadX, vquadY, vpixelRMSX, vpixelRMSY, vintegralsY, vcogsY, vsovernsY, vcogsAlignedY;
    std::unordered_map<std::size_t, std::vector<std::vector<double>> *> vsovernsPerChannel, vsovernsPerChannelY;
    std::unordered_map<std::size_t, std::vector<int> *> vlengths, vpixelSizes, vlengthsY;
    std::unordered_map<std::size_t, unsigned short> vpixelNumbers;
    
    if (nov2021BT) detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81 cm // November 2021 beamtest
    // if (june2022BT) detectorPos = {-7.006, -6.206, -0.470, 0.330, 5.870, 6.670}; // old
    if (june2022BT) detectorPos = {-7.006, -6.191, -0.470, 0.330, 5.870, 6.685}; // 0, 8.0, 65.36, 73.36, 128.76, 136.76 mm (sens. surf) // June/August 2022 beamtest
    if (sep2022BT) {
        detectorPosPixel = {-7.8985, 7.8985};
        detectorPos = {-6.699, -5.884, -0.316, 0.484, 5.884, 6.699};
    }
    if (nov2022BT) {
        detectorPosPixel = {-7.807, 7.807};
        detectorPos = {-6.6075, -5.7925, -0.4075, 0.3925, 5.7925, 6.6075};
    }
    if (apr2023BT || summer2023BT) {
        detectorPosPixel = {-7.8825, 7.8825};
        detectorPos = {-6.645, -5.83, -0.395, 0.4050, 5.83, 6.645};
        if (enableStripY) {
            if (nDetectorsY > 2) detectorPosY = {-7.04, 0.0460, 7.04};
            else detectorPosY = {-7.04, 0.0460};
            detectorPosErr.assign(nDetectorsY, 0.01);
        }
    }
    
    detectorPosErr.assign(nDetectors, 0.01);
    
    double edge_widthX_quad1{2.}, edge_widthY_quad1{2.}, edge_widthX_quad2{2.}, edge_widthY_quad2{2.};

    int pdg;
    if (!beamTestData) {
        pdg = 13; // particle pdg code for cosmics (mu-)
        pTruth = 0.1; // GeV/c
    }
    else {
        pdg = -211; // pi- during beamtest
        if (june2022BT) {
            pdg = 11; // e-
            pTruth = 0.5; // 1 GeV/c, 0.5 GeV/c
            if (enablePixels) {
                pdg = 2212; // proton
                pTruth = 180; // 180 GeV/c
            }
        }
        else if (sep2022BT) {
            pdg = 11; // e- is 11, protons 2212
            pTruth = 1; // GeV/c
        }
        else if (nov2022BT) {
            pdg = (TDatabasePDG::Instance())->GetParticle("alpha")->PdgCode();
            pTruth = 150; // 150 GeV/c
        }
        else if (apr2023BT) {
            pdg = 2212; // pi+ 211, proton 2212, positron -11
            pTruth = 0.5; // GeV/c
        }
        else if (summer2023BT) {
            pdg = 2212;
            pTruth = 0.5; // GeV/c
        }
    }
    
    mass = (TDatabasePDG::Instance())->GetParticle(pdg)->Mass();
    
    std::shared_ptr<genfit::KalmanFitterRefTrack> fitter{nullptr};
    fitter = std::make_shared<genfit::KalmanFitterRefTrack>();
    
    TGeoManager* geoMan = new TGeoManager("Geometry", "miniPAN geometry");
    geoMan->Import(geomFile.c_str());

    if (!useFieldMap) {
        // genfit::FieldManager::getInstance()->init(new genfit::ConstField(0, 0, 0)); // in kGauss 1 T = 10 kG here we set the mag field of the Earth
        genfit::FieldManager::getInstance()->init(new genfit::ConstField(-4.0, 0., 0.));
    } else {
        if (nov2021BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("30mm", pathToFieldMap + "2-magnets-30mm-N48.txt"));
        else if (june2022BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("june2022", pathToFieldMap + "magnet_map_june_august_2022.txt"));
        else if (sep2022BT || nov2022BT || apr2023BT || summer2023BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("latest", pathToFieldMap + "magnet_map_sep_2022_onwards.txt"));
        // genfit::FieldManager::getInstance()->useCache(true, 8);
    }
    
    genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
        
    // const double charge{TDatabasePDG::Instance()->GetParticle(pdg)->Charge()/(3.)};

    #ifdef SHOW_TRACKS
        std::unique_ptr<genfit::EventDisplay> display(genfit::EventDisplay::getInstance());
        display->disableOptions();
        display->setOptions("GHT"); // G show geometry
        display->reset();
    #endif
    
    std::unique_ptr<TGraphErrors> gr{nullptr};
        
    std::unique_ptr<TFile> inFile(TFile::Open(inputFile.c_str(), "read"));
    TTree* clustersTree;
    if (enablePixels) clustersTree = static_cast<TTree*>(inFile->Get("MiniPAN_Events"));
    else clustersTree = static_cast<TTree*>(inFile->Get("sync_clusters_tree"));
        
    std::vector<std::array<Int_t, NBRANCH>> branches, branchesY;
    std::vector<std::array<Int_t, NBRANCHPIX>> branchesPixel;
    std::array<Int_t, NBRANCH> temp_arr;
    std::array<Int_t, NBRANCHPIX> temp_arr_pix;
    branches.assign(nDetectors, temp_arr);
    branchesY.assign(nDetectorsY, temp_arr);
    branchesPixel.assign(nPixels, temp_arr_pix);
        
    const auto &nEntries = clustersTree->GetEntries();
    
    std::size_t j{0};
    for (std::size_t i{0}; i < nDetectors; ++i) {
        j = i;
        if (enableStripY) {
            if (i > 1) {
                j++;
                if (i > 3) j++;
            }
        }
        vintegrals[i] = nullptr;
        vcogs[i] = nullptr;
        vsoverns[i] = nullptr;
        vlengths[i] = nullptr;
        vsovernsPerChannel[i] = nullptr;
        vcogsAligned[i] = new std::vector<double>;
        (branches.at(i)).at(0) = clustersTree->SetBranchAddress(Form("integral%lu", j), &(vintegrals.at(i)));
        (branches.at(i)).at(2) = clustersTree->SetBranchAddress(Form("cog%lu", j), &(vcogs.at(i)));
        (branches.at(i)).at(3) = clustersTree->SetBranchAddress(Form("sovern%lu", j), &(vsoverns.at(i)));
        if (!enablePixels) (branches.at(i)).at(4) = clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", j), &(vsovernsPerChannel.at(i)));
        (branches.at(i)).at(1) = clustersTree->SetBranchAddress(Form("length%lu", j), &(vlengths.at(i)));
    }
    if (enableStripY) {
        std::size_t j{0};
        for (std::size_t i{0}; i < nDetectorsY; ++i) {
            j = 3 * i + 2;
            vintegralsY[i] = nullptr;
            vcogsY[i] = nullptr;
            vsovernsY[i] = nullptr;
            vlengthsY[i] = nullptr;
            vsovernsPerChannelY[i] = nullptr;
            vcogsAlignedY[i] = new std::vector<double>;
            (branchesY.at(i)).at(0) = clustersTree->SetBranchAddress(Form("integral%lu", j), &(vintegralsY.at(i)));
            (branchesY.at(i)).at(2) = clustersTree->SetBranchAddress(Form("cog%lu", j), &(vcogsY.at(i)));
            (branchesY.at(i)).at(3) = clustersTree->SetBranchAddress(Form("sovern%lu", j), &(vsovernsY.at(i)));
            if (!enablePixels) (branchesY.at(i)).at(4) = clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", j), &(vsovernsPerChannelY.at(i)));
            (branchesY.at(i)).at(1) = clustersTree->SetBranchAddress(Form("length%lu", j), &(vlengthsY.at(i)));
        }
    }
    
    if (enablePixels) {
        for (std::size_t i{0}; i < nPixels; ++i) {
            vquadX[i] = nullptr;
            vquadY[i] = nullptr;
            vpixelSizes[i] = nullptr;
            vpixelRMSX[i] = nullptr;
            vpixelRMSY[i] = nullptr;
            (branchesPixel.at(i)).at(0) = clustersTree->SetBranchAddress(Form("clstrMeanX_quad%d_mm", i > 0 ? 2 : 1), &vquadX.at(i));
            (branchesPixel.at(i)).at(1) = clustersTree->SetBranchAddress(Form("clstrMeanY_quad%d_mm", i > 0 ? 2 : 1), &vquadY.at(i));
            (branchesPixel.at(i)).at(2) = clustersTree->SetBranchAddress(Form("clstrSize_quad%d", i > 0 ? 2 : 1), &vpixelSizes.at(i));
            (branchesPixel.at(i)).at(3) = clustersTree->SetBranchAddress(Form("clstrStdX_quad%d_mm", i > 0 ? 2 : 1), &vpixelRMSX.at(i));
            (branchesPixel.at(i)).at(4) = clustersTree->SetBranchAddress(Form("clstrStdY_quad%d_mm", i > 0 ? 2 : 1), &vpixelRMSY.at(i));
        }
    }

    // branch check
        
    if (enablePixels) {
        for (std::size_t i{0}; i < nPixels; ++i) {
            for (uint br{0}; br < branchesPixel.at(i).size(); br++) {
                std::cout << "ladder " << i << " branch " << br << " has value " << branchesPixel.at(i).at(br) << std::endl;
            }
        }
        for (std::size_t i{0}; i < nDetectors; ++i) {
            for (uint br{0}; br < 3; br++) {
                std::cout << "ladder " << i << " branch " << br << " has value " << branches.at(i).at(br) << std::endl;
            }
        }
    }
    else {
        for (std::size_t i{0}; i < nDetectors; ++i) {
            for (uint br{0}; br < branches.at(i).size(); br++) {
                std::cout << "ladder " << i << " branch " << br << " has value " << branches.at(i).at(br) << std::endl;
            }
        }
    }
    
    double chi2, pvalue, momReco, momErr, momX, momY, momZ;
    int hnmeas, hPDG;

    std::shared_ptr<TVectorD> xCoord, yCoord, zCoord, xErr, yErr, zErr;
    std::shared_ptr<TVectorD> xPixel, yPixel, xPixelErr, yPixelErr;
    xPixel = std::make_shared<TVectorD>(nPixels);
    yPixel = std::make_shared<TVectorD>(nPixels);
    xPixelErr = std::make_shared<TVectorD>(nPixels);
    yPixelErr = std::make_shared<TVectorD>(nPixels);
    TVectorD chi2array;
    double chi2Sum{0};
    
    unsigned int N1event{0}, N2events{0}, N3events{0}, N4events{0}, N5plusevents{0};
    unsigned int N1GoodEvent{0};
    unsigned int Nnot0Layer{0}, Nnot1Layer{0}, Nnot2Layer{0}, Nnot3Layer{0}, Nnot4Layer{0}, Nnot5Layer{0};

    
    genfit::Track* fitTrack{nullptr};
    TVector3 pos(0, 0, detectorPos.front());
    TVector3 mom;
    mom = TVector3(0, 0, pTruth); // 10 GeV/c
    TMatrixDSym covM(6);
    for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
    for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
    genfit::AbsTrackRep* rep{nullptr};
    genfit::AbsTrackRep* secondRep{nullptr};
    genfit::AbsTrackRep* thirdRep{nullptr};
    
    genfit::MeasuredStateOnPlane fittedState;
    genfit::ReferenceStateOnPlane* refState{nullptr};
    genfit::FitStatus* fitStatus{nullptr};
    TVector3 fittedTrackDir, primaryPartDir;
    genfit::TrackPoint* trackPoint{nullptr};
    genfit::AbsMeasurement* rawMeas{nullptr};
    genfit::KalmanFitterInfo* fitterInfo{nullptr};
    genfit::MeasurementOnPlane residual;
    genfit::AbsTrackRep* cardRep{nullptr};
    unsigned int nTrackPoints;
    int detId;
    TVectorD scdState(2);
    std::vector<std::unique_ptr<TH1D>> residualsY;
    std::unique_ptr<TH1D> momentumReco = std::make_unique<TH1D>("mom", "Reconstructed Momentum;GeV/c", 500, 0., 5.);
    
    auto notSingle = [](std::size_t n) -> bool {
        return n != 1;
    };
    
    std::array<std::size_t, nDetectors> len;
    std::array<std::size_t, nDetectors>::iterator it;
    std::array<std::size_t, nDetectorsY> lenY;
    std::array<std::size_t, nDetectorsY>::iterator itY;
    std::size_t nTracks{0};
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        clustersTree->GetEntry(event);
        
        for (std::size_t i{0}; i < nDetectors; ++i) len[i] = vintegrals.at(i)->size();
        if (enableStripY) for (std::size_t i{0}; i < nDetectorsY; ++i) lenY[i] = vintegralsY.at(i)->size();
        
        it = std::find_if(len.begin(), len.end(), notSingle);
        if (!enableStripY) {
            if (it == len.end() && (enablePixels ? (vpixelSizes.at(0)->size() == 1 && vpixelSizes.at(1)->size() == 1) : true)) nTracks = 1;
            else continue;
        }
        else {
            itY = std::find_if(lenY.begin(), lenY.end(), notSingle);
            if (it == len.end() && itY == lenY.end() && (enablePixels ? (vpixelSizes.at(0)->size() == 1 && vpixelSizes.at(1)->size() == 1) : true)) {
                nTracks = 1;
            }
            else continue;
        }
        
        
        if (nTracks == 1) {
                        
            N1event++;
            
            std::cout << "event: " << event << std::endl;
            
            correctStripPositions(vcogs, vcogsAligned, true, 0);
            
            if (enableStripY) for (std::size_t i{0}; i < vcogsAlignedY.size(); ++i) vcogsAlignedY[i]->push_back(vcogsY[i]->at(0));
            if (!nov2021BT) {
                updateVcogs(vcogsAligned, 0, false);
                if (enableStripY) updateVcogs(vcogsAlignedY, 0, true);
                
            }
            else {
                for (std::size_t i{0}; i < nDetectors; ++i) vcogsAligned[i]->push_back(vcogs.at(i)->at(0));
            }

            if (enablePixels) {
                for (std::size_t i{0}; i < nPixels; ++i) {
                    if (i == 0) {
                        (*yPixel)[i + 1] = pixelLengthY - vquadY[i]->at(0) / 10. - pixelLengthY / 2.;
                        (*xPixel)[i + 1] = vquadX[i]->at(0) / 10. - pixelLength / 2.;
                        if (vpixelSizes[i]->at(0) == 1) {
                            (*xPixelErr)[i + 1] = conversionCoefPixel / sqrt(12.);
                            (*yPixelErr)[i + 1] = conversionCoefPixel / sqrt(12.);
                        }
                        else {
                            // an issue in the input file
                            // (*xPixelErr)[i + 1] = conversionCoefPixel / sqrt(12.);
                            if (vpixelRMSY[i]->at(0) > 1.e-4) (*xPixelErr)[i + 1] = (vpixelRMSY[i]->at(0)) / 10.;
                            else (*xPixelErr)[i + 1] = conversionCoefPixel / sqrt(12.);
                            if (vpixelRMSX[i]->at(0) > 1.e-4) (*yPixelErr)[i + 1] = (vpixelRMSX[i]->at(0)) / 10.;
                            else (*yPixelErr)[i + 1] = conversionCoefPixel / sqrt(12.);
                        }
                    }
                    else {
                        (*yPixel)[i - 1] = vquadY[i]->at(0) / 10. - pixelLengthY / 2.;
                        (*xPixel)[i - 1] = vquadX[i]->at(0) / 10. - pixelLength / 2.;
                        if (vpixelSizes[i]->at(0) == 1) {
                            (*xPixelErr)[i - 1] = conversionCoefPixel / sqrt(12.);
                            (*yPixelErr)[i - 1] = conversionCoefPixel / sqrt(12.);
                        }
                        else {
                            if (vpixelRMSY[i]->at(0) > 1.e-4) (*xPixelErr)[i - 1] = (vpixelRMSY[i]->at(0)) / 10.;
                            else (*xPixelErr)[i - 1] = conversionCoefPixel / sqrt(12.);
                            if (vpixelRMSX[i]->at(0) > 1.e-4) (*yPixelErr)[i - 1] = (vpixelRMSX[i]->at(0)) / 10.;
                            else (*yPixelErr)[i - 1] = conversionCoefPixel / sqrt(12.);
                        }
                    }
                    (*xPixelErr)[i] = conversionCoefPixel / sqrt(12.);
                    (*yPixelErr)[i] = conversionCoefPixel / sqrt(12.);
                }
            }
            
            xCoord = std::make_shared<TVectorD>(*transformVectors(vcogsAlignedY, vsoverns, 0, xPixel, enablePixels, enableStripY));
            yCoord = std::make_shared<TVectorD>(*transformVectors(vcogsAligned, vsoverns, 0, yPixel, enablePixels, enableStripY));
            yErr = std::make_shared<TVectorD>(*transformVectors(vlengths, vsoverns, 0, yPixelErr, enablePixels, enableStripY));
            xErr = std::make_shared<TVectorD>(*transformVectors(vlengthsY, vsoverns, 0, xPixelErr, enablePixels, enableStripY));
            if (enableStripY && enablePixels) {
                std::vector<double> detectorPosFinal;
                detectorPosFinal.insert(detectorPosFinal.end(), detectorPos.begin(), detectorPos.end());
                detectorPosFinal.insert(detectorPosFinal.end(), detectorPosPixel.begin(), detectorPosPixel.end());
                detectorPosFinal.insert(detectorPosFinal.end(), detectorPosY.begin(), detectorPosY.end());
                std::sort(detectorPosFinal.begin(), detectorPosFinal.end());
                zCoord = std::make_shared<TVectorD>(nDetectors + nPixels + nDetectorsY, &detectorPosFinal[0]);
                detectorPosErr.assign(nDetectors + nPixels + nDetectorsY, 0.01);
                zErr = std::make_shared<TVectorD>(nDetectors + nPixels + nDetectorsY, &detectorPosErr[0]);
                detectorPosFinal.clear();
                detectorPosFinal.shrink_to_fit();
                (*yErr)[1] = GetStripYLength((*xCoord)[1]) / sqrt(12.);
                (*yErr)[5] = GetStripYLength((*xCoord)[5]) / sqrt(12.);
                if (nDetectorsY > 2) (*yErr)[9] = GetStripYLength((*xCoord)[9]) / sqrt(12.);
            }
            else if (enableStripY && !enablePixels) {
                std::vector<double> detectorPosFinal;
                detectorPosFinal.insert(detectorPosFinal.end(), detectorPos.begin(), detectorPos.end());
                detectorPosFinal.insert(detectorPosFinal.end(), detectorPosY.begin(), detectorPosY.end());
                std::sort(detectorPosFinal.begin(), detectorPosFinal.end());
                zCoord = std::make_shared<TVectorD>(nDetectors + nDetectorsY, &detectorPosFinal[0]);
                detectorPosErr.assign(nDetectors + nDetectorsY, 0.01);
                zErr = std::make_shared<TVectorD>(nDetectors + nDetectorsY, &detectorPosErr[0]);
                detectorPosFinal.clear();
                detectorPosFinal.shrink_to_fit();
                (*yErr)[0] = GetStripYLength((*xCoord)[0]) / sqrt(12.);
                (*yErr)[4] = GetStripYLength((*xCoord)[4]) / sqrt(12.);
                if (nDetectorsY > 2) (*yErr)[8] = GetStripYLength((*xCoord)[8]) / sqrt(12.);
            }
            else if (!enableStripY && enablePixels) {
                std::vector<double> detectorPosFinal;
                detectorPosFinal.insert(detectorPosFinal.end(), detectorPos.begin(), detectorPos.end());
                detectorPosFinal.insert(detectorPosFinal.end(), detectorPosPixel.begin(), detectorPosPixel.end());
                std::sort(detectorPosFinal.begin(), detectorPosFinal.end());
                zCoord = std::make_shared<TVectorD>(nDetectors + nPixels, &detectorPosFinal[0]);
                detectorPosErr.assign(nDetectors + nPixels, 0.01);
                zErr = std::make_shared<TVectorD>(nDetectors + nPixels, &detectorPosErr[0]);
                detectorPosFinal.clear();
                detectorPosFinal.shrink_to_fit();
            }
            else {
                zCoord = std::make_shared<TVectorD>(nDetectors, &detectorPos[0]);
                detectorPosErr.assign(nDetectors, 0.01);
                zErr = std::make_shared<TVectorD>(nDetectors, &detectorPosErr[0]);
            }
            xCoord->Print();
            yCoord->Print();
            zCoord->Print();
            xErr->Print();
            yErr->Print();
            zErr->Print();
            int sign{1};
            rep = new genfit::RKTrackRep(sign * pdg);
            genfit::MeasuredStateOnPlane stateRef(rep);
            mom.SetXYZ(0, 0, pTruth); // 10 GeV/c
            pos.SetXYZ((*yCoord)[0], (*xCoord)[0], (*zCoord)[0]);
            for (int i = 0; i < 3; ++i) covM(i, i) = detectorResolution * detectorResolution * 100.;
            for (int i = 3; i < 6; ++i) covM(i, i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
            rep->setPosMomCov(stateRef, pos, mom, covM);
            
            //second rep
            if (!beamTestData || enableSecondRep) {
                sign = -1;
                if (beamTestData) { sign = 1; pdg = 211; }
                secondRep = new genfit::RKTrackRep(sign * pdg);
                genfit::MeasuredStateOnPlane stateSecondRef(secondRep);
                mom.SetXYZ(0, 0, pTruth);
                pos.SetXYZ((*yCoord)[0], (*xCoord)[0], (*zCoord)[0]);
                for (int i = 0; i < 3; ++i) covM(i, i) = detectorResolution * detectorResolution * 100.;
                for (int i = 3; i < 6; ++i) covM(i, i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
                secondRep->setPosMomCov(stateSecondRef, pos, mom, covM);
                if (enableThirdRep) {
                    sign = -1;
                    pdg = 11;
                    thirdRep = new genfit::RKTrackRep(sign * pdg);
                    genfit::MeasuredStateOnPlane stateThirdRef(thirdRep);
                    mom.SetXYZ(0, 0, pTruth);
                    pos.SetXYZ((*yCoord)[0], (*xCoord)[0], (*zCoord)[0]);
                    for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
                    for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
                    thirdRep->setPosMomCov(stateThirdRef, pos, mom, covM);
                }
            }
                
            // create track
            TVectorD seedState(6);
            TMatrixDSym seedCov(6);
            rep->get6DStateCov(stateRef, seedState, seedCov);
            fitTrack = new genfit::Track(rep, seedState, seedCov); //initialized with smeared rep
            if (!beamTestData || enableSecondRep) {
                fitTrack->addTrackRep(secondRep);
                if (enableThirdRep) fitTrack->addTrackRep(thirdRep);
            }
            
            std::cout << "after fitTrack definition: " << std::endl;
            
            std::cout << "before performFitting: " << std::endl;
            
            bool fitResult = performFitting(*zCoord, *yCoord, *zErr, *yErr, *xCoord, *xErr, fitTrack, fitter);
            
            std::cout << "after performFitting: " << std::endl;
            
            cardRep = fitTrack->getCardinalRep();
            
            fitStatus = fitTrack->getFitStatus(cardRep);
            chi2 = fitStatus->getChi2() / fitStatus->getNdf();
            pvalue = fitStatus->getPVal();
            if (pvalue > 1.0E-33) chi2Sum += fitStatus->getChi2();

            hnmeas = yCoord->GetNrows();
            
            std::cout << "before residual calculation: " << std::endl;
            
            nTrackPoints = fitTrack->getNumPoints();
            
            std::cout << "nTrackPoints: " << nTrackPoints << std::endl;

            if (fitResult && fitStatus->isFitConverged()) {
                
                for (std::size_t i = 0; i < nTrackPoints; ++i) {
                    trackPoint = fitTrack->getPointWithMeasurementAndFitterInfo(i, cardRep);
                    rawMeas = trackPoint->getRawMeasurement();
                    std::unique_ptr<const genfit::AbsHMatrix> HitHMatrix(rawMeas->constructHMatrix(cardRep));
                    detId = rawMeas->getDetId();
                    fitterInfo = trackPoint->getKalmanFitterInfo();
                    residual = fitterInfo->getResidual();
                    refState = fitterInfo->getReferenceState();
                    scdState = residual.getState();
//                    scdState = rawMeas->getRawHitCoords() - HitHMatrix->Hv(refState->getState());
                    if (residualsY.size() < nTrackPoints) residualsY.push_back(std::make_unique<TH1D>(Form("h_%lu", i), Form("Residuals Y. Layer %lu", i), 500, -0.2, 0.2));
                    residualsY.at(i)->Fill(scdState[1]);
                }
                
                if (pvalue > 1.0E-33) N1GoodEvent++;
                
                fittedState = fitTrack->getFittedState();
                
                momReco = fittedState.getMomMag();
                momentumReco->Fill(momReco);
                momErr = (momReco - fabs(pTruth)) / fabs(pTruth);
                
                std::cout << "Reconstructed momentum: " << momReco << " GeV/c" << std::endl;
                std::cout << "Reconstructed momentum error: " << momErr << " GeV/c" << std::endl;
                
                momX = (fittedState.getMom()).X();
                momY = (fittedState.getMom()).Y();
                momZ = (fittedState.getMom()).Z();
                
                hPDG = fittedState.getPDG();
                
                #ifdef SHOW_TRACKS
                    display->addEvent(fitTrack);
                #endif
                
                delete fitTrack;
                fitTrack = nullptr;
            }
            
            for (std::size_t i{0}; i < nDetectors; ++i) {
                vcogsAligned[i]->clear();
                vcogsAligned[i]->shrink_to_fit();
            }
            if (enableStripY) {
                for (std::size_t i{0}; i < nDetectorsY; ++i) {
                    vcogsAlignedY[i]->clear();
                    vcogsAlignedY[i]->shrink_to_fit();
                }
            }
            
            std::cout << "after residual calculation: " << std::endl;

        }
        nTracks = 0;
    }
    
    std::unique_ptr<TCanvas> canvasMom = std::make_unique<TCanvas>();
    momentumReco->Draw();
    canvasMom->Modified();
    canvasMom->Update();
    
    chi2array.ResizeTo(1);
    chi2array[0] = chi2Sum / N1GoodEvent;
    chi2Sum = 0.;
    
    inFile->Close();
    
    std::cout << "Total number of events: " << nEntries << std::endl;
    std::cout << "Number of events with 1 cluster in every plane: " << N1event << std::endl;
    std::cout << "Number of events with 2 clusters in every plane: " << N2events << std::endl;
    std::cout << "Number of events with 3 clusters in every plane: " << N3events << std::endl;
    std::cout << "Number of events with 4 clusters in every plane: " << N4events << std::endl;
    std::cout << "Number of events with 5+ clusters in every plane: " << N5plusevents << std::endl;
    
    std::cout << "Efficiency, layer 0: " << N1event * 100.0 / Nnot0Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 1: " << N1event * 100.0 / Nnot1Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 2: " << N1event * 100.0 / Nnot2Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 3: " << N1event * 100.0 / Nnot3Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 4: " << N1event * 100.0 / Nnot4Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 5: " << N1event * 100.0 / Nnot5Layer << " %" << std::endl;
    
    double totalEff = (N1event * 1.0 / Nnot0Layer) * (N1event * 1.0 / Nnot1Layer) * (N1event * 1.0 / Nnot2Layer) * (N1event * 1.0 / Nnot3Layer) * (N1event * 1.0 / Nnot4Layer) * (N1event * 1.0 / Nnot5Layer);
    
    std::cout << "Tracking efficiency: " << totalEff * 100.0 << " %" << std::endl;
    
    #ifdef SHOW_TRACKS
        display->open();
    #endif
    
    myApp->Run();
    myApp->Delete();
    
    return 0;
}

