//
//  StripXFinitePlane.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 18.02.22.
//

#include "StripXFinitePlane.hpp"

namespace genfit {

StripXFinitePlane::StripXFinitePlane() {
    dimensions_.assign(2, 1);
}

StripXFinitePlane::StripXFinitePlane(double halfDimU, double halfDimV) {
    dimensions_.assign(2, 0);
    dimensions_.at(0) = halfDimU;
    dimensions_.at(1) = halfDimV;
}

StripXFinitePlane::StripXFinitePlane(const vector<double>& dim) {
    if (dim.size() > 2) dimensions_.assign(2, 1);
    else dimensions_ = dim;
}

StripXFinitePlane::StripXFinitePlane(const StripXFinitePlane& otherPlane) {
    dimensions_ = otherPlane.dimensions_;
}

StripXFinitePlane& StripXFinitePlane::operator=(const StripXFinitePlane& rhsPlane) {
    if (this == &rhsPlane) return *this;
    std::copy(rhsPlane.dimensions_.begin(),rhsPlane.dimensions_.end(),dimensions_.begin());
    return *this;
}

StripXFinitePlane::~StripXFinitePlane() {
    dimensions_.clear();
    dimensions_.shrink_to_fit();
}

StripXFinitePlane* StripXFinitePlane::clone() const {
    return new StripXFinitePlane(*this);
}

bool StripXFinitePlane::isInActive(double u, double v) const {
    if ((u <= dimensions_.at(0) && u >= -dimensions_.at(0)) && (v <= dimensions_.at(1) && v >= -dimensions_.at(1))) return true;
    else return false;
}

void StripXFinitePlane::Print(const Option_t*) const {
    std::cout<<"StripXFinitePlane: "
         <<"U half-size: "<<dimensions_.at(0)<<", V half-size: "<<dimensions_.at(1)<<";"<<std::endl;
}

}
