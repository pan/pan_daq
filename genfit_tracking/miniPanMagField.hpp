//
//  miniPanMagField.hpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 17.01.22.
//

#ifndef miniPanMagField_hpp
#define miniPanMagField_hpp

#include <fstream>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "AbsBField.h"

namespace genfit {

/** @brief Class that defines a (3d) magnetic field map (distances in cm, fields in kGauss)
 *
 *  @author Daniil Sukhonos (UNIGE) forked and modified from FairShip (https://github.com/ShipSoft/FairShip.git)
 *
 */

class PanBFieldMap : public AbsBField {
    
    typedef std::vector< std::vector<Float_t> > floatArray;
    
private:
    floatArray* fieldMap_; // in kGauss
    //! The name of the map file
    std::string mapFileName_;
    
    //! Initialisation boolean
    Bool_t initialised_;
    
    std::string label_;

    //! Flag to specify if we are a copy of the field map (just a change of global offsets)
    Bool_t isCopy_;

    //! The number of bins along x
    Int_t Nx_;
    
    //! The number of bins along y
    Int_t Ny_;

    //! The number of bins along z
    Int_t Nz_;

    //! The total number of bins
    Int_t N_;

    //! The minimum value of x for the map
    Float_t xMin_;

    //! The maximum value of x for the map
    Float_t xMax_;

    //! The bin width along x
    Float_t dx_;

    //! The co-ordinate range along x
    Float_t xRange_;

    //! The minimum value of y for the map
    Float_t yMin_;

    //! The maximum value of y for the map
    Float_t yMax_;

    //! The bin width along y
    Float_t dy_;

    //! The co-ordinate range along y
    Float_t yRange_;

    //! The minimum value of z for the map
    Float_t zMin_;

    //! The maximum value of z for the map
    Float_t zMax_;

    //! The bin width along z
    Float_t dz_;

    //! The co-ordinate range along z
    Float_t zRange_;
    //! Bin A for the trilinear interpolation
    Int_t binA_;

    //! Bin B for the trilinear interpolation
    Int_t binB_;

    //! Bin C for the trilinear interpolation
    Int_t binC_;

    //! Bin D for the trilinear interpolation
    Int_t binD_;

    //! Bin E for the trilinear interpolation
    Int_t binE_;

    //! Bin F for the trilinear interpolation
    Int_t binF_;

    //! Bin G for the trilinear interpolation
    Int_t binG_;

    //! Bin H for the trilinear interpolation
    Int_t binH_;

    //! Fractional bin distance along x
    Float_t xFrac_;

    //! Fractional bin distance along y
    Float_t yFrac_;

    //! Fractional bin distance along z
    Float_t zFrac_;

    //! Complimentary fractional bin distance along x
    Float_t xFrac1_;

    //! Complimentary fractional bin distance along y
    Float_t yFrac1_;

    //! Complimentary fractional bin distance along z
    Float_t zFrac1_;
    //! Double converting Tesla to kiloGauss (for VMC/FairRoot B field units)
    Float_t Tesla_;
    //! Converting mm to cm (for Genfit)
    Float_t cm_;
protected:
    PanBFieldMap(const PanBFieldMap&);
    PanBFieldMap& operator=(const PanBFieldMap&) = delete;
    //! Enumeration to specify the co-ordinate type
    enum CoordAxis {xAxis = 0, yAxis, zAxis};

    //! Initialisation
    void initialise();

    //! Read the field map data and store the information internally
    void readMapFile();

    //! Process the ROOT file containing the field map data
    void readRootFile();

    //! Process the text file containing the field map data
    void readTextFile();

    // ! Set the coordinate limits from information stored in the datafile
    void setLimits();

    //! Check to see if a point is within the map validity range
    /*!
      \param [in] x The x co-ordinate of the point (cm)
      \param [in] y The y co-ordinate of the point (cm)
      \param [in] z The z co-ordinate of the point (cm)
      \returns true/false if the point is inside the field map range
    */
    Bool_t insideRange(Float_t x, Float_t y, Float_t z);

    //! Typedef for an int-double pair
    typedef std::pair<Int_t, Float_t> binPair;

    //! Get the bin number and fractional distance from the leftmost bin edge
    /*!
      \param [in] x The co-ordinate component of the point (cm)
      \param [in] theAxis The co-ordinate axis (CoordAxis enumeration for x, y or z)
      \returns the bin number and fractional distance from the leftmost bin edge as a pair
    */
    binPair getBinInfo(Float_t x, CoordAxis theAxis);

    //! Find the vector entry of the field map data given the bins iX, iY and iZ
    /*!
      \param [in] iX The bin along the x axis
      \param [in] iY The bin along the y axis
      \param [in] iZ The bin along the z axis
      \returns the index entry for the field map data vector
    */
    Int_t getMapBin(Int_t iX, Int_t iY, Int_t iZ);

    //! Calculate the magnetic field component using trilinear interpolation.
    //! This function uses the various "binX" integers and "uFrac" variables
    /*!
      \param [in] theAxis The co-ordinate axis (CoordAxis enumeration for x, y or z)
      \returns the magnetic field component for the given axis
    */
    Float_t BInterCalc(CoordAxis theAxis);
public:
    
    PanBFieldMap(const std::string& label, const std::string& mapFileName);
    virtual ~PanBFieldMap();
    
    //! Retrieve the field map
    /*!
      \returns the field map
    */
    floatArray* getFieldMap() const {return fieldMap_;}

    //! Get the name of the map file
    /*!
      \returns the name of the map file as an STL string
    */
    std::string GetMapFileName() const {return mapFileName_;}
    
    std::string GetName() const {return label_;}

    //! Get the number of bins along x
    /*!
      \returns the number of bins along x
    */
    Int_t GetNx() const {return Nx_;}

    //! Get the number of bins along y
    /*!
      \returns the number of bins along y
    */
    Int_t GetNy() const {return Ny_;}

    //! Get the number of bins along z
    /*!
      \returns the number of bins along z
    */
    Int_t GetNz() const {return Nz_;}

    //! Get the total numer of bins
    /*!
      \returns the total number of bins
    */
    Int_t GetNBins() const {return N_;}

    //! Get the minimum value of x for the map
    /*!
      \returns the minimum x co-ordinate (cm)
    */
    Float_t GetXMin() const {return xMin_;}

    //! Get the maximum value of x for the map
    /*!
      \returns the maximum x co-ordinate (cm)
    */
    Float_t GetXMax() const {return xMax_;}

    //! Get the bin width along x for the map
    /*!
      \returns the bin width along x (cm)
    */
    Float_t GetdX() const {return dx_;}

     //! Get the x co-ordinate range for the map
    /*!
      \returns the x co-ordinate range (cm)
    */
    Float_t GetXRange() const {return xRange_;}

   //! Get the minimum value of y for the map
    /*!
      \returns the minimum y co-ordinate (cm)
    */
    Float_t GetYMin() const {return yMin_;}

    //! Get the maximum value of y for the map
    /*!
      \returns the maximum y co-ordinate (cm)
    */
    Float_t GetYMax() const {return yMax_;}

    //! Get the bin width along y for the map
    /*!
      \returns the bin width along y (cm)
    */
    Float_t GetdY() const {return dy_;}

     //! Get the y co-ordinate range for the map
    /*!
      \returns the y co-ordinate range (cm)
    */
    Float_t GetYRange() const {return yRange_;}

    //! Get the minimum value of z for the map
    /*!
      \returns the minimum z co-ordinate (cm)
    */
    Float_t GetZMin() const {return zMin_;}

    //! Get the maximum value of z for the map
    /*!
      \returns the maximum z co-ordinate (cm)
    */
    Float_t GetZMax() const {return zMax_;}

    //! Get the bin width along z for the map
    /*!
      \returns the bin width along z (cm)
    */
    Float_t GetdZ() const {return dz_;}

     //! Get the z co-ordinate range for the map
    /*!
      \returns the z co-ordinate range (cm)
    */
    Float_t GetZRange() const {return zRange_;}

    //! Get the boolean flag to specify if we are a "copy"
    /*!
      \returns the boolean copy flag status
    */
    Bool_t IsACopy() const {return isCopy_;}
    
    TVector3 Field(const TVector3& pos);
    
    //! return value at position
    TVector3 get(const TVector3& pos) const;
    void get(const double& posX, const double& posY, const double& posZ, double& Bx, double& By, double& Bz) const;
    
};

} /* End of namespace genfit */

#endif /* miniPanMagField_hpp */
