//
//  miniPanMagField.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 17.01.22.
//

#include "miniPanMagField.hpp"

namespace genfit {

PanBFieldMap::PanBFieldMap(const std::string& label, const std::string& mapFileName) :
    fieldMap_(new floatArray()),
    label_(label),
    mapFileName_(mapFileName),
    initialised_(kFALSE),
    isCopy_(kFALSE),
    Nx_(0), Ny_(0), Nz_(0), N_(0),
    xMin_(0.0), xMax_(0.0),
    dx_(0.0), xRange_(0.0),
    yMin_(0.0), yMax_(0.0),
    dy_(0.0), yRange_(0.0),
    zMin_(0.0), zMax_(0.0),
    dz_(0.0), zRange_(0.0),
    Tesla_(10.0),
    cm_(10.0)
{
    this->initialise();
}

PanBFieldMap::~PanBFieldMap()
{
    // Delete the internal vector storing the field map values
    if (fieldMap_ && isCopy_ == kFALSE) {
        delete fieldMap_; fieldMap_ = 0;
    }

}

PanBFieldMap::PanBFieldMap(const PanBFieldMap& oldMap) :
    fieldMap_(oldMap.fieldMap_),
    label_(oldMap.label_),
    mapFileName_(oldMap.mapFileName_),
    initialised_(kFALSE),
    isCopy_(kTRUE),
    Nx_(oldMap.Nx_), Ny_(oldMap.Ny_), Nz_(oldMap.Nz_), N_(oldMap.N_),
    xMin_(oldMap.xMin_), xMax_(oldMap.xMax_),
    dx_(oldMap.dx_), xRange_(oldMap.xRange_),
    yMin_(oldMap.yMin_), yMax_(oldMap.yMax_),
    dy_(oldMap.dy_), yRange_(oldMap.yRange_),
    zMin_(oldMap.zMin_), zMax_(oldMap.zMax_),
    dz_(oldMap.dz_), zRange_(oldMap.zRange_),
    Tesla_(oldMap.Tesla_),
    cm_(oldMap.cm_)
{
        this->initialise();
}

TVector3 PanBFieldMap::Field(const TVector3& pos) {
    TVector3 BField;
    // Set the B field components given the global position co-ordinates

    // Convert the global position into a local one for the volume field.
    // Initialise the local co-ords, which will get overwritten if the
    // co-ordinate transformation exists. For a global field, any local
    // volume transformation is ignored
    TVector3 localCoords = pos;

    // The local position co-ordinates
    Float_t x = localCoords.X();
    Float_t y = localCoords.Y();
    Float_t z = localCoords.Z();

    Float_t BySign(1.0);
    Float_t BxSign(1.0);

    // First check to see if we are inside the field map range
    Bool_t inside = this->insideRange(x, y, z);
    if (inside == kFALSE) {return BField;}

    // Find the neighbouring bins for the given point
    binPair xBinInfo = this->getBinInfo(x, PanBFieldMap::xAxis);
    binPair yBinInfo = this->getBinInfo(y, PanBFieldMap::yAxis);
    binPair zBinInfo = this->getBinInfo(z, PanBFieldMap::zAxis);

    Int_t iX = xBinInfo.first;
    Int_t iY = yBinInfo.first;
    Int_t iZ = zBinInfo.first;

    // Check that the bins are valid
    if (iX == -1 || iY == -1 || iZ == -1) {return BField;}

    // Get the various neighbouring bin entries
    Int_t iX1(iX + 1);
    Int_t iY1(iY + 1);
    Int_t iZ1(iZ + 1);

    binA_ = this->getMapBin(iX, iY, iZ);
    binB_ = this->getMapBin(iX1, iY, iZ);
    binC_ = this->getMapBin(iX, iY1, iZ);
    binD_ = this->getMapBin(iX1, iY1, iZ);
    binE_ = this->getMapBin(iX, iY, iZ1);
    binF_ = this->getMapBin(iX1, iY, iZ1);
    binG_ = this->getMapBin(iX, iY1, iZ1);
    binH_ = this->getMapBin(iX1, iY1, iZ1);

    // Retrieve the fractional bin distances
    xFrac_ = xBinInfo.second;
    yFrac_ = yBinInfo.second;
    zFrac_ = zBinInfo.second;

    // Set the complimentary fractional bin distances
    xFrac1_ = 1.0 - xFrac_;
    yFrac1_ = 1.0 - yFrac_;
    zFrac1_ = 1.0 - zFrac_;

    // Finally get the magnetic field components using trilinear interpolation
    // and scale with the appropriate multiplication factor for By (default = -1.0)
    BField.SetY(this->BInterCalc(PanBFieldMap::xAxis)*BxSign);
    BField.SetX(this->BInterCalc(PanBFieldMap::yAxis)*BySign); // !!!! swap axes of magnetic field, because StripX is measuring Y coordinate in the geometry !!!!
    BField.SetZ(this->BInterCalc(PanBFieldMap::zAxis));
    
    return BField;
}

TVector3 PanBFieldMap::get(const TVector3& pos) const {
    PanBFieldMap mapCopy(*this);
    return mapCopy.Field(pos);
}

void PanBFieldMap::get(const double& xPos, const double& yPos, const double& zPos, double& Bx, double& By, double& Bz) const {

    // Set the B field components given the global position co-ordinates

    // Convert the global position into a local one for the volume field.
    // Initialise the local co-ords, which will get overwritten if the
    // co-ordinate transformation exists. For a global field, any local
    // volume transformation is ignored
    TVector3 localCoords(xPos, yPos, zPos);
    PanBFieldMap mapCopy(*this);
    TVector3 BField = mapCopy.Field(localCoords);
    Bx = BField.X();
    By = BField.Y();
    Bz = BField.Z();

}


void PanBFieldMap::initialise()
{
    
    if (initialised_ == kFALSE) {
    
    if (isCopy_ == kFALSE) {this->readMapFile();}

    initialised_ = kTRUE;

    }

}

void PanBFieldMap::readMapFile()
{

    std::cout<<"PanBFieldMap::readMapFile() creating field "<<this->GetName()
         <<" using file "<<mapFileName_<<std::endl;

    // Check to see if we have a ROOT file
    if (mapFileName_.find(".root") != std::string::npos) {

        this->readRootFile();

    } else {

        this->readTextFile();

    }

}

void PanBFieldMap::readRootFile() {

    TFile* theFile = TFile::Open(mapFileName_.c_str());

    if (!theFile) {
        std::cout<<"PanBFieldMap: could not find the file "<<mapFileName_<<std::endl;
        return;
    }
    
    // Coordinate ranges
    TTree* rTree = dynamic_cast<TTree*>(theFile->Get("Range"));
    if (!rTree) {
        std::cout<<"PanBFieldMap: could not find Range tree in "<<mapFileName_<<std::endl;
        return;
    }

    rTree->SetBranchAddress("xMin", &xMin_);
    rTree->SetBranchAddress("xMax", &xMax_);
    rTree->SetBranchAddress("dx", &dx_);
    rTree->SetBranchAddress("yMin", &yMin_);
    rTree->SetBranchAddress("yMax", &yMax_);
    rTree->SetBranchAddress("dy", &dy_);
    rTree->SetBranchAddress("zMin", &zMin_);
    rTree->SetBranchAddress("zMax", &zMax_);
    rTree->SetBranchAddress("dz", &dz_);

    // Fill the ranges
    rTree->GetEntry(0);

    this->setLimits();

    // Make sure we don't have a copy
    if (isCopy_ == kFALSE) {

        // The data is expected to contain Bx,By,Bz data values
        // in ascending z,y,x co-ordinate order

        TTree* dTree = dynamic_cast<TTree*>(theFile->Get("Data"));
        if (!dTree) {
            std::cout<<"PanBFieldMap: could not find Data tree in "<<mapFileName_<<std::endl;
            return;
        }

        Float_t Bx, By, Bz;
        // Only enable the field components
        dTree->SetBranchStatus("*", 0);
        dTree->SetBranchStatus("Bx", 1);
        dTree->SetBranchStatus("By", 1);
        dTree->SetBranchStatus("Bz", 1);

        dTree->SetBranchAddress("Bx", &Bx);
        dTree->SetBranchAddress("By", &By);
        dTree->SetBranchAddress("Bz", &Bz);

        Int_t nEntries = dTree->GetEntries();
        if (nEntries != N_) {
            std::cout<<"Expected "<<N_<<" field map entries but found "<<nEntries<<std::endl;
            nEntries = 0;
        }

        fieldMap_->reserve(nEntries);

        for (Int_t i = 0; i < nEntries; i++) {

            dTree->GetEntry(i);

            // B field values are in Tesla. This means these values are multiplied
            // by a factor of ten since both FairRoot and the VMC interface use kGauss
            Bx *= Tesla_;
            By *= Tesla_;
            Bz *= Tesla_;

            // Store the B field 3-vector
            std::vector<Float_t> BVector(3);
            BVector[0] = Bx; BVector[1] = By; BVector[2] = Bz;
            fieldMap_->push_back(BVector);

        }

    }

    theFile->Close();

}

void PanBFieldMap::readTextFile() {

    std::ifstream getData(mapFileName_.c_str());

    std::string tmpString("");

    getData >> zMax_ >> yMax_ >> xMax_ >> dz_;
    dy_ = dz_ / cm_;
    dx_ = dz_ / cm_;
    dz_ /= cm_;
    // convert from mm to cm
    zMin_ = (zMax_ - 1) * (-1) / cm_;
    yMin_ = (yMax_ - 1) * (-1) / cm_;
    xMin_ = (xMax_ - 1) * (-1) / cm_;
    zMax_ = (zMax_ - 1) / cm_;
    yMax_ = (yMax_ - 1) / cm_;
    xMax_ = (xMax_ - 1) / cm_;
    
    // skip 7 lines
    int NLinesSkipped{7};
    
    for (int i = 0; i < NLinesSkipped; ++i) {
        getData >> tmpString >> tmpString >> tmpString;
    }

    this->setLimits();

    // Check to see if this object is a "copy"
    if (isCopy_ == kFALSE) {

        // Read the field map and store the magnetic field components

        // Second line can be ignored since they are
        // just labels for the data columns for readability
        getData >> tmpString;
        
        // The remaining lines contain Bx,By,Bz data values
        // in ascending z,y,x co-ord order
        fieldMap_->reserve(N_);

        Float_t Bx(0.0), By(0.0), Bz(0.0);

        for (Int_t i = 0; i < N_; i++) {
            
            getData >> tmpString >> tmpString >> tmpString >> Bx >> By >> Bz >> tmpString;

            // B field values are in Tesla. This means these values are multiplied
            // by a factor of ten since both FairRoot and the VMC interface use kGauss
            Bx *= Tesla_;
            By *= Tesla_;
            Bz *= Tesla_;

            // Store the B field 3-vector
            std::vector<Float_t> BVector(3);
            BVector[0] = Bx; BVector[1] = By; BVector[2] = Bz;
            fieldMap_->push_back(BVector);
            
        }

    }

    getData.close();

}

Bool_t PanBFieldMap::insideRange(Float_t x, Float_t y, Float_t z)
{

    Bool_t inside(kFALSE);

    if (x >= xMin_ && x <= xMax_ && y >= yMin_ &&
    y <= yMax_ && z >= zMin_ && z <= zMax_) {inside = kTRUE;}
    
    return inside;

}

void PanBFieldMap::setLimits() {

    // Since the default SHIP distance unit is cm, we do not need to convert
    // these map limits, i.e. cm = 1 already

    xRange_ = xMax_ - xMin_;
    yRange_ = yMax_ - yMin_;
    zRange_ = zMax_ - zMin_;

    // Find the number of bins using the limits and bin sizes. The number of bins
    // includes both the minimum and maximum values. To ensure correct rounding
    // up to the nearest integer we need to add 1.5 not 1.0.
    if (dx_ > 0.0) {
    Nx_ = static_cast<Int_t>(((xMax_ - xMin_)/dx_) + 1.5);
    }
    if (dy_ > 0.0) {
    Ny_ = static_cast<Int_t>(((yMax_ - yMin_)/dy_) + 1.5);
    }
    if (dz_ > 0.0) {
    Nz_ = static_cast<Int_t>(((zMax_ - zMin_)/dz_) + 1.5);
    }

    N_ = Nx_*Ny_*Nz_;

    std::cout<<"x limits: "<<xMin_<<", "<<xMax_<<", dx = "<<dx_<<std::endl;
    std::cout<<"y limits: "<<yMin_<<", "<<yMax_<<", dy = "<<dy_<<std::endl;
    std::cout<<"z limits: "<<zMin_<<", "<<zMax_<<", dz = "<<dz_<<std::endl;

    std::cout<<"Total number of bins = "<<N_
         <<"; Nx = "<<Nx_<<", Ny = "<<Ny_<<", Nz = "<<Nz_<<std::endl;

}

PanBFieldMap::binPair PanBFieldMap::getBinInfo(Float_t u, PanBFieldMap::CoordAxis theAxis)
{

    Float_t du(0.0), uMin(0.0), Nu(0);

    if (theAxis == PanBFieldMap::xAxis) {
        du = dx_; uMin = xMin_; Nu = Nx_;
    } else if (theAxis == PanBFieldMap::yAxis) {
        du = dy_; uMin = yMin_; Nu = Ny_;
    } else if (theAxis == PanBFieldMap::zAxis) {
        du = dz_; uMin = zMin_; Nu = Nz_;
    }

    Int_t iBin(-1);
    Float_t fracL(0.0);

    if (du > 1e-10) {

        // Get the number of fractional bin widths the point is from the first volume bin
        Float_t dist = (u - uMin)/du;
        // Get the integer equivalent of this distance, which is the bin number
        iBin = static_cast<Int_t>(dist);
        // Get the actual fractional distance of the point from the leftmost bin edge
        fracL = (dist - iBin*1.0);

    }

    // Check that the bin number is valid
    if (iBin < 0 || iBin >= Nu) {
        iBin = -1; fracL = 0.0;
    }

    // Return the bin number and fractional distance of the point from the leftmost bin edge
    binPair binInfo(iBin, fracL);

    return binInfo;

}

Int_t PanBFieldMap::getMapBin(Int_t iX, Int_t iY, Int_t iZ)
{

    // Get the index of the map entry corresponding to the x,y,z bins.
    // Remember that the map is ordered in ascending z, y, then x

    Int_t index = (iX*Ny_ + iY)*Nz_ + iZ;
    if (index < 0) {
        index = 0;
    } else if (index >= N_) {
        index = N_-1;
    }

    return index;

}

Float_t PanBFieldMap::BInterCalc(PanBFieldMap::CoordAxis theAxis)
{

    // Find the magnetic field component along theAxis using trilinear
    // interpolation based on the current position and neighbouring bins
    Float_t result(0.0);

    Int_t iAxis(0);

    if (theAxis == PanBFieldMap::yAxis) {
        iAxis = 1;
    } else if (theAxis == PanBFieldMap::zAxis) {
        iAxis = 2;
    }

    if (fieldMap_) {

        // Get the field component values for the neighbouring bins
        Float_t A = (*fieldMap_)[binA_][iAxis];
        Float_t B = (*fieldMap_)[binB_][iAxis];
        Float_t C = (*fieldMap_)[binC_][iAxis];
        Float_t D = (*fieldMap_)[binD_][iAxis];
        Float_t E = (*fieldMap_)[binE_][iAxis];
        Float_t F = (*fieldMap_)[binF_][iAxis];
        Float_t G = (*fieldMap_)[binG_][iAxis];
        Float_t H = (*fieldMap_)[binH_][iAxis];

        // Perform linear interpolation along x
        Float_t F00 = A*xFrac1_ + B*xFrac_;
        Float_t F10 = C*xFrac1_ + D*xFrac_;
        Float_t F01 = E*xFrac1_ + F*xFrac_;
        Float_t F11 = G*xFrac1_ + H*xFrac_;

        // Linear interpolation along y
        Float_t F0 = F00*yFrac1_ + F10*yFrac_;
        Float_t F1 = F01*yFrac1_ + F11*yFrac_;

        // Linear interpolation along z
        result = F0*zFrac1_ + F1*zFrac_;

    }

    return result;

}

} /* End of namespace genfit */
