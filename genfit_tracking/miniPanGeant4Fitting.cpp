//
//  miniPanGeant4Fitting.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 29.03.22.
//

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TVectorD.h"
#include "TFitResult.h"
#include "TEveManager.h"
#include "TGeoManager.h"
#include "TVector3.h"
#include "TDatabasePDG.h"
#include "TMath.h"

#include <iostream>
#include <numeric>
#include <string>
#include <algorithm>
#include <cmath>
#include <utility>
#include <vector>
#include <array>
#include <unordered_map>

#include "miniPanMagField.hpp"
#include "ConstField.h"
#include "Exception.h"
#include "FieldManager.h"
#include "KalmanFitterRefTrack.h"
#include "KalmanFitter.h"
#include "AbsKalmanFitter.h"
#include "KalmanFitterInfo.h"
#include "FitStatus.h"
#include "MeasuredStateOnPlane.h"
#include "StateOnPlane.h"
#include "SharedPlanePtr.h"
#include "Track.h"
#include "TrackPoint.h"
#include "DetPlane.h"

#include "MaterialEffects.h"
#include "AbsTrackRep.h"
#include "RKTrackRep.h"
#include "TGeoMaterialInterface.h"

#include "EventDisplay.h"

#include "AbsMeasurement.h"
#include "PlanarMeasurement.h"
#include "MeasurementOnPlane.h"

typedef unsigned int uint;
typedef vector<double> vdouble;
typedef vector<int> vint;
typedef vector<string> vstring;

constexpr std::size_t NLAD{2};
constexpr std::size_t NBOARD{3};
constexpr std::size_t NBRANCH{4};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double mmToCm{0.1}; // cm / mm
constexpr float detectorResolution(0.0025); // resolution of a StripX detector in cm
constexpr float stripLength(5.1); // scd strip length in cm
constexpr float particleMass{938.272}; // in MeV/c^2

vdouble detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81
vdouble detectorErr;
std::array<vdouble, NLAD * NBOARD> deltas;

struct hitsColl {
    TVectorD xCoordColl;
    TVectorD yCoordColl;
    TVectorD zCoordColl;
    std::vector<std::size_t> xCoordCollSize;
    std::vector<std::size_t> yCoordCollSize;
    std::vector<std::size_t> zCoordCollSize;
};

std::unordered_map<std::string, int> volumes = {
    {"SiliconStripX0", 0},
    {"SiliconStripX1", 1},
    {"SiliconStripX2", 2},
    {"SiliconStripX3", 3},
    {"SiliconStripX4", 4},
    {"SiliconStripX5", 5}
};

bool performFitting(const TVectorD &zCoord, const TVectorD &yCoord, const TVectorD &xCoord, const TVectorD &zErr, const TVectorD &yErr, const TVectorD &xErr, genfit::Track* &fitTrack, genfit::KalmanFitterRefTrack* &fitter) {
    
    int detId(0); // detector ID
    int planeId(0);
    int hitId(0); // hit ID
    
    genfit::PlanarMeasurement* measurement = nullptr;
    
    TMatrixDSym hitCov(2);
    hitCov.UnitMatrix();
    hitCov(1,1) = detectorResolution * detectorResolution / 12;
    hitCov(0,0) = stripLength * stripLength / 12;

    TVectorD hitCoords(2);
    hitCoords[0] = 0;
    hitCoords[1] = 0;
    
    TVector3 detPlaneN, detPlaneU, detPlaneV, detPlane0;
    genfit::AbsTrackRep* cardRep;
    genfit::FitStatus* fitStatus = nullptr;
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        detPlane0.SetXYZ(0, 0, zCoord[i]);
        // hitCoords[0] = xCoord[i];
        hitCoords[1] = yCoord[i];
        // hitCov(0,0) = xErr[i] * xErr[i];
        hitCov(1,1) = yErr[i] * yErr[i];
        detPlaneN.SetXYZ(0,0,1);
        detPlaneN.SetMag(1);
        detPlaneU = detPlaneN.Orthogonal();
        detPlaneU.SetMag(1);
        detPlaneV = detPlaneN.Cross(detPlaneU);
        detPlaneV.SetMag(1);
        detPlaneU.SetXYZ(fabs(detPlaneU.X()), fabs(detPlaneU.Y()), fabs(detPlaneU.Z()));
        detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
        measurement = new genfit::PlanarMeasurement(hitCoords, hitCov, detId, ++hitId, nullptr);
        measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV)), ++planeId);
        fitTrack->insertPoint(new genfit::TrackPoint(measurement->clone(), fitTrack));
        delete measurement;
        measurement = nullptr;
    }
    
    if (fitTrack->getNumPoints() == 0) return false;
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track before fit, not consistent" << std::endl;
        std::cerr << "Exception, next event" << std::endl;
        std::cerr << e.what();
        return false;
    }
    // do the fit
    try {
        fitter->processTrack(fitTrack);
    }
    catch (genfit::Exception& e) {
        std::cout << "genfit failed to fit track" << std::endl;
        std::cerr << "Exception, next event" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track after fit, not consistent" << std::endl;
        std::cerr << "Exception, next event" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->determineCardinalRep();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with cardinal rep determination after fit" << std::endl;
        std::cerr << "Exception, next event" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->getFittedState();
        // print fit result
        // (fitTrack->getFittedState()).Print();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with fittedstate" << std:: endl;
        std::cerr << "Exception, next event" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    cardRep = fitTrack->getCardinalRep();
    
    try {
        fitStatus = fitTrack->getFitStatus(cardRep);
        std::cout << "N Failed Points: " << fitStatus->getNFailedPoints() << std::endl;
        if (!fitStatus->isFitted()) return false;
        auto nmeas = fitStatus->getNdf();
        if (nmeas < 1) return false;
        auto chi2Value  = fitStatus->getChi2()/nmeas;
        std::cout << "N measurements: " << nmeas << std::endl;
        std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
        if (chi2Value < 0) return false;
        if (!fitStatus->isFitConverged()) {
            try {
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->determineCardinalRep();
                fitTrack->getFittedState();
                cardRep = fitTrack->getCardinalRep();
                fitStatus = fitTrack->getFitStatus(cardRep);
                nmeas = fitStatus->getNdf();
                if (nmeas < 1) return false;
                chi2Value  = fitStatus->getChi2()/nmeas;
                std::cout << "N measurements: " << nmeas << std::endl;
                std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
                if (chi2Value < 0) return false;
            } catch (genfit::Exception& e) {
                std::cerr << "Exception" << std::endl;
                std::cerr << e.what();
                throw e;
            }
        }
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with Fit Status" << std::endl;
        std::cerr << "Exception, next event" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    delete measurement;
    measurement = nullptr;
    
    return true;
}

void TracksFitting(std::string inputFile = "PAN_GEANT_pion10GeVc_NoMag_20220324.root", std::string outputFile = "tracking_histos_minipan_geant4.root", std::string geomFile = "MiniPANGeom.root", bool enableMagField = false) {
    
    gStyle->SetOptFit(1);
    
    detectorErr.assign(NLAD * NBOARD, detectorResolution / sqrt(12));
    
    // primary tree branches
    Int_t pdgId;
    Double_t kinEnergy, X0, Y0, Z0, Px, Py, Pz, P;
    // hits tree branches
    Char_t volName[14];
    Int_t EventID, TrackID, StripID, StepStatus;
    Double_t PosX, PosY, PosZ;
    
    const genfit::eFitterType fitterId = genfit::RefKalman;
    const int nIter = 20; // max number of iterations
    const double dPVal = 1.E-3; // convergence criterion
    
    genfit::KalmanFitterRefTrack* fitter = 0;
    fitter = new genfit::KalmanFitterRefTrack(nIter, dPVal);
    fitter->setMaxIterations(nIter);
    //fitter->useSquareRootFormalism();
    
    auto geoMan = new TGeoManager("Geometry", "PixPAN geometry");
    geoMan->Import(geomFile.c_str());
    
    if (enableMagField) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("n48","2-magnets-30mm-N48.txt"));
    else genfit::FieldManager::getInstance()->init(new genfit::ConstField(0, 0, 0)); // in kGauss 1 T = 10 kG
    genfit::FieldManager::getInstance()->useCache(true, 8);
    
    genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
    
    genfit::EventDisplay* display = genfit::EventDisplay::getInstance();
    display->reset();
    
    TGraphErrors *gr;
    
    TFile *inFile = TFile::Open(inputFile.c_str(), "read");
    TTree *primariesTree = (TTree *) inFile->Get("PrimaryParticle");
    TTree *hitsTree = (TTree *) inFile->Get("HitsCol");
    
    std::vector<std::array<TBranch *, NBRANCH>> branches;
    std::array<TBranch *, 4> temp_arr;
    branches.assign(NLAD * NBOARD, temp_arr);
    
    const auto &nEntries = primariesTree->GetEntries();
    
    primariesTree->SetBranchAddress("pdgId", &pdgId);
    primariesTree->SetBranchAddress("kinEnergy", &kinEnergy);
    primariesTree->SetBranchAddress("X0", &X0);
    primariesTree->SetBranchAddress("Y0", &Y0);
    primariesTree->SetBranchAddress("Z0", &Z0);
    primariesTree->SetBranchAddress("Px", &Px);
    primariesTree->SetBranchAddress("Py", &Py);
    primariesTree->SetBranchAddress("Pz", &Pz);
    primariesTree->SetBranchAddress("P", &P);
    
    hitsTree->SetBranchAddress("EventID", &EventID);
    hitsTree->SetBranchAddress("TrackID", &TrackID);
    hitsTree->SetBranchAddress("VolName", &volName);
    hitsTree->SetBranchAddress("StripID", &StripID);
    hitsTree->SetBranchAddress("StepStatus", &StepStatus);
    hitsTree->SetBranchAddress("PosX", &PosX);
    hitsTree->SetBranchAddress("PosY", &PosY);
    hitsTree->SetBranchAddress("PosZ", &PosZ);

    //*
    TFile *outFile = TFile::Open(outputFile.c_str(), "recreate");
    TTree *tracksTree = new TTree("tracks_tree", "Fitted tracks");
    //*/
    float chi2, pvalue, rmse_x, rmse_y, rmse_z, momReco, momX, momY, momZ, momErr, energyErr;
    float xInitDir, yInitDir, zInitDir, momInit, p0, p1;
    int hnmeas, hPDG, hConvTracks;
    uint conversionDepth;
    std::vector<float> xErrors, yErrors, zErrors;
    TMatrixDSym covMatrix, correlMatrix;
    //*
    auto chi2br = tracksTree->Branch("chi2br", &chi2, "chi2/F");
    auto pvaluebr = tracksTree->Branch("pvaluebr", &pvalue, "pvalue/F");
    // auto p0br = tracksTree->Branch("p0br", &p0, "p0/F");
    // auto p1br = tracksTree->Branch("p1br", &p1, "p1/F");
    auto covMatrixbr = tracksTree->Branch("covMatrixbr", &covMatrix);
    // auto corelMatrixbr = tracksTree->Branch("correlMatrixbr", &correlMatrix);
    auto rmse_ybr = tracksTree->Branch("rmse_ybr", &rmse_y, "rmse_y/F");
    auto rmse_xbr = tracksTree->Branch("rmse_xbr", &rmse_x, "rmse_x/F");
    //auto xdirbr = tracksTree->Branch("xdirbr", &xdir, "xdir/F");
    //auto ydirbr = tracksTree->Branch("ydirbr", &ydir, "ydir/F");
    //auto zdirbr = tracksTree->Branch("zdirbr", &zdir, "zdir/F");
    auto hnmeasbr = tracksTree->Branch("hnmeasbr", &hnmeas, "hnmeas/I");
    auto yerrbr = tracksTree->Branch("yerrbr", &yErrors);
    auto xerrbr = tracksTree->Branch("xerrbr", &xErrors);
    auto momRecobr = tracksTree->Branch("momRecobr", &momReco, "momReco/F");
    auto momXbr = tracksTree->Branch("momXbr", &momX, "momX/F");
    auto momYbr = tracksTree->Branch("momYbr", &momY, "momY/F");
    auto momZbr = tracksTree->Branch("momZbr", &momZ, "momZ/F");
    auto momErrbr = tracksTree->Branch("momErrbr", &momErr, "momErr/F");
    auto energyErrbr = tracksTree->Branch("energyErrbr", &energyErr, "energyErr/F");
    auto hPDGbr = tracksTree->Branch("hPDGbr", &hPDG, "hPDG/I");
    // auto hConvTracksbr = tracksTree->Branch("hConvTracksbr", &hConvTracks, "hConvTracks/I");
    //*/
    TVectorD *xCoord, *yCoord, *zCoord, *xErr, *yErr, *zErr;
    yErr = new TVectorD(NLAD * NBOARD, &detectorErr[0]);
    xErr = new TVectorD(NLAD * NBOARD, &detectorErr[0]);
    TVectorD chi2array;
    double chi2Sum{0};
    
    unsigned int N1GoodEvent{0};
    
    genfit::Track* fitTrack(nullptr);
    TVector3 pos(0, 0, -12);
    TVector3 mom;
    mom = TVector3(0, 0, 10); // 10 GeV/c momentum
    TMatrixDSym covM(6);
    for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution;
    for (int i = 3; i < 6; ++i) covM(i,i) = pow(detectorResolution / 6 / sqrt(3), 2); // 6 is set arbitrary ~ nTrackpoints
    
    genfit::MeasuredStateOnPlane fittedState;
    genfit::FitStatus* fitStatus = nullptr;
    TVector3 fittedTrackDir, primaryPartDir;
    genfit::TrackPoint* trackPoint = nullptr;
    genfit::AbsMeasurement* rawMeas = nullptr;
    genfit::KalmanFitterInfo* fitterInfo = nullptr;
    genfit::MeasurementOnPlane residual;
    genfit::AbsTrackRep* cardRep = nullptr;
    unsigned int nTrackPoints;
    int detId;
    TVectorD scdState(2);
    float kinEnergyReco{0.0};
    
    hitsColl* hitsCollArray = new hitsColl[nEntries];
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        (hitsCollArray[event]).xCoordColl.ResizeTo(NLAD * NBOARD);
        (hitsCollArray[event]).yCoordColl.ResizeTo(NLAD * NBOARD);
        (hitsCollArray[event]).zCoordColl.ResizeTo(NLAD * NBOARD);
        
        (hitsCollArray[event]).xCoordCollSize.assign(NLAD * NBOARD, 0);
        (hitsCollArray[event]).yCoordCollSize.assign(NLAD * NBOARD, 0);
        (hitsCollArray[event]).zCoordCollSize.assign(NLAD * NBOARD, 0);
    }
    
    for (std::size_t hit{0}; hit < hitsTree->GetEntries(); ++hit) {
        hitsTree->GetEntry(hit);
        if (TrackID == 1) { // only primary tracks
            if (StepStatus == 0) {
                // std::cout << "StepStatus = 0" << std::endl;
                (hitsCollArray[EventID]).xCoordColl[volumes.at(volName)] += PosX;
                (hitsCollArray[EventID]).xCoordCollSize[volumes.at(volName)]++;
                // std::cout << "(hitsCollArray[" << EventID << "]).xCoordCollSize[" << volumes.at(volName) << "]: " << (hitsCollArray[EventID]).xCoordCollSize[volumes.at(volName)] << std::endl;
                (hitsCollArray[EventID]).yCoordColl[volumes.at(volName)] += PosY;
                (hitsCollArray[EventID]).yCoordCollSize[volumes.at(volName)]++;
                // std::cout << "(hitsCollArray[" << EventID << "]).yCoordCollSize[" << volumes.at(volName) << "]: " << (hitsCollArray[EventID]).yCoordCollSize[volumes.at(volName)] << std::endl;
                (hitsCollArray[EventID]).zCoordColl[volumes.at(volName)] += PosZ;
                (hitsCollArray[EventID]).zCoordCollSize[volumes.at(volName)]++;
                //  std::cout << "(hitsCollArray[" << EventID << "]).zCoordCollSize[" << volumes.at(volName) << "]: " << (hitsCollArray[EventID]).zCoordCollSize[volumes.at(volName)] << std::endl;
            }
        }
    }
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        
        delete fitTrack;
        fitTrack = nullptr;
        
        primariesTree->GetEntry(event);
        pos = TVector3(Y0, X0, Z0); // swap the x and y axes to follow the geometry
        pos *= mmToCm;
        mom = TVector3(Py,Px,Pz);
        mom *= 1E-03; // from MeV/c to GeV/c
        bool skipEvent{false};
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) { // middle pos in the detector
            if ((hitsCollArray[event]).xCoordCollSize[i] == 0 || (hitsCollArray[event]).yCoordCollSize[i] == 0 || (hitsCollArray[event]).zCoordCollSize[i] == 0) {
                // std::cout << "(hitsCollArray[" << event << "]).xCoordCollSize[" << i << "]: " << (hitsCollArray[event]).xCoordCollSize[i] << std::endl;
                // std::cout << "(hitsCollArray[" << event << "]).yCoordCollSize[" << i << "]: " << (hitsCollArray[event]).yCoordCollSize[i] << std::endl;
                // std::cout << "(hitsCollArray[" << event << "]).zCoordCollSize[" << i << "]: " << (hitsCollArray[event]).zCoordCollSize[i] << std::endl;
                skipEvent = true;
                break;
            }
            (hitsCollArray[event]).xCoordColl[i] *= mmToCm / (hitsCollArray[event]).xCoordCollSize[i];
            (hitsCollArray[event]).yCoordColl[i] *= mmToCm / (hitsCollArray[event]).yCoordCollSize[i];
            (hitsCollArray[event]).zCoordColl[i] *= mmToCm / (hitsCollArray[event]).zCoordCollSize[i];
        }
        
        if (skipEvent) continue;
        
        std::cout << "event: " << event << std::endl;
        
        double sign(1.);
        genfit::AbsTrackRep* rep = new genfit::RKTrackRep(sign*pdgId);
        genfit::MeasuredStateOnPlane stateRef(rep);
        rep->setPosMomCov(stateRef, pos, mom, covM);
            
        // create track
        TVectorD seedState(6);
        TMatrixDSym seedCov(6);
        rep->get6DStateCov(stateRef, seedState, seedCov);
        fitTrack = new genfit::Track(rep, seedState, seedCov); //initialized with smeared rep
        
        // std::cout << "after fitTrack definition: " << std::endl;
        
        // std::cout << "before performFitting: " << std::endl;
        
        bool fitResult = performFitting((hitsCollArray[event]).zCoordColl, (hitsCollArray[event]).xCoordColl, (hitsCollArray[event]).yCoordColl, *zErr, *yErr, *xErr, fitTrack, fitter); // swap the x and y axes to follow the geometry
        
        // std::cout << "after performFitting: " << std::endl;
        
        yErrors.clear();
        yErrors.shrink_to_fit();
        xErrors.clear();
        xErrors.shrink_to_fit();
        
        // std::cout << "before residual calculation: " << std::endl;
        
        hnmeas = (hitsCollArray[event]).xCoordColl.GetNrows();
        
        cardRep = fitTrack->getCardinalRep();
        fitStatus = fitTrack->getFitStatus(cardRep);
        
        if (fitResult && fitStatus->isFitConverged()) {
            
            fittedState = fitTrack->getFittedState();
            chi2 = fitStatus->getChi2() / fitStatus->getNdf();
            pvalue = fitStatus->getPVal();
            chi2Sum += fitStatus->getChi2();
            covMatrix.ResizeTo(fittedState.getCov());
            // correlMatrix.ResizeTo(r->GetCorrelationMatrix());
            covMatrix = fittedState.getCov();
            // correlMatrix = r->GetCorrelationMatrix();
            
            nTrackPoints = fitTrack->getNumPoints();
            
            std::cout << "nTrackPoints: " << nTrackPoints << std::endl;
            
            for (std::size_t i = 0; i < nTrackPoints; ++i) {
                trackPoint = fitTrack->getPointWithMeasurementAndFitterInfo(i, cardRep);
                rawMeas = trackPoint->getRawMeasurement();
                fittedState = fitTrack->getFittedState(i, cardRep);
                detId = rawMeas->getDetId();
                fitterInfo = trackPoint->getKalmanFitterInfo();
                residual = fitterInfo->getResidual();
                scdState = residual.getState();
                xErrors.push_back(scdState[0]);
                yErrors.push_back(scdState[1]);
            }
            
            fittedState = fitTrack->getFittedState();
            
            N1GoodEvent++;
            
            rmse_x = TMath::RMS(xErrors.begin(), xErrors.end());
            rmse_y = TMath::RMS(yErrors.begin(), yErrors.end());
            
            momReco = fittedState.getMomMag();
            momErr = (momReco - P * 1E-03) / (P * 1E-03);
            momX = (fittedState.getMom()).X();
            momY = (fittedState.getMom()).Y();
            momZ = (fittedState.getMom()).Z();
            
            kinEnergyReco = particleMass * (sqrt(1 + (momReco * 1000) * (momReco * 1000) / (particleMass * particleMass)) - 1);
            energyErr = (kinEnergyReco - kinEnergy) / kinEnergy;
            
            hPDG = fittedState.getPDG();
            
            tracksTree->Fill();
            
            display->addEvent(fitTrack);
        }
        
        // std::cout << "after residual calculation: " << std::endl;

    }
    
    delete[] hitsCollArray;
    
    chi2array.ResizeTo(1);
    chi2array[0] = chi2Sum / N1GoodEvent;
    chi2Sum = 0.;
    
    outFile->cd();
    tracksTree->Write();
    outFile->Close();
    
    inFile->Close();
    
    std::cout << "Total number of events: " << nEntries << std::endl;
    
    delete fitter;
    
    display->setOptions("ABDEFGHMPT"); // G show geometry
    display->open();
}

