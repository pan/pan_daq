//
//  panGeometry.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 17.01.22.
//

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TVectorD.h"
#include "TFitResult.h"
#include "TGeoManager.h"
#include "TString.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <unordered_map>
#include <algorithm>
#include <numeric>

typedef unsigned int uint;
typedef vector<double> vdouble;
typedef vector<int> vint;
typedef vector<string> vstring;

constexpr std::size_t NMAG{2};
constexpr std::size_t NLAD{6};
std::size_t NYLAD{3};
constexpr std::size_t NPLAD{2};
constexpr std::size_t NBOARD{3};
constexpr std::size_t NBRANCH{4};
constexpr std::size_t NTOTCH{2048};
constexpr std::size_t NYCH{128};
constexpr std::size_t NPCH{512};
constexpr double conversionCoef{0.0025}; // cm / ch for StripX
constexpr double conversionCoefY{0.04}; // cm / ch for StripY
constexpr double conversionCoefPixel{0.0055}; // cm / ch for Pixel
constexpr bool nov2021BT{false};
constexpr bool june2022BT{false};
constexpr bool sep2022BT{false};
constexpr bool nov2022BT{false};
constexpr bool apr2023BT{false};
constexpr bool aug2023BT{true};
constexpr bool enablePixels{true};

vdouble detectorPos;
vdouble magnetPos = {-3.1, 3.1};
vdouble detectorPosErr = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
vector<vdouble> alignPars = {
    {7.5977900e-06, -6.8999600e-07, 8.30209e-05},
    {-3.7525000e-05, 9.1912700e-07, -0.000340924},
    {0.00022215100, -4.5594400e-08, 0.00020294},
    {-0.00017130100, 8.1104300e-08, 0.000157194},
    {-5.7136400e-06, -1.0651500e-06, 5.19773e-05},
    {-4.5760600e-05, 7.2106000e-07, 0.000498967}
    // {7.88065e-04, -0.193191e-04, 1.49739e-06},
    // {-29.9637e-04, 0.138847e-04, -1.20184e-05},
    // {16.8955e-04, -0.22272e-04, 2.54002e-06},
    // {11.3012e-04, 0.391145e-04, 1.06067e-05},
    // {21.4351e-04, -0.172721e-04, -7.14252e-07},
    // {-26.0258e-04, 0.0270422e-04, -1.69074e-05}
};
vdouble detectorPosY;
vdouble detectorPosPixel;

void readParameters() {
    if (june2022BT) {
        detectorPos = {-7.006, -6.191, -0.470, 0.330, 5.870, 6.685};
        detectorPosY = {-5.870, -0.009, 7.006};
        detectorPosPixel = {-7.8695, 7.8845};
    }
    else if (sep2022BT) {
        detectorPos = {-6.699, -5.884, -0.316, 0.484, 5.884, 6.699};
        detectorPosY = {-7.02, 0.1630, 7.02};
        detectorPosPixel = {-7.8985, 7.8985};
    }
    else if (nov2022BT) {
        detectorPos = {-6.6075, -5.7925, -0.4075, 0.3925, 5.7925, 6.6075};
        detectorPosY = {-6.9285, 0.0715, 6.9285};
        detectorPosPixel = {-7.807, 7.807};
    }
    else if (apr2023BT) {
        detectorPos = {-6.645, -5.83, -0.395, 0.405, 5.83, 6.645};
        NYLAD = 2;
        detectorPosY = {-7.004, 7.004};
        detectorPosPixel = {-7.8825, 7.8825};
    }
    else if (aug2023BT) {
        detectorPos = {-6.645, -5.83, -0.395, 0.405, 5.83, 6.645};
        NYLAD = 3;
        detectorPosY = {-7.004, 0.0460, 7.004};
        detectorPosPixel = {-7.8825, 7.8825};
    }
    else {
        detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81
    }
}

void buildAlignedGeometry(vector<vdouble> pars = alignPars) {
    
    TGeoManager *geom = new TGeoManager("panAlign", "MiniPAN aligned beamtest geometry");

    //--- define some materials

    TGeoElementTable *table = geom->GetElementTable();
    TGeoElement *elN = table->FindElement("N");
    TGeoElement *elO = table->FindElement("O");
    TGeoElement *elAr = table->FindElement("Ar");
    TGeoElement *elAl = table->FindElement("Al");
    TGeoElement *elSi = table->FindElement("Si");

    // TGeoMaterial *matVacuum = new TGeoMaterial("Vacuum", 0,0,0);

    TGeoMaterial *matAl = new TGeoMaterial("Al", elAl, 2.7);
    TGeoMaterial *matSi = new TGeoMaterial("Si", elSi, 2.33);
    TGeoMixture *matAir = new TGeoMixture("Air", 3, 0.0012);
    matAir->AddElement(elN, 0.78);
    matAir->AddElement(elO, 0.21);
    matAir->AddElement(elAr, 0.01);
    matAir->SetState(TGeoMaterial::kMatStateGas);
    matAir->SetTemperature(288.15);

    //   //--- define some media
    TGeoMedium *Air = new TGeoMedium("Environment",1, matAir);
    TGeoMedium *Al = new TGeoMedium("Frame Material",2, matAl);
    TGeoMedium *Si = new TGeoMedium("Detector Material",3, matSi);

    //--- make the top container volume
    Double_t worldx = 110.;
    Double_t worldy = 50.;
    Double_t worldz = 5.;
    TGeoVolume *top = geom->MakeBox("TOP", Air, 4., 4., 15.);
    geom->SetTopVolume(top);
    TGeoVolume *stripXbox = geom->MakeBox("StripX", Air, 3., 3., 0.1);
    //stripXbox->SetVisibility(kFALSE);
    stripXbox->SetLineColor(kRed);

    //--- make a silicon strip

    TGeoVolume *strip = geom->MakeBox("strip", Si, 2.55, 0.00124, 0.0075); // 0.0025 cm pitch, 5.1 cm length, 150 um thickness
    strip->SetLineColor(kGray);
    
    for (int i = 0; i < (int)NTOTCH; ++i) {
        stripXbox->AddNode(strip, i + 1, new TGeoTranslation(0., (i - (int)NTOTCH / 2) * conversionCoef, 0.)); // StripX is measuring Y coordinate for simplicity for now...
    }

    TGeoRotation *rot1 = nullptr;
    
    for (std::size_t i = 0; i < NLAD; ++i) {
        rot1 = new TGeoRotation(("rot" + std::to_string(i + 1)).c_str(), 0, pars.at(i)[2], 0);
        top->AddNode(stripXbox, i + 1, new TGeoCombiTrans(0., pars.at(i)[0], detectorPos.at(i) + pars.at(i)[1], rot1));
    }

    //--- close the geometry
    geom->CloseGeometry();
    
    geom->Export("MiniPANGeomAlign.root");

    //--- draw the ROOT box.
    // by default the picture will appear in the standard ROOT TPad.
    //if you have activated the following line in system.rootrc,
    //it will appear in the GL viewer
    //#Viewer3D.DefaultDrawOption:   ogl

    geom->SetVisLevel(2);
    top->Draw("ogle");
}

void buildMiniPANGeometry(TString filename = "MiniPANGeom.root") {
    
    // gStyle->SetCanvasPreferGL(true);

    TGeoManager *geom = new TGeoManager("pan", "MiniPAN beamtest geometry");

    //--- define some materials

    TGeoElementTable *table = geom->GetElementTable();
    TGeoElement *elN = table->FindElement("N");
    TGeoElement *elO = table->FindElement("O");
    TGeoElement *elAr = table->FindElement("Ar");
    TGeoElement *elAl = table->FindElement("Al");
    TGeoElement *elSi = table->FindElement("Si");
    TGeoElement *elNd = table->FindElement("Nd");
    TGeoElement *elFe = table->FindElement("Fe");
    TGeoElement *elB = table->FindElement("B");

    // TGeoMaterial *matVacuum = new TGeoMaterial("Vacuum", 0,0,0);

    TGeoMaterial *matAl = new TGeoMaterial("Al", elAl, 2.7);
    TGeoMaterial *matSi = new TGeoMaterial("Si", elSi, 2.33);
    TGeoMixture *matAir = new TGeoMixture("Air", 3, 0.0012);
    matAir->SetState(TGeoMaterial::kMatStateGas);
    matAir->SetTemperature(288.15);
    matAir->SetPressure(6.32420e+8);
    matAir->AddElement(elN, 0.78);
    matAir->AddElement(elO, 0.21);
    matAir->AddElement(elAr, 0.01);
    
    TGeoMixture *matMagnet = new TGeoMixture("Magnet", 3, 7.5);
    matMagnet->SetState(TGeoMaterial::kMatStateSolid);
    matMagnet->SetTemperature(288.15);
    matMagnet->AddElement(elNd, 2);
    matMagnet->AddElement(elFe, 14);
    matMagnet->AddElement(elB, 1);

    //   //--- define some media
    TGeoMedium *Air = new TGeoMedium("Environment",1, matAir);
    TGeoMedium *Al = new TGeoMedium("Frame Material",2, matAl);
    TGeoMedium *Si = new TGeoMedium("Detector Material",3, matSi);
    TGeoMedium *Magnet = new TGeoMedium("Magnet Material", 4, matMagnet);

    //--- define the transformations
    TGeoTranslation *tr1 = new TGeoTranslation(conversionCoef, 0, 0.);
    TGeoTranslation *tr2 = new TGeoTranslation(0., 0., 10.);

    TGeoTranslation *tr3 = new TGeoTranslation(10., 20., 0.);
    TGeoTranslation *tr4 = new TGeoTranslation(5., 10., 0.);
    TGeoTranslation *tr5 = new TGeoTranslation(20., 0., 0.);
    TGeoTranslation *tr6 = new TGeoTranslation(-5., 0., 0.);
    TGeoTranslation *tr7 = new TGeoTranslation(7.5, 7.5, 0.);
    TGeoRotation   *rot1 = new TGeoRotation("rot1", 90., 0., 90., 270., 0., 0.);
    TGeoCombiTrans *combi1 = new TGeoCombiTrans(7.5, -7.5, 0., rot1);
    TGeoTranslation *tr8 = new TGeoTranslation(7.5, -5., 0.);
    TGeoTranslation *tr9 = new TGeoTranslation(7.5, 20., 0.);
    TGeoTranslation *tr10 = new TGeoTranslation(85., 0., 0.);
    TGeoTranslation *tr11 = new TGeoTranslation(35., 0., 0.);
    TGeoTranslation *tr12 = new TGeoTranslation(-15., 0., 0.);
    TGeoTranslation *tr13 = new TGeoTranslation(-65., 0., 0.);

    TGeoTranslation  *tr14 = new TGeoTranslation(0,0,-100);
    TGeoCombiTrans *combi2 = new TGeoCombiTrans(0,0,100,
                                    new TGeoRotation("rot2",90,180,90,90,180,0));
    TGeoCombiTrans *combi3 = new TGeoCombiTrans(100,0,0,
                                    new TGeoRotation("rot3",90,270,0,0,90,180));
    TGeoCombiTrans *combi4 = new TGeoCombiTrans(-100,0,0,
                                    new TGeoRotation("rot4",90,90,0,0,90,0));
    TGeoCombiTrans *combi5 = new TGeoCombiTrans(0,100,0,
                                    new TGeoRotation("rot5",0,0,90,180,90,270));
    TGeoCombiTrans *combi6 = new TGeoCombiTrans(0,-100,0,
                                    new TGeoRotation("rot6",180,0,90,180,90,90));

    //--- make the top container volume
    Double_t worldx = 110.;
    Double_t worldy = 50.;
    Double_t worldz = 5.;
    TGeoVolume *top = geom->MakeBox("TOP", Air, 4., 4., 15.);
    geom->SetTopVolume(top);
    TGeoVolume *stripXbox = geom->MakeBox("StripX", Air, 3., 3., 0.1);
    TGeoVolume *stripYbox = geom->MakeBox("StripY", Air, 3., 3., 0.1);
    TGeoVolume *pixelBox = geom->MakeBox("Pixel", Air, 1.5, 1.5, 0.25);
    //stripXbox->SetVisibility(kFALSE);
    stripXbox->SetLineColor(kRed);

    //--- make a silicon strip

    TGeoVolume *strip = geom->MakeBox("stripX", Si, 2.56, 2.56, 0.0075); // 0.0025 cm pitch, 5.12 cm length, 150 um thickness
    TGeoVolume *stripY = geom->MakeBox("stripY", Si, 2.56, 2.56, 0.0075); // 0.04 cm pitch, 5.12 cm length, 150 um thickness
    // TGeoVolume *stripY = geom->MakeTube("stripY", Si, 0, 2.56, 0.0075);
    TGeoVolume *pixel = geom->MakeBox("pixel", Si, 1.4135, 1.4135, 0.0355); // 55 um pitch, 710 um thickness, 0.845 um of the PCB thickness
    TGeoVolume *magnet = geom->MakeTube("magnet", Magnet, 2.5, 3.5, 2.5);
    TGeoVolume *magnetRing = geom->MakeTube("magnetRing", Al, 3.501, 3.8, 2.5);
    strip->SetLineColor(kGray);
    stripY->SetLineColor(kGray);
    pixel->SetLineColor(kGray);
    magnet->SetLineColor(kBlack);
    magnetRing->SetLineColor(kYellow);

    stripXbox->AddNode(strip, 1, new TGeoTranslation(0, 0, 0)); // StripX is measuring Y coordinate for simplicity for now...
    
    if (!nov2021BT) {
        stripYbox->AddNode(stripY, 1, rot1); // Stripy is measuring X coordinate for simplicity for now...
        pixelBox->AddNode(pixel, 1, new TGeoTranslation(0, 0, 0));
    }
        
    for (std::size_t i = 0; i < NLAD; ++i) {
        top->AddNode(stripXbox, i + 1, new TGeoTranslation(0. ,0. ,detectorPos.at(i)));
    }
    
    for (std::size_t i = 0; i < NMAG; ++i) {
        top->AddNode(magnet, i + 1, new TGeoCombiTrans(0, 0, magnetPos.at(i), rot1));
        top->AddNode(magnetRing, i + 1, new TGeoCombiTrans(0, 0, magnetPos.at(i), rot1));
    }
    
    if (!nov2021BT) {
        for (std::size_t i = 0; i < NYLAD; ++i) top->AddNode(stripYbox, i + 1, new TGeoTranslation(0. ,0. ,detectorPosY.at(i)));
        for (std::size_t i = 0; i < NPLAD; ++i) {
            top->AddNode(pixelBox, i + 1, new TGeoTranslation(0. ,0. ,detectorPosPixel.at(i)));
        }
    }

    //--- close the geometry
    geom->CloseGeometry();
    
    geom->Export(filename);

    //--- draw the ROOT box.
    // by default the picture will appear in the standard ROOT TPad.
    //if you have activated the following line in system.rootrc,
    //it will appear in the GL viewer
    //#Viewer3D.DefaultDrawOption:   ogl

    geom->SetVisLevel(2);
    top->Draw("ogle");

}
