This repository contains various tools and scripts used to analyse the data produced by Mini.PAN.
There are tools to decode and synchronise data for StripX/Y detectors, analyse ToF data, do track reconstruction and alignment of the device.
For the moment all tools and scripts are built separately. The user can find the build/usage instructions for every essential tool below.

# StripX/Y data viewer

## Overview

This repository contains the source code for the DisPanOnline software, which is designed to view the data recorded by StripX/Y sensors, perform simple analysis and clustering.

## Building Instructions

To build the DisPanOnline software, follow these steps:

### Step 1: Clone the Repository

Download this repository to your local machine.

### Step 2: Install Prerequisites

Ensure that you have the following dependencies installed on your system:

- [Root](https://root.cern/) (v6.26.10 or higher)
- GCC compiler v11.2 or higher

### Step 3: Build the Software

Run the provided instructions.

```bash
cd <path-to-pan_daq>
source <path-to-root-install>/bin/thisroot.sh
rootcling -f newfdg.cxx -c TGNewFileDialog.hxx TGNewFileDialog_LinkDef.h
rootcling -f exDict.cxx -c DisPanOnline.hxx DisPanOnline_LinkDef.h
g++ -fPIC -O2 -g -Wall -I/usr/include/netinet -c exDict.cxx newfdg.cxx main.cpp DisPanOnline.cxx GpioDaq/GpioDaq.cxx FitUdpServer/FitUdpServer.cxx Detector.cpp TGNewFileDialog.cxx `root-config --cflags`
g++ -static-libstdc++ -o DisPanOnline exDict.o newfdg.o main.o DisPanOnline.o GpioDaq.o FitUdpServer.o Detector.o TGNewFileDialog.o `root-config --evelibs` -lMinuit
```

### Step 4: Execute the Software

After successful compilation, you can execute the `DisPanOnline` binary to run the software.

```bash
./DisPanOnline <number-of-stripX> <number-of-stripY>
# 1 StripX detector
./DisPanOnline 1 0
# 2 StripX detectors
./DisPanOnline 2 0
# 1 StripY detector
./DisPanOnline 0 1
# 2 StripX and 1 StripY detector
./DisPanOnline 2 1
```

# MiniPAN alignment routine (straight tracks, no magnets)

## Overview

This repository contains the source code for the alignment software, which is designed to do track based alignment for the MiniPAN prototype using straight tracks (without magnetic field). The minimisation is based on the gradient descent method.

## Building Instructions

To build the alignment software, follow these steps:


### Step 1: Install Prerequisites

Ensure that you have the following dependencies installed on your system:

- [Root](https://root.cern/) (v6.24.06 or higher)
- [Boost](https://www.boost.org/) (v1.82.0 or higher)
- [CMake](https://cmake.org/) build system v3.17 or higher
- GCC compiler v11.2 or higher

### Step 2: Build the Software

Run the provided instructions:

```bash
cd <path-to-pan_daq>
source <path-to-root-install>/bin/thisroot.sh
cd alignment/
mkdir build && cd build/
cmake -DBOOST_ROOT=<path-to-boost-install>/ ..
cmake --build . -- install -j<number-of-threads-on-your-machine>
```

### Step 3: Execute the Software

The software is designed to be executed on the cluster running the scripts from `alignment/bash_scripts` directory

```bash
# Login to the cluster (Yggdrasil for instance)

ssh <your-user-name>@login1.yggdrasil.hpc.unige.ch

# Load the following modules

module load GCC/11.2.0
module load OpenMPI/4.1.1
module load ROOT/6.24.06
module load CMake
module load Boost

# Download and extract the pan_daq repository

cd <path-to-pan_daq>/alignment/bash_scripts

# Adjust the main loop file according to your needs (follow the comments in the file)
# The following example applies for the alignment using only strip detectors

vim alignment_main_loop_strips.sh

# Command to start the alignment algorithm execution on the cluster

nohup ./alignment_main_loop_strips.sh <synchronized-root-file-name> 100 <number-of-iterations> > <logfile-name> &

# For example

nohup ./alignment_main_loop_strips.sh synced_clusters_09042023_2145.root 100 500 > nohup_09042023_2145.log &

# The user should monitor the following files:
# global parameters file
# It is updated every alignment iteration

cat ~/scratch/alignment/global_pars/parameters_09042023_2145.txt

Gamma       # Gamma is the current gradient step size. It is always <= 1
0.25
DeltaX DeltaY DeltaZ ThetaX ThetaY ThetaZ       # These are current values of alignment correction parameters
0.0000000000e+00 0.0000000000e+00 0.0000000000e+00 0.0000000000e+00 0.0000000000e+00 0.0000000000e+00 # StripX 0
0.0000000000e+00 1.9885315443e-02 -2.0490865596e-04 5.3326979130e-05 0.0000000000e+00 0.0000000000e+00 # StripX 1
0.0000000000e+00 -8.0424190190e-03 9.1155896648e-05 -2.1718020262e-05 0.0000000000e+00 0.0000000000e+00 # StripX 2
0.0000000000e+00 8.6249055039e-03 -9.4888922115e-05 2.4100370682e-05 0.0000000000e+00 0.0000000000e+00 # StripX 3
0.0000000000e+00 -1.1474050119e-02 1.1562389214e-04 -2.3565254286e-05 0.0000000000e+00 0.0000000000e+00 # StripX 4
0.0000000000e+00 9.8328480652e-03 -1.2184132309e-04 2.6326508857e-05 0.0000000000e+00 0.0000000000e+00 # StripX 5
GradDeltaX GradDeltaY GradDeltaZ GradThetaX GradThetaY GradThetaZ       # These are the last gradient values used to obtain the current alignment correction parameters
0.0000000000e+00 0.0000000000e+00 0.0000000000e+00 0.0000000000e+00 0.0000000000e+00 0.0000000000e+00 # StripX 0
0.0000000000e+00 -1.4110962315e-02 1.3464330898e-04 -3.5184771357e-05 0.0000000000e+00 0.0000000000e+00 # StripX 1
0.0000000000e+00 2.6349904112e-02 -2.5389207786e-04 5.8537544666e-05 0.0000000000e+00 0.0000000000e+00 # StripX 2
0.0000000000e+00 -8.4787579570e-03 8.3583701643e-05 -2.0409267331e-05 0.0000000000e+00 0.0000000000e+00 # StripX 3
0.0000000000e+00 1.6922151046e-02 -1.6038187512e-04 3.2650438876e-05 0.0000000000e+00 0.0000000000e+00 # StripX 4
0.0000000000e+00 -2.6714143397e-02 2.6200750090e-04 -5.5574234963e-05 0.0000000000e+00 0.0000000000e+00 # StripX 5
N1GoodEvent:        # Number of events used for the alignment in the current step
10098
goodChi2Iter:   # number of consecutive "good" iterations: the gradient direction is within +-45deg cone
0
badChi2Iter:    # number of concecutive "bad" iterations
2
iterAlign:      # current iteration
3
chi2sum:
3.0428854946e+03 # current chi2/N_tracks

# alignment convergeance file

cat /home/users/s/sukhonod/scratch/alignment/convergeance/convergeance_data_09042023_2145.txt
# Number of the iteration | global chi2/N_tracks | the value of Gamma (gradient step size)
1 3.6057188169e+03 1
2 3.1992925391e+03 0.5
3 3.0428854946e+03 0.25
```

When the total number of iterations is reached, the output file is produced. It can be found here:

```bash
ls ~/scratch/alignment/fitted_rootfiles/

tracking_histos_aligned_09042023_2145.root # output file after the alignment
tracking_histos_09042023_2145.root # output file before the alignment (produced in the beginning of the alignment algorithm)

```

To run the alignment with both strips and pixel detectors use another script:

```bash
# Change the script according to your needs

vim alignment_main_loop_pixel.sh

# Run the alignment loop

nohup ./alignment_main_loop_pixel.sh <synchronized-root-file-name> 100 <number-of-iterations> > <logfile-name> &

```

This script will produce the same files as the one for strips only.

# MiniPAN alignment routine for curved tracks (with magnets)

## Overview

This repository contains the source code for the alignment software, which is designed to do track based alignment for the MiniPAN prototype using curved tracks (with magnetic field). The alignment is done using Millepede II software.

## Building Instructions

To build the alignment software, follow these steps:


### Step 1: Install Prerequisites

Ensure that you have the following dependencies installed on your system:

- [Root](https://root.cern/) (v6.24.06 or higher)
- [Boost](https://www.boost.org/) (v1.82.0 or higher)
- [CMake](https://cmake.org/) build system v3.17 or higher
- [Google Test (GTest)](https://github.com/google/googletest) is installed.
- GCC compiler v11.2 or higher

### Step 2: Build the Software 

Follow these steps to build the software:

```bash
source <path-to-root-installation>/bin/thisroot.sh
cd <path-to-pan_daq>/alignment/GBL_Millepede/GenFit-02-00-02-alignment/
mkdir build install && cd build/
cmake -DBOOST_ROOT=<path-to-boost-installation>/ -DGTEST_ROOT=<path-to-googletest-installation>/ -DCMAKE_INSTALL_PREFIX=`pwd`/../install ..
cmake --build . -- install -j<number-of-threads>
cd ../../
mkdir build && cd build/
cmake -DBOOST_ROOT=<path-to-boost-installation>/ ..
cmake --build . -- install -j<number-of-threads>
```

### Step 3: Install Millepede II software

[Millepede](https://www.desy.de/~kleinwrt/MP2/doc/html/index.html) II software is included into the repository as a submodule. If you cloned the software with submodules, you need only to compile it:

```bash
cd <path-to-pan_daq>/alignment/GBL_Millepede/MillepedeII/
make pede
```

If the `MillepedeII` subfolder is empty, then you need to download Millepede II separetely:

```bash
cd <path-to-pan_daq>/alignment/GBL_Millepede/MillepedeII/
git clone --depth 1 --branch V04-16-03 \
     https://gitlab.desy.de/claus.kleinwort/millepede-ii.git ./
make pede
```

Run the test to make sure that everithing works:

```bash
./pede -t
```

Any extra information on the Millepede II software can be found [here](https://www.desy.de/~kleinwrt/MP2/doc/html/index.html).

### Step 4: Run the alignment routine

Add run instructions here

# StripX/Y binary to ROOT format conversion tool

## Overview

This repository contains the source code for the conversion software, which is designed to perform a conversion of the raw binary data from StripX/Y sensors to the ROOT file format. This file is then used by the clustering algorithm of the StripX/Y data viewer.

## Building instructions

To build the conversion software, follow these steps:


### Step 1: Install Prerequisites

Ensure that you have the following dependencies installed on your system:

- [Root](https://root.cern/) (v6.24.06 or higher)
- [Boost](https://www.boost.org/) (v1.82.0 or higher)
- [CMake](https://cmake.org/) build system v3.17 or higher
- GCC compiler v11.2 or higher

### Step 2: Build the Software 

Follow these steps to build the software:

```bash
source <path-to-root-installation>/bin/thisroot.sh
cd <path-to-pan_daq>/scripts/Daq_to_ROOT_convertor_multi_threads/
mkdir build install && cd build/
cmake -DBOOST_ROOT=<path-to-boost-installation>/ ..
cmake --build . -- install -j<number-of-threads>
```

### Step 3: Run the conversion software

Add run instructions here

# StripX/Y synchronization tool

## Overview

This repository contains the source code for the synchronization software, which is designed to perform the synchronization of the events between 3 GPIO boards used to take data from StripX/Y boards.

## Building instructions

To build the synchronization software, follow these steps:


### Step 1: Install Prerequisites

Ensure that you have the following dependencies installed on your system:

- [Root](https://root.cern/) (v6.24.06 or higher)
- [Boost](https://www.boost.org/) (v1.82.0 or higher)
- [CMake](https://cmake.org/) build system v3.17 or higher
- GCC compiler v11.2 or higher

### Step 2: Build the Software 

Follow these steps to build the software:

```bash
source <path-to-root-installation>/bin/thisroot.sh
cd <path-to-pan_daq>/synchronisation/Strips_Synchronization/
mkdir build install && cd build/
cmake -DBOOST_ROOT=<path-to-boost-installation>/ ..
cmake --build . -- install -j<number-of-threads>
```

### Step 3: Run the conversion software

Add run instructions here
