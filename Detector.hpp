//
//  Detector.hpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 31.05.22.
//

#ifndef Detector_hpp
#define Detector_hpp

namespace Boards {

extern unsigned int NLAD;
extern unsigned int fNTOTCH;
static constexpr unsigned int NBytesFrameHeaderTail{32};

class Detector {
public:
    Detector();
    Detector(unsigned int, unsigned int, unsigned int, int);
    virtual ~Detector() {}
    
    unsigned int getNLCH() const {return fNLCH;}
    unsigned int getNADCCH() const {return fNADCCH;}
    
    unsigned int getNVA() const {return fNVA;}
    unsigned int getNVACH() const {return fNVACH;}
    unsigned int getNADC() const {return fNADC;}
    int getConvertMode() const {return convertMode;}
    unsigned int getNBytes() const {return fNBytes;}
    
    void setNVA(unsigned int nva) {fNVA = nva;}
    void setNVACH(unsigned int nvach) {fNVACH = nvach;}
    void setNADC(unsigned int nadc) {fNADC = nadc;}
    
protected:
    unsigned int fNLCH{0}, fNADCCH{0}, fNBytes{0};
    
private:
    unsigned int fNVA;
    unsigned int fNVACH;
    unsigned int fNADC;
    int convertMode;
};

class StripX : public Detector {
public:
    StripX();
    ~StripX() {}
};

class StripY : public Detector {
public:
    StripY();
    ~StripY() {}
};

}

#endif /* Detector_hpp */
