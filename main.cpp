#include "DisPanOnline.hxx"
#include <stdlib.h>

int main(int argc, char * argv[]) {
	
	TApplication* myApp = new TApplication("myApp", &argc, argv) ;

	new MyMainFrame(gClient->GetRoot(), 1200, 800, atoi(argv[1]), atoi(argv[2]));
	
	myApp->Run();
    myApp->Delete();
	return 0;

}
