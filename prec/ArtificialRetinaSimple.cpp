//
//  ArtificialRetinaSimple.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 07.12.21.
//

#include "ArtificialRetinaSimple.hpp"

ArtificialRetina::ArtificialRetina() : _receptorSigma{1.0}, _threshold{0.5}, _lowBoundMapK{0.}, _lowBoundMapB{0.}, _upBoundMapK{0.}, _upBoundMapB{0.}, _telescopeLength{0.}, h2{nullptr} {
    // 1 channel of the detector
    // 50 % of the maximum peak
    _vectorK.ResizeTo(0);
    _vectorB.ResizeTo(0);
}

ArtificialRetina::ArtificialRetina(double sigma, double threshold = 0.5) : _receptorSigma{sigma}, _threshold{threshold}, _lowBoundMapK{0.}, _lowBoundMapB{0.}, _upBoundMapK{0.}, _upBoundMapB{0.}, _telescopeLength{0.}, h2{nullptr} {
    _vectorK.ResizeTo(0);
    _vectorB.ResizeTo(0);
}

ArtificialRetina::ArtificialRetina(const ArtificialRetina &obj) : _receptorSigma{obj._receptorSigma}, _threshold{obj._threshold}, _lowBoundMapK{obj._lowBoundMapK}, _lowBoundMapB{obj._lowBoundMapB}, _upBoundMapK{obj._upBoundMapK}, _upBoundMapB{obj._upBoundMapB}, _telescopeLength{obj._telescopeLength} {
    if (obj.h2) h2 = new TH2D(*obj.h2);
    else h2 = nullptr;
    TVectorD bufferK(obj._vectorK);
    TVectorD bufferB(obj._vectorB);
    _vectorK = bufferK;
    _vectorB = bufferB;
}
 
ArtificialRetina::~ArtificialRetina() {
    _vectorK.ResizeTo(0);
    _vectorB.ResizeTo(0);
    if (h2) {
        h2->Reset("ICESM");
        delete h2;
        h2 = nullptr;
    }
}

void ArtificialRetina::GenerateMap(double length) {
    _telescopeLength = length;
    _lowBoundMapK = (-1.) * _detectorSize * (1. + _misalignmentMargin) / _telescopeLength;
    _upBoundMapK = (-1.) * _lowBoundMapK;
    _lowBoundMapB = (-1.) * _detectorSize * _misalignmentMargin;
    _upBoundMapB = _detectorSize * (1. + _misalignmentMargin);
    
    // unsigned int sizeKvector = (unsigned int) (2 * _upBoundMapK / _receptorSigma * 3) + 1; // 3 sigmas between each map node
    // unsigned int sizeBvector = (unsigned int) ((_upBoundMapB - _lowBoundMapB) / _receptorSigma * 3) + 1;
    
    unsigned int sizeKvector = _vecSize;
    unsigned int sizeBvector = _vecSize;
    
    _vectorK.ResizeTo(sizeKvector);
    _vectorB.ResizeTo(sizeBvector);
    
    for (std::size_t i{0}; i < sizeKvector; ++i) _vectorK[i] = _lowBoundMapK + i * (_upBoundMapK - _lowBoundMapK) / sizeKvector;
    for (std::size_t i{0}; i < sizeBvector; ++i) _vectorB[i] = _lowBoundMapB + i * (_upBoundMapB - _lowBoundMapB) / sizeBvector;
    
    if (!h2) h2 = new TH2D("h2", "test", _mapSize, _lowBoundMapK, _upBoundMapK, _mapSize, _lowBoundMapB, _upBoundMapB);
    else h2->Reset("ICESM");
    h2->SetStats(0);
}

void ArtificialRetina::FillMap(double zPos, double yPos) {
    
    unsigned int sizeKvector = _vectorK.GetNrows();
    unsigned int sizeBvector = _vectorB.GetNrows();
    for (std::size_t i{0}; i < sizeKvector; ++i) {
        for (std::size_t j{0}; j < sizeBvector; ++j) {
            h2->Fill(_vectorK[i], _vectorB[j], TMath::Exp((-1.) * TMath::Sq(yPos - _vectorK[i] * zPos - _vectorB[j]) / (2. * TMath::Sq(_receptorSigma))));
        }
    }
}

std::unordered_map<std::size_t, std::pair<double, double>> ArtificialRetina::FindPeaks() {
    
    std::unordered_map<std::size_t, std::pair<double, double>> collectionOfRecoTracks;
    
    unsigned int sizeKvector = _vectorK.GetNrows();
    unsigned int sizeBvector = _vectorB.GetNrows();
    
    //now the real stuff: Finding the peaks
    TSpectrum2 s(10);
    Int_t nfound = s.Search(h2,2,"noMarkov nodraw goff", _threshold);
     
    //searching good and ghost peaks (approximation)
    Double_t *xpeaks = s.GetPositionX();
    Double_t *ypeaks = s.GetPositionY();
    for (Int_t pf{0}; pf < nfound; pf++) {
        collectionOfRecoTracks[pf] = {xpeaks[pf], ypeaks[pf]};
    }
     
    // s.Print();
    
    return collectionOfRecoTracks;
}

void ArtificialRetina::AssociateClustersWithTrackCand(std::unordered_map<std::size_t, std::pair<double, double>> &patterns, std::unordered_map<int, vector<double> *> &vcogsPR, std::unordered_map<int, vector<double> *> &vintegralsPR, std::unordered_map<int, vector<int> *> &vlengthsPR, std::unordered_map<int, vector<double> *> &vsovernsPR, std::unordered_map<int, std::vector<std::vector<double>> *> &vsovernPerChannelPR, std::unordered_map<int, vector<double> *> &vcogs, std::unordered_map<int, vector<double> *> &vintegrals, std::unordered_map<int, vector<int> *> &vlengths, std::unordered_map<int, vector<double> *> &vsoverns, std::unordered_map<int, std::vector<std::vector<double>> *> &vsovernPerChannel, std::vector<double> &detectorPos) {
    
    std::vector<double> yCoord, integral, sovern;
    std::vector<int> length;
    std::vector<std::vector<double>> sovernPerChannel;
    double yTrack{0.};
    std::vector<double>::iterator it;
    std::size_t element_id{0};
    
    for (std::size_t i{0}; i < patterns.size(); ++i) {
        for (std::size_t j{0}; j < vcogs.size(); ++j) {
            yTrack = patterns[i].first * detectorPos.at(j) + patterns[i].second;
            if (vcogs[j]->size() > 0) {
                it = std::min_element(vcogs[j]->begin(), vcogs[j]->end(), [yTrack] (double lhs, double rhs) {return fabs(lhs - yTrack) < fabs(rhs - yTrack);});
                element_id = std::distance(vcogs[j]->begin(), it);
                if (*it - yTrack < 3 * _receptorSigma) {
                    yCoord.push_back(*it);
                    integral.push_back(vintegrals[j]->at(element_id));
                    length.push_back(vlengths[j]->at(element_id));
                    sovern.push_back(vsoverns[j]->at(element_id));
                    sovernPerChannel.push_back(vsovernPerChannel[j]->at(element_id));
                }
            }
        }
        if (yCoord.size() == nDetectors) {
            for (std::size_t j{0}; j < yCoord.size(); ++j) {
                vcogsPR[j]->push_back(yCoord.at(j));
                vintegralsPR[j]->push_back(integral.at(j));
                vlengthsPR[j]->push_back(length.at(j));
                vsovernsPR[j]->push_back(sovern.at(j));
                vsovernPerChannelPR[j]->push_back(sovernPerChannel.at(j));
            }
        }
        yCoord.clear();
        yCoord.shrink_to_fit();
        integral.clear();
        integral.shrink_to_fit();
        length.clear();
        length.shrink_to_fit();
        sovern.clear();
        sovern.shrink_to_fit();
        sovernPerChannel.clear();
        sovernPerChannel.shrink_to_fit();
    }
}

void ArtificialRetina::FindTrackCand(std::unordered_map<int, vector<double> *> &vcogsPR, std::unordered_map<int, vector<double> *> &vintegralsPR, std::unordered_map<int, vector<int> *> &vlengthsPR, std::unordered_map<int, vector<double> *> &vsovernsPR, std::unordered_map<int, std::vector<std::vector<double>> *> &vsovernPerChannelPR, std::unordered_map<int, vector<double> *> &vcogs, std::unordered_map<int, vector<double> *> &vintegrals, std::unordered_map<int, vector<int> *> &vlengths, std::unordered_map<int, vector<double> *> &vsoverns, std::unordered_map<int, std::vector<std::vector<double>> *> &vsovernPerChannel, std::vector<double> &detectorPos) {
    
    GenerateMap(detectorPos.back() - detectorPos.front());
    
    for (std::size_t j{0}; j < vcogs.size(); ++j) {
        if (vcogs[j]->size() > 0) {
            for (std::size_t k{0}; k < vcogs[j]->size(); ++k) {
                FillMap(detectorPos.at(j), vcogs[j]->at(k));
            }
        }
    }
    
    std::unordered_map<std::size_t, std::pair<double, double>> collectionOfRecoTracks = FindPeaks();
    AssociateClustersWithTrackCand(collectionOfRecoTracks, vcogsPR, vintegralsPR, vlengthsPR, vsovernsPR, vsovernPerChannelPR, vcogs, vintegrals, vlengths, vsoverns, vsovernPerChannel, detectorPos);
    
}

void ArtificialRetina::GenerateVcogs() {
    for (std::size_t j{0}; j < nDetectors; ++j) {
        vcogs[j] = new vector<double>;
        for (std::size_t k{0}; k < 2; ++k) {
            vcogs[j]->push_back(500. + k * 1000. + j * 4.);
        }
    }
}
