//
//  ArtificialRetinaSimple.hpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 07.12.21.
//

#ifndef ArtificialRetinaSimple_hpp
#define ArtificialRetinaSimple_hpp

#include <iostream>
#include <cmath>
#include <algorithm>
#include <unordered_map>
#include <utility>

#include "TVectorD.h"
#include "TSpectrum2.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TH2.h"
#include "TF2.h"
#include "TMath.h"
#include "TROOT.h"

class ArtificialRetina {
    
private:
    double _receptorSigma, _lowBoundMapK, _lowBoundMapB, _upBoundMapK, _upBoundMapB, _telescopeLength, _threshold;
    TVectorD _vectorK, _vectorB;
    TH2D *h2;
    
public:
    
    static constexpr std::size_t _detectorSize{2048}; // 51.2 mm // PAN StripX sensitive area size in mm
    static constexpr std::size_t _mapSize{200};
    static constexpr std::size_t _vecSize{400};
    static constexpr std::size_t nDetectors{6};
    
    double _misalignmentMargin{0.05}; // 5 % misalignment margin
    
    
    ArtificialRetina();
    ArtificialRetina(double, double);
    ArtificialRetina(ArtificialRetina const &obj);
    ArtificialRetina operator = (ArtificialRetina const &obj);
    ~ArtificialRetina();
    
    void setGrainingSigma(double sigma) {_receptorSigma = sigma;}
    void setThreshold(double thresh) {_threshold = thresh;}
    
    double getTelescopeLength() const {return _telescopeLength;}
    double getThreshold() const {return _threshold;}
    double getGrainingSigma() const {return _receptorSigma;}
    
    TH2D * getHistogram() const {return h2;}
    
    void GenerateMap(double length);
    void FillMap(double zPos, double yPos);
    std::unordered_map<std::size_t, std::pair<double, double>> FindPeaks();
    void AssociateClustersWithTrackCand(std::unordered_map<std::size_t, std::pair<double, double>> &patterns, std::unordered_map<int, vector<double> *> &vcogsPR, std::unordered_map<int, vector<double> *> &vintegralsPR, std::unordered_map<int, vector<int> *> &vlengthsPR, std::unordered_map<int, vector<double> *> &vsovernsPR, std::unordered_map<int, std::vector<std::vector<double>> *> &vsovernPerChannelPR, std::unordered_map<int, vector<double> *> &vcogs, std::unordered_map<int, vector<double> *> &vintegrals, std::unordered_map<int, vector<int> *> &vlengths, std::unordered_map<int, vector<double> *> &vsoverns, std::unordered_map<int, std::vector<std::vector<double>> *> &vsovernPerChannel, std::vector<double> &detectorPos);
    void FindTrackCand(std::unordered_map<int, vector<double> *> &vcogsPR, std::unordered_map<int, vector<double> *> &vintegralsPR, std::unordered_map<int, vector<int> *> &vlengthsPR, std::unordered_map<int, vector<double> *> &vsovernsPR, std::unordered_map<int, std::vector<std::vector<double>> *> &vsovernPerChannelPR, std::unordered_map<int, vector<double> *> &vcogs, std::unordered_map<int, vector<double> *> &vintegrals, std::unordered_map<int, vector<int> *> &vlengths, std::unordered_map<int, vector<double> *> &vsoverns, std::unordered_map<int, std::vector<std::vector<double>> *> &vsovernPerChannel, std::vector<double> &detectorPos);
    
    std::unordered_map<int, vector<double> *> vcogs;
    void GenerateVcogs();
};

#endif /* ArtificialRetinaSimple_hpp */
