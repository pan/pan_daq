//
//  applyPatternRecognition.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 14.07.22.
//

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TVectorD.h"
#include "TStyle.h"

#include <iostream>
#include <vector>
#include <array>
#include <unordered_map>
#include <algorithm>

#include "ArtificialRetinaSimple.hpp"

typedef vector<double> vdouble;
typedef vector<int> vint;

constexpr std::size_t NLAD{2};
constexpr std::size_t NBOARD{3};
constexpr std::size_t NBRANCH{5};

constexpr bool nov2021BT{false};
constexpr bool june2022BT{true};

vdouble detectorPos;

void ApplyPR(std::size_t nIter = 10, string inputFile = "synced_clusters.root", string outputFile = "synced_clusters_afterPR.root") {
    gStyle->SetOptFit(1);
    
    std::unordered_map<int, vdouble *> vintegrals, vcogs, vsoverns, vcogsPR, vintegralsPR, vsovernsPR;
    std::unordered_map<int, std::vector<std::vector<double>> *> vsovernPerChannel, vsovernPerChannelPR;
    std::unordered_map<int, vint *> vlengths, vlengthsPR;
    
    if (nov2021BT) detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81 cm // November 2021 beamtest
    if (june2022BT) detectorPos = {-7.006, -6.206, -0.470, 0.330, 5.870, 6.670}; // 0, 8.0, 65.36, 73.36, 128.76, 136.76 mm // June 2022 beamtest
    
    TFile *inFile = TFile::Open(inputFile.c_str(), "read");
    TTree *clustersTree = (TTree *) inFile->Get("sync_clusters_tree");
    
    std::vector<std::array<TBranch *, NBRANCH>> branches;
    std::array<TBranch *, NBRANCH> temp_arr;
    branches.assign(NLAD * NBOARD, temp_arr);
    
    const auto &nEntries = clustersTree->GetEntries();
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        vintegrals[i] = new vdouble;
        vcogs[i] = new vdouble;
        vsoverns[i] = new vdouble;
        vlengths[i] = new vint;
        vsovernPerChannel[i] = new std::vector<std::vector<double>>;
        vcogsPR[i] = new vdouble;
        vintegralsPR[i] = new vdouble;
        vsovernsPR[i] = new vdouble;
        vlengthsPR[i] = new vint;
        vsovernPerChannelPR[i] = new std::vector<std::vector<double>>;
        clustersTree->SetBranchAddress(Form("integral%lu", i), &vintegrals.at(i), &((branches.at(i)).at(0)));
        clustersTree->SetBranchAddress(Form("cog%lu", i), &vcogs.at(i), &((branches.at(i)).at(2)));
        clustersTree->SetBranchAddress(Form("sovern%lu", i), &vsoverns.at(i), &((branches.at(i)).at(3)));
        clustersTree->SetBranchAddress(Form("length%lu", i), &vlengths.at(i), &((branches.at(i)).at(1)));
        clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", i), &vsovernPerChannel.at(i), &((branches.at(i)).at(4)));
    }
    
    // branch check
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        for (uint br{0}; br < branches.at(i).size(); br++)
            cout << "ladder " << i << " branch " << br << " has name " << branches.at(i).at(br)->GetName() << endl;
    }
    
    TFile *outFile = TFile::Open(outputFile.c_str(), "recreate");
    TTree *tracksTree = new TTree("sync_clusters_tree", "Clusters after PR");
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        tracksTree->Branch(Form("integral%lu", i), vintegralsPR.at(i));
        tracksTree->Branch(Form("cog%lu", i), vcogsPR.at(i));
        tracksTree->Branch(Form("sovern%lu", i), vsovernsPR.at(i));
        tracksTree->Branch(Form("length%lu", i), vlengthsPR.at(i));
        tracksTree->Branch(Form("sovernPerChannel%lu", i), vsovernPerChannelPR.at(i));
    }
    
    ArtificialRetina *ar{nullptr};
    TH2D* h{nullptr};
    TCanvas* c{nullptr};
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        
        
        clustersTree->GetEntry(event);
        
        if (event % 100 == 0) std::cout << "Event: " << event << std::endl;
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            if (june2022BT && i % 2 == 0) {
                for (std::size_t j{0}; j < vcogs[i]->size(); ++j) vcogs[i]->at(j) = 2047 - vcogs[i]->at(j);
            }
        }
        
        ar = new ArtificialRetina(3.0,0.5);
        
        ar->FindTrackCand(vcogsPR, vintegralsPR, vlengthsPR, vsovernsPR, vsovernPerChannelPR, vcogs, vintegrals, vlengths, vsoverns, vsovernPerChannel, detectorPos);
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            if (june2022BT && i % 2 == 0) {
                for (std::size_t j{0}; j < vcogsPR[i]->size(); ++j) vcogsPR[i]->at(j) = 2047 - vcogsPR[i]->at(j);
            }
        }
        
        tracksTree->Fill();
        
        //c = new TCanvas();
        //h = new TH2D(*ar->getHistogram());
        //h->Draw("col");
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            vcogsPR.at(i)->clear();
            vcogsPR.at(i)->shrink_to_fit();
            vintegralsPR.at(i)->clear();
            vintegralsPR.at(i)->shrink_to_fit();
            vsovernsPR.at(i)->clear();
            vsovernsPR.at(i)->shrink_to_fit();
            vlengthsPR.at(i)->clear();
            vlengthsPR.at(i)->shrink_to_fit();
            vsovernPerChannelPR.at(i)->clear();
            vsovernPerChannelPR.at(i)->shrink_to_fit();
        }
        
        delete ar;
        ar = nullptr;
    }
    
    outFile->cd();
    tracksTree->Write();
    outFile->Close();
    
    inFile->Close();
    
}
