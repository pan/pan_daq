//
//  Detector.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 31.05.22.
//

#include "Detector.hpp"

namespace Boards {

unsigned int NLAD{0};
unsigned int fNTOTCH{0};

Detector::Detector() : fNVA{1}, fNVACH{1}, fNADC{1}, convertMode{0} {
    fNLCH = fNVA * fNVACH;
    fNADCCH = fNLCH / fNADC;
}

Detector::Detector(unsigned int nva, unsigned int nvach, unsigned int nadc, int mode) : fNVA{nva}, fNVACH{nvach}, fNADC{nadc}, convertMode{mode} {
    fNLCH = fNVA * fNVACH;
    fNADCCH = fNLCH / fNADC;
}

StripX::StripX() : Detector(32, 64, 8, 1) {
    NLAD++;
    fNTOTCH += fNLCH;
    fNBytes = 4096;
}

StripY::StripY() : Detector(1, 128, 1, 0) {
    NLAD++;
    fNTOTCH += fNLCH;
    fNBytes = 512;
}

}
