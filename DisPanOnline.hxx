#ifndef DISPANONLINE_H
#define DISPANONLINE_H

#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
// #include <RQ_OBJECT.h>
#include <TGTab.h>
//#include <TGFileDialog.h>
#include <TGTab.h>
#include <TFile.h>
#include <TText.h>
#include <TLine.h>
#include <TGraph.h>
#include <TBox.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGFrame.h>
#include <TGTextEdit.h>
#include <TGTextView.h>
#include <TAxis.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TArrow.h>
#include <TObject.h>
#include <TList.h>
#include <TTimer.h>
#include <TApplication.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TString.h>
#include <TGProgressBar.h>
#include <TCut.h>
#include <TDatime.h>
#include <TGButton.h>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <array>
#include <unordered_map>
#include <list>
#include <fftw3.h>
#include "TGProgressBar.h"
#include "TSpectrum.h"
#include "TVirtualFitter.h"
#include "TProfile.h"
#include "TMinuit.h"
#include "TPolyMarker.h"
#include "TGComboBox.h"
#include "TBrowser.h"
#include "TLatex.h"
#include "TPaveStats.h"
#include "TGNewFileDialog.hxx"
#include "GpioDaq/GpioDaq.hxx"
#include "FitUdpServer/FitUdpServer.hxx"
#include "Detector.hpp"

using namespace std;

constexpr int VERSION{2};
constexpr int RELEASE{9};
constexpr unsigned int NBRANCHES{7};

constexpr int NRUNCN{16};

const string ChanLabel[2] = {"channel", "strip"};

const int CONVERT[6] = {0, 1, 2, 3, 4, 5};
// const int CONVERT[1]={1};
// const int NCH=384;
// const int NLCH=384;
// const int NADCCH=384;
// const int NVA=6;
// const int NVACH=64;

const string ParamName[] = {
    "refcaldir=",
    "caldir=",
    "imagedir=",
    "filedir=",
    "boardconfigfile=",
    "refcalibfile_lad0=",
    "refcalibfile_lad1=",
    "refcalibfile_lad2=",
    "refcalibfile_lad3=",
    "refcalibfile_lad4=",
    "refcalibfile_lad5=",
    "redcalibfile_lad0=",
    "redcalibfile_lad1=",
    "redcalibfile_lad2=",
    "redcalibfile_lad3=",
    "redcalibfile_lad4=",
    "redcalibfile_lad5=",
};

typedef struct channel
{
  int Slot;
  double ADC;
  double CN;
  int Status;
  struct channel *next; // Next channel found =NULL if none
} channel;

typedef struct cluster
{
  int first;               // First strip of the cluster
  int length;              // Length of the cluster last=first+length
  float cog;               // Center of gravity
  int maxloc;              // Strip with highest signal
  float maxval;            // Highest signal
  float integral;          // Total charge collected
  float sovern1;           // S/N (integral/sqrt(sigma**2))
  float sovern2;           // S/N (integral/sigma mean)
  float sovern3;           // S/N (sqrt(sum((signal/sigma)**2))
  vector<double> sovernPerChannel; // S/N (signal/sigma) per channel within some area around the cluster and inside the cluster
  struct channel *channel; // Pointer to channel list
  struct cluster *next;    // Next cluster found =NULL if none
} cluster;

typedef unsigned int uint;
typedef vector<TH1F *> vTH1Fp;
typedef vector<double> vdouble;
typedef vector<vdouble> vvdouble;
typedef vector<int> vint;
typedef vector<TF1 *> vTF1p;
typedef vector<vTH1Fp> vvTH1Fp;
typedef vector<TH2F *> vTH2Fp;
typedef vector<TGraph *> vTGraphp;
typedef vector<string> vstring;

Double_t langaufun(Double_t *x, Double_t *par);
double CNFunY(double *x, double *par);
double GetStripYLength(double strip);

class MyMainFrame : public TGMainFrame
{
  // RQ_OBJECT("MyMainFrame")

private:
  unsigned int NLAD{1};
  unsigned int USB_BUFFER_SIZE{0}; // for single stripX 4128; for 2 stripXs 8224; for stripY 544; header and tail 32 bytes
  uint fNADC{8};
  // TGMainFrame *fMain;
  TGTab *fDataTab;
  TGHorizontalFrame *HframeConfig;
  TGCheckButton *TGCBshowrefped, *TGCBshowrefsig, *TGCBShowFFTNorm, *TGCBShowFFTSum, *TGCBdodynped, *TGCBshowProfileLenVsCog, *TGCBshowProfileIntVsCog, *TGCBshowOccCut, *TGCBshowCogCut, *TGCBshowLensCut, *TGCBshowLenVsCogCut, *TGCBshowIntVsCogCut, *TGCBshowIntsCut, *TGCBshowProfileSovern, *TGCBshowSovernCut;
  TGTextEntry *fTeFile, *fTeDAQFile, *fTeIPaddress, *fTeIPaddressData;
  TGTextView *TGTVlog;
  TGLabel *flabelconfigtext;
  TGComboBox *TGCBEventSel;
  TGCheckButton *TGCBDoCalib, *TGCBComputeCN, *TGCBNegativeSignals, *TGCBNegativeSignals1, *TGCBStripMode, *TGCBCorrectChannelNumbers, *TGCBSaveClusters, *TGCBCNPoly4Fit;
  TGNumberEntry *TGNEevent, *TGNEsuperimpose, *TGNEadcax[2][4], *TGNEpedsubtax[2][4], *TGNEpedax[2][4], *TGNEsrawax[2][4], *TGNEsigmaax[2][4], *TGNEpedcnsubtax[2][4], *TGNEpedcompax[2][4], *TGNEsigmacompax[2][4], *TGNEfftax[2][4], *TGNEChHiax[4], *TGNEChannel, *TGNEoccupancyax[2][4], *TGNEcogax[2][4], *TGNElensax[2][4], *TGNElenvscogax[2][4], *TGNEintvscogax[2][4], *TGNEintsax[2][4], *TGNEintsaxY[4], *TGNEintkax[4], *TGNEadcpedsubtax[4], *TGNEadcpedcnsubtax[4], *TGNEnclusax[2][4], *TGNEPhElCalax[4], *TGNEcncutval, *TGNEcnycutval, *TGNEexcludeevents, *TGNEcluHighThresh, *TGNEcluHighThreshY, *TGNEcluLowThresh, *TGNEminValidIntegral, *TGNEpixelSearchParam, *TGNEpixelSearchRebin, *TGNEsovernax[2][4];
  TGNumberEntry *TGNEFPSigma, *TGNEFPThreshold, *TGNEFPNIter, *TGNEFPAverWindow, *TGNEFPRangeMin, *TGNEFPRangeMax, *TGNBufferSize, *TGCalNBufferSize, *TGNEspreadCut, *TGNFrameSize, *TGNEPedestalsLowCut, *TGNEPedestalsHighCut, *TGNERawNoiseLowCut, *TGNERawNoiseHighCut, *TGNERawNoiseHighCutY, *TGNENoiseLowCut, *TGNENoiseHighCut, *TGNENoiseHighCutY, *TGNPort, *TGNPortData, *TGNEmaskStripX0, *TGNEmaskStripX1, *TGNEmaskStripY;
    TGListBox *fListBoxStripX0, *fListBoxStripX1, *fListBoxStripY;
  TRootEmbeddedCanvas *fEadccanvas, *fEpedsubtcanvas, *fEadcpedsubtcanvas, *fEadcpedcnsubtcanvas, *fEpedcanvas, *fEsrawcanvas, *fEsigmacanvas, *fEpedcnsubtcanvas, *fEpedcompcanvas, *fEsigmacompcanvas, *fEfftcanvas, *fEChHicanvas, *fEoccupancycanvas, *fEcogcanvas, *fElenscanvas, *fElenvscogcanvas, *fEintvscogcanvas, *fEintscanvas, *fEintkcanvas, *fEncluscanvas, *fEsoverncanvas, *fEcogxycanvas;
  TCanvas *fPedSubtCanvas, *fAdcPedSubtCanvas, *fAdcPedCnSubtCanvas, *fAdcCanvas, *fPedCanvas, *fSrawCanvas, *fSigmaCanvas, *fPedCnSubtCanvas, *fPedCompCanvas, *fSigmaCompCanvas, *fFftCanvas, *fChHiCanvas, *fOccupancyCanvas, *fCogCanvas, *fLensCanvas, *fLenVsCogCanvas, *fIntVsCogCanvas, *fIntsCanvas, *fNclusCanvas, *fPhElCalCanvas, *fSovernCanvas, *fCogxyCanvas;
    string fCurrentPath, fCalPath, fCalRefPath, fImagePath, fFilePath, fFilename, fFilenameWOPath, fPathOfFile, fFilenameWOExt, fConfigFilename, fDAQfileName{"/home/herduser/Data/tests/testdaq.daq"}, fIPaddress{"192.168.2.1"}, fIPaddressData{"192.168.2.12"};
  TPad *fPhElSubPad;
  int fCurrTab;
  TFile *fFile, *out_clustered_file;
  TTree *fTree, *clusters_tree{nullptr};
  std::unordered_map<int, vector<double>> integral_map, cog_map, sovern_map;
    std::unordered_map<int, std::vector<std::vector<double>>> sovernPerChannel_map;
  std::unordered_map<int, vector<int>> length_map;
    std::unordered_map<int, unsigned int> eventNumber_map;
    std::unordered_map<int, ULong64_t> frameCounter_map;
  std::vector<std::array<TBranch *, NBRANCHES>> branches;
  TBranch *fSilevents;
  TTimer *fRefreshtimer;
  TGTextButton *fTGTBStartDaq, *fTGTBStartrun, *fTGTBStoprun, *fTGTBContinuerun, *fTGTBSave, *fTGTBRecalib, *fTGTBSavePECal;
  // TGraph *gradc[NLAD], *grpedsubt[NLAD], *grpedcnsubt[NLAD], *grclusters[NLAD];
  vTGraphp vgradc, vgrpedsubt, vgrpedcnsubt, vgrPixelResultsSlope, vgrPixelResultsIntercept, vgrclusters, vgradcpedsubt, vgradcpedcnsubt;
  //TGraph *grADCpedsubt[2 * NADC], *grADCpedcnsubt[2 * NADC];
  // TGraph *grPixelResultsSlope[NLAD], *grPixelResultsIntercept[NLAD];
  // TH1F *hisFFT[NLAD][NADC], *hisSumFFT[NLAD][NADC], *hisFFTnorm[NLAD][NADC], *hisSumFFTnorm[NLAD][NADC], *hisOccupancy[NLAD], *hisOccupancyCut[NLAD], *hisCog[NLAD], *hisLens[NLAD], *hisIntS[NLAD], *hisCogCut[NLAD], *hisLensCut[NLAD], *hisIntSCut[NLAD], *hisNclus[NLAD];
  vvTH1Fp vhisFFT, vhisSumFFT, vhisFFTnorm, vhisSumFFTnorm;
  vTH1Fp vhisOccupancy, vhisOccupancyCut, vhisCog, vhisLens, vhisIntS, vhisCogCut, vhisLensCut, vhisIntSCut, vhisNclus;
  // vTH1Fp vHisChHi[NLAD], vHisChHiPe[NLAD];
  // TH2F *hisLenVsCog[NLAD], *hisLenVsCogCut[NLAD], *hisIntVsCog[NLAD], *hisIntVsCogCut[NLAD], *h2ChHi[NLAD], *h2ChHiPe[NLAD];
  vvTH1Fp vvHisChHi, vvHisChHiPe;
  vTH2Fp vh2ChHi, vh2ChHiPe, vh2LenVsCog, vh2LenVsCogCut, vh2IntVsCog, vh2IntVsCogCut, vh2Sovern, vh2SovernCut, vh2CogXY;
  TGHProgressBar *fTGHPBprogress;
  unsigned short fStrip[4608], fStripPrevious[4608]; // NCH-->4608
  unsigned int fEventNumberDAQ;
  ULong64_t fFramaCounterDAQ;
    int fNevents, fPortGPIO{11000}, fPortData{50003};
  // int fIndex[NLCH];
  vdouble fPedestals, fRawSigmas, fSigmas, fCN, fReadOut, fRunningCN;
  vdouble fRefPedestals, fRefRawSigmas, fRefSigmas;
  vdouble fRedPedestals, fRedRawSigmas, fRedSigmas;
  vdouble fvCalibSlope, fvCalibIntercept;
  vint fNst, fStatus, fRefStatus, fRedStatus;
  vvdouble fBufferRunningCN;

  TGLayoutHints *TGLHexpandX, *TGLHleftTop, *TGLHleft, *TGLHexpandXexpandY;

    std::vector<Boards::Detector*> vBoards;
    uint fNVA{0}, fNVACH, fNLCH, fNTOTCH, fNADCCH;
  int fSuperImposed;
  int fEventCounter;
  int fDisplayBusy;
  int fClearDisplay;
  int fChannel, fChannel_min, fChannel_max;
  double fAdcAxis[2][4], fPedSubtAxis[2][4], fAdcPedSubtAxis[4], fAdcPedCnSubtAxis[4], fCogxyAxis[4], fPedAxis[2][4], fSrawAxis[2][4], fSigmaAxis[2][4], fPedCnSubtAxis[2][4], fPedCompAxis[2][4], fSigmaCompAxis[2][4], fFftAxis[2][4], fOccupancyAxis[2][4], fCogAxis[2][4], fLensAxis[2][4], fLenVsCogAxis[2][4], fIntVsCogAxis[2][4], fIntsAxis[2][4], fIntsAxisY[4], fNclusAxis[2][4], fPhElCalAxis[4], fSovernAxis[2][4];
  vector<string> fvRefCalFile, fvRedCalFile;
  vector<string> fCanvasNames;
  vector<TCanvas *> fCanvasList;
  vdouble fSpectrum;
  double fRefPedAttenuation, fRefPedBaseline;
  double fCluHighThresh, fCluHighThreshY, fCluLowThresh, fMinInt, fMinValidIntegral, fPixelSearchParam, fPixelSearchRebin;
  unsigned int fMaskClu, fNBufferEvents{100};
  cluster *Scluster;
  cluster *Kcluster;
  int fWidth, fHeight;
  double CNcutval, CNYcutval;
  double ExcludeEvents;
  double fFindPeaksSigma, fFindPeaksThreshold, fFindPeaksRangeMin, fFindPeaksRangeMax;
  int fFindPeaksDeconIter, fFindPeaksAverWindow;
  // bool fDynPedActive;
  int fChanLabelMode; // 1=strip, 0=channel
  bool fFileOpened, fDoRecalibration{false};

  GpioDaq::UnigeGpioHerd *fUnigeGpio = nullptr;
  HerdFit::FitEvent *fFitEvent = nullptr;

  TThread *fThreadCommand = nullptr;
    TThread *fThreadReader = nullptr;
  TThread *fThreadBuffer = nullptr;
    TThread *fThreadBufferAdjust = nullptr;
  TThread *fThreadGetEvent = nullptr;

    uint fBufferPos, fBufferStatus, fBufferCounter, fLostCounter, fNstripX, fNstripY, fMaskStripX0{0}, fMaskStripX1{0}, fMaskStripY{0};
  vushort fvEvent;
  vushort fdecvEvent;
  bool fDaqRun{false};
  bool fQuitRun{false};
  unsigned int fPedCnt, fSrawCnt, fSigmaCnt, fCalCnt, fSkipCnt;
  unsigned int NPEDEVENTS{2048}, NSRAWEVENTS{2048}, NSIGEVENTS{2048};

  double fSpreadCut, fPedestalsLowCut, fPedestalsHighCut, fRawNoiseLowCut, fRawNoiseHighCut, fRawNoiseHighCutY, fNoiseLowCut, fNoiseHighCut, fNoiseHighCutY;

public:
  MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, uint nStripX = 2, uint nStripY = 1);
  virtual ~MyMainFrame();

  void LoadConfiguration();
  void SetConfiguration(string line);
  void SaveConfiguration();
  void ManageSelectedDataTab(int tabnum);
  void ChooseFile();
  void StartScope();
  void StopScope();
  void ContinueScope();
  void UpdateName();
  void Reconnect();
  void ChoseConfigFile();
  void UpdateDAQName();
  void AddAdcTab(TGCompositeFrame *cframe);
  void AddFftTab(TGCompositeFrame *cframe);
  void AddSovernTab(TGCompositeFrame *cframe);
  void AddOccupancyTab(TGCompositeFrame *cframe);
  void AddCogTab(TGCompositeFrame *cframe);
  void AddLensTab(TGCompositeFrame *cframe);
  void AddLenVsCogTab(TGCompositeFrame *cframe);
  void AddIntVsCogTab(TGCompositeFrame *cframe);
  void AddIntsTab(TGCompositeFrame *cframe);
  void AddIntkTab(TGCompositeFrame *cframe);
  void AddPedSubtTab(TGCompositeFrame *cframe);
  void AddPedCnSubtTab(TGCompositeFrame *cframe);
  void AddCogxyTab(TGCompositeFrame *cframe);

  // void AddPhotoElCalibTab(TGCompositeFrame *cframe);
  // void AddAdcPedSubtTab(TGCompositeFrame *cframe);
  // void AddAdcPedCnSubtTab(TGCompositeFrame *cframe);
  void AddSrawTab(TGCompositeFrame *cframe);
  void AddSigmaTab(TGCompositeFrame *cframe);
  void AddPedTab(TGCompositeFrame *cframe);
  void AddPedCompTab(TGCompositeFrame *cframe);
  void AddSigmaCompTab(TGCompositeFrame *cframe);
  void AddLogTab(TGCompositeFrame *cframe);
  void AddSettingsTab(TGCompositeFrame *cframe);
  void AddNclusTab(TGCompositeFrame *cframe);
  void LinesVas(uint lad, int drawline = 0);
  void LinesAdcs(int drawline = 0);
  void CloseWindow();
  int OpenFile();
  void AddLogMessage(string message);
  void InitGraphHis();
  void UpdateCluHighThresh();
  void UpdateCluHighThreshY();
  void UpdateCluLowThresh();
  void UpdateMinValidIntegral();
  void UpdateAdcDisplay(int event, int refreshaxes = 0);
  void UpdatePedSubtDisplay(int event, int refreshaxes = 0);
  void UpdateAdcPedCnSubtDisplay(int event, int refreshaxes = 0);
  void UpdatePedCnSubtDisplay(int event, int refreshaxes = 0);
  void UpdateFftDisplay(int refreshaxes = 0);
  void UpdateLenVsCogAxes(bool isy);
  void UpdateIntVsCogAxes(bool isy);
  void UpdatePixelSearchParam();
  void UpdatePixelSearchRebin();
  // void UpdateFindPeaksPar();
  // void UpdateChHiDisplay(int event, int refreshaxes=0);
  void UpdateSovernDisplay(int refreshaxes = 0);
  void UpdateOccupancyDisplay(int refreshaxes = 0);
  void UpdateCogDisplay(int refreshaxes = 0);
  void UpdateLensDisplay(int refreshaxes = 0);
  void UpdateLenVsCogDisplay(int refreshaxes = 0);
  void UpdateIntVsCogDisplay(int refreshaxes = 0);
  // void UpdateLenkDisplay(int event, int refreshaxes=0);
  void UpdateIntsDisplay(int refreshaxes = 0);
  // void UpdateIntkDisplay(int event, int refreshaxes=0);
  void BuildGraph();
  void ComputePedestals(int startevent = 0);
  bool ComputePedestals2(int startevent, double pedcut);
  bool ComputeOnlineCalibration(bool start);
  bool ComputeOnlinePedestals(bool start);
  bool ComputeOnlineSigma(bool start);
  bool ComputeOnlineRawSigma(bool start);
  void ComputeRawSigmas(int startevent = 0);
  void ComputeSigmas(int startevent = 0, int maskCN = 0xFF);
  // void ComputeChHi();
  void DisplayPedestals();
  // void DisplayChHi();
  void DisplayPedCompar();
  void DisplayRawSigmas();
  void DisplaySigmas();
  void DisplaySigmaCompar();
  void DisplaySovern();
  void DisplayOccupancy();
  void DisplayCog();
  void DisplayLens();
  void DisplayLenVsCog();
  void DisplayIntVsCog();
  void DisplayInts();
  void DisplayNclus();
  void InitRefreshTimer();
  void StartEventTrigger();
  void StopEventTrigger();
  void RefreshDisplay();
  void SetRunButtons(int startdaq, int start, int stop, int contin);
  void UpdateSuperImpose();
  void DrawGraph(TCanvas *c, int pad, int lad, vTGraphp vgr, string option = "l");
  void DrawGraphSuperimposed(TCanvas *c, int ipad, int lad, int event, vTGraphp vgr);
  void ClearSuperimposedGraphs(TCanvas *c, int event, int back);
  void InitAxes();
  void UpdateAdcAxes(bool isy);
  void UpdatePedSubtAxes(bool isy);
  //void UpdateAdcPedSubtAxes();
  //void UpdateAdcPedCnSubtAxes();
  void UpdatePedCnSubtAxes(bool isy);
  void UpdatePedAxes(bool isy);
  void UpdatePedCompAxes(bool isy);
  void UpdateSrawAxes(bool isy);
  void UpdateSigmaAxes(bool isy);
  void UpdateSigmaCompAxes(bool isy);
  void UpdateSovernAxes(bool isy);
  void UpdateOccupancyAxes(bool isy);
  void UpdateCogAxes(bool isy);
  void UpdateLensAxes(bool isy);
  // void UpdateLenkAxes();
  void UpdateIntsAxes(bool isy);
  //void UpdateIntsAxesY();
  // void UpdateIntkAxes();
  void UpdateNclusAxes(bool isy);
  void SaveCalibrations();
  int ComputeCN(double fMaj, int mask); // put 0xFF to take into account all bad channel flags, put 0 to ignore all of them
  void SubtractCN();
  void InitRefCalibrations();
  void InitRedCalibrations();
  vector<string> SplitString(string fullfilename, string separator);
  void LoadRefCalibrations(uint lad);
  void LoadRedCalibrations(uint lad);
  void InitVectors();
  void RemoveGraph(string name);
  void SaveImage();
  string GetDateTimeString();
  void DisplayThisEvent();
  void ComputeFFT();
  void DoFFT(double *array, int nech, double *his);
  void UpdateFftAxes(bool isy);
  void ConvertRefCalibrations();
  void FreeCluster(cluster **aCluster);
  void FreeChannel(channel **aChannel);
  void DoCluster(int ladder, char aSorK, cluster **aCluster);
  void InitReduction();
  void Reduction(bool dostats);
  void ListCluster(cluster *acluster, string mesg = "");
  void ListChannels(channel *achannel);
  void BuildGrCluster(cluster *acluster, TGraph *gr);
  void DrawHis(TCanvas *canvas, int pad, TH1 *his);
  void ResetGraph(TGraph *gr);
  void DrawClusterGraph(TCanvas *c, int ipad, int lad);
  vdouble BuildClusterStats(cluster *acluster, int ladder);
  // TF1 *peaks_finder(TH1F *h);
  // TF1 *FindPeaks(TH1F *h);
  void SetCNcut(double cut);
  void SetCNYcut(double cut);
  void SetExcludeEvents(double val);
  void UpdateCNcutval();
  void UpdateCNYcutval();
  void UpdateExcludeEvents();
  void ReCalibrate();
  void DynamicPedestals();
  void ResetHistos();
  void InitLayoutHints();

  void DoRunningCN();
  void InitRunningCN();
  bool TestFitSuccess(bool verbose = false);
  // void DarkCountFindOnePixel(double threshold);
  // void CalibrateChHi();
  void SavePECalibration();
  // void StartCalibrateChHi();
  double ConvertPe(double adc, int lad, int ch);
  TCanvas *GetCanvas(string name, double xmin, double ymin, double xmax, double ymax);
  unsigned short GetStripValue(int ch, int convertMode = 0);
  int ConvertDaqToRoot(long removebytes = 0);
  long GetFileSize(string filename);
  // void UpdateEventFilter();
  void ComputeReduction();
  unsigned short ConvertChannelToStrip(unsigned short ch);
  unsigned short ConvertStripToChannel(unsigned short ch);
  void UpdateChanLabel();
  int ConvertLad(int lad);
  void DivideCanvas(TCanvas *c);

  bool InitCommunication();
  void InitBuffer();
  void UpdateFrameSize();
  void UpdateIPaddress();
    void UpdatePort();
    void UpdateIPaddressData();
    void UpdatePortData();
  void UpdateBufferSize();
  void UpdatePedestalBuffer();

  void StartThreads();
  void CleanThreads();

  void CleanupCommunication();
  void CleanupBuffer();

  static void GetEvent(void *arg);
  bool GetQuitLoop() const { return fQuitRun; }
  int StartDaq();

  void SetChannelStatus(int ch, int value);

  void UpdateSpreadCut();
    void AddMaskedStripX0();
    void AddMaskedStripX1();
    void AddMaskedStripY();
    void HandleKeyPressListBox(Int_t wid, Int_t id);
    void UpdatePedestalsLowCut();
    void UpdatePedestalsHighCut();
    void UpdateRawNoiseLowCut();
    void UpdateRawNoiseHighCut();
    void UpdateRawNoiseHighCutY();
    void UpdateNoiseLowCut();
    void UpdateNoiseHighCut();
    void UpdateNoiseHighCutY();
    void AddChannelHistoTab(TGCompositeFrame *cframe);
    vector<unsigned int> ReadFourWords(FILE *afile);
    unsigned int ReadSingleWord(FILE *afile);
    int ConvertDaqToRootNew();
    bool ComputeCNoneVA(uint va, uint lad, double fMaj, int mask_channelOK);
    uint GetLadIndex(int ch);
    int GetLadNCH(uint lad);
    bool IsLadY(uint lad);
    void BuildAxisControls(TGHorizontalFrame *Hframe, TGNumberEntry *TGNEax[2][4], double fAxis[2][4], string FunConnect);
    void UpdateAxes(bool isy, TGNumberEntry *tgne[2][4], double (&fAxis)[2][4]);
    void CreateSquarePads(TCanvas *c, int nx, int ny);
    void SetSquareAspect(TVirtualPad *pad, int nx, int ny);
    void DisplayCogxy();
    ClassDef(MyMainFrame, 0);
    };
#endif
