#include "DisPanV2R9.hxx"
#include <fftw3.h>

const char *filetypes[] = {
    //  "All files",     "*",
    //"TRB calibration files", "*.cal",
    //"TRB calibration files", "*.txt",
    "ROOT files", "*.root",
    "DAQ files", "*.daq",
    //"bin threshold files",   "*.bin",
    //  "Text files",    "*.[tT][xX][tT]",
    0, 0};

//const intADCCORRECT=0;

MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, int nva, int nvach)
    : TGMainFrame(p, w, h)
{

    fWidth = w;
    fHeight = h;

    SetCleanup(kDeepCleanup);
    gStyle->SetLineScalePS(1);
    gStyle->SetPalette(53);
    gStyle->SetOptStat(111111);
    // getting the current directory
    char *path = getcwd(0, 0);
    fCurrentPath = path;
    fCalPath = fCurrentPath;
    fCalRefPath = fCurrentPath;
    fImagePath = fCurrentPath;
    fFilePath = fCurrentPath;
    fPathOfFile = fCurrentPath;
    fDisplayBusy = 0;
    fClearDisplay = 0;
    for (int lad = 0; lad < NLAD; lad++)
    {
        fRefCalFile[lad] = "";
        fRedCalFile[lad] = "";
    }

    fNVA = nva;             // number of ASICs in one ladder
    fNVACH = nvach;         // number of channels in one ASIC
    fNLCH = fNVA * fNVACH;  // total number of channels in one ladder
    fNTOTCH = fNLCH * NLAD; // total number of channels from the DAQ (e.g. more than one ladder in the DAQ)
    fNADCCH = fNLCH / NADC; // total number of channels per ADC

    fCluHighThresh = 7;
    fCluLowThresh = 2;

    fMinValidIntegral = 500;

    fPixelSearchParam = 10;
    fPixelSearchRebin = 3;

    fChanLabelMode = 1; // strip
    fFileOpened = false;

    InitLayoutHints();
    InitAxes();
    InitVectors();

    SetCNcut(7);
    SetExcludeEvents(10);
    //memset(&fStripPrevious,0,sizeof(fStripPrevious));

    TGVerticalFrame *Vframe = new TGVerticalFrame(this, 200, 40);
    AddFrame(Vframe, TGLHexpandXexpandY);

    fDataTab = new TGTab(Vframe, fWidth, fHeight);
    fDataTab->Connect("Selected(Int_t)", "MyMainFrame", this, "ManageSelectedDataTab(Int_t)");
    Vframe->AddFrame(fDataTab, TGLHexpandXexpandY);

    // ADC view (no ped subtraction)
    TGCompositeFrame *TGCFAdcView = fDataTab->AddTab("ADC");
    AddAdcTab(TGCFAdcView);

    // ped subtracted view
    TGCompositeFrame *TGCFPedSubtView = fDataTab->AddTab("Ped-subt");
    AddPedSubtTab(TGCFPedSubtView);

    // signal view
    TGCompositeFrame *TGCFPedCnSubtView = fDataTab->AddTab("CN-subt");
    AddPedCnSubtTab(TGCFPedCnSubtView);

    // adc view, ped subt tab
    //C TGCompositeFrame *TGCFAdcPedSubtView=fDataTab->AddTab("ADC ped-subt");
    //C AddAdcPedSubtTab(TGCFAdcPedSubtView);

    // adc view, ped-cn subt tab
    //C TGCompositeFrame *TGCFAdcPedCnSubtView=fDataTab->AddTab("ADC CN-subt");
    //C AddAdcPedCnSubtTab(TGCFAdcPedCnSubtView);

    // occupancy
    TGCompositeFrame *TGCFOccupancyView = fDataTab->AddTab("Occupancy");
    AddOccupancyTab(TGCFOccupancyView);

    // cog
    TGCompositeFrame *TGCFCogView = fDataTab->AddTab("Cog");
    AddCogTab(TGCFCogView);

    // lens
    TGCompositeFrame *TGCFLensView = fDataTab->AddTab("Len");
    AddLensTab(TGCFLensView);

    // len vs cog
    TGCompositeFrame *TGCFLenVsCogView = fDataTab->AddTab("Len vs cog");
    AddLenVsCogTab(TGCFLenVsCogView);

    // ints
    TGCompositeFrame *TGCFIntsView = fDataTab->AddTab("Int");
    AddIntsTab(TGCFIntsView);

    // int vs cog
    TGCompositeFrame *TGCFIntVsCogView = fDataTab->AddTab("Int vs cog");
    AddIntVsCogTab(TGCFIntVsCogView);

    // number of clusters
    TGCompositeFrame *TGCFNclusView = fDataTab->AddTab("Nclu S");
    AddNclusTab(TGCFNclusView);

    // intk
    //C TGCompositeFrame *TGCFIntkView=fDataTab->AddTab("Int K");
    //C AddIntkTab(TGCFIntkView);

    // FFT view (no ped subtraction)
    TGCompositeFrame *TGCFFftView = fDataTab->AddTab("FFT");
    AddFftTab(TGCFFftView);

    // calibratrion: pedestals
    TGCompositeFrame *TGCFPedView = fDataTab->AddTab("Pedestals");
    AddPedTab(TGCFPedView);

    // calibration: raw sigma
    TGCompositeFrame *TGCFSrawView = fDataTab->AddTab("Raw sigma");
    AddSrawTab(TGCFSrawView);

    // calibration: sigma
    TGCompositeFrame *TGCFSigmaView = fDataTab->AddTab("Sigma");
    AddSigmaTab(TGCFSigmaView);

    // calibratrion: pedestals compare
    TGCompositeFrame *TGCFPedCompView = fDataTab->AddTab("Delta Pedestals");
    AddPedCompTab(TGCFPedCompView);

    // calibratrion: sigmas compare
    TGCompositeFrame *TGCFSigmaCompView = fDataTab->AddTab("Sigma Compar");
    AddSigmaCompTab(TGCFSigmaCompView);

    // log
    TGCompositeFrame *TGCFLog = fDataTab->AddTab("Log");
    AddLogTab(TGCFLog);

    // settings
    TGCompositeFrame *TGCFSettings = fDataTab->AddTab("Settings");
    AddSettingsTab(TGCFSettings);

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGTextButton *loadfile = new TGTextButton(Hframe, "Choose file");
    loadfile->Connect("Clicked()", "MyMainFrame", this, "ChooseFile()");
    Hframe->AddFrame(loadfile, TGLHleft);

    fTeFile = new TGTextEntry(Hframe, "choose a file (.daq or .root) first");
    fTeFile->Connect("ReturnPressed()", "MyMainFrame", this, "UpdateName()");
    //fTeFile->SetBackgroundColor(0x00ff00);
    Hframe->AddFrame(fTeFile, TGLHexpandX);

    Vframe->AddFrame(Hframe, TGLHexpandX);

    TGHorizontalFrame *Hframe2 = new TGHorizontalFrame(Vframe, 200, 40);
    fTGTBStartrun = new TGTextButton(Hframe2, "Start");
    fTGTBStartrun->Connect("Clicked()", "MyMainFrame", this, "StartScope()");
    Hframe2->AddFrame(fTGTBStartrun, TGLHleft);

    fTGTBStoprun = new TGTextButton(Hframe2, "Stop");
    fTGTBStoprun->Connect("Clicked()", "MyMainFrame", this, "StopScope()");
    Hframe2->AddFrame(fTGTBStoprun, TGLHleft);

    fTGTBContinuerun = new TGTextButton(Hframe2, "Continue");
    fTGTBContinuerun->Connect("Clicked()", "MyMainFrame", this, "ContinueScope()");
    Hframe2->AddFrame(fTGTBContinuerun, TGLHleft);

    TGLabel *labelevent = new TGLabel(Hframe2, "event:");
    Hframe2->AddFrame(labelevent, TGLHleft);

    TGNEevent = new TGNumberEntry(Hframe2, 0);
    TGNEevent->Connect("ValueSet(Long_t)", "MyMainFrame", this, "DisplayThisEvent()");
    Hframe2->AddFrame(TGNEevent, TGLHleft);

    TGLabel *labeldebug{new TGLabel(Hframe2, "Event select:")};
    Hframe2->AddFrame(labeldebug, TGLHleft);

    TGCBEventSel = new TGComboBox(Hframe2);
    TGCBEventSel->AddEntry("No filter", 0);
    TGCBEventSel->AddEntry("Even entries", 1);
    TGCBEventSel->AddEntry("Odd entries", 2);
    TGCBEventSel->Resize(100, 20);
    TGCBEventSel->Select(0);
    //TGCBEventSel->Connect("Selected(Int_t)", "MyMainFrame", this, "UpdateEventFilter()");
    Hframe2->AddFrame(TGCBEventSel, TGLHleft);

    TGLabel *labelsuper = new TGLabel(Hframe2, "superimpose:");
    Hframe2->AddFrame(labelsuper, TGLHleft);

    TGNEsuperimpose = new TGNumberEntry(Hframe2, 0);
    TGNEsuperimpose->SetNumStyle(TGNumberFormat::kNESInteger);
    TGNEsuperimpose->SetLimits(TGNumberFormat::kNELLimitMinMax, 0, 500);

    TGNEsuperimpose->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateSuperImpose()");
    Hframe2->AddFrame(TGNEsuperimpose, TGLHleft);

    fTGTBSave = new TGTextButton(Hframe2, "Save Image");
    fTGTBSave->Connect("Clicked()", "MyMainFrame", this, "SaveImage()");
    Hframe2->AddFrame(fTGTBSave, TGLHleft);

    fTGTBRecalib = new TGTextButton(Hframe2, "Recalibrate");
    fTGTBRecalib->Connect("Clicked()", "MyMainFrame", this, "ReCalibrate()");
    Hframe2->AddFrame(fTGTBRecalib, TGLHleft);

    //fTGTBChannelCal = new TGTextButton(Hframe2, "Chan. cal");
    //fTGTBChannelCal->Connect("Clicked()", "MyMainFrame", this, "StartCalibrateChHi()");
    //Hframe2->AddFrame(fTGTBChannelCal, TGLHleft);

    Vframe->AddFrame(Hframe2, TGLHexpandX);

    TGHorizontalFrame *Hframe3 = new TGHorizontalFrame(Vframe, 200, 40); // for the checkboxes

    TGCBDoCalib = new TGCheckButton(Hframe3, "Do initial calibration");
    TGCBDoCalib->SetDown();
    Hframe3->AddFrame(TGCBDoCalib, TGLHleft);

    TGCBComputeCN = new TGCheckButton(Hframe3, "CN calculation");
    TGCBComputeCN->SetDown(kTRUE);
    Hframe3->AddFrame(TGCBComputeCN, TGLHleft);

    TGCBdodynped = new TGCheckButton(Hframe3, "Dynamic pedestals");
    TGCBdodynped->SetDown(0); // dynped inactive by default
    Hframe3->AddFrame(TGCBdodynped, TGLHleft);

    Vframe->AddFrame(Hframe3, TGLHexpandX);

    fTGHPBprogress = new TGHProgressBar(Vframe, TGProgressBar::kStandard, 300);
    //fTGHPBprogress->ShowPosition();
    fTGHPBprogress->SetBarColor("lightblue");
    fTGHPBprogress->SetRange(0, 100);
    fTGHPBprogress->SetPosition(0);

    Vframe->AddFrame(fTGHPBprogress, TGLHexpandX);

    fCurrTab = 0;
    fDataTab->SetTab(fCurrTab);
    TGTabElement *tge = fDataTab->GetCurrentTab();
    tge->SetBackgroundColor(0x00ff00);
    gClient->NeedRedraw(tge);

    SetWindowName(Form("PAN Strip display v%dr%d", VERSION, RELEASE));
    // Map all subwindows of main frame
    MapSubwindows();
    // Initialize the layout algorithm
    Resize(GetDefaultSize());
    // Map main frame
    MapWindow();

    InitGraphHis();
    fSuperImposed = 0;
    InitRefreshTimer();
    SetRunButtons(1, 0, 0);
    AddLogMessage("Welcome !");
    LoadConfiguration();
    fPathOfFile = fFilePath;
    InitRefCalibrations();
    InitReduction();
}

MyMainFrame::~MyMainFrame()
{
    // Clean up used widgets: frames, buttons, layout hints

    Cleanup();
}

void MyMainFrame::InitLayoutHints()
{

    TGLHexpandX = new TGLayoutHints(kLHintsExpandX, 5, 5, 3, 4);
    TGLHleftTop = new TGLayoutHints(kLHintsLeft | kLHintsTop, 10, 10, 10, 1);
    TGLHleft = new TGLayoutHints(kLHintsLeft, 5, 5, 3, 4);
    TGLHexpandXexpandY = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 10, 10, 10, 1);
}

void MyMainFrame::InitReduction()
{

    Scluster = 0; // mandatory to initialize the pointer to zero, else, you will have problems...
    Kcluster = 0;

    // initisalize the cluster options
    //fCut1=3.5;
    //fCut2=1.5;
    //fCutHighThresh=100;
    //fCutLowThresh=50;
    fMaskClu = 0xFFFFFFFF;
    fMinInt = 0;
}

void MyMainFrame::InitVectors()
{

    fPedestals.clear();
    fPedestals.assign(fNTOTCH, 0);
    fRawSigmas.clear();
    fRawSigmas.assign(fNTOTCH, 0);
    fSigmas.clear();
    fSigmas.assign(fNTOTCH, 0);
    fRefPedestals.clear();
    fRefPedestals.assign(fNTOTCH, 0);
    fRefRawSigmas.clear();
    fRefRawSigmas.assign(fNTOTCH, 0);
    fRefSigmas.clear();
    fRefSigmas.assign(fNTOTCH, 0);

    fvCalibSlope.assign(fNLCH, 1);
    fvCalibIntercept.assign(fNLCH, 0);
}

void MyMainFrame::SetRunButtons(int start, int stop, int contin)
{

    fTGTBStartrun->SetEnabled((start) ? kTRUE : kFALSE);
    fTGTBStoprun->SetEnabled((stop) ? kTRUE : kFALSE);
    fTGTBContinuerun->SetEnabled((contin) ? kTRUE : kFALSE);
}

void MyMainFrame::InitAxes()
{
    fAdcAxis[0] = 0;
    fAdcAxis[1] = fNLCH;
    fAdcAxis[2] = 0;
    fAdcAxis[3] = 1000;

    fPedSubtAxis[0] = 0;
    fPedSubtAxis[1] = fNLCH;
    fPedSubtAxis[2] = -100;
    fPedSubtAxis[3] = 100;

    fAdcPedSubtAxis[0] = 0;
    fAdcPedSubtAxis[1] = fNLCH;
    fAdcPedSubtAxis[2] = -100;
    fAdcPedSubtAxis[3] = 4096;

    fAdcPedCnSubtAxis[0] = 0;
    fAdcPedCnSubtAxis[1] = fNLCH;
    fAdcPedCnSubtAxis[2] = -100;
    fAdcPedCnSubtAxis[3] = 4096;

    fPedCnSubtAxis[0] = 0;
    fPedCnSubtAxis[1] = fNLCH;
    fPedCnSubtAxis[2] = -20;
    fPedCnSubtAxis[3] = 50;

    fPedAxis[0] = 0;
    fPedAxis[1] = fNLCH;
    fPedAxis[2] = 0;
    fPedAxis[3] = 1000;

    fSrawAxis[0] = 0;
    fSrawAxis[1] = fNLCH;
    fSrawAxis[2] = 0;
    fSrawAxis[3] = 50;

    fSigmaAxis[0] = 0;
    fSigmaAxis[1] = fNLCH;
    fSigmaAxis[2] = 0;
    fSigmaAxis[3] = 20;

    fPedCompAxis[0] = 0;
    fPedCompAxis[1] = fNLCH;
    fPedCompAxis[2] = -50;
    fPedCompAxis[3] = 1000;

    fSigmaCompAxis[0] = 0;
    fSigmaCompAxis[1] = fNLCH;
    fSigmaCompAxis[2] = -50;
    fSigmaCompAxis[3] = 500;

    fFftAxis[0] = 0;
    fFftAxis[1] = fNLCH;
    fFftAxis[2] = 0;
    fFftAxis[3] = 100;

    fChHiAxis[0] = -100;
    fChHiAxis[1] = 4096; //C maximum value of ADC
    fChHiAxis[2] = 1;
    fChHiAxis[3] = 1000000; //C number of events

    fPhElCalAxis[0] = 0;
    fPhElCalAxis[1] = 384;
    fPhElCalAxis[2] = 0;
    fPhElCalAxis[3] = 500;

    fChannel = 0;
    fChannel_min = 0;
    fChannel_max = 127;

    fOccupancyAxis[0] = 0;
    fOccupancyAxis[1] = fNLCH;
    fOccupancyAxis[2] = 0;
    fOccupancyAxis[3] = 100;

    fCogAxis[0] = 0;
    fCogAxis[1] = fNLCH;
    fCogAxis[2] = 0;
    fCogAxis[3] = 20;

    fLensAxis[0] = 0;
    fLensAxis[1] = 40;
    fLensAxis[2] = 0;
    fLensAxis[3] = 1000;

    fLenVsCogAxis[0] = 0;
    fLenVsCogAxis[1] = fNLCH;
    fLenVsCogAxis[2] = 0;
    fLenVsCogAxis[3] = 20;

    fIntVsCogAxis[0] = 0;
    fIntVsCogAxis[1] = fNLCH;
    fIntVsCogAxis[2] = 0;
    fIntVsCogAxis[3] = 1000;

    fIntsAxis[0] = 0;
    fIntsAxis[1] = 200;
    fIntsAxis[2] = 1;
    fIntsAxis[3] = 400;

    fIntkAxis[0] = 0;
    fIntkAxis[1] = 100;
    fIntkAxis[2] = 0;
    fIntkAxis[3] = 100;

    fNclusAxis[0] = 0;
    fNclusAxis[1] = 50;
    fNclusAxis[2] = 1;
    fNclusAxis[3] = 1000000;
}

vector<string> MyMainFrame::SplitString(string fullfilename, string separator)
{

    size_t found = fullfilename.find_last_of(separator);

    vector<string> pathinfo;

    pathinfo.push_back(fullfilename.substr(0, found));
    pathinfo.push_back(fullfilename.substr(found + 1));

    return pathinfo;
}

void MyMainFrame::LoadConfiguration()
{

    string confname = gSystem->HomeDirectory();
    confname += "/.DisPan";

    ifstream confFile(confname);
    if (!confFile.is_open())
    {
        AddLogMessage("No configuration file available, a new one will be saved in your home directory when you will exit from DisPan.");
        //cout << "no configuration file available" << endl;
        return;
    }

    string line;
    while (1)
    {
        if (!(getline(confFile, line)))
            break;
        SetConfiguration(line);
    }

    confFile.close();
}

void MyMainFrame::SetConfiguration(string line)
{

    string parnam;

    parnam = ParamName[0];
    if (line.find(parnam) == 0)
    {
        fCalRefPath = line.substr(parnam.size(), line.size() - parnam.size());
        //cout << "fImagePath=" << fImagePath << endl;
        AddLogMessage("fCalRefPath=" + fCalRefPath);
        return;
    }

    parnam = ParamName[1];
    if (line.find(parnam) == 0)
    {
        fCalPath = line.substr(parnam.size(), line.size() - parnam.size());
        //cout << "fImagePath=" << fImagePath << endl;
        AddLogMessage("fCalPath=" + fCalPath);
        return;
    }

    parnam = ParamName[2];
    if (line.find(parnam) == 0)
    {
        fImagePath = line.substr(parnam.size(), line.size() - parnam.size());
        //cout << "fImagePath=" << fImagePath << endl;
        AddLogMessage("fImagePath=" + fImagePath);
        return;
    }

    parnam = ParamName[3];
    if (line.find(parnam) == 0)
    {
        fFilePath = line.substr(parnam.size(), line.size() - parnam.size());
        //cout << "fImagePath=" << fImagePath << endl;
        AddLogMessage("fFilePath=" + fFilePath);
        return;
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        parnam = ParamName[4 + lad];
        if (line.find(parnam) == 0)
        {
            fRefCalFile[lad] = line.substr(parnam.size(), line.size() - parnam.size());
            //cout << "fImagePath=" << fImagePath << endl;
            AddLogMessage(Form("fRefCalFile_%d=", lad) + fRefCalFile[lad]);
            return;
        }
    }

    int start = 4 + NLAD;

    for (int lad = 0; lad < NLAD; lad++)
    {
        parnam = ParamName[start + lad];
        if (line.find(parnam) == 0)
        {
            fRedCalFile[lad] = line.substr(parnam.size(), line.size() - parnam.size());
            //cout << "fImagePath=" << fImagePath << endl;
            AddLogMessage(Form("fRedCalFile_%d=", lad) + fRedCalFile[lad]);
            return;
        }
    }
}

void MyMainFrame::SaveConfiguration()
{

    string confname = gSystem->HomeDirectory();
    confname += "/.DisPan";

    ofstream confFile(confname);
    if (!confFile.is_open())
    {
        AddLogMessage("File problem, configuration file cannot be saved.");
        return;
    }

    string line;
    line = ParamName[0] + fCalRefPath;
    confFile << line << endl;

    line = ParamName[1] + fCalPath;
    confFile << line << endl;

    line = ParamName[2] + fImagePath;
    confFile << line << endl;

    line = ParamName[3] + fFilePath;
    confFile << line << endl;

    for (int lad = 0; lad < NLAD; lad++)
    {
        line = ParamName[4 + lad] + fRefCalFile[lad];
        confFile << line << endl;
    }

    int start = 4 + NLAD;

    for (int lad = 0; lad < NLAD; lad++)
    {
        line = ParamName[start + lad] + fRedCalFile[lad];
        confFile << line << endl;
    }

    confFile.close();
}

void MyMainFrame::ManageSelectedDataTab(int tabnum)
{

    TGTabElement *tge = 0;

    int oldtab = fCurrTab;

    fCurrTab = tabnum;

    tge = fDataTab->GetTabTab(fCurrTab);
    int normalcol = tge->GetBackground();

    tge->SetBackgroundColor(0x00ff00);
    gClient->NeedRedraw(tge);

    tge = fDataTab->GetTabTab(oldtab);
    tge->SetBackgroundColor(normalcol);

    //cout << "before needredraw" << endl;
    gClient->NeedRedraw(tge);
}

void MyMainFrame::ChooseFile()
{

    //static TString dir(".");
    TString dir(fPathOfFile);
    TGFileInfo fi;
    fi.fFileTypes = filetypes;
    fi.fIniDir = StrDup(dir);
    //fi.fIniDir    = StrDup("/Users/cperrina/fibres2016/test_beam_october_2017/daq/root/");
    //fi.fFilename = StrDup("filenotchosen");//sprintf(fi.fFilename,"filenotchosen");
    //printf("fIniDir = %s\n", fi.fIniDir);

    cout << "fIniDir = " << fi.fIniDir << endl;

    new TGFileDialog(fClient->GetRoot(), this, kFDOpen, &fi);

    if (fi.fFilename == 0)
        return;

    fFilename = fi.fFilename;

    fTeFile->SetText(fFilename.c_str());

    fFilenameWOPath = fFilename.substr(1 + fFilename.find_last_of("/"));
    fPathOfFile = fFilename.substr(0, fFilename.find_last_of("/"));
    fFilenameWOExt = fFilenameWOPath.substr(0, fFilenameWOPath.find_last_of("."));

    cout << fFilenameWOPath << "  " << fPathOfFile << "  " << fFilenameWOExt << endl;
    if (OpenFile())
    {
        AddLogMessage(Form("Error with file %s", fFilename.c_str()));
        fTeFile->SetBackgroundColor(0xfecccd);
    }
    else
    {
        AddLogMessage(Form("File %s is successfully open", fFilename.c_str()));
        fTeFile->SetBackgroundColor(0xcdffcf);
    }
    //else TGTVlog->Clear();
    //StartScope();
}

int MyMainFrame::ConvertDaqToRoot(long removebytes)
{

    string inputFileName{fFilename};
    string outputFileName{fFilename.substr(0, fFilename.length() - 4)};
    outputFileName += (removebytes) ? "_repaired.root" : ".root";

    TFile *out_file = new TFile(outputFileName.c_str(), "recreate");

    // ushort adc[fNTOTCH], padc[fNTOTCH];
    unsigned char trigger, ev_id;
    unsigned short adcArray[fNTOTCH];
    unsigned char val1, val2, dummy; // bytes

    TTree *T = new TTree("T", "T");
    T->Branch("adc", adcArray, Form("adc[%d]/s", fNTOTCH));

    FILE *file = fopen(inputFileName.c_str(), "rb");

    if (file == 0)
    {
        AddLogMessage(Form("Input file %s not found, stop.", inputFileName.c_str()));
        return 1;
    }
    // trick to manage files with wrong size (file still might be corrupted though.... --> careful look at the data should be necessary in that case !)
    for (int i{0}; i < removebytes; i++)
        fread(&dummy, sizeof(dummy), 1, file);

    int channel{0};
    int counter{0};
    int event{0};
    int pedcounter{0};
    do
    {
        if (feof(file))
            break;

        fread(&val1, sizeof(val1), 1, file);
        fread(&val2, sizeof(val2), 1, file);
        unsigned short adcValue{val1};
        adcValue += val2 << 8;
        //std::cout << Form("0x%02x 0x%02x 0x%04x %4d", val1, val2, adcValue, adcValue) << endl;

        if (adcValue == 0xCAFE or adcValue == 0xDECA)
            break;

        int AdcIndex{counter % NADC};
        int ChannelIndex{counter / NADC};
        channel = (AdcIndex)*fNADCCH + ChannelIndex;
        //std::cout << "event = " << event << "   counter = " << counter << "    channel = " << channel << endl;

        //vh_adc.at(AdcIndex)->Fill(adcValue);
        adcArray[channel] = adcValue;
        //h_adc2->Fill(adc2);
        //h_adc1->Fill(adc1);

        // cout << hex << "0x" << val << endl ;
        // cout << dec << (int)trigger << " ";
        // cout << dec << (int)ev_id << " ";
        // cout << dec << adc2 << " ";
        // cout << dec << adc1 << endl;

        counter++;

        if (channel == fNTOTCH - 1)
        {

            T->Fill();
            memset(&adcArray, 0, sizeof(adcArray));
            event++;
            counter = 0;
        }

    } while (1);

    T->Write(0, TObject::kOverwrite);
    AddLogMessage(Form("We have found %lld events\n", T->GetEntries()));
    out_file->Write(0, TObject::kOverwrite);

    delete out_file;

    return 0;
}

int MyMainFrame::OpenFile()
{
    fFileOpened = false;
    string message = "";

    string filetype{fFilename.substr(fFilename.length() - 3)};
    if (filetype == "daq")
    {
        int ret{1};
        long fsize{GetFileSize(fFilename)}; // in bytes
        std::cout << "file size is : " << fsize << std::endl;
        long deltasize{(fsize - 4) % (4 * 64 * 8 * 2)};
        if (deltasize)
            std::cout << "look out ! deltasize=" << deltasize << std::endl;
        // (fileSizeInWords - 2) % (4*64*8)
        ret = ConvertDaqToRoot(deltasize);

        if (ret)
            return 10;

        string gen{fFilename.substr(0, fFilename.length() - 4)};
        fFilename = gen + ".root";
    }

    fFile = new TFile(fFilename.c_str());
    if (!fFile->IsOpen())
    {
        //cout << "problem with file " << fFilename << endl;
        message = "Problem opening file " + fFilename;
        AddLogMessage(message);
        return 1;
    }

    fFile->GetObject("T", fTree);

    if (fTree == 0)
    {
        AddLogMessage("Problem with tree pointer");
        return 2;
    }

    //C fSilevents=fTree->GetBranch("strip[4608]");
    fSilevents = fTree->GetBranch("adc");
    //cout << "silevents: " << silevents << endl;

    if (fSilevents == 0)
    {
        AddLogMessage("data branch not found");
        return 3;
    }

    fSilevents->SetAddress(&fStrip);

    fNevents = fTree->GetEntries();
    AddLogMessage(Form("File %s opened successfully", fFilename.c_str()));
    AddLogMessage(Form("%d entries found in tree", fNevents));
    fFileOpened = true;
    return 0;
}

void MyMainFrame::StartScope()
{
    if (!fFileOpened)
        return;

    AddLogMessage("Starting run");
    InitRunningCN();
    // we need to clear all the root objects

    if (TGCBDoCalib->IsDown())
    {
        ComputePedestals(ExcludeEvents);
        //cout << "1: ComputePedestals2 100" << endl;
        //ComputePedestals2(0, 100);
        //cout << "2: ComputePedestals2 50" << endl;
        //ComputePedestals2(0, 50);
        //cout << "3: ComputePedestals2 25" << endl;
        //ComputePedestals2(0, 25);
        /*
        bool cp2res{false};
        cp2res = ComputePedestals2(0, 100);
        if (cp2res)
        {
            cout << "ComputePedestals2(0, 100) : ok" << endl;
            cp2res = ComputePedestals2(0, 50);
            if (cp2res)
            {
                cout << "ComputePedestals2(0, 50) : ok" << endl;
                cp2res = ComputePedestals2(0, 50);
                if (cp2res)
                    cout << "ComputePedestals2(0, 25) : ok" << endl;
            }
        }
        */
        cout << "4: ComputeRawSigmas" << endl;
        ComputeRawSigmas(ExcludeEvents);
        cout << "5: ComputeSigmas" << endl;
        ComputeSigmas(ExcludeEvents);
        cout << "6: SaveCalibrations" << endl;
        SaveCalibrations();
        cout << "7" << endl;
    }
    ResetHistos();
    cout << "8: ComputeReduction" << endl;
    //ComputeChHi();
    ComputeReduction();
    cout << "9" << endl;
    //BuildGraph(); //cc
    DisplayPedestals();
    DisplayRawSigmas();
    DisplaySigmas();
    DisplayPedCompar();
    DisplaySigmaCompar();

    DisplayOccupancy();
    DisplayCog();
    DisplayLens();
    DisplayLenVsCog();
    DisplayInts();
    DisplayIntVsCog();
    DisplayNclus();

    fEventCounter = 0;

    StartEventTrigger();

    // for (int ch=0; ch<fNTOTCH; ch++) {
    //   int lad=ch/NLCH;
    //   hisChHi[lad][ch]->Reset();
    // }

    SetRunButtons(0, 1, 0);
}

void MyMainFrame::StopScope()
{

    AddLogMessage("Stopping run");
    StopEventTrigger();

    SetRunButtons(1, 0, 1);
}

void MyMainFrame::ContinueScope()
{

    AddLogMessage("Continuing run");
    StartEventTrigger();

    SetRunButtons(0, 1, 0);
}

TCanvas *MyMainFrame::GetCanvas(string name, double xmin, double ymin, double xmax, double ymax)
{
    TCanvas *c = (TCanvas *)gROOT->GetListOfCanvases()->FindObject(name.c_str());
    if (c == 0)
        c = new TCanvas(name.c_str(), name.c_str(), xmin, ymin, xmax, ymax);

    c->cd();
    return c;
}

bool MyMainFrame::TestFitSuccess(bool verbose)
{ // from https://root-forum.cern.ch/t/reading-out-fit-status/19480/11
    // well, this does not seem to work, in the end...
    string minuitstatus{string(gMinuit->fCstatu)};
    if (verbose)
        cout << minuitstatus << endl;
    if (verbose)
        cout << "fempty = " << gMinuit->fEmpty << endl;
    if (minuitstatus.compare("CONVERGED ") != 0 && minuitstatus.compare("OK        ") != 0) //the spaces are important
    {
        if (verbose)
            cout << "  Minimization did not converge! (status_\"" << minuitstatus << "\")" << std::endl;
        return false;
    }
    else
        return true;
}

void MyMainFrame::BuildGraph()
{

    fReadOut.clear();
    fReadOut.assign(fNTOTCH, 0);
    fSpectrum.clear();
    fSpectrum.assign(fNTOTCH, 0);

    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        //cout << "event " << i << "  channel " << ch << "  " << strip[ch] << endl;
        int lad = ch / fNLCH;
        int ladch = ch % fNLCH;
        int adc = ch / fNADCCH;
        //int adc8=adc%(NADC*2); // total number of ADCs is 8.
        //int cycle=adc/(NADC*2); // three reading cycles.
        int adcch = ch % fNADCCH; // channel index among a single ADC

        //if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;

        gradc[lad]->SetPoint(ladch, ladch, GetStripValue(ch, fChanLabelMode));

        //C cout<<"ch = "<<ch<<", fStrip[ch] = "<<fStrip[ch]<<endl;

        fReadOut.at(ch) = GetStripValue(ch, fChanLabelMode) - fPedestals.at(ch);
        grpedsubt[lad]->SetPoint(ladch, ladch, fReadOut.at(ch));
        //grADCpedsubt[adc8]->SetPoint(adcch+cycle*fNADCCH, adcch+cycle*fNADCCH, fReadOut.at(ch));
        //fReadOut.at(ch)=fStrip[ch]-fStrip[ch%fNADCCH+2*fNADCCH]; //trick to subtract signals of 1 ADC K of 1st ladder
        //fReadOut.at(ch)=fStrip[ch]-fStripPrevious[ch%fNADCCH+2*fNADCCH]; //trick to subtract signals of 1 ADC K of 1st ladder
        //grpedsubt[lad]->SetPoint(ladch,ladch,fReadOut.at(ch));

        //att
        // hisChHi[lad][ch]->Fill(fReadOut.at(ch));
        // hisChHi[lad][ch]->SetStats(kTRUE);
    }
    //memcpy(&fStripPrevious,&fStrip,sizeof(fStrip));
    //ComputeCN();
    int success{ComputeCN(0.6)};
    if (!success)
    {
        success = ComputeCN(0.4);
        if (!success)
            success = ComputeCN(0.2);
    }
    SubtractCN();
    DoRunningCN();

    if (TGCBdodynped->IsDown())
        DynamicPedestals();
    // for (int ch=0; ch<fNTOTCH; ch++) {
    //   int adc=ch/fNADCCH;
    //   //int adc8=adc%(NADC*2); // total number of ADCs is 8.
    //   //int cycle=adc/(NADC*2); // three reading cycles.
    //   int adcch=ch%fNADCCH; // channel index among a single ADC
    //   //grADCpedcnsubt[adc8]->SetPoint(adcch+cycle*fNADCCH, adcch+cycle*fNADCCH, fReadOut.at(ch));
    //}

    //Reduction();
    int sign{TGCBNegativeSignals->IsDown() ? -1 : 1};
    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        int lad = ch / fNLCH;
        int ladch = ch % fNLCH;

        grpedcnsubt[lad]->SetPoint(ladch, ladch, sign * fReadOut.at(ch));
        //hisFFT[lad]->SetBinContent(ladch+1, fSpectrum.at(ch));
        //cout << ch << "   " << fSpectrum.at(ch) << endl;
        //hisFFT[lad]->SetBinContent(ladch+1, 10);
    }

    //ComputeFFT(); // 20210603 Deactivated temporarily
}

void MyMainFrame::UpdateAdcDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateAdcDisplay()
    }
    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (int lad = 0; lad < NLAD; lad++)
        {
            fAdcCanvas->cd(CONVERT[lad]);
            //fAdcCanvas->cd(1);
            TH1F *frame = gPad->DrawFrame(fAdcAxis[0], fAdcAxis[2], fAdcAxis[1], fAdcAxis[3]);
            frame->SetTitle(Form("ADC - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
            LinesVas();
        }
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        //TVirtualPad *pad=fAdcCanvas->cd(CONVERT[lad]);
        if (!fSuperImposed)
        {
            //C cout<<"Nous sommes avant DrawGraph !!"<<endl;
            DrawGraph(fAdcCanvas, CONVERT[lad], lad, gradc);
        }
        else
        {
            DrawGraphSuperimposed(fAdcCanvas, CONVERT[lad], lad, event, gradc);
        }
    }
    fAdcCanvas->Update();

    fDisplayBusy = 0;
}

// void MyMainFrame::UpdateOccupancyDisplay(int event, int refreshaxes) {

//   if (fDisplayBusy) {
//     cout << "busy" << endl;
//     return; // in case of multiple calls of UpdateOccupancyDisplay()
//   }
//   fDisplayBusy=1;

//   if (refreshaxes) {
//     for (int lad=0; lad<NLAD; lad++) {
//       fOccupancyCanvas->cd(CONVERT[lad]);
//       TH1F *frame=gPad->DrawFrame(fOccupancyAxis[0],fOccupancyAxis[2],fOccupancyAxis[1],fOccupancyAxis[3]);
//       frame->SetTitle(Form("Occupancy - Ladder %d; channel;",lad));
//       LinesVas();
//     }
//   }

//   for (int lad=0; lad<NLAD; lad++) {
//     fOccupancyCanvas->cd(CONVERT[lad]);
//     gPad->GetListOfPrimitives()->Remove(hisOccupancy[lad]);
//     hisOccupancy[lad]->Draw("same");
//     gPad->Update();
//   }
//   fOccupancyCanvas->Update();

//   fDisplayBusy=0;
// }

// void MyMainFrame::UpdateCogDisplay(int event, int refreshaxes) {

//   if (fDisplayBusy) {
//     cout << "busy" << endl;
//     return; // in case of multiple calls of UpdateCogDisplay()
//   }
//   fDisplayBusy=1;

//   if (refreshaxes) {
//     for (int lad=0; lad<NLAD; lad++) {
//       fCogCanvas->cd(CONVERT[lad]);
//       TH1F *frame=gPad->DrawFrame(fCogAxis[0],fCogAxis[2],fCogAxis[1],fCogAxis[3]);
//       frame->SetTitle(Form("Center of gravity - Ladder %d; channel;",lad));
//       LinesVas();
//     }
//   }

//   for (int lad=0; lad<NLAD; lad++) {
//     fCogCanvas->cd(CONVERT[lad]);
//     gPad->GetListOfPrimitives()->Remove(hisCog[lad]);
//     hisCog[lad]->Draw("same");
//     gPad->Update();
//   }
//   fCogCanvas->Update();

//   fDisplayBusy=0;
// }

/* void MyMainFrame::UpdateLensDisplay(int event, int refreshaxes) { */

/*   if (fDisplayBusy) { */
/*     cout << "busy" << endl; */
/*     return; // in case of multiple calls of UpdateLensDisplay() */
/*   } */
/*   fDisplayBusy=1; */

/*   if (refreshaxes) { */
/*     for (int lad=0; lad<NLAD; lad++) { */
/*       fLensCanvas->cd(CONVERT[lad]); */
/*       TH1F *frame=gPad->DrawFrame(fLensAxis[0],fLensAxis[2],fLensAxis[1],fLensAxis[3]); */
/*       frame->SetTitle(Form("Cluster length - Ladder %d; channel;",lad)); */
/*       //LinesVas(); */
/*     } */
/*   } */

/*   for (int lad=0; lad<NLAD; lad++) { */
/*     fLensCanvas->cd(CONVERT[lad]); */
/*     gPad->GetListOfPrimitives()->Remove(hisLens[lad]); */
/*     hisLens[lad]->Draw("same"); */
/*     gPad->Update(); */
/*   } */
/*   fLensCanvas->Update(); */

/*   fDisplayBusy=0; */
/* } */

// void MyMainFrame::UpdateLenkDisplay(int event, int refreshaxes) {

//   if (fDisplayBusy) {
//     cout << "busy" << endl;
//     return; // in case of multiple calls of UpdateLenkDisplay()
//   }
//   fDisplayBusy=1;

//   if (refreshaxes) {
//     for (int lad=0; lad<NLAD; lad++) {
//       fLenkCanvas->cd(CONVERT[lad]);
//       TH1F *frame=gPad->DrawFrame(fLenkAxis[0],fLenkAxis[2],fLenkAxis[1],fLenkAxis[3]);
//       frame->SetTitle(Form("K-Cluster length - Ladder %d; channel;",lad));
//       //LinesVas();
//     }
//   }

//   for (int lad=0; lad<NLAD; lad++) {
//     fLenkCanvas->cd(CONVERT[lad]);
//     gPad->GetListOfPrimitives()->Remove(hisLenk[lad]);
//     hisLenk[lad]->Draw("same");
//     gPad->Update();
//   }
//   fLenkCanvas->Update();

//   fDisplayBusy=0;
// }

// void MyMainFrame::UpdateIntsDisplay(int event, int refreshaxes) {

//   if (fDisplayBusy) {
//     cout << "busy" << endl;
//     return; // in case of multiple calls of UpdateIntsDisplay()
//   }
//   fDisplayBusy=1;

//   if (refreshaxes) {
//     for (int lad=0; lad<NLAD; lad++) {
//       fIntsCanvas->cd(CONVERT[lad]);
//       TH1F *frame=gPad->DrawFrame(fIntsAxis[0],fIntsAxis[2],fIntsAxis[1],fIntsAxis[3]);
//       frame->SetTitle(Form("Integral S-side - Ladder %d; channel;",lad));
//       //LinesVas();
//     }
//   }

//   for (int lad=0; lad<NLAD; lad++) {
//     fIntsCanvas->cd(CONVERT[lad]);
//     gPad->GetListOfPrimitives()->Remove(hisIntS[lad]);
//     hisIntS[lad]->Draw("same");
//     gPad->Update();
//   }
//   fIntsCanvas->Update();

//   fDisplayBusy=0;
// }

// void MyMainFrame::UpdateIntkDisplay(int event, int refreshaxes) {

//   if (fDisplayBusy) {
//     cout << "busy" << endl;
//     return; // in case of multiple calls of UpdateIntkDisplay()
//   }
//   fDisplayBusy=1;

//   if (refreshaxes) {
//     for (int lad=0; lad<NLAD; lad++) {
//       fIntkCanvas->cd(CONVERT[lad]);
//       TH1F *frame=gPad->DrawFrame(fIntkAxis[0],fIntkAxis[2],fIntkAxis[1],fIntkAxis[3]);
//       frame->SetTitle(Form("Integral S-side - Ladder %d; channel;",lad));
//       //LinesVas();
//     }
//   }

//   for (int lad=0; lad<NLAD; lad++) {
//     fIntkCanvas->cd(CONVERT[lad]);
//     gPad->GetListOfPrimitives()->Remove(hisIntK[lad]);
//     hisIntK[lad]->Draw("same");
//     gPad->Update();
//   }
//   fIntkCanvas->Update();

//   fDisplayBusy=0;
// }

void MyMainFrame::UpdateFftDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateFftDisplay()
    }
    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (int lad = 0; lad < NLAD; lad++)
        {
            fFftCanvas->cd(CONVERT[lad]);
            TH1F *frame = gPad->DrawFrame(fFftAxis[0], fFftAxis[2], fFftAxis[1], fFftAxis[3]);
            frame->SetTitle(Form("FFT - Ladder %d; frequency;", lad));
            //LinesVas();
        }
    }

    int adc{0};

    for (int lad{0}; lad < NLAD; lad++)
    {
        fFftCanvas->cd(CONVERT[lad]);
        gPad->GetListOfPrimitives()->Remove(hisFFT[lad][adc]);
        gPad->GetListOfPrimitives()->Remove(hisFFTnorm[lad][adc]);
        gPad->GetListOfPrimitives()->Remove(hisSumFFT[lad][adc]);
        gPad->GetListOfPrimitives()->Remove(hisSumFFTnorm[lad][adc]);
        TH1F *his = (TGCBShowFFTNorm->IsDown()) ? ((TGCBShowFFTSum->IsDown()) ? hisSumFFTnorm[lad][adc] : hisFFTnorm[lad][adc])
                                                : ((TGCBShowFFTSum->IsDown()) ? hisSumFFT[lad][adc] : hisFFT[lad][adc]);
        //hisFFT[lad]->Draw("same");
        his->Draw("same HIST");
        gPad->Update();
    }
    fFftCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::DrawClusterGraph(TCanvas *c, int ipad, int lad)
{

    TGraph *gr = grclusters[lad];
    TVirtualPad *pad = c->cd(ipad);
    pad->GetListOfPrimitives()->Remove(gr); // first we remove the graph from the object list of the canvas, so that we avoid annoying messages from root if the graph is empty
    ResetGraph(gr);
    BuildGrCluster(Scluster, gr);
    BuildGrCluster(Kcluster, gr);
    if (gr->GetN())
        gr->Draw("p");
    pad->Update();
}

void MyMainFrame::DrawGraph(TCanvas *c, int ipad, int lad, TGraph *gr[NLAD], string option)
{

    TVirtualPad *pad = c->cd(ipad);
    pad->GetListOfPrimitives()->Remove(gr[lad]);
    if (gr[lad]->GetN())
        gr[lad]->Draw(option.c_str());
    pad->Update();
}

void MyMainFrame::DrawGraphSuperimposed(TCanvas *c, int ipad, int lad, int event, TGraph *gr[NLAD])
{

    TVirtualPad *pad = c->cd(ipad);
    TGraph *copy = (TGraph *)gr[lad]->Clone();
    copy->SetName(Form("copy_%d_%d", event, lad));
    copy->Draw("l");
    int back = event - fSuperImposed;
    if (back > -1)
    {
        RemoveGraph(Form("copy_%d_%d", back, lad));
        //TGraph *gr=(TGraph*)pad->FindObject(Form("copy_%d_%d",back,lad));
        //if (gr) delete gr;
    }
    pad->Update();
}

void MyMainFrame::ClearSuperimposedGraphs(TCanvas *c, int event, int back)
{

    if (event < 0 || back < 0)
        return;

    for (int lad = 0; lad < NLAD; lad++)
    {

        if (NLAD > 1)
            TVirtualPad *pad = c->cd(CONVERT[lad]);

        //for (int i=event-back; i<event; i++) {
        for (int i = 0; i < event; i++)
        {
            //cout << "i=" << i << "event=" << event << " back=" << back <<endl;
            RemoveGraph(Form("copy_%d_%d", i, lad));
            //TGraph *gr=(TGraph*)pad->FindObject(Form("copy_%d_%d",i,lad));
            //if (gr) delete gr;
        }
    }
}

void MyMainFrame::UpdatePedSubtDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
        return; // in case of multiple calls of UpdateAdcDisplay()

    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (int lad = 0; lad < NLAD; lad++)
        {
            fPedSubtCanvas->cd(CONVERT[lad]);
            TH1F *frame = gPad->DrawFrame(fPedSubtAxis[0], fPedSubtAxis[2], fPedSubtAxis[1], fPedSubtAxis[3]);
            frame->SetTitle(Form("Ped. subt. - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
            LinesVas();
        }
    }
    //  TCanvas *c2=fPedSubtCanvas;

    for (int lad = 0; lad < NLAD; lad++)
    {

        if (!fSuperImposed)
            DrawGraph(fPedSubtCanvas, CONVERT[lad], lad, grpedsubt);
        else
            DrawGraphSuperimposed(fPedSubtCanvas, CONVERT[lad], lad, event, grpedsubt);
    }
    fPedSubtCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateAdcPedSubtDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
        return; // in case of multiple calls of UpdateAdcDisplay()

    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (int adc = 0; adc < 2 * NADC; adc++)
        {
            fAdcPedSubtCanvas->cd(adc + 1);
            TH1F *frame = gPad->DrawFrame(fAdcPedSubtAxis[0], fAdcPedSubtAxis[2], fAdcPedSubtAxis[1], fAdcPedSubtAxis[3]);
            frame->SetTitle(Form("Adc Ped. subt. - ADC %d; readings;", adc));
            LinesAdcs();
        }
    }
    //  TCanvas *c2=fPedSubtCanvas;

    for (int adc = 0; adc < 2 * NADC; adc++)
    {

        if (!fSuperImposed)
            DrawGraph(fAdcPedSubtCanvas, adc + 1, adc, grADCpedsubt);
        else
            DrawGraphSuperimposed(fAdcPedSubtCanvas, adc + 1, adc, event, grADCpedsubt);
    }
    fAdcPedSubtCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateAdcPedCnSubtDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
        return; // in case of multiple calls of UpdateAdcDisplay()

    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (int adc = 0; adc < 2 * NADC; adc++)
        {
            fAdcPedCnSubtCanvas->cd(adc + 1);
            TH1F *frame = gPad->DrawFrame(fAdcPedCnSubtAxis[0], fAdcPedCnSubtAxis[2], fAdcPedCnSubtAxis[1], fAdcPedCnSubtAxis[3]);
            frame->SetTitle(Form("Adc signal - ADC %d; readings;", adc));
            LinesAdcs();
        }
    }
    //  TCanvas *c2=fPedCnSubtCanvas;

    for (int adc = 0; adc < 2 * NADC; adc++)
    {

        if (!fSuperImposed)
            DrawGraph(fAdcPedCnSubtCanvas, adc + 1, adc, grADCpedcnsubt);
        else
            DrawGraphSuperimposed(fAdcPedCnSubtCanvas, adc + 1, adc, event, grADCpedcnsubt);
    }
    fAdcPedCnSubtCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdatePedCnSubtDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
        return; // in case of multiple calls of UpdateAdcDisplay()

    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (int lad = 0; lad < NLAD; lad++)
        {
            fPedCnSubtCanvas->cd(CONVERT[lad]);
            TH1F *frame = gPad->DrawFrame(fPedCnSubtAxis[0], fPedCnSubtAxis[2], fPedCnSubtAxis[1], fPedCnSubtAxis[3]);
            frame->SetTitle(Form("Signals - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
            LinesVas();
        }
    }
    //  TCanvas *c2=fPedSubtCanvas;

    for (int lad = 0; lad < NLAD; lad++)
    {
        DoCluster(lad, 'S', &Scluster);
        ListCluster(Scluster, Form("Event %d, Ladder %d, S-side: ", event, lad));
        ResetGraph(grclusters[lad]);
        BuildGrCluster(Scluster, grclusters[lad]);
    }

    for (int lad = 0; lad < NLAD; lad++)
    {

        if (!fSuperImposed)
            DrawGraph(fPedCnSubtCanvas, CONVERT[lad], lad, grpedcnsubt);
        else
            DrawGraphSuperimposed(fPedCnSubtCanvas, CONVERT[lad], lad, event, grpedcnsubt);

        //DrawHis(fPedCnSubtCanvas,CONVERT[lad],hisClusters[lad]);

        if (grclusters[lad]->GetN())
            DrawGraph(fPedCnSubtCanvas, CONVERT[lad], lad, grclusters, "p");
        //DrawClusterGraph(fPedCnSubtCanvas,CONVERT[lad],lad);
    }

    fPedCnSubtCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::ComputePedestals(int startevent)
{

    fTGHPBprogress->Reset();

    vdouble().swap(fPedestals); // clear and reallocate memory see cplusplus.com
    fPedestals.assign(fNTOTCH, 0);
    vint().swap(fStatus);
    fStatus.assign(fNTOTCH, 0);

    int PedCnt = 0;

    for (int event = startevent; event < fNevents; event++)
    {
        int selmode{TGCBEventSel->GetSelected()};
        if (selmode == 1 and (event % 2) == 1)
            continue; // we exclude odd indices
        else if (selmode == 2 and (event % 2) == 0)
            continue; // we exclude even indices

        fTree->GetEntry(event);
        fEventCounter = event;
        PedCnt++;

        if (PedCnt % 100 == 0)
        {
            //fTGHPBprogress->SetPosition((PedCnt*100.)/NPEDEVENTS); // in linux, this is enough to update the display of the progress bar
            cout << "Progress: " << (PedCnt * 100.) / NPEDEVENTS << " %" << endl;
            //fTGHPBprogress->ShowPosition(); // in mac, you need to add this
            //gSystem->ProcessEvents(); // and this.
        }

        for (int ch = 0; ch < fNTOTCH; ch++)
        {
            int adc = ch / fNADCCH;
            //if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;
            fPedestals.at(ch) += GetStripValue(ch, fChanLabelMode);
        }
        if (PedCnt == NPEDEVENTS)
            break;
    }

    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        fPedestals.at(ch) /= PedCnt;
        if (fPedestals.at(ch) < 5)
            fStatus.at(ch) |= 1;
        if (fPedestals.at(ch) > 4090)
            fStatus.at(ch) |= 2;
        //AddLogMessage(Form("Pedestal = %5.1lf for channel %d, status = %d", fPedestals.at(ch), ch, fStatus.at(ch)));
    }

    //fTGHPBprogress->SetPosition(100);
    //fTGHPBprogress->ShowPosition();
    //gSystem->ProcessEvents();

    AddLogMessage(Form("Pedestals computed with %d events.", PedCnt));
}

bool MyMainFrame::ComputePedestals2(int startevent, double pedcut)
{

    bool result{false};
    fTGHPBprogress->Reset();

    vdouble fPedestals2;
    //fPedestals2.clear();
    fPedestals2.assign(fNTOTCH, 0);
    int PedCnt{0};
    int LastEv{0};

    for (int ch{0}; ch < fNTOTCH; ch++)
    {

        if (ch % 40 == 0)
        {
            //fTGHPBprogress->SetPosition((ch*100.)/fNTOTCH);
            cout << "Progress: " << (ch * 100.) / fNTOTCH << " %" << endl;
            //fTGHPBprogress->ShowPosition();
            //gSystem->ProcessEvents();
        }

        PedCnt = 0;
        for (int event{startevent}; event < fNevents; event++)
        {
            fTree->GetEntry(event);
            fEventCounter = event;
            if (fabs(GetStripValue(ch, fChanLabelMode) - fPedestals.at(ch)) > pedcut)
                continue;
            fPedestals2.at(ch) += GetStripValue(ch, fChanLabelMode);
            PedCnt++;
            if (PedCnt == NPEDEVENTS)
            {
                LastEv = event;
                break;
            }
        }

        if (PedCnt != NPEDEVENTS)
        {
            fPedestals2.at(ch) = fPedestals.at(ch); // we did not succeed to find 2048 events in the whole DAQ file to compute new pedestals, we keep the old ones.
            result = false;
        }
        else
        { //for (int ch=0; ch<fNTOTCH; ch++)
            fPedestals2.at(ch) /= PedCnt;
            if (fPedestals2.at(ch) < 5)
                fStatus.at(ch) |= 1;
            if (fPedestals2.at(ch) > 4090)
                fStatus.at(ch) |= 2;
            result = true;
        }
        //AddLogMessage(Form("Optima pedestal (cut=%f) = %5.1lf for channel %d computed with %d events. Last event = %d. Status = %d", pedcut, fPedestals2.at(ch), ch, PedCnt, LastEv, fStatus.at(ch)));
        fPedestals.at(ch) = fPedestals2.at(ch);
    }

    //fPedestals2.clear();
    // as suggested in cplusplus.com, the way to clear and reallocate free memory is to do as follows:
    vdouble().swap(fPedestals2); // right way to clear vector and reallocate memory

    //fTGHPBprogress->SetPosition(100);
    //fTGHPBprogress->ShowPosition();
    //gSystem->ProcessEvents();

    return result;
}

void MyMainFrame::ComputeRawSigmas(int startevent)
{

    fTGHPBprogress->Reset();

    //TH2F *h2ADCval{new TH2F("h2ADCval", "pedsubt distribution", 2048, -0.5, 2048 - 0.5, 60, -30, 30)};

    int sRawCnt = 0;
    vdouble().swap(fRawSigmas);
    fRawSigmas.assign(fNTOTCH, 0);

    for (int event = startevent; event < fNevents; event++)
    {
        int selmode{TGCBEventSel->GetSelected()};
        if (selmode == 1 and (event % 2) == 1)
            continue; // we exclude odd indices
        else if (selmode == 2 and (event % 2) == 0)
            continue; // we exclude even indices

        fTree->GetEntry(event);
        fEventCounter = event;
        sRawCnt++;

        if (sRawCnt % 100 == 0)
        {
            //fTGHPBprogress->SetPosition((sRawCnt*100.)/NSRAWEVENTS); // in linux, this is enough to update the display of the progress bar
            cout << "Progress: " << (sRawCnt * 100.) / NSRAWEVENTS << " %" << endl;
            //fTGHPBprogress->ShowPosition(); // in mac, you need to add this
            //gSystem->ProcessEvents(); // and this.
        }

        for (int ch = 0; ch < fNTOTCH; ch++)
        {
            int adc = ch / fNADCCH;
            //if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;
            double val = GetStripValue(ch, fChanLabelMode) - fPedestals.at(ch);
            //h2ADCval->Fill(ch, val);
            val *= val;
            fRawSigmas.at(ch) += val;
        }
        if (sRawCnt == NSRAWEVENTS)
            break;
    }

    //if (sRawCnt<2048) cout << "look out, only " << sRawCnt << " events to compute raw sigma" << endl;

    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        fRawSigmas.at(ch) /= sRawCnt;
        fRawSigmas.at(ch) = sqrt(fRawSigmas.at(ch));
        if (fRawSigmas.at(ch) < 1.7)
            fStatus.at(ch) |= 4;
        if (fRawSigmas.at(ch) > 10.0)
            fStatus.at(ch) |= 8;
    }

    //fTGHPBprogress->SetPosition(100);
    //fTGHPBprogress->ShowPosition();
    //gSystem->ProcessEvents();

    //TCanvas *c{new TCanvas("c", "c", 800, 600)};
    //h2ADCval->Draw("colz");
    //c->Update();

    AddLogMessage(Form("Raw sigmas computed with %d events.", sRawCnt));
}

void MyMainFrame::ComputeSigmas(int startevent)
{

    fTGHPBprogress->Reset();

    vdouble().swap(fSigmas);
    fSigmas.assign(fNTOTCH, 0);
    int SigmaCnt = 0;
    vdouble().swap(fReadOut);
    fReadOut.assign(fNTOTCH, 0);
    int cn6_cnt{0};
    int cn4_cnt{0};
    int cn2_cnt{0};
    int cn1_cnt{0};
    int kocn_cnt{0};

    for (int event = startevent; event < fNevents; event++)
    {
        int selmode{TGCBEventSel->GetSelected()};
        if (selmode == 1 and (event % 2) == 1)
            continue; // we exclude odd indices
        else if (selmode == 2 and (event % 2) == 0)
            continue; // we exclude even indices

        fTree->GetEntry(event);
        fEventCounter = event;
        SigmaCnt++;

        if (SigmaCnt % 100 == 0)
        {
            //fTGHPBprogress->SetPosition((SigmaCnt*100.)/NSIGEVENTS); // in linux, this is enough to update the display of the progress bar
            cout << "Progress: " << (SigmaCnt * 100.) / NSIGEVENTS << " %" << endl;
            //fTGHPBprogress->ShowPosition(); // in mac, you need to add this
            //gSystem->ProcessEvents(); // and this.
        }

        for (int ch = 0; ch < fNTOTCH; ch++)
        {
            int adc = ch / fNADCCH;
            //if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;
            fReadOut.at(ch) = GetStripValue(ch, fChanLabelMode) - fPedestals.at(ch);
        }
        //if (TGCBComputeCN->IsDown()) {
        int success{ComputeCN(0.6)};
        if (success)
            cn6_cnt++;
        else
        {
            success = ComputeCN(0.4);
            if (success)
                cn4_cnt++;
            else
            {
                success = ComputeCN(0.2);
                if (success)
                    cn2_cnt++;
                else
                {
                    success = ComputeCN(0.1);
                    if (success)
                        cn1_cnt++;
                    else
                        kocn_cnt++;
                }
            }
        }
        SubtractCN();
        DoRunningCN();
        //}
        for (int ch = 0; ch < fNTOTCH; ch++)
        {
            double val = fReadOut.at(ch);
            val *= val;
            fSigmas.at(ch) += val;
        }
        if (SigmaCnt == NSIGEVENTS)
            break;
        //for (int i=0; i<fNTOTCH; i++) fStripPrevious[i]=fStrip[i];
        //memcpy(&fStripPrevious,&fStrip,sizeof(fStrip));
    }

    //if (sRawCnt<2048) cout << "look out, only " << sRawCnt << " events to compute raw sigma" << endl;

    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        fSigmas.at(ch) /= SigmaCnt;
        fSigmas.at(ch) = sqrt(fSigmas.at(ch));
        if (fSigmas.at(ch) < 1)
            fStatus.at(ch) |= 16;
        if (fSigmas.at(ch) > 2.5)
            fStatus.at(ch) |= 32;
    }

    //fTGHPBprogress->SetPosition(100);
    //fTGHPBprogress->ShowPosition();
    //gSystem->ProcessEvents();

    AddLogMessage(Form("Sigmas computed with %d events.", SigmaCnt));

    std::cout << "CN results:" << endl;
    std::cout << " cn6_cnt = " << cn6_cnt << endl;
    std::cout << " cn4_cnt = " << cn4_cnt << endl;
    std::cout << " cn2_cnt = " << cn2_cnt << endl;
    std::cout << " cn1_cnt = " << cn1_cnt << endl;
    std::cout << "kocn_cnt = " << kocn_cnt << endl;
}

// void MyMainFrame::StartFFT() {

//   // for (int lad=0; lad<NLAD; lad++)
//   //   for (int adc=0; adc<NADC; adc++) {
//   //     fSpectrum[lad][adc].clear();
//   //     fSpectrum[lad][adc].assign(fNADCCH,0);
//   //   }

//   fReadOut.clear();
//   fReadOut.assign(fNTOTCH,0);

//   for (int event=0; event<fNevents; event++) {
//     fTree->GetEntry(event);
//     //SigmaCnt++;
//     for (int ch=0; ch<fNTOTCH; ch++) {
//       int adc=ch/fNADCCH;
//       if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;
//       fReadOut.at(ch)=fStrip[ch]-fPedestals.at(ch);
//     }
//     ComputeCN();
//     SubtractCN();

//     ComputeFFT();

//   }

//   AddLogMessage("FFT distributions computed.");

// }

void MyMainFrame::ComputeFFT()
{
    //cout << "welcome to computefft" << endl;
    //  double adcSA[fNADCCH], adcSB[fNADCCH], adcKA[fNADCCH], adcKB[fNADCCH];
    //  double fSpectrumSA[fNADCCH], fSpectrumSB[fNADCCH], fSpectrumKA[fNADCCH], fSpectrumKB[fNADCCH];

    vdouble vAdcval(fNADCCH);
    vdouble vSpectrum(fNADCCH);

    for (int lad{0}; lad < NLAD; lad++)
        for (int adc{0}; adc < NADC; adc++)
        {
            for (int ch = 0; ch < fNADCCH; ch++)
            {
                vAdcval.at(ch) = fReadOut.at(ch + adc * fNADCCH + lad * fNLCH);
            }
            //memset(&fSpectrum, 0, sizeof(fSpectrum));
            DoFFT(vAdcval.data(), fNADCCH, vSpectrum.data());
            for (int ch = 0; ch < fNADCCH; ch++)
            {
                //cout << Form("%3d: %4.2lf  %4.2lf  %4.2lf  %4.2lf\n", ch, fSpectrumSA[ch], fSpectrumSB[ch], fSpectrumKA[ch], fSpectrumKB[ch]);

                hisFFT[lad][adc]->SetBinContent(ch + 1, vSpectrum.at(ch));
                hisSumFFT[lad][adc]->SetBinContent(ch + 1, hisSumFFT[lad][adc]->GetBinContent(ch + 1) + vSpectrum.at(ch));

                hisFFTnorm[lad][adc]->SetBinContent(ch + 1, vSpectrum.at(ch));
                hisSumFFTnorm[lad][adc]->SetBinContent(ch + 1, hisSumFFT[lad][adc]->GetBinContent(ch + 1));
            }
            //cout << hisFFTnorm[0]->GetSumOfWeights() << "  " << hisSumFFTnorm[0]->GetSumOfWeights() << endl;
            hisFFTnorm[lad][adc]->Scale(1 / hisFFTnorm[lad][adc]->GetSumOfWeights());
            hisSumFFTnorm[lad][adc]->Scale(1 / hisSumFFTnorm[lad][adc]->GetSumOfWeights());
            //cout << hisFFTnorm[0]->GetSumOfWeights() << "  " << hisSumFFTnorm[0]->GetSumOfWeights() << endl;
        }
}

void MyMainFrame::DoFFT(double *array, int nech, double *his)
{
    fftw_complex *out;

    //for (int i=0; i<nech; i++)  printf("i=%3d  array=%lf\n",i,array[i]);

    //printf("this is %s\n", his->GetName());

    fftw_plan fftp;
    out = new fftw_complex[nech / 2 + 1];
    fftp = fftw_plan_dft_r2c_1d(nech, array, out, FFTW_ESTIMATE);
    fftw_execute(fftp);

    float val;
    for (int i = 0; i < (nech / 2 + 1); i++)
    {
        val = (out[i][0] * out[i][0] + out[i][1] * out[i][1]) / nech;
        his[1 + i] += val; // +1 because we then fill a histogram object
                           //his->SetBinContent(i+1,his->GetBinContent(i+1)+val);
    }

    fftw_destroy_plan(fftp);
    delete[] out;
}

void MyMainFrame::RemoveGraph(string name)
{

    TGraph *gr = (TGraph *)gPad->FindObject(name.c_str());
    if (gr)
        delete gr;
}

void MyMainFrame::DisplayPedestals()
{

    TCanvas *c = fPedCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        RemoveGraph("Peds");
        RemoveGraph("RefPeds");
    }
    c->Update();

    TGraph *ped[NLAD], *refped[NLAD];
    for (int i = 0; i < NLAD; i++)
    {
        ped[i] = new TGraph();
        ped[i]->SetMarkerStyle(7);
        ped[i]->SetMarkerColor(kRed);
        ped[i]->SetLineColor(kRed + 2);
        ped[i]->SetName("Peds");
    }
    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        int lad = ch / fNLCH;
        ped[lad]->SetPoint(ch % fNLCH, ch % fNLCH, fPedestals.at(ch));
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fPedAxis[0], fPedAxis[2], fPedAxis[1], fPedAxis[3]);
        frame->SetTitle(Form("Pedestals - Ladder %d; %s", lad, ChanLabel[fChanLabelMode].c_str()));
        ped[lad]->Draw("l");
        LinesVas();
    }

    if (TGCBshowrefped->IsDown())
    {
        for (int i = 0; i < NLAD; i++)
        {
            refped[i] = new TGraph();
            refped[i]->SetMarkerStyle(7);
            refped[i]->SetMarkerColor(kBlue);
            refped[i]->SetLineColor(kBlue + 2);
            refped[i]->SetName("RefPeds");
        }

        for (int ch = 0; ch < fNTOTCH; ch++)
        {
            int lad = ch / fNLCH;
            refped[lad]->SetPoint(ch % fNLCH, ch % fNLCH, fRefPedestals.at(ch));
        }
        for (int lad = 0; lad < NLAD; lad++)
        {
            c->cd(CONVERT[lad]);
            //gPad->DrawFrame(fPedAxis[0],fPedAxis[2],fPedAxis[1],fPedAxis[3]);
            refped[lad]->Draw("l");
            //LinesVas();
        }
    }

    c->Update();
}

void MyMainFrame::DisplayOccupancy()
{
    TCanvas *c = fOccupancyCanvas;

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        //gPad->SetLogy();
        TH1F *frame = gPad->DrawFrame(fOccupancyAxis[0], fOccupancyAxis[2], fOccupancyAxis[1], fOccupancyAxis[3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Occupancy - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        TH1F *his = (TGCBshowOccCut->IsDown() ? hisOccupancyCut[lad] : hisOccupancy[lad]);
        his->SetStats(kTRUE);
        his->Draw("sames");
        gPad->Update();
        LinesVas();
    }

    c->Update();
}

void MyMainFrame::DisplayCog()
{
    TCanvas *c = fCogCanvas;

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fCogAxis[0], fCogAxis[2], fCogAxis[1], fCogAxis[3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Cog - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        TH1F *his = (TGCBshowCogCut->IsDown() ? hisCogCut[lad] : hisCog[lad]);
        his->SetStats(kTRUE);
        his->Draw("sames");
        gPad->Update();
        LinesVas();
    }

    c->Update();
}

void MyMainFrame::DisplayLens()
{
    TCanvas *c = fLensCanvas;

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        //gPad->SetLogy();
        TH1F *frame = gPad->DrawFrame(fLensAxis[0], (fLensAxis[2] < 1) ? 1 : fLensAxis[2], fLensAxis[1], fLensAxis[3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Lens - Ladder %d; cluster length;", lad));
        TH1F *his = (TGCBshowLensCut->IsDown() ? hisLensCut[lad] : hisLens[lad]);
        his->SetStats(kTRUE);
        his->Draw("sames");
        gPad->Update();
        //LinesVas();
    }

    c->Update();
}

void MyMainFrame::DisplayLenVsCog()
{
    TCanvas *c = fLenVsCogCanvas;

    bool profile_x{TGCBshowProfileLenVsCog->IsDown()};

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        if (!profile_x)
            gPad->SetLogz();
        if (profile_x)
        {
            TH1F *frame = gPad->DrawFrame(fLenVsCogAxis[0], (fLenVsCogAxis[2] < 0) ? 0 : fLenVsCogAxis[2], fLenVsCogAxis[1], fLenVsCogAxis[3]);
            frame->SetTitle(Form("Len vs cog - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        }

        TH2F *his2 = (TGCBshowLenVsCogCut->IsDown() ? hisLenVsCogCut[lad] : hisLenVsCog[lad]);
        gStyle->SetOptStat(1111111);
        his2->SetStats(kTRUE);
        his2->GetXaxis()->SetRangeUser(fLenVsCogAxis[0], fLenVsCogAxis[1]);
        his2->GetYaxis()->SetRangeUser(fLenVsCogAxis[2], fLenVsCogAxis[3]);
        if (!profile_x)
            his2->Draw("colz");
        else
            his2->ProfileX()->Draw("same");
        gPad->Update();
        LinesVas();
    }

    c->Update();
}

void MyMainFrame::DisplayIntVsCog()
{
    TCanvas *c = fIntVsCogCanvas;

    bool profile_x{TGCBshowProfileIntVsCog->IsDown()};

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        if (!profile_x)
            gPad->SetLogz();
        if (profile_x)
        {
            TH1F *frame = gPad->DrawFrame(fIntVsCogAxis[0], (fIntVsCogAxis[2] < 0) ? 0 : fIntVsCogAxis[2], fIntVsCogAxis[1], fIntVsCogAxis[3]);
            //frame->SetTitle(Form("Int vs cog - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
            frame->SetTitle(Form("Int vs cog - Ladder %d; cog;integral", lad));
        }

        TH2F *his2 = (TGCBshowIntVsCogCut->IsDown() ? hisIntVsCogCut[lad] : hisIntVsCog[lad]);
        gStyle->SetOptStat(1111111);
        his2->SetStats(kTRUE);
        his2->GetXaxis()->SetRangeUser(fIntVsCogAxis[0], fIntVsCogAxis[1]);
        his2->GetYaxis()->SetRangeUser(fIntVsCogAxis[2], fIntVsCogAxis[3]);
        if (!profile_x)
            his2->Draw("colz");
        else
            his2->ProfileX()->Draw("same");
        gPad->Update();
        LinesVas();
    }

    c->Update();
}

void MyMainFrame::DisplayInts()
{
    TCanvas *c = fIntsCanvas;

    TF1 *flandau = new TF1("flandau", "landau");
    flandau->SetRange(20, 60);

    TF1 *flangau = new TF1("flangau", langaufun, 15, 60, 4);
    flangau->SetParameters(5, 30, 100, 5);
    flangau->SetParNames("Width", "MP", "Area", "GSigma");
    flangau->SetLineColor(kRed);
    flangau->SetLineStyle(1);
    //flangau->SetParLimits(0, 0.7, 20);

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        //gPad->SetLogy();
        TH1F *frame = gPad->DrawFrame(fIntsAxis[0], (fIntsAxis[2] <= 0) ? 1 : fIntsAxis[2], fIntsAxis[1], fIntsAxis[3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Ints - Ladder %d; cluster integral (ADCc);", lad));
        TH1F *his = (TGCBshowIntsCut->IsDown() ? hisIntSCut[lad] : hisIntS[lad]);
        his->SetStats(kTRUE);
        his->Draw("sames");
        his->Fit(flandau, "N");
        flangau->SetParameter(1, flandau->GetParameter(1));
        flangau->SetParameter(2, flandau->GetParameter(2));
        flangau->SetRange(flandau->GetParameter(1) - 10, flandau->GetParameter(1) + 50);
        his->Fit(flangau, "RM");
        gPad->Update();
    }

    c->Update();
}

void MyMainFrame::DisplayNclus()
{
    TCanvas *c = fNclusCanvas;

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        gPad->SetLogy();
        TH1F *frame = gPad->DrawFrame(fNclusAxis[0], (fNclusAxis[2] <= 0) ? 1 : fNclusAxis[2], fNclusAxis[1], fNclusAxis[3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Nclus - Ladder %d; clusters/event;", lad));
        hisNclus[lad]->SetStats(kTRUE);
        hisNclus[lad]->Draw("sames");
        gPad->Update();
    }

    c->Update();
}

void MyMainFrame::DisplayPedCompar()
{

    TCanvas *c = fPedCompCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        RemoveGraph("DelPeds");
    }
    c->Update();

    TGraph *delped[NLAD];
    for (int i = 0; i < NLAD; i++)
    {
        delped[i] = new TGraph();
        delped[i]->SetMarkerStyle(7);
        delped[i]->SetMarkerColor(kRed);
        delped[i]->SetLineColor(kBlue + 2);
        delped[i]->SetName("DelPeds");
    }
    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        int lad = ch / fNLCH;
        delped[lad]->SetPoint(ch % fNLCH, ch % fNLCH, fPedestals.at(ch) - fRefPedestals.at(ch));
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fPedCompAxis[0], fPedCompAxis[2], fPedCompAxis[1], fPedCompAxis[3]);
        frame->SetTitle(Form("Ped. diff. - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        delped[lad]->Draw("l");
        LinesVas();
    }

    c->Update();
}

void MyMainFrame::DisplayRawSigmas()
{

    TCanvas *c = fSrawCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        RemoveGraph("Sraws");
    }
    c->Update();

    TGraph *sraw[NLAD];
    for (int i = 0; i < NLAD; i++)
    {
        sraw[i] = new TGraph();
        sraw[i]->SetMarkerStyle(7);
        sraw[i]->SetMarkerColor(kRed);
        sraw[i]->SetLineColor(kRed + 2);
        sraw[i]->SetName("Sraws");
    }

    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        int lad = ch / fNLCH;
        sraw[lad]->SetPoint(ch % fNLCH, ch % fNLCH, fRawSigmas.at(ch));
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fSrawAxis[0], fSrawAxis[2], fSrawAxis[1], fSrawAxis[3]);
        frame->SetTitle(Form("Raw sigmas - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        sraw[lad]->Draw("l");
        LinesVas();
    }

    c->Update();
}

void MyMainFrame::DisplaySigmas()
{

    TCanvas *c = fSigmaCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        RemoveGraph("Sigmas");
        RemoveGraph("RefSigmas");
    }
    c->Update();

    TGraph *sigma[NLAD], *refsigma[NLAD];
    for (int i = 0; i < NLAD; i++)
    {
        sigma[i] = new TGraph();
        sigma[i]->SetMarkerStyle(7);
        sigma[i]->SetMarkerColor(kRed);
        sigma[i]->SetLineColor(kRed + 2);
        sigma[i]->SetName("Sigmas");
    }

    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        int lad = ch / fNLCH;
        sigma[lad]->SetPoint(ch % fNLCH, ch % fNLCH, fSigmas.at(ch));
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fSigmaAxis[0], fSigmaAxis[2], fSigmaAxis[1], fSigmaAxis[3]);
        frame->SetTitle(Form("Sigmas - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        sigma[lad]->Draw("l");
        LinesVas();
    }

    if (TGCBshowrefsig->IsDown())
    {
        for (int i = 0; i < NLAD; i++)
        {
            refsigma[i] = new TGraph();
            refsigma[i]->SetMarkerStyle(7);
            refsigma[i]->SetMarkerColor(kBlue);
            refsigma[i]->SetLineColor(kBlue + 2);
            refsigma[i]->SetName("RefSigmas");
        }

        for (int ch = 0; ch < fNTOTCH; ch++)
        {
            int lad = ch / fNLCH;
            refsigma[lad]->SetPoint(ch % fNLCH, ch % fNLCH, fRefSigmas.at(ch));
        }
        for (int lad = 0; lad < NLAD; lad++)
        {
            c->cd(CONVERT[lad]);
            //gPad->DrawFrame(fPedAxis[0],fPedAxis[2],fPedAxis[1],fPedAxis[3]);
            refsigma[lad]->Draw("l");
            //LinesVas();
        }
    }

    c->Update();
}

void MyMainFrame::DisplaySigmaCompar()
{

    TCanvas *c = fSigmaCompCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        RemoveGraph("Sigmas");
        RemoveGraph("RefSigmas");
    }
    c->Update();

    TGraph *sigma[NLAD], *refsigma[NLAD];
    for (int i = 0; i < NLAD; i++)
    {
        sigma[i] = new TGraph();
        sigma[i]->SetMarkerStyle(7);
        sigma[i]->SetMarkerColor(kRed);
        sigma[i]->SetName("Sigmas");
        refsigma[i] = new TGraph();
        refsigma[i]->SetMarkerStyle(7);
        refsigma[i]->SetMarkerColor(kBlue);
        refsigma[i]->SetName("RefSigmas");
    }

    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        int lad = ch / fNLCH;
        sigma[lad]->SetPoint(ch % fNLCH, ch % fNLCH, fSigmas.at(ch));
        refsigma[lad]->SetPoint(ch % fNLCH, ch % fNLCH, -fRefSigmas.at(ch));
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        c->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fSigmaCompAxis[0], fSigmaCompAxis[2], fSigmaCompAxis[1], fSigmaCompAxis[3]);
        frame->SetTitle(Form("Sigma comparison - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str())),
            sigma[lad]->Draw("pl");
        refsigma[lad]->Draw("pl");
        LinesVas(1);
    }

    c->Update();
}

void MyMainFrame::InitGraphHis()
{

    for (int adc = 0; adc < 2 * NADC; adc++)
    {
        grADCpedsubt[adc] = new TGraph();
        grADCpedsubt[adc]->SetName(Form("diff_adc_%d", adc));
        grADCpedcnsubt[adc] = new TGraph();
        grADCpedcnsubt[adc]->SetName(Form("signal_adc_%d", adc));
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        for (int ch = 0; ch < fNTOTCH + 1; ch++)
        {
            TH1F *his{new TH1F(Form("hisChHi_lad%d_ch%d", lad, ch), Form("ChHi ladder %d ch %d", lad, ch), 8000, -4000., 4000.)}; //attatt
            vHisChHi[lad].push_back(his);
            his = new TH1F(Form("hisChHiPe_lad%d_ch%d", lad, ch), Form("ChHiPe ladder %d ch %d", lad, ch), 100, -0.5, 100 - 0.5); //attatt
            vHisChHiPe[lad].push_back(his);
        }
        h2ChHi[lad] = new TH2F(Form("h2ChHi_lad%d", lad), Form("h2ChHi_lad%d", lad), fNTOTCH + 1, 0, fNTOTCH + 1, 4096, 0, 4096);
        h2ChHiPe[lad] = new TH2F(Form("h2ChHiPe_lad%d", lad), Form("h2ChHiPe_lad%d", lad), fNTOTCH + 1, 0, fNTOTCH + 1, 2000, 0, 100);
    }

    for (int lad{0}; lad < NLAD; lad++)
    {
        gradc[lad] = new TGraph();
        gradc[lad]->SetName(Form("ladder_%d", lad));
        grpedsubt[lad] = new TGraph();
        grpedsubt[lad]->SetName(Form("diff_ladder_%d", lad));
        grpedcnsubt[lad] = new TGraph();
        grpedcnsubt[lad]->SetName(Form("signal_ladder_%d", lad));

        grPixelResultsIntercept[lad] = new TGraph();

        grPixelResultsSlope[lad] = new TGraph();

        for (int adc{0}; adc < NADC; adc++)
        {
            hisFFT[lad][adc] = new TH1F(Form("hisFFT_lad%d_adc%d", lad, adc), Form("FFT ladder %d ADC %d", lad, adc), fNLCH, 0, fNLCH);
            hisFFTnorm[lad][adc] = new TH1F(Form("hisFFTnorm_lad%d_adc%d", lad, adc), Form("Normalized FFT ladder %d ADC %d", lad, adc), fNLCH, 0, fNLCH);
            hisSumFFT[lad][adc] = new TH1F(Form("hisSumFFT_lad%d_adc%d", lad, adc), Form("Cumulative FFT ladder %d ADC %d", lad, adc), fNLCH, 0, fNLCH);
            hisSumFFTnorm[lad][adc] = new TH1F(Form("hisSumFFTnorm_lad%d_adc%d", lad, adc), Form("Normalized Cumulative FFT ladder %d ADC %d", lad, adc), fNLCH, 0, fNLCH);
        }

        hisOccupancy[lad] = new TH1F(Form("hisOccupancy_lad%d", lad), Form("hisOccupancy_lad%d", lad), fNLCH, 0, fNLCH);
        hisOccupancy[lad]->SetFillStyle(3001);
        hisOccupancy[lad]->SetFillColor(kAzure - 8);
        hisOccupancy[lad]->SetLineColor(kBlue + 2);

        hisOccupancyCut[lad] = new TH1F(Form("hisOccupancyCut_lad%d", lad), Form("hisOccupancyCut_lad%d", lad), fNLCH, 0, fNLCH);
        hisOccupancyCut[lad]->SetFillStyle(3001);
        hisOccupancyCut[lad]->SetFillColor(kAzure - 8);
        hisOccupancyCut[lad]->SetLineColor(kBlue + 2);

        hisCog[lad] = new TH1F(Form("hisCog_lad%d", lad), Form("hisCog_lad%d", lad), fNLCH * 10, 0, fNLCH);
        hisCog[lad]->SetFillStyle(3001);
        hisCog[lad]->SetFillColor(kAzure - 8);
        hisCog[lad]->SetLineColor(kBlue + 2);

        hisCogCut[lad] = new TH1F(Form("hisCogCut_lad%d", lad), Form("hisCogCut_lad%d", lad), fNLCH * 10, 0, fNLCH);
        hisCogCut[lad]->SetFillStyle(3001);
        hisCogCut[lad]->SetFillColor(kAzure - 8);
        hisCogCut[lad]->SetLineColor(kBlue + 2);

        hisLens[lad] = new TH1F(Form("hisLens_lad%d", lad), Form("hisLens_lad%d", lad), 2 * fNADCCH, 0, 2 * fNADCCH);
        hisLens[lad]->SetFillStyle(3001);
        hisLens[lad]->SetFillColor(kAzure - 8);
        hisLens[lad]->SetLineColor(kBlue + 2);

        hisLensCut[lad] = new TH1F(Form("hisLensCut_lad%d", lad), Form("hisLensCut_lad%d", lad), 2 * fNADCCH, 0, 2 * fNADCCH);
        hisLensCut[lad]->SetFillStyle(3001);
        hisLensCut[lad]->SetFillColor(kAzure - 8);
        hisLensCut[lad]->SetLineColor(kBlue + 2);

        hisIntS[lad] = new TH1F(Form("hisIntS_lad%d", lad), Form("hisIntS_lad%d", lad), 1000, 0, 500);
        hisIntS[lad]->SetFillStyle(3001);
        hisIntS[lad]->SetFillColor(kAzure - 8);
        hisIntS[lad]->SetLineColor(kBlue + 2);

        hisIntSCut[lad] = new TH1F(Form("hisIntSCut_lad%d", lad), Form("hisIntSCut_lad%d", lad), 1000, 0, 500);
        hisIntSCut[lad]->SetFillStyle(3001);
        hisIntSCut[lad]->SetFillColor(kAzure - 8);
        hisIntSCut[lad]->SetLineColor(kBlue + 2);
        //hisIntK[lad]=new TH1F(Form("hisIntK_lad%d",lad),Form("hisIntK_lad%d",lad),2000,0,2000);
        //hisIntK[lad]->SetFillStyle(3001);
        //hisIntK[lad]->SetFillColor(kAzure-8);
        //hisIntK[lad]->SetLineColor(kBlue+2);

        hisNclus[lad] = new TH1F(Form("hisnCluS_lad%d", lad), Form("hisnCluS_lad%d", lad), 50, 0, 50);
        hisNclus[lad]->SetFillStyle(3001);
        hisNclus[lad]->SetFillColor(kAzure - 8);
        hisNclus[lad]->SetLineColor(kBlue + 2);

        hisLenVsCog[lad] = new TH2F(Form("hisLenVsCog_lad%d", lad), Form("Len vs cog - Ladder %d; channel;", lad), fNLCH * 4, 0, fNLCH, 2 * fNADCCH, 0, 2 * fNADCCH);
        hisLenVsCogCut[lad] = new TH2F(Form("hisLenVsCogCut_lad%d", lad), Form("Len vs cog (selection) - Ladder %d; channel;", lad), fNLCH * 4, 0, fNLCH, 2 * fNADCCH, 0, 2 * fNADCCH);

        hisIntVsCog[lad] = new TH2F(Form("hisIntVsCog_lad%d", lad), Form("Int vs cog - Ladder %d; channel;", lad), fNLCH * 4, 0, fNLCH, 5000, 0, 10000);
        hisIntVsCogCut[lad] = new TH2F(Form("hisIntVsCogCut_lad%d", lad), Form("Int vs cog (selection) - Ladder %d; channel;", lad), fNLCH * 4, 0, fNLCH, 5000, 0, 10000);

        grclusters[lad] = new TGraph();
        grclusters[lad]->SetName(Form("clusters_ladder_%d", lad));
        grclusters[lad]->SetMarkerStyle(23);
        grclusters[lad]->SetMarkerColor(kRed);
    }

    //for (int i=0; i<fNLCH; i++) fIndex[i]=i;
}

void MyMainFrame::ResetHistos()
{

    for (int lad = 0; lad < NLAD; lad++)
    {
        for (int ch = 0; ch < fNTOTCH + 1; ch++)
        {
            vHisChHi[lad].at(ch)->Reset();
            vHisChHiPe[lad].at(ch)->Reset();
        }
    }

    for (int lad = 0; lad < NLAD; lad++)
    {
        for (int adc{0}; adc < NADC; adc++)
        {
            hisFFT[lad][adc]->Reset();
            hisFFTnorm[lad][adc]->Reset();
            hisSumFFT[lad][adc]->Reset();
            hisSumFFTnorm[lad][adc]->Reset();
        }
        hisOccupancy[lad]->Reset();
        hisOccupancyCut[lad]->Reset();
        hisCog[lad]->Reset();
        hisLens[lad]->Reset();
        hisIntS[lad]->Reset();
        hisNclus[lad]->Reset();
        hisCogCut[lad]->Reset();
        hisLensCut[lad]->Reset();
        hisIntSCut[lad]->Reset();
        hisNclus[lad]->Reset();
        hisLenVsCog[lad]->Reset();
        hisLenVsCog[lad]->ProfileX()->Reset();
        hisLenVsCogCut[lad]->Reset();
        hisLenVsCogCut[lad]->ProfileX()->Reset();
        hisIntVsCog[lad]->Reset();
        hisIntVsCog[lad]->ProfileX()->Reset();
        hisIntVsCogCut[lad]->Reset();
        hisIntVsCogCut[lad]->ProfileX()->Reset();
    }
}

void MyMainFrame::ResetGraph(TGraph *gr)
{

    if (gr == 0)
        return;

    gr->Set(0);

    //for (int i=0; i<gr->GetN(); i++) gr->RemovePoint(0);
}

void MyMainFrame::UpdateName()
{
    fFilename = fTeFile->GetText();
    if (OpenFile())
    {
        AddLogMessage(Form("Error with file %s", fFilename.c_str()));
        fTeFile->SetBackgroundColor(0xfecccd);
    }
    else
    {
        AddLogMessage(Form("File %s is successfully open", fFilename.c_str()));
        fTeFile->SetBackgroundColor(0xcdffcf);
    }
}

void MyMainFrame::AddAdcTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEadccanvas = new TRootEmbeddedCanvas("Eadccanvas", Vframe, fWidth, fHeight);
    fAdcCanvas = fEadccanvas->GetCanvas();
    fAdcCanvas->Clear();
    //fAdcCanvas->Divide(3,2);

    fCanvasNames.push_back("ADC");
    fCanvasList.push_back(fAdcCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fAdcCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fAdcAxis[0], fAdcAxis[2], fAdcAxis[1], fAdcAxis[3]);
        frame->SetTitle(Form("ADC - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEadcax[i] = new TGNumberEntry(Hframe, fAdcAxis[i]);
        TGNEadcax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateAdcAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEadcax[i], TGLHleft);
    }

    Vframe->AddFrame(fEadccanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddOccupancyTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEoccupancycanvas = new TRootEmbeddedCanvas("Eoccupancycanvas", Vframe, fWidth, fHeight);
    fOccupancyCanvas = fEoccupancycanvas->GetCanvas();
    fOccupancyCanvas->Clear();
    fOccupancyCanvas->ToggleEventStatus();
    //fOccupancyCanvas->Divide(3,2);

    fCanvasNames.push_back("OCCUPANCY");
    fCanvasList.push_back(fOccupancyCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fOccupancyCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fOccupancyAxis[0], fOccupancyAxis[2], fOccupancyAxis[1], fOccupancyAxis[3]);
        frame->SetTitle(Form("Occupancy - Ladder %d; channel;", lad));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEoccupancyax[i] = new TGNumberEntry(Hframe, fOccupancyAxis[i]);
        TGNEoccupancyax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateOccupancyAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEoccupancyax[i], TGLHleft);
    }

    TGCBshowOccCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowOccCut->Connect("Clicked()", "MyMainFrame", this, "DisplayOccupancy()");
    Hframe->AddFrame(TGCBshowOccCut, TGLHleft);

    Vframe->AddFrame(fEoccupancycanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddCogTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEcogcanvas = new TRootEmbeddedCanvas("Ecogcanvas", Vframe, fWidth, fHeight);
    fCogCanvas = fEcogcanvas->GetCanvas();
    fCogCanvas->Clear();
    //fCogCanvas->Divide(3,2);

    fCanvasNames.push_back("COG");
    fCanvasList.push_back(fCogCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fCogCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fCogAxis[0], fCogAxis[2], fCogAxis[1], fCogAxis[3]);
        frame->SetTitle(Form("Center of gravity - Ladder %d; channel;", lad));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEcogax[i] = new TGNumberEntry(Hframe, fCogAxis[i]);
        TGNEcogax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCogAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEcogax[i], TGLHleft);
    }

    TGCBshowCogCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowCogCut->Connect("Clicked()", "MyMainFrame", this, "DisplayCog()");
    Hframe->AddFrame(TGCBshowCogCut, TGLHleft);

    Vframe->AddFrame(fEcogcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddLensTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fElenscanvas = new TRootEmbeddedCanvas("Elenscanvas", Vframe, fWidth, fHeight);
    fLensCanvas = fElenscanvas->GetCanvas();
    fLensCanvas->Clear();
    //fLensCanvas->Divide(3,2);

    fCanvasNames.push_back("LENS");
    fCanvasList.push_back(fLensCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fLensCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fLensAxis[0], fLensAxis[2], fLensAxis[1], fLensAxis[3]);
        frame->SetTitle(Form("S-Cluster length - Ladder %d; channel;", lad));
        //LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNElensax[i] = new TGNumberEntry(Hframe, fLensAxis[i]);
        TGNElensax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateLensAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNElensax[i], TGLHleft);
    }

    TGCBshowLensCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowLensCut->Connect("Clicked()", "MyMainFrame", this, "DisplayLens()");
    Hframe->AddFrame(TGCBshowLensCut, TGLHleft);

    Vframe->AddFrame(fElenscanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddLenVsCogTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fElenvscogcanvas = new TRootEmbeddedCanvas("Elenvscogcanvas", Vframe, fWidth, fHeight);
    fLenVsCogCanvas = fElenvscogcanvas->GetCanvas();
    fLenVsCogCanvas->Clear();
    //fLenVsCogCanvas->Divide(3,2);

    fCanvasNames.push_back("LENVSCOG");
    fCanvasList.push_back(fLenVsCogCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fLenVsCogCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fLenVsCogAxis[0], fLenVsCogAxis[2], fLenVsCogAxis[1], fLenVsCogAxis[3]);
        frame->SetTitle(Form("Cluster length vs cog - Ladder %d; channel;", lad));
        //LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNElenvscogax[i] = new TGNumberEntry(Hframe, fLenVsCogAxis[i]);
        TGNElenvscogax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateLenVsCogAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNElenvscogax[i], TGLHleft);
    }

    TGCBshowProfileLenVsCog = new TGCheckButton(Hframe, "Show x-profile");
    TGCBshowProfileLenVsCog->Connect("Clicked()", "MyMainFrame", this, "DisplayLenVsCog()");
    Hframe->AddFrame(TGCBshowProfileLenVsCog, TGLHleft);

    TGCBshowLenVsCogCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowLenVsCogCut->Connect("Clicked()", "MyMainFrame", this, "DisplayLenVsCog()");
    Hframe->AddFrame(TGCBshowLenVsCogCut, TGLHleft);

    Vframe->AddFrame(fElenvscogcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddIntVsCogTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEintvscogcanvas = new TRootEmbeddedCanvas("Eintvscogcanvas", Vframe, fWidth, fHeight);
    fIntVsCogCanvas = fEintvscogcanvas->GetCanvas();
    fIntVsCogCanvas->Clear();
    //fIntVsCogCanvas->Divide(3,2);

    fCanvasNames.push_back("INTVSCOG");
    fCanvasList.push_back(fIntVsCogCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fIntVsCogCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fIntVsCogAxis[0], fIntVsCogAxis[2], fIntVsCogAxis[1], fIntVsCogAxis[3]);
        frame->SetTitle(Form("Cluster integral vs cog - Ladder %d; channel;", lad));
        //LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEintvscogax[i] = new TGNumberEntry(Hframe, fIntVsCogAxis[i]);
        TGNEintvscogax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateIntVsCogAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEintvscogax[i], TGLHleft);
    }

    TGCBshowProfileIntVsCog = new TGCheckButton(Hframe, "Show x-profile");
    TGCBshowProfileIntVsCog->Connect("Clicked()", "MyMainFrame", this, "DisplayIntVsCog()");
    Hframe->AddFrame(TGCBshowProfileIntVsCog, TGLHleft);

    TGCBshowIntVsCogCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowIntVsCogCut->Connect("Clicked()", "MyMainFrame", this, "DisplayIntVsCog()");
    Hframe->AddFrame(TGCBshowIntVsCogCut, TGLHleft);

    Vframe->AddFrame(fEintvscogcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddIntsTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEintscanvas = new TRootEmbeddedCanvas("Eintscanvas", Vframe, fWidth, fHeight);
    fIntsCanvas = fEintscanvas->GetCanvas();
    fIntsCanvas->Clear();
    //fIntsCanvas->Divide(3,2);

    fCanvasNames.push_back("INTS");
    fCanvasList.push_back(fIntsCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fIntsCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fIntsAxis[0], fIntsAxis[2], fIntsAxis[1], fIntsAxis[3]);
        frame->SetTitle(Form("Integral S-side - Ladder %d; cluster integral (ADCc);", lad));
        //LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEintsax[i] = new TGNumberEntry(Hframe, fIntsAxis[i]);
        TGNEintsax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateIntsAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEintsax[i], TGLHleft);
    }

    TGCBshowIntsCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowIntsCut->Connect("Clicked()", "MyMainFrame", this, "DisplayInts()");
    Hframe->AddFrame(TGCBshowIntsCut, TGLHleft);

    Vframe->AddFrame(fEintscanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddNclusTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEncluscanvas = new TRootEmbeddedCanvas("ENcluscanvas", Vframe, fWidth, fHeight);
    fNclusCanvas = fEncluscanvas->GetCanvas();
    fNclusCanvas->Clear();
    //fIntsCanvas->Divide(3,2);

    fCanvasNames.push_back("NCLUS");
    fCanvasList.push_back(fNclusCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fNclusCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fNclusAxis[0], fNclusAxis[2], fNclusAxis[1], fNclusAxis[3]);
        frame->SetTitle(Form("N of clusters S-side - Ladder %d; channel;", lad));
        //LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEnclusax[i] = new TGNumberEntry(Hframe, fNclusAxis[i]);
        TGNEnclusax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateNclusAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEnclusax[i], TGLHleft);
    }

    Vframe->AddFrame(fEncluscanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddIntkTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEintkcanvas = new TRootEmbeddedCanvas("Eintkcanvas", Vframe, fWidth, fHeight);
    fIntkCanvas = fEintkcanvas->GetCanvas();
    fIntkCanvas->Clear();
    //fIntkCanvas->Divide(3,2);

    fCanvasNames.push_back("INTK");
    fCanvasList.push_back(fIntkCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fIntkCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fIntkAxis[0], fIntkAxis[2], fIntkAxis[1], fIntkAxis[3]);
        frame->SetTitle(Form("Integral S-side - Ladder %d; channel;", lad));
        //LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEintkax[i] = new TGNumberEntry(Hframe, fIntkAxis[i]);
        TGNEintkax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateIntkAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEintkax[i], TGLHleft);
    }

    Vframe->AddFrame(fEintkcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddFftTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEfftcanvas = new TRootEmbeddedCanvas("Efftcanvas", Vframe, fWidth, fHeight);
    fFftCanvas = fEfftcanvas->GetCanvas();
    fFftCanvas->Clear();
    //fFftCanvas->Divide(3,2);

    fCanvasNames.push_back("FFT");
    fCanvasList.push_back(fFftCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fFftCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fFftAxis[0], fFftAxis[2], fFftAxis[1], fFftAxis[3]);
        frame->SetTitle(Form("FFT - Ladder %d; frequency;", lad));
        //LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min x:");
    minmax[1] = new TGLabel(Hframe, "max x:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEfftax[i] = new TGNumberEntry(Hframe, fFftAxis[i]);
        TGNEfftax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateFftAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEfftax[i], TGLHleft);
    }

    //  normlabel=new TGLabel(Hframe,"
    TGCBShowFFTNorm = new TGCheckButton(Hframe, "Normalized");
    TGCBShowFFTNorm->SetDown();
    Hframe->AddFrame(TGCBShowFFTNorm, TGLHleft);

    TGCBShowFFTSum = new TGCheckButton(Hframe, "Cumulative");
    TGCBShowFFTSum->SetDown();
    Hframe->AddFrame(TGCBShowFFTSum, TGLHleft);

    Vframe->AddFrame(fEfftcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddPedSubtTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEpedsubtcanvas = new TRootEmbeddedCanvas("Epedsubtcanvas", Vframe, fWidth, fHeight);
    fPedSubtCanvas = fEpedsubtcanvas->GetCanvas();
    fPedSubtCanvas->Clear();
    //fPedSubtCanvas->Divide(3,2);

    fCanvasNames.push_back("PedSub");
    fCanvasList.push_back(fPedSubtCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fPedSubtCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fPedSubtAxis[0], fPedSubtAxis[2], fPedSubtAxis[1], fPedSubtAxis[3]);
        frame->SetTitle(Form("Ped. subt. - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEpedsubtax[i] = new TGNumberEntry(Hframe, fPedSubtAxis[i]);
        TGNEpedsubtax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePedSubtAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEpedsubtax[i], TGLHleft);
    }

    Vframe->AddFrame(fEpedsubtcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddAdcPedSubtTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEadcpedsubtcanvas = new TRootEmbeddedCanvas("Eadcpedsubtcanvas", Vframe, fWidth, fHeight);
    fAdcPedSubtCanvas = fEadcpedsubtcanvas->GetCanvas();
    fAdcPedSubtCanvas->Clear();
    fAdcPedSubtCanvas->Divide(4, 2);

    fCanvasNames.push_back("AdcPedSub");
    fCanvasList.push_back(fAdcPedSubtCanvas);

    for (int adc = 0; adc < 2 * NADC; adc++)
    {
        fAdcPedSubtCanvas->cd(adc + 1);
        TH1F *frame = gPad->DrawFrame(fAdcPedSubtAxis[0], fAdcPedSubtAxis[2], fAdcPedSubtAxis[1], fAdcPedSubtAxis[3]);
        frame->SetTitle(Form("Adc Ped. subt. - ADC %d; readings;", adc));
        LinesAdcs();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEadcpedsubtax[i] = new TGNumberEntry(Hframe, fAdcPedSubtAxis[i]);
        TGNEadcpedsubtax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateAdcPedSubtAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEadcpedsubtax[i], TGLHleft);
    }

    Vframe->AddFrame(fEadcpedsubtcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddAdcPedCnSubtTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEadcpedcnsubtcanvas = new TRootEmbeddedCanvas("Eadcpedcnsubtcanvas", Vframe, fWidth, fHeight);
    fAdcPedCnSubtCanvas = fEadcpedcnsubtcanvas->GetCanvas();
    fAdcPedCnSubtCanvas->Clear();
    fAdcPedCnSubtCanvas->Divide(4, 2);

    fCanvasNames.push_back("AdcPedSub");
    fCanvasList.push_back(fAdcPedCnSubtCanvas);

    for (int adc = 0; adc < 2 * NADC; adc++)
    {
        fAdcPedCnSubtCanvas->cd(adc + 1);
        TH1F *frame = gPad->DrawFrame(fAdcPedCnSubtAxis[0], fAdcPedCnSubtAxis[2], fAdcPedCnSubtAxis[1], fAdcPedCnSubtAxis[3]);
        frame->SetTitle(Form("Adc signal - ADC %d; readings;", adc));
        LinesAdcs();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEadcpedcnsubtax[i] = new TGNumberEntry(Hframe, fAdcPedCnSubtAxis[i]);
        TGNEadcpedcnsubtax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateAdcPedCnSubtAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEadcpedcnsubtax[i], TGLHleft);
    }

    Vframe->AddFrame(fEadcpedcnsubtcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddPedCnSubtTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEpedcnsubtcanvas = new TRootEmbeddedCanvas("Epedcnsubtcanvas", Vframe, fWidth, fHeight);
    fPedCnSubtCanvas = fEpedcnsubtcanvas->GetCanvas();
    fPedCnSubtCanvas->Clear();
    //fPedCnSubtCanvas->Divide(3,2);

    fCanvasNames.push_back("CNSub");
    fCanvasList.push_back(fPedCnSubtCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fPedCnSubtCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fPedCnSubtAxis[0], fPedCnSubtAxis[2], fPedCnSubtAxis[1], fPedCnSubtAxis[3]);
        frame->SetTitle(Form("Signals - Ladder %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEpedcnsubtax[i] = new TGNumberEntry(Hframe, fPedCnSubtAxis[i]);
        TGNEpedcnsubtax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePedCnSubtAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEpedcnsubtax[i], TGLHleft);
    }

    Vframe->AddFrame(fEpedcnsubtcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddPedTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEpedcanvas = new TRootEmbeddedCanvas("Epedcanvas", Vframe, fWidth, fHeight);
    fPedCanvas = fEpedcanvas->GetCanvas();
    fPedCanvas->Clear();
    //fPedCanvas->Divide(3,2);

    fCanvasNames.push_back("Pedestals");
    fCanvasList.push_back(fPedCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fPedCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fPedAxis[0], fPedAxis[2], fPedAxis[1], fPedAxis[3]);
        frame->SetTitle(Form("Pedestals - Ladder %d; channels;", lad));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEpedax[i] = new TGNumberEntry(Hframe, fPedAxis[i]);
        TGNEpedax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePedAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEpedax[i], TGLHleft);
    }

    TGCBshowrefped = new TGCheckButton(Hframe, "Show reference");
    TGCBshowrefped->Connect("Clicked()", "MyMainFrame", this, "DisplayPedestals()");

    Hframe->AddFrame(TGCBshowrefped, TGLHleft);

    Vframe->AddFrame(fEpedcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddPedCompTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEpedcompcanvas = new TRootEmbeddedCanvas("Epedcompcanvas", Vframe, fWidth, fHeight);
    fPedCompCanvas = fEpedcompcanvas->GetCanvas();
    fPedCompCanvas->Clear();
    //fPedCompCanvas->Divide(3,2);

    fCanvasNames.push_back("PedComp");
    fCanvasList.push_back(fPedCompCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fPedCompCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fPedCompAxis[0], fPedCompAxis[2], fPedCompAxis[1], fPedCompAxis[3]);
        frame->SetTitle(Form("Ped. diff. - Ladder %d; channels;", lad));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEpedcompax[i] = new TGNumberEntry(Hframe, fPedCompAxis[i]);
        TGNEpedcompax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePedCompAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEpedcompax[i], TGLHleft);
    }

    Vframe->AddFrame(fEpedcompcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddSrawTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEsrawcanvas = new TRootEmbeddedCanvas("Esrawcanvas", Vframe, fWidth, fHeight);
    fSrawCanvas = fEsrawcanvas->GetCanvas();
    fSrawCanvas->Clear();
    //fSrawCanvas->Divide(3,2);

    fCanvasNames.push_back("RawSigmas");
    fCanvasList.push_back(fSrawCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fSrawCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fSrawAxis[0], fSrawAxis[2], fSrawAxis[1], fSrawAxis[3]);
        frame->SetTitle(Form("Raw sigmas- Ladder %d; channels;", lad));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEsrawax[i] = new TGNumberEntry(Hframe, fSrawAxis[i]);
        TGNEsrawax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateSrawAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEsrawax[i], TGLHleft);
    }

    Vframe->AddFrame(fEsrawcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddSigmaTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEsigmacanvas = new TRootEmbeddedCanvas("Esigmacanvas", Vframe, fWidth, fHeight);
    fSigmaCanvas = fEsigmacanvas->GetCanvas();
    fSigmaCanvas->Clear();
    //fSigmaCanvas->Divide(3,2);

    fCanvasNames.push_back("Sigmas");
    fCanvasList.push_back(fSigmaCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fSigmaCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fSigmaAxis[0], fSigmaAxis[2], fSigmaAxis[1], fSigmaAxis[3]);
        frame->SetTitle(Form("Sigmas - Ladder %d; channels;", lad));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEsigmaax[i] = new TGNumberEntry(Hframe, fSigmaAxis[i]);
        TGNEsigmaax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateSigmaAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEsigmaax[i], TGLHleft);
    }

    TGCBshowrefsig = new TGCheckButton(Hframe, "Show reference");
    TGCBshowrefsig->Connect("Clicked()", "MyMainFrame", this, "DisplaySigmas()");

    Hframe->AddFrame(TGCBshowrefsig, TGLHleft);

    Vframe->AddFrame(fEsigmacanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddSigmaCompTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEsigmacompcanvas = new TRootEmbeddedCanvas("Esigmacompcanvas", Vframe, fWidth, fHeight);
    fSigmaCompCanvas = fEsigmacompcanvas->GetCanvas();
    fSigmaCompCanvas->Clear();
    //fSigmaCompCanvas->Divide(3,2);

    fCanvasNames.push_back("SigmaComp");
    fCanvasList.push_back(fSigmaCompCanvas);

    for (int lad = 0; lad < NLAD; lad++)
    {
        fSigmaCompCanvas->cd(CONVERT[lad]);
        TH1F *frame = gPad->DrawFrame(fSigmaCompAxis[0], fSigmaCompAxis[2], fSigmaCompAxis[1], fSigmaCompAxis[3]);
        frame->SetTitle(Form("Sigmas comparison - Ladder %d; channels;", lad));
        LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGLabel *minmax[4];
    minmax[0] = new TGLabel(Hframe, "min ch:");
    minmax[1] = new TGLabel(Hframe, "max ch:");
    minmax[2] = new TGLabel(Hframe, "min y:");
    minmax[3] = new TGLabel(Hframe, "max y:");

    for (int i = 0; i < 4; i++)
    {
        TGNEsigmacompax[i] = new TGNumberEntry(Hframe, fSigmaCompAxis[i]);
        TGNEsigmacompax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateSigmaCompAxes()");
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEsigmacompax[i], TGLHleft);
    }

    Vframe->AddFrame(fEsigmacompcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddLogTab(TGCompositeFrame *cframe)
{

    TGTVlog = new TGTextView(cframe);
    cframe->AddFrame(TGTVlog, TGLHexpandXexpandY);

    fCanvasNames.push_back("Log");
    fCanvasList.push_back(0);
}

void MyMainFrame::AddSettingsTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);
    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe2 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe3 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe4 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe5 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe6 = new TGHorizontalFrame(Vframe, 200, 40);

    TGLabel *labelExcludeEvents = new TGLabel(Hframe, "First events to exclude: ");
    TGNEexcludeevents = new TGNumberEntry(Hframe, ExcludeEvents);
    TGNEexcludeevents->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateExcludeEvents()");

    Hframe->AddFrame(labelExcludeEvents, TGLHleft);
    Hframe->AddFrame(TGNEexcludeevents, TGLHleft);

    TGLabel *labelCNcutval = new TGLabel(Hframe, "Common noise signal exclusion threshold: ");
    TGNEcncutval = new TGNumberEntry(Hframe, CNcutval);
    TGNEcncutval->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCNcutval()");

    Hframe->AddFrame(labelCNcutval, TGLHleft);
    Hframe->AddFrame(TGNEcncutval, TGLHleft);
    //
    TGLabel *labelCluHighThresh = new TGLabel(Hframe2, "Cluster high threshold (ADCc): ");
    TGNEcluHighThresh = new TGNumberEntry(Hframe2, fCluHighThresh);
    TGNEcluHighThresh->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCluHighThresh()");

    Hframe2->AddFrame(labelCluHighThresh, TGLHleft);
    Hframe2->AddFrame(TGNEcluHighThresh, TGLHleft);
    //
    TGLabel *labelCluLowThresh = new TGLabel(Hframe3, "Cluster low threshold (ADCc): ");
    TGNEcluLowThresh = new TGNumberEntry(Hframe3, fCluLowThresh);
    TGNEcluLowThresh->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCluLowThresh()");

    Hframe3->AddFrame(labelCluLowThresh, TGLHleft);
    Hframe3->AddFrame(TGNEcluLowThresh, TGLHleft);

    TGLabel *labelMinValidIntegral = new TGLabel(Hframe4, "Minimum cluster integral (noise cluster exclusion threshold, ADCc): ");
    TGNEminValidIntegral = new TGNumberEntry(Hframe4, fMinValidIntegral);
    TGNEminValidIntegral->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateMinValidIntegral()");

    Hframe4->AddFrame(labelMinValidIntegral, TGLHleft);
    Hframe4->AddFrame(TGNEminValidIntegral, TGLHleft);

    //TGLabel *labelNegativeSignals= new TGLabel(Hframe5, "Negative signals (debug mode): ");
    TGCBNegativeSignals = new TGCheckButton(Hframe5, "Negative signals (debug mode)");
    TGCBNegativeSignals->SetDown(1);
    TGCBStripMode = new TGCheckButton(Hframe5, "Strip mode");
    TGCBStripMode->Connect("Clicked()", "MyMainFrame", this, "UpdateChanLabel()");
    TGCBStripMode->SetDown(fChanLabelMode);
    //TGNEpixelSearchParam = new TGNumberEntry(Hframe5, fPixelSearchParam);
    //TGNEpixelSearchParam->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePixelSearchParam()");

    //Hframe5->AddFrame(labelNegativeSignals, TGLHleft);
    Hframe5->AddFrame(TGCBNegativeSignals, TGLHleft);
    Hframe5->AddFrame(TGCBStripMode, TGLHleft);
    //Hframe6->AddFrame(labelPixelSearchRebin, TGLHleft);
    //Hframe6->AddFrame(TGNEpixelSearchRebin, TGLHleft);

    Vframe->AddFrame(Hframe, TGLHexpandX);
    Vframe->AddFrame(Hframe2, TGLHexpandX);
    Vframe->AddFrame(Hframe3, TGLHexpandX);
    Vframe->AddFrame(Hframe4, TGLHexpandX);
    Vframe->AddFrame(Hframe5, TGLHexpandX);
    Vframe->AddFrame(Hframe6, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHleftTop);

    fCanvasNames.push_back("Settings");
    fCanvasList.push_back(0);
}

void MyMainFrame::AddLogMessage(string message)
{

    TDatime now;
    now.Set();
    string datim{now.AsSQLString()};

    message = datim + " : " + message;

    TGTVlog->AddLine(message.c_str());
}

void MyMainFrame::LinesVas(int drawline)
{

    gPad->Update();
    Double_t xmin, ymin, xmax, ymax;
    gPad->GetRangeAxis(xmin, ymin, xmax, ymax);
    TLine *line = new TLine();
    int color = kBlue;
    line->SetLineColor(color);
    //TText *text=new TText();
    //text->SetTextAlign(21); // vert and hor. alignment: horiz centered, top
    //text->SetTextColor(kRed);
    //line->SetLineStyle(3);
    for (Int_t va = 1; va < fNVA + 1; va++)
    { // fisrt and last sep. lines are not drawn
        line->SetLineStyle((va % 4 == 0) ? 1 : 3);
        //if (va == 7)
        //  color = kRed;
        line->SetLineColor(color);
        if (va * 64 > xmin && va * 64 < xmax)
            line->DrawLine(va * 64, ymin, va * 64, ymax);
        if ((va - 0.5) * 64 > xmin && (va - 0.5) * 64 <= xmax)
        {
            TText *text = new TText((va - 0.5) * 64, ymax + (ymax - ymin) * 0.01, Form("%d", va));
            text->SetTextSize(0.03);
            text->SetName(Form("textva%d", va));
            text->SetTextAlign(21); // vert and hor. alignment: horiz centered, top
            text->SetTextColor(color);
            text->Draw();
        }
    }
    if (drawline)
    {
        line->SetLineColor(kBlack);
        line->SetLineStyle(1);
        line->DrawLine(xmin, 0, xmax, 0);
    }
}

void MyMainFrame::LinesAdcs(int drawline)
{

    gPad->Update();
    Double_t xmin, ymin, xmax, ymax;
    gPad->GetRangeAxis(xmin, ymin, xmax, ymax);
    TLine *line = new TLine();
    int color = kBlue;
    line->SetLineColor(color);
    //TText *text=new TText();
    //text->SetTextAlign(21); // vert and hor. alignment: horiz centered, top
    //text->SetTextColor(kRed);
    //line->SetLineStyle(3);
    for (Int_t read = 1; read < 4; read++)
    { // fisrt and last sep. lines are not drawn
        line->SetLineStyle(3);
        if (read * 192 > xmin && read * 192 < xmax)
            line->DrawLine(read * 192, ymin, read * 192, ymax);
        if ((read - 0.5) * 192 > xmin && (read - 0.5) * 192 <= xmax)
        {
            TText *text = new TText((read - 0.5) * 192, ymax + (ymax - ymin) * 0.01, Form("%d", read));
            text->SetName(Form("textread%d", read));
            text->SetTextAlign(21); // vert and hor. alignment: horiz centered, top
            text->SetTextColor(color);
            text->Draw();
        }
    }
    if (drawline)
    {
        line->SetLineColor(kBlack);
        line->SetLineStyle(1);
        line->DrawLine(xmin, 0, xmax, 0);
    }
}

void MyMainFrame::CloseWindow()
{

    SaveConfiguration();

    gApplication->Terminate(0);
}

void MyMainFrame::DisplayThisEvent()
{

    //cout << "display this event" << endl;

    int nevent{(int)TGNEevent->GetNumber()};

    if (nevent > fNevents - 1)
        nevent = fNevents - 1;

    if (nevent < 0)
        nevent = 0;

    fEventCounter = nevent;

    RefreshDisplay();
}

void MyMainFrame::InitRefreshTimer()
{

    fRefreshtimer = new TTimer(0); // refresh every 1 ms
    fRefreshtimer->Connect("Timeout()", "MyMainFrame", this, "RefreshDisplay()");
    //refreshtimer->Start();
}

void MyMainFrame::StartEventTrigger()
{

    fRefreshtimer->Start();
}

void MyMainFrame::StopEventTrigger()
{

    fRefreshtimer->Stop();
}

void MyMainFrame::UpdateSuperImpose()
{

    //fClearDisplay=fSuperImposed;

    ClearSuperimposedGraphs(fAdcCanvas, fEventCounter, fSuperImposed);
    ClearSuperimposedGraphs(fPedSubtCanvas, fEventCounter, fSuperImposed);
    fSuperImposed = TGNEsuperimpose->GetNumber();
}

//void MyMainFrame::UpdateEventFilter(){
//
// int selmode{TGCBEventSel->GetSelected()};
//    cout << " selmode = " << selmode << endl;
//
//}

void MyMainFrame::RefreshDisplay()
{

    if (fEventCounter == fNevents)
        return;

    int selmode{TGCBEventSel->GetSelected()};
    //cout << " selmode = " << selmode << endl;

    if ((selmode == 1 and (fEventCounter % 2) == 0)    // we accept odd indices
        or (selmode == 2 and (fEventCounter % 2) == 1) // we accept even indices
        or selmode == 0)                               // we accept everything
    {
        TGNEevent->SetNumber(fEventCounter);
        if (fEventCounter == fNevents - 1)
            TGNEevent->SetNumber(fEventCounter);
        fTree->GetEntry(fEventCounter);

        BuildGraph();
        int current = fDataTab->GetCurrent();
        if (current == 0)
            UpdateAdcDisplay(fEventCounter);
        if (current == 1)
            UpdatePedSubtDisplay(fEventCounter);
        if (current == 2)
            UpdatePedCnSubtDisplay(fEventCounter);
        //if (current==3) UpdateChHiDisplay(fEventCounter);
        //if (current==3) UpdateAdcPedSubtDisplay(fEventCounter);
        //if (current==4) UpdateAdcPedCnSubtDisplay(fEventCounter);
        //if (current==4) UpdateOccupancyDisplay(fEventCounter);
        //if (current==5) UpdateCogDisplay(fEventCounter);
        //if (current==6) UpdateLensDisplay(fEventCounter);
        //if (current==8) UpdateLenVsCogDisplay(fEventCounter);
        //if (current==7) UpdateIntsDisplay(fEventCounter);
        //if (current==10) UpdateIntkDisplay(fEventCounter);
        if (current == 10)
            UpdateFftDisplay();
    }
    fEventCounter++;
    if (fEventCounter == fNevents)
    {
        //cout << "last event... stopping scan" << endl;
        StopEventTrigger();
        SetRunButtons(1, 0, 0);
        AddLogMessage("Run stopped");
    }
}

void MyMainFrame::UpdateMinValidIntegral()
{

    double newval{TGNEminValidIntegral->GetNumber()};

    if (newval > 0)
        fMinValidIntegral = newval;

    AddLogMessage(Form("Minimum cluster integral value has been set to %f", fMinValidIntegral));
}

void MyMainFrame::UpdateCluHighThresh()
{

    double newval{TGNEcluHighThresh->GetNumber()};

    if (newval > 0)
        fCluHighThresh = newval;

    AddLogMessage(Form("Cluster identifcation high threshold has been set to %f", fCluHighThresh));
}

void MyMainFrame::UpdateCluLowThresh()
{

    double newval{TGNEcluLowThresh->GetNumber()};

    if (newval > 0)
        fCluLowThresh = newval;

    AddLogMessage(Form("Cluster identifcation low threshold has been set to %f", fCluLowThresh));
}

void MyMainFrame::UpdatePixelSearchParam()
{

    double newval{TGNEpixelSearchParam->GetNumber()};

    if (newval > 0)
        fPixelSearchParam = newval;

    AddLogMessage(Form("Pixel search parameter has been set to %f", fPixelSearchParam));
}

void MyMainFrame::UpdatePixelSearchRebin()
{

    double newval{TGNEpixelSearchRebin->GetNumber()};

    if (newval > 0)
        fPixelSearchRebin = newval;

    AddLogMessage(Form("Pixel search rebin factor has been set to %f", fPixelSearchRebin));
}

void MyMainFrame::UpdateAdcAxes()
{

    double minx{TGNEadcax[0]->GetNumber()};
    double maxx{TGNEadcax[1]->GetNumber()};
    double miny{TGNEadcax[2]->GetNumber()};
    double maxy{TGNEadcax[3]->GetNumber()};

    if (minx >= maxx)
    {
        TGNEadcax[0]->SetNumber(fAdcAxis[0]);
        TGNEadcax[1]->SetNumber(fAdcAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEadcax[2]->SetNumber(fAdcAxis[2]);
        TGNEadcax[3]->SetNumber(fAdcAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fAdcAxis[i] = TGNEadcax[i]->GetNumber();

    UpdateAdcDisplay(fEventCounter, 1);
}

void MyMainFrame::UpdateOccupancyAxes()
{

    double minx = TGNEoccupancyax[0]->GetNumber();
    double maxx = TGNEoccupancyax[1]->GetNumber();
    double miny = TGNEoccupancyax[2]->GetNumber();
    double maxy = TGNEoccupancyax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEoccupancyax[0]->SetNumber(fOccupancyAxis[0]);
        TGNEoccupancyax[1]->SetNumber(fOccupancyAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEoccupancyax[2]->SetNumber(fOccupancyAxis[2]);
        TGNEoccupancyax[3]->SetNumber(fOccupancyAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fOccupancyAxis[i] = TGNEoccupancyax[i]->GetNumber();

    //UpdateOccupancyDisplay(fEventCounter,1);
    DisplayOccupancy();
}

void MyMainFrame::UpdateCogAxes()
{

    double minx = TGNEcogax[0]->GetNumber();
    double maxx = TGNEcogax[1]->GetNumber();
    double miny = TGNEcogax[2]->GetNumber();
    double maxy = TGNEcogax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEcogax[0]->SetNumber(fCogAxis[0]);
        TGNEcogax[1]->SetNumber(fCogAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEcogax[2]->SetNumber(fCogAxis[2]);
        TGNEcogax[3]->SetNumber(fCogAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fCogAxis[i] = TGNEcogax[i]->GetNumber();

    //UpdateCogDisplay(fEventCounter,1);
    DisplayCog();
}

void MyMainFrame::UpdateLensAxes()
{

    double minx = TGNElensax[0]->GetNumber();
    double maxx = TGNElensax[1]->GetNumber();
    double miny = TGNElensax[2]->GetNumber();
    double maxy = TGNElensax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNElensax[0]->SetNumber(fLensAxis[0]);
        TGNElensax[1]->SetNumber(fLensAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNElensax[2]->SetNumber(fLensAxis[2]);
        TGNElensax[3]->SetNumber(fLensAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fLensAxis[i] = TGNElensax[i]->GetNumber();

    //UpdateLensDisplay(fEventCounter,1);
    DisplayLens();
}

void MyMainFrame::UpdateLenVsCogAxes()
{

    double minx = TGNElenvscogax[0]->GetNumber();
    double maxx = TGNElenvscogax[1]->GetNumber();
    double miny = TGNElenvscogax[2]->GetNumber();
    double maxy = TGNElenvscogax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNElenvscogax[0]->SetNumber(fLenVsCogAxis[0]);
        TGNElenvscogax[1]->SetNumber(fLenVsCogAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNElenvscogax[2]->SetNumber(fLenVsCogAxis[2]);
        TGNElenvscogax[3]->SetNumber(fLenVsCogAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fLenVsCogAxis[i] = TGNElenvscogax[i]->GetNumber();

    DisplayLenVsCog();
    //UpdateLenVsCogDisplay(fEventCounter,1);
}

void MyMainFrame::UpdateIntVsCogAxes()
{

    double minx = TGNEintvscogax[0]->GetNumber();
    double maxx = TGNEintvscogax[1]->GetNumber();
    double miny = TGNEintvscogax[2]->GetNumber();
    double maxy = TGNEintvscogax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEintvscogax[0]->SetNumber(fIntVsCogAxis[0]);
        TGNEintvscogax[1]->SetNumber(fIntVsCogAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEintvscogax[2]->SetNumber(fIntVsCogAxis[2]);
        TGNEintvscogax[3]->SetNumber(fIntVsCogAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fIntVsCogAxis[i] = TGNEintvscogax[i]->GetNumber();

    DisplayIntVsCog();
    //UpdateLenVsCogDisplay(fEventCounter,1);
}

void MyMainFrame::UpdateIntsAxes()
{

    double minx = TGNEintsax[0]->GetNumber();
    double maxx = TGNEintsax[1]->GetNumber();
    double miny = TGNEintsax[2]->GetNumber();
    double maxy = TGNEintsax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEintsax[0]->SetNumber(fIntsAxis[0]);
        TGNEintsax[1]->SetNumber(fIntsAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEintsax[2]->SetNumber(fIntsAxis[2]);
        TGNEintsax[3]->SetNumber(fIntsAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fIntsAxis[i] = TGNEintsax[i]->GetNumber();

    //UpdateIntsDisplay(fEventCounter,1);
    DisplayInts();
}

void MyMainFrame::UpdateNclusAxes()
{

    double minx = TGNEnclusax[0]->GetNumber();
    double maxx = TGNEnclusax[1]->GetNumber();
    double miny = TGNEnclusax[2]->GetNumber();
    double maxy = TGNEnclusax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEnclusax[0]->SetNumber(fNclusAxis[0]);
        TGNEnclusax[1]->SetNumber(fNclusAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEnclusax[2]->SetNumber(fNclusAxis[2]);
        TGNEnclusax[3]->SetNumber(fNclusAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fNclusAxis[i] = TGNEnclusax[i]->GetNumber();

    //UpdateNclusDisplay(fEventCounter,1);
    DisplayNclus();
}

void MyMainFrame::UpdateFftAxes()
{

    double minx = TGNEfftax[0]->GetNumber();
    double maxx = TGNEfftax[1]->GetNumber();
    double miny = TGNEfftax[2]->GetNumber();
    double maxy = TGNEfftax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEfftax[0]->SetNumber(fFftAxis[0]);
        TGNEfftax[1]->SetNumber(fFftAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEfftax[2]->SetNumber(fFftAxis[2]);
        TGNEfftax[3]->SetNumber(fFftAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fFftAxis[i] = TGNEfftax[i]->GetNumber();

    UpdateFftDisplay(1);
}

void MyMainFrame::UpdatePedSubtAxes()
{

    double minx = TGNEpedsubtax[0]->GetNumber();
    double maxx = TGNEpedsubtax[1]->GetNumber();
    double miny = TGNEpedsubtax[2]->GetNumber();
    double maxy = TGNEpedsubtax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEpedsubtax[0]->SetNumber(fPedSubtAxis[0]);
        TGNEpedsubtax[1]->SetNumber(fPedSubtAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEpedsubtax[2]->SetNumber(fPedSubtAxis[2]);
        TGNEpedsubtax[3]->SetNumber(fPedSubtAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fPedSubtAxis[i] = TGNEpedsubtax[i]->GetNumber();

    UpdatePedSubtDisplay(fEventCounter, 1);
}

void MyMainFrame::UpdateAdcPedSubtAxes()
{

    double minx = TGNEadcpedsubtax[0]->GetNumber();
    double maxx = TGNEadcpedsubtax[1]->GetNumber();
    double miny = TGNEadcpedsubtax[2]->GetNumber();
    double maxy = TGNEadcpedsubtax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEadcpedsubtax[0]->SetNumber(fAdcPedSubtAxis[0]);
        TGNEadcpedsubtax[1]->SetNumber(fAdcPedSubtAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEadcpedsubtax[2]->SetNumber(fAdcPedSubtAxis[2]);
        TGNEadcpedsubtax[3]->SetNumber(fAdcPedSubtAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fAdcPedSubtAxis[i] = TGNEadcpedsubtax[i]->GetNumber();

    UpdateAdcPedSubtDisplay(fEventCounter, 1);
}

void MyMainFrame::UpdateAdcPedCnSubtAxes()
{

    double minx = TGNEadcpedcnsubtax[0]->GetNumber();
    double maxx = TGNEadcpedcnsubtax[1]->GetNumber();
    double miny = TGNEadcpedcnsubtax[2]->GetNumber();
    double maxy = TGNEadcpedcnsubtax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEadcpedcnsubtax[0]->SetNumber(fAdcPedCnSubtAxis[0]);
        TGNEadcpedcnsubtax[1]->SetNumber(fAdcPedCnSubtAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEadcpedcnsubtax[2]->SetNumber(fAdcPedCnSubtAxis[2]);
        TGNEadcpedcnsubtax[3]->SetNumber(fAdcPedCnSubtAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fAdcPedCnSubtAxis[i] = TGNEadcpedcnsubtax[i]->GetNumber();

    UpdateAdcPedCnSubtDisplay(fEventCounter, 1);
}

void MyMainFrame::UpdatePedCnSubtAxes()
{

    double minx = TGNEpedcnsubtax[0]->GetNumber();
    double maxx = TGNEpedcnsubtax[1]->GetNumber();
    double miny = TGNEpedcnsubtax[2]->GetNumber();
    double maxy = TGNEpedcnsubtax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEpedcnsubtax[0]->SetNumber(fPedCnSubtAxis[0]);
        TGNEpedcnsubtax[1]->SetNumber(fPedCnSubtAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEpedcnsubtax[2]->SetNumber(fPedCnSubtAxis[2]);
        TGNEpedcnsubtax[3]->SetNumber(fPedCnSubtAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fPedCnSubtAxis[i] = TGNEpedcnsubtax[i]->GetNumber();

    UpdatePedCnSubtDisplay(fEventCounter, 1);
}

void MyMainFrame::UpdatePedAxes()
{

    double minx = TGNEpedax[0]->GetNumber();
    double maxx = TGNEpedax[1]->GetNumber();
    double miny = TGNEpedax[2]->GetNumber();
    double maxy = TGNEpedax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEpedax[0]->SetNumber(fPedAxis[0]);
        TGNEpedax[1]->SetNumber(fPedAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEpedax[2]->SetNumber(fPedAxis[2]);
        TGNEpedax[3]->SetNumber(fPedAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fPedAxis[i] = TGNEpedax[i]->GetNumber();

    DisplayPedestals();
}

void MyMainFrame::UpdatePedCompAxes()
{

    double minx = TGNEpedcompax[0]->GetNumber();
    double maxx = TGNEpedcompax[1]->GetNumber();
    double miny = TGNEpedcompax[2]->GetNumber();
    double maxy = TGNEpedcompax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEpedcompax[0]->SetNumber(fPedCompAxis[0]);
        TGNEpedcompax[1]->SetNumber(fPedCompAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEpedcompax[2]->SetNumber(fPedCompAxis[2]);
        TGNEpedcompax[3]->SetNumber(fPedCompAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fPedCompAxis[i] = TGNEpedcompax[i]->GetNumber();

    DisplayPedCompar();
}

void MyMainFrame::UpdateSrawAxes()
{

    double minx = TGNEsrawax[0]->GetNumber();
    double maxx = TGNEsrawax[1]->GetNumber();
    double miny = TGNEsrawax[2]->GetNumber();
    double maxy = TGNEsrawax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEsrawax[0]->SetNumber(fSrawAxis[0]);
        TGNEsrawax[1]->SetNumber(fSrawAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEsrawax[2]->SetNumber(fSrawAxis[2]);
        TGNEsrawax[3]->SetNumber(fSrawAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fSrawAxis[i] = TGNEsrawax[i]->GetNumber();

    DisplayRawSigmas();
}

void MyMainFrame::UpdateSigmaAxes()
{

    double minx = TGNEsigmaax[0]->GetNumber();
    double maxx = TGNEsigmaax[1]->GetNumber();
    double miny = TGNEsigmaax[2]->GetNumber();
    double maxy = TGNEsigmaax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEsigmaax[0]->SetNumber(fSigmaAxis[0]);
        TGNEsigmaax[1]->SetNumber(fSigmaAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEsigmaax[2]->SetNumber(fSigmaAxis[2]);
        TGNEsigmaax[3]->SetNumber(fSigmaAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fSigmaAxis[i] = TGNEsigmaax[i]->GetNumber();

    DisplaySigmas();
}

void MyMainFrame::UpdateExcludeEvents()
{

    double exclude{TGNEexcludeevents->GetNumber()};
    if (exclude < 0)
    {
        exclude = 0;
        TGNEexcludeevents->SetNumber(exclude);
    }

    SetExcludeEvents(exclude);
}

void MyMainFrame::UpdateCNcutval()
{

    double cut{TGNEcncutval->GetNumber()};
    if (cut < 0)
    {
        cut = -cut;
        TGNEcncutval->SetNumber(cut);
    }

    SetCNcut(cut);
}

void MyMainFrame::UpdateSigmaCompAxes()
{

    double minx = TGNEsigmacompax[0]->GetNumber();
    double maxx = TGNEsigmacompax[1]->GetNumber();
    double miny = TGNEsigmacompax[2]->GetNumber();
    double maxy = TGNEsigmacompax[3]->GetNumber();

    if (minx >= maxx)
    {
        TGNEsigmacompax[0]->SetNumber(fSigmaCompAxis[0]);
        TGNEsigmacompax[1]->SetNumber(fSigmaCompAxis[1]);
        return;
    }

    if (miny >= maxy)
    {
        TGNEsigmacompax[2]->SetNumber(fSigmaCompAxis[2]);
        TGNEsigmacompax[3]->SetNumber(fSigmaCompAxis[3]);
    }

    for (int i = 0; i < 4; i++)
        fSigmaCompAxis[i] = TGNEsigmacompax[i]->GetNumber();

    DisplaySigmaCompar();
}

string MyMainFrame::GetDateTimeString()
{

    TDatime now;
    now.Set();
    int date, time;
    TDatime::GetDateTime(now.Get(), date, time);
    string sdate = to_string(date);
    string stime = to_string(time);
    stime = string(6 - stime.length(), '0') + stime;
    string datim = sdate + "-" + stime;

    return datim;
}

void MyMainFrame::SaveCalibrations()
{

    string datim{GetDateTimeString()};

    for (int ilad{0}; ilad < NLAD; ilad++)
    {
        string fname{fCalPath + "/" + Form("Ladder_%d", ilad)};
        fname += "_" + datim + ".cal";

        ofstream calibfil(fname);
        if (!calibfil.is_open())
        {
            AddLogMessage("failed to create file " + fname);
            continue;
        }
        for (int channel{0}; channel < fNLCH; channel++)
        {
            int va{channel / 64};
            int vacha{channel % 64};
            string line{Form("%4d, %4d, %4d, %10.3f, %10.3f, %10.3f, 0.0, %4d", channel, va, vacha, fPedestals.at(channel), fRawSigmas.at(channel), fSigmas.at(channel), fStatus.at(channel))};
            calibfil << line << endl;
        }

        calibfil.close();
        AddLogMessage("Calibration file " + fname + " written");
    }
}

void MyMainFrame::SetCNcut(double cut)
{

    if (cut < 0)
        cut = -cut;

    CNcutval = cut;
}

void MyMainFrame::SetExcludeEvents(double val)
{

    if (val < 0)
        val = 0;

    ExcludeEvents = val;
}

void MyMainFrame::InitRunningCN()
{

    //cout << "welcome to initrunningcn" << endl;

    vdouble().swap(fRunningCN);
    fRunningCN.assign(fNVA, 0);

    vdouble dummy;
    dummy.assign(fNVA, 0);

    vvdouble().swap(fBufferRunningCN);
    for (int i{0}; i < NRUNCN; i++)
        fBufferRunningCN.push_back(dummy);

    vdouble().swap(dummy);
}

void MyMainFrame::DoRunningCN()
{

    //cout << "welcome to dorunningcn !" << endl;

    vdouble().swap(fRunningCN);
    fRunningCN.assign(fNVA, 0);

    if (fCN.size() == 0)
        return;
    //for (size_t va{0}; va<fNVA ; va++)  cout << fCN.at(va) <<"  " ;
    //cout << endl;

    int pos{fEventCounter % NRUNCN};
    //cout << "cn counter: " << pos << endl;
    fBufferRunningCN.at(pos) = fCN;

    for (int va{0}; va < fNVA; va++)
    {
        for (size_t buf{0}; buf < fBufferRunningCN.size(); buf++)
            fRunningCN.at(va) += fBufferRunningCN.at(buf).at(va);
        fRunningCN.at(va) /= fBufferRunningCN.size();
        //cout << "va= " << va << "  fRunningCN = " << fRunningCN.at(va) << endl;
    }
}

int MyMainFrame::ComputeCN(double fMaj)
{ // based on ams TDR test system program

    vdouble().swap(fCN); // common noise vector
    vint().swap(fNst);   // ??
    fCN.assign(NLAD * fNVA, 0);
    fNst.assign(NLAD * fNVA, 0);

    //int mask_channelOK = 511; // 511=111111111  508=111111100   (old 91=1011011)

    int mask_channelOK{0xF0};
    //double fMaj {0.6};

    int mainconverge{0};

    for (int va = 0; va < NLAD * fNVA; va++)
    {
        //cout << "computeCN, VA=" << va << endl;
        int chref = 1;
        int converge = 0;
        int Nstall = 0;

        double aMaj = 0.0;

        while (!converge && chref < fNVACH)
        {
            fNst.at(va) = 0;
            fCN.at(va) = 0;
            Nstall = 0;
            if (fStatus.at(va * fNVACH + chref) & mask_channelOK)
            {
                converge = 0;
                chref++;
            }
            else
            {
                double aRef = fReadOut.at(va * fNVACH + chref);
                for (int ch = 0; ch < fNVACH; ch++)
                {
                    //CLRBIT(fStatus[va][ch],8);
                    if (!(fStatus.at(va * fNVACH + ch) & mask_channelOK))
                    {
                        Nstall++;
                        //if ((fabs(fReadOut.at(va*64+ch)-aRef)<3.0*fRawSigmas.at(va*NVACH+chref))) {
                        if ((fabs(fReadOut.at(va * fNVACH + ch) - aRef) < CNcutval))
                        {
                            fNst.at(va)++;
                            fCN.at(va) += fReadOut.at(va * fNVACH + ch);
                        }
                    }
                }
                aMaj = (double)fNst.at(va) / (double)Nstall;
                //cout << Form("va = %d, chref = %d, aMaj = %f", va, chref, aMaj) << endl;
                if (aMaj > fMaj)
                {
                    converge = 1;
                    fCN.at(va) /= fNst.at(va);
                }
                else
                {
                    chref++;
                    fCN.at(va) = 0.0;
                    Nstall = 0;
                    fNst.at(va) = 0;
                }
            }
        }

        //int bin = GetBin(kHISTO_CN,fCN[va]);
        //histo->fhCN[va][bin] += 1;
        //    printf("CN2 info: va=%02d first=%2d Nst=%2d All=%2d Maj=%6.4f\n"
        //    	   ,va,chref,fNst[va],Nstall,aMaj);
        if (!converge)
            AddLogMessage(Form("event %d : , VA = %d, Sorry no CN convergence", fEventCounter, va));
        mainconverge &= converge;
    }

    //cout << Form("cn1 = %4.2f, cn2 = %4.2f",fCN.at(0), fCN.at(1)) << endl;

    //AddLogMessage(Form("cn1 = %4.2, cn2 = %4.2",fCN.at(0), fCN.at(1)));

    return mainconverge;
}

void MyMainFrame::SubtractCN()
{

    for (int ch = 0; ch < fNTOTCH; ch++)
    {
        int va = ch / 64;
        fReadOut.at(ch) -= fCN.at(va);
    }
}

void MyMainFrame::InitRedCalibrations()
{

    vdouble().swap(fRedPedestals);
    fRedPedestals.assign(fNTOTCH, 0);
    vdouble().swap(fRedSigmas);
    fRedSigmas.assign(fNTOTCH, 0);
    vdouble().swap(fRedRawSigmas);
    fRedRawSigmas.assign(fNTOTCH, 0);
    vint().swap(fRedStatus);
    fRedStatus.assign(fNTOTCH, 0);

    for (int lad = 0; lad < NLAD; lad++)
        LoadRedCalibrations(lad);
}

void MyMainFrame::LoadRedCalibrations(int lad)
{

    if (lad >= NLAD)
        return;

    if (fRedCalFile[lad] == "")
        return;

    string fullpath = fCalPath + "/" + fRedCalFile[lad];

    ifstream calFile(fullpath);

    if (!calFile.is_open())
    {
        AddLogMessage("Calibration file " + fullpath + " not found");
        return;
    }

    int channel;
    string line, copy, left, right;
    size_t found;
    vector<string> info;

    int offset = lad * fNLCH;

    while (1)
    {
        if (!(getline(calFile, line)))
            break;
        copy = line;
        info.clear();
        while (1)
        {
            found = copy.find_first_of(",");
            if (found == std::string::npos)
                break;
            left = copy.substr(0, found);
            right = copy.substr(found + 1);
            info.push_back(left);
            copy = right;
        }

        channel = stoi(info.at(0)) + offset;
        fRedPedestals.at(channel) = stod(info.at(3));
        fRedRawSigmas.at(channel) = stod(info.at(4));
        fRedSigmas.at(channel) = stod(info.at(5));
        fRedStatus.at(channel) = stoi(info.at(7));
    }

    AddLogMessage(Form("Reduction calibration of ladder %d have been loaded", lad));
}

void MyMainFrame::InitRefCalibrations()
{

    fRefPedAttenuation = 0.5;
    fRefPedBaseline = 400.0;

    vdouble().swap(fRefPedestals);
    fRefPedestals.assign(fNTOTCH, 0);
    vdouble().swap(fRefSigmas);
    fRefSigmas.assign(fNTOTCH, 0);
    vdouble().swap(fRefRawSigmas);
    fRefRawSigmas.assign(fNTOTCH, 0);
    vint().swap(fRefStatus);
    fRefStatus.assign(fNTOTCH, 0);

    for (int lad = 0; lad < NLAD; lad++)
        LoadRefCalibrations(lad);

    //ConvertRefCalibrations();
}

void MyMainFrame::LoadRefCalibrations(int lad)
{

    if (lad >= NLAD)
        return;

    if (fRefCalFile[lad] == "")
        return;

    string fullpath = fCalRefPath + "/" + fRefCalFile[lad];

    ifstream calFile(fullpath);

    if (!calFile.is_open())
    {
        AddLogMessage("Calibration file " + fullpath + " not found");
        return;
    }

    int channel;
    string line, copy, left, right;
    size_t found;
    vector<string> info;

    int offset = lad * fNLCH;

    while (1)
    {
        if (!(getline(calFile, line)))
            break;
        copy = line;
        info.clear();
        while (1)
        {
            found = copy.find_first_of(",");
            if (found == std::string::npos)
                break;
            left = copy.substr(0, found);
            right = copy.substr(found + 1);
            info.push_back(left);
            copy = right;
        }

        channel = stoi(info.at(0)) + offset;
        //fRefPedestals.at(channel)=stod(info.at(3)); !!!
        //fRefRawSigmas.at(channel)=stod(info.at(4)); !!!
        //fRefSigmas.at(channel)=stod(info.at(5)); !!!
    }

    AddLogMessage(Form("Ref calibration of ladder %d have been loaded", lad));
}

void MyMainFrame::ConvertRefCalibrations()
{

    // convert the calibrations made by a TDR: inverts the K-side signals, and attenuate the spread by a given ratio.

    vector<double> meanped; //mean value of ped for each VA
    meanped.clear();
    meanped.assign(NLAD * fNVA, 0.0);

    for (int va = 0; va < fNVA * NLAD; va++)
    {

        for (int ch = 0; ch < fNVACH; ch++)
        {
            int channel = va * fNVACH + ch;
            meanped.at(va) += fRefPedestals.at(channel);
        }
        meanped.at(va) /= (fNVACH * 1.0);
        //cout << Form("va %3d, meanped: %5.1f",va,meanped.at(va)) << endl;
    }

    for (int ch = 0; ch < fNTOTCH; ch++)
    {

        int va = ch / 64;
        //int vach=ch%64;
        int ladva = va % fNVA; // we need to know if we are dealing with S or K Vas

        double delped = fRefPedestals.at(ch) - meanped.at(va);
        delped *= fRefPedAttenuation;
        //fRefPedestals.at(ch)=meanped.at(va)+(ladva>5)?-delped:delped; // K-side ? then we must invert the ped signal
        if (ladva < 6)
            fRefPedestals.at(ch) = fRefPedBaseline + delped;
        else
            fRefPedestals.at(ch) = fRefPedBaseline - delped;
    }

    AddLogMessage("Reference calibrations converted");

    vdouble().swap(meanped);
}

void MyMainFrame::SaveImage()
{

    int current = fDataTab->GetCurrent();

    string canname = fCanvasNames.at(current);
    TCanvas *c = fCanvasList.at(current);
    if (c == 0)
        return;

    string filename = fImagePath + "/" + canname + "_" + GetDateTimeString() + ".png";
    string filename_C = fImagePath + "/" + canname + "_" + GetDateTimeString() + ".C";

    c->Print(filename.c_str());
    c->Print(filename_C.c_str());

    AddLogMessage("Image " + filename + " saved.");
    AddLogMessage("Image " + filename_C + " saved.");
}

void MyMainFrame::ReCalibrate()
{

    StopScope();

    int event{(int)TGNEevent->GetNumber()};

    ComputePedestals(event);
    bool cp2res{false};
    cp2res = ComputePedestals2(0, 100);
    if (cp2res)
    {
        cout << "ComputePedestals2(0, 100) : ok" << endl;
        cp2res = ComputePedestals2(0, 50);
        if (cp2res)
        {
            cout << "ComputePedestals2(0, 50) : ok" << endl;
            cp2res = ComputePedestals2(0, 50);
            if (cp2res)
                cout << "ComputePedestals2(0, 25) : ok" << endl;
        }
    }
    ComputeRawSigmas(event);
    ComputeSigmas(event);
    SaveCalibrations();

    DisplayPedestals();
    DisplayRawSigmas();
    DisplaySigmas();
    DisplayPedCompar();
    DisplaySigmaCompar();

    ContinueScope();
}

void MyMainFrame::DynamicPedestals()
{

    for (int va{0}; va < fNVA; va++)
    {

        int count{0};
        for (int ch{0}; ch < fNVACH; ch++)
        {
            int chan{ch + va * fNVACH};
            double data{fReadOut.at(chan)};
            if (data > 0)
            {
                fPedestals.at(chan) += 0.25;
                count++;
            }
            else if (data < 0)
            {
                fPedestals.at(chan) -= 0.25;
                count--;
            }
        }
        double mean_drift{(count * 0.25) / fNVACH};
        // and now we subtract the mean drift
        for (int ch{0}; ch < fNVACH; ch++)
            fPedestals.at(ch + va * fNVACH) -= mean_drift; // + fRunningCN.at(va);

        if (fEventCounter > 0 && fEventCounter % 1000 == 0)
            for (int ch{0}; ch < fNVACH; ch++)
                fPedestals.at(ch + va * fNVACH) += fRunningCN.at(va); // drift correction every 1000 events
    }
}

void MyMainFrame::FreeCluster(cluster **aCluster)
{

    //cout << "welcome do freecluster" << endl;

    cluster *actual, *save;
    actual = *aCluster;
    *aCluster = 0;
    while (actual != 0)
    {
        //cout << "freecluster, inside loop" << endl;
        save = actual->next;
        FreeChannel(&actual->channel);
        delete actual;
        actual = save;
    }
}

void MyMainFrame::FreeChannel(channel **aChannel)
{
    //cout << "welcome do freechannel" << endl;

    channel *actual, *save;
    actual = *aChannel;
    *aChannel = 0;
    while (actual != 0)
    {
        //cout << "freechannel, inside loop" << endl;
        save = actual->next;
        delete actual;
        actual = save;
    }
}

void MyMainFrame::DoCluster(int ladder, char aSorK, cluster **aCluster)
{

    int i{0};
    int clusmin, clusmax;
    int ifirst{0}, ilast{0};
    //int numva=0;

    int noclusters = 1;
    cluster *clusnow{0};
    channel *channow{0};

    int firstva{0}, lastva{0};

    int offset{ladder * fNLCH}; //c if ladder == 0, NLCH=128 => offset == 0

    int sign{TGCBNegativeSignals->IsDown() ? -1 : 1};

    FreeCluster(aCluster);

    //    numva=va_per_ladder;
    //    firstva=0;
    //    lastva=va_per_ladder;
    //    ifirst=firstva*channel_per_va;
    //    ilast =lastva*channel_per_va;

    if (aSorK == 'S')
    {
        //numva=NVA;
        firstva = 0;
        lastva = fNVA - 1;
        ifirst = offset + firstva * fNVACH;     //c NVACH=64 => ifirst=offset==0
        ilast = offset + (lastva + 1) * fNVACH; //c ilast=128
    }

    // if (aSorK == 'K'){
    //   //numva=NVA;
    //   firstva=6;
    //   lastva=12;
    //   ifirst=offset+firstva*NVACH;
    //   ilast =offset+lastva*NVACH;
    // }

    int nclus = 0;

    i = ifirst;
    while (i < ilast)
    {
        if ((sign * fReadOut.at(i) <= fCluHighThresh) || fStatus.at(i)) // a bad strip cannot be the seed of a cluster
        {
            i++;
            continue;
        }
        //if over th. @i==89
        nclus++; //nclus==1

        clusmin = i - 1; //clusmin==88
        if (clusmin >= ifirst)
        {
            while (clusmin >= ifirst &&
                   sign * fReadOut.at(clusmin) > fCluLowThresh)
            {
                clusmin--;
            }
        }
        clusmin++;

        clusmax = i + 1; //clusmax==90
        if (clusmax < ilast)
        {
            while (clusmax < ilast &&
                   sign * fReadOut.at(clusmax) > fCluLowThresh)
            {
                clusmax++;
            }
        }
        clusmax--;
        i = clusmax + 1;

        float aInt = 0.0;
        for (int l = clusmin; l <= clusmax; l++)
        {
            aInt += sign * fReadOut.at(l);
        }

        if (aInt < fMinInt)
            continue;

        if (noclusters)
        {
            *aCluster = new cluster;
            clusnow = *aCluster;
            noclusters = false;
        }
        else
        {
            clusnow->next = new cluster;
            clusnow = clusnow->next;
        }

        clusnow->first = clusmin - offset;
        clusnow->length = clusmax - clusmin + 1;
        clusnow->cog = 0.0;
        clusnow->maxloc = 0;
        clusnow->maxval = 0.0;
        clusnow->integral = 0.0;
        clusnow->sovern1 = 0.0;
        clusnow->sovern2 = 0.0;
        clusnow->sovern3 = 0.0;
        clusnow->next = 0;

        for (int l = clusmin; l <= clusmax; l++)
        {
            //va=Getva(l);
            //ch=Getch(l);
            //        if (strcmp(fName,"amsS2")==0)
            //  	printf("  ch:%4d %10.2f %10.2f %10.2f\n",l,fReadOut[va][ch],
            //  	       fSigma[va][ch],fCN[va]);
            if (sign * fReadOut.at(l) > clusnow->maxval)
            {
                clusnow->maxval = sign * fReadOut.at(l);
                clusnow->maxloc = l - offset;
            }
            clusnow->integral += sign * fReadOut.at(l);
            clusnow->cog += sign * fReadOut.at(l) * (l + 1);
            if (fSigmas.at(l) > 0)
            {
                clusnow->sovern1 += fSigmas.at(l) * fSigmas.at(l);
                clusnow->sovern2 += fSigmas.at(l);
                clusnow->sovern3 += fReadOut.at(l) / fSigmas.at(l) * fReadOut.at(l) / fSigmas.at(l);
            }
        }
        clusnow->cog /= clusnow->integral;
        clusnow->cog -= 1.0 + offset;
        if (clusnow->sovern1 > 0)
            clusnow->sovern1 = clusnow->integral / sqrt(clusnow->sovern1);
        if (clusnow->sovern2 > 0)
            clusnow->sovern2 = clusnow->length * clusnow->integral / clusnow->sovern2;
        clusnow->sovern3 = sqrt(clusnow->sovern3);

        //      if (strcmp(fName,"amsS2")==0)
        //        printf("clus:%10.2f %4d %4d\n",clusnow->integral,
        //  	     clusnow->first,clusnow->length);

        // piece of code to include highest strip if clulen=1
        // does not work like it is!

        int loval = clusmin, hival = clusmax;

        for (int l = loval; l <= hival; l++)
        {
            //va=Getva(l);
            //ch=Getch(l);
            if (l == loval)
            {
                clusnow->channel = new channel;
                channow = clusnow->channel;
            }
            else
            {
                channow->next = new channel;
                channow = channow->next;
            }

            channow->Slot = l - offset;
            channow->ADC = sign * fReadOut.at(l);
            channow->CN = fCN.at(l / fNVACH);
            channow->Status = fStatus.at(l);
            channow->next = 0;
        }
    }

    //if (nclus>9) cout << "event: " << fEventCounter << " has " << nclus << " clusters !!!" << endl;
}

void MyMainFrame::Reduction()
{

    //int bin;
    //int nclus_S=0;
    //int nclus_K=0;
    //cluster *actual;
    //channel *actual_channel;

    for (int ladder = 0; ladder < NLAD; ladder++)
    {

        //cout << "ladder " << ladder << endl;

        DoCluster(ladder, 'S', &Scluster);
        //DoCluster(ladder, 'K',&Kcluster);

        //ListCluster(Scluster, Form("Ladder %d, S-side: ", ladder)); //c
        //ListCluster(Kcluster,Form("Ladder %d, K-side: ", ladder));

        //hisClusters[ladder]->Reset();
        ResetGraph(grclusters[ladder]);
        BuildClusterStats(Scluster, ladder);
        //BuildClusterStats(Kcluster, ladder);
        //BuildGrCluster(Scluster, grclusters[ladder]);
        //BuildGrCluster(Kcluster,grclusters[ladder]);
    }
}

void MyMainFrame::ListCluster(cluster *acluster, string mesg)
{

    cluster *actual = 0;
    //channel *actual_channel=0;

    actual = acluster;

    int nclus = 0;

    while (actual != 0)
    {
        nclus++;
        std::cout << mesg << Form("  i=%3d, len=%3d, cog=%5.2f, maxloc=%3d, integral=%7.2f, sovern1=%5.2f", nclus, actual->length, actual->cog, actual->maxloc, actual->integral, actual->sovern1) << endl;
        ListChannels(actual->channel);
        actual = actual->next;
    }

    //c if (nclus) cout << nclus << " clusters found !" << endl;
}

void MyMainFrame::ListChannels(channel *achannel)
{

    channel *actual = achannel;
    int nchan = 0;
    while (actual != 0)
    {

        nchan++;
        std::cout << Form("      channel %d, position %3d, adc=%7.2f, cn=%7.2f", nchan, actual->Slot, actual->ADC, actual->CN) << endl;
        actual = actual->next;
    }
}

void MyMainFrame::BuildClusterStats(cluster *acluster, int ladder)
{

    cluster *actual = acluster;
    channel *achannel = 0;
    int nclus = 0;

    //TGraph *gr = grclusters[ladder];
    TH1F *hocc = hisOccupancy[ladder];
    TH1F *hocccut = hisOccupancyCut[ladder];
    TH1F *hcog = hisCog[ladder];
    TH1F *hcogcut = hisCogCut[ladder];
    TH1F *hlens = hisLens[ladder];
    TH1F *hlenscut = hisLensCut[ladder];
    //TH1F *hlenk=hisLenk[ladder];
    TH1F *hints = hisIntS[ladder];
    TH1F *hintscut = hisIntSCut[ladder];
    //TH1F *hintk=hisIntK[ladder];
    TH1F *hnclus = hisNclus[ladder];

    TH2F *h2len = hisLenVsCog[ladder];
    TH2F *h2lencut = hisLenVsCogCut[ladder];

    TH2F *h2int = hisIntVsCog[ladder];
    TH2F *h2intcut = hisIntVsCogCut[ladder];

    while (actual != 0)
    {
        //c cout<<"-->"<<nclus<<endl;
        nclus++;
        achannel = actual->channel;

        // if ((acluster->cog)>=NLCH/2) {
        //   hintk->Fill(acluster->integral);
        //   hlenk->Fill(acluster->length);
        // }
        // else {
        double integral{actual->integral};
        int length{actual->length};
        double cog{actual->cog};
        hints->Fill(integral);
        hlens->Fill(length);
        // for debug purposes
        //if ((cog<46 || cog>49) && length>4) cout << "event: " << fEventCounter << " length=" << length << " cog= " << cog << endl;
        //if (!(cog<46 || cog>49) && length>3) cout << "event: " << fEventCounter << " length=" << length << " cog= " << cog << endl;
        h2len->Fill(cog, length);
        h2int->Fill(cog, integral);
        hcog->Fill(cog);

        if (integral > fMinValidIntegral)
        {
            hintscut->Fill(integral);
            hlenscut->Fill(length);
            h2lencut->Fill(cog, length);
            h2intcut->Fill(cog, integral);
            hcogcut->Fill(cog);
        }
        //cout << actual->integral << endl;
        //}

        while (achannel != 0)
        {
            //his->SetBinContent(1+achannel->Slot,achannel->ADC);
            //gr->SetPoint(gr->GetN(), achannel->Slot, achannel->ADC);
            //c cout<<"slot = "<<achannel->Slot<<endl;
            hocc->Fill(achannel->Slot);
            if (integral > fMinValidIntegral)
                hocccut->Fill(achannel->Slot);
            achannel = achannel->next;
        }

        actual = actual->next;
    }
    hnclus->Fill(nclus);
}

void MyMainFrame::BuildGrCluster(cluster *acluster, TGraph *gr)
{

    cluster *actual = acluster;
    channel *achannel = 0;
    int nclus = 0;

    while (actual != 0)
    {
        nclus++;
        achannel = actual->channel;

        while (achannel != 0)
        {
            //his->SetBinContent(1+achannel->Slot,achannel->ADC);
            gr->SetPoint(gr->GetN(), achannel->Slot, achannel->ADC);
            achannel = achannel->next;
        }

        actual = actual->next;
    }
}

void MyMainFrame::DrawHis(TCanvas *c, int ipad, TH1 *his)
{
    TVirtualPad *pad = c->cd(ipad);
    pad->GetListOfPrimitives()->Remove(his);
    his->Draw("same");
}

unsigned short MyMainFrame::GetStripValue(int ch, int convertMode)
{

    int index{ch};

    if (convertMode == 0)
        index = ch;
    else if (convertMode == 1)
    {
        // We convert from channel value to strip value, which should be the normal mode once debugging at readout level is finished.
        index = ConvertStripToChannel(ch);
    }
    else
        std::cout << "unkown conversion option " << convertMode << std::endl;

    return fStrip[index];
}

unsigned short MyMainFrame::ConvertChannelToStrip(unsigned short ch)
{

    unsigned short index{ch};

    if (ch >= 0 and ch < 256)
    {
        index = 255 - ch + 1536;
    }
    else if (ch >= 256 and ch < 512)
    {
        index = ch - 256 + 1792;
    }
    else if (ch >= 512 and ch < 768)
    {
        index = 767 - ch + 1024;
    }
    else if (ch >= 768 and ch < 1024)
    {
        index = 1023 - ch + 512;
    }
    else if (ch >= 1024 and ch < 1280)
    {
        index = ch - 1024 + 256;
    }
    else if (ch >= 1280 and ch < 1536)
    {
        index = 1535 - ch + 0;
    }
    else if (ch >= 1536 and ch < 1792)
    {
        index = ch - 1536 + 768;
    }
    else if (ch >= 1792 and ch < 2048)
    {
        index = ch - 1792 + 1280;
    }
    else
        std::cout << "you should never arrive here...." << std::endl;

    return index;
}

unsigned short MyMainFrame::ConvertStripToChannel(unsigned short strip)
{

    unsigned short index{strip};

    if (strip >= 0 and strip < 256)
        index = (255 - strip) + 1280;
    else if (strip >= 256 and strip < 512)
        index = strip - 256 + 1024;
    else if (strip >= 512 and strip < 768)
        index = 767 - strip + 768;
    else if (strip >= 768 and strip < 1024)
        index = strip - 768 + 1536;
    else if (strip >= 1024 and strip < 1280)
        index = 1279 - strip + 512;
    else if (strip >= 1280 and strip < 1536)
        index = strip - 1280 + 1792;
    else if (strip >= 1536 and strip < 1792)
        index = 1791 - strip + 0;
    else if (strip >= 1792 and strip < 2048)
        index = strip - 1792 + 256;
    else
        std::cout << "ConvertStripToChannel: you should never arrive here.... strip = " << strip << std::endl;
    return index;
}

long MyMainFrame::GetFileSize(string filename)
{
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

void MyMainFrame::ComputeReduction()
{

    InitRunningCN();
    fTGHPBprogress->Reset();

    //int SigmaCnt=0;
    vdouble().swap(fReadOut);
    fReadOut.assign(fNTOTCH, 0);

    std::cout << "20: Resetting ADC histograms" << endl;

    /*
    for (int lad = 0; lad < NLAD; lad++)
    {
        h2ChHi[lad]->Reset();
        h2ChHiPe[lad]->Reset();
        for (int ch = 0; ch < fNTOTCH; ch++)
        {
            //int lad=ch/NLCH;   <----- this does not work, considering the hisChHi[][fNTOTCH] trick. I will have to fix that one day
            //cout << "resetting " << lad << "  " << ch << endl;
            vHisChHi[lad].at(ch)->Reset();
            vHisChHiPe[lad].at(ch)->Reset();
        }
        vHisChHi[lad].at(fNTOTCH)->Reset();
    }*/
    std::cout << "22: Resetting occupancy histograms" << endl;
    for (int lad = 0; lad < NLAD; lad++)
    {
        hisOccupancy[lad]->Reset();
        hisOccupancyCut[lad]->Reset();
    }

    std::cout << "before starting event scanning" << endl;
    //TH2F *h2ADCval{new TH2F("h2ADCval", "cnsubt distribution", 2048, -0.5, 2048 - 0.5, 200, -100, 100)};

    for (int event = ExcludeEvents; event < fNevents; event++)
    { // we ignore the first 10 events
        fTree->GetEntry(event);
        fEventCounter = event;
        //cout << "event " << event << endl;

        if (event % (fNevents / 20) == 0)
        {
            //fTGHPBprogress->SetPosition((event*100.)/fNevents); // in linux, this is enough to update the display of the progress bar
            std::cout << "Progress: " << (event * 100.) / fNevents << " %" << endl;
            //fTGHPBprogress->ShowPosition(); // in mac, you need to add this
            //gSystem->ProcessEvents(); // and this.
            //fTGHPBprogress->DoRedraw(); // let's try this one - does not work, it is a protected method
        }

        double spread{0};
        int goodcnt{0};
        for (int ch = 0; ch < fNTOTCH; ch++)
        {
            fReadOut.at(ch) = GetStripValue(ch, fChanLabelMode) - fPedestals.at(ch);
        }

        //ComputeCN();
        if (TGCBComputeCN->IsDown())
        {
            int success{ComputeCN(0.6)};
            if (!success)
            {
                success = ComputeCN(0.4);
                if (!success)
                    success = ComputeCN(0.2);
            }
            SubtractCN();
            DoRunningCN();
        }
        if (TGCBdodynped->IsDown())
            DynamicPedestals();

        Reduction();
    }

    //fTGHPBprogress->SetPosition(100);
    //fTGHPBprogress->ShowPosition();
    //gSystem->ProcessEvents();

    AddLogMessage("Signal histograms filled.");
}

Double_t langaufun(Double_t *x, Double_t *par)
{
    // From the root tutorials.
    //Fit parameters:
    //par[0]=Width (scale) parameter of Landau density
    //par[1]=Most Probable (MP, location) parameter of Landau density
    //par[2]=Total area (integral -inf to inf, normalization constant)
    //par[3]=Width (sigma) of convoluted Gaussian function
    //
    //In the Landau distribution (represented by the CERNLIB approximation),
    //the maximum is located at x=-0.22278298 with the location parameter=0.
    //This shift is corrected within this function, so that the actual
    //maximum is identical to the MP parameter.

    // Numeric constants
    Double_t invsq2pi = 0.3989422804014; // (2 pi)^(-1/2)
    Double_t mpshift = -0.22278298;      // Landau maximum location

    // Control constants
    Double_t np = 100.0; // number of convolution steps
    Double_t sc = 5.0;   // convolution extends to +-sc Gaussian sigmas

    // Variables
    Double_t xx;
    Double_t mpc;
    Double_t fland;
    Double_t sum = 0.0;
    Double_t xlow, xupp;
    Double_t step;
    Double_t i;

    // MP shift correction
    mpc = par[1] - mpshift * par[0];

    // Range of convolution integral
    xlow = x[0] - sc * par[3];
    xupp = x[0] + sc * par[3];

    step = (xupp - xlow) / np;

    // Convolution integral of Landau and Gaussian by sum
    for (i = 1.0; i <= np / 2; i++)
    {
        xx = xlow + (i - .5) * step;
        fland = TMath::Landau(xx, mpc, par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0], xx, par[3]);

        xx = xupp - (i - .5) * step;
        fland = TMath::Landau(xx, mpc, par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0], xx, par[3]);
    }

    return (par[2] * step * sum * invsq2pi / par[3]);
}

void MyMainFrame::UpdateChanLabel()
{
    fChanLabelMode = TGCBStripMode->IsDown();
    //string chanlab{ChanLabel[fChanLabelMode]};
    if (fFileOpened)
    {
        //UpdateAdcDisplay(fEventCounter, 1);
        //UpdateAdcPedSubtDisplay(fEventCounter, 1);
        //UpdateAdcPedCnSubtDisplay(fEventCounter, 1);
    }
}

int DisPanV2R9(int nva = 32, int nvach = 64)
{
    // Popup the GUI...
    if (nva <= 0)
    {
        cout << "you need to provide the number of ASICS" << endl;
        return 1;
    }

    //TApplication theApp("App",&argc,argv);
    new MyMainFrame(gClient->GetRoot(), 800, 600, nva, nvach);
    //cout << "goodbye"<<endl;

    //theApp.Run();

    return 0;
}
