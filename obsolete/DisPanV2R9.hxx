#include <TGClient.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
//#include <RQ_OBJECT.h>
#include <TGTab.h>
#include <TGFileDialog.h>
#include <TGTab.h>
#include <TFile.h>
#include <TText.h>
#include <TLine.h>
#include <TGraph.h>
#include <TBox.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGFrame.h>
#include <TGTextEdit.h>
#include <TGTextView.h>
#include <TAxis.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TArrow.h>
#include <TObject.h>
#include <TList.h>
#include <TTimer.h>
#include <TApplication.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TGProgressBar.h>
#include <TCut.h>
#include <TDatime.h>
#include <TGButton.h>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "TGProgressBar.h"
#include "TSpectrum.h"
#include "TVirtualFitter.h"
#include "TProfile.h"
#include "TMinuit.h"
#include "TPolyMarker.h"
#include "TGComboBox.h"

using namespace std;

constexpr int VERSION{2};
constexpr int RELEASE{9};

constexpr int NPEDEVENTS{2048};
constexpr int NSRAWEVENTS{2048};
constexpr int NSIGEVENTS{2048};

constexpr int NRUNCN{16};

const int NLAD = 1;
const int NADC = 8;

const string ChanLabel[2] = {"channel", "strip"};

const int CONVERT[6] = {0, 1, 2, 3, 4, 5};
//const int CONVERT[1]={1};
//const int NCH=384;
//const int NLCH=384;
//const int NADCCH=384;
//const int NVA=6;
//const int NVACH=64;

const string ParamName[] = {
    "refcaldir=",
    "caldir=",
    "imagedir=",
    "filedir=",
    "refcalibfile_lad0=",
    "refcalibfile_lad1=",
    "refcalibfile_lad2=",
    "refcalibfile_lad3=",
    "refcalibfile_lad4=",
    "refcalibfile_lad5=",
    "redcalibfile_lad0=",
    "redcalibfile_lad1=",
    "redcalibfile_lad2=",
    "redcalibfile_lad3=",
    "redcalibfile_lad4=",
    "redcalibfile_lad5=",
};

typedef struct channel
{
    int Slot;
    double ADC;
    double CN;
    int Status;
    struct channel *next; // Next channel found =NULL if none
} channel;

typedef struct cluster
{
    int first;               // First strip of the cluster
    int length;              // Length of the cluster last=first+length
    float cog;               // Center of gravity
    int maxloc;              // Strip with highest signal
    float maxval;            // Highest signal
    float integral;          // Total charge collected
    float sovern1;           // S/N (integral/sqrt(sigma**2))
    float sovern2;           // S/N (integral/sigma mean)
    float sovern3;           // S/N (sqrt(sum((signal/sigma)**2))
    struct channel *channel; // Pointer to channel list
    struct cluster *next;    // Next cluster found =NULL if none
} cluster;

typedef vector<TH1F *> vTH1Fp;
typedef vector<double> vdouble;
typedef vector<vdouble> vvdouble;
typedef vector<int> vint;
typedef vector<TF1 *> vTF1p;

Double_t langaufun(Double_t *x, Double_t *par);

class MyMainFrame : public TGMainFrame
{
    //RQ_OBJECT("MyMainFrame")
    ClassDef(MyMainFrame, 1);

private:
    TGMainFrame *fMain;
    TGTab *fDataTab;
    TGCheckButton *TGCBshowrefped, *TGCBshowrefsig, *TGCBShowFFTNorm, *TGCBShowFFTSum, *TGCBdodynped, *TGCBshowProfileLenVsCog, *TGCBshowProfileIntVsCog, *TGCBshowOccCut, *TGCBshowCogCut, *TGCBshowLensCut, *TGCBshowLenVsCogCut, *TGCBshowIntVsCogCut, *TGCBshowIntsCut;
    TGTextEntry *fTeFile;
    TGTextView *TGTVlog;
    TGComboBox *TGCBEventSel;
    TGCheckButton *TGCBDoCalib, *TGCBComputeCN, *TGCBNegativeSignals, *TGCBStripMode;
    TGNumberEntry *TGNEevent, *TGNEsuperimpose, *TGNEadcax[4], *TGNEpedsubtax[4], *TGNEpedax[4], *TGNEsrawax[4], *TGNEsigmaax[4], *TGNEpedcnsubtax[4], *TGNEpedcompax[4], *TGNEsigmacompax[4], *TGNEfftax[4], *TGNEChHiax[4], *TGNEChannel, *TGNEoccupancyax[4], *TGNEcogax[4], *TGNElensax[4], *TGNElenvscogax[4], *TGNEintvscogax[4], *TGNEintsax[4], *TGNEintkax[4], *TGNEadcpedsubtax[4], *TGNEadcpedcnsubtax[4], *TGNEnclusax[4], *TGNEPhElCalax[4], *TGNEcncutval, *TGNEexcludeevents, *TGNEcluHighThresh, *TGNEcluLowThresh, *TGNEminValidIntegral, *TGNEpixelSearchParam, *TGNEpixelSearchRebin;
    TGNumberEntry *TGNEFPSigma, *TGNEFPThreshold, *TGNEFPNIter, *TGNEFPAverWindow, *TGNEFPRangeMin, *TGNEFPRangeMax;
    TRootEmbeddedCanvas *fEadccanvas, *fEpedsubtcanvas, *fEadcpedsubtcanvas, *fEadcpedcnsubtcanvas, *fEpedcanvas, *fEsrawcanvas, *fEsigmacanvas, *fEpedcnsubtcanvas, *fEpedcompcanvas, *fEsigmacompcanvas, *fEfftcanvas, *fEChHicanvas, *fEoccupancycanvas, *fEcogcanvas, *fElenscanvas, *fElenvscogcanvas, *fEintvscogcanvas, *fEintscanvas, *fEintkcanvas, *fEncluscanvas, *fEPhElCalcanvas;
    TCanvas *fPedSubtCanvas, *fAdcPedSubtCanvas, *fAdcPedCnSubtCanvas, *fAdcCanvas, *fPedCanvas, *fSrawCanvas, *fSigmaCanvas, *fPedCnSubtCanvas, *fPedCompCanvas, *fSigmaCompCanvas, *fFftCanvas, *fChHiCanvas, *fOccupancyCanvas, *fCogCanvas, *fLensCanvas, *fLenVsCogCanvas, *fIntVsCogCanvas, *fIntsCanvas, *fIntkCanvas, *fNclusCanvas, *fPhElCalCanvas;
    string fCurrentPath, fCalPath, fCalRefPath, fImagePath, fFilePath, fFilename, fFilenameWOPath, fPathOfFile, fFilenameWOExt;
    TPad *fPhElSubPad;
    int fCurrTab;
    TFile *fFile;
    TTree *fTree;
    TBranch *fSilevents;
    TTimer *fRefreshtimer;
    TGTextButton *fTGTBStartrun, *fTGTBStoprun, *fTGTBContinuerun, *fTGTBSave, *fTGTBRecalib, *fTGTBSavePECal;
    TGraph *gradc[NLAD], *grpedsubt[NLAD], *grpedcnsubt[NLAD], *grclusters[NLAD];
    TGraph *grADCpedsubt[2 * NADC], *grADCpedcnsubt[2 * NADC];
    TGraph *grPixelResultsSlope[NLAD], *grPixelResultsIntercept[NLAD];
    TH1F *hisFFT[NLAD][NADC], *hisSumFFT[NLAD][NADC], *hisFFTnorm[NLAD][NADC], *hisSumFFTnorm[NLAD][NADC], *hisOccupancy[NLAD], *hisOccupancyCut[NLAD], *hisCog[NLAD], *hisLens[NLAD], *hisIntS[NLAD], *hisCogCut[NLAD], *hisLensCut[NLAD], *hisIntSCut[NLAD], *hisNclus[NLAD];
    vTH1Fp vHisChHi[NLAD], vHisChHiPe[NLAD];
    TH2F *hisLenVsCog[NLAD], *hisLenVsCogCut[NLAD], *hisIntVsCog[NLAD], *hisIntVsCogCut[NLAD], *h2ChHi[NLAD], *h2ChHiPe[NLAD];
    TGHProgressBar *fTGHPBprogress;
    unsigned short fStrip[4608], fStripPrevious[4608]; //NCH-->4608
    int fNevents;
    //int fIndex[NLCH];
    vdouble fPedestals, fRawSigmas, fSigmas, fCN, fReadOut, fRunningCN;
    vdouble fRefPedestals, fRefRawSigmas, fRefSigmas;
    vdouble fRedPedestals, fRedRawSigmas, fRedSigmas;
    vdouble fvCalibSlope, fvCalibIntercept;
    vint fNst, fStatus, fRefStatus, fRedStatus;
    vvdouble fBufferRunningCN;

    TGLayoutHints *TGLHexpandX, *TGLHleftTop, *TGLHleft, *TGLHexpandXexpandY;

    int fNVA, fNVACH, fNLCH, fNTOTCH, fNADCCH;
    int fSuperImposed;
    int fEventCounter;
    int fDisplayBusy;
    int fClearDisplay;
    int fChannel, fChannel_min, fChannel_max;
    double fAdcAxis[4], fPedSubtAxis[4], fAdcPedSubtAxis[4], fAdcPedCnSubtAxis[4], fPedAxis[4], fSrawAxis[4], fSigmaAxis[4], fPedCnSubtAxis[4], fPedCompAxis[4], fSigmaCompAxis[4], fFftAxis[4], fChHiAxis[4], fOccupancyAxis[4], fCogAxis[4], fLensAxis[4], fLenVsCogAxis[4], fIntVsCogAxis[4], fIntsAxis[4], fIntkAxis[4], fNclusAxis[4], fPhElCalAxis[4];
    string fRefCalFile[NLAD], fRedCalFile[NLAD];
    vector<string> fCanvasNames;
    vector<TCanvas *> fCanvasList;
    vdouble fSpectrum;
    double fRefPedAttenuation, fRefPedBaseline;
    double fCluHighThresh, fCluLowThresh, fMinInt, fMinValidIntegral, fPixelSearchParam, fPixelSearchRebin;
    unsigned int fMaskClu;
    cluster *Scluster;
    cluster *Kcluster;
    int fWidth, fHeight;
    double CNcutval;
    double ExcludeEvents;
    double fFindPeaksSigma, fFindPeaksThreshold, fFindPeaksRangeMin, fFindPeaksRangeMax;
    int fFindPeaksDeconIter, fFindPeaksAverWindow;
    //bool fDynPedActive;
    int fChanLabelMode; // 1=strip, 0=channel
    bool fFileOpened;

public:
    MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, int nva = 6, int nvach = 64);
    virtual ~MyMainFrame();

    void LoadConfiguration();
    void SetConfiguration(string line);
    void SaveConfiguration();
    void ManageSelectedDataTab(int tabnum);
    void ChooseFile();
    void StartScope();
    void StopScope();
    void ContinueScope();
    void UpdateName();
    void AddAdcTab(TGCompositeFrame *cframe);
    void AddFftTab(TGCompositeFrame *cframe);
    void AddOccupancyTab(TGCompositeFrame *cframe);
    void AddCogTab(TGCompositeFrame *cframe);
    void AddLensTab(TGCompositeFrame *cframe);
    void AddLenVsCogTab(TGCompositeFrame *cframe);
    void AddIntVsCogTab(TGCompositeFrame *cframe);
    void AddIntsTab(TGCompositeFrame *cframe);
    void AddIntkTab(TGCompositeFrame *cframe);
    void AddPedSubtTab(TGCompositeFrame *cframe);
    void AddPedCnSubtTab(TGCompositeFrame *cframe);
    //void AddChannelHistoTab(TGCompositeFrame *cframe);
    //void AddPhotoElCalibTab(TGCompositeFrame *cframe);
    void AddAdcPedSubtTab(TGCompositeFrame *cframe);
    void AddAdcPedCnSubtTab(TGCompositeFrame *cframe);
    void AddSrawTab(TGCompositeFrame *cframe);
    void AddSigmaTab(TGCompositeFrame *cframe);
    void AddPedTab(TGCompositeFrame *cframe);
    void AddPedCompTab(TGCompositeFrame *cframe);
    void AddSigmaCompTab(TGCompositeFrame *cframe);
    void AddLogTab(TGCompositeFrame *cframe);
    void AddSettingsTab(TGCompositeFrame *cframe);
    void AddNclusTab(TGCompositeFrame *cframe);
    void LinesVas(int drawline = 0);
    void LinesAdcs(int drawline = 0);
    void CloseWindow();
    int OpenFile();
    void AddLogMessage(string message);
    void InitGraphHis();
    void UpdateCluHighThresh();
    void UpdateCluLowThresh();
    void UpdateMinValidIntegral();
    void UpdateAdcDisplay(int event, int refreshaxes = 0);
    void UpdatePedSubtDisplay(int event, int refreshaxes = 0);
    void UpdateAdcPedSubtDisplay(int event, int refreshaxes = 0);
    void UpdateAdcPedCnSubtDisplay(int event, int refreshaxes = 0);
    void UpdatePedCnSubtDisplay(int event, int refreshaxes = 0);
    void UpdateFftDisplay(int refreshaxes = 0);
    void UpdateLenVsCogAxes();
    void UpdateIntVsCogAxes();
    void UpdatePixelSearchParam();
    void UpdatePixelSearchRebin();
    //void UpdateFindPeaksPar();
    //void UpdateChHiDisplay(int event, int refreshaxes=0);
    //void UpdateOccupancyDisplay(int event, int refreshaxes=0);
    //void UpdateCogDisplay(int event, int refreshaxes=0);
    //void UpdateLensDisplay(int event, int refreshaxes=0);
    //void UpdateLenkDisplay(int event, int refreshaxes=0);
    //void UpdateIntsDisplay(int event, int refreshaxes=0);
    //void UpdateIntkDisplay(int event, int refreshaxes=0);
    void BuildGraph();
    void ComputePedestals(int startevent = 0);
    bool ComputePedestals2(int startevent, double pedcut);
    void ComputeRawSigmas(int startevent = 0);
    void ComputeSigmas(int startevent = 0);
    //void ComputeChHi();
    void DisplayPedestals();
    //void DisplayChHi();
    void DisplayPedCompar();
    void DisplayRawSigmas();
    void DisplaySigmas();
    void DisplaySigmaCompar();
    void DisplayOccupancy();
    void DisplayCog();
    void DisplayLens();
    void DisplayLenVsCog();
    void DisplayIntVsCog();
    void DisplayInts();
    void DisplayNclus();
    void InitRefreshTimer();
    void StartEventTrigger();
    void StopEventTrigger();
    void RefreshDisplay();
    void SetRunButtons(int start, int stop, int contin);
    void UpdateSuperImpose();
    void DrawGraph(TCanvas *c, int pad, int lad, TGraph *gr[NLAD], string option = "l");
    void DrawGraphSuperimposed(TCanvas *c, int ipad, int lad, int event, TGraph *gr[NLAD]);
    void ClearSuperimposedGraphs(TCanvas *c, int event, int back);
    void InitAxes();
    void UpdateAdcAxes();
    void UpdatePedSubtAxes();
    void UpdateAdcPedSubtAxes();
    void UpdateAdcPedCnSubtAxes();
    void UpdatePedCnSubtAxes();
    void UpdatePedAxes();
    void UpdatePedCompAxes();
    void UpdateSrawAxes();
    void UpdateSigmaAxes();
    void UpdateSigmaCompAxes();
    void UpdateOccupancyAxes();
    void UpdateCogAxes();
    void UpdateLensAxes();
    //void UpdateLenkAxes();
    void UpdateIntsAxes();
    //void UpdateIntkAxes();
    void UpdateNclusAxes();
    void SaveCalibrations();
    int ComputeCN(double fMaj);
    void SubtractCN();
    void InitRefCalibrations();
    void InitRedCalibrations();
    vector<string> SplitString(string fullfilename, string separator);
    void LoadRefCalibrations(int lad);
    void LoadRedCalibrations(int lad);
    void InitVectors();
    void RemoveGraph(string name);
    void SaveImage();
    string GetDateTimeString();
    void DisplayThisEvent();
    void ComputeFFT();
    void DoFFT(double *array, int nech, double *his);
    void UpdateFftAxes();
    //void UpdateChHiChannel();
    //void UpdateChHiAxes();
    void ConvertRefCalibrations();
    void FreeCluster(cluster **aCluster);
    void FreeChannel(channel **aChannel);
    void DoCluster(int ladder, char aSorK, cluster **aCluster);
    void InitReduction();
    void Reduction();
    void ListCluster(cluster *acluster, string mesg = "");
    void ListChannels(channel *achannel);
    void BuildGrCluster(cluster *acluster, TGraph *gr);
    void DrawHis(TCanvas *canvas, int pad, TH1 *his);
    void ResetGraph(TGraph *gr);
    void DrawClusterGraph(TCanvas *c, int ipad, int lad);
    void BuildClusterStats(cluster *acluster, int ladder);
    //TF1 *peaks_finder(TH1F *h);
    //TF1 *FindPeaks(TH1F *h);
    void SetCNcut(double cut);
    void SetExcludeEvents(double val);
    void UpdateCNcutval();
    void UpdateExcludeEvents();
    void ReCalibrate();
    void DynamicPedestals();
    void ResetHistos();
    void InitLayoutHints();

    void DoRunningCN();
    void InitRunningCN();
    bool TestFitSuccess(bool verbose = false);
    //void DarkCountFindOnePixel(double threshold);
    //void CalibrateChHi();
    void SavePECalibration();
    //void StartCalibrateChHi();
    double ConvertPe(double adc, int lad, int ch);
    TCanvas *GetCanvas(string name, double xmin, double ymin, double xmax, double ymax);
    unsigned short GetStripValue(int ch, int convertMode = 0);
    int ConvertDaqToRoot(long removebytes = 0);
    long GetFileSize(string filename);
    //void UpdateEventFilter();
    void ComputeReduction();
    unsigned short ConvertChannelToStrip(unsigned short ch);
    unsigned short ConvertStripToChannel(unsigned short ch);
    void UpdateChanLabel();
};
