#include "DisPanOnline.hxx"

const char *filetypes[] = {
    //  "All files",     "*",
    //"TRB calibration files", "*.cal",
    //"TRB calibration files", "*.txt",
    "ROOT files", "*.root",
    "DAQ files", "*.daq",
    //"bin threshold files",   "*.bin",
    //  "Text files",    "*.[tT][xX][tT]",
    0, 0};

// const intADCCORRECT=0;

MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, uint nStripX, uint nStripY)
    : TGMainFrame(p, w, h)
{

    fWidth = w;
    fHeight = h;

    fNstripX = nStripX;
    fNstripY = nStripY;
    SetCleanup(kDeepCleanup);
    gStyle->SetLineScalePS(1);
    gStyle->SetPalette(53);
    gStyle->SetOptStat(111111);
    // getting the current directory
    char *path = getcwd(0, 0);
    fCurrentPath = path;
    fCalPath = fCurrentPath;
    fCalRefPath = fCurrentPath;
    fImagePath = fCurrentPath;
    fFilePath = fCurrentPath;
    fPathOfFile = fCurrentPath;
    fConfigFilename = "";
    fDisplayBusy = 0;
    fClearDisplay = 0;
    
    if (nStripX == 0 && nStripY == 0) {
        std::cerr << "You should use at least 1 detector!" << std::endl;
        std::abort();
    }

    for (std::size_t i{0}; i < nStripX; ++i)
    {
        vBoards.push_back(new Boards::StripX());
        USB_BUFFER_SIZE += (vBoards.back())->getNBytes();
    }
    for (std::size_t i{0}; i < nStripY; ++i)
    {
        vBoards.push_back(new Boards::StripY());
        USB_BUFFER_SIZE += (vBoards.back())->getNBytes();
    }
    
    USB_BUFFER_SIZE += Boards::NBytesFrameHeaderTail;

    for (std::size_t i{0}; i < vBoards.size(); ++i)
    {
        fNVA += (vBoards.at(i))->getNVA();
    }

    fNLCH = 2048; // total number of channels in one ladder
    NLAD = Boards::NLAD;
    std::cout << "NLAD: " << NLAD << std::endl;
    fNADC = 1;
    fNTOTCH = Boards::fNTOTCH; // total number of channels from the DAQ (e.g. more than one ladder in the DAQ)
    std::cout << "fNTOTCH: " << fNTOTCH << std::endl;
    fNADCCH = fNLCH / fNADC; // total number of channels per ADC

    fvRefCalFile.clear();
    fvRedCalFile.clear();
    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fvRefCalFile.push_back("");
        fvRedCalFile.push_back("");
    }

    fCluHighThresh = 5;
    fCluHighThreshY = 7;
    fCluLowThresh = 1.5;
    fSpreadCut = 2.5;
    fPedestalsLowCut = 5;
    fPedestalsHighCut = 4090;
    fRawNoiseLowCut = 0.5;
    fRawNoiseHighCut = 10.0;
    fRawNoiseHighCutY = 50.0;

    fNoiseLowCut = 1.0;
    fNoiseHighCut = 2.5;
    fNoiseHighCutY = 15;

    fMinValidIntegral = 500;

    fPixelSearchParam = 10;
    fPixelSearchRebin = 3;

    fFileOpened = false;

    InitLayoutHints();
    InitAxes();
    InitVectors();

    SetCNcut(10);
    SetCNYcut(75);
    SetExcludeEvents(0);
    // memset(&fStripPrevious,0,sizeof(fStripPrevious));

    TGVerticalFrame *Vframe = new TGVerticalFrame(this, 200, 40);
    AddFrame(Vframe, TGLHexpandXexpandY);

    fDataTab = new TGTab(Vframe, fWidth, fHeight);
    fDataTab->Connect("Selected(Int_t)", "MyMainFrame", this, "ManageSelectedDataTab(Int_t)");
    Vframe->AddFrame(fDataTab, TGLHexpandXexpandY);

    // ADC view (no ped subtraction)
    TGCompositeFrame *TGCFAdcView = fDataTab->AddTab("ADC");
    AddAdcTab(TGCFAdcView);

    // ped subtracted view
    TGCompositeFrame *TGCFPedSubtView = fDataTab->AddTab("Ped-subt");
    AddPedSubtTab(TGCFPedSubtView);

    // signal view
    TGCompositeFrame *TGCFPedCnSubtView = fDataTab->AddTab("CN-subt");
    AddPedCnSubtTab(TGCFPedCnSubtView);

    //    TGCompositeFrame *TGCFChannelHistoView = fDataTab->AddTab("Channel-h");
    //    AddChannelHistoTab(TGCFChannelHistoView);

    // Signal over noise tab

    TGCompositeFrame *TGCFSovernView = fDataTab->AddTab("Signal over noise");
    AddSovernTab(TGCFSovernView);

    // occupancy
    TGCompositeFrame *TGCFOccupancyView = fDataTab->AddTab("Occupancy");
    AddOccupancyTab(TGCFOccupancyView);

    // cog
    TGCompositeFrame *TGCFCogView = fDataTab->AddTab("Cog");
    AddCogTab(TGCFCogView);

    // lens
    TGCompositeFrame *TGCFLensView = fDataTab->AddTab("Len");
    AddLensTab(TGCFLensView);

    // len vs cog
    TGCompositeFrame *TGCFLenVsCogView = fDataTab->AddTab("Len vs cog");
    AddLenVsCogTab(TGCFLenVsCogView);

    // ints
    TGCompositeFrame *TGCFIntsView = fDataTab->AddTab("Int");
    AddIntsTab(TGCFIntsView);

    // int vs cog
    TGCompositeFrame *TGCFIntVsCogView = fDataTab->AddTab("Int vs cog");
    AddIntVsCogTab(TGCFIntVsCogView);

    // number of clusters
    TGCompositeFrame *TGCFNclusView = fDataTab->AddTab("Nclu");
    AddNclusTab(TGCFNclusView);

    // CoGX vs CogY
    TGCompositeFrame *TGCFCogxyView = fDataTab->AddTab("CogY vs. CogX");
    AddCogxyTab(TGCFCogxyView);

    // FFT view (no ped subtraction)
    TGCompositeFrame *TGCFFftView = fDataTab->AddTab("FFT");
    AddFftTab(TGCFFftView);

    // calibratrion: pedestals
    TGCompositeFrame *TGCFPedView = fDataTab->AddTab("Pedestals");
    AddPedTab(TGCFPedView);

    // calibration: raw sigma
    TGCompositeFrame *TGCFSrawView = fDataTab->AddTab("Raw sigma");
    AddSrawTab(TGCFSrawView);

    // calibration: sigma
    TGCompositeFrame *TGCFSigmaView = fDataTab->AddTab("Sigma");
    AddSigmaTab(TGCFSigmaView);

    // calibratrion: pedestals compare
    TGCompositeFrame *TGCFPedCompView = fDataTab->AddTab("Delta Pedestals");
    AddPedCompTab(TGCFPedCompView);

    // calibratrion: sigmas compare
    TGCompositeFrame *TGCFSigmaCompView = fDataTab->AddTab("Sigma Compar");
    AddSigmaCompTab(TGCFSigmaCompView);

    // log
    TGCompositeFrame *TGCFLog = fDataTab->AddTab("Log");
    AddLogTab(TGCFLog);

    // settings
    TGCompositeFrame *TGCFSettings = fDataTab->AddTab("Settings");
    AddSettingsTab(TGCFSettings);

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGTextButton *loadfile = new TGTextButton(Hframe, "Choose file");
    loadfile->Connect("Clicked()", "MyMainFrame", this, "ChooseFile()");
    Hframe->AddFrame(loadfile, TGLHleft);

    fTeFile = new TGTextEntry(Hframe, "choose a file (.daq or .root) first");
    fTeFile->Connect("ReturnPressed()", "MyMainFrame", this, "UpdateName()");
    // fTeFile->SetBackgroundColor(0x00ff00);
    Hframe->AddFrame(fTeFile, TGLHleft);

    TGLabel *labelDAQfileName = new TGLabel(Hframe, "DAQ file:");
    Hframe->AddFrame(labelDAQfileName, TGLHleft);

    fTeDAQFile = new TGTextEntry(Hframe, "/home/herduser/Data/tests/testdaq.daq");
    fTeDAQFile->Connect("ReturnPressed()", "MyMainFrame", this, "UpdateDAQName()");
    Hframe->AddFrame(fTeDAQFile, TGLHexpandX);

    Vframe->AddFrame(Hframe, TGLHexpandX);

    HframeConfig = new TGHorizontalFrame(Vframe, 200, 40);

    TGTextButton *reconnect = new TGTextButton(HframeConfig, "Reconnect");
    reconnect->Connect("Clicked()", "MyMainFrame", this, "Reconnect()");
    HframeConfig->AddFrame(reconnect, TGLHleft);

//    TGLabel *labelFrameSize = new TGLabel(HframeConfig, "Event frame size:");
//    HframeConfig->AddFrame(labelFrameSize, TGLHleft);
//
//    TGNFrameSize = new TGNumberEntry(HframeConfig, USB_BUFFER_SIZE);
//    TGNFrameSize->SetNumStyle(TGNumberFormat::kNESInteger);
//    TGNFrameSize->SetNumAttr(TGNumberFormat::kNEANonNegative);
//    TGNFrameSize->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateFrameSize()");
//    HframeConfig->AddFrame(TGNFrameSize, TGLHleft);

    TGLabel *labelBuffer = new TGLabel(HframeConfig, "Event buffer size:");
    HframeConfig->AddFrame(labelBuffer, TGLHleft);

    TGNBufferSize = new TGNumberEntry(HframeConfig, 100);
    TGNBufferSize->SetNumStyle(TGNumberFormat::kNESInteger);
    TGNBufferSize->SetNumAttr(TGNumberFormat::kNEANonNegative);
    TGNBufferSize->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateBufferSize()");
    HframeConfig->AddFrame(TGNBufferSize, TGLHleft);

    TGLabel *labelIPaddress = new TGLabel(HframeConfig, "GPIO GUI server IP:");
    HframeConfig->AddFrame(labelIPaddress, TGLHleft);

    fTeIPaddress = new TGTextEntry(HframeConfig, fIPaddress.c_str());
    fTeIPaddress->Connect("ReturnPressed()", "MyMainFrame", this, "UpdateIPaddress()");
    HframeConfig->AddFrame(fTeIPaddress, TGLHleft);

    TGLabel *labelPort = new TGLabel(HframeConfig, "GPIO GUI port:");
    HframeConfig->AddFrame(labelPort, TGLHleft);

    TGNPort = new TGNumberEntry(HframeConfig, fPortGPIO);
    TGNPort->SetNumStyle(TGNumberFormat::kNESInteger);
    TGNPort->SetNumAttr(TGNumberFormat::kNEANonNegative);
    TGNPort->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePort()");
    HframeConfig->AddFrame(TGNPort, TGLHleft);

    TGLabel *labelIPaddressData = new TGLabel(HframeConfig, "GPIO board IP:");
    HframeConfig->AddFrame(labelIPaddressData, TGLHleft);

    fTeIPaddressData = new TGTextEntry(HframeConfig, fIPaddressData.c_str());
    fTeIPaddressData->Connect("ReturnPressed()", "MyMainFrame", this, "UpdateIPaddressData()");
    HframeConfig->AddFrame(fTeIPaddressData, TGLHleft);

    TGLabel *labelPortData = new TGLabel(HframeConfig, "GPIO board port:");
    HframeConfig->AddFrame(labelPortData, TGLHleft);

    TGNPortData = new TGNumberEntry(HframeConfig, fPortData);
    TGNPortData->SetNumStyle(TGNumberFormat::kNESInteger);
    TGNPortData->SetNumAttr(TGNumberFormat::kNEANonNegative);
    TGNPortData->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePortData()");
    HframeConfig->AddFrame(TGNPortData, TGLHleft);

    TGTextButton *loadconfigfile = new TGTextButton(HframeConfig, "Load configuration file");
    loadconfigfile->Connect("Clicked()", "MyMainFrame", this, "ChoseConfigFile()");
    HframeConfig->AddFrame(loadconfigfile, TGLHleft);

    TGLabel *labelconfig = new TGLabel(HframeConfig, "Configuration file:");
    HframeConfig->AddFrame(labelconfig, TGLHleft);

    flabelconfigtext = new TGLabel(HframeConfig, fConfigFilename.c_str());
    HframeConfig->AddFrame(flabelconfigtext, TGLHleft);

    Vframe->AddFrame(HframeConfig, TGLHexpandX);

    TGHorizontalFrame *Hframe2 = new TGHorizontalFrame(Vframe, 200, 40);
    fTGTBStartDaq = new TGTextButton(Hframe2, "Start Online DAQ");
    fTGTBStartDaq->Connect("Clicked()", "MyMainFrame", this, "StartDaq()");
    Hframe2->AddFrame(fTGTBStartDaq, TGLHleft);

    fTGTBStartrun = new TGTextButton(Hframe2, "Start");
    fTGTBStartrun->Connect("Clicked()", "MyMainFrame", this, "StartScope()");
    Hframe2->AddFrame(fTGTBStartrun, TGLHleft);

    fTGTBStoprun = new TGTextButton(Hframe2, "Stop");
    fTGTBStoprun->Connect("Clicked()", "MyMainFrame", this, "StopScope()");
    Hframe2->AddFrame(fTGTBStoprun, TGLHleft);

    fTGTBContinuerun = new TGTextButton(Hframe2, "Continue");
    fTGTBContinuerun->Connect("Clicked()", "MyMainFrame", this, "ContinueScope()");
    Hframe2->AddFrame(fTGTBContinuerun, TGLHleft);

    TGLabel *labelevent = new TGLabel(Hframe2, "event:");
    Hframe2->AddFrame(labelevent, TGLHleft);

    TGNEevent = new TGNumberEntry(Hframe2, 0);
    TGNEevent->SetNumStyle(TGNumberFormat::kNESInteger);
    TGNEevent->SetNumAttr(TGNumberFormat::kNEANonNegative);
    TGNEevent->Connect("ValueSet(Long_t)", "MyMainFrame", this, "DisplayThisEvent()");
    Hframe2->AddFrame(TGNEevent, TGLHleft);

    TGLabel *labeldebug{new TGLabel(Hframe2, "Event select:")};
    Hframe2->AddFrame(labeldebug, TGLHleft);

    TGCBEventSel = new TGComboBox(Hframe2);
    TGCBEventSel->AddEntry("No filter", 0);
    TGCBEventSel->AddEntry("Even entries", 1);
    TGCBEventSel->AddEntry("Odd entries", 2);
    TGCBEventSel->Resize(100, 20);
    TGCBEventSel->Select(0);
    // TGCBEventSel->Connect("Selected(Int_t)", "MyMainFrame", this, "UpdateEventFilter()");
    Hframe2->AddFrame(TGCBEventSel, TGLHleft);

    TGLabel *labelsuper = new TGLabel(Hframe2, "superimpose:");
    Hframe2->AddFrame(labelsuper, TGLHleft);

    TGNEsuperimpose = new TGNumberEntry(Hframe2, 0);
    TGNEsuperimpose->SetNumStyle(TGNumberFormat::kNESInteger);
    TGNEsuperimpose->SetNumAttr(TGNumberFormat::kNEANonNegative);
    TGNEsuperimpose->SetLimits(TGNumberFormat::kNELLimitMinMax, 0, 500);

    TGNEsuperimpose->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateSuperImpose()");
    Hframe2->AddFrame(TGNEsuperimpose, TGLHleft);

    fTGTBSave = new TGTextButton(Hframe2, "Save Image");
    fTGTBSave->Connect("Clicked()", "MyMainFrame", this, "SaveImage()");
    Hframe2->AddFrame(fTGTBSave, TGLHleft);

    fTGTBRecalib = new TGTextButton(Hframe2, "Recalibrate");
    fTGTBRecalib->Connect("Clicked()", "MyMainFrame", this, "ReCalibrate()");
    Hframe2->AddFrame(fTGTBRecalib, TGLHleft);

    // fTGTBChannelCal = new TGTextButton(Hframe2, "Chan. cal");
    // fTGTBChannelCal->Connect("Clicked()", "MyMainFrame", this, "StartCalibrateChHi()");
    // Hframe2->AddFrame(fTGTBChannelCal, TGLHleft);

    TGLabel *labelCalNEvents = new TGLabel(Hframe2, "N events for calibration:");
    Hframe2->AddFrame(labelCalNEvents, TGLHleft);

    TGCalNBufferSize = new TGNumberEntry(Hframe2, NPEDEVENTS);
    TGCalNBufferSize->SetNumStyle(TGNumberFormat::kNESInteger);
    TGCalNBufferSize->SetNumAttr(TGNumberFormat::kNEANonNegative);
    TGCalNBufferSize->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePedestalBuffer()");
    Hframe2->AddFrame(TGCalNBufferSize, TGLHleft);

    Vframe->AddFrame(Hframe2, TGLHexpandX);

    TGHorizontalFrame *Hframe3 = new TGHorizontalFrame(Vframe, 200, 40); // for the checkboxes

    TGCBDoCalib = new TGCheckButton(Hframe3, "Do initial calibration");
    TGCBDoCalib->SetDown();
    Hframe3->AddFrame(TGCBDoCalib, TGLHleft);

    TGCBComputeCN = new TGCheckButton(Hframe3, "CN calculation");
    TGCBComputeCN->SetDown(kTRUE);
    Hframe3->AddFrame(TGCBComputeCN, TGLHleft);

    TGCBdodynped = new TGCheckButton(Hframe3, "Dynamic pedestals");
    TGCBdodynped->SetDown(0); // dynped inactive by default
    Hframe3->AddFrame(TGCBdodynped, TGLHleft);

    TGCBSaveClusters = new TGCheckButton(Hframe3, "Save clusters to ROOT file");
    TGCBSaveClusters->SetDown(kFALSE);
    Hframe3->AddFrame(TGCBSaveClusters, TGLHleft);

    Vframe->AddFrame(Hframe3, TGLHexpandX);

    fTGHPBprogress = new TGHProgressBar(Vframe, TGProgressBar::kStandard, 300);
    // fTGHPBprogress->ShowPosition();
    fTGHPBprogress->SetBarColor("lightblue");
    fTGHPBprogress->SetRange(0, 100);
    fTGHPBprogress->SetPosition(0);

    Vframe->AddFrame(fTGHPBprogress, TGLHexpandX);

    fCurrTab = 0;
    fDataTab->SetTab(fCurrTab);
    TGTabElement *tge = fDataTab->GetCurrentTab();
    tge->SetBackgroundColor(0x00ff00);
    gClient->NeedRedraw(tge);

    SetWindowName(Form("PAN Strip display v%dr%d", VERSION, RELEASE));
    // Map all subwindows of main frame
    MapSubwindows();
    // Initialize the layout algorithm
    Resize(GetDefaultSize());
    // Map main frame
    MapWindow();

    InitGraphHis();
    fSuperImposed = 0;
    InitRefreshTimer();
    SetRunButtons(0, 1, 0, 0);
    AddLogMessage("Welcome !");
    LoadConfiguration();
    fPathOfFile = fFilePath;
    InitRefCalibrations();
    InitReduction();

    // TBrowser *tb{new TBrowser()};

    ROOT::EnableThreadSafety();

    InitCommunication();
    InitBuffer();
}

MyMainFrame::~MyMainFrame()
{
    // Clean up used widgets: frames, buttons, layout hints

    CleanupCommunication();
    delete fUnigeGpio;
    fUnigeGpio = nullptr;
    CleanupBuffer();
    delete fFitEvent;
    fFitEvent = nullptr;

    Cleanup();
}

void MyMainFrame::InitLayoutHints()
{

    TGLHexpandX = new TGLayoutHints(kLHintsExpandX, 5, 5, 3, 4);
    TGLHleftTop = new TGLayoutHints(kLHintsLeft | kLHintsTop, 10, 10, 10, 1);
    TGLHleft = new TGLayoutHints(kLHintsLeft, 5, 5, 3, 4);
    TGLHexpandXexpandY = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 10, 10, 10, 1);
}

void MyMainFrame::InitReduction()
{

    Scluster = 0; // mandatory to initialize the pointer to zero, else, you will have problems...
    Kcluster = 0;

    // initisalize the cluster options
    // fCut1=3.5;
    // fCut2=1.5;
    // fCutHighThresh=100;
    // fCutLowThresh=50;
    fMaskClu = 0xFFFFFFFF;
    fMinInt = 0;
}

void MyMainFrame::InitVectors()
{

    fPedestals.clear();
    fPedestals.assign(fNTOTCH, 0);
    fRawSigmas.clear();
    fRawSigmas.assign(fNTOTCH, 0);
    fSigmas.clear();
    fSigmas.assign(fNTOTCH, 0);
    fRefPedestals.clear();
    fRefPedestals.assign(fNTOTCH, 0);
    fRefRawSigmas.clear();
    fRefRawSigmas.assign(fNTOTCH, 0);
    fRefSigmas.clear();
    fRefSigmas.assign(fNTOTCH, 0);

    // fvCalibSlope.assign(fNLCH, 1);

    // fvCalibIntercept.assign(fNLCH, 0);

    vh2ChHi.clear();
    vh2ChHi.assign(NLAD, 0);
    vh2ChHiPe.clear();
    vh2ChHiPe.assign(NLAD, 0);

    vh2LenVsCog.clear();
    vh2LenVsCogCut.clear();

    vh2Sovern.clear();
    vh2SovernCut.clear();

    vh2IntVsCog.clear();
    vh2IntVsCogCut.clear();

    vh2LenVsCog.assign(NLAD, 0);
    vh2LenVsCogCut.assign(NLAD, 0);
    vh2Sovern.assign(NLAD, 0);
    vh2SovernCut.assign(NLAD, 0);
    vh2IntVsCog.assign(NLAD, 0);
    vh2IntVsCogCut.assign(NLAD, 0);

    vh2CogXY.clear();
    vh2CogXY.assign(NLAD, 0);

    vTH1Fp dummy;
    vvHisChHi.clear();
    vvHisChHiPe.clear();
    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        dummy.assign((vBoards.at(lad))->getNLCH() + 1, 0);
        vvHisChHi.push_back(dummy);
        vvHisChHiPe.push_back(dummy);
    }

    vhisFFT.clear();
    vhisSumFFT.clear();
    vhisFFTnorm.clear();
    vhisSumFFTnorm.clear();
    vhisOccupancy.clear();
    vhisOccupancyCut.clear();
    vhisCog.clear();
    vhisLens.clear();
    vhisIntS.clear();
    vhisCogCut.clear();
    vhisLensCut.clear();
    vhisIntSCut.clear();
    vhisNclus.clear();

    vTH1Fp templateVec;
    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        templateVec.assign((vBoards.at(lad))->getNADC(), 0);
        vhisFFT.push_back(templateVec);
        vhisSumFFT.push_back(templateVec);
        vhisFFTnorm.push_back(templateVec);
        vhisSumFFTnorm.push_back(templateVec);
    }

    vhisOccupancy.assign(NLAD, 0);
    vhisOccupancyCut.assign(NLAD, 0);
    vhisCog.assign(NLAD, 0);
    vhisLens.assign(NLAD, 0);
    vhisIntS.assign(NLAD, 0);
    vhisCogCut.assign(NLAD, 0);
    vhisLensCut.assign(NLAD, 0);
    vhisIntSCut.assign(NLAD, 0);
    vhisNclus.assign(NLAD, 0);

    vgradc.clear();
    vgrpedsubt.clear();
    vgrpedcnsubt.clear();
    vgrPixelResultsSlope.clear();
    vgrPixelResultsIntercept.clear();
    vgrclusters.clear();

    vgradc.assign(NLAD, 0);
    vgrpedsubt.assign(NLAD, 0);
    vgrpedcnsubt.assign(NLAD, 0);
    vgrPixelResultsSlope.assign(NLAD, 0);
    vgrPixelResultsIntercept.assign(NLAD, 0);
    vgrclusters.assign(NLAD, 0);

    vgradcpedsubt.clear();
    vgradcpedsubt.assign(2 * fNADC, 0);
    vgradcpedcnsubt.clear();
    vgradcpedcnsubt.assign(2 * fNADC, 0);
}

void MyMainFrame::SetRunButtons(int startDaq, int start, int stop, int contin)
{

    fTGTBStartDaq->SetEnabled((startDaq) ? kTRUE : kFALSE);
    fTGTBStartrun->SetEnabled((start) ? kTRUE : kFALSE);
    fTGTBStoprun->SetEnabled((stop) ? kTRUE : kFALSE);
    fTGTBContinuerun->SetEnabled((contin) ? kTRUE : kFALSE);
}

void MyMainFrame::InitAxes()
{
    fAdcAxis[0][0] = 0;
    fAdcAxis[0][1] = 2048;
    fAdcAxis[0][2] = 0;
    fAdcAxis[0][3] = 1000;

    fAdcAxis[1][0] = 0;
    fAdcAxis[1][1] = 128;
    fAdcAxis[1][2] = 0;
    fAdcAxis[1][3] = 1000;

    fPedSubtAxis[0][0] = 0;
    fPedSubtAxis[0][1] = 2048;
    fPedSubtAxis[0][2] = -20;
    fPedSubtAxis[0][3] = 20;

    fPedSubtAxis[1][0] = 0;
    fPedSubtAxis[1][1] = 128;
    fPedSubtAxis[1][2] = -150;
    fPedSubtAxis[1][3] = 150;

    fAdcPedSubtAxis[0] = 0;
    fAdcPedSubtAxis[1] = 2048;
    fAdcPedSubtAxis[2] = -100;
    fAdcPedSubtAxis[3] = 4096;

    fAdcPedCnSubtAxis[0] = 0;
    fAdcPedCnSubtAxis[1] = 2048;
    fAdcPedCnSubtAxis[2] = -100;
    fAdcPedCnSubtAxis[3] = 4096;

    fPedCnSubtAxis[0][0] = 0;
    fPedCnSubtAxis[0][1] = 2048;
    fPedCnSubtAxis[0][2] = -20;
    fPedCnSubtAxis[0][3] = 50;

    fPedCnSubtAxis[1][0] = 0;
    fPedCnSubtAxis[1][1] = 128;
    fPedCnSubtAxis[1][2] = -50;
    fPedCnSubtAxis[1][3] = 200;

    fPedAxis[0][0] = 0;
    fPedAxis[0][1] = 2048;
    fPedAxis[0][2] = 0;
    fPedAxis[0][3] = 1000;

    fPedAxis[1][0] = 0;
    fPedAxis[1][1] = 128;
    fPedAxis[1][2] = 0;
    fPedAxis[1][3] = 1000;

    fSrawAxis[0][0] = 0;
    fSrawAxis[0][1] = 2048;
    fSrawAxis[0][2] = 0;
    fSrawAxis[0][3] = 10;

    fSrawAxis[1][0] = 0;
    fSrawAxis[1][1] = 128;
    fSrawAxis[1][2] = 0;
    fSrawAxis[1][3] = 50;

    fSigmaAxis[0][0] = 0;
    fSigmaAxis[0][1] = 2048;
    fSigmaAxis[0][2] = 0;
    fSigmaAxis[0][3] = 10;

    fSigmaAxis[1][0] = 0;
    fSigmaAxis[1][1] = 128;
    fSigmaAxis[1][2] = 0;
    fSigmaAxis[1][3] = 30;

    fPedCompAxis[0][0] = 0;
    fPedCompAxis[0][1] = 2048;
    fPedCompAxis[0][2] = -50;
    fPedCompAxis[0][3] = 1000;

    fPedCompAxis[1][0] = 0;
    fPedCompAxis[1][1] = 128;
    fPedCompAxis[1][2] = -50;
    fPedCompAxis[1][3] = 1000;

    fSigmaCompAxis[0][0] = 0;
    fSigmaCompAxis[0][1] = 2048;
    fSigmaCompAxis[0][2] = -50;
    fSigmaCompAxis[0][3] = 500;

    fSigmaCompAxis[1][0] = 0;
    fSigmaCompAxis[1][1] = 128;
    fSigmaCompAxis[1][2] = -50;
    fSigmaCompAxis[1][3] = 500;

    fFftAxis[0][0] = 0;
    fFftAxis[0][1] = 2048;
    fFftAxis[0][2] = 0;
    fFftAxis[0][3] = 100;

    fFftAxis[1][0] = 0;
    fFftAxis[1][1] = 128;
    fFftAxis[1][2] = 0;
    fFftAxis[1][3] = 100;

    fCogxyAxis[0] = -10;
    fCogxyAxis[1] = 60;
    fCogxyAxis[2] = -10;
    fCogxyAxis[3] = 60;

    fPhElCalAxis[0] = 0;
    fPhElCalAxis[1] = 384;
    fPhElCalAxis[2] = 0;
    fPhElCalAxis[3] = 500;

    fChannel = 0;
    fChannel_min = 0;
    fChannel_max = 127;

    fSovernAxis[0][0] = 0;
    fSovernAxis[0][1] = 2048;
    fSovernAxis[0][2] = 0;
    fSovernAxis[0][3] = 50;

    fSovernAxis[1][0] = 0;
    fSovernAxis[1][1] = 128;
    fSovernAxis[1][2] = 0;
    fSovernAxis[1][3] = 50;

    fOccupancyAxis[0][0] = 0;
    fOccupancyAxis[0][1] = 2048;
    fOccupancyAxis[0][2] = 0;
    fOccupancyAxis[0][3] = 100;

    fOccupancyAxis[1][0] = 0;
    fOccupancyAxis[1][1] = 128;
    fOccupancyAxis[1][2] = 0;
    fOccupancyAxis[1][3] = 100;

    fCogAxis[0][0] = 0;
    fCogAxis[0][1] = 2048;
    fCogAxis[0][2] = 0;
    fCogAxis[0][3] = 20;

    fCogAxis[1][0] = 0;
    fCogAxis[1][1] = 128;
    fCogAxis[1][2] = 0;
    fCogAxis[1][3] = 20;

    fLensAxis[0][0] = 0;
    fLensAxis[0][1] = 40;
    fLensAxis[0][2] = 0;
    fLensAxis[0][3] = 1000;

    fLensAxis[1][0] = 0;
    fLensAxis[1][1] = 40;
    fLensAxis[1][2] = 0;
    fLensAxis[1][3] = 1000;

    fLenVsCogAxis[0][0] = 0;
    fLenVsCogAxis[0][1] = 2048;
    fLenVsCogAxis[0][2] = 0;
    fLenVsCogAxis[0][3] = 20;

    fLenVsCogAxis[1][0] = 0;
    fLenVsCogAxis[1][1] = 128;
    fLenVsCogAxis[1][2] = 0;
    fLenVsCogAxis[1][3] = 20;

    fIntVsCogAxis[0][0] = 0;
    fIntVsCogAxis[0][1] = 2048;
    fIntVsCogAxis[0][2] = 0;
    fIntVsCogAxis[0][3] = 200;

    fIntVsCogAxis[1][0] = 0;
    fIntVsCogAxis[1][1] = 128;
    fIntVsCogAxis[1][2] = 0;
    fIntVsCogAxis[1][3] = 1000;

    fIntsAxis[0][0] = 0;
    fIntsAxis[0][1] = 200;
    fIntsAxis[0][2] = 0;
    fIntsAxis[0][3] = 400;

    fIntsAxis[1][0] = 0;
    fIntsAxis[1][1] = 600;
    fIntsAxis[1][2] = 0;
    fIntsAxis[1][3] = 400;

    fNclusAxis[0][0] = 0;
    fNclusAxis[0][1] = 50;
    fNclusAxis[0][2] = 1;
    fNclusAxis[0][3] = 100000;

    fNclusAxis[1][0] = 0;
    fNclusAxis[1][1] = 50;
    fNclusAxis[1][2] = 1;
    fNclusAxis[1][3] = 100000;
}

vector<string> MyMainFrame::SplitString(string fullfilename, string separator)
{

    size_t found = fullfilename.find_last_of(separator);

    vector<string> pathinfo;

    pathinfo.push_back(fullfilename.substr(0, found));
    pathinfo.push_back(fullfilename.substr(found + 1));

    return pathinfo;
}

void MyMainFrame::LoadConfiguration()
{

    string confname = gSystem->HomeDirectory();
    confname += "/.DisPan";

    ifstream confFile(confname);
    if (!confFile.is_open())
    {
        AddLogMessage("No configuration file available, a new one will be saved in your home directory when you will exit from DisPan.");
        // cout << "no configuration file available" << endl;
        return;
    }

    string line;
    while (1)
    {
        if (!(getline(confFile, line)))
            break;
        SetConfiguration(line);
    }

    confFile.close();
}

void MyMainFrame::SetConfiguration(string line)
{

    string parnam;

    parnam = ParamName[0];
    if (line.find(parnam) == 0)
    {
        fCalRefPath = line.substr(parnam.size(), line.size() - parnam.size());
        // cout << "fImagePath=" << fImagePath << endl;
        AddLogMessage("fCalRefPath=" + fCalRefPath);
        return;
    }

    parnam = ParamName[1];
    if (line.find(parnam) == 0)
    {
        fCalPath = line.substr(parnam.size(), line.size() - parnam.size());
        // cout << "fImagePath=" << fImagePath << endl;
        AddLogMessage("fCalPath=" + fCalPath);
        return;
    }

    parnam = ParamName[2];
    if (line.find(parnam) == 0)
    {
        fImagePath = line.substr(parnam.size(), line.size() - parnam.size());
        // cout << "fImagePath=" << fImagePath << endl;
        AddLogMessage("fImagePath=" + fImagePath);
        return;
    }

    parnam = ParamName[3];
    if (line.find(parnam) == 0)
    {
        fFilePath = line.substr(parnam.size(), line.size() - parnam.size());
        // cout << "fImagePath=" << fImagePath << endl;
        AddLogMessage("fFilePath=" + fFilePath);
        return;
    }

    parnam = ParamName[4];
    if (line.find(parnam) == 0)
    {
        fConfigFilename = line.substr(parnam.size(), line.size() - parnam.size());
        // cout << "fImagePath=" << fImagePath << endl;
        flabelconfigtext->SetText(fConfigFilename.c_str());
        if (!fConfigFilename.empty())
            SetRunButtons(1, 1, 0, 0);
        AddLogMessage("fConfigFilename=" + fConfigFilename);
        return;
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        parnam = ParamName[5 + lad];
        if (line.find(parnam) == 0)
        {
            fvRefCalFile.at(lad) = line.substr(parnam.size(), line.size() - parnam.size());
            // cout << "fImagePath=" << fImagePath << endl;
            AddLogMessage(Form("fRefCalFile_%d=", lad) + fvRefCalFile.at(lad));
            return;
        }
    }

    int start = 5 + NLAD;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        parnam = ParamName[start + lad];
        if (line.find(parnam) == 0)
        {
            fvRedCalFile.at(lad) = line.substr(parnam.size(), line.size() - parnam.size());
            // cout << "fImagePath=" << fImagePath << endl;
            AddLogMessage(Form("fRedCalFile_%d=", lad) + fvRedCalFile.at(lad));
            return;
        }
    }
}

void MyMainFrame::SaveConfiguration()
{

    string confname = gSystem->HomeDirectory();
    confname += "/.DisPan";

    ofstream confFile(confname);
    if (!confFile.is_open())
    {
        AddLogMessage("File problem, configuration file cannot be saved.");
        return;
    }

    string line;
    line = ParamName[0] + fCalRefPath;
    confFile << line << endl;

    line = ParamName[1] + fCalPath;
    confFile << line << endl;

    line = ParamName[2] + fImagePath;
    confFile << line << endl;

    line = ParamName[3] + fFilePath;
    confFile << line << endl;

    line = ParamName[4] + fConfigFilename;
    confFile << line << endl;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        line = ParamName[5 + lad] + fvRefCalFile.at(lad);
        confFile << line << endl;
    }

    int start = 5 + NLAD;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        line = ParamName[start + lad] + fvRedCalFile.at(lad);
        confFile << line << endl;
    }

    confFile.close();
}

void MyMainFrame::ManageSelectedDataTab(int tabnum)
{

    TGTabElement *tge = 0;

    int oldtab = fCurrTab;

    fCurrTab = tabnum;

    tge = fDataTab->GetTabTab(fCurrTab);
    Pixel_t normalcol = tge->GetBackground();

    tge->SetBackgroundColor(0x00ff00);
    gClient->NeedRedraw(tge);

    tge = fDataTab->GetTabTab(oldtab);
    tge->SetBackgroundColor(normalcol);

    // cout << "before needredraw" << endl;
    gClient->NeedRedraw(tge);
}

void MyMainFrame::ChooseFile()
{

    // static TString dir(".");
    TString dir(fPathOfFile);
    TGFileInfo fi;
    fi.fFileTypes = filetypes;
    fi.fIniDir = StrDup(dir);
    // fi.fIniDir    = StrDup("/Users/cperrina/fibres2016/test_beam_october_2017/daq/root/");
    // fi.fFilename = StrDup("filenotchosen");//sprintf(fi.fFilename,"filenotchosen");
    // printf("fIniDir = %s\n", fi.fIniDir);

    cout << "fIniDir = " << fi.fIniDir << endl;

    new TGNewFileDialog(fClient->GetRoot(), this, kNFDOpen, &fi);

    if (fi.fFilename == 0)
        return;

    fFilename = fi.fFilename;

    fTeFile->SetText(fFilename.c_str());

    fFilenameWOPath = fFilename.substr(1 + fFilename.find_last_of("/"));
    fPathOfFile = fFilename.substr(0, fFilename.find_last_of("/"));
    fFilenameWOExt = fFilenameWOPath.substr(0, fFilenameWOPath.find_last_of("."));

    cout << fFilenameWOPath << "  " << fPathOfFile << "  " << fFilenameWOExt << endl;
    if (OpenFile())
    {
        AddLogMessage(Form("Error with file %s", fFilename.c_str()));
        fTeFile->SetBackgroundColor(0xfecccd);
    }
    else
    {
        AddLogMessage(Form("File %s is successfully open", fFilename.c_str()));
        fTeFile->SetBackgroundColor(0xcdffcf);
    }
    // else TGTVlog->Clear();
    // StartScope();
}

vector<unsigned int> MyMainFrame::ReadFourWords(FILE *afile)
{
    unsigned int adcValue{0};
    vector<unsigned int> fourWords;
    unsigned char val1, val2, val3, val4;

    for (int i{0}; i < 4; i++)
    {
        fread(&val1, sizeof(val1), 1, afile);
        fread(&val2, sizeof(val2), 1, afile);
        fread(&val3, sizeof(val3), 1, afile);
        fread(&val4, sizeof(val4), 1, afile);
        adcValue = val1 + (val2 << 8) + (val3 << 16) + (val4 << 24);

        fourWords.push_back(adcValue);
    }

    return fourWords;
}

unsigned int MyMainFrame::ReadSingleWord(FILE *afile) {
	
	unsigned char val1{'\0'}, val2{'\0'}, val3{'\0'}, val4{'\0'};
	std::size_t n{0};
	
	n = fread(&val1, sizeof(val1), 1, afile);
    n = fread(&val2, sizeof(val2), 1, afile);
    n = fread(&val3, sizeof(val3), 1, afile);
    n = fread(&val4, sizeof(val4), 1, afile);
    return static_cast<unsigned int>(val1 + (val2 << 8) + (val3 << 16) + (val4 << 24));
}

int MyMainFrame::ConvertDaqToRootNew()
{

    constexpr unsigned int FRAME_START{0x80AAAAAA};
    constexpr unsigned int FRAME_END{0x80333333};

    string inputFileName{fFilename};
    string outputFileName{fFilename.substr(0, fFilename.length() - 4)};
    outputFileName += ".root";

    unsigned short adcArray[fNTOTCH], channel_type{0};
    unsigned int eventNumber{0}, frameType{0}, eventTag{0}, event{0}, adc1{0}, adc2{0}, current_word{0}, big_frame_counter{0}, MSB{0}, OldMSB{0};
    ULong64_t frame_counter{0}, frame_counter_prev{0}, TScounter{0};
    Double_t eventTime[2];
    Double_t oldTime{0};
    eventTime[0]=0;
    eventTime[1] = 0;
    bool eventTagChanged{false};
    TFile *out_file = new TFile(outputFileName.c_str(), "recreate");

    TTree *T = new TTree("T", "T");
    T->Branch("adc", adcArray, Form("adc[%d]/s", fNTOTCH));
    T->Branch("nevent", &eventNumber, "nevent/i");
    T->Branch("frameType", &frameType, "frameType/i");
    T->Branch("eventTag", &eventTag, "eventTag/i");
    T->Branch("nframe", &frame_counter, "frame_counter/l");
    T->Branch("eventTime", eventTime, "eventTimeSt/D:deltaTime/D");

    FILE *file = fopen(inputFileName.c_str(), "rb");
    
    vector<unsigned int> data;

    if (file == 0)
    {
        AddLogMessage(Form("Input file %s not found, stop.", inputFileName.c_str()));
        return 1;
    }

    // to understand progress, let's find the file size
    fseek(file, 0, SEEK_END);
    long int FileSize{ftell(file)};
    rewind(file);
    int FileProgressStep{0};

    while (1)
    {
        if (feof(file))
            break;
        double FileProgress{(double)(ftell(file))/FileSize*100};
        if (FileProgress>=FileProgressStep*5) {
            cout << "File conversion progress: " << round(FileProgress) << " %"<< endl;
            FileProgressStep++;
        }
        data.clear();
        data = ReadFourWords(file);

        if (data.at(0) == FRAME_START)
        {
            frameType = data.at(1);
            eventNumber = data.at(2) & 0xFFFFFF;
            eventNumber += ((data.at(3) >> 16) & 0xFF) << 24;
            eventTag = data.at(3) & 0x7;
            eventTagChanged = false;

            unsigned int code0{frameType & 0xFF};
            unsigned int code1{(frameType >> 8) & 0xFF};
            unsigned int nchannels{2048 / 8}; // sum over all fNLCH / fNADC per board?
            if (code0 == 0x33) nchannels = 128;
            if (code1 == 0x59 && code0 != 0x33) {
                nchannels = 2048 / 2;
                nchannels += 128;
                if (code0 == 0x32) nchannels += 2048 / 2;
            }
            unsigned int first{0};
            unsigned int channel{0}, stripYchannel{0}, stripX0channel{0}, stripX1channel{0};
            unsigned int stripX1adc{0}, stripX0adc{0};

            // now we read the data.
            for (unsigned int i{0}; i < nchannels; i++)
            {
                if (code1 == 0x58)
                {
                    data.clear();
                    data = ReadFourWords(file);
                    for (int j{0}; j < 4; j++)
                    {
                        adc1 = data.at(j) & 0x1FFF;
                        adc2 = (data.at(j) >> 13) & 0x1FFF;
                        if (((data.at(j) >> 26) & 0x7) != eventTag)
                        {
                            std::cout << "A: event tag changed within the event, skipping to the next one..." << std::endl;
                            std::cout << "iteration: " << event << std::endl;
                            std::cout << "eventTag of a frame: " << eventTag << std::endl;
                            std::cout << "new eventTag: " << ((data.at(j) >> 26) & 0x7) << std::endl;
                            std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                            std::cout << "eventNumber: " << eventNumber << std::endl;
                            eventTagChanged = true;
                            current_word = data.at(j);
                            break;
                        }
                        channel = 2 * j * 2048 / 8 + i;
                        adcArray[first + channel] = adc1;
                        channel = (2 * j + 1) * 2048 / 8 + i;
                        adcArray[first + channel] = adc2;
                    }
                    if (eventTagChanged) {
                        while (!feof(file)) // important in particular if we are reading a file not closed yet (i.e. while DAQ is still running)
                        {
                            std::cout << "A: current_word = 0x" << std::hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << std::dec << std::endl;
                            //if (((current_word >> 24) & 0xFF) == 0x84) {
                            //    std::cout << "0x84 found - breaking" << std::endl;
                            //    break;
                            //}
                            // there is a bug in the FW: the CRC word might not be written correcly anymore once we have an uncomplete event (the 0x84 identifier is not written - for the least), and this happens also for all the successive events
                            if (current_word == 0x80333333){ // as a replacement: we find the end frame word, then we read 3 more words
                                current_word = ReadSingleWord(file);
                                current_word = ReadSingleWord(file);
                                current_word = ReadSingleWord(file);
                                break;
                            }
                            current_word = ReadSingleWord(file);
                        }
                        break;
                    }
                    if (code0 == 0x32)
                    {
                        data.clear();
                        data = ReadFourWords(file);
                        for (int j{0}; j < 4; j++)
                        {
                            adc1 = data.at(j) & 0x1FFF;
                            adc2 = (data.at(j) >> 13) & 0x1FFF;
                            if (((data.at(j) >> 26) & 0x7) != eventTag)
                            {
                                std::cout << "B: event tag changed within the event, skipping to the next one..." << std::endl;
                                std::cout << "iteration: " << event << std::endl;
                                std::cout << "eventTag of a frame: " << eventTag << std::endl;
                                std::cout << "new eventTag: " << ((data.at(j) >> 26) & 0x7) << std::endl;
                                std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                                std::cout << "eventNumber: " << eventNumber << std::endl;
                                eventTagChanged = true;
                                current_word = data.at(j);
                                break;
                            }
                            channel = 2 * j * 2048 / 8 + i;
                            adcArray[first + channel + 2048] = adc1;
                            channel = (2 * j + 1) * 2048 / 8 + i;
                            adcArray[first + channel + 2048] = adc2;
                        }
                    }
                    if (eventTagChanged) {
                        while (!feof(file)) // important in particular if we are reading a file not closed yet (i.e. while DAQ is still running)
                        {
                            std::cout << "B: current_word = 0x" << std::hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << std::dec << std::endl;
                            //if (((current_word >> 24) & 0xFF) == 0x84) {
                            //    std::cout << "0x84 found - breaking" << std::endl;
                            //    break;
                            //}
                            // there is a bug in the FW: the CRC word might not be written correcly anymore once we have an uncomplete event (the 0x84 identifier is not written - for the least), and this happens also for all the successive events
                            if (current_word == 0x80333333){ // as a replacement: we find the end frame word, then we read 3 more words
                                current_word = ReadSingleWord(file);
                                current_word = ReadSingleWord(file);
                                current_word = ReadSingleWord(file);
                                break;
                            }
                            current_word = ReadSingleWord(file);
                        }
                        break;
                    }
                }
                else if (code1 == 0x59 && code0 != 0x33)
                {
                    current_word = ReadSingleWord(file);
                    channel_type = (current_word >> 29) & 0x3;
                    if (channel_type != 3)
                    {
                        adc1 = current_word & 0x1FFF;
                        adc2 = (current_word >> 13) & 0x1FFF;
                        if (((current_word >> 26) & 0x7) != eventTag)
                        {
                            std::cout << "C: event tag changed within the event, skipping to the next one..." << std::endl;
                            std::cout << "iteration: " << event << std::endl;
                            std::cout << "eventTag of a frame: " << eventTag << std::endl;
                            std::cout << "new eventTag: " << ((current_word >> 26) & 0x7) << std::endl;
                            std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                            std::cout << "eventNumber: " << eventNumber << std::endl;
                            std::cout << "current word = 0x" << hex << current_word << "    adc1 = 0x" << adc1 << "    adc2 = 0x" << adc2 << dec << std::endl;
                            eventTagChanged = true;
                            while (!feof(file)) // important in particular if we are reading a file not closed yet (i.e. while DAQ is still running)
                            {
                                cout << "C: current_word = 0x" << hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << dec << endl;
                                //if (((current_word >> 24) & 0xFF) == 0x84) {
                                //    std::cout << "0x84 found - breaking" << std::endl;
                                //    break;
                                //}
                                // there is a bug in the FW: the CRC word might not be written correcly anymore once we have an uncomplete event (the 0x84 identifier is not written - for the least), and this happens also for all the successive events
                                if (current_word == 0x80333333){ // as a replacement: we find the end frame word, then we read 3 more words
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    break;
                                }
                                current_word = ReadSingleWord(file);
                            }
                            break;
                        }
                        if (code0 != 0x32 || channel_type == 0)
                        {
                            channel = 2 * stripX0adc * 2048 / 8 + stripX0channel;
                            adcArray[first + channel] = adc1;
                            channel = (2 * stripX0adc + 1) * 2048 / 8 + stripX0channel;
                            adcArray[first + channel] = adc2;
                            stripX0adc++;
                            if (stripX0adc > 3) {
								stripX0adc = 0;
								stripX0channel++;
							}
                        }
                        else if (channel_type == 1)
                        {
                            channel = 2 * stripX1adc * 2048 / 8 + stripX1channel;
                            adcArray[first + channel + 2048] = adc1;
                            channel = (2 * stripX1adc + 1) * 2048 / 8 + stripX1channel;
                            adcArray[first + channel + 2048] = adc2;
                            stripX1adc++;
                            if (stripX1adc > 3)
                            {
                                stripX1adc = 0;
                                stripX1channel++;
                            }
                        }
                    }
                    else
                    {
                        adc1 = current_word & 0x3FFFFFF;
                        if (((current_word >> 26) & 0x7) != eventTag)
                        {
                            std::cout << "D: event tag changed within the event, skipping to the next one..." << std::endl;
                            std::cout << "iteration: " << event << std::endl;
                            std::cout << "eventTag of a frame: " << eventTag << std::endl;
                            std::cout << "new eventTag: " << ((current_word >> 26) & 0x7) << std::endl;
                            std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                            std::cout << "eventNumber: " << eventNumber << std::endl;
                            eventTagChanged = true;
                            while (!feof(file))
                            {
                                //if (current_word >> 24 == 0x84)
                                //    break;
                                //current_word = ReadSingleWord(file);
                                //cout << "D: current_word = 0x" << hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << dec << endl;
                                if (current_word == 0x80333333)
                                { // as a replacement: we find the end frame word, then we read 3 more words
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    break;
                                }
                                current_word = ReadSingleWord(file);
                            }
                            break;
                        }
                        if (code0 != 0x32)
                        {
                            channel = stripYchannel;
                            adcArray[first + 2048 + channel] = adc1;
                        }
                        else
                        {
                            channel = stripYchannel;
                            adcArray[first + 2 * 2048 + channel] = adc1;
                        }
                        stripYchannel++;
                    }
                }
                else
                {
                    data.clear();
                    data = ReadFourWords(file);
                    for (int j{0}; j < 4; j++)
                    {
                        adc1 = data.at(j) & 0x3FFFFFF;
                        if (((data.at(j) >> 26) & 0x7) != eventTag)
                        {
                            std::cout << "E: event tag changed within the event, skipping to the next one..." << std::endl;
                            std::cout << "iteration: " << event << std::endl;
                            std::cout << "eventTag of a frame: " << eventTag << std::endl;
                            std::cout << "new eventTag: " << ((data.at(j) >> 26) & 0x7) << std::endl;
                            std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                            std::cout << "eventNumber: " << eventNumber << std::endl;
                            eventTagChanged = true;
                            current_word = data.at(j);
                            break;
                        }
                        channel = j + i;
                        adcArray[first + channel] = adc1;
                    }
                    i += 3;
                    if (eventTagChanged) {
                        while (!feof(file)) // important in particular if we are reading a file not closed yet (i.e. while DAQ is still running)
                        {
                            std::cout << "A: current_word = 0x" << std::hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << std::dec << std::endl;
                            //if (((current_word >> 24) & 0xFF) == 0x84) {
                            //    std::cout << "0x84 found - breaking" << std::endl;
                            //    break;
                            //}
                            // there is a bug in the FW: the CRC word might not be written correcly anymore once we have an uncomplete event (the 0x84 identifier is not written - for the least), and this happens also for all the successive events
                            if (current_word == 0x80333333){ // as a replacement: we find the end frame word, then we read 3 more words
                                current_word = ReadSingleWord(file);
                                current_word = ReadSingleWord(file);
                                current_word = ReadSingleWord(file);
                                break;
                            }
                            current_word = ReadSingleWord(file);
                        }
                        break;
                    }
                }
            }
        }
        else if (data.at(0) == FRAME_END)
        {
            MSB=(data.at(1) & 0x3F);
            if (OldMSB>MSB){
                //cout << hex << "oldmsb =0x" << OldMSB << " MSB=0x" << MSB << dec << endl;
                big_frame_counter++;
            }
            OldMSB = MSB;
            frame_counter = static_cast<ULong64_t>((data.at(1)) & 0x3F) << 31; // !!! very nasty !!! you need to force the conversion to ULong64_t else bits >31 will be cut (i.e. in this case all of them...).
            frame_counter += (data.at(2) & 0x7FFFFFFF);
            //if (frame_counter_prev > frame_counter)
            //    big_frame_counter++;
            //frame_counter_prev = frame_counter;
            TScounter = frame_counter + (static_cast<ULong64_t>(big_frame_counter * 0x40) << 31);
            eventTime[0] = TScounter * (1./40.e+6); // Clock freq is 40 MHz
            eventTime[1] = eventTime[0]-oldTime;
            oldTime = eventTime[0];
            //cout << "trigger = " << data.at(3) << "  bigcnt = " << big_frame_counter << "  " << TScounter << "   " << eventTime[0] << endl;
            if (!eventTagChanged)
            {
                T->Fill();
                memset(&adcArray, 0, sizeof(adcArray));
            }
            event++;
            // counter = 0;
        } else {
            if (feof(file))
                break;
            cout << "Event " << eventNumber <<  " :  Exception: 0x" << hex << data.at(0) << "  0x" << data.at(1) << "  0x" << data.at(2) << "  0x" << data.at(3) << endl;
            cout << "we try to locate again the end frame" << endl;
            current_word = ReadSingleWord(file);
            while (!feof(file))
            {
                // if (current_word >> 24 == 0x84)
                //     break;
                // current_word = ReadSingleWord(file);
                //cout << "Excpt: current_word = 0x" << hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << dec << endl;
                if (current_word == 0x80333333)
                { // as a replacement: we find the end frame word, then we read 3 more words
                    current_word = ReadSingleWord(file);
                    current_word = ReadSingleWord(file);
                    current_word = ReadSingleWord(file);
                    break;
                }
                current_word = ReadSingleWord(file);
            }
        }
    }
    T->Write(0, TObject::kOverwrite);
    AddLogMessage(Form("We have found %lld events\n", T->GetEntries()));
    out_file->Write(0, TObject::kOverwrite);
    T->Delete();
    out_file->Close();

    delete out_file;

    return 0;
}

int MyMainFrame::ConvertDaqToRoot(long removebytes)
{

    string inputFileName{fFilename};
    string outputFileName{inputFileName.substr(0, inputFileName.length() - 4)};
    outputFileName += (removebytes) ? "_repaired.root" : ".root";

    TFile *out_file = new TFile(outputFileName.c_str(), "recreate");

    // ushort adc[fNTOTCH], padc[fNTOTCH];
    unsigned char trigger, ev_id;
    unsigned short adcArray[fNTOTCH], frameType;
    unsigned int eventNumber, adcValue;
    unsigned char val1, val2, val3, val4, dummy; // bytes

    TTree *T = new TTree("T", "T");
    T->Branch("adc", adcArray, Form("adc[%d]/s", fNTOTCH));
    T->Branch("nevent", &eventNumber, "nevent/i");
    T->Branch("frameType", &frameType, "frameType/s");

    FILE *file = fopen(inputFileName.c_str(), "rb");

    if (file == 0)
    {
        AddLogMessage(Form("Input file %s not found, stop.", inputFileName.c_str()));
        return 1;
    }
    // trick to manage files with wrong size (file still might be corrupted though.... --> careful look at the data should be necessary in that case !)
    for (int i{0}; i < removebytes; i++)
        fread(&dummy, sizeof(dummy), 1, file);

    uint channel{0};
    int counter{0};
    int event{0};
    int pedcounter{0};
    unsigned short wordIndex{0}, tag{0}, adc1{0}, adc2{0}, stripX_type{0}, AdcIndex{0}, ChannelIndex{0};
    do
    {
        if (feof(file))
            break;

        fread(&val1, sizeof(val1), 1, file);
        fread(&val2, sizeof(val2), 1, file);
        fread(&val3, sizeof(val3), 1, file);
        fread(&val4, sizeof(val4), 1, file);
        adcValue = val1 + (val2 << 8) + (val3 << 16) + (val4 << 24);
        // std::cout << Form("0x%02x 0x%02x 0x%04x %4d", val1, val2, adcValue, adcValue) << endl;

        wordIndex++;
        if (wordIndex == 4)
            wordIndex = 0;

        if (adcValue == 0x80AAAAAA)
        {
            wordIndex = 0;
            continue;
        }

        if (adcValue == 0x80333333 || adcValue >> 24 == 0x84)
        {
            continue;
        }

        if (adcValue >> 24 == 0x81)
        {
            frameType = adcValue & 0xFF;
            continue;
        }

        if (adcValue >> 24 == 0x82)
        {
            eventNumber = adcValue & 0xFFFFFF;
            continue;
        }

        if (adcValue >> 24 == 0x83)
        {
            tag = adcValue & 0x7;
            counter = 0;
            continue;
        }

        if (adcValue >> 31 == 0x1)
        {
            continue;
        }

        adc1 = adcValue & 0x1FFF;
        adc2 = (adcValue >> 13) & 0x1FFF;
        stripX_type = (adcValue >> 29) & 0x3;
        // cout << "adcValue = 0x" << hex << adcValue << endl;
        // cout << "stripX_type = " << dec << stripX_type << endl;

        if (frameType != 0x32)
            stripX_type = 0;

        if (((adcValue >> 26) & 0x7) != tag)
        {
            counter++;
            continue;
        }

        switch (wordIndex)
        {
        case 0:
            AdcIndex = 0;
            break;

        case 1:
            AdcIndex = 2;
            break;

        case 2:
            AdcIndex = 4;
            break;

        case 3:
            AdcIndex = 6;
            break;
        }

        if (frameType != 0x32)
            ChannelIndex = 2 * counter / fNADC;
        else
            ChannelIndex = counter / fNADC;

        channel = stripX_type * fNLCH + AdcIndex * fNADCCH + ChannelIndex;
        // std::cout << "event = " << event << "   counter = " << counter << "    channel = " << channel << endl;

        // vh_adc.at(AdcIndex)->Fill(adcValue);
        adcArray[channel] = adc1;
        channel = stripX_type * fNLCH + (AdcIndex + 1) * fNADCCH + ChannelIndex;
        adcArray[channel] = adc2;
        // h_adc2->Fill(adc2);
        // h_adc1->Fill(adc1);

        // cout << hex << "0x" << val << endl ;
        // cout << dec << (int)trigger << " ";
        // cout << dec << (int)ev_id << " ";
        // cout << dec << adc2 << " ";
        // cout << dec << adc1 << endl;

        counter++;

        if (channel == fNTOTCH - 1)
        {

            T->Fill();
            memset(&adcArray, 0, sizeof(adcArray));
            event++;
            counter = 0;
        }

    } while (1);

    T->Write(0, TObject::kOverwrite);
    AddLogMessage(Form("We have found %lld events\n", T->GetEntries()));
    out_file->Write(0, TObject::kOverwrite);

    delete out_file;

    return 0;
}

int MyMainFrame::OpenFile()
{
    fFileOpened = false;
    string message = "";

    string filetype{fFilename.substr(fFilename.length() - 3)};
    if (filetype == "daq")
    {
        int ret{1};
        long fsize{GetFileSize(fFilename)}; // in bytes
        std::cout << "file size is : " << fsize << std::endl;
        long deltasize{(fsize - 4) % 8224}; // 8224 bytes per event (new format)
        if (deltasize)
            std::cout << "look out ! deltasize=" << deltasize << std::endl;
        else
            std::cout << "file size is ok" << std::endl;
        // (fileSizeInWords - 2) % (4*64*8)
        // ret = ConvertDaqToRoot(deltasize);
        ret = ConvertDaqToRootNew();
        if (ret)
            return 10;

        string gen{fFilename.substr(0, fFilename.length() - 4)};
        fFilename = gen + ".root";
    }

    fFile = new TFile(fFilename.c_str());
    if (!fFile->IsOpen())
    {
        // cout << "problem with file " << fFilename << endl;
        message = "Problem opening file " + fFilename;
        AddLogMessage(message);
        return 1;
    }

    fFile->GetObject("T", fTree);

    if (fTree == 0)
    {
        AddLogMessage("Problem with tree pointer");
        return 2;
    }

    // C fSilevents=fTree->GetBranch("strip[4608]");
    fSilevents = fTree->GetBranch("adc");
    fTree->SetBranchAddress("nevent", &fEventNumberDAQ);
    fTree->SetBranchAddress("nframe", &fFramaCounterDAQ);
    // cout << "silevents: " << silevents << endl;

    if (fSilevents == 0)
    {
        AddLogMessage("data branch not found");
        return 3;
    }

    fSilevents->SetAddress(&fStrip);

    fNevents = fTree->GetEntries();
    AddLogMessage(Form("File %s opened successfully", fFilename.c_str()));
    AddLogMessage(Form("%d entries found in tree", fNevents));
    fFileOpened = true;
    return 0;
}

void MyMainFrame::Reconnect()
{

    try
    {
        CleanupCommunication();
        fUnigeGpio->SetQuitLoop(false);
        if (fUnigeGpio->OpenConnection())
        {
            throw std::runtime_error("connection to GPIO has a problem");
        }
        CleanupBuffer();
        fFitEvent->SetQuitLoop(true);
        fFitEvent->SetPANEnabled(true);
        fFitEvent->SetMaxMsg(USB_BUFFER_SIZE);
        fFitEvent->SetAddress(fIPaddress);
        fFitEvent->SetPort(fPortData);
        fFitEvent->SetBoardAddress(fIPaddressData);
        int status = fFitEvent->SetupUDPServer();
        if (status == 0)
        {
            bool result = fFitEvent->BindUDPServer();
            if (!result)
                throw std::runtime_error("Binding to a socket failed");
        }
        else
            throw std::runtime_error("Socket server setup failed");
        fTGTBStartDaq->SetEnabled(kTRUE);
    }
    catch (const std::exception& e)
    {
        std::cerr << "Connection was not established previously" << std::endl;
        std::cerr << "Caught an exception: " << e.what() << std::endl;
        fTGTBStartDaq->SetEnabled(kFALSE);
        CleanupCommunication();
        delete fUnigeGpio;
        fUnigeGpio = nullptr;
        InitCommunication();
        CleanupBuffer();
        delete fFitEvent;
        fFitEvent = nullptr;
        InitBuffer();
    }
}

void MyMainFrame::ChoseConfigFile()
{

    TGFileInfo fi;
    new TGNewFileDialog(fClient->GetRoot(), this, kNFDOpen, &fi);
    if (fi.fFilename == 0)
        return;
    if (!fConfigFilename.empty())
        SetRunButtons(1, 1, 0, 0);
    fConfigFilename = fi.fFilename;
    flabelconfigtext->SetText(fConfigFilename.c_str());
    HframeConfig->Layout();
    SetRunButtons(1, 1, 0, 0);
}

void MyMainFrame::UpdateDAQName()
{

    fDAQfileName = fTeDAQFile->GetText();
    fFitEvent->SetTimeStampFile(fDAQfileName);
}

void MyMainFrame::StartScope()
{
    if (!fFileOpened)
        return;

    AddLogMessage("Starting run");
    InitRunningCN();
    // we need to clear all the root objects

    if (TGCBDoCalib->IsDown())
    {
        ComputePedestals(ExcludeEvents);
        // cout << "1: ComputePedestals2 100" << endl;
        // ComputePedestals2(0, 100);
        // cout << "2: ComputePedestals2 50" << endl;
        // ComputePedestals2(0, 50);
        // cout << "3: ComputePedestals2 25" << endl;
        // ComputePedestals2(0, 25);
        /*
        bool cp2res{false};
        cp2res = ComputePedestals2(0, 100);
        if (cp2res)
        {
            cout << "ComputePedestals2(0, 100) : ok" << endl;
            cp2res = ComputePedestals2(0, 50);
            if (cp2res)
            {
                cout << "ComputePedestals2(0, 50) : ok" << endl;
                cp2res = ComputePedestals2(0, 50);
                if (cp2res)
                    cout << "ComputePedestals2(0, 25) : ok" << endl;
            }
        }
        */
        cout << "4: ComputeRawSigmas" << endl;
        ComputeRawSigmas(ExcludeEvents);
        cout << "5: ComputeSigmas" << endl;
        ComputeSigmas(ExcludeEvents, 0xFF);
        cout << "6: SaveCalibrations" << endl;
        SaveCalibrations();
        cout << "7" << endl;
    }
    ResetHistos();
    cout << "8: ComputeReduction" << endl;
    // ComputeChHi();
    if (TGCBSaveClusters->IsDown())
    {
        string inputFileName{fFilename};
        string outputFileName{inputFileName.substr(0, inputFileName.length() - 5)};
        outputFileName += "_clustered.root";
        out_clustered_file = new TFile(outputFileName.c_str(), "recreate");
        clusters_tree = new TTree("clusters_tree", "clusters_tree");
        std::array<TBranch *, NBRANCHES> temp_arr;
        std::vector<double> tmp_double_vec;
        std::vector<std::vector<double>> tmp_double_vvec;
        std::vector<int> tmp_int_vec;
        branches.assign(NLAD, temp_arr);
        for (int lad = 0; lad < (int)NLAD; lad++)
        {
            integral_map[lad] = tmp_double_vec;
            length_map[lad] = tmp_int_vec;
            cog_map[lad] = tmp_double_vec;
            sovern_map[lad] = tmp_double_vec;
            sovernPerChannel_map[lad] = tmp_double_vvec;
            eventNumber_map[lad] = 0;
            frameCounter_map[lad] = 0;
            (branches.at(lad)).at(0) = clusters_tree->Branch(("integral" + to_string(lad)).c_str(), &integral_map.at(lad));
            (branches.at(lad)).at(1) = clusters_tree->Branch(("length" + to_string(lad)).c_str(), &length_map.at(lad));
            (branches.at(lad)).at(2) = clusters_tree->Branch(("cog" + to_string(lad)).c_str(), &cog_map.at(lad));
            (branches.at(lad)).at(3) = clusters_tree->Branch(("sovern" + to_string(lad)).c_str(), &sovern_map.at(lad));
            (branches.at(lad)).at(4) = clusters_tree->Branch(("sovernPerChannel" + to_string(lad)).c_str(), &sovernPerChannel_map.at(lad));
            (branches.at(lad)).at(5) = clusters_tree->Branch(("eventNumber" + to_string(lad)).c_str(), &eventNumber_map.at(lad));
            (branches.at(lad)).at(6) = clusters_tree->Branch(("frameCounter" + to_string(lad)).c_str(), &frameCounter_map.at(lad));
        }
    }
    ComputeReduction();
    cout << "9" << endl;
    // BuildGraph(); //cc
    DisplayPedestals();
    DisplayRawSigmas();
    DisplaySigmas();
    DisplayPedCompar();
    DisplaySigmaCompar();

    DisplaySovern();
    DisplayOccupancy();
    DisplayCog();
    DisplayLens();
    DisplayLenVsCog();
    DisplayInts();
    DisplayIntVsCog();
    DisplayNclus();

    fEventCounter = 0;

    StartEventTrigger();

    // for (int ch=0; ch<fNTOTCH; ch++) {
    //   int lad=ch/NLCH;
    //   hisChHi[lad][ch]->Reset();
    // }

    SetRunButtons(0, 0, 1, 0);
}

void MyMainFrame::StopScope()
{

    SetRunButtons(0, 0, 0, 0);
    AddLogMessage("Stopping run");
    StopEventTrigger();
    if (fDaqRun)
    {
        fUnigeGpio->StopAcquisition();
        sleep(1);
        CleanupCommunication();
        CleanupBuffer();
        fQuitRun = true;
        sleep(2);
        InitCommunication();
        InitBuffer();
        fEventCounter = 0;
        fBufferPos = 0;
        fDaqRun = false;
    }
    SetRunButtons(1, 1, 0, 1);
}

void MyMainFrame::ContinueScope()
{

    AddLogMessage("Continuing run");
    StartEventTrigger();

    SetRunButtons(0, 0, 1, 0);
}

TCanvas *MyMainFrame::GetCanvas(string name, double xmin, double ymin, double xmax, double ymax)
{
    TCanvas *c = (TCanvas *)gROOT->GetListOfCanvases()->FindObject(name.c_str());
    if (c == 0)
        c = new TCanvas(name.c_str(), name.c_str(), xmin, ymin, xmax, ymax);

    c->cd();
    return c;
}

bool MyMainFrame::TestFitSuccess(bool verbose)
{ // from https://root-forum.cern.ch/t/reading-out-fit-status/19480/11
    // well, this does not seem to work, in the end...
    string minuitstatus{string(gMinuit->fCstatu)};
    if (verbose)
        cout << minuitstatus << endl;
    if (verbose)
        cout << "fempty = " << gMinuit->fEmpty << endl;
    if (minuitstatus.compare("CONVERGED ") != 0 && minuitstatus.compare("OK        ") != 0) // the spaces are important
    {
        if (verbose)
            cout << "  Minimization did not converge! (status_\"" << minuitstatus << "\")" << std::endl;
        return false;
    }
    else
        return true;
}

void MyMainFrame::BuildGraph()
{

    fReadOut.clear();
    fReadOut.assign(fNTOTCH, 0);
    fSpectrum.clear();
    fSpectrum.assign(fNTOTCH, 0);
    uint current_ch{0};
    uint current_adc{0};
    uint current_lad{0};

    double spread{0};

    for (uint lad{0}; lad < NLAD; ++lad)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            // cout << "event " << i << "  channel " << ch << "  " << strip[ch] << endl;
            current_ch = ch;
            current_adc = ch * (vBoards.at(lad))->getNADC() / (vBoards.at(lad))->getNLCH();
            current_lad = lad;

            while (current_lad > 0)
            {
                current_adc += (vBoards.at(current_lad - 1))->getNADC();
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }

            int ladch = ch;
            // int adc8=adc%(NADC*2); // total number of ADCs is 8.
            // int cycle=adc/(NADC*2); // three reading cycles.
            int adcch = ch % (vBoards.at(lad))->getNADC() / (vBoards.at(lad))->getNLCH(); // channel index among a single ADC

            // if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;

            vgradc.at(lad)->SetPoint(ladch, ladch, GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode()));

            // C cout<<"ch = "<<ch<<", fStrip[ch] = "<<fStrip[ch]<<endl;

            fReadOut.at(current_ch) = GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode()) - fPedestals.at(current_ch);
            spread += fReadOut.at(current_ch);
            vgrpedsubt.at(lad)->SetPoint(ladch, ladch, fReadOut.at(current_ch));
            // grADCpedsubt[adc8]->SetPoint(adcch+cycle*fNADCCH, adcch+cycle*fNADCCH, fReadOut.at(ch));
            // fReadOut.at(ch)=fStrip[ch]-fStrip[ch%fNADCCH+2*fNADCCH]; //trick to subtract signals of 1 ADC K of 1st ladder
            // fReadOut.at(ch)=fStrip[ch]-fStripPrevious[ch%fNADCCH+2*fNADCCH]; //trick to subtract signals of 1 ADC K of 1st ladder
            // grpedsubt[lad]->SetPoint(ladch,ladch,fReadOut.at(ch));

            // att
            //  hisChHi[lad][ch]->Fill(fReadOut.at(ch));
            //  hisChHi[lad][ch]->SetStats(kTRUE);
        }
    spread /= fNTOTCH;
    std::cout << "spread = " << spread << std::endl;
    // memcpy(&fStripPrevious,&fStrip,sizeof(fStrip));
    // ComputeCN();
    if ((!fDaqRun || fNevents > int(NPEDEVENTS)) && TGCBComputeCN->IsDown())
    {

        ComputeCN(0.8, 0xFF);
        SubtractCN();
        DoRunningCN();

        if (TGCBdodynped->IsDown())
            DynamicPedestals();
        // for (int ch=0; ch<fNTOTCH; ch++) {
        //   int adc=ch/fNADCCH;
        //   //int adc8=adc%(NADC*2); // total number of ADCs is 8.
        //   //int cycle=adc/(NADC*2); // three reading cycles.
        //   int adcch=ch%fNADCCH; // channel index among a single ADC
        //   //grADCpedcnsubt[adc8]->SetPoint(adcch+cycle*fNADCCH, adcch+cycle*fNADCCH, fReadOut.at(ch));
        //}

        Reduction(false);
        int sign{TGCBNegativeSignals->IsDown() ? -1 : 1};
        int sign1{TGCBNegativeSignals1->IsDown() ? -1 : 1};
        for (uint lad{0}; lad < NLAD; ++lad)
            for (uint ch = 0; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                int ladch = ch;
                current_ch = ch;
                current_lad = lad;

                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }

                if (TGCBNegativeSignals->IsDown() && lad > 0)
                    sign = 1;
                if (TGCBNegativeSignals->IsDown() && lad < 1)
                    sign = -1;
                if (TGCBNegativeSignals1->IsDown() && lad > 0 && lad < 2)
                    sign1 = -1;
                if (TGCBNegativeSignals1->IsDown() && lad < 1)
                    sign1 = 1;
                vgrpedcnsubt.at(lad)->SetPoint(ladch, ladch, sign * sign1 * fReadOut.at(current_ch));
                // hisFFT[lad]->SetBinContent(ladch+1, fSpectrum.at(ch));
                // cout << ch << "   " << fSpectrum.at(ch) << endl;
                // hisFFT[lad]->SetBinContent(ladch+1, 10);
            }

        // ComputeFFT(); // 20210603 Deactivated temporarily
    }
}

void MyMainFrame::UpdateAdcDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateAdcDisplay()
    }
    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            fAdcCanvas->cd(ConvertLad(lad));
            bool IsY{IsLadY(lad)};
            // fAdcCanvas->cd(1);
            // fAdcAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fAdcAxis[IsY][0], fAdcAxis[IsY][2], fAdcAxis[IsY][1], fAdcAxis[IsY][3]);
            frame->SetTitle(Form("ADC - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
            LinesVas(lad);
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        // TVirtualPad *pad=fAdcCanvas->cd(CONVERT[lad]);
        if (!fSuperImposed)
        {
            // C cout<<"Nous sommes avant DrawGraph !!"<<endl;
            DrawGraph(fAdcCanvas, ConvertLad(lad), lad, vgradc);
        }
        else
        {
            DrawGraphSuperimposed(fAdcCanvas, ConvertLad(lad), lad, event, vgradc);
        }
    }
    fAdcCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateSovernDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateLensDisplay()
    }
    fDisplayBusy = 1;

    bool profile_x{TGCBshowProfileSovern->IsDown()};

    if (refreshaxes && profile_x)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            bool IsY{IsLadY(lad)};
            fSovernCanvas->cd(ConvertLad(lad));
            // fSovernAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fSovernAxis[IsY][0], fSovernAxis[IsY][2], fSovernAxis[IsY][1], fSovernAxis[IsY][3]);
            frame->SetTitle(Form("Signal over noise vs channel - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
            // LinesVas();
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        bool IsY{IsLadY(lad)};
        fSovernCanvas->cd(ConvertLad(lad));
        TH2F *his2 = (TGCBshowSovernCut->IsDown() ? vh2SovernCut.at(lad) : vh2Sovern.at(lad));
        gStyle->SetOptStat(1111111);
        his2->SetStats(kTRUE);
        // if (refreshaxes) {
        // fSovernAxis[1] = (vBoards.at(lad))->getNLCH();
        his2->GetXaxis()->SetRangeUser(fSovernAxis[IsY][0], fSovernAxis[IsY][1]);
        his2->GetYaxis()->SetRangeUser(fSovernAxis[IsY][2], fSovernAxis[IsY][3]);
        //}
        if (!profile_x)
        {
            gPad->GetListOfPrimitives()->Remove(his2);
            his2->Draw("colz same");
        }
        else
        {
            gPad->GetListOfPrimitives()->Remove(his2->ProfileX());
            his2->ProfileX()->Draw("same");
        }
        gPad->Update();
        // if (refreshaxes)
        LinesVas(lad);
    }
    fSovernCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateOccupancyDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateOccupancyDisplay()
    }
    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            bool IsY{IsLadY(lad)};
            fOccupancyCanvas->cd(ConvertLad(lad));
            // fOccupancyAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fOccupancyAxis[IsY][0], fOccupancyAxis[IsY][2], fOccupancyAxis[IsY][1], fOccupancyAxis[IsY][3]);
            frame->SetTitle(Form("Occupancy - Detector %d; channel;", lad));
            LinesVas(lad);
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fOccupancyCanvas->cd(ConvertLad(lad));
        gPad->GetListOfPrimitives()->Remove(vhisOccupancy[lad]);
        vhisOccupancy[lad]->Draw("same");
        gPad->Update();
    }
    fOccupancyCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateCogDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateCogDisplay()
    }
    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            bool IsY{IsLadY(lad)};
            fCogCanvas->cd(ConvertLad(lad));
            // fCogAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fCogAxis[IsY][0], fCogAxis[IsY][2], fCogAxis[IsY][1], fCogAxis[IsY][3]);
            frame->SetTitle(Form("Center of gravity - Detector %d; channel;", lad));
            LinesVas(lad);
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fCogCanvas->cd(ConvertLad(lad));
        gPad->GetListOfPrimitives()->Remove(vhisCog[lad]);
        vhisCog[lad]->Draw("same");
        gPad->Update();
    }
    fCogCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateLensDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateLensDisplay()
    }
    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            fLensCanvas->cd(ConvertLad(lad));
            bool IsY{IsLadY(lad)};
            TH1F *frame = gPad->DrawFrame(fLensAxis[IsY][0], fLensAxis[IsY][2], fLensAxis[IsY][1], fLensAxis[IsY][3]);
            frame->SetTitle(Form("Cluster length - Detector %d; channel;", lad));
            // LinesVas();
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fLensCanvas->cd(ConvertLad(lad));
        gPad->GetListOfPrimitives()->Remove(vhisLens[lad]);
        vhisLens[lad]->Draw("same");
        gPad->Update();
    }
    fLensCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateLenVsCogDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateLensDisplay()
    }
    fDisplayBusy = 1;

    bool profile_x{TGCBshowProfileLenVsCog->IsDown()};

    if (refreshaxes && profile_x)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            fLenVsCogCanvas->cd(ConvertLad(lad));
            bool IsY{IsLadY(lad)};
            // fLenVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fLenVsCogAxis[IsY][0], fLenVsCogAxis[IsY][2], fLenVsCogAxis[IsY][1], fLenVsCogAxis[IsY][3]);
            frame->SetTitle(Form("Len vs cog - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
            // LinesVas();
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fLenVsCogCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        TH2F *his2 = (TGCBshowLenVsCogCut->IsDown() ? vh2LenVsCogCut.at(lad) : vh2LenVsCog.at(lad));
        gStyle->SetOptStat(1111111);
        his2->SetStats(kTRUE);
        // if (refreshaxes) {
        // fLenVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
        his2->GetXaxis()->SetRangeUser(fLenVsCogAxis[IsY][0], fLenVsCogAxis[IsY][1]);
        his2->GetYaxis()->SetRangeUser(fLenVsCogAxis[IsY][2], fLenVsCogAxis[IsY][3]);
        //}
        if (!profile_x)
        {
            gPad->GetListOfPrimitives()->Remove(his2);
            his2->Draw("colz same");
        }
        else
        {
            gPad->GetListOfPrimitives()->Remove(his2->ProfileX());
            his2->ProfileX()->Draw("same");
        }
        gPad->Update();
        // if (refreshaxes)
        LinesVas(lad);
    }
    fLenVsCogCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateIntVsCogDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateLensDisplay()
    }
    fDisplayBusy = 1;

    bool profile_x{TGCBshowProfileIntVsCog->IsDown()};

    if (refreshaxes && profile_x)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            fIntVsCogCanvas->cd(ConvertLad(lad));
            bool IsY{IsLadY(lad)};
            // fIntVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fIntVsCogAxis[IsY][0], fIntVsCogAxis[IsY][2], fIntVsCogAxis[IsY][1], fIntVsCogAxis[IsY][3]);
            frame->SetTitle(Form("Int vs cog - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
            // LinesVas();
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fIntVsCogCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        TH2F *his2 = (TGCBshowIntVsCogCut->IsDown() ? vh2IntVsCogCut.at(lad) : vh2IntVsCog.at(lad));
        gStyle->SetOptStat(1111111);
        his2->SetStats(kTRUE);
        // if (refreshaxes) {
        // fIntVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
        his2->GetXaxis()->SetRangeUser(fIntVsCogAxis[IsY][0], fIntVsCogAxis[IsY][1]);
        his2->GetYaxis()->SetRangeUser(fIntVsCogAxis[IsY][2], fIntVsCogAxis[IsY][3]);
        //}
        if (!profile_x)
        {
            gPad->GetListOfPrimitives()->Remove(his2);
            his2->Draw("colz same");
        }
        else
        {
            gPad->GetListOfPrimitives()->Remove(his2->ProfileX());
            his2->ProfileX()->Draw("same");
        }
        gPad->Update();
        // if (refreshaxes)
        LinesVas(lad);
    }
    fIntVsCogCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateIntsDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateIntsDisplay()
    }
    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            fIntsCanvas->cd(ConvertLad(lad));
            TH1F *frame = gPad->DrawFrame(fIntsAxis[IsLadY(lad)][0], fIntsAxis[IsLadY(lad)][2], fIntsAxis[IsLadY(lad)][1], fIntsAxis[IsLadY(lad)][3]);
            frame->SetTitle(Form("Integral - Detector %d; cluster integral (ADCc);", lad));
            // LinesVas();
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fIntsCanvas->cd(ConvertLad(lad));
        gPad->GetListOfPrimitives()->Remove(vhisIntS.at(lad));
        vhisIntS.at(lad)->Draw("same");
        gPad->Update();
    }
    fIntsCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateFftDisplay(int refreshaxes)
{

    if (fDisplayBusy)
    {
        cout << "busy" << endl;
        return; // in case of multiple calls of UpdateFftDisplay()
    }
    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            fFftCanvas->cd(ConvertLad(lad));
            bool IsY{IsLadY(lad)};
            // fFftAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fFftAxis[IsY][0], fFftAxis[IsY][2], fFftAxis[IsY][1], fFftAxis[IsY][3]);
            frame->SetTitle(Form("FFT - Detector %d; frequency;", lad));
            // LinesVas();
        }
    }

    int adc{0};

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        gPad->GetListOfPrimitives()->Remove((vhisFFT.at(lad)).at(adc));
        gPad->GetListOfPrimitives()->Remove((vhisFFTnorm.at(lad)).at(adc));
        gPad->GetListOfPrimitives()->Remove((vhisSumFFT.at(lad)).at(adc));
        gPad->GetListOfPrimitives()->Remove((vhisSumFFTnorm.at(lad)).at(adc));
        TH1F *his = (TGCBShowFFTNorm->IsDown()) ? ((TGCBShowFFTSum->IsDown()) ? (vhisSumFFTnorm.at(lad)).at(adc) : (vhisFFTnorm.at(lad)).at(adc))
                                                : ((TGCBShowFFTSum->IsDown()) ? (vhisSumFFT.at(lad)).at(adc) : (vhisFFT.at(lad)).at(adc));
        // hisFFT[lad]->Draw("same");
        his->Draw("same HIST");
        gPad->Update();
    }
    fFftCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::DrawClusterGraph(TCanvas *c, int ipad, int lad)
{

    TGraph *gr = vgrclusters.at(lad);
    TVirtualPad *pad = c->cd(ipad);
    pad->GetListOfPrimitives()->Remove(gr); // first we remove the graph from the object list of the canvas, so that we avoid annoying messages from root if the graph is empty
    ResetGraph(gr);
    BuildGrCluster(Scluster, gr);
    BuildGrCluster(Kcluster, gr);
    if (gr->GetN())
        gr->Draw("p");
    pad->Update();
}

void MyMainFrame::DrawGraph(TCanvas *c, int ipad, int lad, vTGraphp vgr, string option)
{

    TVirtualPad *pad = c->cd(ipad);
    TGraph *agr{vgr.at(lad)};
    pad->GetListOfPrimitives()->Remove(agr);
    if (agr->GetN())
        agr->Draw(option.c_str());
    pad->Update();
}

void MyMainFrame::DrawGraphSuperimposed(TCanvas *c, int ipad, int lad, int event, vTGraphp vgr)
{

    TVirtualPad *pad = c->cd(ipad);
    TGraph *agr{vgr.at(lad)};
    TGraph *copy = (TGraph *)agr->Clone();
    copy->SetName(Form("copy_%d_%d", event, lad));
    copy->Draw("l");
    int back = event - fSuperImposed;
    if (back > -1)
    {
        RemoveGraph(Form("copy_%d_%d", back, lad));
        // TGraph *gr=(TGraph*)pad->FindObject(Form("copy_%d_%d",back,lad));
        // if (gr) delete gr;
    }
    pad->Update();
}

void MyMainFrame::ClearSuperimposedGraphs(TCanvas *c, int event, int back)
{

    if (event < 0 || back < 0)
        return;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {

        if (NLAD > 1)
            TVirtualPad *pad = c->cd(ConvertLad(lad));

        // for (int i=event-back; i<event; i++) {
        for (int i = 0; i < event; i++)
        {
            // cout << "i=" << i << "event=" << event << " back=" << back <<endl;
            RemoveGraph(Form("copy_%d_%d", i, lad));
            // TGraph *gr=(TGraph*)pad->FindObject(Form("copy_%d_%d",i,lad));
            // if (gr) delete gr;
        }
    }
}

void MyMainFrame::UpdatePedSubtDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
        return; // in case of multiple calls of UpdateAdcDisplay()

    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            bool IsY{IsLadY(lad)};
            fPedSubtCanvas->cd(ConvertLad(lad));
            // fPedSubtAxis[IsY][1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fPedSubtAxis[IsY][0], fPedSubtAxis[IsY][2], fPedSubtAxis[IsY][1], fPedSubtAxis[IsY][3]);
            frame->SetTitle(Form("Ped. subt. - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
            LinesVas(lad);
        }
    }
    //  TCanvas *c2=fPedSubtCanvas;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {

        if (!fSuperImposed)
            DrawGraph(fPedSubtCanvas, ConvertLad(lad), lad, vgrpedsubt);
        else
            DrawGraphSuperimposed(fPedSubtCanvas, ConvertLad(lad), lad, event, vgrpedsubt);
    }
    fPedSubtCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdateAdcPedCnSubtDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
        return; // in case of multiple calls of UpdateAdcDisplay()

    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (uint adc = 0; adc < 2 * fNADC; adc++)
        {
            fAdcPedCnSubtCanvas->cd(adc + 1);
            TH1F *frame = gPad->DrawFrame(fAdcPedCnSubtAxis[0], fAdcPedCnSubtAxis[2], fAdcPedCnSubtAxis[1], fAdcPedCnSubtAxis[3]);
            frame->SetTitle(Form("Adc signal - ADC %d; readings;", adc));
            LinesAdcs();
        }
    }
    //  TCanvas *c2=fPedCnSubtCanvas;

    for (uint adc = 0; adc < 2 * fNADC; adc++)
    {

        if (!fSuperImposed)
            DrawGraph(fAdcPedCnSubtCanvas, adc + 1, adc, vgradcpedcnsubt);
        else
            DrawGraphSuperimposed(fAdcPedCnSubtCanvas, adc + 1, adc, event, vgradcpedcnsubt);
    }
    fAdcPedCnSubtCanvas->Update();

    fDisplayBusy = 0;
}

void MyMainFrame::UpdatePedCnSubtDisplay(int event, int refreshaxes)
{

    if (fDisplayBusy)
        return; // in case of multiple calls of UpdateAdcDisplay()

    fDisplayBusy = 1;

    if (refreshaxes)
    {
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            fPedCnSubtCanvas->cd(ConvertLad(lad));
            bool IsY{IsLadY(lad)};
            // fPedCnSubtAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fPedCnSubtAxis[IsY][0], fPedCnSubtAxis[IsY][2], fPedCnSubtAxis[IsY][1], fPedCnSubtAxis[IsY][3]);
            frame->SetTitle(Form("Signals - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
            LinesVas(lad);
        }
    }
    //  TCanvas *c2=fPedSubtCanvas;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        DoCluster(lad, 'S', &Scluster);
        ListCluster(Scluster, Form("Event %d, Detector %d, S-side: ", event, lad));
        ResetGraph(vgrclusters.at(lad));
        BuildGrCluster(Scluster, vgrclusters.at(lad));
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {

        if (!fSuperImposed)
            DrawGraph(fPedCnSubtCanvas, ConvertLad(lad), lad, vgrpedcnsubt);
        else
            DrawGraphSuperimposed(fPedCnSubtCanvas, ConvertLad(lad), lad, event, vgrpedcnsubt);

        // DrawHis(fPedCnSubtCanvas,CONVERT[lad],hisClusters[lad]);

        if (vgrclusters.at(lad)->GetN())
            DrawGraph(fPedCnSubtCanvas, ConvertLad(lad), lad, vgrclusters, "p");
        // DrawClusterGraph(fPedCnSubtCanvas,CONVERT[lad],lad);
    }

    fPedCnSubtCanvas->Update();

    fDisplayBusy = 0;
}

bool MyMainFrame::ComputeOnlinePedestals(bool start)
{

    bool running{true};
    if (start)
    {
        fPedCnt = 0;
        vdouble().swap(fPedestals); // clear and reallocate memory see cplusplus.com
        fPedestals.assign(fNTOTCH, 0);
        vint().swap(fStatus);
        fStatus.assign(fNTOTCH, 0);
    }
    fPedCnt++;

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; ++lad)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            fPedestals.at(current_ch) += GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode());
        }

    if (fPedCnt == NPEDEVENTS)
    {
        for (uint ch{0}; ch < fNTOTCH; ch++)
        {
            fPedestals.at(ch) /= fPedCnt;
            if (fPedestals.at(ch) < fPedestalsLowCut)
                // fStatus.at(ch) |= 1;
                SetChannelStatus(ch, 1);
            if (fPedestals.at(ch) > fPedestalsHighCut)
                // fStatus.at(ch) |= 2;
                SetChannelStatus(ch, 2);
        }
        running = false;
        AddLogMessage(Form("Online Pedestals computed with %d events.", fPedCnt));
    }

    return running;
}

bool MyMainFrame::ComputeOnlineRawSigma(bool start)
{

    bool running{true};
    if (start)
    {
        fSrawCnt = 0;
        vdouble().swap(fRawSigmas); // clear and reallocate memory see cplusplus.com
        fRawSigmas.assign(fNTOTCH, 0);
        // vint().swap(fStatus);
        // fStatus.assign(fNTOTCH,0);
    }
    fSrawCnt++;

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; ++lad)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            // int adc=ch/fNADCCH;
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            double val = GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode()) - fPedestals.at(current_ch);
            // h2ADCval->Fill(ch, val);
            val *= val;
            fRawSigmas.at(current_ch) += val;
            // fPedestals.at(ch)+=GetStripValue(ch, fChanLabelMode);
        }

    if (fSrawCnt == NSRAWEVENTS)
    {
        for (uint ch{0}; ch < fNTOTCH; ch++)
        {
            fRawSigmas.at(ch) /= fSrawCnt;
            if (fRawSigmas.at(ch) < fRawNoiseLowCut)
                // fStatus.at(ch) |= 4;
                SetChannelStatus(ch, 4);
            if (fRawSigmas.at(ch) > fRawNoiseHighCut)
            {
                // fStatus.at(ch) |= 8;
                cout << "channel " << ch << " sraw = " << fRawSigmas.at(ch) << endl;
                SetChannelStatus(ch, 8);
            }
        }
        running = false;
        AddLogMessage(Form("Online raw sigams computed with %d events.", fSrawCnt));
    }

    return running;
}

bool MyMainFrame::ComputeOnlineSigma(bool start)
{
    int cn6_cnt{0};
    int cn4_cnt{0};
    int cn2_cnt{0};
    int cn1_cnt{0};
    int kocn_cnt{0};

    bool running{true};

    if (start)
    {
        fSigmaCnt = 0;
        vdouble().swap(fSigmas); // clear and reallocate memory see cplusplus.com
        fSigmas.assign(fNTOTCH, 0);
        // vint().swap(fStatus);
        // fStatus.assign(fNTOTCH,0);
    }
    fSigmaCnt++;

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; ++lad)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            // int adc=ch/fNADCCH;
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            fReadOut.at(current_ch) = GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode()) - fPedestals.at(current_ch);
        }

    ComputeCN(0.8, 0xFF);
    SubtractCN();
    DoRunningCN();

    for (uint ch{0}; ch < fNTOTCH; ch++)
    {
        double val = fReadOut.at(ch);
        val *= val;
        fSigmas.at(ch) += val;
    }
    if (fSigmaCnt == NSIGEVENTS)
    {
        for (uint ch{0}; ch < fNTOTCH; ch++)
        {
            fSigmas.at(ch) /= fSigmaCnt;
            fSigmas.at(ch) = sqrt(fSigmas.at(ch));
            if (fSigmas.at(ch) < fNoiseLowCut)
                // fStatus.at(ch) |= 16;
                SetChannelStatus(ch, 16);
            if (fSigmas.at(ch) > fNoiseHighCut)
                // fStatus.at(ch) |= 32;
                SetChannelStatus(ch, 32);
        }
        running = false;
        AddLogMessage(Form("Online sigmas computed with %d events.", fSrawCnt));

        // and now we save and update all the informations
    }

    return running;
}

bool MyMainFrame::ComputeOnlineCalibration(bool start)
{

    bool running{true};
    if (start)
    {
        fCalCnt = 0;
        fPedCnt = 0;
        fSkipCnt = 0;
        vdouble().swap(fPedestals); // clear and reallocate memory see cplusplus.com
        fPedestals.assign(fNTOTCH, 0);
        vint().swap(fStatus);
        fStatus.assign(fNTOTCH, 0);
        fSrawCnt = 0;
        vdouble().swap(fRawSigmas); // clear and reallocate memory see cplusplus.com
        fRawSigmas.assign(fNTOTCH, 0);
        fSigmaCnt = 0;
        vdouble().swap(fSigmas); // clear and reallocate memory see cplusplus.com
        fSigmas.assign(fNTOTCH, 0);
        vdouble().swap(fReadOut); // clear and reallocate memory see cplusplus.com
        fReadOut.assign(fNTOTCH, 0);
        fTGHPBprogress->Reset();
    }

    if (fSkipCnt < ExcludeEvents)
    { // very often the first events are not nice, so they distrot the cal data
        fSkipCnt++;
        return running;
    }

    fCalCnt++;

    double Progress = (fCalCnt * 100) / (NPEDEVENTS + NSRAWEVENTS + NSIGEVENTS);
    fTGHPBprogress->SetPosition(Progress);
    fTGHPBprogress->ShowPosition();
    gSystem->ProcessEvents();

    // DisplayPedestals();
    // cout << "before: fPedCnt=" << fPedCnt << endl;
    // cout << "before: fSrawCnt=" << fSrawCnt << endl;
    // cout << "before: fSigmaCnt=" << fSigmaCnt << endl;
    if (fPedCnt < NPEDEVENTS)
        ComputeOnlinePedestals(false);
    else if (fSrawCnt < NSRAWEVENTS)
        ComputeOnlineRawSigma(false);
    else if (fSigmaCnt < NSIGEVENTS)
        ComputeOnlineSigma(false);
    else
    {
        SaveCalibrations();
        // cout << "Before DisplayPedestals" << endl;
        DisplayPedestals();
        // cout << "After DisplayPedestals" << endl;
        DisplayRawSigmas();
        DisplaySigmas();
        DisplayPedCompar();
        DisplaySigmaCompar();
        running = false;
    }
    // cout << "after: fPedCnt=" << fPedCnt << endl;
    // cout << "after: fSrawCnt=" << fSrawCnt << endl;
    // cout << "after: fSigmaCnt=" << fSigmaCnt << endl;
    return running;
}

void MyMainFrame::ComputePedestals(int startevent)
{

    fTGHPBprogress->Reset();

    vdouble().swap(fPedestals); // clear and reallocate memory see cplusplus.com
    fPedestals.assign(fNTOTCH, 0);
    vint().swap(fStatus);
    fStatus.assign(fNTOTCH, 0);

    unsigned int PedCnt = 0;

    uint current_ch{0};
    uint current_lad{0};

    for (int event = startevent; event < fNevents; event++)
    {
        int selmode{TGCBEventSel->GetSelected()};
        if (selmode == 1 and (event % 2) == 1)
            continue; // we exclude odd indices
        else if (selmode == 2 and (event % 2) == 0)
            continue; // we exclude even indices

        fTree->GetEntry(event);
        fEventCounter = event;
        PedCnt++;

        if (PedCnt % 100 == 0)
        {
            fTGHPBprogress->SetPosition((PedCnt*100.)/NPEDEVENTS); // in linux, this is enough to update the display of the progress bar
            cout << "Progress: " << round((PedCnt * 100.) / NPEDEVENTS) << " %" << endl;
            fTGHPBprogress->ShowPosition(); // in mac, you need to add this
            gSystem->ProcessEvents(); // and this.
        }

        for (uint lad{0}; lad < NLAD; ++lad)
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                // if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                fPedestals.at(current_ch) += GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode());
            }
        if (PedCnt == NPEDEVENTS)
            break;
    }

    for (uint ch{0}; ch < fNTOTCH; ch++)
    {
        fPedestals.at(ch) /= PedCnt;
        if (fPedestals.at(ch) < fPedestalsLowCut)
            // fStatus.at(ch) |= 1;
            SetChannelStatus(ch, 1);
        if (fPedestals.at(ch) > fPedestalsHighCut)
            // fStatus.at(ch) |= 2;
            SetChannelStatus(ch, 2);
        // AddLogMessage(Form("Pedestal = %5.1lf for channel %d, status = %d", fPedestals.at(ch), ch, fStatus.at(ch)));
    }

    fTGHPBprogress->SetPosition(100);
    fTGHPBprogress->ShowPosition();
    gSystem->ProcessEvents();

    AddLogMessage(Form("Pedestals computed with %d events.", PedCnt));
}

bool MyMainFrame::ComputePedestals2(int startevent, double pedcut)
{

    bool result{false};
    fTGHPBprogress->Reset();

    vdouble fPedestals2;
    // fPedestals2.clear();
    fPedestals2.assign(fNTOTCH, 0);
    unsigned int PedCnt{0};
    int LastEv{0};

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; ++lad)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }

            if ((current_ch) % 40 == 0)
            {
                fTGHPBprogress->SetPosition((ch*100.)/fNTOTCH);
                cout << "Progress: " << round(((current_ch)*100.) / fNTOTCH) << " %" << endl;
                fTGHPBprogress->ShowPosition();
                gSystem->ProcessEvents();
            }

            PedCnt = 0;
            for (int event{startevent}; event < fNevents; event++)
            {
                fTree->GetEntry(event);
                fEventCounter = event;
                if (fabs(GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode()) - fPedestals.at(current_ch)) > pedcut)
                    continue;
                fPedestals2.at(current_ch) += GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode());
                PedCnt++;
                if (PedCnt == NPEDEVENTS)
                {
                    LastEv = event;
                    break;
                }
            }

            if (PedCnt != NPEDEVENTS)
            {
                fPedestals2.at(current_ch) = fPedestals.at(current_ch); // we did not succeed to find 2048 events in the whole DAQ file to compute new pedestals, we keep the old ones.
                result = false;
            }
            else
            { // for (int ch=0; ch<fNTOTCH; ch++)
                fPedestals2.at(current_ch) /= PedCnt;
                if (fPedestals2.at(current_ch) < fPedestalsLowCut)
                    // fStatus.at(ch) |= 1;
                    SetChannelStatus(current_ch, 1);
                if (fPedestals2.at(current_ch) > fPedestalsHighCut)
                    // fStatus.at(ch) |= 2;
                    SetChannelStatus(current_ch, 2);
                result = true;
            }
            // AddLogMessage(Form("Optima pedestal (cut=%f) = %5.1lf for channel %d computed with %d events. Last event = %d. Status = %d", pedcut, fPedestals2.at(ch), ch, PedCnt, LastEv, fStatus.at(ch)));
            fPedestals.at(current_ch) = fPedestals2.at(current_ch);
        }

    // fPedestals2.clear();
    //  as suggested in cplusplus.com, the way to clear and reallocate free memory is to do as follows:
    vdouble().swap(fPedestals2); // right way to clear vector and reallocate memory

    fTGHPBprogress->SetPosition(100);
    fTGHPBprogress->ShowPosition();
    gSystem->ProcessEvents();

    return result;
}

void MyMainFrame::ComputeRawSigmas(int startevent)
{

    fTGHPBprogress->Reset();

    // TH2F *h2ADCval{new TH2F("h2ADCval", "pedsubt distribution", 2048, -0.5, 2048 - 0.5, 60, -30, 30)};

    unsigned int sRawCnt = 0;
    vdouble().swap(fRawSigmas);
    fRawSigmas.assign(fNTOTCH, 0);

    uint current_ch{0};
    uint current_lad{0};

    for (int event = startevent; event < fNevents; event++)
    {
        int selmode{TGCBEventSel->GetSelected()};
        if (selmode == 1 and (event % 2) == 1)
            continue; // we exclude odd indices
        else if (selmode == 2 and (event % 2) == 0)
            continue; // we exclude even indices

        fTree->GetEntry(event);
        fEventCounter = event;
        sRawCnt++;

        if (sRawCnt % 100 == 0)
        {
            fTGHPBprogress->SetPosition((sRawCnt*100.)/NSRAWEVENTS); // in linux, this is enough to update the display of the progress bar
            cout << "Progress: " << round((sRawCnt * 100.) / NSRAWEVENTS) << " %" << endl;
            fTGHPBprogress->ShowPosition(); // in mac, you need to add this
            gSystem->ProcessEvents(); // and this.
        }

        for (uint lad{0}; lad < NLAD; ++lad)
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                // if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;
                double val = GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode()) - fPedestals.at(current_ch);
                // h2ADCval->Fill(ch, val);
                val *= val;
                fRawSigmas.at(current_ch) += val;
            }
        if (sRawCnt == NSRAWEVENTS)
            break;
    }

    // if (sRawCnt<2048) cout << "look out, only " << sRawCnt << " events to compute raw sigma" << endl;

    for (uint ch{0}; ch < fNTOTCH; ch++)
    {
        uint lad{GetLadIndex(ch)};
        bool IsY{IsLadY(lad)};
        // cout << "ch = " << ch << "  lad = " << lad << "  IsY = " << IsY << endl;
        fRawSigmas.at(ch) /= sRawCnt;
        fRawSigmas.at(ch) = sqrt(fRawSigmas.at(ch));
        if (fRawSigmas.at(ch) < fRawNoiseLowCut)
            // fStatus.at(ch) |= 4;
            SetChannelStatus(ch, 4);
        if (fRawSigmas.at(ch) > ((IsY) ? fRawNoiseHighCutY : fRawNoiseHighCut))
        {
            // fStatus.at(ch) |= 8;
            cout << "channel " << ch << " sraw = " << fRawSigmas.at(ch) << endl;
            SetChannelStatus(ch, 8);
        }
    }

    fTGHPBprogress->SetPosition(100);
    fTGHPBprogress->ShowPosition();
    gSystem->ProcessEvents();

    // TCanvas *c{new TCanvas("c", "c", 800, 600)};
    // h2ADCval->Draw("colz");
    // c->Update();

    AddLogMessage(Form("Raw sigmas computed with %d events.", sRawCnt));
}

void MyMainFrame::ComputeSigmas(int startevent, int maskCN)
{

    fTGHPBprogress->Reset();

    vdouble().swap(fSigmas);
    fSigmas.assign(fNTOTCH, 0);
    uint SigmaCnt = 0;
    vdouble().swap(fReadOut);
    fReadOut.assign(fNTOTCH, 0);

    uint current_ch{0};
    uint current_lad{0};

    for (int event = startevent; event < fNevents; event++)
    {
        int selmode{TGCBEventSel->GetSelected()};
        if (selmode == 1 and (event % 2) == 1)
            continue; // we exclude odd indices
        else if (selmode == 2 and (event % 2) == 0)
            continue; // we exclude even indices

        fTree->GetEntry(event);
        fEventCounter = event;
        SigmaCnt++;

        if (SigmaCnt % 100 == 0)
        {
            fTGHPBprogress->SetPosition((SigmaCnt*100.)/NSIGEVENTS); // in linux, this is enough to update the display of the progress bar
            std::cout << "Progress: " << round((SigmaCnt * 100.) / NSIGEVENTS) << " %" << std::endl;
            fTGHPBprogress->ShowPosition(); // in mac, you need to add this
            gSystem->ProcessEvents(); // and this.
        }

        for (uint lad{0}; lad < NLAD; ++lad)
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                // if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;
                fReadOut.at(current_ch) = GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode()) - fPedestals.at(current_ch);
            }
        // if (TGCBComputeCN->IsDown()) {

        ComputeCN(0.8, maskCN);
        SubtractCN();
        DoRunningCN();

        //}
        for (uint ch{0}; ch < fNTOTCH; ch++)
        {
            double val = fReadOut.at(ch);
            val *= val;
            fSigmas.at(ch) += val;
        }
        // cout << "SigmaCnt = " << SigmaCnt << endl;
        if (SigmaCnt == NSIGEVENTS)
            break;
        // for (int i=0; i<fNTOTCH; i++) fStripPrevious[i]=fStrip[i];
        // memcpy(&fStripPrevious,&fStrip,sizeof(fStrip));
    }

    // if (sRawCnt<2048) cout << "look out, only " << sRawCnt << " events to compute raw sigma" << endl;

    for (uint ch{0}; ch < fNTOTCH; ch++)
    {
        uint lad{GetLadIndex(ch)};
        bool IsY{IsLadY(lad)};
        fSigmas.at(ch) /= SigmaCnt;
        fSigmas.at(ch) = sqrt(fSigmas.at(ch));
        if (fSigmas.at(ch) < fNoiseLowCut)
            // fStatus.at(ch) |= 16;
            SetChannelStatus(ch, 16);
        if (fSigmas.at(ch) > ((IsY) ? fNoiseHighCutY : fNoiseHighCut))
            // fStatus.at(ch) |= 32;
            SetChannelStatus(ch, 32);
    }

    fTGHPBprogress->SetPosition(100);
    fTGHPBprogress->ShowPosition();
    gSystem->ProcessEvents();

    AddLogMessage(Form("Sigmas computed with %d events.", SigmaCnt));
}

// void MyMainFrame::StartFFT() {

//   // for (int lad=0; lad<NLAD; lad++)
//   //   for (int adc=0; adc<NADC; adc++) {
//   //     fSpectrum[lad][adc].clear();
//   //     fSpectrum[lad][adc].assign(fNADCCH,0);
//   //   }

//   fReadOut.clear();
//   fReadOut.assign(fNTOTCH,0);

//   for (int event=0; event<fNevents; event++) {
//     fTree->GetEntry(event);
//     //SigmaCnt++;
//     for (int ch=0; ch<fNTOTCH; ch++) {
//       int adc=ch/fNADCCH;
//       if (ADCCORRECT) if (adc==5 || adc==5+8 || adc==5+16) if (fStrip[ch]<3120) fStrip[ch]+=128;
//       fReadOut.at(ch)=fStrip[ch]-fPedestals.at(ch);
//     }
//     ComputeCN();
//     SubtractCN();

//     ComputeFFT();

//   }

//   AddLogMessage("FFT distributions computed.");

// }

void MyMainFrame::ComputeFFT()
{
    // cout << "welcome to computefft" << endl;
    //   double adcSA[fNADCCH], adcSB[fNADCCH], adcKA[fNADCCH], adcKB[fNADCCH];
    //   double fSpectrumSA[fNADCCH], fSpectrumSB[fNADCCH], fSpectrumKA[fNADCCH], fSpectrumKB[fNADCCH];

    vdouble vAdcval(fNADCCH);
    vdouble vSpectrum(fNADCCH);

    for (unsigned int lad{0}; lad < NLAD; lad++)
        for (uint adc{0}; adc < fNADC; adc++)
        {
            for (uint ch{0}; ch < fNADCCH; ch++)
            {
                vAdcval.at(ch) = fReadOut.at(ch + adc * fNADCCH + lad * fNLCH);
            }
            // memset(&fSpectrum, 0, sizeof(fSpectrum));
            DoFFT(vAdcval.data(), fNADCCH, vSpectrum.data());
            for (uint ch{0}; ch < fNADCCH; ch++)
            {
                // cout << Form("%3d: %4.2lf  %4.2lf  %4.2lf  %4.2lf\n", ch, fSpectrumSA[ch], fSpectrumSB[ch], fSpectrumKA[ch], fSpectrumKB[ch]);

                (vhisFFT.at(lad)).at(adc)->SetBinContent(ch + 1, vSpectrum.at(ch));
                (vhisSumFFT.at(lad)).at(adc)->SetBinContent(ch + 1, (vhisSumFFT.at(lad)).at(adc)->GetBinContent(ch + 1) + vSpectrum.at(ch));

                (vhisFFTnorm.at(lad)).at(adc)->SetBinContent(ch + 1, vSpectrum.at(ch));
                (vhisSumFFTnorm.at(lad)).at(adc)->SetBinContent(ch + 1, (vhisSumFFT.at(lad)).at(adc)->GetBinContent(ch + 1));
            }
            // cout << hisFFTnorm[0]->GetSumOfWeights() << "  " << hisSumFFTnorm[0]->GetSumOfWeights() << endl;
            (vhisFFTnorm.at(lad)).at(adc)->Scale(1 / (vhisFFTnorm.at(lad)).at(adc)->GetSumOfWeights());
            (vhisSumFFTnorm.at(lad)).at(adc)->Scale(1 / (vhisSumFFTnorm.at(lad)).at(adc)->GetSumOfWeights());
            // cout << hisFFTnorm[0]->GetSumOfWeights() << "  " << hisSumFFTnorm[0]->GetSumOfWeights() << endl;
        }
}

void MyMainFrame::DoFFT(double *array, int nech, double *his)
{
    /*
    fftw_complex *out;

    // for (int i=0; i<nech; i++)  printf("i=%3d  array=%lf\n",i,array[i]);

    // printf("this is %s\n", his->GetName());

    fftw_plan fftp;
    out = new fftw_complex[nech / 2 + 1];
    fftp = fftw_plan_dft_r2c_1d(nech, array, out, FFTW_ESTIMATE);
    fftw_execute(fftp);

    float val;
    for (int i = 0; i < (nech / 2 + 1); i++)
    {
        val = (out[i][0] * out[i][0] + out[i][1] * out[i][1]) / nech;
        his[1 + i] += val; // +1 because we then fill a histogram object
                           // his->SetBinContent(i+1,his->GetBinContent(i+1)+val);
    }

    fftw_destroy_plan(fftp);
    delete[] out;
     */
    return;
}

void MyMainFrame::RemoveGraph(string name)
{

    TGraph *gr = (TGraph *)gPad->FindObject(name.c_str());
    if (gr)
        delete gr;
}

void MyMainFrame::DisplayPedestals()
{

    TCanvas *c{fPedCanvas};

    if (c == 0)
    {
        cout << " canvas not found" << endl;
        return;
    }
    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        cout << "remove: lad=" << lad << endl;
        c->cd(lad + 1);
        // RemoveGraph("Peds");
        // RemoveGraph("RefPeds");
        cout << " after removal lad=" << lad << endl;
    }
    c->Update();

    TGraph *ped[NLAD], *refped[NLAD];
    for (uint i{0}; i < NLAD; i++)
    {
        cout << " ped creation: i=" << i << endl;
        ped[i] = new TGraph();
        ped[i]->SetMarkerStyle(7);
        ped[i]->SetMarkerColor(kRed);
        ped[i]->SetLineColor(kRed + 2);
        ped[i]->SetName("Peds");
    }

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; lad++)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            ped[lad]->SetPoint(ch, ch, fPedestals.at(current_ch));
        }

    for (uint lad{0}; lad < NLAD; lad++)
    {
        bool IsY{IsLadY(lad)};
        // cout << " drawing: lad = " << lad << endl;
        c->cd(ConvertLad(lad));
        // fPedAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame{gPad->DrawFrame(fPedAxis[IsY][0], fPedAxis[IsY][2], fPedAxis[IsY][1], fPedAxis[IsY][3])};
        frame->SetTitle(Form("Pedestals - Detector %d; %s", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        ped[lad]->Draw("l");
        LinesVas(lad);
    }

    if (TGCBshowrefped->IsDown()) // PA I deactivate this for the moment
    {
        for (uint i{0}; i < NLAD; i++)
        {
            refped[i] = new TGraph();
            refped[i]->SetMarkerStyle(7);
            refped[i]->SetMarkerColor(kBlue);
            refped[i]->SetLineColor(kBlue + 2);
            refped[i]->SetName("RefPeds");
        }

        for (uint lad{0}; lad < NLAD; lad++)
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                refped[lad]->SetPoint(ch, ch, fRefPedestals.at(current_ch));
            }
        for (uint lad{0}; lad < NLAD; lad++)
        {
            c->cd(ConvertLad(lad));
            // gPad->DrawFrame(fPedAxis[0],fPedAxis[2],fPedAxis[1],fPedAxis[3]);
            refped[lad]->Draw("l");
            // LinesVas();
        }
    }

    c->Update();
}

void MyMainFrame::DisplaySovern()
{
    TCanvas *c = fSovernCanvas;

    bool profile_x{TGCBshowProfileSovern->IsDown()};

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        bool IsY{IsLadY(lad)};
        c->cd(ConvertLad(lad));
        if (!profile_x)
            gPad->SetLogz();
        if (profile_x)
        {
            // fSovernAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fSovernAxis[IsY][0], (fSovernAxis[IsY][2] < 0) ? 0 : fSovernAxis[IsY][2], fSovernAxis[IsY][1], fSovernAxis[IsY][3]);
            frame->SetTitle(Form("Signal over noise vs channel - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        }

        TH2F *his2 = (TGCBshowSovernCut->IsDown() ? vh2SovernCut.at(lad) : vh2Sovern.at(lad));
        gStyle->SetOptStat(1111111);
        his2->SetStats(kTRUE);
        // fSovernAxis[1] = (vBoards.at(lad))->getNLCH();
        his2->GetXaxis()->SetRangeUser(fSovernAxis[IsY][0], fSovernAxis[IsY][1]);
        his2->GetYaxis()->SetRangeUser(fSovernAxis[IsY][2], fSovernAxis[IsY][3]);
        if (!profile_x)
            his2->Draw("colz");
        else
            his2->ProfileX()->Draw("same");
        gPad->Update();
        LinesVas(lad);
    }

    c->Update();
}

void MyMainFrame::DisplayOccupancy()
{
    TCanvas *c = fOccupancyCanvas;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // gPad->SetLogy();
        // fOccupancyAxis[IsY][1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fOccupancyAxis[IsY][0], fOccupancyAxis[IsY][2], fOccupancyAxis[IsY][1], fOccupancyAxis[IsY][3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Occupancy - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        TH1F *his = TGCBshowOccCut->IsDown() ? vhisOccupancyCut.at(lad) : vhisOccupancy.at(lad);
        his->SetStats(kTRUE);
        his->Draw("sames");
        gPad->Update();
        LinesVas(lad);
    }

    c->Update();
}

void MyMainFrame::DisplayCog()
{
    TCanvas *c = fCogCanvas;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        bool IsY{IsLadY(lad)};
        c->cd(ConvertLad(lad));
        // fCogAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fCogAxis[IsY][0], fCogAxis[IsY][2], fCogAxis[IsY][1], fCogAxis[IsY][3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Cog - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        TH1F *his = (TGCBshowCogCut->IsDown() ? vhisCogCut.at(lad) : vhisCog.at(lad));
        his->SetStats(kTRUE);
        his->Draw("sames");
        gPad->Update();
        LinesVas(lad);
    }

    c->Update();
}


void MyMainFrame::DisplayCogxy() {

    TCanvas *c = fCogxyCanvas;

    for (int det{0}; det < fNstripX; det++) {
        c->cd(det+1);
        vh2CogXY.at(det)->Draw("colz");
        TPaveStats *St = (TPaveStats*)(vh2CogXY.at(det))->FindObject("stats");
        if (St!=0) {
            St->SetFillColorAlpha(kWhite, 0.3);
            double WidthNDC{St->GetX2NDC()-St->GetX1NDC()};
            double HeightDNC{St->GetY2NDC() - St->GetY1NDC()};
            St->SetX1NDC(0.11);
            St->SetX2NDC(0.11 + WidthNDC);
            St->SetY2NDC(0.88);
            St->SetY1NDC(0.88 - HeightDNC);
        }
    }
    c->Update();
}


void MyMainFrame::DisplayLens()
{
    TCanvas *c = fLensCanvas;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // gPad->SetLogy();
        TH1F *frame = gPad->DrawFrame(fLensAxis[IsY][0], (fLensAxis[IsY][2] < 1) ? 1 : fLensAxis[IsY][2], fLensAxis[IsY][1], fLensAxis[IsY][3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Lens - Detector %d; cluster length;", lad));
        TH1F *his = (TGCBshowLensCut->IsDown() ? vhisLensCut.at(lad) : vhisLens.at(lad));
        his->SetStats(kTRUE);
        his->Draw("sames");
        gPad->Update();
        // LinesVas();
    }

    c->Update();
}

void MyMainFrame::DisplayLenVsCog()
{
    TCanvas *c = fLenVsCogCanvas;

    bool profile_x{TGCBshowProfileLenVsCog->IsDown()};

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        bool IsY{IsLadY(lad)};
        c->cd(ConvertLad(lad));
        if (!profile_x)
            gPad->SetLogz();
        if (profile_x)
        {
            // fLenVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fLenVsCogAxis[IsY][0], (fLenVsCogAxis[IsY][2] < 0) ? 0 : fLenVsCogAxis[IsY][2], fLenVsCogAxis[IsY][1], fLenVsCogAxis[IsY][3]);
            frame->SetTitle(Form("Len vs cog - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        }

        TH2F *his2 = (TGCBshowLenVsCogCut->IsDown() ? vh2LenVsCogCut.at(lad) : vh2LenVsCog.at(lad));
        gStyle->SetOptStat(1111111);
        his2->SetStats(kTRUE);
        // fLenVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
        his2->GetXaxis()->SetRangeUser(fLenVsCogAxis[IsY][0], fLenVsCogAxis[IsY][1]);
        his2->GetYaxis()->SetRangeUser(fLenVsCogAxis[IsY][2], fLenVsCogAxis[IsY][3]);
        if (!profile_x)
            his2->Draw("colz");
        else
            his2->ProfileX()->Draw("same");
        gPad->Update();
        LinesVas(lad);
    }

    c->Update();
}

void MyMainFrame::DisplayIntVsCog()
{
    TCanvas *c = fIntVsCogCanvas;

    bool profile_x{TGCBshowProfileIntVsCog->IsDown()};

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        if (!profile_x)
            gPad->SetLogz();
        if (profile_x)
        {
            // fIntVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
            TH1F *frame = gPad->DrawFrame(fIntVsCogAxis[IsY][0], (fIntVsCogAxis[IsY][2] < 0) ? 0 : fIntVsCogAxis[IsY][2], fIntVsCogAxis[IsY][1], fIntVsCogAxis[IsY][3]);
            // frame->SetTitle(Form("Int vs cog - Detector %d; %s;", lad, ChanLabel[fChanLabelMode].c_str()));
            frame->SetTitle(Form("Int vs cog - Detector %d; cog;integral", lad));
        }

        TH2F *his2 = (TGCBshowIntVsCogCut->IsDown() ? vh2IntVsCogCut.at(lad) : vh2IntVsCog.at(lad));
        gStyle->SetOptStat(1111111);
        his2->SetStats(kTRUE);
        // fIntVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
        his2->GetXaxis()->SetRangeUser(fIntVsCogAxis[IsY][0], fIntVsCogAxis[IsY][1]);
        his2->GetYaxis()->SetRangeUser(fIntVsCogAxis[IsY][2], fIntVsCogAxis[IsY][3]);
        if (!profile_x)
            his2->Draw("colz");
        else
            his2->ProfileX()->Draw("same");
        gPad->Update();
        LinesVas(lad);
    }

    c->Update();
}

void MyMainFrame::DisplayInts()
{
    TCanvas *c = fIntsCanvas;

    TF1 *flandau = new TF1("flandau", "landau");
    flandau->SetRange(20, 60);

    TF1 *flangau = new TF1("flangau", langaufun, 15, 60, 4);
    flangau->SetParameters(5, 30, 100, 5);
    flangau->SetParNames("Width", "MP", "Area", "GSigma");
    flangau->SetLineColor(kRed);
    flangau->SetLineStyle(1);
    flangau->SetParLimits(3, 0, 100);
    // flangau->SetParLimits(0, 0.7, 20);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        // gPad->SetLogy();
        bool IsY{IsLadY(lad)};

        if (!IsY)
        {
            flandau->SetRange(20, 60);
            flangau->SetParameters(5, 30, 100, 5);
            flangau->SetParLimits(1, 10, 50);
        }
        else
        { // StripY
            flandau->SetRange(150, 400);
            flangau->SetParameters(5, 250, 100, 5);
            flangau->SetParLimits(1, 150, 300);
        }
        TH1F *frame;
        // if (!IsLadY(lad))
        frame = gPad->DrawFrame(fIntsAxis[IsY][0], (fIntsAxis[IsY][2] <= 0) ? 1 : fIntsAxis[IsY][2], fIntsAxis[IsY][1], fIntsAxis[IsY][3]);
        // else
        //   frame = gPad->DrawFrame(fIntsAxisY[0], (fIntsAxisY[2] <= 0) ? 1 : fIntsAxisY[2], fIntsAxisY[1], fIntsAxisY[3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Ints - Detector %d; cluster integral (ADCc);", lad));
        TH1F *his = (TGCBshowIntsCut->IsDown() ? vhisIntSCut.at(lad) : vhisIntS.at(lad));
        his->SetStats(kTRUE);
        his->Draw("sames");
        his->Fit(flandau, "N");
        flangau->SetParameter(1, flandau->GetParameter(1));
        flangau->SetParameter(2, flandau->GetParameter(2));
        if (IsY)
            //flangau->SetRange(flandau->GetParameter(1) - 100, flandau->GetParameter(1) + 250);
            flangau->SetRange(150, 450);
        else
            flangau->SetRange(flandau->GetParameter(1) - 10, flandau->GetParameter(1) + 80);
        his->Fit(flangau, "RM");

        uint ndet{fNstripX + fNstripY};
        double posx{0.5};
        double posy{0.5};
        double size{0.05};

        switch (ndet)
        {
        case 2:
            posx = 0.6;
            posy = 0.6;
            size = 0.03;
            break;
        case 3:
            posx = 0.7;
            posy = 0.7;
            size = 0.05;
            break;
        default:
            posx = 0.5;
            posy = 0.5;
            size = 0.04;
        }
        TLatex *tlt{new TLatex()};
        tlt->SetNDC();
        tlt->SetTextSize(size);
        tlt->DrawLatex(posx, posy, Form("Width = %5.1f", flangau->GetParameter(0)));
        posy -= 0.05;
        tlt->DrawLatex(posx, posy, Form("MPV = %5.1f", flangau->GetParameter(1)));
        posy -= 0.05;
        tlt->DrawLatex(posx, posy, Form("Area = %5.1f", flangau->GetParameter(2)));
        posy -= 0.05;
        tlt->DrawLatex(posx, posy, Form("GSigma = %5.1f", flangau->GetParameter(3)));
        posy -= 0.05;
        tlt->DrawLatex(posx, posy, Form("#chi^{2}/ndf = %5.1f", flangau->GetChisquare() / flangau->GetNDF()));

        gPad->Update();
    }

    c->Update();
}

void MyMainFrame::DisplayNclus()
{
    TCanvas *c = fNclusCanvas;

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        gPad->SetLogy();
        TH1F *frame = gPad->DrawFrame(fNclusAxis[IsY][0], (fNclusAxis[IsY][2] <= 0) ? 1 : fNclusAxis[IsY][2], fNclusAxis[IsY][1], fNclusAxis[IsY][3]);
        gStyle->SetOptStat(1111111);
        frame->SetTitle(Form("Nclus - Detector %d; clusters/event;", lad));
        vhisNclus.at(lad)->SetStats(kTRUE);
        vhisNclus.at(lad)->Draw("sames");
        gPad->Update();
    }

    c->Update();
}

void MyMainFrame::DisplayPedCompar()
{

    TCanvas *c = fPedCompCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        // RemoveGraph("DelPeds");
    }
    c->Update();

    TGraph *delped[NLAD];
    for (uint i{0}; i < NLAD; i++)
    {
        delped[i] = new TGraph();
        delped[i]->SetMarkerStyle(7);
        delped[i]->SetMarkerColor(kRed);
        delped[i]->SetLineColor(kBlue + 2);
        delped[i]->SetName("DelPeds");
    }

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; lad++)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            delped[lad]->SetPoint(ch, ch, fPedestals.at(current_ch) - fRefPedestals.at(current_ch));
        }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fPedCompAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fPedCompAxis[IsY][0], fPedCompAxis[IsY][2], fPedCompAxis[IsY][1], fPedCompAxis[IsY][3]);
        frame->SetTitle(Form("Ped. diff. - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        delped[lad]->Draw("l");
        LinesVas(lad);
    }

    c->Update();
}

void MyMainFrame::DisplayRawSigmas()
{

    TCanvas *c = fSrawCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        // RemoveGraph("Sraws");
    }
    c->Update();

    TGraph *sraw[NLAD];
    for (uint i{0}; i < NLAD; i++)
    {
        sraw[i] = new TGraph();
        sraw[i]->SetMarkerStyle(7);
        sraw[i]->SetMarkerColor(kRed);
        sraw[i]->SetLineColor(kRed + 2);
        sraw[i]->SetName("Sraws");
    }

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; lad++)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            sraw[lad]->SetPoint(ch, ch, fRawSigmas.at(current_ch));
        }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fSrawAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fSrawAxis[IsY][0], fSrawAxis[IsY][2], fSrawAxis[IsY][1], fSrawAxis[IsY][3]);
        frame->SetTitle(Form("Raw sigmas - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        sraw[lad]->Draw("l");
        LinesVas(lad);
    }

    c->Update();
}

void MyMainFrame::DisplaySigmas()
{

    TCanvas *c = fSigmaCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        // RemoveGraph("Sigmas");
        // RemoveGraph("RefSigmas");
    }
    c->Update();

    TGraph *sigma[NLAD], *refsigma[NLAD];
    for (uint i{0}; i < NLAD; i++)
    {
        sigma[i] = new TGraph();
        sigma[i]->SetMarkerStyle(7);
        sigma[i]->SetMarkerColor(kRed);
        sigma[i]->SetLineColor(kRed + 2);
        sigma[i]->SetName("Sigmas");
    }

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; lad++)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            sigma[lad]->SetPoint(ch, ch, fSigmas.at(current_ch));
        }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fSigmaAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fSigmaAxis[IsY][0], fSigmaAxis[IsY][2], fSigmaAxis[IsY][1], fSigmaAxis[IsY][3]);
        frame->SetTitle(Form("Sigmas - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        sigma[lad]->Draw("l");
        LinesVas(lad);
    }

    if (TGCBshowrefsig->IsDown())
    {
        for (uint i{0}; i < NLAD; i++)
        {
            refsigma[i] = new TGraph();
            refsigma[i]->SetMarkerStyle(7);
            refsigma[i]->SetMarkerColor(kBlue);
            refsigma[i]->SetLineColor(kBlue + 2);
            refsigma[i]->SetName("RefSigmas");
        }

        for (uint lad{0}; lad < NLAD; lad++)
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                refsigma[lad]->SetPoint(ch, ch, fRefSigmas.at(current_ch));
            }
        for (unsigned int lad{0}; lad < NLAD; lad++)
        {
            c->cd(ConvertLad(lad));
            // gPad->DrawFrame(fPedAxis[0],fPedAxis[2],fPedAxis[1],fPedAxis[3]);
            refsigma[lad]->Draw("l");
            // LinesVas();
        }
    }

    c->Update();
}

void MyMainFrame::DisplaySigmaCompar()
{

    TCanvas *c = fSigmaCompCanvas;

    for (int i = 1; i < 7; i++)
    {
        c->cd(i);
        // RemoveGraph("Sigmas");
        // RemoveGraph("RefSigmas");
    }
    c->Update();

    TGraph *sigma[NLAD], *refsigma[NLAD];
    for (uint i{0}; i < NLAD; i++)
    {
        sigma[i] = new TGraph();
        sigma[i]->SetMarkerStyle(7);
        sigma[i]->SetMarkerColor(kRed);
        sigma[i]->SetName("Sigmas");
        refsigma[i] = new TGraph();
        refsigma[i]->SetMarkerStyle(7);
        refsigma[i]->SetMarkerColor(kBlue);
        refsigma[i]->SetName("RefSigmas");
    }

    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; lad++)
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
        {
            current_ch = ch;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            sigma[lad]->SetPoint(ch, ch, fSigmas.at(current_ch));
            refsigma[lad]->SetPoint(ch, ch, -fRefSigmas.at(current_ch));
        }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        c->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fSigmaCompAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fSigmaCompAxis[IsY][0], fSigmaCompAxis[IsY][2], fSigmaCompAxis[IsY][1], fSigmaCompAxis[IsY][3]);
        frame->SetTitle(Form("Sigma comparison - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        sigma[lad]->Draw("pl");
        refsigma[lad]->Draw("pl");
        LinesVas(lad, 1);
    }

    c->Update();
}

void MyMainFrame::InitGraphHis()
{

    for (uint adc = 0; adc < 2 * fNADC; adc++)
    {
        vgradcpedsubt.at(adc) = new TGraph();
        vgradcpedsubt.at(adc)->SetName(Form("diff_adc_%d", adc));
        vgradcpedcnsubt.at(adc) = new TGraph();
        vgradcpedcnsubt.at(adc)->SetName(Form("signal_adc_%d", adc));
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH() + 1; ch++)
        {
            TH1F *his{new TH1F(Form("hisChHi_lad%d_ch%d", lad, ch), Form("ChHi Detector %d ch %d", lad, ch), 4000, -500., 500.)}; // attatt
            (vvHisChHi.at(lad)).at(ch) = his;
            his = new TH1F(Form("hisChHiPe_lad%d_ch%d", lad, ch), Form("ChHiPe Detector %d ch %d", lad, ch), 100, -0.5, 100 - 0.5); // attatt
            (vvHisChHiPe.at(lad)).at(ch) = his;
        }
        vh2ChHi.at(lad) = (new TH2F(Form("h2ChHi_lad%d", lad), Form("h2ChHi_lad%d", lad), fNTOTCH + 1, 0, fNTOTCH + 1, 4096, 0, 4096));
        vh2ChHiPe.at(lad) = new TH2F(Form("h2ChHiPe_lad%d", lad), Form("h2ChHiPe_lad%d", lad), fNTOTCH + 1, 0, fNTOTCH + 1, 2000, 0, 100);
    }

    TGraph *gr{0};
    TH1F *his{0};

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        gr = new TGraph();
        gr->SetName(Form("ladder_%d", lad));
        vgradc.at(lad) = gr;
        gr = new TGraph();
        gr->SetName(Form("diff_ladder_%d", lad));
        vgrpedsubt.at(lad) = gr;

        gr = new TGraph();
        gr->SetName(Form("signal_ladder_%d", lad));
        vgrpedcnsubt.at(lad) = gr;

        vgrPixelResultsIntercept.at(lad) = new TGraph();

        vgrPixelResultsSlope.at(lad) = new TGraph();
        for (uint adc{0}; adc < (vBoards.at(lad))->getNADC(); adc++)
        {
            (vhisFFT.at(lad)).at(adc) = new TH1F(Form("hisFFT_lad%d_adc%d", lad, adc), Form("FFT Detector %d ADC %d", lad, adc), (vBoards.at(lad))->getNLCH(), 0, (vBoards.at(lad))->getNLCH());
            (vhisFFTnorm.at(lad)).at(adc) = new TH1F(Form("hisFFTnorm_lad%d_adc%d", lad, adc), Form("Normalized FFT Detector %d ADC %d", lad, adc), (vBoards.at(lad))->getNLCH(), 0, (vBoards.at(lad))->getNLCH());
            (vhisSumFFT.at(lad)).at(adc) = new TH1F(Form("hisSumFFT_lad%d_adc%d", lad, adc), Form("Cumulative FFT Detector %d ADC %d", lad, adc), (vBoards.at(lad))->getNLCH(), 0, (vBoards.at(lad))->getNLCH());
            (vhisSumFFTnorm.at(lad)).at(adc) = new TH1F(Form("hisSumFFTnorm_lad%d_adc%d", lad, adc), Form("Normalized Cumulative FFT Detector %d ADC %d", lad, adc), (vBoards.at(lad))->getNLCH(), 0, (vBoards.at(lad))->getNLCH());
        }
        his = new TH1F(Form("hisOccupancy_lad%d", lad), Form("hisOccupancy_lad%d", lad), (vBoards.at(lad))->getNLCH(), 0, (vBoards.at(lad))->getNLCH());
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisOccupancy.at(lad) = his;

        his = new TH1F(Form("hisOccupancyCut_lad%d", lad), Form("hisOccupancyCut_lad%d", lad), (vBoards.at(lad))->getNLCH(), 0, (vBoards.at(lad))->getNLCH());
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisOccupancyCut.at(lad) = his;

        his = new TH1F(Form("hisCog_lad%d", lad), Form("hisCog_lad%d", lad), (vBoards.at(lad))->getNLCH() * 10, 0, (vBoards.at(lad))->getNLCH());
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisCog.at(lad) = his;

        his = new TH1F(Form("hisCogCut_lad%d", lad), Form("hisCogCut_lad%d", lad), (vBoards.at(lad))->getNLCH() * 10, 0, (vBoards.at(lad))->getNLCH());
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisCogCut.at(lad) = his;

        his = new TH1F(Form("hisLens_lad%d", lad), Form("hisLens_lad%d", lad), 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC(), 0, 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC());
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisLens.at(lad) = his;

        his = new TH1F(Form("hisLensCut_lad%d", lad), Form("hisLensCut_lad%d", lad), 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC(), 0, 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC());
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisLensCut.at(lad) = his;

        int xmax = IsLadY(lad) ? 5000 : 1000;
        int nbins = IsLadY(lad) ? 5000 : 5000;
        his = new TH1F(Form("hisIntS_lad%d", lad), Form("hisIntS_lad%d", lad), nbins, 0, xmax);
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisIntS.at(lad) = his;

        his = new TH1F(Form("hisIntSCut_lad%d", lad), Form("hisIntSCut_lad%d", lad), 5000, 0, 5000);
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisIntSCut.at(lad) = his;

        his = new TH1F(Form("hisnCluS_lad%d", lad), Form("hisnCluS_lad%d", lad), 50, 0, 50);
        his->SetFillStyle(3001);
        his->SetFillColor(kAzure - 8);
        his->SetLineColor(kBlue + 2);
        vhisNclus.at(lad) = his;
        vh2LenVsCog.at(lad) = (new TH2F(Form("hisLenVsCog_lad%d", lad), Form("Len vs cog - Detector %d; channel;", lad), (vBoards.at(lad))->getNLCH() * 4, 0, (vBoards.at(lad))->getNLCH(), 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC(), 0, 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC()));
        vh2LenVsCogCut.at(lad) = (new TH2F(Form("hisLenVsCogCut_lad%d", lad), Form("Len vs cog (selection) - Detector %d; channel;", lad), (vBoards.at(lad))->getNLCH() * 4, 0, (vBoards.at(lad))->getNLCH(), 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC(), 0, 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC()));

        vh2Sovern.at(lad) = (new TH2F(Form("hisSovern_lad%d", lad), Form("Signal over noise vs channel - Detector %d; channel;", lad), (vBoards.at(lad))->getNLCH(), 0, (vBoards.at(lad))->getNLCH(), 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC(), 0, 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC()));
        vh2SovernCut.at(lad) = (new TH2F(Form("hisSovernCut_lad%d", lad), Form("Signal over noise vs channel (selection) - Detector %d; channel;", lad), (vBoards.at(lad))->getNLCH(), 0, (vBoards.at(lad))->getNLCH(), 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC(), 0, 2 * (vBoards.at(lad))->getNLCH() / (vBoards.at(lad))->getNADC()));

        vh2IntVsCog.at(lad) = (new TH2F(Form("hisIntVsCog_lad%d", lad), Form("Int vs cog - Detector %d; channel;", lad), (vBoards.at(lad))->getNLCH() * 4, 0, (vBoards.at(lad))->getNLCH(), 5000, 0, 10000));
        vh2IntVsCogCut.at(lad) = (new TH2F(Form("hisIntVsCogCut_lad%d", lad), Form("Int vs cog (selection) - Detector %d; channel;", lad), (vBoards.at(lad))->getNLCH() * 4, 0, (vBoards.at(lad))->getNLCH(), 5000, 0, 10000));

        vh2CogXY.at(lad) = (new TH2F(Form("hisCogXY_lad%d", lad), Form("Cog Y vs Cog X - Detector %d; CoG X (mm); CoG Y (mm)", lad), 140, -10, 60, 140, -10, 60));

        gr = new TGraph();
        gr->SetName(Form("clusters_ladder_%d", lad));
        gr->SetMarkerStyle(23);
        gr->SetMarkerColor(kRed);
        vgrclusters.at(lad) = gr;
    }

    // for (int i=0; i<fNLCH; i++) fIndex[i]=i;
}

void MyMainFrame::ResetHistos()
{

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH() + 1; ch++)
        {
            (vvHisChHi.at(lad)).at(ch)->Reset();
            (vvHisChHiPe.at(lad)).at(ch)->Reset();
        }
    }

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        for (uint adc{0}; adc < (vBoards.at(lad))->getNADC(); adc++)
        {
            (vhisFFT.at(lad)).at(adc)->Reset();
            (vhisFFTnorm.at(lad)).at(adc)->Reset();
            (vhisSumFFT.at(lad)).at(adc)->Reset();
            (vhisSumFFTnorm.at(lad)).at(adc)->Reset();
        }
        vhisOccupancy.at(lad)->Reset();
        vhisOccupancyCut.at(lad)->Reset();
        vhisCog.at(lad)->Reset();
        vhisLens.at(lad)->Reset();
        vhisIntS.at(lad)->Reset();
        vhisNclus.at(lad)->Reset();
        vhisCogCut.at(lad)->Reset();
        vhisLensCut.at(lad)->Reset();
        vhisIntSCut.at(lad)->Reset();
        vhisNclus.at(lad)->Reset();
        vh2LenVsCog.at(lad)->Reset();
        vh2LenVsCog.at(lad)->ProfileX()->Reset();
        vh2LenVsCogCut.at(lad)->Reset();
        vh2LenVsCogCut.at(lad)->ProfileX()->Reset();
        vh2Sovern.at(lad)->Reset();
        vh2Sovern.at(lad)->ProfileX()->Reset();
        vh2SovernCut.at(lad)->Reset();
        vh2SovernCut.at(lad)->ProfileX()->Reset();
        vh2IntVsCog.at(lad)->Reset();
        vh2IntVsCog.at(lad)->ProfileX()->Reset();
        vh2IntVsCogCut.at(lad)->Reset();
        vh2IntVsCogCut.at(lad)->ProfileX()->Reset();

        vh2CogXY.at(lad)->Reset();
    }
}

void MyMainFrame::ResetGraph(TGraph *gr)
{

    if (gr == 0)
        return;

    gr->Set(0);

    // for (int i=0; i<gr->GetN(); i++) gr->RemovePoint(0);
}

void MyMainFrame::UpdateName()
{
    fFilename = fTeFile->GetText();
    if (OpenFile())
    {
        AddLogMessage(Form("Error with file %s", fFilename.c_str()));
        fTeFile->SetBackgroundColor(0xfecccd);
    }
    else
    {
        AddLogMessage(Form("File %s is successfully open", fFilename.c_str()));
        fTeFile->SetBackgroundColor(0xcdffcf);
    }
}

void MyMainFrame::AddAdcTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEadccanvas = new TRootEmbeddedCanvas("Eadccanvas", Vframe, fWidth, fHeight);
    fAdcCanvas = fEadccanvas->GetCanvas();
    fAdcCanvas->Clear();
    DivideCanvas(fAdcCanvas);
    // fAdcCanvas->Divide(3,2);

    fCanvasNames.push_back("ADC");
    fCanvasList.push_back(fAdcCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fAdcCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fAdcAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fAdcAxis[IsY][0], fAdcAxis[IsY][2], fAdcAxis[IsY][1], fAdcAxis[IsY][3]);
        frame->SetTitle(Form("ADC - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEadcax, fAdcAxis, "UpdateAdcAxes");

    Vframe->AddFrame(fEadccanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddSovernTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEsoverncanvas = new TRootEmbeddedCanvas("Esoverncanvas", Vframe, fWidth, fHeight);
    fSovernCanvas = fEsoverncanvas->GetCanvas();
    fSovernCanvas->Clear();
    DivideCanvas(fSovernCanvas);
    // fSovernCanvas->Divide(3,2);

    fCanvasNames.push_back("SOVERN");
    fCanvasList.push_back(fSovernCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fSovernCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fSovernAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fSovernAxis[IsY][0], fSovernAxis[IsY][2], fSovernAxis[IsY][1], fSovernAxis[IsY][3]);
        frame->SetTitle(Form("Signal over noise vs channel - Detector %d; channel;", lad));
        // LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEsovernax, fSovernAxis, "UpdateSovernAxes");

    TGCBshowProfileSovern = new TGCheckButton(Hframe, "Show x-profile");
    TGCBshowProfileSovern->Connect("Clicked()", "MyMainFrame", this, "DisplaySovern()");
    Hframe->AddFrame(TGCBshowProfileSovern, TGLHleft);

    TGCBshowSovernCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowSovernCut->Connect("Clicked()", "MyMainFrame", this, "DisplaySovern()");
    Hframe->AddFrame(TGCBshowSovernCut, TGLHleft);

    Vframe->AddFrame(fEsoverncanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddOccupancyTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEoccupancycanvas = new TRootEmbeddedCanvas("Eoccupancycanvas", Vframe, fWidth, fHeight);
    fOccupancyCanvas = fEoccupancycanvas->GetCanvas();
    fOccupancyCanvas->Clear();
    fOccupancyCanvas->ToggleEventStatus();
    DivideCanvas(fOccupancyCanvas);
    // fOccupancyCanvas->Divide(3,2);

    fCanvasNames.push_back("OCCUPANCY");
    fCanvasList.push_back(fOccupancyCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fOccupancyCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fOccupancyAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fOccupancyAxis[IsY][0], fOccupancyAxis[IsY][2], fOccupancyAxis[IsY][1], fOccupancyAxis[IsY][3]);
        frame->SetTitle(Form("Occupancy - Detector %d; channel;", lad));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEoccupancyax, fOccupancyAxis, "UpdateOccupancyAxes");

    TGCBshowOccCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowOccCut->Connect("Clicked()", "MyMainFrame", this, "DisplayOccupancy()");
    Hframe->AddFrame(TGCBshowOccCut, TGLHleft);

    Vframe->AddFrame(fEoccupancycanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddCogTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEcogcanvas = new TRootEmbeddedCanvas("Ecogcanvas", Vframe, fWidth, fHeight);
    fCogCanvas = fEcogcanvas->GetCanvas();
    fCogCanvas->Clear();
    DivideCanvas(fCogCanvas);
    // fCogCanvas->Divide(3,2);

    fCanvasNames.push_back("COG");
    fCanvasList.push_back(fCogCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fCogCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fCogAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fCogAxis[IsY][0], fCogAxis[IsY][2], fCogAxis[IsY][1], fCogAxis[IsY][3]);
        frame->SetTitle(Form("Center of gravity - Detector %d; channel;", lad));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEcogax, fCogAxis, "UpdateCogAxes");

    TGCBshowCogCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowCogCut->Connect("Clicked()", "MyMainFrame", this, "DisplayCog()");
    Hframe->AddFrame(TGCBshowCogCut, TGLHleft);

    Vframe->AddFrame(fEcogcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddLensTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fElenscanvas = new TRootEmbeddedCanvas("Elenscanvas", Vframe, fWidth, fHeight);
    fLensCanvas = fElenscanvas->GetCanvas();
    fLensCanvas->Clear();
    DivideCanvas(fLensCanvas);
    // fLensCanvas->Divide(3,2);

    fCanvasNames.push_back("LENS");
    fCanvasList.push_back(fLensCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        bool IsY{IsLadY(lad)};
        fLensCanvas->cd(ConvertLad(lad));
        TH1F *frame = gPad->DrawFrame(fLensAxis[IsY][0], fLensAxis[IsY][2], fLensAxis[IsY][1], fLensAxis[IsY][3]);
        frame->SetTitle(Form("S-Cluster length - Detector %d; channel;", lad));
        // LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNElensax, fLensAxis, "UpdateLensAxes");

    TGCBshowLensCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowLensCut->Connect("Clicked()", "MyMainFrame", this, "DisplayLens()");
    Hframe->AddFrame(TGCBshowLensCut, TGLHleft);

    Vframe->AddFrame(fElenscanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddLenVsCogTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fElenvscogcanvas = new TRootEmbeddedCanvas("Elenvscogcanvas", Vframe, fWidth, fHeight);
    fLenVsCogCanvas = fElenvscogcanvas->GetCanvas();
    fLenVsCogCanvas->Clear();
    DivideCanvas(fLenVsCogCanvas);
    // fLenVsCogCanvas->Divide(3,2);

    fCanvasNames.push_back("LENVSCOG");
    fCanvasList.push_back(fLenVsCogCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fLenVsCogCanvas->cd(ConvertLad(lad));
        // fLenVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
        bool IsY{IsLadY(lad)};
        TH1F *frame = gPad->DrawFrame(fLenVsCogAxis[IsY][0], fLenVsCogAxis[IsY][2], fLenVsCogAxis[IsY][1], fLenVsCogAxis[IsY][3]);
        frame->SetTitle(Form("Cluster length vs cog - Detector %d; channel;", lad));
        // LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNElenvscogax, fLenVsCogAxis, "UpdateLenVsCogAxes");
    /*
        TGLabel *minmax[4];
        minmax[0] = new TGLabel(Hframe, "min x:");
        minmax[1] = new TGLabel(Hframe, "max x:");
        minmax[2] = new TGLabel(Hframe, "min y:");
        minmax[3] = new TGLabel(Hframe, "max y:");

        for (int i = 0; i < 4; i++)
        {
            TGNElenvscogax[i] = new TGNumberEntry(Hframe, fLenVsCogAxis[i]);
            TGNElenvscogax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateLenVsCogAxes()");
        }

        for (int i = 0; i < 4; i++)
        {
            Hframe->AddFrame(minmax[i], TGLHleft);
            Hframe->AddFrame(TGNElenvscogax[i], TGLHleft);
        }
    */
    TGCBshowProfileLenVsCog = new TGCheckButton(Hframe, "Show x-profile");
    TGCBshowProfileLenVsCog->Connect("Clicked()", "MyMainFrame", this, "DisplayLenVsCog()");
    Hframe->AddFrame(TGCBshowProfileLenVsCog, TGLHleft);

    TGCBshowLenVsCogCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowLenVsCogCut->Connect("Clicked()", "MyMainFrame", this, "DisplayLenVsCog()");
    Hframe->AddFrame(TGCBshowLenVsCogCut, TGLHleft);

    Vframe->AddFrame(fElenvscogcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddIntVsCogTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEintvscogcanvas = new TRootEmbeddedCanvas("Eintvscogcanvas", Vframe, fWidth, fHeight);
    fIntVsCogCanvas = fEintvscogcanvas->GetCanvas();
    fIntVsCogCanvas->Clear();
    DivideCanvas(fIntVsCogCanvas);
    // fIntVsCogCanvas->Divide(3,2);

    fCanvasNames.push_back("INTVSCOG");
    fCanvasList.push_back(fIntVsCogCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fIntVsCogCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fIntVsCogAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fIntVsCogAxis[IsY][0], fIntVsCogAxis[IsY][2], fIntVsCogAxis[IsY][1], fIntVsCogAxis[IsY][3]);
        frame->SetTitle(Form("Cluster integral vs cog - Detector %d; channel;", lad));
        // LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEintvscogax, fIntVsCogAxis, "UpdateIntVsCogAxes");
    /*
        TGLabel *minmax[4];
        minmax[0] = new TGLabel(Hframe, "min x:");
        minmax[1] = new TGLabel(Hframe, "max x:");
        minmax[2] = new TGLabel(Hframe, "min y:");
        minmax[3] = new TGLabel(Hframe, "max y:");

        for (int i = 0; i < 4; i++)
        {
            TGNEintvscogax[i] = new TGNumberEntry(Hframe, fIntVsCogAxis[i]);
            TGNEintvscogax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateIntVsCogAxes()");
        }

        for (int i = 0; i < 4; i++)
        {
            Hframe->AddFrame(minmax[i], TGLHleft);
            Hframe->AddFrame(TGNEintvscogax[i], TGLHleft);
        }
    */
    TGCBshowProfileIntVsCog = new TGCheckButton(Hframe, "Show x-profile");
    TGCBshowProfileIntVsCog->Connect("Clicked()", "MyMainFrame", this, "DisplayIntVsCog()");
    Hframe->AddFrame(TGCBshowProfileIntVsCog, TGLHleft);

    TGCBshowIntVsCogCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowIntVsCogCut->Connect("Clicked()", "MyMainFrame", this, "DisplayIntVsCog()");
    Hframe->AddFrame(TGCBshowIntVsCogCut, TGLHleft);

    Vframe->AddFrame(fEintvscogcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddIntsTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEintscanvas = new TRootEmbeddedCanvas("Eintscanvas", Vframe, fWidth, fHeight);
    fIntsCanvas = fEintscanvas->GetCanvas();
    fIntsCanvas->Clear();
    DivideCanvas(fIntsCanvas);
    // fIntsCanvas->Divide(3,2);

    fCanvasNames.push_back("INTS");
    fCanvasList.push_back(fIntsCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fIntsCanvas->cd(ConvertLad(lad));
        TH1F *frame = gPad->DrawFrame(fIntsAxis[IsLadY(lad)][0], fIntsAxis[IsLadY(lad)][2], fIntsAxis[IsLadY(lad)][1], fIntsAxis[IsLadY(lad)][3]);
        frame->SetTitle(Form("Integral - Detector %d; cluster integral (ADCc);", lad));
        // LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);

    BuildAxisControls(Hframe, TGNEintsax, fIntsAxis, "UpdateIntsAxes");

    TGCBshowIntsCut = new TGCheckButton(Hframe, "With cluster integral cut");
    TGCBshowIntsCut->Connect("Clicked()", "MyMainFrame", this, "DisplayInts()");
    Hframe->AddFrame(TGCBshowIntsCut, TGLHleft);

    Vframe->AddFrame(fEintscanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddNclusTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEncluscanvas = new TRootEmbeddedCanvas("ENcluscanvas", Vframe, fWidth, fHeight);
    fNclusCanvas = fEncluscanvas->GetCanvas();
    fNclusCanvas->Clear();
    DivideCanvas(fNclusCanvas);
    // fIntsCanvas->Divide(3,2);

    fCanvasNames.push_back("NCLUS");
    fCanvasList.push_back(fNclusCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fNclusCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        TH1F *frame = gPad->DrawFrame(fNclusAxis[IsY][0], fNclusAxis[IsY][2], fNclusAxis[IsY][1], fNclusAxis[IsY][3]);
        frame->SetTitle(Form("N of clusters S-side - Detector %d; channel;", lad));
        // LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEnclusax, fNclusAxis, "UpdateNclusAxes");
    /*
        TGLabel *minmax[4];
        minmax[0] = new TGLabel(Hframe, "min x:");
        minmax[1] = new TGLabel(Hframe, "max x:");
        minmax[2] = new TGLabel(Hframe, "min y:");
        minmax[3] = new TGLabel(Hframe, "max y:");

        for (int i = 0; i < 4; i++)
        {
            TGNEnclusax[i] = new TGNumberEntry(Hframe, fNclusAxis[i]);
            TGNEnclusax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateNclusAxes()");
        }

        for (int i = 0; i < 4; i++)
        {
            Hframe->AddFrame(minmax[i], TGLHleft);
            Hframe->AddFrame(TGNEnclusax[i], TGLHleft);
        }
    */
    Vframe->AddFrame(fEncluscanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddFftTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEfftcanvas = new TRootEmbeddedCanvas("Efftcanvas", Vframe, fWidth, fHeight);
    fFftCanvas = fEfftcanvas->GetCanvas();
    fFftCanvas->Clear();
    DivideCanvas(fFftCanvas);
    // fFftCanvas->Divide(3,2);

    fCanvasNames.push_back("FFT");
    fCanvasList.push_back(fFftCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fFftCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fFftAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fFftAxis[IsY][0], fFftAxis[IsY][2], fFftAxis[IsY][1], fFftAxis[IsY][3]);
        frame->SetTitle(Form("FFT - Detector %d; frequency;", lad));
        // LinesVas();
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEfftax, fFftAxis, "UpdateFftAxes");
    /*
        TGLabel *minmax[4];
        minmax[0] = new TGLabel(Hframe, "min x:");
        minmax[1] = new TGLabel(Hframe, "max x:");
        minmax[2] = new TGLabel(Hframe, "min y:");
        minmax[3] = new TGLabel(Hframe, "max y:");

        for (int i = 0; i < 4; i++)
        {
            TGNEfftax[i] = new TGNumberEntry(Hframe, fFftAxis[i]);
            TGNEfftax[i]->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateFftAxes()");
        }

        for (int i = 0; i < 4; i++)
        {
            Hframe->AddFrame(minmax[i], TGLHleft);
            Hframe->AddFrame(TGNEfftax[i], TGLHleft);
        }
    */
    //  normlabel=new TGLabel(Hframe,"
    TGCBShowFFTNorm = new TGCheckButton(Hframe, "Normalized");
    TGCBShowFFTNorm->SetDown();
    Hframe->AddFrame(TGCBShowFFTNorm, TGLHleft);

    TGCBShowFFTSum = new TGCheckButton(Hframe, "Cumulative");
    TGCBShowFFTSum->SetDown();
    Hframe->AddFrame(TGCBShowFFTSum, TGLHleft);

    Vframe->AddFrame(fEfftcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddPedSubtTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEpedsubtcanvas = new TRootEmbeddedCanvas("Epedsubtcanvas", Vframe, fWidth, fHeight);
    fPedSubtCanvas = fEpedsubtcanvas->GetCanvas();
    fPedSubtCanvas->Clear();
    DivideCanvas(fPedSubtCanvas);
    // fPedSubtCanvas->Divide(3,2);

    fCanvasNames.push_back("PedSub");
    fCanvasList.push_back(fPedSubtCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fPedSubtCanvas->cd(ConvertLad(lad));
        fPedSubtAxis[IsLadY(lad)][1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fPedSubtAxis[IsLadY(lad)][0], fPedSubtAxis[IsLadY(lad)][2], fPedSubtAxis[IsLadY(lad)][1], fPedSubtAxis[IsLadY(lad)][3]);
        frame->SetTitle(Form("Ped. subt. - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);

    BuildAxisControls(Hframe, TGNEpedsubtax, fPedSubtAxis, "UpdatePedSubtAxes");

    Vframe->AddFrame(fEpedsubtcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddPedCnSubtTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEpedcnsubtcanvas = new TRootEmbeddedCanvas("Epedcnsubtcanvas", Vframe, fWidth, fHeight);
    fPedCnSubtCanvas = fEpedcnsubtcanvas->GetCanvas();
    fPedCnSubtCanvas->Clear();
    DivideCanvas(fPedCnSubtCanvas);
    // fPedCnSubtCanvas->Divide(3,2);

    fCanvasNames.push_back("CNSub");
    fCanvasList.push_back(fPedCnSubtCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        bool IsY{IsLadY(lad)};
        fPedCnSubtCanvas->cd(ConvertLad(lad));
        // fPedCnSubtAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fPedCnSubtAxis[IsY][0], fPedCnSubtAxis[IsY][2], fPedCnSubtAxis[IsY][1], fPedCnSubtAxis[IsY][3]);
        frame->SetTitle(Form("Signals - Detector %d; %s;", lad, ChanLabel[(vBoards.at(lad))->getConvertMode()].c_str()));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEpedcnsubtax, fPedCnSubtAxis, "UpdatePedCnSubtAxes");

    Vframe->AddFrame(fEpedcnsubtcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddPedTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEpedcanvas = new TRootEmbeddedCanvas("Epedcanvas", Vframe, fWidth, fHeight);
    fPedCanvas = fEpedcanvas->GetCanvas();
    fPedCanvas->Clear();
    DivideCanvas(fPedCanvas);
    // fPedCanvas->Divide(3,2);

    fCanvasNames.push_back("Pedestals");
    fCanvasList.push_back(fPedCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fPedCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fPedAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fPedAxis[IsY][0], fPedAxis[IsY][2], fPedAxis[IsY][1], fPedAxis[IsY][3]);
        frame->SetTitle(Form("Pedestals - Detector %d; channels;", lad));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEpedax, fPedAxis, "UpdatePedAxes");

    TGCBshowrefped = new TGCheckButton(Hframe, "Show reference");
    TGCBshowrefped->Connect("Clicked()", "MyMainFrame", this, "DisplayPedestals()");

    Hframe->AddFrame(TGCBshowrefped, TGLHleft);

    Vframe->AddFrame(fEpedcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddPedCompTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEpedcompcanvas = new TRootEmbeddedCanvas("Epedcompcanvas", Vframe, fWidth, fHeight);
    fPedCompCanvas = fEpedcompcanvas->GetCanvas();
    fPedCompCanvas->Clear();
    DivideCanvas(fPedCompCanvas);
    // fPedCompCanvas->Divide(3,2);

    fCanvasNames.push_back("PedComp");
    fCanvasList.push_back(fPedCompCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fPedCompCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fPedCompAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fPedCompAxis[IsY][0], fPedCompAxis[IsY][2], fPedCompAxis[IsY][1], fPedCompAxis[IsY][3]);
        frame->SetTitle(Form("Ped. diff. - Detector %d; channels;", lad));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEpedcompax, fPedCompAxis, "UpdatePedCompAxes");

    Vframe->AddFrame(fEpedcompcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddSrawTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEsrawcanvas = new TRootEmbeddedCanvas("Esrawcanvas", Vframe, fWidth, fHeight);
    fSrawCanvas = fEsrawcanvas->GetCanvas();
    fSrawCanvas->Clear();
    DivideCanvas(fSrawCanvas);
    // fSrawCanvas->Divide(3,2);

    fCanvasNames.push_back("RawSigmas");
    fCanvasList.push_back(fSrawCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        bool IsY{IsLadY(lad)};
        fSrawCanvas->cd(ConvertLad(lad));
        // fSrawAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fSrawAxis[IsY][0], fSrawAxis[IsY][2], fSrawAxis[IsY][1], fSrawAxis[IsY][3]);
        frame->SetTitle(Form("Raw sigmas- Detector %d; channels;", lad));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEsrawax, fSrawAxis, "UpdateSrawAxes");

    Vframe->AddFrame(fEsrawcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddSigmaTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEsigmacanvas = new TRootEmbeddedCanvas("Esigmacanvas", Vframe, fWidth, fHeight);
    fSigmaCanvas = fEsigmacanvas->GetCanvas();
    fSigmaCanvas->Clear();
    DivideCanvas(fSigmaCanvas);
    // fSigmaCanvas->Divide(3,2);

    fCanvasNames.push_back("Sigmas");
    fCanvasList.push_back(fSigmaCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fSigmaCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fSigmaAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fSigmaAxis[IsY][0], fSigmaAxis[IsY][2], fSigmaAxis[IsY][1], fSigmaAxis[IsY][3]);
        frame->SetTitle(Form("Sigmas - Detector %d; channels;", lad));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEsigmaax, fSigmaAxis, "UpdateSigmaAxes");

    TGCBshowrefsig = new TGCheckButton(Hframe, "Show reference");
    TGCBshowrefsig->Connect("Clicked()", "MyMainFrame", this, "DisplaySigmas()");

    Hframe->AddFrame(TGCBshowrefsig, TGLHleft);

    Vframe->AddFrame(fEsigmacanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddSigmaCompTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEsigmacompcanvas = new TRootEmbeddedCanvas("Esigmacompcanvas", Vframe, fWidth, fHeight);
    fSigmaCompCanvas = fEsigmacompcanvas->GetCanvas();
    fSigmaCompCanvas->Clear();
    DivideCanvas(fSigmaCompCanvas);
    // fSigmaCompCanvas->Divide(3,2);

    fCanvasNames.push_back("SigmaComp");
    fCanvasList.push_back(fSigmaCompCanvas);

    for (unsigned int lad{0}; lad < NLAD; lad++)
    {
        fSigmaCompCanvas->cd(ConvertLad(lad));
        bool IsY{IsLadY(lad)};
        // fSigmaCompAxis[1] = (vBoards.at(lad))->getNLCH();
        TH1F *frame = gPad->DrawFrame(fSigmaCompAxis[IsY][0], fSigmaCompAxis[IsY][2], fSigmaCompAxis[IsY][1], fSigmaCompAxis[IsY][3]);
        frame->SetTitle(Form("Sigmas comparison - Detector %d; channels;", lad));
        LinesVas(lad);
    }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    BuildAxisControls(Hframe, TGNEsigmacompax, fSigmaCompAxis, "UpdateSigmaCompAxes");

    Vframe->AddFrame(fEsigmacompcanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddCogxyTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);

    fEcogxycanvas = new TRootEmbeddedCanvas("cogxycanvas", Vframe, fWidth, fHeight);
    fCogxyCanvas = fEcogxycanvas->GetCanvas();
    fCogxyCanvas->Clear();
    if (fNstripY > 0)
    {
        CreateSquarePads(fCogxyCanvas, fNstripX, fNstripY);
    }
    // DivideCanvas(fCogxyCanvas);
    //  fSigmaCompCanvas->Divide(3,2);

    fCanvasNames.push_back("Cogxy");
    fCanvasList.push_back(fCogxyCanvas);

    for (unsigned int lady{0}; lady < fNstripY; lady++)
        for (unsigned int lad{0}; lad < fNstripX; lad++)
        {
            fCogxyCanvas->cd(ConvertLad(lad));
            bool IsY{IsLadY(lad)};
            TH1F *frame = gPad->DrawFrame(fCogxyAxis[0], fCogxyAxis[2], fCogxyAxis[1], fCogxyAxis[3]);
            frame->SetTitle(Form("Cog Y vs Cog X - Detector %d; CoG X (mm); CoG Y (mm)", lad));
        
        }

    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    //BuildAxisControls(Hframe, TGNEsigmacompax, fSigmaCompAxis, "UpdateSigmaCompAxes");

    Vframe->AddFrame(fEcogxycanvas, TGLHexpandXexpandY);
    Vframe->AddFrame(Hframe, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHexpandXexpandY);
}

void MyMainFrame::AddLogTab(TGCompositeFrame *cframe)
{

    TGTVlog = new TGTextView(cframe);
    cframe->AddFrame(TGTVlog, TGLHexpandXexpandY);

    fCanvasNames.push_back("Log");
    fCanvasList.push_back(0);
}

void MyMainFrame::AddSettingsTab(TGCompositeFrame *cframe)
{

    TGVerticalFrame *Vframe = new TGVerticalFrame(cframe, 200, 40);
    TGHorizontalFrame *Hframe = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe2 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe3 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe301 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe302 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe303 = new TGHorizontalFrame(Vframe, 200, 120);
    TGHorizontalFrame *Hframe31 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe4 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe5 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe6 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe7 = new TGHorizontalFrame(Vframe, 200, 40);
    TGHorizontalFrame *Hframe8 = new TGHorizontalFrame(Vframe, 200, 40);

    TGLabel *labelExcludeEvents = new TGLabel(Hframe, "First events to exclude: ");
    TGNEexcludeevents = new TGNumberEntry(Hframe, ExcludeEvents);
    TGNEexcludeevents->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateExcludeEvents()");

    Hframe->AddFrame(labelExcludeEvents, TGLHleft);
    Hframe->AddFrame(TGNEexcludeevents, TGLHleft);

    TGLabel *labelCNcutval = new TGLabel(Hframe, "Common noise signal exclusion threshold: ");
    TGNEcncutval = new TGNumberEntry(Hframe, CNcutval);
    TGNEcncutval->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCNcutval()");
    
    Hframe->AddFrame(labelCNcutval, TGLHleft);
    Hframe->AddFrame(TGNEcncutval, TGLHleft);
    
    TGLabel *labelCNYcutval = new TGLabel(Hframe, "Common noise signal exclusion threshold for StripY: ");
    TGNEcnycutval = new TGNumberEntry(Hframe, CNYcutval);
    TGNEcnycutval->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCNYcutval()");
    
    Hframe->AddFrame(labelCNYcutval, TGLHleft);
    Hframe->AddFrame(TGNEcnycutval, TGLHleft);
    
    TGCBCNPoly4Fit = new TGCheckButton(Hframe, "Use pol4 fit to compute CN in StripY");
    TGCBCNPoly4Fit->SetDown(kTRUE);
    
    Hframe->AddFrame(TGCBCNPoly4Fit, TGLHleft);
    
    //
    TGLabel *labelCluHighThresh = new TGLabel(Hframe2, "Cluster high threshold X (sigma): ");
    TGNEcluHighThresh = new TGNumberEntry(Hframe2, fCluHighThresh);
    TGNEcluHighThresh->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCluHighThresh()");

    TGLabel *labelCluHighThreshY = new TGLabel(Hframe2, "Cluster high threshold Y (sigma): ");
    TGNEcluHighThreshY = new TGNumberEntry(Hframe2, fCluHighThreshY);
    TGNEcluHighThreshY->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCluHighThreshY()");

    Hframe2->AddFrame(labelCluHighThresh, TGLHleft);
    Hframe2->AddFrame(TGNEcluHighThresh, TGLHleft);

    Hframe2->AddFrame(labelCluHighThreshY, TGLHleft);
    Hframe2->AddFrame(TGNEcluHighThreshY, TGLHleft);
    //
    TGLabel *labelCluLowThresh = new TGLabel(Hframe3, "Cluster low threshold (sigma): ");
    TGNEcluLowThresh = new TGNumberEntry(Hframe3, fCluLowThresh);
    TGNEcluLowThresh->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateCluLowThresh()");

    Hframe3->AddFrame(labelCluLowThresh, TGLHleft);
    Hframe3->AddFrame(TGNEcluLowThresh, TGLHleft);
    
    TGLabel *labelMaskStripsX0 = new TGLabel(Hframe301, "Mask strips for StripX0:");
    TGLabel *labelMaskStripsX1 = new TGLabel(Hframe301, "Mask strips for StripX1:");
    TGLabel *labelMaskStripsY = new TGLabel(Hframe301, "Mask strips for StripY:");
    
    Hframe301->AddFrame(labelMaskStripsX0, TGLHleft);
    Hframe301->AddFrame(labelMaskStripsX1, TGLHleft);
    Hframe301->AddFrame(labelMaskStripsY, TGLHleft);
    
    TGNEmaskStripX0 = new TGNumberEntry(Hframe302, fMaskStripX0);
    TGNEmaskStripX0->Connect("ValueSet(Long_t)", "MyMainFrame", this, "AddMaskedStripX0()");
    TGNEmaskStripX1 = new TGNumberEntry(Hframe302, fMaskStripX1);
    TGNEmaskStripX1->Connect("ValueSet(Long_t)", "MyMainFrame", this, "AddMaskedStripX1()");
    TGNEmaskStripY = new TGNumberEntry(Hframe302, fMaskStripY);
    TGNEmaskStripY->Connect("ValueSet(Long_t)", "MyMainFrame", this, "AddMaskedStripY()");
    
    Hframe302->AddFrame(TGNEmaskStripX0, TGLHleft);
    Hframe302->AddFrame(TGNEmaskStripX1, TGLHleft);
    Hframe302->AddFrame(TGNEmaskStripY, TGLHleft);
    
    fListBoxStripX0 = new TGListBox(Hframe303, 0);
    fListBoxStripX0->Resize(150, 100);
    fListBoxStripX0->Connect("DoubleClicked(Int_t, Int_t)", "MyMainFrame", this, "HandleKeyPressListBox(Int_t, Int_t)");
    fListBoxStripX1 = new TGListBox(Hframe303, 1);
    fListBoxStripX1->Resize(150, 100);
    fListBoxStripX1->Connect("DoubleClicked(Int_t, Int_t)", "MyMainFrame", this, "HandleKeyPressListBox(Int_t, Int_t)");
    fListBoxStripY = new TGListBox(Hframe303, 2);
    fListBoxStripY->Resize(150, 100);
    fListBoxStripY->Connect("DoubleClicked(Int_t, Int_t)", "MyMainFrame", this, "HandleKeyPressListBox(Int_t, Int_t)");
    
    Hframe303->AddFrame(fListBoxStripX0, TGLHleft);
    Hframe303->AddFrame(fListBoxStripX1, TGLHleft);
    Hframe303->AddFrame(fListBoxStripY, TGLHleft);
    
    TGLabel *labelDoubleClick = new TGLabel(Hframe303, "Double click on the item in the list to remove it");
    
    Hframe303->AddFrame(labelDoubleClick, TGLHleft);

    TGLabel *labelSpreadCut = new TGLabel(Hframe31, "Spread cut value: ");
    TGNEspreadCut = new TGNumberEntry(Hframe31, fSpreadCut);
    TGNEspreadCut->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateSpreadCut()");

    Hframe31->AddFrame(labelSpreadCut, TGLHleft);
    Hframe31->AddFrame(TGNEspreadCut, TGLHleft);

    TGLabel *labelMinValidIntegral = new TGLabel(Hframe4, "Minimum cluster integral (noise cluster exclusion threshold, ADCc): ");
    TGNEminValidIntegral = new TGNumberEntry(Hframe4, fMinValidIntegral);
    TGNEminValidIntegral->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateMinValidIntegral()");

    Hframe4->AddFrame(labelMinValidIntegral, TGLHleft);
    Hframe4->AddFrame(TGNEminValidIntegral, TGLHleft);

    // TGLabel *labelNegativeSignals= new TGLabel(Hframe5, "Negative signals (debug mode): ");
    TGCBNegativeSignals = new TGCheckButton(Hframe5, "Negative signals for StripX0 (debug mode)");
    TGCBNegativeSignals->SetDown(0);
    TGCBNegativeSignals1 = new TGCheckButton(Hframe5, "Negative signals for StripX1 (debug mode)");
    TGCBNegativeSignals1->SetDown(0);
    TGCBStripMode = new TGCheckButton(Hframe5, "Strip mode");
    TGCBStripMode->Connect("Clicked()", "MyMainFrame", this, "UpdateChanLabel()");
    TGCBStripMode->SetDown(fChanLabelMode);
    TGCBCorrectChannelNumbers = new TGCheckButton(Hframe5, "Correct channel numbers for old StripX firmware");
    TGCBCorrectChannelNumbers->SetDown(0);
    // TGNEpixelSearchParam = new TGNumberEntry(Hframe5, fPixelSearchParam);
    // TGNEpixelSearchParam->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePixelSearchParam()");

    // Hframe5->AddFrame(labelNegativeSignals, TGLHleft);
    Hframe5->AddFrame(TGCBNegativeSignals, TGLHleft);
    Hframe5->AddFrame(TGCBNegativeSignals1, TGLHleft);
    Hframe5->AddFrame(TGCBStripMode, TGLHleft);
    Hframe5->AddFrame(TGCBCorrectChannelNumbers, TGLHleft);

    TGLabel *labelPedestalsLowCut = new TGLabel(Hframe6, "Pedestals low cut value: ");
    TGNEPedestalsLowCut = new TGNumberEntry(Hframe6, fPedestalsLowCut);
    TGNEPedestalsLowCut->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePedestalsLowCut()");

    TGLabel *labelPedestalsHighCut = new TGLabel(Hframe6, "Pedestals high cut value: ");
    TGNEPedestalsHighCut = new TGNumberEntry(Hframe6, fPedestalsHighCut);
    TGNEPedestalsHighCut->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdatePedestalsHighCut()");

    Hframe6->AddFrame(labelPedestalsLowCut, TGLHleft);
    Hframe6->AddFrame(TGNEPedestalsLowCut, TGLHleft);
    Hframe6->AddFrame(labelPedestalsHighCut, TGLHleft);
    Hframe6->AddFrame(TGNEPedestalsHighCut, TGLHleft);

    TGLabel *labelRawNoiseLowCut = new TGLabel(Hframe7, "Raw noise low cut value: ");
    TGNERawNoiseLowCut = new TGNumberEntry(Hframe7, fRawNoiseLowCut);
    TGNERawNoiseLowCut->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateRawNoiseLowCut()");

    TGLabel *labelRawNoiseHighCut = new TGLabel(Hframe7, "Raw noise high cut value (X): ");
    TGNERawNoiseHighCut = new TGNumberEntry(Hframe7, fRawNoiseHighCut);
    TGNERawNoiseHighCut->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateRawNoiseHighCut()");

    TGLabel *labelRawNoiseHighCutY = new TGLabel(Hframe7, "Raw noise high cut value (Y): ");
    TGNERawNoiseHighCutY = new TGNumberEntry(Hframe7, fRawNoiseHighCutY);
    TGNERawNoiseHighCutY->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateRawNoiseHighCutY()");

    Hframe7->AddFrame(labelRawNoiseLowCut, TGLHleft);
    Hframe7->AddFrame(TGNERawNoiseLowCut, TGLHleft);
    Hframe7->AddFrame(labelRawNoiseHighCut, TGLHleft);
    Hframe7->AddFrame(TGNERawNoiseHighCut, TGLHleft);
    Hframe7->AddFrame(labelRawNoiseHighCutY, TGLHleft);
    Hframe7->AddFrame(TGNERawNoiseHighCutY, TGLHleft);

    TGLabel *labelNoiseLowCut = new TGLabel(Hframe8, "Noise low cut value: ");
    TGNENoiseLowCut = new TGNumberEntry(Hframe8, fNoiseLowCut);
    TGNENoiseLowCut->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateNoiseLowCut()");

    TGLabel *labelNoiseHighCut = new TGLabel(Hframe8, "Noise high cut value (X): ");
    TGNENoiseHighCut = new TGNumberEntry(Hframe8, fNoiseHighCut);
    TGNENoiseHighCut->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateNoiseHighCut()");

    TGLabel *labelNoiseHighCutY = new TGLabel(Hframe8, "Noise high cut value (Y): ");
    TGNENoiseHighCutY = new TGNumberEntry(Hframe8, fNoiseHighCutY);
    TGNENoiseHighCutY->Connect("ValueSet(Long_t)", "MyMainFrame", this, "UpdateNoiseHighCutY()");

    Hframe8->AddFrame(labelNoiseLowCut, TGLHleft);
    Hframe8->AddFrame(TGNENoiseLowCut, TGLHleft);
    Hframe8->AddFrame(labelNoiseHighCut, TGLHleft);
    Hframe8->AddFrame(TGNENoiseHighCut, TGLHleft);
    Hframe8->AddFrame(labelNoiseHighCutY, TGLHleft);
    Hframe8->AddFrame(TGNENoiseHighCutY, TGLHleft);

    Vframe->AddFrame(Hframe, TGLHexpandX);
    Vframe->AddFrame(Hframe2, TGLHexpandX);
    Vframe->AddFrame(Hframe3, TGLHexpandX);
    Vframe->AddFrame(Hframe301, TGLHexpandX);
    Vframe->AddFrame(Hframe302, TGLHexpandX);
    Vframe->AddFrame(Hframe303, TGLHexpandX);
    Vframe->AddFrame(Hframe31, TGLHexpandX);
    Vframe->AddFrame(Hframe4, TGLHexpandX);
    Vframe->AddFrame(Hframe5, TGLHexpandX);
    Vframe->AddFrame(Hframe6, TGLHexpandX);
    Vframe->AddFrame(Hframe7, TGLHexpandX);
    Vframe->AddFrame(Hframe8, TGLHexpandX);
    cframe->AddFrame(Vframe, TGLHleftTop);

    fCanvasNames.push_back("Settings");
    fCanvasList.push_back(0);
}

void MyMainFrame::AddLogMessage(string message)
{

    TDatime now;
    now.Set();
    string datim{now.AsSQLString()};

    message = datim + " : " + message;

    TGTVlog->AddLine(message.c_str());
}

void MyMainFrame::LinesVas(uint lad, int drawline)
{

    gPad->Update();
    Double_t xmin, ymin, xmax, ymax;
    gPad->GetRangeAxis(xmin, ymin, xmax, ymax);
    TLine *line = new TLine();
    int color = kBlue;
    line->SetLineColor(color);
    // TText *text=new TText();
    // text->SetTextAlign(21); // vert and hor. alignment: horiz centered, top
    // text->SetTextColor(kRed);
    // line->SetLineStyle(3);
    for (uint va{1}; va < (vBoards.at(lad))->getNVA() + 1; va++)
    { // fisrt and last sep. lines are not drawn
        line->SetLineStyle((va % 4 == 0) ? 1 : 3);
        // if (va == 7)
        //   color = kRed;
        line->SetLineColor(color);
        if (va * (vBoards.at(lad))->getNVACH() > xmin && va * (vBoards.at(lad))->getNVACH() < xmax)
            line->DrawLine(va * (vBoards.at(lad))->getNVACH(), ymin, va * (vBoards.at(lad))->getNVACH(), ymax);
        if ((va - 0.5) * (vBoards.at(lad))->getNVACH() > xmin && (va - 0.5) * (vBoards.at(lad))->getNVACH() <= xmax)
        {
            TText *text = new TText((va - 0.5) * (vBoards.at(lad))->getNVACH(), ymax + (ymax - ymin) * 0.01, Form("%d", va));
            text->SetTextSize(0.02);
            text->SetName(Form("textva%d", va));
            text->SetTextAlign(21); // vert and hor. alignment: horiz centered, top
            text->SetTextColor(color);
            text->Draw();
        }
    }
    if (drawline)
    {
        line->SetLineColor(kBlack);
        line->SetLineStyle(1);
        line->DrawLine(xmin, 0, xmax, 0);
    }
}

void MyMainFrame::LinesAdcs(int drawline)
{

    gPad->Update();
    Double_t xmin, ymin, xmax, ymax;
    gPad->GetRangeAxis(xmin, ymin, xmax, ymax);
    TLine *line = new TLine();
    int color = kBlue;
    line->SetLineColor(color);
    // TText *text=new TText();
    // text->SetTextAlign(21); // vert and hor. alignment: horiz centered, top
    // text->SetTextColor(kRed);
    // line->SetLineStyle(3);
    for (Int_t read = 1; read < 4; read++)
    { // fisrt and last sep. lines are not drawn
        line->SetLineStyle(3);
        if (read * 192 > xmin && read * 192 < xmax)
            line->DrawLine(read * 192, ymin, read * 192, ymax);
        if ((read - 0.5) * 192 > xmin && (read - 0.5) * 192 <= xmax)
        {
            TText *text = new TText((read - 0.5) * 192, ymax + (ymax - ymin) * 0.01, Form("%d", read));
            text->SetName(Form("textread%d", read));
            text->SetTextAlign(21); // vert and hor. alignment: horiz centered, top
            text->SetTextColor(color);
            text->Draw();
        }
    }
    if (drawline)
    {
        line->SetLineColor(kBlack);
        line->SetLineStyle(1);
        line->DrawLine(xmin, 0, xmax, 0);
    }
}

void MyMainFrame::CloseWindow()
{

    SaveConfiguration();

    gApplication->Terminate(0);
}

void MyMainFrame::DisplayThisEvent()
{

    // cout << "display this event" << endl;

    int nevent;

    if (!fDaqRun)
    {
        nevent = (int)TGNEevent->GetNumber();
    }
    else
    {
        nevent = fNevents;
    }

    if (nevent > fNevents - 1)
        nevent = fNevents - 1;

    fEventCounter = nevent;

    RefreshDisplay();
}

void MyMainFrame::InitRefreshTimer()
{

    fRefreshtimer = new TTimer(0); // refresh every 100 ms
    fRefreshtimer->Connect("Timeout()", "MyMainFrame", this, "RefreshDisplay()");
    // refreshtimer->Start();
}

void MyMainFrame::StartEventTrigger()
{

    fRefreshtimer->Start();
}

void MyMainFrame::StopEventTrigger()
{

    fRefreshtimer->Stop();
}

void MyMainFrame::UpdateSuperImpose()
{

    // fClearDisplay=fSuperImposed;

    ClearSuperimposedGraphs(fAdcCanvas, fEventCounter, fSuperImposed);
    ClearSuperimposedGraphs(fPedSubtCanvas, fEventCounter, fSuperImposed);
    fSuperImposed = TGNEsuperimpose->GetNumber();
}

// void MyMainFrame::UpdateEventFilter(){
//
//  int selmode{TGCBEventSel->GetSelected()};
//     cout << " selmode = " << selmode << endl;
//
// }

void MyMainFrame::RefreshDisplay()
{

    if (fEventCounter == fNevents)
        return;

    int selmode{TGCBEventSel->GetSelected()};
    // cout << " selmode = " << selmode << endl;

    if ((selmode == 1 and (fEventCounter % 2) == 0)    // we accept odd indices
        or (selmode == 2 and (fEventCounter % 2) == 1) // we accept even indices
        or selmode == 0)                               // we accept everything
    {
        TGNEevent->SetNumber(fEventCounter);
        if (fEventCounter == fNevents - 1)
            TGNEevent->SetNumber(fEventCounter);
        if (!fDaqRun)
        {
            fTree->GetEntry(fEventCounter);
        }

        BuildGraph();
        int current = fDataTab->GetCurrent();
        if (current == 0)
            UpdateAdcDisplay(fEventCounter);
        if (current == 1)
            UpdatePedSubtDisplay(fEventCounter);
        if (current == 2)
            UpdatePedCnSubtDisplay(fEventCounter);

        //  if (current==4) UpdateAdcPedCnSubtDisplay(fEventCounter);
        if (current == 3)
            UpdateSovernDisplay();
        if (current == 4)
            UpdateOccupancyDisplay();
        if (current == 5)
            UpdateCogDisplay();
        if (current == 6)
            UpdateLensDisplay();
        if (current == 7)
            UpdateLenVsCogDisplay();
        if (current == 8)
            UpdateIntsDisplay(fEventCounter);
        if (current == 9)
            UpdateIntVsCogDisplay();
        if (current == 12)
            UpdateFftDisplay();
    }
    fEventCounter++;
    if (!fDaqRun)
    {
        if (fEventCounter == fNevents)
        {
            StopEventTrigger();
            SetRunButtons(1, 1, 0, 0);
            AddLogMessage("Run stopped");
        }
    }
}

void MyMainFrame::UpdateMinValidIntegral()
{

    double newval{TGNEminValidIntegral->GetNumber()};

    if (newval > 0)
        fMinValidIntegral = newval;

    AddLogMessage(Form("Minimum cluster integral value has been set to %f", fMinValidIntegral));
}

void MyMainFrame::UpdateCluHighThresh()
{

    double newval{TGNEcluHighThresh->GetNumber()};

    if (newval > 0)
        fCluHighThresh = newval;

    AddLogMessage(Form("Cluster identifcation high threshold X has been set to %f", fCluHighThresh));
}

void MyMainFrame::UpdateCluHighThreshY()
{

    double newval{TGNEcluHighThreshY->GetNumber()};

    if (newval > 0)
        fCluHighThreshY = newval;

    AddLogMessage(Form("Cluster identifcation high threshold Y has been set to %f", fCluHighThresh));
}

void MyMainFrame::UpdateCluLowThresh()
{

    double newval{TGNEcluLowThresh->GetNumber()};

    if (newval > 0)
        fCluLowThresh = newval;

    AddLogMessage(Form("Cluster identifcation low threshold has been set to %f", fCluLowThresh));
}

void MyMainFrame::UpdateSpreadCut()
{

    double newval{TGNEspreadCut->GetNumber()};

    if (newval > 0)
        fSpreadCut = newval;

    AddLogMessage(Form("spread cut has been set to %f", fSpreadCut));
}

void MyMainFrame::UpdatePedestalsHighCut()
{

    double newval{TGNEPedestalsHighCut->GetNumber()};

    if (newval > 0)
        fPedestalsHighCut = newval;

    AddLogMessage(Form("Pedestals high cut has been set to %f", fPedestalsHighCut));
}

void MyMainFrame::UpdatePedestalsLowCut()
{

    double newval{TGNEPedestalsLowCut->GetNumber()};

    if (newval > 0)
        fPedestalsLowCut = newval;

    AddLogMessage(Form("Pedestals low cut has been set to %f", fPedestalsLowCut));
}

void MyMainFrame::UpdateRawNoiseLowCut()
{

    double newval{TGNERawNoiseLowCut->GetNumber()};

    if (newval > 0)
        fRawNoiseLowCut = newval;

    AddLogMessage(Form("Raw noise low cut has been set to %f", fRawNoiseLowCut));
}

void MyMainFrame::UpdateRawNoiseHighCut()
{

    double newval{TGNERawNoiseHighCut->GetNumber()};

    if (newval > 0)
        fRawNoiseHighCut = newval;

    AddLogMessage(Form("Raw noise high cut X has been set to %f", fRawNoiseHighCut));
}

void MyMainFrame::UpdateRawNoiseHighCutY()
{

    double newval{TGNERawNoiseHighCutY->GetNumber()};

    if (newval > 0)
        fRawNoiseHighCutY = newval;

    AddLogMessage(Form("Raw noise high cut Y has been set to %f", fRawNoiseHighCutY));
}

void MyMainFrame::UpdateNoiseLowCut()
{

    double newval{TGNENoiseLowCut->GetNumber()};

    if (newval > 0)
        fNoiseLowCut = newval;

    AddLogMessage(Form("Noise low cut has been set to %f", fNoiseLowCut));
}

void MyMainFrame::UpdateNoiseHighCut()
{

    double newval{TGNENoiseHighCut->GetNumber()};

    if (newval > 0)
        fNoiseHighCut = newval;

    AddLogMessage(Form("Noise high cut has been set to %f", fNoiseHighCut));
}

void MyMainFrame::UpdateNoiseHighCutY()
{

    double newval{TGNENoiseHighCutY->GetNumber()};

    if (newval > 0)
        fNoiseHighCutY = newval;

    AddLogMessage(Form("Noise high cut Y has been set to %f", fNoiseHighCutY));
}

void MyMainFrame::AddMaskedStripX0() {
    
    ULong_t newval{static_cast<ULong_t>(TGNEmaskStripX0->GetNumber())};
    
    if (!fStatus.empty()) {
        if (fStatus.at(newval) == 0) {
            SetChannelStatus(newval, 64);
            TString item;
            item.Form("%d", newval);
            fListBoxStripX0->AddEntry(item, newval);
            fListBoxStripX0->MapSubwindows();
            fListBoxStripX0->Layout();
        }
    }
}

void MyMainFrame::AddMaskedStripX1() {
    
    ULong_t newval{static_cast<ULong_t>(TGNEmaskStripX1->GetNumber())};
    
    if (!fStatus.empty() && fNstripX > 1) {
        if (fStatus.at(newval + 2048) == 0) {
            SetChannelStatus(newval + 2048, 64);
            TString item;
            item.Form("%d", newval);
            fListBoxStripX1->AddEntry(item, newval);
            fListBoxStripX1->MapSubwindows();
            fListBoxStripX1->Layout();
        }
    }
}

void MyMainFrame::AddMaskedStripY() {
    
    ULong_t newval{static_cast<ULong_t>(TGNEmaskStripY->GetNumber())};
    
    if (!fStatus.empty() && fNstripY > 0) {
        if (fStatus.at(fNTOTCH - 128 + newval) == 0) {
            SetChannelStatus(fNTOTCH - 128 + newval, 64);
            TString item;
            item.Form("%d", newval);
            fListBoxStripY->AddEntry(item, newval);
            fListBoxStripY->MapSubwindows();
            fListBoxStripY->Layout();
        }
    }
}

void MyMainFrame::HandleKeyPressListBox(Int_t wid, Int_t id) {
    switch (wid) {
        case 0:
            if (fListBoxStripX0->GetSelected() != -1) {
                SetChannelStatus(id, 0);
                fListBoxStripX0->RemoveEntry(id);
                fListBoxStripX0->Layout();
            }
            break;
        case 1:
            if (fListBoxStripX1->GetSelected() != -1) {
                SetChannelStatus(id + 2048, 0);
                fListBoxStripX1->RemoveEntry(id);
                fListBoxStripX1->Layout();
            }
        case 2:
            if (fListBoxStripY->GetSelected() != -1) {
                SetChannelStatus(fNTOTCH - 128 + id, 0);
                fListBoxStripY->RemoveEntry(id);
                fListBoxStripY->Layout();
            }
        default:
            break;
    }
}

void MyMainFrame::UpdatePixelSearchParam()
{

    double newval{TGNEpixelSearchParam->GetNumber()};

    if (newval > 0)
        fPixelSearchParam = newval;

    AddLogMessage(Form("Pixel search parameter has been set to %f", fPixelSearchParam));
}

void MyMainFrame::UpdatePixelSearchRebin()
{

    double newval{TGNEpixelSearchRebin->GetNumber()};

    if (newval > 0)
        fPixelSearchRebin = newval;

    AddLogMessage(Form("Pixel search rebin factor has been set to %f", fPixelSearchRebin));
}

void MyMainFrame::UpdateAdcAxes(bool isy)
{
    UpdateAxes(isy, TGNEadcax, fAdcAxis);
    UpdateAdcDisplay(fEventCounter, 1);
}

void MyMainFrame::UpdateSovernAxes(bool isy)
{
    UpdateAxes(isy, TGNEsovernax, fSovernAxis);
    DisplaySovern();
}

void MyMainFrame::UpdateOccupancyAxes(bool isy)
{
    UpdateAxes(isy, TGNEoccupancyax, fOccupancyAxis);
    DisplayOccupancy();
}

void MyMainFrame::UpdateCogAxes(bool isy)
{
    UpdateAxes(isy, TGNEcogax, fCogAxis);
    DisplayCog();
}

void MyMainFrame::UpdateLensAxes(bool isy)
{
    UpdateAxes(isy, TGNElensax, fLensAxis);
    DisplayLens();
}

void MyMainFrame::UpdateLenVsCogAxes(bool isy)
{
    UpdateAxes(isy, TGNElenvscogax, fLenVsCogAxis);
    DisplayLenVsCog();
}

void MyMainFrame::UpdateIntVsCogAxes(bool isy)
{
    UpdateAxes(isy, TGNEintvscogax, fIntVsCogAxis);
    DisplayIntVsCog();
}

void MyMainFrame::UpdateIntsAxes(bool isy)
{
    UpdateAxes(isy, TGNEintsax, fIntsAxis);
    DisplayInts();
}

void MyMainFrame::UpdateNclusAxes(bool isy)
{
    UpdateAxes(isy, TGNEnclusax, fNclusAxis);
    DisplayNclus();
}

void MyMainFrame::UpdateFftAxes(bool isy)
{
    UpdateAxes(isy, TGNEfftax, fFftAxis);
    UpdateFftDisplay(1);
}

void MyMainFrame::UpdatePedSubtAxes(bool isy)
{
    UpdateAxes(isy, TGNEpedsubtax, fPedSubtAxis);
    UpdatePedSubtDisplay(fEventCounter, 1);
}

void MyMainFrame::UpdatePedCnSubtAxes(bool isy)
{
    UpdateAxes(isy, TGNEpedcnsubtax, fPedCnSubtAxis);
    UpdatePedCnSubtDisplay(fEventCounter, 1);
}

void MyMainFrame::UpdatePedAxes(bool isy)
{
    UpdateAxes(isy, TGNEpedax, fPedAxis);
    DisplayPedestals();
}

void MyMainFrame::UpdatePedCompAxes(bool isy)
{
    UpdateAxes(isy, TGNEpedcompax, fPedCompAxis);
    DisplayPedCompar();
}

void MyMainFrame::UpdateSrawAxes(bool isy)
{
    UpdateAxes(isy, TGNEsrawax, fSrawAxis);
    DisplayRawSigmas();
}

void MyMainFrame::UpdateSigmaAxes(bool isy)
{
    UpdateAxes(isy, TGNEsigmaax, fSigmaAxis);
    DisplaySigmas();
}

void MyMainFrame::UpdateSigmaCompAxes(bool isy)
{
    UpdateAxes(isy, TGNEsigmacompax, fSigmaCompAxis);
    DisplaySigmaCompar();
}

void MyMainFrame::UpdateExcludeEvents()
{

    double exclude{TGNEexcludeevents->GetNumber()};
    if (exclude < 0)
    {
        exclude = 0;
        TGNEexcludeevents->SetNumber(exclude);
    }

    SetExcludeEvents(exclude);
}

void MyMainFrame::UpdateCNcutval()
{

    double cut{TGNEcncutval->GetNumber()};
    if (cut < 0)
    {
        cut = -cut;
        TGNEcncutval->SetNumber(cut);
    }

    SetCNcut(cut);
}

void MyMainFrame::UpdateCNYcutval()
{

    double cut{TGNEcnycutval->GetNumber()};
    if (cut < 0)
    {
        cut = -cut;
        TGNEcnycutval->SetNumber(cut);
    }

    SetCNYcut(cut);
}

string MyMainFrame::GetDateTimeString()
{

    TDatime now;
    now.Set();
    int date, time;
    TDatime::GetDateTime(now.Get(), date, time);
    string sdate = to_string(date);
    string stime = to_string(time);
    stime = string(6 - stime.length(), '0') + stime;
    string datim = sdate + "-" + stime;

    return datim;
}

void MyMainFrame::SaveCalibrations()
{

    string datim{GetDateTimeString()};
    string gen{fFilename.substr(0, fFilename.length() - 5)};

    uint current_ch{0};
    uint current_lad{0};

    for (uint ilad{0}; ilad < NLAD; ilad++)
    {
        // string fname{fCalPath + "/" + Form("Ladder_%d", ilad)};
        // fname += "_" + datim + ".cal";

        string fname{gen + Form("_Ladder_%d.cal", ilad)};

        ofstream calibfil(fname);
        if (!calibfil.is_open())
        {
            AddLogMessage("failed to create file " + fname);
            continue;
        }
        for (uint channel{0}; channel < (vBoards.at(ilad))->getNLCH(); channel++)
        {
            current_ch = channel;
            current_lad = ilad;
            while (current_lad > 0)
            {
                current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                current_lad--;
            }
            uint glchannel{current_ch};
            uint va{channel / (vBoards.at(ilad))->getNVACH()};
            uint vacha{channel % (vBoards.at(ilad))->getNVACH()};
            string line{Form("%4d, %4d, %4d, %10.3f, %10.3f, %10.3f, 0.0, %4d", channel, va, vacha, fPedestals.at(glchannel), fRawSigmas.at(glchannel), fSigmas.at(glchannel), fStatus.at(glchannel))};
            calibfil << line << endl;
        }

        calibfil.close();
        AddLogMessage("Calibration file " + fname + " written");
    }
}

void MyMainFrame::SetCNcut(double cut)
{

    if (cut < 0)
        cut = -cut;

    CNcutval = cut;
}

void MyMainFrame::SetCNYcut(double cut)
{

    if (cut < 0)
        cut = -cut;

    CNYcutval = cut;
}

void MyMainFrame::SetExcludeEvents(double val)
{

    if (val < 0)
        val = 0;

    ExcludeEvents = val;
}

void MyMainFrame::InitRunningCN()
{

    // cout << "welcome to initrunningcn" << endl;

    vdouble().swap(fRunningCN);
    fRunningCN.assign(fNVA, 0);

    vdouble dummy;
    dummy.assign(fNVA, 0);

    vvdouble().swap(fBufferRunningCN);
    for (int i{0}; i < NRUNCN; i++)
        fBufferRunningCN.push_back(dummy);

    vdouble().swap(dummy);
}

void MyMainFrame::DoRunningCN()
{

    // cout << "welcome to dorunningcn !" << endl;

    vdouble().swap(fRunningCN);
    fRunningCN.assign(fNVA, 0);

    if (fCN.size() == 0)
        return;
    // for (size_t va{0}; va<fNVA ; va++)  cout << fCN.at(va) <<"  " ;
    // cout << endl;

    int pos{fEventCounter % NRUNCN};
    // cout << "cn counter: " << pos << endl;
    fBufferRunningCN.at(pos) = fCN;

    for (uint va{0}; va < fNVA; va++)
    {
        for (size_t buf{0}; buf < fBufferRunningCN.size(); buf++)
            fRunningCN.at(va) += fBufferRunningCN.at(buf).at(va);
        fRunningCN.at(va) /= fBufferRunningCN.size();
        // cout << "va= " << va << "  fRunningCN = " << fRunningCN.at(va) << endl;
    }
}

bool MyMainFrame::ComputeCNoneVA(uint va, uint lad, double fMaj, int mask_channelOK)
{

    // cout << "computeCN, VA=" << va << endl;
    uint chref{1};
    bool converge{false};
    int Nstall{0};

    double aMaj = 0.0;
    uint current_ch{0};
    uint current_lad{lad};

    while (!converge && chref < (vBoards.at(lad))->getNVACH())
    {
        fNst.at(va) = 0;
        fCN.at(va) = 0;
        Nstall = 0;
        current_lad = lad;
        current_ch = va;
        while (current_lad > 0)
        {
            current_ch -= (vBoards.at(current_lad - 1))->getNVA();
            current_lad--;
        }
        current_ch *= (vBoards.at(lad))->getNVACH();
        current_lad = lad;
        while (current_lad > 0)
        {
            current_ch += (vBoards.at(current_lad - 1))->getNLCH();
            current_lad--;
        }
        if (fStatus.at((current_ch + chref) & mask_channelOK))
        {
            converge = 0;
            chref++;
        }
        else
        {
            double aRef = fReadOut.at(current_ch + chref);
            for (uint ch{0}; ch < (vBoards.at(lad))->getNVACH(); ch++)
            {
                // CLRBIT(fStatus[va][ch],8);
                if (!(fStatus.at((current_ch + ch) & mask_channelOK)))
                {
                    Nstall++;
                    // if ((fabs(fReadOut.at(va*64+ch)-aRef)<3.0*fRawSigmas.at(va*NVACH+chref))) {
                    if (fabs(fReadOut.at(current_ch + ch) - aRef) < CNcutval)
                    {
                        fNst.at(va)++;
                        fCN.at(va) += fReadOut.at(current_ch + ch);
                    }
                }
            }
            aMaj = (double)fNst.at(va) / (double)Nstall;
            // cout << Form("va = %d, chref = %d, aMaj = %f", va, chref, aMaj) << endl;
            if (aMaj > fMaj)
            {
                converge = true;
                fCN.at(va) /= fNst.at(va);
            }
            else
            {
                chref++;
                fCN.at(va) = 0.0;
                Nstall = 0;
                fNst.at(va) = 0;
            }
        }
    }

    // int bin = GetBin(kHISTO_CN,fCN[va]);
    // histo->fhCN[va][bin] += 1;
    //     printf("CN2 info: va=%02d first=%2d Nst=%2d All=%2d Maj=%6.4f\n"
    //     	   ,va,chref,fNst[va],Nstall,aMaj);

    return converge;
}

int MyMainFrame::ComputeCN(double fMaj, int mask_channelOK)
{ // based on ams TDR test system program

    vdouble().swap(fCN); // common noise vector
    vint().swap(fNst);   // ??
    fCN.assign(fNVA, 0);
    fNst.assign(fNVA, 0);

    // cout << " ComputeCN: size of vector = " << fCN.size() << endl;
    // int mask_channelOK = 511; // 511=111111111  508=111111100   (old 91=1011011)

    // int mask_channelOK{0xFF};
    //  double fMaj {0.6};

    int mainconverge{0};
    uint current_va{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; ++lad)
        for (uint va{0}; va < (vBoards.at(lad))->getNVA(); va++)
        {
            current_va = va;
            current_lad = lad;
            while (current_lad > 0)
            {
                current_va += (vBoards.at(current_lad - 1))->getNVA();
                current_lad--;
            }
            bool success{ComputeCNoneVA(current_va, lad, fMaj, mask_channelOK)};
            if (!success)
            {
                success = ComputeCNoneVA(current_va, lad, fMaj / 2, mask_channelOK);
                if (!success)
                    success = ComputeCNoneVA(current_va, lad, fMaj, 0);
            }
            mainconverge |= success;

            /*
            // cout << "computeCN, VA=" << va << endl;
            uint chref = 1;
            int converge = 0;
            int Nstall = 0;

            double aMaj = 0.0;

            while (!converge && chref < fNVACH)
            {
                fNst.at(va) = 0;
                fCN.at(va) = 0;
                Nstall = 0;
                if (fStatus.at(va * fNVACH + chref) & mask_channelOK)
                {
                    converge = 0;
                    chref++;
                }
                else
                {
                    double aRef = fReadOut.at(va * fNVACH + chref);
                    for (uint ch{0}; ch < fNVACH; ch++)
                    {
                        // CLRBIT(fStatus[va][ch],8);
                        if (!(fStatus.at(va * fNVACH + ch) & mask_channelOK))
                        {
                            Nstall++;
                            // if ((fabs(fReadOut.at(va*64+ch)-aRef)<3.0*fRawSigmas.at(va*NVACH+chref))) {
                            if ((fabs(fReadOut.at(va * fNVACH + ch) - aRef) < CNcutval))
                            {
                                fNst.at(va)++;
                                fCN.at(va) += fReadOut.at(va * fNVACH + ch);
                            }
                        }
                    }
                    aMaj = (double)fNst.at(va) / (double)Nstall;
                    // cout << Form("va = %d, chref = %d, aMaj = %f", va, chref, aMaj) << endl;
                    if (aMaj > fMaj)
                    {
                        converge = 1;
                        fCN.at(va) /= fNst.at(va);
                    }
                    else
                    {
                        chref++;
                        fCN.at(va) = 0.0;
                        Nstall = 0;
                        fNst.at(va) = 0;
                    }
                }
            }

            // int bin = GetBin(kHISTO_CN,fCN[va]);
            // histo->fhCN[va][bin] += 1;
            //     printf("CN2 info: va=%02d first=%2d Nst=%2d All=%2d Maj=%6.4f\n"
            //     	   ,va,chref,fNst[va],Nstall,aMaj);
            if (!converge)
                AddLogMessage(Form("event %d : , VA = %d, Sorry no CN convergence", fEventCounter, va));
            mainconverge &= converge;

            */
        }
    // cout << " size of CN = " << fCN.size() << endl;
    // cout << Form("cn1 = %4.2f, cn2 = %4.2f",fCN.at(0), fCN.at(1)) << endl;

    // AddLogMessage(Form("cn1 = %4.2, cn2 = %4.2",fCN.at(0), fCN.at(1)));

    return mainconverge;
}

void MyMainFrame::SubtractCN()
{

    uint current_va{0};
    uint current_ch{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; ++lad)
        if (vBoards.at(lad)->getNLCH() > 128)
        {
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {

                current_ch = ch;
                current_lad = lad;
                current_va = ch / (vBoards.at(lad))->getNVACH();
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_va += (vBoards.at(current_lad - 1))->getNVA();
                    current_lad--;
                }
                fReadOut.at(current_ch) -= fCN.at(current_va);
            }
        }
        else
        {
            TGraph *gr{new TGraph()};
            TGraph *grcnsub{new TGraph()};
            TF1 *funcn{nullptr};
            if (TGCBCNPoly4Fit->IsDown()) {
                funcn = new TF1("funcn", "pol4", 0, 127);
            }
            else {
                funcn = new TF1("funcn", CNFunY, 0, 127, 2);
                funcn->SetParameter(0, 1. / 51.2325);
                funcn->SetParameter(1, 0.);
            }
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                grcnsub->AddPoint(ch, fReadOut.at(current_ch));
                if (fStatus.at(current_ch) == 0)
                    gr->AddPoint(ch, fReadOut.at(current_ch)); // we take only the good channels, hoping they are not too few
                    
            }
            gr->Fit(funcn, "RQN");
            gr->Delete();
            gr = new TGraph();
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++) {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                if (fStatus.at(current_ch) == 0 && fabs(fReadOut.at(current_ch) - funcn->Eval(ch)) < CNYcutval)
                    gr->AddPoint(ch, fReadOut.at(current_ch)); // we take only the good channels, hoping they are not too few
            }
            gr->Fit(funcn, "RQN");
            for (int i{0}; i < grcnsub->GetN(); i++)
            {
                grcnsub->SetPoint(i, grcnsub->GetX()[i], grcnsub->GetY()[i] - funcn->Eval(grcnsub->GetX()[i]));
            }
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                fReadOut.at(current_ch) = grcnsub->GetY()[ch];
            }
            // we cleanupo everything
            delete gr;
            delete grcnsub;
            delete funcn;
        }
}

void MyMainFrame::InitRedCalibrations()
{

    vdouble().swap(fRedPedestals);
    fRedPedestals.assign(fNTOTCH, 0);
    vdouble().swap(fRedSigmas);
    fRedSigmas.assign(fNTOTCH, 0);
    vdouble().swap(fRedRawSigmas);
    fRedRawSigmas.assign(fNTOTCH, 0);
    vint().swap(fRedStatus);
    fRedStatus.assign(fNTOTCH, 0);

    for (unsigned int lad{0}; lad < NLAD; lad++)
        LoadRedCalibrations(lad);
}

void MyMainFrame::LoadRedCalibrations(uint lad)
{

    if (lad >= NLAD)
        return;

    if (fvRedCalFile.at(lad) == "")
        return;

    string fullpath = fCalPath + "/" + fvRedCalFile.at(lad);

    ifstream calFile(fullpath);

    if (!calFile.is_open())
    {
        AddLogMessage("Calibration file " + fullpath + " not found");
        return;
    }

    int channel;
    string line, copy, left, right;
    size_t found;
    vector<string> info;

    int offset{0};
    uint current_lad = lad;
    while (current_lad > 0)
    {
        offset += (vBoards.at(current_lad - 1))->getNLCH();
        current_lad--;
    }

    while (1)
    {
        if (!(getline(calFile, line)))
            break;
        copy = line;
        info.clear();
        while (1)
        {
            found = copy.find_first_of(",");
            if (found == std::string::npos)
                break;
            left = copy.substr(0, found);
            right = copy.substr(found + 1);
            info.push_back(left);
            copy = right;
        }

        channel = stoi(info.at(0)) + offset;
        fRedPedestals.at(channel) = stod(info.at(3));
        fRedRawSigmas.at(channel) = stod(info.at(4));
        fRedSigmas.at(channel) = stod(info.at(5));
        fRedStatus.at(channel) = stoi(info.at(7));
    }

    AddLogMessage(Form("Reduction calibration of Detector %d have been loaded", lad));
}

void MyMainFrame::InitRefCalibrations()
{

    fRefPedAttenuation = 0.5;
    fRefPedBaseline = 400.0;

    vdouble().swap(fRefPedestals);
    fRefPedestals.assign(fNTOTCH, 0);
    vdouble().swap(fRefSigmas);
    fRefSigmas.assign(fNTOTCH, 0);
    vdouble().swap(fRefRawSigmas);
    fRefRawSigmas.assign(fNTOTCH, 0);
    vint().swap(fRefStatus);
    fRefStatus.assign(fNTOTCH, 0);

    for (unsigned int lad{0}; lad < NLAD; lad++)
        LoadRefCalibrations(lad);

    // ConvertRefCalibrations();
}

void MyMainFrame::LoadRefCalibrations(uint lad)
{

    if (lad >= NLAD)
        return;

    if (fvRefCalFile.at(lad) == "")
        return;

    string fullpath = fCalRefPath + "/" + fvRefCalFile.at(lad);

    ifstream calFile(fullpath);

    if (!calFile.is_open())
    {
        AddLogMessage("Calibration file " + fullpath + " not found");
        return;
    }

    int channel;
    string line, copy, left, right;
    size_t found;
    vector<string> info;

    int offset{0};
    uint current_lad = lad;
    while (current_lad > 0)
    {
        offset += (vBoards.at(current_lad - 1))->getNLCH();
        current_lad--;
    }

    while (1)
    {
        if (!(getline(calFile, line)))
            break;
        copy = line;
        info.clear();
        while (1)
        {
            found = copy.find_first_of(",");
            if (found == std::string::npos)
                break;
            left = copy.substr(0, found);
            right = copy.substr(found + 1);
            info.push_back(left);
            copy = right;
        }

        channel = stoi(info.at(0)) + offset;
        fRefPedestals.at(channel) = stod(info.at(3));
        fRefRawSigmas.at(channel) = stod(info.at(4));
        fRefSigmas.at(channel) = stod(info.at(5));
    }
    AddLogMessage(Form("Ref calibration of Detector %d have been loaded", lad));
}

void MyMainFrame::ConvertRefCalibrations()
{

    // convert the calibrations made by a TDR: inverts the K-side signals, and attenuate the spread by a given ratio.

    vector<double> meanped; // mean value of ped for each VA
    meanped.clear();
    meanped.assign(NLAD * fNVA, 0.0);

    for (uint va{0}; va < fNVA * NLAD; va++)
    {

        for (uint ch{0}; ch < fNVACH; ch++)
        {
            uint channel = va * fNVACH + ch;
            meanped.at(va) += fRefPedestals.at(channel);
        }
        meanped.at(va) /= (fNVACH * 1.0);
        // cout << Form("va %3d, meanped: %5.1f",va,meanped.at(va)) << endl;
    }

    for (uint ch{0}; ch < fNTOTCH; ch++)
    {

        int va = ch / 64;
        // int vach=ch%64;
        int ladva = va % fNVA; // we need to know if we are dealing with S or K Vas

        double delped = fRefPedestals.at(ch) - meanped.at(va);
        delped *= fRefPedAttenuation;
        // fRefPedestals.at(ch)=meanped.at(va)+(ladva>5)?-delped:delped; // K-side ? then we must invert the ped signal
        if (ladva < 6)
            fRefPedestals.at(ch) = fRefPedBaseline + delped;
        else
            fRefPedestals.at(ch) = fRefPedBaseline - delped;
    }

    AddLogMessage("Reference calibrations converted");

    vdouble().swap(meanped);
}

void MyMainFrame::SaveImage()
{

    int current = fDataTab->GetCurrent();

    string canname = fCanvasNames.at(current);
    TCanvas *c = fCanvasList.at(current);
    if (c == 0)
        return;

    string filename = fImagePath + "/" + canname + "_" + GetDateTimeString() + ".png";
    string filename_C = fImagePath + "/" + canname + "_" + GetDateTimeString() + ".C";

    c->Print(filename.c_str());
    c->Print(filename_C.c_str());

    AddLogMessage("Image " + filename + " saved.");
    AddLogMessage("Image " + filename_C + " saved.");
}

void MyMainFrame::UpdateFrameSize()
{
    USB_BUFFER_SIZE = TGNFrameSize->GetNumber();
    if (fDaqRun)
        StopScope();
    Reconnect();
}

void MyMainFrame::UpdateIPaddress()
{

    fIPaddress = fTeIPaddress->GetText();
    if (fDaqRun)
        StopScope();
    CleanupCommunication();
    delete fUnigeGpio;
    fUnigeGpio = nullptr;
    InitCommunication();
}

void MyMainFrame::UpdatePort()
{
    fPortGPIO = TGNPort->GetNumber();
    if (fDaqRun)
        StopScope();
    CleanupCommunication();
    delete fUnigeGpio;
    fUnigeGpio = nullptr;
    InitCommunication();
}

void MyMainFrame::UpdateIPaddressData()
{

    fIPaddressData = fTeIPaddressData->GetText();
    if (fDaqRun)
        StopScope();
    CleanupBuffer();
    delete fFitEvent;
    fFitEvent = nullptr;
    InitBuffer();
}

void MyMainFrame::UpdatePortData()
{
    fPortData = TGNPortData->GetNumber();
    if (fDaqRun)
        StopScope();
    CleanupBuffer();
    delete fFitEvent;
    fFitEvent = nullptr;
    InitBuffer();
}

void MyMainFrame::UpdateBufferSize()
{

    fNBufferEvents = TGNBufferSize->GetNumber();
    if (fDaqRun)
        StopScope();
    fFitEvent->SetEventBufferSize(fNBufferEvents);
    fFitEvent->SetPANEnabled(true);
}

void MyMainFrame::UpdatePedestalBuffer()
{

    if (fDaqRun)
        StopScope();
    NPEDEVENTS = TGCalNBufferSize->GetNumber();
    NSRAWEVENTS = NPEDEVENTS;
    NSIGEVENTS = NPEDEVENTS;
}

void MyMainFrame::ReCalibrate()
{

    if (!fDaqRun)
    {
        StopScope();

        int event{(int)TGNEevent->GetNumber()};

        ComputePedestals(event);
        bool cp2res{false};
        cp2res = ComputePedestals2(0, 100);
        if (cp2res)
        {
            cout << "ComputePedestals2(0, 100) : ok" << endl;
            cp2res = ComputePedestals2(0, 50);
            if (cp2res)
            {
                cout << "ComputePedestals2(0, 50) : ok" << endl;
                cp2res = ComputePedestals2(0, 50);
                if (cp2res)
                    cout << "ComputePedestals2(0, 25) : ok" << endl;
            }
        }
        ComputeRawSigmas(event);
        ComputeSigmas(event, 0xFF);
        SaveCalibrations();

        DisplayPedestals();
        DisplayRawSigmas();
        DisplaySigmas();
        DisplayPedCompar();
        DisplaySigmaCompar();

        ContinueScope();
    }
    else
        fDoRecalibration = true;
}

void MyMainFrame::DynamicPedestals()
{
    uint current_ch{0};
    uint current_va{0};
    uint current_lad{0};

    for (uint lad{0}; lad < NLAD; ++lad)
        for (uint va{0}; va < (vBoards.at(lad))->getNVA(); va++)
        {
            int count{0};
            for (uint ch{0}; ch < (vBoards.at(lad))->getNVACH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                uint chan{current_ch + va * (vBoards.at(lad))->getNVACH()};
                double data{fReadOut.at(chan)};
                if (data > 0)
                {
                    fPedestals.at(chan) += 0.25;
                    count++;
                }
                else if (data < 0)
                {
                    fPedestals.at(chan) -= 0.25;
                    count--;
                }
            }
            double mean_drift{(count * 0.25) / ((vBoards.at(lad))->getNVACH())};
            // and now we subtract the mean drift
            for (uint ch{0}; ch < (vBoards.at(lad))->getNVACH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                fPedestals.at(current_ch + va * (vBoards.at(lad))->getNVACH()) -= mean_drift; // + fRunningCN.at(va);
            }

            if (fEventCounter > 0 && fEventCounter % 1000 == 0)
                for (uint ch{0}; ch < (vBoards.at(lad))->getNVACH(); ch++)
                {
                    current_ch = ch;
                    current_va = va;
                    current_lad = lad;
                    while (current_lad > 0)
                    {
                        current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                        current_va += (vBoards.at(current_lad - 1))->getNVA();
                        current_lad--;
                    }
                    fPedestals.at(current_ch + va * (vBoards.at(lad))->getNVACH()) += fRunningCN.at(current_va); // drift correction every 1000 events
                }
        }
}

void MyMainFrame::FreeCluster(cluster **aCluster)
{

    // cout << "welcome do freecluster" << endl;

    cluster *actual, *save;
    actual = *aCluster;
    *aCluster = 0;
    while (actual != 0)
    {
        // cout << "freecluster, inside loop" << endl;
        save = actual->next;
        FreeChannel(&actual->channel);
        delete actual;
        actual = save;
    }
}

void MyMainFrame::FreeChannel(channel **aChannel)
{
    // cout << "welcome do freechannel" << endl;

    channel *actual, *save;
    actual = *aChannel;
    *aChannel = 0;
    while (actual != 0)
    {
        // cout << "freechannel, inside loop" << endl;
        save = actual->next;
        delete actual;
        actual = save;
    }
}

void MyMainFrame::DoCluster(int ladder, char aSorK, cluster **aCluster)
{
    bool IsY{IsLadY(ladder)};
    double CluHighThresh{(IsY) ? fCluHighThreshY : fCluHighThresh};

    int i{0};
    int clusmin, clusmax;
    int ifirst{0}, ilast{0};
    // int numva=0;

    bool noclusters = true;
    cluster *clusnow{0};
    channel *channow{0};

    int firstva{0}, lastva{0};

    uint current_va{0};

    uint offset{0};
    uint current_lad = ladder;
    while (current_lad > 0)
    {
        offset += (vBoards.at(current_lad - 1))->getNLCH();
        current_lad--;
    }

    // c if ladder == 0, NLCH=128 => offset == 0

    int sign{TGCBNegativeSignals->IsDown() ? -1 : 1};
    int sign1{TGCBNegativeSignals1->IsDown() ? -1 : 1};

    FreeCluster(aCluster);

    //    numva=va_per_ladder;
    //    firstva=0;
    //    lastva=va_per_ladder;
    //    ifirst=firstva*channel_per_va;
    //    ilast =lastva*channel_per_va;

    if (aSorK == 'S')
    {
        // numva=NVA;
        firstva = 0;
        lastva = (vBoards.at(ladder))->getNVA() - 1;
        ifirst = offset + firstva * (vBoards.at(ladder))->getNVACH();     // c NVACH=64 => ifirst=offset==0
        ilast = offset + (lastva + 1) * (vBoards.at(ladder))->getNVACH(); // c ilast=128
    }

    // if (aSorK == 'K'){
    //   //numva=NVA;
    //   firstva=6;
    //   lastva=12;
    //   ifirst=offset+firstva*NVACH;
    //   ilast =offset+lastva*NVACH;
    // }

    int nclus = 0;

    i = ifirst;
    while (i < ilast)
    {
        if (TGCBNegativeSignals->IsDown() && ladder > 0)
            sign = 1;
        if (TGCBNegativeSignals->IsDown() && ladder < 1)
            sign = -1;
        if (TGCBNegativeSignals1->IsDown() && ladder > 0 && ladder < 2)
            sign1 = -1;
        if (TGCBNegativeSignals1->IsDown() && ladder < 1)
            sign1 = 1;
        if (!fStatus.at(i))
            vh2Sovern.at(ladder)->Fill(i - offset, sign * sign1 * fReadOut.at(i) / fSigmas.at(i));
        if ((sign * sign1 * fReadOut.at(i) <= CluHighThresh * fSigmas.at(i)) || fStatus.at(i)) // a bad strip cannot be the seed of a cluster
        {
            i++;
            continue;
        }
        // if over th. @i==89
        nclus++; // nclus==1

        clusmin = i - 1; // clusmin==88
        if (clusmin >= ifirst)
        {
            while (clusmin >= ifirst &&
                   sign * sign1 * fReadOut.at(clusmin) > fCluLowThresh * fSigmas.at(clusmin))
            {
                if (fStatus.at(clusmin) && fStatus.at(clusmin + 1))
                {
                    clusmin++;
                    break;
                }
                clusmin--;
            }
        }
        clusmin++;
        if (fStatus.at(clusmin))
            clusmin++;

        clusmax = i + 1; // clusmax==90
        if (clusmax < ilast)
        {
            while (clusmax < ilast &&
                   sign * sign1 * fReadOut.at(clusmax) > fCluLowThresh * fSigmas.at(clusmax))
            {
                if (fStatus.at(clusmax) && fStatus.at(clusmax - 1))
                {
                    clusmax--;
                    break;
                }
                if ((sign * sign1 * fReadOut.at(clusmax) >= CluHighThresh * fSigmas.at(clusmax)) && (sign * sign1 * fReadOut.at(clusmax - 1) < CluHighThresh * fSigmas.at(clusmax - 1)) && fStatus.at(clusmax) == 0 && fStatus.at(clusmax - 1) == 0)
                {
                    clusmax--;
                    break;
                }
                clusmax++;
            }
        }
        clusmax--;
        if (fStatus.at(clusmax))
            clusmax--;

        float aInt = 0.0;
        for (int l = clusmin; l <= clusmax; l++)
        {
            aInt += sign * sign1 * fReadOut.at(l);
        }

        if (aInt < fMinInt)
        {
            i++;
            continue;
        }

        i = clusmax + 1;
        if (noclusters)
        {
            *aCluster = new cluster;
            clusnow = *aCluster;
            noclusters = false;
        }
        else
        {
            clusnow->next = new cluster;
            clusnow = clusnow->next;
        }

        clusnow->first = clusmin - offset;
        // clusnow->length = clusmax - clusmin + 1;
        clusnow->length = 0;
        clusnow->cog = 0.0;
        clusnow->maxloc = 0;
        clusnow->maxval = 0.0;
        clusnow->integral = 0.0;
        clusnow->sovern1 = 0.0;
        clusnow->sovern2 = 0.0;
        clusnow->sovern3 = 0.0;
        (clusnow->sovernPerChannel).clear();
        clusnow->next = 0;

        for (int l = clusmin; l <= clusmax; l++)
        {
            // va=Getva(l);
            // ch=Getch(l);
            //         if (strcmp(fName,"amsS2")==0)
            //   	printf("  ch:%4d %10.2f %10.2f %10.2f\n",l,fReadOut[va][ch],
            //   	       fSigma[va][ch],fCN[va]);
            if (fStatus.at(l) == 0)
            {
                if (sign * sign1 * fReadOut.at(l) > clusnow->maxval)
                {
                    clusnow->maxval = sign * sign1 * fReadOut.at(l);
                    clusnow->maxloc = l - offset;
                }
                clusnow->integral += sign * sign1 * fReadOut.at(l);
                clusnow->cog += sign * sign1 * fReadOut.at(l) * (l + 1);
                clusnow->length += 1;
                if (fSigmas.at(l) > 0)
                {
                    clusnow->sovern1 += fSigmas.at(l) * fSigmas.at(l);
                    clusnow->sovern2 += fSigmas.at(l);
                    clusnow->sovern3 += (fReadOut.at(l) / fSigmas.at(l)) * (fReadOut.at(l) / fSigmas.at(l));
                }
            }
        }
        clusnow->cog /= clusnow->integral;
        clusnow->cog -= 1.0 + offset;
        if (clusnow->sovern1 > 0)
            clusnow->sovern1 = clusnow->integral / sqrt(clusnow->sovern1);
        if (clusnow->sovern2 > 0)
            clusnow->sovern2 = clusnow->length * clusnow->integral / clusnow->sovern2;
        clusnow->sovern3 = sqrt(clusnow->sovern3);

        int areaMin{clusmin - 10}, areaMax{clusmax + 10};
        if (areaMin < ifirst)
            areaMin = ifirst;
        if (areaMax > ilast)
            areaMax = ilast;

        for (int l = areaMin; l < areaMax; l++)
        {
            if (fStatus.at(l) == 0)
                (clusnow->sovernPerChannel).push_back(sign * sign1 * fReadOut.at(l) / fSigmas.at(l));
        }
        //      if (strcmp(fName,"amsS2")==0)
        //        printf("clus:%10.2f %4d %4d\n",clusnow->integral,
        //  	     clusnow->first,clusnow->length);

        // piece of code to include highest strip if clulen=1
        // does not work like it is!

        int loval = clusmin, hival = clusmax;

        for (int l = loval; l <= hival; l++)
        {
            // va=Getva(l);
            // ch=Getch(l);
            if (l == loval)
            {
                clusnow->channel = new channel;
                channow = clusnow->channel;
            }
            else
            {
                channow->next = new channel;
                channow = channow->next;
            }

            current_lad = ladder;
            current_va = 0;
            while (current_lad > 0)
            {
                current_va += (vBoards.at(current_lad - 1))->getNVA();
                current_lad--;
            }

            channow->Slot = l - offset;
            channow->ADC = sign * sign1 * fReadOut.at(l);
            channow->CN = fCN.at((l - offset) / (vBoards.at(ladder))->getNVACH() + current_va);
            channow->Status = fStatus.at(l);
            channow->next = 0;
        }
        ifirst = i;
    }

    // if (nclus>9) cout << "event: " << fEventCounter << " has " << nclus << " clusters !!!" << endl;
}

void MyMainFrame::Reduction(bool dostats)
{

    // int bin;
    // int nclus_S=0;
    // int nclus_K=0;
    // cluster *actual;
    // channel *actual_channel;
    vdouble vladcog;
    vdouble vcog;

    for (unsigned int ladder{0}; ladder < NLAD; ladder++)
    {

        // cout << "ladder " << ladder << endl;

        DoCluster(ladder, 'S', &Scluster);
        // DoCluster(ladder, 'K',&Kcluster);

        // ListCluster(Scluster, Form("Ladder %d, S-side: ", ladder)); //c
        // ListCluster(Kcluster,Form("Ladder %d, K-side: ", ladder));

        // hisClusters[ladder]->Reset();
        ResetGraph(vgrclusters.at(ladder));
        if (dostats)
        {
            vcog = BuildClusterStats(Scluster, ladder);
            if (TGCBSaveClusters->IsDown())
            {
                eventNumber_map.at(ladder) = fEventNumberDAQ;
                frameCounter_map.at(ladder) = fFramaCounterDAQ;
            }
        }
        // cout << "vcog size: " << vcog.size() << endl;
        if (vcog.size() == 1)
            vladcog.push_back(vcog.at(0));
    }

    // cout << "vladcog size: " << vladcog.size() << endl;

    if ((fNstripY == 1) and dostats)
    { // the last detector is Y, if it exists
        if (vladcog.size() == (fNstripX + fNstripY))
        {
            for (size_t lad{0}; lad < fNstripX; lad++)
            {
                vh2CogXY.at(lad)->Fill(vladcog.at(lad) * 0.025, vladcog.at(fNstripX) * 0.4);
            }
        }
    }

    if (dostats and TGCBSaveClusters->IsDown())
    {
        // for (int ladder = 0; ladder < NLAD; ladder++) if (!(integral_map.at(ladder)).empty() && !(length_map.at(ladder)).empty() && !(cog_map.at(ladder)).empty())
        clusters_tree->Fill();
        for (int ladder = 0; ladder < (int)NLAD; ladder++)
        {
            (integral_map.at(ladder)).clear();
            (length_map.at(ladder)).clear();
            (cog_map.at(ladder)).clear();
            (sovern_map.at(ladder)).clear();
            (sovernPerChannel_map.at(ladder)).clear();
            (integral_map.at(ladder)).shrink_to_fit();
            (length_map.at(ladder)).shrink_to_fit();
            (cog_map.at(ladder)).shrink_to_fit();
            (sovern_map.at(ladder)).shrink_to_fit();
            (sovernPerChannel_map.at(ladder)).shrink_to_fit();
        }
    }
}

void MyMainFrame::ListCluster(cluster *acluster, string mesg)
{

    cluster *actual = 0;
    // channel *actual_channel=0;

    actual = acluster;

    int nclus = 0;

    while (actual != 0)
    {
        nclus++;
        std::cout << mesg << Form("  i=%3d, len=%3d, cog=%5.2f, maxloc=%3d, integral=%7.2f, sovern1=%5.2f", nclus, actual->length, actual->cog, actual->maxloc, actual->integral, actual->sovern1) << endl;
        ListChannels(actual->channel);
        actual = actual->next;
    }

    // c if (nclus) cout << nclus << " clusters found !" << endl;
}

void MyMainFrame::ListChannels(channel *achannel)
{

    channel *actual = achannel;
    int nchan = 0;
    while (actual != 0)
    {

        nchan++;
        std::cout << Form("      channel %d, position %3d, adc=%7.2f, cn=%7.2f", nchan, actual->Slot, actual->ADC, actual->CN) << endl;
        actual = actual->next;
    }
}

vdouble MyMainFrame::BuildClusterStats(cluster *acluster, int ladder)
{
    vdouble vcog;
    cluster *actual = acluster;
    channel *achannel = 0;
    int nclus = 0;

    TH1F *hocc{vhisOccupancy.at(ladder)};
    TH1F *hocccut{vhisOccupancyCut.at(ladder)};
    TH1F *hcog{vhisCog.at(ladder)};
    TH1F *hcogcut{vhisCogCut.at(ladder)};
    TH1F *hlens{vhisLens.at(ladder)};
    TH1F *hlenscut{vhisLensCut.at(ladder)};
    // TH1F *hlenk{vhisLenk.at(ladder)};
    TH1F *hints{vhisIntS.at(ladder)};
    TH1F *hintscut{vhisIntSCut.at(ladder)};
    // TH1F *hintk{vhisIntK.at(ladder)};
    TH1F *hnclus{vhisNclus.at(ladder)};

    TH2F *h2len{vh2LenVsCog.at(ladder)};
    TH2F *h2lencut{vh2LenVsCogCut.at(ladder)};

    TH2F *h2int{vh2IntVsCog.at(ladder)};
    TH2F *h2intcut{vh2IntVsCogCut.at(ladder)};

    while (actual != 0)
    {
        // c cout<<"-->"<<nclus<<endl;
        nclus++;
        achannel = actual->channel;

        // if ((acluster->cog)>=NLCH/2) {
        //   hintk->Fill(acluster->integral);
        //   hlenk->Fill(acluster->length);
        // }
        // else {
        double integral{actual->integral};
        int length{actual->length};
        double cog{actual->cog};
        double sovern1{actual->sovern1};
        vector<double> sovernPerChannel = actual->sovernPerChannel;
        vcog.push_back(cog);
        if (TGCBSaveClusters->IsDown())
        {
            (integral_map.at(ladder)).push_back(integral);
            (length_map.at(ladder)).push_back(length);
            (cog_map.at(ladder)).push_back(cog);
            (sovern_map.at(ladder)).push_back(sovern1);
            (sovernPerChannel_map.at(ladder)).push_back(sovernPerChannel);
        }
        hints->Fill(integral);
        hlens->Fill(length);
        // for debug purposes
        // if ((cog<46 || cog>49) && length>4) cout << "event: " << fEventCounter << " length=" << length << " cog= " << cog << endl;
        // if (!(cog<46 || cog>49) && length>3) cout << "event: " << fEventCounter << " length=" << length << " cog= " << cog << endl;
        h2len->Fill(cog, length);
        h2int->Fill(cog, integral);
        hcog->Fill(cog);

        if (integral > fMinValidIntegral)
        {
            hintscut->Fill(integral);
            hlenscut->Fill(length);
            h2lencut->Fill(cog, length);
            h2intcut->Fill(cog, integral);
            hcogcut->Fill(cog);
        }
        // cout << actual->integral << endl;
        // }

        while (achannel != 0)
        {
            // his->SetBinContent(1+achannel->Slot,achannel->ADC);
            // gr->SetPoint(gr->GetN(), achannel->Slot, achannel->ADC);
            // c cout<<"slot = "<<achannel->Slot<<endl;
            hocc->Fill(achannel->Slot);
            if (integral > fMinValidIntegral)
                hocccut->Fill(achannel->Slot);
            achannel = achannel->next;
        }

        actual = actual->next;
        sovernPerChannel.clear();
        sovernPerChannel.shrink_to_fit();
    }
    hnclus->Fill(nclus);
    return vcog;
}

void MyMainFrame::BuildGrCluster(cluster *acluster, TGraph *gr)
{

    cluster *actual = acluster;
    channel *achannel = 0;
    int nclus = 0;

    while (actual != 0)
    {
        nclus++;
        achannel = actual->channel;

        while (achannel != 0)
        {
            // his->SetBinContent(1+achannel->Slot,achannel->ADC);
            gr->SetPoint(gr->GetN(), achannel->Slot, achannel->ADC);
            achannel = achannel->next;
        }

        actual = actual->next;
    }
}

void MyMainFrame::DrawHis(TCanvas *c, int ipad, TH1 *his)
{
    TVirtualPad *pad = c->cd(ipad);
    pad->GetListOfPrimitives()->Remove(his);
    his->Draw("same");
}

unsigned short MyMainFrame::GetStripValue(int ch, int convertMode)
{

    int index{ch};

    if (convertMode == 0)
        index = ch;
    else if (convertMode == 1)
    {
        // We convert from channel value to strip value, which should be the normal mode once debugging at readout level is finished.
        index = ConvertStripToChannel(ch);
    }
    else
        std::cout << "unkown conversion option " << convertMode << std::endl;
    
    if (index == 9999) return 0;
    else return fStrip[index];
}

unsigned short MyMainFrame::ConvertChannelToStrip(unsigned short ch)
{

    unsigned short index{ch};

    if (ch >= 0 and ch < 256)
    {
        index = 255 - ch + 1536;
    }
    else if (ch >= 256 and ch < 512)
    {
        index = ch - 256 + 1792;
    }
    else if (ch >= 512 and ch < 768)
    {
        index = 767 - ch + 1024;
    }
    else if (ch >= 768 and ch < 1024)
    {
        index = 1023 - ch + 512;
    }
    else if (ch >= 1024 and ch < 1280)
    {
        index = ch - 1024 + 256;
    }
    else if (ch >= 1280 and ch < 1536)
    {
        index = 1535 - ch + 0;
    }
    else if (ch >= 1536 and ch < 1792)
    {
        index = ch - 1536 + 768;
    }
    else if (ch >= 1792 and ch < 2048)
    {
        index = ch - 1792 + 1280;
    }
    else
        std::cout << "you should never arrive here...." << std::endl;

    return index;
}

unsigned short MyMainFrame::ConvertStripToChannel(unsigned short strip)
{

    bool TwoStripXflag{false};
    if (strip >= 2048)
    {
        strip -= 2048;
        TwoStripXflag = true;
    }

    unsigned short index{strip};

    if (strip >= 0 and strip < 256)
    {
        index = (255 - strip) + 1280;
        if (TGCBCorrectChannelNumbers->IsDown()) {
            if (strip < 64) {
                index -= 2;
                if (strip > 61) index = 9999;
            }
            else if (strip < 128) {
                index -= 1;
                if (strip > 126) index = 9999;
            }
            else if (strip < 192) index -= 0;
            else if (strip < 256) {
                index += 1;
                if (strip < 193) index = 9999;
            }
        }
    }
    else if (strip >= 256 and strip < 512)
    {
        index = strip - 256 + 1024;
        if (TGCBCorrectChannelNumbers->IsDown()) {
            if (strip < (256 + 64)) {
                index += 1;
                if (strip > (256 + 64 - 2)) index = 9999;
            }
            else if (strip < (256 + 128)) index += 0;
            else if (strip < (256 + 192)) {
                index -= 1;
                if (strip < (256 + 128 + 1)) index = 9999;
            }
            else if (strip < (256 + 256)) {
                index -= 2;
                if (strip < (256 + 192 + 2)) index = 9999;
            }
        }
    }
    else if (strip >= 512 and strip < 768)
    {
        index = 767 - strip + 768;
        if (TGCBCorrectChannelNumbers->IsDown()) {
            if (strip < (512 + 64)) {
                index -= 2;
                if (strip > (512 + 64 - 3)) index = 9999;
            }
            else if (strip < (512 + 128)) {
                index -= 1;
                if (strip > (512 + 128 - 2)) index = 9999;
            }
            else if (strip < (512 + 192)) index -= 0;
            else if (strip < (512 + 256)) {
                index += 1;
                if (strip < (512 + 192 + 1)) index = 9999;
            }
        }
    }
    else if (strip >= 768 and strip < 1024)
    {
        index = strip - 768 + 1536;
        if (TGCBCorrectChannelNumbers->IsDown()) {
            if (strip < (768 + 64)) {
                index += 1;
                if (strip > (768 + 64 - 2)) index = 9999;
            }
            else if (strip < (768 + 128)) index += 0;
            else if (strip < (768 + 192)) {
                index -= 1;
                if (strip < (768 + 128 + 1)) index = 9999;
            }
            else if (strip < (768 + 256)) {
                index -= 2;
                if (strip < (768 + 192 + 2)) index = 9999;
            }
        }
    }
    else if (strip >= 1024 and strip < 1280)
    {
        index = 1279 - strip + 512;
        if (TGCBCorrectChannelNumbers->IsDown()) {
            if (strip < (1024 + 64)) {
                index -= 2;
                if (strip > (1024 + 64 - 3)) index = 9999;
            }
            else if (strip < (1024 + 128)) {
                index -= 1;
                if (strip > (1024 + 128 - 2)) index = 9999;
            }
            else if (strip < (1024 + 192)) index -= 0;
            else if (strip < (1024 + 256)) {
                index += 1;
                if (strip < (1024 + 192 + 1)) index = 9999;
            }
        }
    }
    else if (strip >= 1280 and strip < 1536)
    {
        index = strip - 1280 + 1792;
        if (TGCBCorrectChannelNumbers->IsDown()) {
            if (strip < (1280 + 64)) {
                index += 1;
                if (strip > (1280 + 64 - 2)) index = 9999;
            }
            else if (strip < (1280 + 128)) index += 0;
            else if (strip < (1280 + 192)) {
                index -= 1;
                if (strip < (1280 + 128 + 1)) index = 9999;
            }
            else if (strip < (1280 + 256)) {
                index -= 2;
                if (strip < (1280 + 192 + 2)) index = 9999;
            }
        }
    }
    else if (strip >= 1536 and strip < 1792)
    {
        index = 1791 - strip + 0;
        if (TGCBCorrectChannelNumbers->IsDown()) {
            if (strip < (1536 + 64)) {
                index -= 2;
                if (strip > (1536 + 64 - 3)) index = 9999;
            }
            else if (strip < (1536 + 128)) {
                index -= 1;
                if (strip > (1536 + 128 - 2)) index = 9999;
            }
            else if (strip < (1536 + 192)) index -= 0;
            else if (strip < (1536 + 256)) {
                index += 1;
                if (strip < (1536 + 192 + 1)) index = 9999;
            }
        }
    }
    else if (strip >= 1792 and strip < 2048)
    {
        index = strip - 1792 + 256;
        if (TGCBCorrectChannelNumbers->IsDown()) {
            if (strip < (1792 + 64)) {
                index += 1;
                if (strip > (1792 + 64 - 2)) index = 9999;
            }
            else if (strip < (1792 + 128)) index += 0;
            else if (strip < (1792 + 192)) {
                index -= 1;
                if (strip < (1792 + 128 + 1)) index = 9999;
            }
            else if (strip < (1792 + 256)) {
                index -= 2;
                if (strip < (1792 + 192 + 2)) index = 9999;
            }
        }
    }
    else
        std::cout << "ConvertStripToChannel: you should never arrive here.... strip = " << strip << std::endl;
    if (TwoStripXflag)
        return index + 2048;
    else
        return index;
}

long MyMainFrame::GetFileSize(string filename)
{
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

void MyMainFrame::ComputeReduction()
{

    InitRunningCN();
    fTGHPBprogress->Reset();

    // int SigmaCnt=0;
    vdouble().swap(fReadOut);
    fReadOut.assign(fNTOTCH, 0);

    std::cout << "20: Resetting ADC histograms" << endl;

    /*
    for (int lad = 0; lad < NLAD; lad++)
    {
        h2ChHi[lad]->Reset();
        h2ChHiPe[lad]->Reset();
        for (uint ch{0}; ch < fNTOTCH; ch++)
        {
            //int lad=ch/NLCH;   <----- this does not work, considering the hisChHi[][fNTOTCH] trick. I will have to fix that one day
            //cout << "resetting " << lad << "  " << ch << endl;
            vHisChHi[lad].at(ch)->Reset();
            vHisChHiPe[lad].at(ch)->Reset();
        }
        vHisChHi[lad].at(fNTOTCH)->Reset();
    }*/
    std::cout << "22: Resetting occupancy histograms" << endl;
    for (unsigned int lad = 0; lad < NLAD; lad++)
    {
        vhisOccupancy.at(lad)->Reset();
        vhisOccupancyCut.at(lad)->Reset();
    }

    std::cout << "before starting event scanning" << endl;
    // TH2F *h2ADCval{new TH2F("h2ADCval", "cnsubt distribution", 2048, -0.5, 2048 - 0.5, 200, -100, 100)};
    cout << "fNevents = " << fNevents << endl;

    uint current_ch{0};
    uint current_lad{0};

    for (int event = ExcludeEvents; event < fNevents; event++)
    { // we ignore the first 10 events
        fTree->GetEntry(event);
        fEventCounter = event;
        // cout << "event " << event << endl;

        if (event % (fNevents / 20) == 0)
        {
            fTGHPBprogress->SetPosition((event*100.)/fNevents); // in linux, this is enough to update the display of the progress bar
            std::cout << "Progress: " << round((event * 100.) / fNevents) << " %" << endl;
            fTGHPBprogress->ShowPosition(); // in mac, you need to add this
            gSystem->ProcessEvents(); // and this.
            // fTGHPBprogress->DoRedraw(); // let's try this one - does not work, it is a protected method
        }

        double spread{0};
        int goodcnt{0};
        for (unsigned int lad = 0; lad < NLAD; lad++)
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }
                fReadOut.at(current_ch) = GetStripValue(current_ch, (vBoards.at(lad))->getConvertMode()) - fPedestals.at(current_ch);
                spread += fReadOut.at(current_ch);
            }
        spread /= fNTOTCH;
        if (spread < 0)
            spread = -spread;

        if (spread > fSpreadCut)
        {
            // if (TGCBSaveClusters->IsDown())
            //     clusters_tree->Fill(); // this makes sure that an empty event will be anyway saved
            // continue;
        }
        // cout << "spread = " << spread << endl;
        //  ComputeCN();
        if (TGCBComputeCN->IsDown())
        {
            ComputeCN(0.8, 0xFF);
            SubtractCN();
            DoRunningCN();
        }

        // let's fill the signal values in the dedicated histograms
        for (uint lad{0}; lad < NLAD; lad++)
            for (uint ch{0}; ch < (vBoards.at(lad))->getNLCH(); ch++)
            {
                current_ch = ch;
                current_lad = lad;
                while (current_lad > 0)
                {
                    current_ch += (vBoards.at(current_lad - 1))->getNLCH();
                    current_lad--;
                }

                uint channel{ch};

                (vvHisChHi.at(lad)).at(channel)->Fill(fReadOut.at(current_ch));
            }

        if (TGCBdodynped->IsDown())
            DynamicPedestals();

        Reduction(true);
    }
/*
    TCanvas *aCanvas{new TCanvas("aCanvas", "aCanvas", 1000, 800)};
    aCanvas->Divide(1, 2);
    aCanvas->cd(1);
    vh2CogXY.at(0)->Draw("colz");
    aCanvas->cd(2);
    vh2CogXY.at(1)->Draw("colz");
    aCanvas->Update();
*/
    DisplayCogxy();

    if (TGCBSaveClusters->IsDown())
    {
        clusters_tree->Write(0, TObject::kOverwrite);
        out_clustered_file->Write(0, TObject::kOverwrite);
        out_clustered_file->Close();

        delete out_clustered_file;
        out_clustered_file = nullptr;
        TGCBSaveClusters->SetDown(kFALSE);
    }
    fTGHPBprogress->SetPosition(100);
    fTGHPBprogress->ShowPosition();
    gSystem->ProcessEvents();

    AddLogMessage("Signal histograms filled.");
}

int MyMainFrame::ConvertLad(int lad)
{

    // for the moment, no particular re-organization, except for the starting value, 1
    if (NLAD == 1)
        return lad;
    else
        return lad + 1;
}

void MyMainFrame::DivideCanvas(TCanvas *c)
{
    switch (NLAD)
    {
    case 1:
        for (uint i{0}; i < NLAD; i++)
        {
            c->cd(i + 1)->SetRightMargin(0.01);
            c->cd(i + 1)->SetLeftMargin(0.06);
            c->cd(i + 1)->SetTopMargin(0.05);
        }
        break;

    case 2:
        c->Divide(2, 1);
        for (uint i{0}; i < NLAD; i++)
        {
            c->cd(i + 1)->SetRightMargin(0.04);
            // c->cd(i + 1)->SetLeftMargin(0.06);
            c->cd(i + 1)->SetBottomMargin(0.08);
        }
        break;

    case 3:
        c->Divide(1, 3);
        break;

    case 4:
        c->Divide(2, 2);
        break;

    case 5:
    case 6:
        c->Divide(3, 2);
        break;
    }
}

void MyMainFrame::CreateSquarePads(TCanvas *c, int nx, int ny)
{

    c->Divide(nx, ny);
    for (int i{0}; i < (nx + ny); i++)
    {
        c->cd(i + 1);
        SetSquareAspect(gPad, nx, ny);
    }
}

void MyMainFrame::SetSquareAspect(TVirtualPad *pad, int nx, int ny)
{
    // modified code from an old suggestion in root forum (https://root-forum.cern.ch/t/resizing-pads-created-with-c1-divide-nx-ny/3080)

    double xlo, ylo, xup, yup;
    //double xwid2, ywid2;

    pad->GetPadPar(xlo, ylo, xup, yup); // in NDC
    //double xwid{(xup - xlo) / 2.};
    //double ywid{(yup - ylo) / 2.};
    double xmid {(xlo + xup) / 2.};
    double ymid {(ylo + yup) / 2.};

    double canw{static_cast <double>(pad->GetWw())}; // parent canvas dimension in pixels
    double canh{static_cast <double> (pad->GetWh())}; // parent canvas dimension in pixels
    //double WW{pad->GetWNDC()}; // pad width in NDC
    //double HH{pad->GetHNDC()}; // pad height in NDC

    double xabsw{0};
    double yabsw{0};

    if ((canw / nx) > (canh / ny))
        xabsw = canh / ny;
    else xabsw = canw / nx;
      
    yabsw = xabsw;

    double WW {xabsw / canw};
    double HH {yabsw / canh};

    double xwid2 {WW / 2.};
    double ywid2 {HH / 2.};

    xlo = xmid - xwid2;
    xup = xmid + xwid2;
    ylo = ymid - ywid2;
    yup = ymid + ywid2;
    pad->SetPad(xlo, ylo, xup, yup);
}

bool MyMainFrame::InitCommunication()
{

    // if (UnigeGpio) delete UnigeGpio;
    fUnigeGpio = new GpioDaq::UnigeGpioHerd();
    fUnigeGpio->SetQuitLoop(false);
    fUnigeGpio->SetBufferSize(USB_BUFFER_SIZE);
    fUnigeGpio->SetServerAddress(fIPaddress); // win10 computer
    fUnigeGpio->SetPort(fPortGPIO);
    if (fUnigeGpio->OpenConnection())
    {
        cout << "connection to GPIO has a problem" << endl;
        // delete fUnigeGpio;
        // fUnigeGpio = nullptr;
        fTGTBStartDaq->SetEnabled(kFALSE);
        return true;
    }
    fTGTBStartDaq->SetEnabled(kTRUE);
    return false;
}

void MyMainFrame::InitBuffer()
{

    fFitEvent = new HerdFit::FitEvent(fNTOTCH, fNBufferEvents); // fNTOTCH channels, buffer of fNBufferEvents events
    fFitEvent->SetQuitLoop(true);
    fFitEvent->SetPANEnabled(true);
    fFitEvent->SetMaxMsg(USB_BUFFER_SIZE);
    fFitEvent->SetAddress(fIPaddress); // win10 computer
    fFitEvent->SetPort(fPortData);
    fFitEvent->SetBoardAddress(fIPaddressData);
    int status = fFitEvent->SetupUDPServer();
    if (status == 0) {
        bool result = fFitEvent->BindUDPServer();
        if (result) fTGTBStartDaq->SetEnabled(kTRUE);
        else fTGTBStartDaq->SetEnabled(kFALSE);
    }
    else {
        fTGTBStartDaq->SetEnabled(kFALSE);
        return;
    }
}

void MyMainFrame::StartThreads()
{

    cout << "Starting Thread 1" << endl;

    fThreadCommand = new TThread("ThreadCommand", GpioDaq::ReadData, (void *)fUnigeGpio);
    fThreadCommand->Run();

    cout << "Thread 1 started" << endl;
    
    cout << "Starting Thread 2" << endl;

    fThreadReader = new TThread("ThreadReader", HerdFit::udpDataReceiver, (void *)fFitEvent);
    fThreadReader->Run();

    cout << "Thread 2 started" << endl;

    cout << "Starting Thread 3" << endl;

    fThreadBuffer = new TThread("ThreadBuffer", HerdFit::udpserverFIT, (void *)fFitEvent);
    fThreadBuffer->Run();

    cout << "Thread 3 started" << endl;
    
    cout << "Starting Thread 4" << endl;

    fThreadBufferAdjust = new TThread("ThreadBufferAdjust", HerdFit::udpBufferAdjust, (void *)fFitEvent);
    fThreadBufferAdjust->Run();

    cout << "Thread 4 started" << endl;
    
}

void MyMainFrame::CleanThreads()
{
    bool ThreadCancelled{false};
    TThread::EState s;
    
    while (!ThreadCancelled) {
        s = fThreadCommand->GetState();
        if (s == TThread::kCanceledState) ThreadCancelled = true;
    }
    fThreadCommand->Delete();
    fThreadCommand = nullptr;
    ThreadCancelled = false;
    
    while (!ThreadCancelled) {
        s = fThreadReader->GetState();
        if (s == TThread::kCanceledState) ThreadCancelled = true;
    }
    fThreadReader->Delete();
    fThreadReader = nullptr;
    ThreadCancelled = false;
    
    while (!ThreadCancelled) {
        s = fThreadBuffer->GetState();
        if (s == TThread::kCanceledState) ThreadCancelled = true;
    }
    fThreadBuffer->Delete();
    fThreadBuffer = nullptr;
    ThreadCancelled = false;
    
    while (!ThreadCancelled) {
        s = fThreadBufferAdjust->GetState();
        if (s == TThread::kCanceledState) ThreadCancelled = true;
    }
    fThreadBufferAdjust->Delete();
    fThreadBufferAdjust = nullptr;
    ThreadCancelled = false;
    
}

void MyMainFrame::CleanupCommunication()
{

    if (fUnigeGpio)
    {
        fUnigeGpio->SetQuitLoop(true);
        sleep(1);
        fUnigeGpio->CloseConnection();
        fTGTBStartDaq->SetEnabled(kFALSE);
        // delete fUnigeGpio;
    }
}

void MyMainFrame::CleanupBuffer()
{

    fFitEvent->SetQuitLoop(false);
    sleep(1);
    fFitEvent->CloseUDPServer();
    fFitEvent->Clear();
    fTGTBStartDaq->SetEnabled(kFALSE);
    // delete fFitEvent;
}

void MyMainFrame::GetEvent(void *arg)
{

    TThread::SetCancelOn();
    TThread::SetCancelDeferred();

    MyMainFrame *inst = (MyMainFrame *)arg;
    inst->fNevents = 0;
    bool calstart{true};
    bool calrunning{true}, startTimer{false};
    if (!(inst->TGCBDoCalib)->IsDown())
    {
        calrunning = false;
        startTimer = true;
    }

    while (!inst->GetQuitLoop())
    {

        TThread::CancelPoint();

        inst->fBufferStatus = (inst->fFitEvent)->GetEntryStatus(inst->fBufferPos);
        inst->fBufferCounter = (inst->fFitEvent)->GetEntryCounter(inst->fBufferPos);
        inst->fLostCounter = (inst->fFitEvent)->GetLostEvents();
        // cout << "old counter = " << oldcounter << "   counter = " << counter << "   status = " << status << endl;

        if (inst->fBufferStatus == 1)
        {
            cout << "GetEvent: buffercounter = " << inst->fBufferCounter << "   entrystatus = " << inst->fBufferStatus << "  lost (could not write into buffer) = " << inst->fLostCounter << endl;
            inst->fvEvent = (inst->fFitEvent)->GetEntryEvent(inst->fBufferPos);
            cout << "GetEvent: new entry status = " << (inst->fFitEvent)->GetEntryStatus(inst->fBufferPos) << endl;

            cout << "GetEvent: buffer status: " << (inst->fFitEvent)->GetNStatus(0) << " entries with status 0, " << (inst->fFitEvent)->GetNStatus(1) << " entries with status 1, " << (inst->fFitEvent)->GetNStatus(2) << " entries with status 2" << endl;

            // if ((inst->fFitEvent)->isEventComplete(inst->fBufferPos)) {
            if (true)
            {
                inst->fdecvEvent = inst->fvEvent;
                std::copy((inst->fdecvEvent).begin(), (inst->fdecvEvent).end(), inst->fStrip);
                (inst->fNevents)++;
                cout << "GetEvent: Event is complete,  fNevents = " << inst->fNevents << endl;
                // if (!calrunning) inst->DisplayThisEvent();
                if (calrunning)
                    calrunning = inst->ComputeOnlineCalibration(calstart);
                else if (!calrunning && (inst->TGCBDoCalib)->IsDown())
                    inst->fQuitRun = true;
                else if (!calrunning && startTimer)
                {
                    inst->StartEventTrigger();
                    startTimer = false;
                    cout << "Event Trigger started" << endl;
                }
                if (calstart)
                    calstart = false;
                if (inst->fDoRecalibration && !calstart && !calrunning)
                {
                    inst->StopEventTrigger();
                    usleep(100);
                    startTimer = true;
                    calstart = true;
                    calrunning = true;
                    inst->fDoRecalibration = false;
                }

                inst->fEventCounter = inst->fNevents - 1;
                /*if (inst->fNevents % 100 == 0 && inst->fNevents > 100) { // inst->fNevents % 100 == 0 && inst->fNevents > 100
                 (inst->fUnigeGpio)->SetBusy(true);
                 (inst->fUnigeGpio)->SetDirectParameters();
                 if (inst->fNevents == 200) {
                 inst->ComputePedestals(0);
                 bool cp2res{false};
                 cp2res=inst->ComputePedestals2(0, 100);
                 if (cp2res) {
                 cp2res=inst->ComputePedestals2(0, 50);
                 if (cp2res) {
                 cp2res=inst->ComputePedestals2(0, 25);
                 }
                 }
                 }
                 //inst->DisplayThisEvent();
                 (inst->fUnigeGpio)->SetBusy(false);
                 (inst->fUnigeGpio)->SetDirectParameters();
                 //inst->StopScope();
                 }*/
            }
        }
        (inst->fBufferPos)++;
        usleep(100);
    }
    if (!calrunning && (inst->TGCBDoCalib)->IsDown())
    {
        inst->StopScope();
        (inst->fUnigeGpio)->SetTrigInSource("NIM_IN0");
        usleep(1);
        (inst->fUnigeGpio)->SetDirectParameters();
    }
}

int MyMainFrame::StartDaq()
{

    AddLogMessage("Starting online run");
    InitRunningCN();
    ResetHistos();

    fBufferPos = 0;
    fBufferStatus = 0;
    fBufferCounter = 0;
    fLostCounter = 0;

    if (fUnigeGpio->GetQuitLoop() && !fFitEvent->GetQuitLoop())
        CleanThreads();
    fUnigeGpio->SetQuitLoop(false);
    fFitEvent->SetQuitLoop(true);
    StartThreads();

    fDaqRun = true;
    if (fQuitRun)
    {
        bool ThreadCancelled{false};
        TThread::EState s;
        
        while (!ThreadCancelled) {
            s = fThreadGetEvent->GetState();
            if (s == TThread::kCanceledState) ThreadCancelled = true;
        }
        fThreadGetEvent->Delete();
        fThreadGetEvent = nullptr;
    }
    fQuitRun = false;
    SetRunButtons(0, 0, 1, 0);
    //*
    if (fUnigeGpio->OpenConfigFile(fConfigFilename.c_str()))
        cout << "config file successfully opened" << endl;
    else if (fUnigeGpio->OpenConfigFile("/home/herd/Applications/V3.1_20201029/config_boardV2_FPGAV31_halfgain_st300ns_biascorrection_254VATA012345_on_packetsize3072_delay11.xml"))
        cout << "linux config file successfully opened" << endl;
    else if (fUnigeGpio->OpenConfigFile("c:\\home\\herd\\Applications\\V3.1_20201029\\config_boardV2_FPGAV31_halfgain_st300ns_biascorrection_254VATA012345_on_packetsize3072_delay11.xml"))
        cout << "windows config file successfully opened" << endl;
    else
    {

        cout << "configuration file not opened" << endl;
        SetRunButtons(1, 1, 0, 0);
        return 1;
    }
    //*/
    // fUnigeGpio->SetTrigInSource("NIM_IN0"); // NIM_IN0 / Adapter
    fUnigeGpio->SetFrequencyOfAutotrigger("1000"); // 1300 Hz is the maximum sustainable rate for the DAQ
    fUnigeGpio->ClearFIFO(true);
    usleep(1);
    fUnigeGpio->SetDirectParameters();
    fUnigeGpio->ClearFIFO(false);
    usleep(1);
    fUnigeGpio->ClearEventsCounter(true);
    fUnigeGpio->SetDirectParameters();
    //*
    if (fUnigeGpio->BoardConfigure())
        cout << "Board configuration done" << endl;
    else
        cout << "Board configuration fialed" << endl;
    if (fUnigeGpio->UpdateASICsParameters())
        cout << "ASICs parameters updated" << endl;
    else
    {
        cout << "ASICs configuration failed" << endl;
        SetRunButtons(1, 1, 0, 0);
        return 2;
    }
    //*/
    if (TGCBDoCalib->IsDown())
    {
        fUnigeGpio->SetTrigInSource("Internal");
        usleep(1);
        fUnigeGpio->SetDirectParameters();
    }

    fUnigeGpio->StartAcquisition(fDAQfileName.c_str(), true, true); // 1st true=async DAQ, 2nd true=send data via IP
    cout << "Acquisition started" << endl;
    fEventCounter = 0;
    sleep(1);
    fThreadGetEvent = new TThread("ThreadGetEvent", (void (*)(void *)) & GetEvent, (void *)this);
    fThreadGetEvent->Run();
    cout << "GetEvent thread started" << endl;

    return 0;
}

Double_t langaufun(Double_t *x, Double_t *par)
{
    // From the root tutorials.
    // Fit parameters:
    // par[0]=Width (scale) parameter of Landau density
    // par[1]=Most Probable (MP, location) parameter of Landau density
    // par[2]=Total area (integral -inf to inf, normalization constant)
    // par[3]=Width (sigma) of convoluted Gaussian function
    //
    // In the Landau distribution (represented by the CERNLIB approximation),
    // the maximum is located at x=-0.22278298 with the location parameter=0.
    // This shift is corrected within this function, so that the actual
    // maximum is identical to the MP parameter.

    // Numeric constants
    Double_t invsq2pi = 0.3989422804014; // (2 pi)^(-1/2)
    Double_t mpshift = -0.22278298;      // Landau maximum location

    // Control constants
    Double_t np = 100.0; // number of convolution steps
    Double_t sc = 5.0;   // convolution extends to +-sc Gaussian sigmas

    // Variables
    Double_t xx;
    Double_t mpc;
    Double_t fland;
    Double_t sum = 0.0;
    Double_t xlow, xupp;
    Double_t step;
    Double_t i;

    // MP shift correction
    mpc = par[1] - mpshift * par[0];

    // Range of convolution integral
    xlow = x[0] - sc * par[3];
    xupp = x[0] + sc * par[3];

    step = (xupp - xlow) / np;

    // Convolution integral of Landau and Gaussian by sum
    for (i = 1.0; i <= np / 2; i++)
    {
        xx = xlow + (i - .5) * step;
        fland = TMath::Landau(xx, mpc, par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0], xx, par[3]);

        xx = xupp - (i - .5) * step;
        fland = TMath::Landau(xx, mpc, par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0], xx, par[3]);
    }

    return (par[2] * step * sum * invsq2pi / par[3]);
}

void MyMainFrame::UpdateChanLabel()
{
    fChanLabelMode = TGCBStripMode->IsDown();
    // string chanlab{ChanLabel[fChanLabelMode]};
    if (fFileOpened)
    {
        // UpdateAdcDisplay(fEventCounter, 1);
        // UpdateAdcPedCnSubtDisplay(fEventCounter, 1);
    }
}

void MyMainFrame::SetChannelStatus(int ch, int value)
{

    fStatus.at(ch) |= value;
    cout << "channel " << ch << " status updated to " << fStatus.at(ch) << endl;
}

double GetStripYLength(double strip)
{
    constexpr double L0{51.2325};
    double pitch{0.4};

    strip -= 63.5;

    double l{TMath::Sqrt(L0 * L0 - 4 * strip * strip * pitch * pitch)};

    return l;
}

double CNFunY(double *x, double *par)
{
    double amplitude = par[0];
    double shift = par[1];
    return amplitude * GetStripYLength(x[0]) + shift;
}

uint MyMainFrame::GetLadIndex(int ch)
{
    // vint vsizes;
    int val{ch};
    uint ladval{0xFFFF};
    for (size_t lad{0}; lad < vBoards.size(); lad++)
    {
        uint nch{vBoards.at(lad)->getNLCH()};
        val -= nch;
        if (val < 0)
        {
            ladval = lad;
            break;
        }
    }
    if (ladval == 0xFFFF)
        cout << "Strange, ladval = 0xFFFF, this should never happen" << endl;

    return ladval;
}

int MyMainFrame::GetLadNCH(uint lad)
{

    return (vBoards.at(lad))->getNLCH();
}

bool MyMainFrame::IsLadY(uint lad)
{
    return (GetLadNCH(lad) == 128);
}

void MyMainFrame::UpdateAxes(bool isy, TGNumberEntry *tgne[2][4], double (&fAxis)[2][4])
{
    double minx = tgne[isy][0]->GetNumber();
    double maxx = tgne[isy][1]->GetNumber();
    double miny = tgne[isy][2]->GetNumber();
    double maxy = tgne[isy][3]->GetNumber();

    if (minx >= maxx)
    {
        tgne[isy][0]->SetNumber(fAxis[isy][0]);
        tgne[isy][1]->SetNumber(fAxis[isy][1]);
        return;
    }

    if (miny >= maxy)
    {
        tgne[isy][2]->SetNumber(fAxis[isy][2]);
        tgne[isy][3]->SetNumber(fAxis[isy][3]);
    }

    for (int i = 0; i < 4; i++)
        fAxis[isy][i] = tgne[isy][i]->GetNumber();
}

void MyMainFrame::BuildAxisControls(TGHorizontalFrame *Hframe, TGNumberEntry *TGNEax[2][4], double fAxis[2][4], string FunConnect)
{
    TGLabel *minmax[4];
    TGLabel *minmaxY[4];

    minmax[0] = new TGLabel(Hframe, "min x (X):");
    minmax[1] = new TGLabel(Hframe, "max x (X):");
    minmax[2] = new TGLabel(Hframe, "min y (X):");
    minmax[3] = new TGLabel(Hframe, "max y (X):");
    if (fNstripY > 0)
    {
        minmaxY[0] = new TGLabel(Hframe, "min x (Y):");
        minmaxY[1] = new TGLabel(Hframe, "max x (Y):");
        minmaxY[2] = new TGLabel(Hframe, "min y (Y):");
        minmaxY[3] = new TGLabel(Hframe, "max y (Y):");
    }
    for (int i = 0; i < 4; i++)
    {
        TGNEax[0][i] = new TGNumberEntry(Hframe, fAxis[0][i]);
        TGNEax[0][i]->Connect("Modified()", "MyMainFrame", this, Form("%s(=0)", FunConnect.c_str()));
        if (fNstripY > 0)
        {
            TGNEax[1][i] = new TGNumberEntry(Hframe, fAxis[1][i]);
            TGNEax[1][i]->Connect("Modified()", "MyMainFrame", this, Form("%s(=1)", FunConnect.c_str()));
        }
    }

    for (int i = 0; i < 4; i++)
    {
        Hframe->AddFrame(minmax[i], TGLHleft);
        Hframe->AddFrame(TGNEax[0][i], TGLHleft);
    }
    if (fNstripY > 0)
        for (int i = 0; i < 4; i++)
        {
            Hframe->AddFrame(minmaxY[i], TGLHleft);
            Hframe->AddFrame(TGNEax[1][i], TGLHleft);
        }
}

int DisPanOnline(uint nStripX = 2, uint nStripY = 1)
{
    // Popup the GUI...

    // TApplication theApp("App",&argc,argv);
    new MyMainFrame(gClient->GetRoot(), 1200, 800, nStripX, nStripY);
    // cout << "goodbye"<<endl;

    // theApp.Run();

    return 0;
}
