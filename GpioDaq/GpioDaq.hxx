#ifndef GPIODAQ_H
#define GPIODAQ_H

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <iostream>
#include <string>
#include <vector>

#include "TThread.h"

using namespace std;

//#define BUFFER_SIZE 3072


namespace GpioDaq {

  class SocketConnection {

 private:
  bool QuitLoop { false };
  unsigned int BUFFER_SIZE{3072};
  int fSockCli;
  int fPort;
  int fDelay;
  string fServerAddress;
  vector <string> vBuffer;
  unsigned char* fRecvBuf;
  
 public:

    SocketConnection(unsigned int buffer_size = 3072);
    virtual ~SocketConnection();

    void SetServerAddress(string address) { fServerAddress = address; }
    string GetServerAddress() const { return fServerAddress; }
    
    void SetPort(int port) { fPort = port; }
    int GetPort() const { return fPort; }
    void SetDelay(int delay) { fDelay = delay; }
    int GetDelay() const { return fDelay; }
    int GetSocket() const { return fSockCli; }
    size_t GetNumberOfMessages() const { return vBuffer.size(); }

    bool OpenConnection();
    int SendCommand(string message);
    void CloseConnection();
    int ReceiveData();
    void SetQuitLoop(bool state) {QuitLoop = state;}
    bool GetQuitLoop() const {return QuitLoop;}
    string GetReply();
    bool SendBaseCommandBool(string command);
    string SendBaseCommandString(string command);
    
    void SetBufferSize(unsigned int buffer_size);
    unsigned int GetBufferSize() const { return BUFFER_SIZE; }
  };

  class UnigeGpioHerd : public SocketConnection {
    
  private:
    string fStrdaqtime;
    unsigned long fDaqPrevHour, fDaqDay, fDaqHour, fDaqMin, fDaqSec;
    unsigned long fDaqtimesec;

  public:
    bool ActivateConfigDevice(int index, bool activate);
    bool StartAcquisition(string filename, bool async, bool udp);
    bool StopAcquisition();
    bool MaskLEDs(bool state);
    
    bool SelectFEB(unsigned short feb);
    bool SetBusy(bool state);
    //bool SetFEBBusy(unsigned short feb, bool state);
    bool SetFEBBusy(bool state);
    bool SetDirectParameters();
    bool SetSoftTrigger(bool state);
    //bool SetFEBSoftTrigger(unsigned short feb, bool state);
    bool SetFEBSoftTrigger(bool state);
    bool SetDebugTrigger(bool state);
    //bool SetFEBDebugTrigger(unsigned short feb, bool state);
    bool SetFEBDebugTrigger(bool state);
    bool SetFEBSoftReset(bool state);
    
    bool SetTrigInSource(string source);
    bool SetAutotriggerState(bool state);
    bool ClearFIFO(bool state);
    bool ClearEventsCounter(bool state);
    bool SetFrequencyOfAutotrigger(string freq);
    bool SetDetectorsConfiguration(string ch);
    bool UpdateASICsParameters();
    
    bool BoardConfigure();
    bool OpenConfigFile(string filename);
    string GetFirmwareVersion();
    bool IsTransferingData();
    string ElapsedTime();
    unsigned int ElapsedTimeSec();
    string XferKBytes();   
    
  };
  
  void *ReadData(void *ptr);
}
  
#endif


/*
------------------------------------------------------------------------
Sync (2 member(s)):

1:
void RunScript(string x_scriptFileName)
	This function runs a script from the interpreter (console or socket) by calling ScriptMain() method in the script file
	param: x_scriptFileName -> script file name to be run, must contain ScriptMain() method

2:
void RunScriptArgs(string x_scriptFileName,System.String[] x_args)
	This function runs a script from the interpreter (console or socket) by calling ScriptMainArgs(...) method in the script file
	param: x_scriptFileName -> script file name to be run, must contain ScriptMainArgs(...) method
	param: x_args -> arguments to be passed to ScriptMainArgs() method in the script file


------------------------------------------------------------------------
Dialog (0 member(s)):


------------------------------------------------------------------------
BoardLib (33 member(s)):

1:
bool IsTransferingData { get; }
	Whether or not there is an ongoing DAQ acquisition

2:
string ElapsedTime { get; }
	Return the elapsed time from DAQ start (can be used during DAQ and at the end for total elapsed time) Format = hh:mm:ss

3:
string MaxXferRate { get; }
	Return the maximum XFER rate in KB/s from DAQ start (can be used during DAQ and at the end for total elapsed time)

4:
string AvgXferRate { get; }
	Return the average XFER rate in KB/s from DAQ start (can be used during DAQ and at the end for total elapsed time)

5:
string XferKBytes { get; }
	Return the nb of Bytes transferred from DAQ start

6:
bool HasDevice { get; }
	Whether or not the application has found a device

7:
bool HasAllBoardId { get; }
	Whether or not the application has found all the board ID for all the devices in the USB Device list

8:
void NewConfig()
	Opens a new configuration

9:
bool OpenConfigFile(string x_filename)
	Opens an existing configuration file. Path is relative to the script file.
	param: x_filename -> The filename for the configuration file
	returns: Whether or not the configuration file has been opened

10:
bool SaveConfigFile(string x_filename = null)
	Save the configuration to the given filename, or, if no filename is passed, overwrite the last opened configuration file
	param: x_filename -> An optional filename
	returns: Whether or not the configuration file has been saved

11:
void SetBoardId(byte x_id)
	Set the board id
	param: x_id -> The desired id

12:
void ActivateConfigDevice(byte x_index,bool x_activate)
	activate the selected configuration device to be sent during board configuration
	param: x_index -> The desired device index (i.e. 0,1,2,...) from the device list as shown in Board Tab/Configuration
	param: x_activate -> activate if true, deactivate if false

13:
void SelectUsbDevice(byte x_deviceListIndex)
	select the USB device from its index in the USB device list
	param: x_deviceListIndex -> Index of the connected USB device to be selected within the device list

14:
void SelectUsbDeviceFromBoardId(byte x_boardId)
	select the USB device from its board ID
	param: x_boardId -> Board Id

15:
string GetUsbDevices()
	get the USB devices list

16:
void Reconnect()
	Reconnect the board

17:
bool ReadStatus()
	Read the status from the board and update the UI
	returns: Whether or not the operation was successful

18:
string GetFirmwareVersion(bool x_fullDescription = false)
	Get firmware version
	param: x_fullDescription -> Allow the full description of the versions
	returns: Firmware version

19:
bool CheckVersion()
	Read and check the versions of ProductVId and HardwareVIds
	returns: true if versions are OK, else false (and user message and break)

20:
bool SetDirectParameters()
	Set the direct parameters of the board
	returns: Whether or not the operation was successful

21:
bool UpdateUserParameters(string x_qualifiedName)
	Update User Parameters for a connected user set/get which is not a device used in board configure
	param: x_qualifiedName -> the UpdateUser tree array to be updated
	returns: true if OK

22:
bool DeviceConfigure(byte x_deviceIndex,ConfigureMode x_mode = ConfigureMode.SendVerifyApply)
	Configure the device (send and/or verify and/or apply)
	param: x_mode -> The modes to use : None, Send, Verify, ValidWord, Apply, Read, SendVerifyApply
	param: x_deviceIndex -> Device index to configure
	returns: Whether or not the board configuration succeeded

23:
bool BoardConfigure(ConfigureMode x_mode = ConfigureMode.SendVerifyApply)
	Configure the board (send and/or verify and/or apply)
	param: x_mode -> The modes to use : None, Send, Verify, ValidWord, Apply, Read, SendVerifyApply
	returns: Whether or not the board configuration succeeded

24:
bool VerifyConfig()
	Verify the configuration, return true on success, otherwise false
	returns: True if succeeded, otherwise false

25:
bool StartAcquisition(string x_filename,bool x_async = false,bool x_UDPreadout = false)
	Start a DAQ acquisition and saves the data at the given filename. Path is relative to the script file. If no filename is passed, it will reuse the last one.
	param: x_filename -> Optional filename for saving the data
	param: x_async -> Whether or not to not wait for the acquisition to complete
	param: x_UDPreadout -> Will start the readout with the UDP packets sent to the IP address and port defined in the AppSettings file
	returns: true on success, else false on failure

26:
void WaitForEndOfTransfer(bool x_verbose = false)
	If running a script, sleeps until the DAQ transfer completes
	param: x_verbose -> Whether or not to print status messages

27:
void StopAcquisition()
	Stop the currently running DAQ acquisition

28:
bool GetBoolVariable(string x_qualifiedName)
	Get a Boolean variable from the configuration
	param: x_qualifiedName -> Name of the configuration variable (case insensitive)
	returns: The value for the given name

Exemple: BoardLib.GetBoolVariable("Board.DirectParam.Mask_LEDs")

29:
byte GetByteVariable(string x_qualifiedName)
	Get a Byte variable from the configuration
	param: x_qualifiedName -> Name of the configuration variable (case insensitive)
	returns: The value for the given name

30:
UInt16 GetUInt16Variable(string x_qualifiedName)
	Get a UInt16 variable from the configuration
	param: x_qualifiedName -> Name of the configuration variable (case insensitive)
	returns: The value for the given name

31:
UInt32 GetUInt32Variable(string x_qualifiedName)
	Get a UInt32 variable from the configuration
	param: x_qualifiedName -> Name of the configuration variable (case insensitive)
	returns: The value for the given name

32:
string GetEnumVariable(string x_qualifiedName)
	Get a Int16 variable from the configuration
	param: x_qualifiedName -> Name of the configuration variable (case insensitive)
	returns: The value for the given name

33:
void SetVariable(string x_qualifiedName,object x_value)
	Set a variable of the configuration
	param: x_qualifiedName -> Name of the configuration variable (case insensitive)
	param: x_value -> The value to assign to the variable

Exemple: BoardLib.SetVariable("Board.DirectParam.Mask_LEDs",true)
         BoardLib.SetVariable("Board.DirectParam.allow_soft_trigger",true)

------------------------------------------------------------------------
App (0 member(s)):


------------------------------------------------------------------------


 */
