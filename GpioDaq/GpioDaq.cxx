
#include "GpioDaq.hxx"

namespace GpioDaq {

  SocketConnection::SocketConnection(unsigned int buffer_size) {
    fSockCli=0;
    fPort=11000;
    fDelay=50*1000; // 50 msec
    fServerAddress="127.0.0.1";
    vBuffer.clear();
    BUFFER_SIZE = buffer_size;
    fRecvBuf = new unsigned char[BUFFER_SIZE];
    memset(fRecvBuf, 0, BUFFER_SIZE);
  }
  
  SocketConnection::~SocketConnection() {
    delete[] fRecvBuf;
  }


  bool SocketConnection::OpenConnection() {
    fSockCli=socket(AF_INET,SOCK_STREAM, 0);
    struct sockaddr_in servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(fPort);  /// Server Port
    servaddr.sin_addr.s_addr = inet_addr(fServerAddress.c_str());  /// server ip
    
    //Connect to the server, successfully return 0, error return - 1
    if (connect(fSockCli, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
      perror("connect");
      return true;
    }
    return false;    
  }

  
  int SocketConnection::SendCommand(string message) {
    message+="\r"; // we add a CR to the command
    int bytesSent=send(fSockCli, message.c_str(), message.size(),0);	
    //cout << bytesSent << " bytes have been sent" << endl;
    
    return bytesSent;
  }


  void SocketConnection::CloseConnection() {
    close(fSockCli);
      vBuffer.clear();
      vBuffer.shrink_to_fit();
    fSockCli = -1;
  }

  
  int SocketConnection::ReceiveData() {
    int bytesRecvd=0;
    //int counter{0};
    
    usleep(fDelay);
    //DataRcvd=false;
    bytesRecvd=recv(fSockCli, fRecvBuf, BUFFER_SIZE,0); /// Receiving
    //cout << bytesRecvd << " bytes have been received" << endl;
    if (bytesRecvd>0) {
      //cout << recvbuf << endl;
      vBuffer.push_back(reinterpret_cast<char*>(fRecvBuf));
      //cout << vBuffer.back() << endl;
      //DataRcvd=true;
      memset(fRecvBuf, 0, BUFFER_SIZE);
    }
    
    
    return bytesRecvd;
  }



  string SocketConnection::GetReply() {
    
    bool retok{false};
    string reply{"TIMEOUT from GetReply"};
    string RET{"[RET]:["};
    int maxiter{500}; // 500 iterations on a delay of 10 ms, that's 5 sec. 
    int itercounter{0};
    
    while (!retok && (itercounter<maxiter)) {
      for (size_t i{0}; i<vBuffer.size(); i++) {
	string line{vBuffer.at(i)};
	size_t found{ line.find(RET)};
	if (found!=line.npos) {
	  size_t start{found+RET.size()};
	  size_t len{line.find_last_of("]")-start};
	  reply=line.substr(start, len);
	  retok=true;
	  break; // we exit from the for loop
	}
      }
      itercounter++;
      usleep(10000); // 10 ms
    }
    
    return reply;
  }


  
  bool SocketConnection::SendBaseCommandBool(string command) {

    string reply { SendBaseCommandString(command) };
    
    return (reply!="False");    
  }



  string SocketConnection::SendBaseCommandString(string command) {
    vBuffer.clear();
    vBuffer.shrink_to_fit();
    SendCommand(command);
    string reply { GetReply() };
    
    return reply;
  } 
  
  void SocketConnection::SetBufferSize(unsigned int buffer_size) {
    delete[] fRecvBuf;
    BUFFER_SIZE = buffer_size;
    fRecvBuf = new unsigned char[BUFFER_SIZE];
  }


  
  bool UnigeGpioHerd::ActivateConfigDevice(int index, bool activate) {
    
    string command{Form("BoardLib.ActivateConfigDevice(%d, %s)",index, activate?"true":"false")};
    
    return SendBaseCommandBool(command);
  }


  bool UnigeGpioHerd::StartAcquisition(string filename, bool async, bool udp) {
    
    string command{Form("BoardLib.StartAcquisition(\"%s\",%s,%s)",filename.c_str(), async?"true":"false", udp?"true":"false") };
    
    bool res{ SendBaseCommandBool(command) };
    
    if (res) {
      fDaqtimesec=0;
      fDaqSec=0;
      fDaqMin=0;
      fDaqHour=0;
      fDaqDay=0;
      fDaqPrevHour=0;
    }
    return res;
  }
 

  bool UnigeGpioHerd::StopAcquisition() {
    
    string command{"BoardLib.StopAcquisition()"};
    
    return SendBaseCommandBool(command);
    
  }

  
  bool UnigeGpioHerd::MaskLEDs(bool state) {
    
    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.Mask_LEDs\", %s)", state?"true":"false")};
    
    return SendBaseCommandBool(command);
  }
  
  bool UnigeGpioHerd::SelectFEB(unsigned short feb) {
    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.FEB_SelectSC\", %d)", feb)};
    return SendBaseCommandBool(command);
  }

  
  bool UnigeGpioHerd::SetBusy(bool state) {
    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.soft_busy\", %s)", state?"true":"false")};
    
    return SendBaseCommandBool(command);
  }  

  //  bool UnigeGpioHerd::SetFEBBusy(unsigned short feb, bool state) {
  bool UnigeGpioHerd::SetFEBBusy(bool state) {
    //SelectFEB(feb);
    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.FEB_SoftBusy\", %s)", state?"true":"false")};
    
    return SendBaseCommandBool(command);
  }

  
  bool UnigeGpioHerd::SetFEBSoftReset(bool state) {
    //SelectFEB(feb);
    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.FEB_SoftReset\", %s)", state?"true":"false")};
    
    return SendBaseCommandBool(command);
  }

  
  bool UnigeGpioHerd::SetDirectParameters() {

    string command{"BoardLib.SetDirectParameters()"};
    
    return SendBaseCommandBool(command);
  }


  bool UnigeGpioHerd::SetSoftTrigger(bool state) {

    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.allow_soft_trigger\", %s)", state?"true":"false")};
    cout << command << endl;
    
    return SendBaseCommandBool(command);
  }
  
  //  bool UnigeGpioHerd::SetFEBSoftTrigger(unsigned short feb, bool state) {
  bool UnigeGpioHerd::SetFEBSoftTrigger(bool state) {
    //SelectFEB(feb);
    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.FEB_SoftTriggerEn\", %s)", state?"true":"false")};
    //cout << command << " for FEB " << feb << endl;
    
    return SendBaseCommandBool(command);
  }

  bool UnigeGpioHerd::SetDebugTrigger(bool state) {

    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.allow_debug_trigger\", %s)", state?"true":"false")};
    cout << command << endl;
    
    return SendBaseCommandBool(command);
  }
  
  //  bool UnigeGpioHerd::SetFEBDebugTrigger(unsigned short feb, bool state) {
  bool UnigeGpioHerd::SetFEBDebugTrigger(bool state) {
    //SelectFEB(feb);
    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.FEB_DebugTriggerEn\", %s)", state?"true":"false")};
    //cout << command << " for FEB " << feb << endl;
    
    return SendBaseCommandBool(command);
  }

bool UnigeGpioHerd::SetTrigInSource(string source) {
    
    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.TrigInSource\", %s)", source.c_str())};
    
    return SendBaseCommandBool(command);
    
}

bool UnigeGpioHerd::SetAutotriggerState(bool state) {

    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.Autotrigger\", %s)", state?"true":"false")};
    
    return SendBaseCommandBool(command);

}

bool UnigeGpioHerd::ClearFIFO(bool state) {

    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.L2_FIFO_Clear\", %s)", state?"true":"false")};
    
    return SendBaseCommandBool(command);

}

bool UnigeGpioHerd::ClearEventsCounter(bool state){
	
	string command{Form("BoardLib.SetVariable(\"Board.DirectParam.EventsCounterClear\", %s)", state?"true":"false")};
	
	return SendBaseCommandBool(command);
}

bool UnigeGpioHerd::SetFrequencyOfAutotrigger(string freq) {

    string command{Form("BoardLib.SetVariable(\"Board.DirectParam.Frequency\", %s)", freq.c_str())};
    
    return SendBaseCommandBool(command);

}

bool UnigeGpioHerd::SetDetectorsConfiguration(string ch) {
	
	string command{Form("BoardLib.SetVariable(\"Board.DirectParam.Channels\", %s)", ch.c_str())};
	
	return SendBaseCommandBool(command);
	
}

bool UnigeGpioHerd::UpdateASICsParameters() {

  string command{"BoardLib.UpdateUserParameters(\"Tracker.Acquisition\")"};
  
  return SendBaseCommandBool(command);
  
}

  bool UnigeGpioHerd::BoardConfigure() {
    
    string command{"BoardLib.BoardConfigure()"};
    
    return SendBaseCommandBool(command);
  }

  
  bool UnigeGpioHerd::OpenConfigFile(string filename) {
    
    string command{"BoardLib.OpenConfigFile("};
    command+='"';
    command+=filename;
    command+='"';
    command+=')';
    
    return SendBaseCommandBool(command);
  }


  string UnigeGpioHerd::GetFirmwareVersion() {
    
    //string reply{"error while reading firmware version"};
    
    string command{"BoardLib.GetFirmwareVersion(false)"};
    //if (SendCommand("BoardLib.GetFirmwareVersion(false)")) reply=GetReply();
    
    return SendBaseCommandString(command);
  }


  bool UnigeGpioHerd::IsTransferingData() {
    
    string command{"BoardLib.IsTransferingData()"};
    
    return SendBaseCommandBool(command);
  }

    
  string UnigeGpioHerd::ElapsedTime() {
    
    fStrdaqtime="";
    //string reply{"error while reading elapsed time"};
    
    string command{"BoardLib.ElapsedTime()"};
    //if (SendCommand("BoardLib.ElapsedTime()")) reply=GetReply();
    
    fStrdaqtime = SendBaseCommandString(command);
    return fStrdaqtime;
  }


  unsigned int UnigeGpioHerd::ElapsedTimeSec() {

    string res{ ElapsedTime() };
    size_t first{ res.find_first_of(":") };
    size_t last { res.find_last_of(":") };
    string hh{ res.substr(0,first) };
    string mm{ res.substr(first+1,last-first-1) };
    string ss{ res.substr(last+1,res.size()-last-1) };
    
    //cout << "Extraction: ***" << hh << "***" << mm << "***" << ss << "***" <<  endl;
    fDaqSec=stoul(ss);
    fDaqMin=stoul(mm);
    fDaqHour=stoul(hh);
    if (fDaqHour<fDaqPrevHour) fDaqDay++;
    fDaqPrevHour=fDaqHour;
    
    fDaqtimesec=fDaqDay*86400+fDaqHour*3600+fDaqMin*60+fDaqSec;

    return fDaqtimesec;
  }

  string UnigeGpioHerd::XferKBytes() {
    
    //string reply{"error while reading transfer rate"};
    
    string command{"BoardLib.XferKBytes()"};
    //if (SendCommand("BoardLib.XferKBytes()")) reply=GetReply();
    
    return SendBaseCommandString(command);
  }
  
  
  void *ReadData(void *ptr) {
	  
	TThread::SetCancelOn();
    TThread::SetCancelDeferred();
	  
    int bytesRecv=0;
    //int counter{0};
    SocketConnection* Board{ (SocketConnection*) ptr };
    do {
		TThread::CancelPoint();
      bytesRecv = Board->ReceiveData();
    } while (!Board->GetQuitLoop());
    
    return 0;
  }

}




