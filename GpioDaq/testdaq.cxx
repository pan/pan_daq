#include "GpioDaq.hxx"



void testdaq() {

  GpioDaq::UnigeGpioHerd UnigeGpio;

  if (UnigeGpio.OpenConnection()) {
    cout << "connection problem" << endl;
    return;
  }
  
  cout << "Starting Thread 1" << endl;
  
  //TThread *h1 = new TThread("h1", socketclient, (void*) 1);

  TThread *thread1 = new TThread("thread1", GpioDaq::ReadData, (void*) &UnigeGpio);
  
  thread1->Run();
  //h1->Join();
  

  unsigned int counter{0};


  
  // Loading configuration file:
  if (UnigeGpio.OpenConfigFile("/home/herd/Applications/V3.1_20201029/config_boardV2_FPGAV31_halfgain_st300ns_biascorrection_254VATA012345_on_packetsize1024_delay11.xml")) {
    cout << "config file successfully opened" << endl;
  };



  UnigeGpio.SetSoftTrigger(true);
  UnigeGpio.MaskLEDs(true);
  UnigeGpio.SetDirectParameters();
  if (UnigeGpio.BoardConfigure()) cout << "board configuration done" << endl;
  UnigeGpio.StartAcquisition("/home/herd/Data/tests/testdaq.daq",true,false);
  
  while (1) {

    //std::cout << "hello socketclient !!!" << std::endl;
    sleep(1);
    //TThread::Ps();
  
    cout << "there are " << UnigeGpio.GetNumberOfMessages() << " entries in the reply buffer" << endl;

    if (counter%10==0) {

      //UnigeGpio.SendCommand("BoardLib.ElapsedTime()");
      //UnigeGpio.SendCommand("BoardLib.GetUsbDevices()");
      //UnigeGpio.SendCommand("BoardLib.GetFirmwareVersion(true)");
      //UnigeGpio.GetFirmwareVersion();

      //if (UnigeGpio.IsTransferingData()) {
      cout << "DAQ is " << (UnigeGpio.IsTransferingData()?"":"not") << " transfering data" << endl;
      cout << "DAQ running since: " << UnigeGpio.ElapsedTime() << "  i.e. " << UnigeGpio.ElapsedTimeSec() << " seconds" << endl;
	cout << "Data acquired: " << UnigeGpio.XferKBytes() << " kB" << endl;
	//}
      
    }

    if (counter==25) {
      UnigeGpio.SetBusy(true);
      UnigeGpio.SetDirectParameters();
    }
    if (counter==45) {
      UnigeGpio.SetBusy(false);
      UnigeGpio.SetDirectParameters();
    }
    if (counter==55) break;
    //else
    //if (counter%12==0) { 
    //  SendCommand("BoardLib.CheckVersion()\r");
    //}
    
    counter++;
  }

  UnigeGpio.StopAcquisition();
  GpioDaq::QuitLoop=false;
  
  UnigeGpio.CloseConnection();
}


