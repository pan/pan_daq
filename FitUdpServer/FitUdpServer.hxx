#ifndef FITUDPSERVER_H
#define FITUDPSERVER_H

#include <sys/types.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close() */
#include <string> /* memset() */
#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include <chrono>
// #include <linux/if_packet.h>
#include "TROOT.h"
#include "TThread.h"
#include "TMutex.h"
#include "TCondition.h"
#include "TString.h"
#include <Riostream.h>

#define LOCAL_SERVER_PORT 11001
// #define MAX_MSG 3072 // 3 packets of 1024 bytes but check with the daq programs how is the packet size and number of packets; 16384 for FRB
#define NCHANNELS 384
#define EVENTBUFFERSIZE 200

typedef unsigned int uint;
typedef unsigned short ushort;
typedef std::vector <ushort> vushort;
typedef std::vector <vushort> vvushort;
typedef std::vector <uint> vuint;
typedef std::vector <vuint> vvuint;

namespace HerdFit{

extern TMutex bufferMutex;
extern TCondition bufferCV;

class FitEvent {
  
private:
  bool fQuitLoop{true};
  uint MAX_MSG{3072};
  vvushort fvEventBuffer;
  vvuint fvEventStatus; // 0: event number, 1: data status 0=undefined,  1=written into buffer, 2=data accessed already once
  uint fDataCounter, fLastEventId;//, fBufferPos;
  uint fTotalEventCounter;
  uint fIncompleteEvents;
  uint fMissedEvents;
  uint fLostEvents;
  size_t fNchannels;
  size_t fEventBufferSize;
    size_t fDataBufferSize;
  size_t fNFEBboards;
  bool fInitReading;
  bool fFRBEnabled{false};
  bool fPANEnabled{false};
  uint fNADC{8};
    int fNADCCH{0};
    
  int fsd{0};
  int fPort{LOCAL_SERVER_PORT};
  std::string fServerAddress{INADDR_ANY};
  std::string fBoardAddress{INADDR_ANY};
    unsigned char* fmsg{nullptr};
  std::string ftimeStampFile{"1.txt"};
  
public:
  FitEvent();
  FitEvent(size_t nchan, size_t bufsize, size_t nboards = 1, size_t nadc = 8);
  ~FitEvent();
  
  std::vector<unsigned long long int> vtimeStamp;
    struct sockaddr_in cliAddr;
    std::vector<std::string> dataBuffer;
    std::vector<ssize_t> message_size_list;
    
  void SetEventBufferSize(size_t buffSize);

  uint GetTotalEventCounter() const { return fTotalEventCounter; }
  uint GetIncompleteEvents() const { return fIncompleteEvents; }
  uint GetMissedEvents() const { return fMissedEvents; }
  uint GetLostEvents() const { return fLostEvents; }

  void Clear();
  void BuildEvent(unsigned char *msg, std::size_t len);

  uint GetEntryStatus(uint bufferpos) const;
  uint GetEntryCounter(uint bufferpos) const;
  vushort GetEntryEvent(uint bufferpos);
  uint GetNStatus(uint status) const;
  bool isBufferNearlyFull() const; 
  bool isBufferFull() const;
  bool isEventComplete(uint bufferpos) const;
  uint GetDataCounter() const;
  
  void SetMaxMsg(uint size) { MAX_MSG = size; }
  uint GetMaxMsg() const { return MAX_MSG; }
    void SetPort(int port) {fPort = port;}
    int GetPort() const {return fPort;}
    void SetAddress(std::string addr) {fServerAddress = addr;}
    std::string GetAddress() const {return fServerAddress;}
    void SetBoardAddress(std::string addr) {fBoardAddress = addr;}
    std::string GetBoardAddress() const {return fBoardAddress;}
    std::size_t GetDataBufferSize() const {return fDataBufferSize;}
    
  void SetFRBEnabled(bool status) { fFRBEnabled = status; }
  bool GetFRBEnabled() const { return fFRBEnabled; }
    void SetPANEnabled(bool status) { fPANEnabled = status; }
    bool GetPANEnabled() const { return fPANEnabled; }
  
  int SetupUDPServer();
  bool BindUDPServer();
  void CloseUDPServer();
    
    int SetupUDPClient();
    bool ConnectToUDP();
  
  std::pair<unsigned char*, ssize_t> ReceiveFromClient(uint max_msg, int flags, struct sockaddr_in &cliAddr);
    std::pair<unsigned char*, int> ReceiveFromServer(uint max_msg, int flags, struct sockaddr_in cliAddr);
    
  void SetQuitLoop(bool state) {fQuitLoop = state;}
  bool GetQuitLoop() const {return fQuitLoop;}
  void SetTimeStampFile(std::string name) {ftimeStampFile = name;}
  std::string GetTimeStampFile() const {return ftimeStampFile;}
};


void* udpDataReceiver(void* ptr);
void* udpBufferAdjust(void* ptr);
void* udpserverFIT(void* ptr);

} // end of name space



#endif

