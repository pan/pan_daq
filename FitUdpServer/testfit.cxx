#include "FitUdpServer.hxx"


void testfit() {

  HerdFit::FitEvent hfevent(384,20); // 384 channels, buffer of 20 events
  vushort vevent;

  printf("Starting Thread 1\n");
  TThread *thread1 = new TThread("thread1", HerdFit::udpserverFIT, (void*) &hfevent);
  thread1->Run();
  //h1->Join();


  uint bufferpos{0}, status{0}, counter{0};
  //uint oldcounter{std::numeric_limits<uint>::max()}; // largest unsigned int, most probably never reached in buffer counter
  uint oldcounter{0};
  uint lostcounter{0};
  
  while (1) {

    //cout << "Total event counter is: " << hfevent.GetTotalEventCounter() << endl;
    //cout << "Number of incomplete events: " << hfevent.GetIncompleteEvents()  << endl;
    //cout << "Number of missed events: " << hfevent.GetMissedEvents() << endl;
    //sleep(1);


    //TThread::Ps();

    status = hfevent.GetEntryStatus(bufferpos);
    counter = hfevent.GetEntryCounter(bufferpos);
    lostcounter = hfevent.GetLostEvents();
    //cout << "old counter = " << oldcounter << "   counter = " << counter << "   status = " << status << endl;
    
    if (status==1) {
      cout << "counter = " << counter << "   status = " << status << "  lost (could not write into buffer) = " << lostcounter << endl;
      vevent=hfevent.GetEntryEvent(bufferpos);
      cout << "new status = " << hfevent.GetEntryStatus(bufferpos) << endl;
      oldcounter=counter;

      cout << "buffer status: " << hfevent.GetNStatus(0) << " with status 0, " << hfevent.GetNStatus(1) << " with status 1, " << hfevent.GetNStatus(2) << " with status 2" << endl; 
    }
    bufferpos++;
    usleep(1);
  }
  
}
