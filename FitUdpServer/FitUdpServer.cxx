

#include "FitUdpServer.hxx"

namespace HerdFit{

TMutex bufferMutex;
TCondition bufferCV(&bufferMutex);
  
  FitEvent::FitEvent() {
    fNchannels = NCHANNELS;
    fEventBufferSize = EVENTBUFFERSIZE;
    fNFEBboards = 1;
    fNADCCH = fNchannels / fNADC;
      fDataBufferSize = 1000;
    Clear();
  }

  FitEvent::FitEvent(size_t nchan, size_t bufsize, size_t nboards, size_t nadc) {
    fNFEBboards = nboards;
    fNchannels = (nchan>0) ? nchan:NCHANNELS ;
    fEventBufferSize = (bufsize>0) ? bufsize:EVENTBUFFERSIZE ;
    fNADC = nadc;
    fNADCCH = fNchannels / fNADC;
      fDataBufferSize = 1000;
    Clear();
  }

FitEvent::~FitEvent() {
    if (fmsg) {
        delete [] fmsg;
        fmsg = nullptr;
    }
}


  void FitEvent::Clear() {
    vushort vempty;
    vempty.assign(fNchannels,0);
    fvEventBuffer.clear();
    fvEventBuffer.shrink_to_fit();
    fvEventBuffer.assign(fEventBufferSize, vempty);

    vuint vempty2;
    vempty2.clear();
    vempty2.assign(2,0);
    fvEventStatus.clear();
    fvEventStatus.shrink_to_fit();
    fvEventStatus.assign(fEventBufferSize, vempty2);

    fDataCounter=0;
    fLastEventId=0;

    fTotalEventCounter=0;
    fIncompleteEvents=0;
    fMissedEvents=0;
    fLostEvents=0;
    fInitReading=true;
    fFRBEnabled=false;
    fPANEnabled=true;
    //MAX_MSG=3072;
  }
  
  void FitEvent::SetEventBufferSize(size_t buffSize) {
    fEventBufferSize = buffSize;
    Clear();
  }


  void FitEvent::BuildEvent(unsigned char *msg, std::size_t len) {

    if (len != (int) MAX_MSG) {
      std::cout << "FitUdpServer: len problem, event rejected, len = " << len << " != " << MAX_MSG << std::endl;
      return;
    }
    // cout << " FitUdpServer: data length = " << dec << len << " i.e. " << len/4. << " 32-bits words" << endl; 
    //fDataCounter = 0;
    vushort vevent;
    vevent.assign(fNchannels,0);
    
      uint value32{0}, channel{0}, feb{0}, adcIndex{0}, channelIndex{0}, ev_id{0};//, datacounter{0}, lasteventid{0};
      ushort adc1{0}, adc2{0}, trigger{0}, chXIndex{0}, chYIndex{0};
    bool data_saved{false};
    unsigned long long int timeStamp{0};
    
    if (fFRBEnabled) {
        for (int i = 0; i < len; i+=4) {
	  value32 = (uint)msg[i]+((uint)msg[i+1]<<8)+((uint)msg[i+2]<<16)+((uint)msg[i+3]<<24);
	  adc1 = value32 & 0xFFF;
	  channel = (value32 >> 12)  & 0x1FF;
	  feb = (value32 >> 21) & 0x1F;
	  ev_id = (value32 >> 26) & 0x3F ;
	  
	  //cout << "FitUdpServer: i = " << i << "    ev_id = " << ev_id << "   feb = " << feb << "    channel = " << channel << endl;
	  
	  if (feb>=fNFEBboards || channel>=fNchannels || value32==0xffffffff) 
	    {
	      //cout << "warning: feb = " << feb << " channel = " << channel << " val = 0x"<< hex << value32 << dec << endl;
	      continue;
	    }
	  
	  if (fInitReading) {
	    fLastEventId=ev_id;
	    fInitReading=false;
	  }

	  vevent.at(channel + feb * fNchannels)=adc1;
	  //cout << Form("channel = %d, feb = %d, event = %d, adc = %d",channel,feb,eventnumber, adc) << endl;
	  fDataCounter++;
	  
	  if (channel==(fNchannels-1) && feb==fNFEBboards-1) { //cout << "FitUdpServer: finished reading out one event" << endl;
	  
	  //if (fLastEventId!=ev_id) {
	    // cout << "FitUdpServer: saving event, fLastEventId = " << fLastEventId << "  ev_id = " << ev_id << endl;
	    // we look for an empty space
	    data_saved=false;
	    for (size_t j{0}; j<fEventBufferSize; j++) {
	      size_t pos{(j + fTotalEventCounter) % fEventBufferSize };
	      if ((fvEventStatus.at(pos)).at(1)==0 || (fvEventStatus.at(pos)).at(1)==2) { // 0: not used yet, 2: data already read and thus buffer position available
		fvEventBuffer.at(pos)=vevent;
		(fvEventStatus.at(pos)).at(0)=fTotalEventCounter;
		(fvEventStatus.at(pos)).at(1)=1; // data written, not read yet, this value should be changed to 2 once the event is read by the analysis program
		data_saved=true;
		break; // we exit from the loop as we have found a place where to store the data
	      }
	    }
	    if (data_saved==false) {
	      fLostEvents++;
	      // cout << "FitUdpServer: could not write into buffer" << endl;
	    }
	    fTotalEventCounter++;
	    if (fDataCounter!=fNchannels * fNFEBboards) {
	      fIncompleteEvents++;
	      std::cout << "FitUdpServer: look out ! incomplete event: fDataCounter = " << fDataCounter << std::endl;
	    }
	    //else cout << "event is complete !!!" << endl;
	    if (ev_id!=(fLastEventId+1)%64) { // event counter on 6 bits
	      if (ev_id<fLastEventId) fMissedEvents=64+ev_id-(fLastEventId+1);
	      else fMissedEvents=ev_id-(fLastEventId+1);
	      std::cout << "FitUdpServer: at least one event has been missed" << std::endl;
	    }
	    fLastEventId=ev_id;
	    fDataCounter=0;
	    vevent.clear();
	    vevent.assign(fNchannels * fNFEBboards,0);
	    // cout << "FitUdpServer: new event: " << ev_id << endl;
	  }
	   
	  //cout << dec << "event = " << ev_id << endl;
	  //cout << dec << "feb = " << feb << endl;
	  //cout << dec << "adc: " << adc1 << endl;
	  //cout << dec << "value32: " << ev_id << " " << feb << " " << channel << " " << adc1 << " end-value32 ";
	  
	}
    }
    else if (fPANEnabled) {
        
        constexpr unsigned int FRAME_START{0x80AAAAAA};
        constexpr unsigned int FRAME_END{0x80333333};
        
        ushort frame_type{0}, tag{0}, strip_type{0}, wordIndex{0}, code0{0}, code1{0};
		bool startOfFrame{false};
        for (int i{0}; i<len; i+=4) {
            
            value32 = (uint)msg[i]+((uint)msg[i+1]<<8)+((uint)msg[i+2]<<16)+((uint)msg[i+3]<<24);
            
            if (value32 == FRAME_START) {
                wordIndex = 0;
				startOfFrame = true;
                continue;
            }

	    if (value32 >> 24 == 0x82) {
                ev_id = value32 & 0xFFFFFF;
				timeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                vtimeStamp.push_back(timeStamp);
				continue;
            }

	    if (!startOfFrame) continue;
                
            if (value32 == FRAME_END || value32 >> 24 == 0x84) {
				startOfFrame = false;
                chXIndex = 0;
                chYIndex = 0;
                continue;
            }
            
            if (value32 >> 24 == 0x81) {
                code0 = value32 & 0xFF;
                code1 = (value32 >> 8) & 0xFF;
                continue;
            }
            
            if (value32 >> 24 == 0x83) {
                ev_id += ((value32 >> 16) & 0xFF) << 24;
                tag = value32 & 0x7;
                fDataCounter = 0;
                continue;
            }
            
            if (value32 >> 31 == 0x1) {
                continue;
            }
            
            strip_type = (value32 >> 29) & 0x3;
            if (strip_type == 2) {
                adc1 = value32 & 0x3FFFFFF;
            }
            else {
                adc1 = value32 & 0x1FFF;
                adc2 = (value32 >> 13) & 0x1FFF;
            }
            
            if ((code0 == 0x30 || code0 == 0x31) && strip_type < 2) strip_type = 0;
            
            if (((value32 >> 26) & 0x7) != tag) {
                std::cout << "FitUdpServer: event tag changed within the event frame!" << std::endl;
                fDataCounter++;
                continue;
            }
            
            if (fDataCounter >= fNchannels) {
            	fDataCounter = 0;
                return;
            }
            
            if (fInitReading) {
              fLastEventId=ev_id;
              fInitReading=false;
            }
            
            switch (wordIndex) {
                case 0:
                    adcIndex = 0;
                    break;
                    
                case 1:
                    adcIndex = 2;
                    break;
                    
                case 2:
                    adcIndex = 4;
                    break;
                    
                case 3:
                    adcIndex = 6;
                    break;
            }
            
            if (code0 == 0x30 || code0 == 0x31) {
                if (strip_type == 2) {
                    channel = 2048 + chYIndex;
                    vevent.at(channel) = adc1;
                }
                else {
                    channel = adcIndex * 2048 / 8 + chXIndex;
                    vevent.at(channel) = adc1;
                    channel = (adcIndex + 1) * 2048 / 8 + chXIndex;
                    vevent.at(channel) = adc2;
                }
            }
            else if (code0 == 0x32) {
                if (strip_type == 2) {
                    channel = 2048 * 2 + chYIndex;
                    vevent.at(channel) = adc1;
                }
                else {
                    channel = strip_type * 2048 + adcIndex * 2048 / 8 + chXIndex;
                    vevent.at(channel) = adc1;
                    channel = strip_type * 2048 + (adcIndex + 1) * 2048 / 8 + chXIndex;
                    vevent.at(channel) = adc2;
                }
            }
            else {
                channel = chYIndex;
                vevent.at(channel) = adc1;
            }
            
            if (strip_type == 2) chYIndex++;
            else {
                if (code0 != 0x32) {
                    if (wordIndex == 3) chXIndex++;
                }
                else {
                    if (wordIndex == 3 && strip_type == 1) chXIndex++;
                }
            }
            
            if (code0 != 0x33 && strip_type < 2) {
                wordIndex++;
                if (wordIndex > 3) wordIndex = 0;
            }
            
            fDataCounter++;
            
            if (channel==(fNchannels-1)) {
                data_saved=false;
                for (size_t j{0}; j<fEventBufferSize; j++) {
                    size_t pos{(j + fTotalEventCounter) % fEventBufferSize };
                    if ((fvEventStatus.at(pos)).at(1)==0 || (fvEventStatus.at(pos)).at(1)==2) { // 0: not used yet, 2: data already read and thus buffer position available
                        fvEventBuffer.at(pos)=vevent;
                        (fvEventStatus.at(pos)).at(0)=fTotalEventCounter;
                        (fvEventStatus.at(pos)).at(1)=1; // data written, not read yet, this value should be changed to 2 once the event is read by the analysis program
						data_saved=true;
                        break; // we exit from the loop as we have found a place where to store the data
                    }
                }
                if (data_saved==false) {
                  fLostEvents++;
                  // cout << "FitUdpServer: could not write into buffer" << endl;
                }
                fTotalEventCounter++;
                if (fDataCounter != fNchannels) {
                  fIncompleteEvents++;
                  std::cout << "FitUdpServer: look out ! incomplete event: fDataCounter = " << fDataCounter << std::endl;
                }
                //else cout << "event is complete !!!" << endl;
                if (ev_id != (fLastEventId + 1)) { // event counter on 24 bits
                  fMissedEvents += ev_id - (fLastEventId + 1);
                  std::cout << "FitUdpServer: at least one event has been missed" << std::endl;
                }
                fLastEventId = ev_id;
                fDataCounter = 0;
                vevent.clear();
                vevent.assign(fNchannels,0);
                // cout << "FitUdpServer: new event: " << ev_id << endl;
            }
        }
    }
    else {    
	    for (int i{0}; i<len; i+=4) {

	      value32 = (uint)msg[i]+((uint)msg[i+1]<<8)+((uint)msg[i+2]<<16)+((uint)msg[i+3]<<24);
	      //cout << hex << value32 << " ";
	      trigger = value32>>30;
	      ev_id = (value32>>24) & 0x3f;
	      adc2 = (value32>>12) & 0xfff;
	      adc1 = value32 & 0xfff;
	      
	      if (fInitReading) {
			fLastEventId=ev_id;
			fInitReading=false;
	      }
	      if (fLastEventId!=ev_id) {
		// we look for an empty space
		data_saved=false;
		for (size_t j{0}; j<fEventBufferSize; j++) {
		  size_t pos{(j + fTotalEventCounter) % fEventBufferSize };
		  if ((fvEventStatus.at(pos)).at(1)==0 || (fvEventStatus.at(pos)).at(1)==2) { // 0: not used yet, 2: data already read and thus buffer position available
		    fvEventBuffer.at(pos)=vevent;
		    (fvEventStatus.at(pos)).at(0)=fTotalEventCounter;
		    (fvEventStatus.at(pos)).at(1)=1; // data written, not read yet, this value should be changed to 2 once the event is read by the analysis program
		    data_saved=true;
		    break; // we exit from the loop as we have found a place where to store the data
		  }
		}
		if (data_saved==false) {
		  fLostEvents++;
		  std::cout << "could not write into buffer" << std::endl;
		}
		fTotalEventCounter++;
		if (fDataCounter!=fNchannels/2) {
		  fIncompleteEvents++;
		  std::cout << "look out ! incomplete event: " << fDataCounter << std::endl;
		}
		//else cout << "event is complete !!!" << endl;
		if (ev_id!=(fLastEventId+1)%64) { // event counter on 6 bits
		  if (ev_id<fLastEventId) fMissedEvents=64+ev_id-(fLastEventId+1);
		  else fMissedEvents=ev_id-(fLastEventId+1);
		  std::cout << "at least one event has been missed" << std::endl;
		}
		fLastEventId=ev_id;
		fDataCounter=0;
		vevent.clear();
		vevent.assign(fNchannels * fNFEBboards,0);
		//cout << "new event: " << ev_id << endl;
	      }

	      vevent.at(2*fDataCounter)=adc1;
	      vevent.at(2*fDataCounter+1)=adc2;
	      
	      //fEventBuffer[fTotalEventCounter % EVENTBUFFERSIZE][2*fDataCounter]=adc1;
	      //fEventBuffer[fTotalEventCounter % EVENTBUFFERSIZE][2*fDataCounter+1]=adc2;

	      //cout << "trigger = " << trigger << endl;
	      //cout << dec << "event = " << ev_id << endl;
	      //cout << dec << "adc1, adc2: " << adc1 << "  " << adc2 << endl;
	      //cout << dec << "value32: " << trigger << " " << ev_id << " " << adc1 << " " << adc2 << " end-value32 ";
	      fDataCounter++;
	    }
    }
    //cout << endl;

    //fBufferPos++;
  
  }


  uint FitEvent::GetEntryStatus(uint bufferpos) const {

    bufferpos%=fEventBufferSize;
    uint status{ (fvEventStatus.at(bufferpos)).at(1) };
    return status;
  }


  uint FitEvent::GetEntryCounter(uint bufferpos) const {
  
    bufferpos%=fEventBufferSize;
    uint counter{ (fvEventStatus.at(bufferpos)).at(0) };
    return counter;
  }

  
  vushort FitEvent::GetEntryEvent(uint bufferpos) {
  
    bufferpos%=fEventBufferSize;
    vushort vret { fvEventBuffer.at(bufferpos) };
    //for (size_t i{0}; i<NCHANNELS; i++) vret.push_back(fEventBuffer[bufferpos][i]);
  
    (fvEventStatus.at(bufferpos)).at(1)=2; // we have read out the event, entry status goes to 2
  
    return vret;  
  }



  uint FitEvent::GetNStatus(uint status) const {

    uint  count{0};
    for (size_t pos{0}; pos<fEventBufferSize; pos++) count+=((fvEventStatus.at(pos)).at(1)==status);
    return count;
  }


  bool FitEvent::isBufferNearlyFull() const {
  
    return (GetNStatus(1)>(fEventBufferSize*0.75));
  }

  bool FitEvent::isBufferFull() const {
  
    return (GetNStatus(1)==fEventBufferSize);
  }
  
  bool FitEvent::isEventComplete(uint bufferpos) const {
    bufferpos%=fEventBufferSize;
    vushort vret { fvEventBuffer.at(bufferpos) };
    for (vushort::const_iterator i = vret.begin(); i != vret.end(); ++i) {
      if (*i == 0) return false;
    }
    return true;
  }
  
  uint FitEvent::GetDataCounter() const {
    return this->fDataCounter;
  }


int FitEvent::SetupUDPClient() {
	
    return 0;
}

int FitEvent::SetupUDPServer() {
    fsd=0;
    unsigned int max_msg = GetMaxMsg();
    fmsg = new unsigned char[max_msg];
    memset(fmsg,0x0,max_msg * sizeof(unsigned char));
    fsd=socket(AF_INET, SOCK_DGRAM, 0);
    if (fsd<0) {
        std::cerr << Form("cannot open socket\n");
        return 1;
    }
    return 0;
}

bool FitEvent::ConnectToUDP() {
	
    return true;

}

bool FitEvent::BindUDPServer() {
    struct sockaddr_in servAddr;
  
    // bind local server port
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = inet_addr(fServerAddress.c_str());
    servAddr.sin_port = htons(fPort);
    int reuse = 1;
    if (setsockopt(fsd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)) == -1) {
        std::cerr << "Error setting SO_REUSEADDR" << std::endl;
        CloseUDPServer();
        return false;
    }

    if (bind(fsd, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
        std::cerr << Form("cannot bind port number %d\n", fPort);
        CloseUDPServer();
        return false;
    }
    std::cout << "Listening for UDP data on " << fServerAddress << ":" << fPort << std::endl;
    return true;
}



  void FitEvent::CloseUDPServer() {
    close(fsd);
    fsd = -1;
    delete[] fmsg;
    fmsg = nullptr;
  }
  
  std::pair<unsigned char*, ssize_t> FitEvent::ReceiveFromClient(uint max_msg, int flags, struct sockaddr_in &cliAddr) {
    
      // init buffer
      memset(fmsg,0x0,max_msg * sizeof(unsigned char));
     
      // receive message 
      uint cliLen = sizeof(cliAddr);
      //cout << Form("len = %d", cliLen) << endl;
      ssize_t n = recvfrom(fsd, fmsg, max_msg, flags, (struct sockaddr *) &cliAddr, &cliLen);
      //cout << Form("n=%d",n) << endl;
      return {fmsg, n};
  }

std::pair<unsigned char*, int> FitEvent::ReceiveFromServer(uint max_msg, int flags, struct sockaddr_in cliAddr) {
    
    // init buffer
    memset(fmsg,0x0,max_msg * sizeof(unsigned char));
    
    // receive message 
    uint cliLen = sizeof(cliAddr);
    //cout << Form("len = %d", cliLen) << endl;
    // int n = recvfrom(fsd, fmsg, max_msg, flags, (struct sockaddr *) &cliAddr, &cliLen);
    int n = recv(fsd, fmsg, max_msg, 0);
    
    return {fmsg, n};
}


void* udpDataReceiver(void* ptr) {
    
    FitEvent* myevent{(FitEvent*) ptr};
    
    int flags{0};
    
    std::pair<unsigned char*, ssize_t> message;
    uint max_msg = myevent->GetMaxMsg();
    
    while (myevent->GetQuitLoop()) {
        message = myevent->ReceiveFromClient(max_msg, flags, myevent->cliAddr);
        
        if (message.second > 0) {
            HerdFit::bufferMutex.Lock();
            myevent->dataBuffer.push_back(std::string(reinterpret_cast<char*>(message.first), message.second));
            myevent->message_size_list.push_back(message.second);
            HerdFit::bufferMutex.UnLock();
            HerdFit::bufferCV.Broadcast();
        }
    }
    
    return 0;
}

void* udpserverFIT(void* ptr) {

    //int main(int argc, char *argv[]) {

	TThread::SetCancelOn();
    TThread::SetCancelDeferred();

    FitEvent* myevent {(FitEvent*)ptr }; 
  
    // struct sockaddr_in servaddr;
    // memset(&servaddr, 0, sizeof(servaddr));
    // servaddr.sin_family = AF_INET;
    // servaddr.sin_port = htons(myevent->GetPort());  /// Server Port
    // servaddr.sin_addr.s_addr = inet_addr((myevent->GetAddress()).c_str());  /// server ip
    uint max_msg = myevent->GetMaxMsg();
    
    unsigned char* full_event = new unsigned char[max_msg];
    memset(full_event, 0x0, max_msg * sizeof(unsigned char));
    uint size_shift{0};
    
    std::ofstream stream;
    std::ofstream timeStamps;
    
    //open output file
    stream.open("testdaq_from_UDP.daq", std::ofstream::binary); // write the binary file using messages from the UDP port
    if( !stream ) std::cout << "Opening file failed" << std::endl;
    timeStamps.open((Form("%s.unix",(myevent->GetTimeStampFile()).c_str())));

    uint ev_id = 0;
    uint event_size{0};
    bool eventBuiltFlag{false};

    unsigned long long int now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    timeStamps << "unix time at the start of the run in ms:  " << now << std::endl;

    // server infinite loop
    while (myevent->GetQuitLoop()) {
		TThread::CancelPoint();
        
        HerdFit::bufferMutex.Lock();
        
        while (myevent->dataBuffer.empty()) {
            if (HerdFit::bufferCV.Wait() >= 0) break;
        }
        
        std::string data = myevent->dataBuffer.front();
        ssize_t data_size = myevent->message_size_list.front();
        myevent->dataBuffer.erase(myevent->dataBuffer.begin());
        myevent->message_size_list.erase(myevent->message_size_list.begin());
        
        HerdFit::bufferMutex.UnLock();
        
        event_size += data_size;
        size_shift = event_size - data_size;
        if (event_size <= max_msg) {
            memcpy(full_event + size_shift, data.data(), data_size);
        }
        else {
            memcpy(full_event + size_shift, data.data(), (max_msg - size_shift) * sizeof(unsigned char));
            eventBuiltFlag = true;
        }
        
        if (stream) stream.write(reinterpret_cast<char*>(&(data.data())[0]), data_size);
        // print received message
        std::string hexData;
        for (char c : data) {
            char hex[5];
            snprintf(hex, sizeof(hex), "%02X ", static_cast<unsigned char>(c));
            hexData += hex;
        }
        
        std::cout << Form("From %s:UDP%u\n", inet_ntoa(myevent->cliAddr.sin_addr), ntohs(myevent->cliAddr.sin_port));
        
        for (size_t i = 0; i < hexData.length(); i += 36) {
            std::string line = hexData.substr(i, 36);
            std::cout << line << std::endl;
        }
        
        std::cout << "Length: " << data.length() << std::endl;
        std::cout << std::endl;
        
        (myevent->vtimeStamp).clear();
        (myevent->vtimeStamp).shrink_to_fit();
          
        if (eventBuiltFlag) {
            myevent->BuildEvent(full_event, max_msg);
            for (unsigned long long int timeStamp : myevent->vtimeStamp) {
                timeStamps << ev_id << " " << timeStamp - now << std::endl;
                ev_id++;
            }
            eventBuiltFlag = false;
            event_size -= max_msg;
            memset(full_event, 0x0, max_msg * sizeof(unsigned char));
            memcpy(full_event, data.data() + (max_msg - size_shift), event_size);
        }
		
      // message = myevent->ReceiveFromClient(max_msg, flags, cliAddr);
      /*
        message = myevent->ReceiveFromServer(max_msg, flags, servaddr);
        
      n = message.second;
      
      if (n<=0) {
        cout << ("cannot receive data\n");
        continue;
      }
      stream.write((char *)&(message.first)[0], n);
      // print received message
      cout << Form("From %s:UDP%u : %s\n", inet_ntoa(servaddr.sin_addr), ntohs(servaddr.sin_port), message.first);
      cout << Form("From %s:UDP%u\n", inet_ntoa(servaddr.sin_addr), ntohs(servaddr.sin_port));
      vtimeStamp.clear();
      vtimeStamp.shrink_to_fit();
      
      myevent->BuildEvent(message.first, n);
      for (unsigned long long int timeStamp : vtimeStamp) {
        timeStamps << ev_id << " " << timeStamp - now << endl;
        ev_id++;
      }
      */
      
    }/* end of server infinite loop */
	
	delete[] full_event;
    
    timeStamps.close();
    stream.close();
    
    return 0;
  }

void* udpBufferAdjust(void* ptr) {
    
    FitEvent* myevent {(FitEvent*)ptr };
    
    while (myevent->GetQuitLoop()) {
        sleep(1);
        if (myevent->dataBuffer.size() > myevent->GetDataBufferSize()) {
            HerdFit::bufferMutex.Lock();
            std::cout << "Adjusting buffer size..." << std::endl;
            myevent->dataBuffer.erase(myevent->dataBuffer.begin());
            HerdFit::bufferMutex.UnLock();
        }
    }
    return 0;
}


}
