//
//  scriptForSoverns.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 21.07.22.
//

#include <stdio.h>

std::vector<std::vector<double>> * sovernsPerChannelOut;

tracks_tree->SetBranchAddress("sovernsbr", &sovernsPerChannelOut);

TH2F * h2 = new TH2F("h2","Signal over noise per channel, layer 0;channel;S/N",25,0,25,60,-10,20);

for (std::size_t i{0}; i < tracks_tree->GetEntries(); ++i) {
    tracks_tree->GetEntry(i);
    for (std::size_t j{0}; j < (sovernsPerChannelOut->at(0)).size(); ++j) h2->Fill(j, (sovernsPerChannelOut->at(0)).at(j));
}

h2->Draw("colz");


// Merge files from beamtest

_file0->cd(); // select a file

TTree* tree0 = new TTree(); // create a tree

tree0 = ((TTree*) _file0->Get("sync_clusters_tree")); // get a tree

_file1->cd();

TTree* tree1 = new TTree();

tree1 = ((TTree*) _file1->Get("sync_clusters_tree"));

_file2->cd();

TTree* tree2 = new TTree();

tree2 = ((TTree*) _file2->Get("sync_clusters_tree"));

TFile *_fileOut = new TFile("synced_clusters_3angles.root","recreate");

_fileOut->cd();

TList ls_of_trees;

ls_of_trees.AddLast(tree0->CloneTree(20000));
ls_of_trees.AddLast(tree1->CloneTree(20000));
ls_of_trees.AddLast(tree2->CloneTree(20000));

TTree* tree_merged = new TTree();
tree_merged = tree_merged->MergeTrees(&ls_of_trees);

tree_merged->Write();

_fileOut->Close();
