//
//  main.cpp
//  Daq_to_ROOT_convertor
//
//  Created by Daniil Sukhonos on 07.08.23.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <memory>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/cxx11/any_of.hpp>

#include "TTree.h"
#include "TFile.h"

namespace fs = boost::filesystem;
namespace po = boost::program_options;

std::vector<std::string> filenames;  // Vector to hold filenames

std::vector<unsigned int> ReadFourWords(std::ifstream& afile)
{
    unsigned int adcValue{0};
    std::vector<unsigned int> fourWords;
    unsigned char val1{'\0'}, val2{'\0'}, val3{'\0'}, val4{'\0'};

    for (int i{0}; i < 4; i++)
    {
        afile.read(reinterpret_cast<char*>(&val1), sizeof(val1));
        afile.read(reinterpret_cast<char*>(&val2), sizeof(val2));
        afile.read(reinterpret_cast<char*>(&val3), sizeof(val3));
        afile.read(reinterpret_cast<char*>(&val4), sizeof(val4));
        adcValue = val1 + (val2 << 8) + (val3 << 16) + (val4 << 24);

        fourWords.push_back(adcValue);
    }

    return fourWords;
}

unsigned int ReadSingleWord(std::ifstream& afile) {
    
    unsigned char val1{'\0'}, val2{'\0'}, val3{'\0'}, val4{'\0'};
    
    afile.read(reinterpret_cast<char*>(&val1), sizeof(val1));
    afile.read(reinterpret_cast<char*>(&val2), sizeof(val2));
    afile.read(reinterpret_cast<char*>(&val3), sizeof(val3));
    afile.read(reinterpret_cast<char*>(&val4), sizeof(val4));
    
    return static_cast<unsigned int>(val1 + (val2 << 8) + (val3 << 16) + (val4 << 24));
}


void ConvertDaqToRootNew(const std::string& fFilename, unsigned int fNTOTCH)
{

    unsigned int FRAME_START{0x80AAAAAA};
    unsigned int FRAME_END{0x80333333};

    std::string outputFileName{fFilename.substr(0, fFilename.length() - 4)};
    outputFileName += ".root";

    unsigned short adcArray[fNTOTCH], channel_type{0};
    unsigned int eventNumber{0}, frameType{0}, eventTag{0}, event{0}, adc1{0}, adc2{0}, current_word{0}, big_frame_counter{0}, MSB{0}, OldMSB{0};
    ULong64_t frame_counter{0}, TScounter{0};
    Double_t eventTime[2];
    Double_t oldTime{0};
    eventTime[0]=0;
    eventTime[1] = 0;
    bool eventTagChanged{false};
    std::unique_ptr<TFile> out_file = std::make_unique<TFile>(outputFileName.c_str(), "recreate");

    TTree* T = new TTree("T", "T");
    T->Branch("adc", adcArray, Form("adc[%d]/s", fNTOTCH));
    T->Branch("nevent", &eventNumber, "nevent/i");
    T->Branch("frameType", &frameType, "frameType/i");
    T->Branch("eventTag", &eventTag, "eventTag/i");
    T->Branch("nframe", &frame_counter, "frame_counter/l");
    T->Branch("eventTime", eventTime, "eventTimeSt/D:deltaTime/D");
    
    std::ifstream file(fFilename, std::ios::binary | std::ios::ate);
    
    std::vector<unsigned int> data;
    
    if (file.is_open()) {
        
        // to understand progress, let's find the file size
        long int FileSize{file.tellg()};
        file.seekg(0, std::ios::beg); // Move back to the beginning
        int FileProgressStep{0};
        double FileProgress{static_cast<double>(file.tellg()) / FileSize * 100};
        
        while (1)
        {
            if (file.eof())
                break;
            FileProgress = static_cast<double>(file.tellg()) / FileSize * 100;
            if (FileProgress>=FileProgressStep*5) {
                std::cout << "File conversion progress: " << round(FileProgress) << " %"<< std::endl;
                FileProgressStep++;
            }
            data.clear();
            data = ReadFourWords(file);

            if (data.at(0) == FRAME_START)
            {
                frameType = data.at(1);
                eventNumber = data.at(2) & 0xFFFFFF;
                eventNumber += ((data.at(3) >> 16) & 0xFF) << 24;
                eventTag = data.at(3) & 0x7;
                eventTagChanged = false;

                unsigned int code0{frameType & 0xFF};
                unsigned int code1{(frameType >> 8) & 0xFF};
                unsigned int nchannels{2048 / 8}; // sum over all fNLCH / fNADC per board?
                if (code0 == 0x33) nchannels = 128;
                if (code1 == 0x59 && code0 != 0x33) {
                    nchannels = 2048 / 2;
                    nchannels += 128;
                    if (code0 == 0x32) nchannels += 2048 / 2;
                }
                unsigned int first{0};
                unsigned int channel{0}, stripYchannel{0}, stripX0channel{0}, stripX1channel{0};
                unsigned int stripX1adc{0}, stripX0adc{0};

                // now we read the data.
                for (unsigned int i{0}; i < nchannels; i++)
                {
                    if (code1 == 0x58)
                    {
                        data.clear();
                        data = ReadFourWords(file);
                        for (int j{0}; j < 4; j++)
                        {
                            adc1 = data.at(j) & 0x1FFF;
                            adc2 = (data.at(j) >> 13) & 0x1FFF;
                            if (((data.at(j) >> 26) & 0x7) != eventTag)
                            {
                                std::cout << "A: event tag changed within the event, skipping to the next one..." << std::endl;
                                std::cout << "iteration: " << event << std::endl;
                                std::cout << "eventTag of a frame: " << eventTag << std::endl;
                                std::cout << "new eventTag: " << ((data.at(j) >> 26) & 0x7) << std::endl;
                                std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                                std::cout << "eventNumber: " << eventNumber << std::endl;
                                eventTagChanged = true;
                                current_word = data.at(j);
                                break;
                            }
                            channel = 2 * j * 2048 / 8 + i;
                            adcArray[first + channel] = adc1;
                            channel = (2 * j + 1) * 2048 / 8 + i;
                            adcArray[first + channel] = adc2;
                        }
                        if (eventTagChanged) {
                            while (!file.eof()) // important in particular if we are reading a file not closed yet (i.e. while DAQ is still running)
                            {
                                std::cout << "A: current_word = 0x" << std::hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << std::dec << std::endl;
                                //if (((current_word >> 24) & 0xFF) == 0x84) {
                                //    std::cout << "0x84 found - breaking" << std::endl;
                                //    break;
                                //}
                                // there is a bug in the FW: the CRC word might not be written correcly anymore once we have an uncomplete event (the 0x84 identifier is not written - for the least), and this happens also for all the successive events
                                if (current_word == 0x80333333){ // as a replacement: we find the end frame word, then we read 3 more words
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    break;
                                }
                                current_word = ReadSingleWord(file);
                            }
                            break;
                        }
                        if (code0 == 0x32)
                        {
                            data.clear();
                            data = ReadFourWords(file);
                            for (int j{0}; j < 4; j++)
                            {
                                adc1 = data.at(j) & 0x1FFF;
                                adc2 = (data.at(j) >> 13) & 0x1FFF;
                                if (((data.at(j) >> 26) & 0x7) != eventTag)
                                {
                                    std::cout << "B: event tag changed within the event, skipping to the next one..." << std::endl;
                                    std::cout << "iteration: " << event << std::endl;
                                    std::cout << "eventTag of a frame: " << eventTag << std::endl;
                                    std::cout << "new eventTag: " << ((data.at(j) >> 26) & 0x7) << std::endl;
                                    std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                                    std::cout << "eventNumber: " << eventNumber << std::endl;
                                    eventTagChanged = true;
                                    current_word = data.at(j);
                                    break;
                                }
                                channel = 2 * j * 2048 / 8 + i;
                                adcArray[first + channel + 2048] = adc1;
                                channel = (2 * j + 1) * 2048 / 8 + i;
                                adcArray[first + channel + 2048] = adc2;
                            }
                        }
                        if (eventTagChanged) {
                            while (!file.eof()) // important in particular if we are reading a file not closed yet (i.e. while DAQ is still running)
                            {
                                std::cout << "B: current_word = 0x" << std::hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << std::dec << std::endl;
                                //if (((current_word >> 24) & 0xFF) == 0x84) {
                                //    std::cout << "0x84 found - breaking" << std::endl;
                                //    break;
                                //}
                                // there is a bug in the FW: the CRC word might not be written correcly anymore once we have an uncomplete event (the 0x84 identifier is not written - for the least), and this happens also for all the successive events
                                if (current_word == 0x80333333){ // as a replacement: we find the end frame word, then we read 3 more words
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    break;
                                }
                                current_word = ReadSingleWord(file);
                            }
                            break;
                        }
                    }
                    else if (code1 == 0x59 && code0 != 0x33)
                    {
                        current_word = ReadSingleWord(file);
                        channel_type = (current_word >> 29) & 0x3;
                        if (channel_type != 3)
                        {
                            adc1 = current_word & 0x1FFF;
                            adc2 = (current_word >> 13) & 0x1FFF;
                            if (((current_word >> 26) & 0x7) != eventTag)
                            {
                                std::cout << "C: event tag changed within the event, skipping to the next one..." << std::endl;
                                std::cout << "iteration: " << event << std::endl;
                                std::cout << "eventTag of a frame: " << eventTag << std::endl;
                                std::cout << "new eventTag: " << ((current_word >> 26) & 0x7) << std::endl;
                                std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                                std::cout << "eventNumber: " << eventNumber << std::endl;
                                std::cout << "current word = 0x" << std::hex << current_word << "    adc1 = 0x" << adc1 << "    adc2 = 0x" << adc2 << std::dec << std::endl;
                                eventTagChanged = true;
                                while (!file.eof()) // important in particular if we are reading a file not closed yet (i.e. while DAQ is still running)
                                {
                                    std::cout << "C: current_word = 0x" << std::hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << std::dec << std::endl;
                                    //if (((current_word >> 24) & 0xFF) == 0x84) {
                                    //    std::cout << "0x84 found - breaking" << std::endl;
                                    //    break;
                                    //}
                                    // there is a bug in the FW: the CRC word might not be written correcly anymore once we have an uncomplete event (the 0x84 identifier is not written - for the least), and this happens also for all the successive events
                                    if (current_word == 0x80333333){ // as a replacement: we find the end frame word, then we read 3 more words
                                        current_word = ReadSingleWord(file);
                                        current_word = ReadSingleWord(file);
                                        current_word = ReadSingleWord(file);
                                        break;
                                    }
                                    current_word = ReadSingleWord(file);
                                }
                                break;
                            }
                            if (code0 != 0x32 || channel_type == 0)
                            {
                                channel = 2 * stripX0adc * 2048 / 8 + stripX0channel;
                                adcArray[first + channel] = adc1;
                                channel = (2 * stripX0adc + 1) * 2048 / 8 + stripX0channel;
                                adcArray[first + channel] = adc2;
                                stripX0adc++;
                                if (stripX0adc > 3) {
                                    stripX0adc = 0;
                                    stripX0channel++;
                                }
                            }
                            else if (channel_type == 1)
                            {
                                channel = 2 * stripX1adc * 2048 / 8 + stripX1channel;
                                adcArray[first + channel + 2048] = adc1;
                                channel = (2 * stripX1adc + 1) * 2048 / 8 + stripX1channel;
                                adcArray[first + channel + 2048] = adc2;
                                stripX1adc++;
                                if (stripX1adc > 3)
                                {
                                    stripX1adc = 0;
                                    stripX1channel++;
                                }
                            }
                        }
                        else
                        {
                            adc1 = current_word & 0x3FFFFFF;
                            if (((current_word >> 26) & 0x7) != eventTag)
                            {
                                std::cout << "D: event tag changed within the event, skipping to the next one..." << std::endl;
                                std::cout << "iteration: " << event << std::endl;
                                std::cout << "eventTag of a frame: " << eventTag << std::endl;
                                std::cout << "new eventTag: " << ((current_word >> 26) & 0x7) << std::endl;
                                std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                                std::cout << "eventNumber: " << eventNumber << std::endl;
                                eventTagChanged = true;
                                while (!file.eof())
                                {
                                    //if (current_word >> 24 == 0x84)
                                    //    break;
                                    //current_word = ReadSingleWord(file);
                                    //cout << "D: current_word = 0x" << hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << dec << endl;
                                    if (current_word == 0x80333333)
                                    { // as a replacement: we find the end frame word, then we read 3 more words
                                        current_word = ReadSingleWord(file);
                                        current_word = ReadSingleWord(file);
                                        current_word = ReadSingleWord(file);
                                        break;
                                    }
                                    current_word = ReadSingleWord(file);
                                }
                                break;
                            }
                            if (code0 != 0x32)
                            {
                                channel = stripYchannel;
                                adcArray[first + 2048 + channel] = adc1;
                            }
                            else
                            {
                                channel = stripYchannel;
                                adcArray[first + 2 * 2048 + channel] = adc1;
                            }
                            stripYchannel++;
                        }
                    }
                    else {
                        data.clear();
                        data = ReadFourWords(file);
                        for (int j{0}; j < 4; j++)
                        {
                            adc1 = data.at(j) & 0x3FFFFFF;
                            if (((data.at(j) >> 26) & 0x7) != eventTag)
                            {
                                std::cout << "E: event tag changed within the event, skipping to the next one..." << std::endl;
                                std::cout << "iteration: " << event << std::endl;
                                std::cout << "eventTag of a frame: " << eventTag << std::endl;
                                std::cout << "new eventTag: " << ((data.at(j) >> 26) & 0x7) << std::endl;
                                std::cout << "(eventNumber + 1) % 8 = " << (eventNumber + 1) % 8 << std::endl;
                                std::cout << "eventNumber: " << eventNumber << std::endl;
                                eventTagChanged = true;
                                current_word = data.at(j);
                                break;
                            }
                            channel = j + i;
                            adcArray[first + channel] = adc1;
                        }
                        i += 3;
                        if (eventTagChanged) {
                            while (!file.eof()) // important in particular if we are reading a file not closed yet (i.e. while DAQ is still running)
                            {
                                std::cout << "A: current_word = 0x" << std::hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << std::dec << std::endl;
                                //if (((current_word >> 24) & 0xFF) == 0x84) {
                                //    std::cout << "0x84 found - breaking" << std::endl;
                                //    break;
                                //}
                                // there is a bug in the FW: the CRC word might not be written correcly anymore once we have an uncomplete event (the 0x84 identifier is not written - for the least), and this happens also for all the successive events
                                if (current_word == 0x80333333){ // as a replacement: we find the end frame word, then we read 3 more words
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    current_word = ReadSingleWord(file);
                                    break;
                                }
                                current_word = ReadSingleWord(file);
                            }
                            break;
                        }
                    }
                }
            }
            else if (data.at(0) == FRAME_END)
            {
                MSB=(data.at(1) & 0x3F);
                if (OldMSB>MSB){
                    //cout << hex << "oldmsb =0x" << OldMSB << " MSB=0x" << MSB << dec << endl;
                    big_frame_counter++;
                }
                OldMSB = MSB;
                frame_counter = static_cast<ULong64_t>((data.at(1)) & 0x3F) << 31; // !!! very nasty !!! you need to force the conversion to ULong64_t else bits >31 will be cut (i.e. in this case all of them...).
                frame_counter += (data.at(2) & 0x7FFFFFFF);
                //if (frame_counter_prev > frame_counter)
                //    big_frame_counter++;
                //frame_counter_prev = frame_counter;
                TScounter = frame_counter + (static_cast<ULong64_t>(big_frame_counter * 0x40) << 31);
                eventTime[0] = TScounter * (1./40.e+6); // Clock freq is 40 MHz
                eventTime[1] = eventTime[0]-oldTime;
                oldTime = eventTime[0];
                //cout << "trigger = " << data.at(3) << "  bigcnt = " << big_frame_counter << "  " << TScounter << "   " << eventTime[0] << endl;
                if (!eventTagChanged)
                {
                    T->Fill();
                    memset(&adcArray, 0, sizeof(adcArray));
                }
                event++;
                // counter = 0;
            } else {
                if (file.eof())
                    break;
                std::cout << "Event " << eventNumber <<  " :  Exception: 0x" << std::hex << data.at(0) << "  0x" << data.at(1) << "  0x" << data.at(2) << "  0x" << data.at(3) << std::endl;
                std::cout << "we try to locate again the end frame" << std::endl;
                current_word = ReadSingleWord(file);
                while (!file.eof())
                {
                    // if (current_word >> 24 == 0x84)
                    //     break;
                    // current_word = ReadSingleWord(file);
                    //cout << "Excpt: current_word = 0x" << hex << current_word << "  last 8 bits: 0x" << ((current_word >> 24) & 0xFF) << dec << endl;
                    if (current_word == 0x80333333)
                    { // as a replacement: we find the end frame word, then we read 3 more words
                        current_word = ReadSingleWord(file);
                        current_word = ReadSingleWord(file);
                        current_word = ReadSingleWord(file);
                        break;
                    }
                    current_word = ReadSingleWord(file);
                }
            }
        }
    } else {
        std::cerr << "Failed to open file: " << fFilename << std::endl;
    }
    T->Write(0, TObject::kOverwrite);
    out_file->Write(0, TObject::kOverwrite);
    
    T->Delete();
    out_file->Close();
}

int main(int argc, const char* argv[]) {
    
    fs::path directoryPath;
    unsigned int nStripX, nStripY;
    bool overwriteFlag;

    // Declare command line options
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "Display help message")
        ("nstripx,x", po::value<unsigned int>(&nStripX)->default_value(2), "Number of stripX detectors")
        ("nstripy,y", po::value<unsigned int>(&nStripY)->default_value(0), "Number of stripY detectors")
        ("overwrite,o", po::value<bool>(&overwriteFlag)->default_value(false), "Overwrite existing root files")
        ("dir,d", po::value<std::string>(), "Directory containing .daq files");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 1;
    }
    
    // Check if the 'dir' option is provided
    if (vm.count("dir")) {
        directoryPath = vm["dir"].as<std::string>();
    }
    else {
        std::cerr << "Please provide the directory containing .daq files using --dir option." << std::endl;
        return 2;
    }
    
    unsigned int nTOTCH{nStripX * 2048 + nStripY * 128};
    
    // Populate the filenames queue with filenames to be processed
    fs::directory_iterator dirIter(directoryPath);
    bool rootFileExists{true};
    for (const auto& entry : dirIter) {
        if (!overwriteFlag && entry.path().extension() == ".daq") {
            // Reset the directory iterator every time
            fs::directory_iterator dirIterFind = fs::directory_iterator(directoryPath);
            rootFileExists = boost::algorithm::any_of(fs::begin(dirIterFind), fs::end(dirIterFind), [&entry](const fs::directory_entry& e) {
                return (entry.path().stem() == e.path().stem()) && (e.path().extension() == ".root");
            });
            if (rootFileExists) {
                continue;
            }
            else rootFileExists = true;
        }
        
        if (entry.path().extension() == ".daq") {
            filenames.push_back(entry.path().string());
        }
    }
    
    // Wait for all threads to finish
    std::size_t i{1};
    if (filenames.size() > 0) {
        for (std::string& filename : filenames) {
            
            std::cout << "Processing file: " << filename << std::endl;
            std::cout << "File " << i << " out of " << filenames.size() << std::endl;
            
            ConvertDaqToRootNew(filename, nTOTCH);
            ++i;
        }
    } else {
        std::cout << "All files in the " << directoryPath << " have been already converted! If you want to overwrite them, run the program with -o true option." << std::endl;
    }

    return 0;
}
