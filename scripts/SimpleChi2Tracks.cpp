//
//  SimpleChi2Tracks.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 02.12.21.
//

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TVectorD.h"
#include "TFitResult.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <unordered_map>
#include <algorithm>
#include <numeric>

typedef unsigned int uint;
typedef vector<double> vdouble;
typedef vector<int> vint;
typedef vector<string> vstring;

constexpr std::size_t NLAD{2};
constexpr std::size_t NBOARD{3};
constexpr std::size_t NBRANCH{4};

vdouble detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81
vdouble detectorPosErr = {0.05, 0.05, 0.05, 0.05, 0.05, 0.05};
std::array<vdouble, NLAD * NBOARD> deltas;

vdouble transformVectorsDouble(std::unordered_map<int, vdouble *> &vec, std::unordered_map<int, vdouble *> &vsoverns, bool errorsFlag = false, bool alignmentFlag = false) {
    vdouble outVec;
    double alpha{0.0};
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (errorsFlag) {
            alpha = sqrt(1.5 + vec[i]->at(0));
            if (vec[i]->at(0) == 1) outVec.push_back(1.0 / sqrt(12.0));
            else outVec.push_back(alpha / vsoverns[i]->at(0)); // due to CoG calculation
        }
        else {
            if (!alignmentFlag) outVec.push_back(vec[i]->at(0));
            else outVec.push_back(vec[i]->at(0) + std::accumulate(deltas[i].begin(), deltas[i].end(), 0.0));
        }
    }
    
    return outVec;
}

vdouble transformVectorsInt(std::unordered_map<int, vint *> &vec, std::unordered_map<int, vdouble *> &vsoverns, bool errorsFlag = false, bool alignmentFlag = false) {
    
    std::unordered_map<int, vdouble *> convVec;
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        convVec[i] = new vdouble(vec[i]->begin(), vec[i]->end());
    }
    
    return transformVectorsDouble(convVec, vsoverns, errorsFlag, alignmentFlag);
}

TVectorD removeElement(std::size_t i, TVectorD * vecOrig) {
    TVectorD outVec, tempVecFirst(*vecOrig), tempVecSecond(*vecOrig);
    
    tempVecFirst.ResizeTo(vecOrig->GetLwb(), i-1);
    tempVecSecond.ResizeTo(i+1,vecOrig->GetUpb());
    outVec.ResizeTo(vecOrig->GetNrows() - 1);
    outVec.SetSub(0, tempVecFirst);
    outVec.SetSub(tempVecFirst.GetNrows(), tempVecSecond);
    
    return outVec;
}

void evaluateUnbiasedResiduals(std::vector<float> &yErrors, TVectorD * yCoord, TVectorD * yErr, TVectorD * zCoord, TVectorD * zErr, TF1 * &fitFunc) {
    
    TGraphErrors *gr;
    TVectorD yCoordMod, yErrMod, zCoordMod, zErrMod;
    yErrors.clear();
    yErrors.shrink_to_fit();
    
    yCoordMod.ResizeTo(NLAD * NBOARD - 1);
    yErrMod.ResizeTo(NLAD * NBOARD - 1);
    zCoordMod.ResizeTo(NLAD * NBOARD - 1);
    zErrMod.ResizeTo(NLAD * NBOARD - 1);
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        
        yCoordMod = removeElement(i, yCoord);
        yErrMod = removeElement(i, yErr);
        zCoordMod = removeElement(i, zCoord);
        zErrMod = removeElement(i, zErr);
        
        gr = new TGraphErrors(zCoordMod, yCoordMod, zErrMod, yErrMod);
        fitFunc->SetParameter(0, yCoordMod[yCoordMod.GetLwb()]);
        fitFunc->SetParameter(1, (- yCoordMod[yCoordMod.GetLwb()] + yCoordMod[yCoordMod.GetUpb()]) / (zCoordMod[yCoordMod.GetUpb()] - zCoordMod[yCoordMod.GetLwb()]));
        gr->Fit(fitFunc,"QREM");
        
        yErrors.push_back(fitFunc->Eval((*zCoord)(i)) - (*yCoord)(i));
        
    }
}

bool notSingle(std::size_t n) {
    return (n != 1);
}

bool notTwo(std::size_t n) {
    return (n != 2);
}

bool notThree(std::size_t n) {
    return (n != 3);
}

bool notFour(std::size_t n) {
    return (n != 4);
}

bool fivePlus(std::size_t n) {
    return (n < 5);
}

double MCSSigma(std::size_t i) {
    double theta0 = 4.11e-5; // calculated for 150 um silicon and 10 GeV/c pions
    double sigma = 0.0;
    for (std::size_t j{0}; j < i; ++j) sigma += (detectorPos.at(i) - detectorPos.at(j)) * (detectorPos.at(i) - detectorPos.at(j));
    return sqrt(sigma * theta0 * theta0);
}

void TracksFitting(std::size_t nIter = 10, string inputFile = "synced_clusters.root", string outputFile = "tracking_histos.root", double epsilon = 1.) {
    
    gStyle->SetOptFit(1);
    
    std::unordered_map<int, vdouble *> vintegrals, vcogs, vsoverns;
    std::unordered_map<int, vint *> vlengths;
    
    TGraphErrors *gr;
    
    TFile *inFile = TFile::Open(inputFile.c_str(), "read");
    TTree *clustersTree = (TTree *) inFile->Get("sync_clusters_tree");
    
    std::vector<std::array<TBranch *, NBRANCH>> branches;
    std::array<TBranch *, 4> temp_arr;
    branches.assign(NLAD * NBOARD, temp_arr);
    
    const auto &nEntries = clustersTree->GetEntries();
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        vintegrals[i] = new vdouble;
        vcogs[i] = new vdouble;
        vsoverns[i] = new vdouble;
        vlengths[i] = new vint;
        clustersTree->SetBranchAddress(Form("integral%lu", i), &vintegrals.at(i), &((branches.at(i)).at(0)));
        clustersTree->SetBranchAddress(Form("cog%lu", i), &vcogs.at(i), &((branches.at(i)).at(2)));
        clustersTree->SetBranchAddress(Form("sovern%lu", i), &vsoverns.at(i), &((branches.at(i)).at(3)));
        clustersTree->SetBranchAddress(Form("length%lu", i), &vlengths.at(i), &((branches.at(i)).at(1)));
    }
    
    // branch check
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        for (uint br{0}; br < branches.at(i).size(); br++)
            cout << "ladder " << i << " branch " << br << " has name " << branches.at(i).at(br)->GetName() << endl;
    }
    //*
    std::array<std::vector<double>, NLAD * NBOARD> arrHistos;
    TFile *outFile = TFile::Open(outputFile.c_str(), "recreate");
    TTree *tracksTree = new TTree("tracks_tree", "Fitted tracks");
    //*/
    float chi2, pvalue, rmse_x, rmse_y, rmse_z, xdir, ydir, zdir;
    float xInitDir, yInitDir, zInitDir, momInit, p0, p1;
    int hnmeas, hPDG, hConvTracks;
    UInt_t conversionDepth;
    std::vector<float> xErrors, yErrors, zErrors;
    TMatrixDSym covMatrix, correlMatrix;
    //*
    auto chi2br = tracksTree->Branch("chi2br", &chi2, "chi2/F");
    auto pvaluebr = tracksTree->Branch("pvaluebr", &pvalue, "pvalue/F");
    auto p0br = tracksTree->Branch("p0br", &p0, "p0/F");
    auto p1br = tracksTree->Branch("p1br", &p1, "p1/F");
    auto covMatrixbr = tracksTree->Branch("covMatrixbr", &covMatrix);
    auto corelMatrixbr = tracksTree->Branch("correlMatrixbr", &correlMatrix);
    auto rmse_ybr = tracksTree->Branch("rmse_ybr", &rmse_y, "rmse_y/F");
    //auto xdirbr = tracksTree->Branch("xdirbr", &xdir, "xdir/F");
    //auto ydirbr = tracksTree->Branch("ydirbr", &ydir, "ydir/F");
    //auto zdirbr = tracksTree->Branch("zdirbr", &zdir, "zdir/F");
    auto hnmeasbr = tracksTree->Branch("hnmeasbr", &hnmeas, "hnmeas/I");
    auto yerrbr = tracksTree->Branch("yerrbr", &yErrors);
    // auto hConvTracksbr = tracksTree->Branch("hConvTracksbr", &hConvTracks, "hConvTracks/I");
    //*/
    TCanvas *c = nullptr;
    std::size_t subCanvasIt{1};
    TF1 *fitFunc = new TF1("fitFunc","pol1(0)", detectorPos.front(), detectorPos.back());
    TVectorD *yCoord, *zCoord, *yErr, *zErr;
    zCoord = new TVectorD(NLAD * NBOARD, &detectorPos[0]);
    zErr = new TVectorD(NLAD * NBOARD, &detectorPosErr[0]);
    TVectorD chi2array;
    double chi2Sum{0};
    
    unsigned int N1event{0}, N2events{0}, N3events{0}, N4events{0}, N5plusevents{0};
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        
        clustersTree->GetEntry(event);
        std::array<std::size_t, NBOARD * NLAD> len;
        std::array<std::size_t, NBOARD * NLAD>::iterator it;
        
        for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
        it = std::find_if(len.begin(), len.end(), notTwo);
        if (it == len.end()) N2events++;
        it = std::find_if(len.begin(), len.end(), notThree);
        if (it == len.end()) N3events++;
        it = std::find_if(len.begin(), len.end(), notFour);
        if (it == len.end()) N4events++;
        it = std::find_if(len.begin(), len.end(), fivePlus);
        if (it == len.end()) N5plusevents++;
        
        it = std::find_if(len.begin(), len.end(), notSingle);
        
        if (it == len.end()) {
            
            N1event++;
            
            // std::cout << "event: " << event << std::endl;
            
            fitFunc->SetParameter(0, vcogs[0]->at(0));
            fitFunc->SetParameter(1, (- vcogs[0]->at(0) + vcogs[NBOARD * NLAD - 1]->at(0)) / (detectorPos.back() - detectorPos.front()));
            fitFunc->SetParLimits(0, 0.0, 2048.0);
            fitFunc->SetParLimits(1, -2048.0/(detectorPos.back() - detectorPos.front()), 2048.0/(detectorPos.back() - detectorPos.front()));
            
            
            yCoord = new TVectorD(NLAD * NBOARD, &transformVectorsDouble(vcogs, vsoverns)[0]);
            yErr = new TVectorD(NLAD * NBOARD, &transformVectorsInt(vlengths, vsoverns, true)[0]);
            
            gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
            TFitResultPtr r = gr->Fit(fitFunc, "QREMS");
            chi2 = r->Chi2() / r->Ndf();
            chi2Sum += chi2;
            pvalue = r->Prob();
            covMatrix.ResizeTo(r->GetCovarianceMatrix());
            correlMatrix.ResizeTo(r->GetCorrelationMatrix());
            covMatrix = r->GetCovarianceMatrix();
            correlMatrix = r->GetCorrelationMatrix();
            p0 = r->Parameter(0);
            p1 = r->Parameter(1);
            // r->Print("V");
            if (event % 100 == 0) {
                if (!c) {
                    c = new TCanvas("c", "A fitted track", 1280, 800);
                    c->Divide(5,2);
                }
                if (subCanvasIt < 11) {
                    c->cd(subCanvasIt);
                    gr->SetTitle(Form("Event %lu", event));
                    gr->GetXaxis()->SetTitle("Layer position Z, cm");
                    gr->GetYaxis()->SetTitle("Channel number");
                    gr->Draw("AP");
                    fitFunc->Draw("same");
                    c->Update();
                    subCanvasIt++;
                }
            }
            
            hnmeas = yCoord->GetNrows();
            evaluateUnbiasedResiduals(yErrors, yCoord, yErr, zCoord, zErr, fitFunc);
            
            for (std::size_t i{0}; i < NLAD * NBOARD; ++i) if (pvalue > 0.01) arrHistos[i].push_back(yErrors.at(i));
            
            rmse_y = TMath::RMS(yErrors.begin(), yErrors.end());
            
            tracksTree->Fill();
        }
    }
    
    chi2array.ResizeTo(1);
    chi2array[0] = chi2Sum / N1event;
    chi2Sum = 0.;
    
    outFile->cd();
    tracksTree->Write();
    outFile->Close();
    
    std::cout << "Total number of events: " << nEntries << std::endl;
    std::cout << "Number of events with 1 cluster in every plane: " << N1event << std::endl;
    std::cout << "Number of events with 2 clusters in every plane: " << N2events << std::endl;
    std::cout << "Number of events with 3 clusters in every plane: " << N3events << std::endl;
    std::cout << "Number of events with 4 clusters in every plane: " << N4events << std::endl;
    std::cout << "Number of events with 5+ clusters in every plane: " << N5plusevents << std::endl;
    
    // Alignment block
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        deltas[i].push_back(std::accumulate(arrHistos[i].begin(), arrHistos[i].end(), 0.0) / arrHistos[i].size());
        std::cout << "Correction parameters for layer: " << i << " deltaY: " << deltas[i].back() << std::endl;
    }
    //*
    string alignedFileName(outputFile.begin(), outputFile.end() - 5);
    alignedFileName += "_aligned.root";
    TFile *alignedFile = TFile::Open(alignedFileName.c_str(), "recreate");
    TTree *alignedTracksTree = new TTree("aligned_Tracks_tree", "Fitted aligned tracks");
    
    float chi2_aligned, pvalue_aligned, rmse_y_aligned;
    float p0_aligned, p1_aligned;
    int hnmeas_aligned;
    std::vector<float> yErrors_aligned;
    TMatrixDSym covMatrix_aligned, correlMatrix_aligned;
    //*
    auto chi2br_aligned = alignedTracksTree->Branch("chi2br", &chi2_aligned, "chi2_aligned/F");
    auto pvaluebr_aligned = alignedTracksTree->Branch("pvaluebr", &pvalue_aligned, "pvalue_aligned/F");
    auto p0br_aligned = alignedTracksTree->Branch("p0br", &p0_aligned, "p0_aligned/F");
    auto p1br_aligned = alignedTracksTree->Branch("p1br", &p1_aligned, "p1_aligned/F");
    auto covMatrixbr_aligned = alignedTracksTree->Branch("covMatrixbr", &covMatrix_aligned);
    auto corelMatrixbr_aligned = alignedTracksTree->Branch("correlMatrixbr", &correlMatrix_aligned);
    auto rmse_ybr_aligned = alignedTracksTree->Branch("rmse_ybr", &rmse_y_aligned, "rmse_y_aligned/F");
    auto hnmeasbr_aligned = alignedTracksTree->Branch("hnmeasbr", &hnmeas_aligned, "hnmeas_aligned/I");
    auto yerrbr_aligned = alignedTracksTree->Branch("yerrbr", &yErrors_aligned);
    
    TCanvas *ac = nullptr;
    subCanvasIt = 1;
    N1event = 0;
    std::size_t nAlignmentIterations = nIter;
    chi2array.ResizeTo(nAlignmentIterations + 1);
    
    for (std::size_t iterAlign{0}; iterAlign < nAlignmentIterations; ++iterAlign) {
    
        for (std::size_t event{0}; event < nEntries; ++event) {
            
            clustersTree->GetEntry(event);
            std::array<std::size_t, NBOARD * NLAD> len;
            std::array<std::size_t, NBOARD * NLAD>::iterator it;
            
            for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
            
            it = std::find_if(len.begin(), len.end(), notSingle);
            
            if (it == len.end()) {
                
                N1event++;
                
                // std::cout << "event: " << event << std::endl;
                
                fitFunc->SetParameter(0, vcogs[0]->at(0) + std::accumulate(deltas[0].begin(), deltas[0].end(), 0.0));
                fitFunc->SetParameter(1, (- vcogs[0]->at(0) - std::accumulate(deltas[0].begin(), deltas[0].end(), 0.0) + vcogs[NBOARD * NLAD - 1]->at(0) + std::accumulate(deltas[NBOARD * NLAD - 1].begin(), deltas[NBOARD * NLAD - 1].end(), 0.0)) / (detectorPos.back() - detectorPos.front()));
                fitFunc->SetParLimits(0, 0.0, 2048.0);
                fitFunc->SetParLimits(1, -2048.0/(detectorPos.back() - detectorPos.front()), 2048.0/(detectorPos.back() - detectorPos.front()));
                
                
                yCoord = new TVectorD(NLAD * NBOARD, &transformVectorsDouble(vcogs, vsoverns, false, true)[0]);
                yErr = new TVectorD(NLAD * NBOARD, &transformVectorsInt(vlengths, vsoverns, true, true)[0]);
                
                gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
                TFitResultPtr r = gr->Fit(fitFunc, "QREMS");
                chi2_aligned = r->Chi2() / r->Ndf();
                chi2Sum += chi2_aligned;
                pvalue_aligned = r->Prob();
                covMatrix_aligned.ResizeTo(r->GetCovarianceMatrix());
                correlMatrix_aligned.ResizeTo(r->GetCorrelationMatrix());
                covMatrix_aligned = r->GetCovarianceMatrix();
                correlMatrix_aligned = r->GetCorrelationMatrix();
                p0_aligned = r->Parameter(0);
                p1_aligned = r->Parameter(1);
                // r->Print("V");
                if (iterAlign == nAlignmentIterations - 1 && event % 100 == 0) {
                    if (!ac) {
                        ac = new TCanvas("ac", "A fitted aligned track", 1280, 800);
                        ac->Divide(5,2);
                    }
                    if (subCanvasIt < 11) {
                        ac->cd(subCanvasIt);
                        gr->SetTitle(Form("Event %lu", event));
                        gr->GetXaxis()->SetTitle("Layer position Z, cm");
                        gr->GetYaxis()->SetTitle("Channel number");
                        gr->Draw("AP");
                        fitFunc->Draw("same");
                        ac->Update();
                        subCanvasIt++;
                    }
                }
                
                hnmeas_aligned = yCoord->GetNrows();
                evaluateUnbiasedResiduals(yErrors_aligned, yCoord, yErr, zCoord, zErr, fitFunc);
                
                for (std::size_t i{0}; i < NLAD * NBOARD; ++i) if (pvalue_aligned > 0.01) arrHistos[i].push_back(yErrors_aligned.at(i));
                
                rmse_y_aligned = TMath::RMS(yErrors_aligned.begin(), yErrors_aligned.end());
                
                if (iterAlign == nAlignmentIterations - 1) alignedTracksTree->Fill();
            }
        }
        
        chi2array[iterAlign + 1] = chi2Sum / N1event;
        chi2Sum = 0.;
        N1event = 0;
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            deltas[i].push_back(std::accumulate(arrHistos[i].begin(), arrHistos[i].end(), 0.0) / arrHistos[i].size());
            std::cout << "Correction parameters for layer: " << i << " deltaY: " << deltas[i].back() << std::endl;
            arrHistos[i].clear();
        }
    }
    
    alignedFile->cd();
    alignedTracksTree->Write();
    alignedFile->Close();
    
    TVectorD iterArray(nAlignmentIterations + 1);
    
    for (std::size_t i{0}; i < iterArray.GetNrows(); ++i) iterArray[i] = i;
    
    TCanvas *ic = new TCanvas("ic", "Number of alignment iterations", 1280, 800);
    TGraph *grAlign = new TGraph(iterArray, chi2array);
    grAlign->SetTitle("Alignment convergence");
    grAlign->GetXaxis()->SetTitle("N iterations");
    grAlign->GetYaxis()->SetTitle("Global #chi^{2}/Ntracks");
    
    grAlign->Draw("ALP");
    ic->Update();
    
    //*/
    inFile->Close();
    
    std::cout << "MCS contribution per layer for 10 GeV/c pions" << std::endl;
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) std::cout << "Layer " << i << " sigmaMCS: " << MCSSigma(i) * 10000.0 << " um" << std::endl;
    
}
