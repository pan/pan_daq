//
//  main.cpp
//  Fit_Residuals
//
//  Created by Daniil Sukhonos on 18.05.23.
//

#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <unordered_map>
#include <algorithm>
#include <numeric>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TMath.h"
#include "TApplication.h"

int main(int argc, char * argv[]) {
    // argv[1] is a ROOT file name
    // argv[2] is the number of detectors
    
    TApplication app("app", &argc, argv);
    TString path("/Users/dasukhon/Documents/Documents_newMac/pan_daq/fitted_rootfiles/");
    
    TString inputFileName(argv[1]);
    inputFileName = inputFileName(0, inputFileName.Length() - 5);
    inputFileName = inputFileName(23, inputFileName.Length());
    
    TFile *inFile = TFile::Open(path + argv[1], "read");
    TTree *tracksTree = (TTree *) inFile->Get("tracks_tree");
    
    std::size_t nDetectors{std::stoul(argv[2])};
    
    gStyle->SetOptFit(1);
    TCanvas** c_arr = new TCanvas*[nDetectors];
    TH1D **hy_arr = new TH1D*[nDetectors];
    TF1* f1 = new TF1("f1", "([0] / ([2] * sqrt(2 * TMath::Pi()))) *  exp(-0.5 * ((x - [1]) / [2])**2) + ([3] / ([4] * sqrt(2 * TMath::Pi()))) * exp(-0.5 * ((x - [1]) / [4])**2)", -1.e-2, 1.e-2);
    f1->SetParLimits(0, 0., 1.);
    f1->SetParLimits(1, -0.002, 0.002);
    f1->SetParLimits(2, 0.0003, 0.0015);
    f1->SetParLimits(3, 0., 1.);
    f1->SetParLimits(4, 0.001, 0.01);
    
    double N1{0.}, N2{0.}, sigma1{0.}, sigma2{0.}, N1_err{0.}, N2_err{0.}, sigma1_err{0.}, sigma2_err{0.};
    double N1_der{0}, N2_der{0}, sigma1_der{0}, sigma2_der{0};
    double* sigma = new double[nDetectors];
    double* sigma_core = new double[nDetectors];
    double* sigma_err = new double[nDetectors];
    double* sigma_core_err = new double[nDetectors];
    double* std_dev = new double[nDetectors];
    double* std_dev_err = new double[nDetectors];
    unsigned int nEntries{0};
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        c_arr[i] = new TCanvas(Form("c%lu", i), Form("Residuals, Layer %lu", i), 0, 0, 1280, 800);
        tracksTree->Draw(Form("yerrbr[%lu]>>hy(500,-1.e-2,1.e-2)", i));
        hy_arr[i] = (TH1D*)gDirectory->Get("hy");
        nEntries = hy_arr[i]->GetEntries();
        gPad->SetLogy();
        hy_arr[i]->GetXaxis()->SetTitle("cm");
        hy_arr[i]->SetTitle(Form("Unbiased Residuals, Layer %lu", i));
        hy_arr[i]->Scale(1./nEntries);
        hy_arr[i]->Fit(f1, "REM");
        N1 = f1->GetParameter(0);
        N2 = f1->GetParameter(3);
        N1_err = f1->GetParError(0);
        N2_err = f1->GetParError(3);
        sigma1 = f1->GetParameter(2);
        sigma2 = f1->GetParameter(4);
        sigma1_err = f1->GetParError(2);
        sigma2_err = f1->GetParError(4);
        std_dev[i] = hy_arr[i]->GetStdDev();
        std_dev_err[i] = hy_arr[i]->GetStdDevError();
        sigma_core[i] = sigma1;
        sigma_core_err[i] = sigma1_err;
        sigma[i] = sqrt((N1 * sigma1 * sigma1 + N2 * sigma2 * sigma2) / (N1 + N2));
        sigma1_der = (N1 * sigma1) / (sigma[i] * (N1 +N2));
        sigma2_der = (N2 * sigma2) / (sigma[i] * (N1 +N2));
        N1_der = (2. * sigma1 * sigma1 * N1 + N2 * (sigma1 * sigma1 + sigma2 * sigma2)) / ((N1 + N2) * (N1 + N2) * 2. * sigma[i]);
        N2_der = (2. * sigma2 * sigma2 * N2 + N1 * (sigma1 * sigma1 + sigma2 * sigma2)) / ((N1 + N2) * (N1 + N2) * 2. * sigma[i]);
        sigma_err[i] = sqrt(N1_der * N1_der * N1_err * N1_err + N2_der * N2_der * N2_err * N2_err + sigma1_der * sigma1_der * sigma1_err * sigma1_err + sigma2_der * sigma2_der * sigma2_err * sigma2_err);
        f1->Draw("same");
        c_arr[i]->Modified();
        c_arr[i]->Update();
        c_arr[i]->SaveAs(path + "../plots/" + Form("resid_layer%lu", i) + inputFileName + ".pdf");
    }
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        std::cout << "Layer " << i << ", sigma = " << sigma[i] * 1.e+04 << " +- " << sigma_err[i] * 1.e+04 << " um" << std::endl;
    }
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        std::cout << "Layer " << i << ", core sigma = " << sigma_core[i] * 1.e+04 << " +- " << sigma_core_err[i] * 1.e+04 << " um" << std::endl;
    }
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        std::cout << "Layer " << i << ", std dev = " << std_dev[i] * 1.e+04 << " +- " << std_dev_err[i] * 1.e+04 << " um" << std::endl;
    }
    
    app.Run();
    
    delete tracksTree;
    inFile->Close();
    
    delete f1;
    delete [] c_arr;
    delete [] sigma;
    delete [] sigma_err;
    delete [] sigma_core;
    delete [] sigma_core_err;
    delete [] std_dev;
    delete [] std_dev_err;

    return 0;
}
