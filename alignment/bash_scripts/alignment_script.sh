#!/bin/bash

workdirPath=/home/users/s/sukhonod/scratch/alignment/

nJobs=$2
fileCounter=0
nIterations=$3
fileId=`echo "$1" | cut -d'.' -f 1 | cut -c16-`
iteration=0
chi2=0

if [ ! -d "$workdirPath"/local_pars"$fileId" ]
then
	mkdir -p "$workdirPath"/local_pars"$fileId"
	mkdir -p "$workdirPath"/err/"$fileId"
	mkdir -p "$workdirPath"/out/"$fileId"
fi

sbatch --error="$workdirPath"/err/build_plots/build_plots_%j.err --output="$workdirPath"/out/build_plots/build_plots_%j.out "$HOME"/alignment_scripts/buildPlots.sh $workdirPath $1 false 

if [ -f "$workdirPath"/global_pars/parameters"$fileId".txt ]
then
	oldIteration=`tail -n 3 "$workdirPath"/global_pars/parameters"$fileId".txt | head -n 1`
else
	oldIteration=0
fi
lastJob=$((nJobs-1))

for ((i = $oldIteration; i < $nIterations; i++))
do
	sbatch --array=0-"$lastJob"%100 --output="$workdirPath"/out/"$fileId"/local_pars_%A_%a.out --error="$workdirPath"/err/"$fileId"/local_pars_%A_%a.err "$HOME"/alignment_scripts/oneStepAlignment.sh $workdirPath $1 $nJobs
        while [ $fileCounter -lt $nJobs ]
        do
                if [ -f "$workdirPath"/local_pars"$fileId"/parameters_"$fileCounter".txt ]
                then
                        fileCounter=$((fileCounter+1))
                fi
        done
        fileCounter=0
        sleep 5
	sbatch --error="$workdirPath"/err/global_pars/global_pars_%j.err --output="$workdirPath"/out/global_pars/global_pars_%j.out "$HOME"/alignment_scripts/makeAlignmentIteration.sh $workdirPath $1 $nJobs
        while [ $fileCounter -eq 0 ]
        do
                if [ -f "$workdirPath"/indicators/parameters_indicator"$fileId".txt ]
                then
                        fileCounter=$((fileCounter+1))
                fi
        done
        fileCounter=0
        sleep 5
	rm "$workdirPath"/local_pars"$fileId"/*
	rm "$workdirPath"/out/"$fileId"/*
	rm "$workdirPath"/err/"$fileId"/*
	rm "$workdirPath"/indicators/parameters_indicator"$fileId".txt
	iteration=`tail -n 3 "$workdirPath"/global_pars/parameters"$fileId".txt | head -n 1`
        chi2=`tail -n 1 "$workdirPath"/global_pars/parameters"$fileId".txt`
	if [ $((iteration-1)) -eq $i ]
	then 
		echo $iteration $chi2 >> "$workdirPath"/convergeance/convergeance_data"$fileId".txt
	else
		i=$((i-1))
	fi
done

sbatch --error="$workdirPath"/err/build_plots/build_plots_%j.err --output="$workdirPath"/out/build_plots/build_plots_%j.out "$HOME"/alignment_scripts/buildPlots.sh $workdirPath $1 true
