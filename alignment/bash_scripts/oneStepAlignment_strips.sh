#!/bin/bash

#SBATCH --partition=shared-cpu
#SBATCH --time=00:10:00
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=3000 # in MB

# change to shared-cpu, if the task runs < 12 hours

job_id=${SLURM_ARRAY_TASK_ID}
fileIter=$4

source ~/.bashrc

module load OpenSSL/1.1

echo $PATH
echo $LD_LIBRARY_PATH

root -q -b "$9"/alignment/number_of_evts.C'("'$1'/synced_rootfiles/'$2'", "sync_clusters_tree")'
NJOBS=$3
NTOTAL=$(cat number.txt)
FIRST=$(((NTOTAL/NJOBS)*(job_id + fileIter)))
LAST=$(( (NTOTAL/NJOBS)*(job_id + fileIter + 1) + ( job_id == NJOBS-1 ? NTOTAL % NJOBS : 0 ) ))

srun "$9"/alignment/bin/oneStepDetectorAlignment"$8" $2 $FIRST $LAST $((job_id + fileIter)) $1 $5 $6 $7

echo "Job " $job_id " finished."

