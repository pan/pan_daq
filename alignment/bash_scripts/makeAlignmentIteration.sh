#!/bin/bash

#SBATCH --partition=shared-cpu
#SBATCH --time=00:10:00
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=3000 # in MB

# change to shared-cpu, if the task runs < 12 hours

job_id=${SLURM_JOB_ID}

source ~/.bashrc

module load OpenSSL/1.1

echo $PATH
echo $LD_LIBRARY_PATH

srun "$5"/alignment/bin/performAlignmentIteration $3 $1 $2 $4

echo "Job " $job_id " finished."

