#!/bin/bash

rm ~/scratch/alignment/global_pars/*
rm -rf ~/scratch/alignment/local_pars_*
rm ~/scratch/alignment/fitted_rootfiles/*
rm ~/scratch/alignment/convergeance/*
rm ~/scratch/alignment/err/*/*
rm ~/scratch/alignment/out/*/*

