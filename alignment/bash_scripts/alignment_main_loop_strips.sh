#!/bin/bash

# set -x # print every line

### CHANGE BELOW ###
workdirPath=/home/users/s/sukhonod/scratch/alignment 						# <--- set the path, where all files will be stored
panDaqPath=/home/users/s/sukhonod/pan_daq 									# <--- set the path to the downloaded copy of the pan_daq repository
### CHANGE ABOVE ###

nJobs=$2
fileCounter=0
nIterations=$3
fileId=`echo "$1" | cut -d'.' -f 1 | cut -c16-`
iteration=0
chi2=0
### CHANGE BELOW ###
nDetectors=9 																# <--- set the number of detectors used to record data (usually 6, if only StripXs were used)
noStripXChannelShift="true"													# <--- if the channel shift in StripX needs to be corrected (old data): "false"
stripYDataEn="true"															# <--- if there is only stripXs (6 detectors): "false"
beamTestData="Aug"															# <--- change if you use:
																			# June and August 2022 data: "June"
																			# September 2022 data: "Sep"
																			# November 2022 data: "Nov" 
																			# April 2023 data: "Aprl"
																			# August 2023 data: "Aug"
### CHANGE ABOVE ###
errorCut=0.5
declare -a jIDs

### NOTHING TO CHANGE BELOW !!! #############################################

if [ ! -d "$workdirPath"/global_pars ]
then
	mkdir -p "$workdirPath"/global_pars
	mkdir -p "$workdirPath"/out/global_pars
	mkdir -p "$workdirPath"/err/global_pars
fi

if [ ! -d "$workdirPath"/fitted_rootfiles ]
then
        mkdir -p "$workdirPath"/fitted_rootfiles
		mkdir -p "$workdirPath"/convergeance
		mkdir -p "$workdirPath"/indicators
        mkdir -p "$workdirPath"/out/build_plots
        mkdir -p "$workdirPath"/err/build_plots
fi

sbatch --error="$workdirPath"/err/build_plots/build_plots_%j.err \
	--output="$workdirPath"/out/build_plots/build_plots_%j.out \
	"$panDaqPath"/alignment/bash_scripts/buildPlots_strips.sh \
	$workdirPath $1 "false" $noStripXChannelShift $stripYDataEn $beamTestData $panDaqPath

if [ -f "$workdirPath"/global_pars/parameters"$fileId".txt ]
then
	oldIteration=`tail -n 3 "$workdirPath"/global_pars/parameters"$fileId".txt | head -n 1`
else
	oldIteration=0
fi

if [ $nJobs -gt 500 ]
then
	lastJob=499
else
	lastJob=$((nJobs-1))
fi

for ((i = 0; i < $nJobs; i = i + 500))
do
	if [ ! -d "$workdirPath"/local_pars"$fileId"_"$i"_"$((i + 500))" ]
	then
        	mkdir -p "$workdirPath"/local_pars"$fileId"_"$i"_"$((i + 500))"
        	mkdir -p "$workdirPath"/err/"$fileId"_"$i"_"$((i + 500))"
        	mkdir -p "$workdirPath"/out/"$fileId"_"$i"_"$((i + 500))"
	fi
done
for ((j = 0; j < $nJobs; j = j + 500))
do
        rm "$workdirPath"/local_pars"$fileId"_"$j"_"$((j + 500))"/*
        rm "$workdirPath"/out/"$fileId"_"$j"_"$((j + 500))"/*
        rm "$workdirPath"/err/"$fileId"_"$j"_"$((j + 500))"/*
done

for ((i = $oldIteration; i < $nIterations; ++i))
do
	if [ $i -gt 50 ]; then errorCut=0.1; fi
        if [ $i -gt 100 ]; then errorCut=0.05; fi
        if [ $i -gt 150 ]; then errorCut=0.01; fi
	for ((j = 0; j < $nJobs; j = j + 500))
	do
		rm "$workdirPath"/local_pars"$fileId"_"$j"_"$((j + 500))"/*
                rm "$workdirPath"/out/"$fileId"_"$j"_"$((j + 500))"/*
                rm "$workdirPath"/err/"$fileId"_"$j"_"$((j + 500))"/*
		if [ $((j + 500)) -le $nJobs ]
		then
			lastJob=499
		else
			lastJob=$((nJobs - j - 1))
		fi

		jIDs[$((j/500))]=$(sbatch --array=0-"$lastJob"%100 \
			--output="$workdirPath"/out/"$fileId"_"$j"_"$((j + 500))"/local_pars_%A_%a.out \
			--error="$workdirPath"/err/"$fileId"_"$j"_"$((j + 500))"/local_pars_%A_%a.err \
			"$panDaqPath"/alignment/bash_scripts/oneStepAlignment_strips.sh \
			$workdirPath $1 $nJobs $j $noStripXChannelShift $errorCut $stripYDataEn \
			$beamTestData $panDaqPath \
			| awk '{print $NF}')	
        done
	
        fileCounter=0
        sleep 5
		rm "$workdirPath"/err/global_pars/global_pars"$fileId"*
		rm "$workdirPath"/out/global_pars/global_pars"$fileId"*
        dependencies=$(IFS=: ; echo "${jIDs[*]}")
		echo "$dependencies"

		sbatch --error="$workdirPath"/err/global_pars/global_pars"$fileId"_%j.err \
			--output="$workdirPath"/out/global_pars/global_pars"$fileId"_%j.out \
			--dependency=afterany:"$dependencies" \
			"$panDaqPath"/alignment/bash_scripts/makeAlignmentIteration.sh \
			$workdirPath $1 $nJobs $nDetectors $panDaqPath
        while [ $fileCounter -eq 0 ]
        do
                if [ -f "$workdirPath"/indicators/parameters_indicator"$fileId".txt ]
                then
                        fileCounter=$((fileCounter+1))
                fi
		sleep 10
        done
        fileCounter=0
        sleep 5

		rm "$workdirPath"/indicators/parameters_indicator"$fileId".txt
		iteration=`tail -n 3 "$workdirPath"/global_pars/parameters"$fileId".txt | head -n 1`
        chi2=`tail -n 1 "$workdirPath"/global_pars/parameters"$fileId".txt`
		gamma=`head -n 2 "$workdirPath"/global_pars/parameters"$fileId".txt | tail -n 1`

		if [ $((iteration-1)) -eq $i ]
		then 
			echo $iteration $chi2 $gamma >> "$workdirPath"/convergeance/convergeance_data"$fileId".txt
		else
			i=$((i-1))
		fi
done

sbatch --error="$workdirPath"/err/build_plots/build_plots_%j.err \
	--output="$workdirPath"/out/build_plots/build_plots_%j.out \
	"$panDaqPath"/alignment/bash_scripts/buildPlots_strips.sh \
	$workdirPath $1 "true" $noStripXChannelShift $stripYDataEn $beamTestData $panDaqPath
