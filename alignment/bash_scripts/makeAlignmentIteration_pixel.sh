#!/bin/bash

#SBATCH --partition=shared-cpu
#SBATCH --time=00:20:00
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=3000 # in MB
#SBATCH --exclude=cpu285

# change to shared-cpu, if the task runs < 12 hours

job_id=${SLURM_JOB_ID}

herdVer="0.1.2"

source ~/.bashrc

source /home/users/s/sukhonod/HerdSoftware-install//setenvHERD_"$herdVer".sh

echo $PATH
echo $LD_LIBRARY_PATH

srun performAlignmentIteration $3 $1 $2 $4

echo "Job " $job_id " finished."

