#!/bin/bash

#SBATCH --partition=shared-cpu
#SBATCH --time=00:10:00
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=3000 # in MB

# change to shared-cpu, if the task runs < 12 hours

job_id=${SLURM_JOB_ID}

herdVer="0.1.2"

source ~/.bashrc

source /home/users/s/sukhonod/HerdSoftware-install//setenvHERD_"$herdVer".sh

echo $PATH
echo $LD_LIBRARY_PATH

srun produceAlignmentPlots $2 $3 $1 $4

echo "Job " $job_id " finished."
