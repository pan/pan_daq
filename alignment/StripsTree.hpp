#ifndef StripsTree_hpp
#define StripsTree_hpp

#include <iostream>
#include <vector>
#include <unordered_map>

#include "TTree.h"
#include "TString.h"
#include "TFile.h"
#include "TBranch.h"

class StripsTree {
    std::unordered_map<int, std::vector<double> *> vintegrals, vcogs, vsoverns;
    std::unordered_map<int, std::vector<std::vector<double>> *> vsovernsPerChannel;
    std::unordered_map<int, std::vector<std::size_t> *> vlengths;
    std::unordered_map<int, UInt_t> eventNumberMap;
    std::unordered_map<int, ULong64_t> frameCounterMap;
    
    std::vector<std::array<TBranch *, 7>> branches;
    
    std::size_t nDetectors;
    
    TFile* _inFile{nullptr};
    TTree* _tree{nullptr};
    
protected:
    void initMembers();
    TTree* loadTree(TString filename);
    
public:
    StripsTree();
    StripsTree(TString filename);
    virtual ~StripsTree();
    
    StripsTree(const StripsTree& tree) = delete;
    StripsTree& operator=(const StripsTree& tree) = delete;
    
    void assignBranches();
    void createBranches();
    
    TTree* getTree() const {return _tree;}
    std::vector<double> getPos(std::size_t detector) const {return *(vcogs.at(detector));}
    double getPosLocal(std::size_t detector, std::size_t hit) const {return vcogs.at(detector)->at(hit);}
    std::vector<double> getIntegral(std::size_t detector) const {return *(vintegrals.at(detector));}
    std::vector<double> getSovern(std::size_t detector) const {return *(vsoverns.at(detector));}
    std::vector<std::vector<double>> getSovernsPerChannel(std::size_t detector) const {return *(vsovernsPerChannel.at(detector));}
    std::vector<std::size_t> getLength(std::size_t detector) const {return *(vlengths.at(detector));}
    std::size_t getEventNumber(std::size_t detector) const {return eventNumberMap.at(detector);}
    ULong64_t getFrameCounter(std::size_t detector) const {return frameCounterMap.at(detector);}
    
    void setTree(TTree* tree) {_tree = tree;}
    void setPos(std::size_t detector, std::vector<double> cogs) {*(vcogs.at(detector)) = cogs; (vcogs.at(detector))->shrink_to_fit();}
    void setPosLocal(std::size_t detector, std::size_t hit, double cog);
    void setIntegral(std::size_t detector, std::vector<double> integrals) {*(vintegrals.at(detector)) = integrals; (vintegrals.at(detector))->shrink_to_fit();}
    void setSovern(std::size_t detector, std::vector<double> soverns) {*(vsoverns.at(detector)) = soverns; (vsoverns.at(detector))->shrink_to_fit();}
    void setSovernsPerChannel(std::size_t detector, std::vector<std::vector<double>> sovernsPerChannel) {*(vsovernsPerChannel.at(detector)) = sovernsPerChannel; (vsovernsPerChannel.at(detector))->shrink_to_fit();}
    void setLength(std::size_t detector, std::vector<std::size_t> lengths) {*(vlengths.at(detector)) = lengths; (vlengths.at(detector))->shrink_to_fit();}
    void setEventNumber(std::size_t detector, UInt_t number) {eventNumberMap.at(detector) = number;}
    void setFrameCounter(std::size_t detector, ULong64_t counterValue) {frameCounterMap.at(detector) = counterValue;}
};

#endif /* StripsTree_hpp */
