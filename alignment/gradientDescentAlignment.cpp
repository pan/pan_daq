//
//  gradientDescentAlignment.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 15.12.21.
//

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TVectorD.h"
#include "TFitResult.h"
#include "TGeoManager.h"
#include "TGeoPhysicalNode.h"
#include "TRandom.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <unordered_map>
#include <algorithm>
#include <numeric>

#include "ConstField.h"
#include "Exception.h"
#include "FieldManager.h"
#include "KalmanFitterRefTrack.h"
#include "KalmanFitter.h"
#include "AbsKalmanFitter.h"
#include "KalmanFitterInfo.h"
#include "FitStatus.h"
#include "MeasuredStateOnPlane.h"
#include "StateOnPlane.h"
#include "SharedPlanePtr.h"
#include "Track.h"
#include "TrackPoint.h"
#include "DetPlane.h"

#include "MaterialEffects.h"
#include "AbsTrackRep.h"
#include "RKTrackRep.h"
#include "TGeoMaterialInterface.h"

#include "EventDisplay.h"

#include "AbsMeasurement.h"
#include "PlanarMeasurement.h"
#include "MeasurementOnPlane.h"

typedef unsigned int uint;
typedef std::vector<double> vdouble;
typedef std::vector<int> vint;
typedef std::vector<string> vstring;

constexpr std::size_t NLAD{2};
constexpr std::size_t NBOARD{3};
constexpr std::size_t NBRANCH{5};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double detectorResolution{0.0025};
constexpr float stripLength(5.12);
uint sampleSize{2000};
constexpr bool nov2021BT{false};
constexpr bool june2022BT{true};
constexpr bool august2022BT{true};
constexpr bool PRenabled{false};

vdouble detectorPos;
vdouble detectorPosErr = {0.01, 0.01, 0.01, 0.01, 0.01, 0.01}; // Let's put higher machining accuracy: 0.02 cm
std::array<vdouble, NLAD * NBOARD> deltas;
std::size_t nTracks{1};
vdouble DeltaY_correct(6);
vdouble DeltaZ_correct(6);
vdouble ThetaX_correct(6);

double DeltaY_limit{0.1}, DeltaZ_limit{0.2}, ThetaX_limit{0.1};

vdouble transformVectorsDouble(std::unordered_map<int, vdouble *> &vec, std::unordered_map<int, vdouble *> &vsoverns, std::size_t j) {
    vdouble outVec;
    double alpha{0.0};
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
		outVec.push_back((vec[i]->at(j) - 1023.5) * conversionCoef);
    }
    
    return outVec;
}

vdouble transformVectorsInt(std::unordered_map<int, vint *> &vec, std::unordered_map<int, vdouble *> &vsoverns, std::size_t j, bool firstStep = false) {
    
    vdouble outVec;
    double alpha{0.0};
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
		alpha = sqrt(1.5 + vec[i]->at(j));
        if (vec[i]->at(j) == 1) outVec.push_back(conversionCoef / sqrt(12.0));
        else outVec.push_back(alpha * conversionCoef / vsoverns[i]->at(j)); // due to CoG calculation
        if (firstStep) outVec.back() = std::sqrt(outVec.back() * outVec.back() + 0.01 * 0.01); // artificially increase the errors by 100 um for the first few grad steps to increase statistics
	}
	
	return outVec;
}

void updateVcogs(std::unordered_map<int, vdouble *> &vec, std::unordered_map<int, vdouble *> &vecAligned, std::size_t j) {
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (i % 2 == 0) vecAligned[i]->push_back(2047. - vec[i]->at(j));
        else vecAligned[i]->push_back(vec[i]->at(j));
    }
}

void updateCoGs(std::unordered_map<int, vdouble *> &vec, std::unordered_map<int, vdouble *> &vecAligned, vdouble& deltaY, vdouble& thetaX, std::size_t j) {
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (june2022BT) vecAligned[i]->at(j) = (vecAligned[i]->at(j) + deltaY[i] / conversionCoef);
        else vecAligned[i]->push_back((vec[i]->at(j) + deltaY[i] / conversionCoef));
    }
}

void applyCorrections(std::unordered_map<int, vdouble *> &vecAligned, const vdouble &correctionsY, std::size_t j) {
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        vecAligned[i]->at(j) = (vecAligned[i]->at(j) + correctionsY.at(i) / conversionCoef);
    }
}

vdouble updateDetPos(std::unordered_map<int, vdouble *> &vec, vdouble& deltaZ, vdouble& thetaX, std::size_t j) {
    vdouble newDetPos(NLAD * NBOARD);
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        newDetPos[i] = detectorPos[i] + deltaZ[i] + 2. * (vec[i]->at(j) - 1023.5) * conversionCoef * thetaX[i] / stripLength;
    }
    return newDetPos;
}

TVectorD removeElement(std::size_t i, TVectorD * vecOrig) {
    TVectorD outVec, tempVecFirst(*vecOrig), tempVecSecond(*vecOrig);
    
    tempVecFirst.ResizeTo(vecOrig->GetLwb(), i - 1);
    tempVecSecond.ResizeTo(i + 1, vecOrig->GetUpb());
    outVec.ResizeTo(vecOrig->GetNrows() - 1);
    outVec.SetSub(0, tempVecFirst);
    outVec.SetSub(tempVecFirst.GetNrows(), tempVecSecond);
    
    return outVec;
}

void evaluateUnbiasedResiduals(std::vector<float> &yErrors, TVectorD * yCoord, TVectorD * yErr, TVectorD * zCoord, TVectorD * zErr, TF1 * fitFunc) {
    
    TGraphErrors *gr{nullptr};
    TVectorD yCoordMod, yErrMod, zCoordMod, zErrMod;
    yErrors.clear();
    yErrors.shrink_to_fit();
    
    yCoordMod.ResizeTo(NLAD * NBOARD - 1);
    yErrMod.ResizeTo(NLAD * NBOARD - 1);
    zCoordMod.ResizeTo(NLAD * NBOARD - 1);
    zErrMod.ResizeTo(NLAD * NBOARD - 1);
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        
        yCoordMod = removeElement(i, yCoord);
        yErrMod = removeElement(i, yErr);
        zCoordMod = removeElement(i, zCoord);
        zErrMod = removeElement(i, zErr);
        
        fitFunc->SetRange(zCoordMod[yCoordMod.GetLwb()] * 1.05, zCoordMod[yCoordMod.GetUpb()] * 1.05);
        
        gr = new TGraphErrors(zCoordMod, yCoordMod, zErrMod, yErrMod);
        fitFunc->SetParameter(0, (yCoordMod.Max() + yCoordMod.Min()) / 2.);
        fitFunc->SetParameter(1, (- yCoordMod[yCoordMod.GetLwb()] + yCoordMod[yCoordMod.GetUpb()]) / (zCoordMod[yCoordMod.GetUpb()] - zCoordMod[yCoordMod.GetLwb()]));
        gr->Fit(fitFunc,"QRNEMC");
        
        yErrors.push_back(fitFunc->Eval((*zCoord)(i)) - (*yCoord)(i));
        
        delete gr;
        gr = nullptr;
    }
    
    fitFunc->SetRange(detectorPos.front() * 1.05, detectorPos.back() * 1.05);
}

bool notSingle(std::size_t n) {
    return (n != 1);
}

bool notTwo(std::size_t n) {
    return (n != 2);
}

bool notThree(std::size_t n) {
    return (n != 3);
}

bool notFour(std::size_t n) {
    return (n != 4);
}

bool fivePlus(std::size_t n) {
    return (n < 5);
}

double MCSSigma(std::size_t i) {
    // double theta0 = 4.11e-5; // calculated for 150 um silicon and 10 GeV/c pions
    double p = 10000.; // MeV/c
    double mass = 139.57; // MeV for pi+-
    if (june2022BT) {
        p = 15000.; // Mev/c
        mass = 938.27; // MeV for proton
    }
    if (august2022BT) {
		p = 180000.; // Mev/c
        mass = 139.57; // MeV for pi+
	}
    double gammaFactor = sqrt(1 + (p / mass) * (p / mass));
    double velocity = p / (mass * gammaFactor);
    double xSi = 9.37; // cm for silicon
    double s = 0.0150; // cm thickness of each tracking plane
    double relThickness = s / xSi;
    double theta0 = (13.6 / (p * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
    double sigma = 0.0;
    for (std::size_t j{0}; j < i; ++j) sigma += (detectorPos.at(i) - detectorPos.at(j)) * (detectorPos.at(i) - detectorPos.at(j));
    return sqrt(sigma * theta0 * theta0);
}

double findDerivative(TF1* fitFunc, const int parIndex, TVectorD* yCoord, TVectorD* yErr, TVectorD* zCoord, TVectorD* zErr, const std::size_t i, const uint paramType, const vdouble &DeltaY) {
    double result{0.}, Delta_limit{0.};
    switch (paramType) {
        case 0:
            Delta_limit = DeltaY_limit;
            break;
        case 1:
            Delta_limit = DeltaZ_limit;
            break;
        case 2:
            Delta_limit = ThetaX_limit;
            break;
        default:
            break;
    }
    double step = 0.001 * Delta_limit * 2.;
    
    TGraphErrors* gr;
    TF1* fitFunc_dev = new TF1(*fitFunc);
    
    double diffLeft{0.}, diffRight{0.}, derivReg{0.}, derivHalf{0.};
    TVectorD yCoord_var(*yCoord);
    TVectorD zCoord_var(*zCoord);
    switch (paramType) {
        case 0:
            yCoord_var[i] = (*yCoord)[i] - step;
            break;
        case 1:
            zCoord_var[i] = (*zCoord)[i] - step;
            break;
        case 2:
            zCoord_var[i] = (*zCoord)[i] - 2. * ((*yCoord)[i] - DeltaY.at(i)) * step / stripLength;
            break;
        default:
            break;
    }
    
    gr = new TGraphErrors(zCoord_var, yCoord_var, *zErr, *yErr);
    gr->Fit(fitFunc_dev, "QRNEM");
    
    diffLeft = fitFunc_dev->GetParameter(parIndex);
    
    delete gr;
    
    switch (paramType) {
        case 0:
            yCoord_var[i] = (*yCoord)[i] + step;
            break;
        case 1:
            zCoord_var[i] = (*zCoord)[i] + step;
            break;
        case 2:
            zCoord_var[i] = (*zCoord)[i] + 2. * ((*yCoord)[i] - DeltaY.at(i)) * step / stripLength;
            break;
        default:
            break;
    }
    
    gr = new TGraphErrors(zCoord_var, yCoord_var, *zErr, *yErr);
    gr->Fit(fitFunc_dev, "QRNEM");
    
    diffRight = fitFunc_dev->GetParameter(parIndex);
    
    delete gr;
    
    derivReg = (diffRight - diffLeft) / (2. * step);
    
    switch (paramType) {
        case 0:
            yCoord_var[i] = (*yCoord)[i] - step / 2.;
            break;
        case 1:
            zCoord_var[i] = (*zCoord)[i] - step / 2.;
            break;
        case 2:
            zCoord_var[i] = (*zCoord)[i] - ((*yCoord)[i] - DeltaY.at(i)) * step / stripLength;
            break;
        default:
            break;
    }
    
    gr = new TGraphErrors(zCoord_var, yCoord_var, *zErr, *yErr);
    gr->Fit(fitFunc_dev, "QRNEM");
    
    diffLeft = fitFunc_dev->GetParameter(parIndex);
    
    delete gr;
    
    switch (paramType) {
        case 0:
            yCoord_var[i] = (*yCoord)[i] + step / 2.;
            break;
        case 1:
            zCoord_var[i] = (*zCoord)[i] + step / 2.;
            break;
        case 2:
            zCoord_var[i] = (*zCoord)[i] + ((*yCoord)[i] - DeltaY.at(i)) * step / stripLength;
            break;
        default:
            break;
    }
    
    gr = new TGraphErrors(zCoord_var, yCoord_var, *zErr, *yErr);
    gr->Fit(fitFunc_dev, "QRNEM");
    
    diffRight = fitFunc_dev->GetParameter(parIndex);
    
    delete gr;
    
    derivHalf = (diffRight - diffLeft) / step;
    
    result = (4. * derivHalf - derivReg) / 3.;
    
    delete fitFunc_dev;
    
    return result;
}

void calculateGrad(TF1* fitFunc, vdouble &gradDelta, TVectorD* yCoord, TVectorD* yErr, TVectorD* zCoord, TVectorD* zErr, const uint paramType, const vdouble &DeltaY) {
    
    vdouble derivativeFitFuncA(NLAD * NBOARD);
    vdouble derivativeFitFuncB(NLAD * NBOARD);
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        derivativeFitFuncA.at(i) = findDerivative(fitFunc, 1, yCoord, yErr, zCoord, zErr, i, paramType, DeltaY);
        derivativeFitFuncB.at(i) = findDerivative(fitFunc, 0, yCoord, yErr, zCoord, zErr, i, paramType, DeltaY);
    }
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        for (std::size_t j{0}; j < NLAD * NBOARD; ++j) {
            switch (paramType) {
                case 0:
                    if (j == i) gradDelta.at(i) += 2.0 * (fitFunc->Eval((*zCoord)[j]) - (*yCoord)[j]) * ((*zCoord)[j] * derivativeFitFuncA.at(j) + derivativeFitFuncB.at(j) - 1.0);
                    else gradDelta.at(i) += 2.0 * (fitFunc->Eval((*zCoord)[j]) - (*yCoord)[j]) * ((*zCoord)[j] * derivativeFitFuncA.at(j) + derivativeFitFuncB.at(j));
                    break;
                case 1:
                    if (j == i) gradDelta.at(i) += 2.0 * (fitFunc->Eval((*zCoord)[j]) - (*yCoord)[j]) * ((*zCoord)[j] * derivativeFitFuncA.at(j) + derivativeFitFuncB.at(j) + fitFunc->GetParameter(1));
                    else gradDelta.at(i) += 2.0 * (fitFunc->Eval((*zCoord)[j]) - (*yCoord)[j]) * ((*zCoord)[j] * derivativeFitFuncA.at(j) + derivativeFitFuncB.at(j));
                    break;
                case 2:
                    if (j == i) gradDelta.at(i) += 2.0 * (fitFunc->Eval((*zCoord)[j]) - (*yCoord)[j]) * ((*zCoord)[j] * derivativeFitFuncA.at(j) + derivativeFitFuncB.at(j) + 2. * fitFunc->GetParameter(1) * ((*yCoord)[j] - DeltaY.at(j)) / stripLength);
                    else gradDelta.at(i) += 2.0 * (fitFunc->Eval((*zCoord)[j]) - (*yCoord)[j]) * ((*zCoord)[j] * derivativeFitFuncA.at(j) + derivativeFitFuncB.at(j));
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    derivativeFitFuncA.clear();
    derivativeFitFuncA.shrink_to_fit();
    derivativeFitFuncB.clear();
    derivativeFitFuncB.shrink_to_fit();
}

double calculateGradDeltaY(TF1* fitFunc, double yCoord, double yErr, double zCoord, double deltaY, double deltaZ, double thetaX, double yErrUnbiased, double yCorrection) {
    double result{0.0};
    double zCorrect = zCoord;
    double yCorrect = yCoord;
    
    result = 2.0 * (fitFunc->Eval(zCorrect) - yCorrect - yCorrection) * (-1.0);
    // result = 2.0 * (yErrUnbiased - yCorrection) * (-1.0);
    
    return result;
}

double calculateGradDeltaZ(TF1* fitFunc, double yCoord, double yErr, double zCoord, double deltaY, double deltaZ, double thetaX, double yErrUnbiased, double yCorrection) {
    double result{0.0};
    double zCorrect = zCoord;
    double yCorrect = yCoord;
    
    result = 2.0 * (fitFunc->Eval(zCorrect) - yCorrect - yCorrection) * (fitFunc->GetParameter(1));
    // result = 2.0 * (yErrUnbiased - yCorrection) * (fitFunc->GetParameter(1));
    
    return result;
}

double calculateGradThetaX(TF1* fitFunc, double yCoord, double yErr, double zCoord, double deltaY, double deltaZ, double thetaX, double yErrUnbiased, double yCorrection) {
    double result{0.0};
    double zCorrect = zCoord;
    double yCorrect = yCoord;
    yCoord = yCorrect - deltaY;
    
    result = 2.0 * (fitFunc->Eval(zCorrect) - yCorrect - yCorrection) * (fitFunc->GetParameter(1) * yCoord); // error here
    // result = 2.0 * (yErrUnbiased - yCorrection) * (fitFunc->GetParameter(1) * yCoord);
    
    return result;
}

void makeGradStep(vdouble &deltaY, vdouble &deltaZ, vdouble &thetaX, vdouble &gradDeltaY, vdouble &gradDeltaZ, vdouble &gradThetaX, double gamma) {
    
    vdouble pars(NLAD * NBOARD * 3), newPars(NLAD * NBOARD * 3);
    
    for (std::size_t i{0}; i < NLAD * NBOARD * 3; ++i) {
        if (i >= NLAD * NBOARD && i < NLAD * NBOARD * 2) {
            pars.at(i) = deltaY.at(i - NLAD * NBOARD);
            newPars.at(i) = pars.at(i) - gamma * gradDeltaY.at(i - NLAD * NBOARD);
            deltaY.at(i - NLAD * NBOARD) = newPars.at(i);
            gradDeltaY.at(i - NLAD * NBOARD) = 0.0;
        } else if (i >= NLAD * NBOARD * 2 && i < NLAD * NBOARD * 3) {
            pars.at(i) = deltaZ.at(i - NLAD * NBOARD * 2);
            newPars.at(i) = pars.at(i) - gamma * gradDeltaZ.at(i - NLAD * NBOARD * 2);
            deltaZ.at(i - NLAD * NBOARD * 2) = newPars.at(i);
            gradDeltaZ.at(i - NLAD * NBOARD * 2) = 0.0;
        }
        else {
            pars.at(i) = thetaX.at(i);
            newPars.at(i) = pars.at(i) - gamma * gradThetaX.at(i);
            thetaX.at(i) = newPars.at(i);
            gradThetaX.at(i) = 0.0;
        }
    }
    
    pars.clear();
    newPars.clear();
    pars.shrink_to_fit();
    newPars.shrink_to_fit();
    
}

uint checkGradDir(vdouble &gradDeltaY, vdouble &gradDeltaZ, vdouble &gradThetaX, vdouble &gradDeltaYPrev, vdouble &gradDeltaZPrev, vdouble &gradThetaXPrev, double epsilon) {
    
    uint result{0};
    double scalarProduct{0.0};
    TVectorD grad(NLAD * NBOARD * 3), gradPrev(NLAD * NBOARD * 3);
    for (std::size_t i{0}; i < NLAD * NBOARD * 3; ++i) {
        if (i >= NLAD * NBOARD && i < NLAD * NBOARD * 2) {
            grad[i] = gradDeltaY.at(i - NLAD * NBOARD);
            gradPrev[i] = gradDeltaYPrev.at(i - NLAD * NBOARD);
        } else if (i >= NLAD * NBOARD * 2 && i < NLAD * NBOARD * 3) {
            grad[i] = gradDeltaZ.at(i - NLAD * NBOARD * 2);
            gradPrev[i] = gradDeltaZPrev.at(i - NLAD * NBOARD * 2);
        } else {
            grad[i] = gradThetaX.at(i);
            gradPrev[i] = gradThetaXPrev.at(i);
        }
        scalarProduct += grad[i] * gradPrev[i];
    }
    
    scalarProduct /= sqrt(grad.Norm2Sqr()) * sqrt(gradPrev.Norm2Sqr());
    
    std::cout << "eps: " << epsilon << " scalarProduct: " << scalarProduct << std::endl;
    
    if (scalarProduct < epsilon) result = 1;
    if (scalarProduct < epsilon && scalarProduct > -epsilon) result = 2;
    
    return result;
}

bool checkDeltaZ(vdouble &deltaZ) {
    bool result{false};
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (fabs(deltaZ[i]) > 0.5) result = true;
    }
    return result;
}

bool performFitting(const TVectorD &zCoord, const TVectorD &yCoord, const TVectorD &zErr, const TVectorD &yErr, vdouble &thetaX, vdouble &deltaZ, genfit::Track* fitTrack, genfit::KalmanFitter* fitter) {
    
    int detId(0); // detector ID
    int planeId(0);
    int hitId(0); // hit ID
    
    genfit::PlanarMeasurement* measurement = nullptr;
    
    TMatrixDSym hitCov(2);
    hitCov.UnitMatrix();
    hitCov(1,1) = detectorResolution*detectorResolution / 12;
    hitCov(0,0) = stripLength * stripLength / 4; // 12 - classic approach

    TVectorD hitCoords(2);
    hitCoords[0] = 0;
    hitCoords[1] = 0;
    
    TVector3 detPlaneN, detPlaneU, detPlaneV, detPlane0;
    genfit::AbsTrackRep* cardRep;
    genfit::FitStatus* fitStatus = nullptr;
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        detPlaneN.SetXYZ(0,0,1);
        detPlaneN.SetMag(1);
        hitCoords[0] = 0;
        detPlaneN.RotateX(TMath::DegToRad() * thetaX[i]);
        detPlane0.SetXYZ(0, 0, detectorPos[i] + deltaZ[i]);
        hitCoords[1] = yCoord[i] / TMath::Cos(TMath::DegToRad() * thetaX[i]);
        hitCov(1,1) = yErr[i] * yErr[i] / (TMath::Cos(TMath::DegToRad() * thetaX[i]) * TMath::Cos(TMath::DegToRad() * thetaX[i]));
        detPlaneU = detPlaneN.Orthogonal();
        detPlaneU.SetMag(1);
        detPlaneV = detPlaneN.Cross(detPlaneU);
        detPlaneV.SetMag(1);
        detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
        measurement = new genfit::PlanarMeasurement(hitCoords, hitCov, detId, ++hitId, nullptr);
        measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV)), planeId);
        fitTrack->insertPoint(new genfit::TrackPoint(measurement->clone(), fitTrack));
        delete measurement;
        measurement = nullptr;
    }
    
    if (fitTrack->getNumPoints() == 0) return false;
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track before fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    // do the fit
    try {
        fitter->processTrack(fitTrack);
    }
    catch (genfit::Exception& e) {
        std::cout << "genfit failed to fit track" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track after fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->determineCardinalRep();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with cardinal rep determination after fit" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->getFittedState();
        // print fit result
        // (fitTrack->getFittedState()).Print();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with fittedstate" << std:: endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    cardRep = fitTrack->getCardinalRep();
    
    try {
        fitStatus = fitTrack->getFitStatus(cardRep);
        // std::cout << "N Failed Points: " << fitStatus->getNFailedPoints() << std::endl;
        if (!fitStatus->isFitted()) return false;
        auto nmeas = fitStatus->getNdf();
        if (nmeas < 1) return false;
        auto chi2Value  = fitStatus->getChi2()/nmeas;
        // std::cout << "N measurements: " << nmeas << std::endl;
        // std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
        if (chi2Value < 0) return false;
        if (!fitStatus->isFitConverged()) {
            try {
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->determineCardinalRep();
                fitTrack->getFittedState();
                cardRep = fitTrack->getCardinalRep();
                fitStatus = fitTrack->getFitStatus(cardRep);
                nmeas = fitStatus->getNdf();
                if (nmeas < 1) return false;
                chi2Value  = fitStatus->getChi2()/nmeas;
                // std::cout << "N measurements: " << nmeas << std::endl;
                // std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
                if (chi2Value < 0) return false;
            } catch (genfit::Exception& e) {
                std::cerr << "Exception" << std::endl;
                std::cerr << e.what();
                throw e;
            }
        }
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with Fit Status" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    delete measurement;
    measurement = nullptr;
    
    return true;
}

std::vector<TGeoPhysicalNode*> makePhysicalNodes() {
    std::vector<TGeoPhysicalNode*> physicalNodes(NLAD * NBOARD);
    TGeoPNEntry* pne{nullptr};
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        pne = gGeoManager->SetAlignableEntry(TString::Format("stripX_%lu",i+1), TString::Format("/TOP_1/StripX_%lu",i+1));
        pne->SetPhysicalNode(physicalNodes.at(i));
    }
    
    return physicalNodes;
}

void alignGeometry(vdouble &deltaY, vdouble &deltaZ, vdouble &thetaX, std::vector<TGeoPhysicalNode*> &physicalNodes) {
    TGeoRotation* rot1{nullptr};
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        std::cout << "alignGeometry i: " << i << std::endl;
        if (!physicalNodes.at(i)) physicalNodes.at(i) = gGeoManager->MakePhysicalNode(TString::Format("/TOP_1/StripX_%lu",i+1));
        std::cout << "after MakePhysicalNode i: " << i << std::endl;
        rot1 = new TGeoRotation(("rot" + std::to_string(i + 1)).c_str(), 0, thetaX.at(i), 0);
        physicalNodes.at(i)->Align(new TGeoCombiTrans(0., deltaY.at(i), detectorPos.at(i) + deltaZ.at(i), rot1));
        std::cout << "after Align i: " << i << std::endl;
        delete rot1;
        rot1 = nullptr;
    }
}

void TracksFitting(std::size_t nIter = 10, string inputFile = "synced_clusters.root", string outputFile = "tracking_histos.root", bool Realign = false, bool kalmanFitterEnabled = false, std::string geomFile = "MiniPANGeom.root") {
    
    gStyle->SetOptFit(1);
    
    std::unordered_map<int, vdouble *> vintegrals, vcogs, vsoverns, vcogsAligned;
    std::unordered_map<int, std::vector<vdouble> *> vsovernsPerChannel;
    std::unordered_map<int, vint *> vlengths;
    
    if (nov2021BT) detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81 cm // November 2021 beamtest
    if (june2022BT) detectorPos = {-7.006, -6.206, -0.470, 0.330, 5.870, 6.670}; // 0, 8.0, 65.36, 73.36, 128.76, 136.76 mm // June 2022 beamtest
    
    const int nKalmanIter = 20; // max number of iterations
    const double dPVal = 1.E-3; // convergence criterion
    int pdg;
    // if (!beamTestData) pdg = 13; // particle pdg code for cosmics (mu-)
    pdg = -211; // pi- during beamtest
    genfit::KalmanFitter* fitter = 0;
    genfit::Track* fitTrack(nullptr);
    TVector3 pos(0, 0, detectorPos.front());
    TVector3 mom;
    TMatrixDSym covM(6);
    
    genfit::MeasuredStateOnPlane fittedState;
    genfit::FitStatus* fitStatus = nullptr;
    TVector3 fittedTrackDir, primaryPartDir;
    genfit::TrackPoint* trackPoint = nullptr;
    genfit::AbsMeasurement* rawMeas = nullptr;
    genfit::KalmanFitterInfo* fitterInfo = nullptr;
    genfit::MeasurementOnPlane residual;
    genfit::AbsTrackRep* cardRep = nullptr;
    uint nTrackPoints{0};
    int detId;
    TVectorD scdState(2);
    
    if (kalmanFitterEnabled) {
        
        fitter = new genfit::KalmanFitter(nKalmanIter, dPVal);
        fitter->setMaxIterations(nKalmanIter);
        
        auto geoMan = new TGeoManager("Geometry", "miniPAN geometry");
        geoMan->Import(geomFile.c_str());
        
        genfit::FieldManager::getInstance()->init(new genfit::ConstField(0, 0, 0)); // in kGauss 1 T = 10 kG here we set the mag field of the Earth
        genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
        
        // if (!beamTestData) mom = TVector3(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
        mom = TVector3(0, 0, 10); // 10 GeV/c
        for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution;
        for (int i = 3; i < 6; ++i) covM(i,i) = pow(detectorResolution / 6 / sqrt(3), 2); // 6 is set arbitrary ~ nTrackpoints
    }
    
    TGraphErrors *gr;
    
    TFile *inFile = TFile::Open(inputFile.c_str(), "read");
    TTree *clustersTree = (TTree *) inFile->Get("sync_clusters_tree");
    
    std::vector<std::array<TBranch *, NBRANCH>> branches;
    std::array<TBranch *, NBRANCH> temp_arr;
    branches.assign(NLAD * NBOARD, temp_arr);
    
    int nEntries = clustersTree->GetEntries();
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        vintegrals[i] = new vdouble;
        vcogs[i] = new vdouble;
        vsoverns[i] = new vdouble;
        vlengths[i] = new vint;
        vsovernsPerChannel[i] = new std::vector<vdouble>;
        vcogsAligned[i] = new vdouble;
        clustersTree->SetBranchAddress(Form("integral%lu", i), &vintegrals.at(i), &((branches.at(i)).at(0)));
        clustersTree->SetBranchAddress(Form("cog%lu", i), &vcogs.at(i), &((branches.at(i)).at(2)));
        clustersTree->SetBranchAddress(Form("sovern%lu", i), &vsoverns.at(i), &((branches.at(i)).at(3)));
        clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", i), &vsovernsPerChannel.at(i), &((branches.at(i)).at(4)));
        clustersTree->SetBranchAddress(Form("length%lu", i), &vlengths.at(i), &((branches.at(i)).at(1)));
    }
    
    // branch check
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        for (uint br{0}; br < branches.at(i).size(); br++)
            cout << "ladder " << i << " branch " << br << " has name " << branches.at(i).at(br)->GetName() << endl;
    }
    //*
    std::array<vdouble, NLAD * NBOARD> arrHistos;
    TFile *outFile = TFile::Open(outputFile.c_str(), "recreate");
    TTree *tracksTree = new TTree("tracks_tree", "Fitted tracks");
    //*/
    float chi2, pvalue, rmse_x, rmse_y, rmse_z, xdir, ydir, zdir;
    float xInitDir, yInitDir, zInitDir, momInit, p0, p1;
    int hnmeas, hPDG, hConvTracks;
    UInt_t conversionDepth;
    std::vector<float> xErrors, yErrors, zErrors;
    TMatrixDSym covMatrix, correlMatrix;
    vint lengthOut;
    vdouble integralOut;
    std::vector<vdouble> sovernsPerChannelOut;
    //*
    auto chi2br = tracksTree->Branch("chi2br", &chi2, "chi2/F");
    auto pvaluebr = tracksTree->Branch("pvaluebr", &pvalue, "pvalue/F");
    auto p0br = tracksTree->Branch("p0br", &p0, "p0/F");
    auto p1br = tracksTree->Branch("p1br", &p1, "p1/F");
    auto covMatrixbr = tracksTree->Branch("covMatrixbr", &covMatrix);
    auto corelMatrixbr = tracksTree->Branch("correlMatrixbr", &correlMatrix);
    auto rmse_ybr = tracksTree->Branch("rmse_ybr", &rmse_y, "rmse_y/F");
    //auto xdirbr = tracksTree->Branch("xdirbr", &xdir, "xdir/F");
    //auto ydirbr = tracksTree->Branch("ydirbr", &ydir, "ydir/F");
    //auto zdirbr = tracksTree->Branch("zdirbr", &zdir, "zdir/F");
    auto hnmeasbr = tracksTree->Branch("hnmeasbr", &hnmeas, "hnmeas/I");
    auto yerrbr = tracksTree->Branch("yerrbr", &yErrors);
    auto lengthbr = tracksTree->Branch("clusterlengthbr", &lengthOut);
    auto integralbr = tracksTree->Branch("clusterintegralbr", &integralOut);
    auto sovernsbr = tracksTree->Branch("sovernsbr", &sovernsPerChannelOut);
    // auto hConvTracksbr = tracksTree->Branch("hConvTracksbr", &hConvTracks, "hConvTracks/I");
    //*/
    TCanvas *c = nullptr;
    std::size_t subCanvasIt{1};
    TF1 *fitFunc = new TF1("fitFunc","pol1(0)", detectorPos.front() * 1.05, detectorPos.back() * 1.05);
    TVectorD *yCoord, *zCoord, *yErr, *zErr;
    zCoord = new TVectorD(NLAD * NBOARD, &detectorPos[0]);
    zErr = new TVectorD(NLAD * NBOARD, &detectorPosErr[0]);
    TVectorD chi2array;
    double chi2Sum{0};
    
    uint N1event{0}, N2events{0}, N3events{0}, N4events{0}, N5plusevents{0};
    uint N1GoodEvent{0};
    uint Nnot0Layer{0}, Nnot1Layer{0}, Nnot2Layer{0}, Nnot3Layer{0}, Nnot4Layer{0}, Nnot5Layer{0};
    
    vdouble deltaY(NBOARD * NLAD), deltaZ(NBOARD * NLAD), thetaX(NBOARD * NLAD);
    vdouble gradDeltaY(NBOARD * NLAD), gradDeltaZ(NBOARD * NLAD), gradThetaX(NBOARD * NLAD);
    vdouble chi2Values;
    double gamma{1.0}, reducingFactor{0.5}, epsilon{0.5}, gamma_precision{0.01}, gamma_max{100.}, gamma_min{0.00001};
    uint patience{10}, badChi2Iter{0}, goodChi2Iter{0};
    
    std::array<std::size_t, NBOARD * NLAD> len;
    std::array<std::size_t, NBOARD * NLAD>::iterator it;
    std::vector<float>::iterator errorCut;
    TFitResultPtr r{nullptr};
    std::vector<TH1D*> vYErrors(NLAD * NBOARD);
    vdouble correctionsY(NLAD * NBOARD);
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        vYErrors.at(i) = new TH1D(Form("h%lu", i),Form("Y Residuals, Layer %lu", i), 100, -0.05, 0.05);
        correctionsY.at(i) = 0.;
    }
    
    // nEntries = 20000; // FOR TEST PURPOSES. REMOVE IT LATER !!!!!!!!!
    TRandom randGen;
    
    for (std::size_t event{0}; event < sampleSize; ++event) {
		
		clustersTree->GetEntry(randGen.Integer(nEntries));
        
        for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
        
        it = std::find_if(len.begin(), len.end(), notSingle);
            
        it = std::min_element(len.begin(), len.end());
            
        if (!PRenabled) {
            it = std::find_if(len.begin(), len.end(), notSingle);
            if (it == len.end()) nTracks = 1;
            else continue;
        }
        else nTracks = *it;
            
        for (std::size_t tr{0}; tr < nTracks; ++tr) {
			
			if (june2022BT) updateVcogs(vcogs, vcogsAligned, tr);
            else {
                for (std::size_t i{0}; i < NLAD * NBOARD; ++i) vcogsAligned[i]->push_back(vcogs[i]->at(tr));
            }
            
            fitFunc->SetRange(detectorPos.front() * 1.05, detectorPos.back() * 1.05);
            
            fitFunc->SetParameter(0, ((vcogsAligned[0]->at(tr)  + vcogsAligned[NBOARD * NLAD - 1]->at(tr)) / 2. - 1023.5) * conversionCoef);
            fitFunc->SetParameter(1, (-vcogsAligned[0]->at(tr) + vcogsAligned[NBOARD * NLAD - 1]->at(tr)) * conversionCoef / (detectorPos.back() - detectorPos.front()));
            fitFunc->SetParLimits(0, -1024.0 * conversionCoef, 1024.0 * conversionCoef);
            fitFunc->SetParLimits(1, -2048.0 * conversionCoef / (detectorPos.back() - detectorPos.front()), 2048.0 * conversionCoef / (detectorPos.back() - detectorPos.front()));
            
            
            yCoord = new TVectorD(NLAD * NBOARD, &transformVectorsDouble(vcogsAligned, vsoverns, tr)[0]);
            yErr = new TVectorD(NLAD * NBOARD, &transformVectorsInt(vlengths, vsoverns, tr, false)[0]); // true
            gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
            r = gr->Fit(fitFunc, "QRNSEM");
            chi2 = r->Chi2() / r->Ndf();
            
            evaluateUnbiasedResiduals(yErrors, yCoord, yErr, zCoord, zErr, fitFunc);
            
            errorCut = std::find_if(yErrors.begin(), yErrors.end(), [](float i){ return std::abs(i) > 0.05; }); // 500 um cut on residuals
            
            if (errorCut == std::end(yErrors)) for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
                (vYErrors.at(i))->Fill(yErrors.at(i));
                correctionsY.at(i) = (vYErrors.at(i))->GetMean();
                correctionsY.at(i) = 0.; // FOR TEST PURPOSES. REMOVE IT LATER !!!!!!!!!
            }
			
			delete yCoord;
            yCoord = nullptr;
            delete yErr;
            yErr = nullptr;
            delete gr;
            gr = nullptr;
        }
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            vcogsAligned[i]->clear();
            vcogsAligned[i]->shrink_to_fit();
        }
	}
    
    for (std::size_t event{0}; event < sampleSize; ++event) {
        
        clustersTree->GetEntry(randGen.Integer(nEntries));
        
        for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
        it = std::find_if(len.begin(), len.end(), notTwo);
        if (it == len.end()) N2events++;
        it = std::find_if(len.begin(), len.end(), notThree);
        if (it == len.end()) N3events++;
        it = std::find_if(len.begin(), len.end(), notFour);
        if (it == len.end()) N4events++;
        it = std::find_if(len.begin(), len.end(), fivePlus);
        if (it == len.end()) N5plusevents++;
        
        it = std::find_if(len.begin() + 1, len.end(), notSingle);
        if (it == len.end()) Nnot0Layer++;
        
        if (len[0] == 1) {
            it = std::find_if(len.begin() + 2, len.end(), notSingle);
            if (it == len.end()) Nnot1Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 2, notSingle);
        if (it == len.begin() + 2) {
            it = std::find_if(len.begin() + 3, len.end(), notSingle);
            if (it == len.end()) Nnot2Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 3, notSingle);
        if (it == len.begin() + 3) {
            it = std::find_if(len.begin() + 4, len.end(), notSingle);
            if (it == len.end()) Nnot3Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 4, notSingle);
        if (it == len.begin() + 4) {
            if (len[5] == 1) Nnot4Layer++;
        }
        
        it = std::find_if(len.begin(), len.end() - 1, notSingle);
        if (it == len.end() - 1) Nnot5Layer++;
        
        it = std::min_element(len.begin(), len.end());
        
        if (!PRenabled) {
            it = std::find_if(len.begin(), len.end(), notSingle);
            if (it == len.end()) nTracks = 1;
            else continue;
        }
        else nTracks = *it;
        
        for (std::size_t tr{0}; tr < nTracks; ++tr) {
            
            N1event++;
            
            // std::cout << "event: " << event << std::endl;
            
            if (june2022BT) updateVcogs(vcogs, vcogsAligned, tr);
            else {
                for (std::size_t i{0}; i < NLAD * NBOARD; ++i) vcogsAligned[i]->push_back(vcogs[i]->at(tr));
            }
            
            fitFunc->SetRange(detectorPos.front() * 1.05, detectorPos.back() * 1.05);
            
            fitFunc->SetParameter(0, ((vcogsAligned[0]->at(tr)  + vcogsAligned[NBOARD * NLAD - 1]->at(tr)) / 2. - 1023.5) * conversionCoef);
            fitFunc->SetParameter(1, (-vcogsAligned[0]->at(tr) + vcogsAligned[NBOARD * NLAD - 1]->at(tr)) * conversionCoef / (detectorPos.back() - detectorPos.front()));
            fitFunc->SetParLimits(0, -1024.0 * conversionCoef, 1024.0 * conversionCoef);
            fitFunc->SetParLimits(1, -2048.0 * conversionCoef / (detectorPos.back() - detectorPos.front()), 2048.0 * conversionCoef / (detectorPos.back() - detectorPos.front()));
            
            
            yCoord = new TVectorD(NLAD * NBOARD, &transformVectorsDouble(vcogsAligned, vsoverns, tr)[0]);
            yErr = new TVectorD(NLAD * NBOARD, &transformVectorsInt(vlengths, vsoverns, tr, false)[0]); // true
            gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
            r = gr->Fit(fitFunc, "QRNSEM");
            chi2 = r->Chi2() / r->Ndf();
            pvalue = r->Prob();
            covMatrix.ResizeTo(r->GetCovarianceMatrix());
            correlMatrix.ResizeTo(r->GetCorrelationMatrix());
            covMatrix = r->GetCovarianceMatrix();
            correlMatrix = r->GetCorrelationMatrix();
            p0 = r->Parameter(0);
            p1 = r->Parameter(1);
            // r->Print("V");
            
            lengthOut.clear();
            lengthOut.shrink_to_fit();
            integralOut.clear();
            integralOut.shrink_to_fit();
            sovernsPerChannelOut.clear();
            sovernsPerChannelOut.shrink_to_fit();
            
            if (event % 100 == 0) {
                if (!c) {
                    c = new TCanvas("c", "A fitted track", 1280, 800);
                    c->Divide(5,2);
                }
                if (subCanvasIt < 11) {
                    c->cd(subCanvasIt);
                    gr->SetTitle(Form("Event %lu", event));
                    gr->GetXaxis()->SetTitle("Layer position Z, cm");
                    gr->GetYaxis()->SetTitle("Channel number");
                    gr->Draw("AP");
                    fitFunc->Draw("same");
                    c->Update();
                    subCanvasIt++;
                    yCoord->Print();
                    zCoord->Print();
                    r->Print("V");
                    
                }
            }
            
            hnmeas = yCoord->GetNrows();
            evaluateUnbiasedResiduals(yErrors, yCoord, yErr, zCoord, zErr, fitFunc);
            
            errorCut = std::find_if(yErrors.begin(), yErrors.end(), [](float i){ return std::abs(i) > 0.05; }); // 500 um cut on residuals
            
            if (errorCut == std::end(yErrors)) {
                chi2Sum += r->Chi2();
                chi2Values.push_back(chi2);
            }
            
            if (errorCut == std::end(yErrors)) N1GoodEvent++;
            
            if (errorCut == std::end(yErrors)) {
                calculateGrad(fitFunc, gradDeltaY, yCoord, yErr, zCoord, zErr, 0, deltaY);
                calculateGrad(fitFunc, gradDeltaZ, yCoord, yErr, zCoord, zErr, 1, deltaY);
                calculateGrad(fitFunc, gradThetaX, yCoord, yErr, zCoord, zErr, 2, deltaY);
                // gradDeltaY.at(i) += calculateGradDeltaY(fitFunc, (*yCoord)[i], (*yErr)[i], (*zCoord)[i], deltaY.at(i), deltaZ.at(i), thetaX.at(i), yErrors[i], correctionsY.at(i));
                // gradDeltaZ.at(i) += calculateGradDeltaZ(fitFunc, (*yCoord)[i], (*yErr)[i], (*zCoord)[i], deltaY.at(i), deltaZ.at(i), thetaX.at(i), yErrors[i], correctionsY.at(i));
                // gradThetaX.at(i) += calculateGradThetaX(fitFunc, (*yCoord)[i], (*yErr)[i], (*zCoord)[i], deltaY.at(i), deltaZ.at(i), thetaX.at(i), yErrors[i], correctionsY.at(i));
            }
            
            rmse_y = TMath::RMS(yErrors.begin(), yErrors.end());
            
            for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
                lengthOut.push_back((vlengths.at(i))->at(tr));
                integralOut.push_back((vintegrals.at(i))->at(tr));
                sovernsPerChannelOut.push_back((vsovernsPerChannel.at(i))->at(tr));
            }
            
            tracksTree->Fill();
            
            delete yCoord;
            yCoord = nullptr;
            delete yErr;
            yErr = nullptr;
            delete gr;
            gr = nullptr;
        }
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            vcogsAligned[i]->clear();
            vcogsAligned[i]->shrink_to_fit();
        }
    }
    
    chi2array.ResizeTo(1);
    chi2array[0] = chi2Sum / N1GoodEvent;
    auto chi2Cut = TMath::Mean(chi2Values.begin(), chi2Values.end()) + TMath::StdDev(chi2Values.begin(), chi2Values.end());
    auto chi2CutOld = chi2Cut;
    
    chi2Sum = 0.;
    chi2Values.clear();
    chi2Values.shrink_to_fit();
    
    // Alignment block
    
    std::cout << "N1GoodEvent " << N1GoodEvent << " out of " << N1event << std::endl;
    std::cout << "Chi2 without alignment: " << chi2array[0] << std::endl;
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        gradDeltaY.at(i) /= N1GoodEvent;
        gradDeltaZ.at(i) /= N1GoodEvent;
        gradThetaX.at(i) /= N1GoodEvent;
        std::cout << "gradDeltaY[" << i << "]: " << gradDeltaY.at(i) << std::endl;
        std::cout << "gradDeltaZ[" << i << "]: " << gradDeltaZ.at(i) << std::endl;
        std::cout << "gradThetaX[" << i << "]: " << gradThetaX.at(i) << std::endl;
        std::cout << "DeltaY[" << i << "]: " << deltaY.at(i) << std::endl;
        std::cout << "DeltaZ[" << i << "]: " << deltaZ.at(i) << std::endl;
        std::cout << "ThetaX[" << i << "]: " << thetaX.at(i) << std::endl;
    }
    
    // Find optimal gamma for the first step
    
    uint step = 0;
    vdouble newDetectorPos(NLAD * NBOARD);
    vdouble deltaYPrev, deltaZPrev, thetaXPrev;
    double golden_ratio{(1. + std::sqrt(5.)) / 2.}, point_c{0.}, point_d{0.};
    
    std::copy(deltaY.begin(), deltaY.end(), std::back_inserter(deltaYPrev));
    std::copy(deltaZ.begin(), deltaZ.end(), std::back_inserter(deltaZPrev));
    std::copy(thetaX.begin(), thetaX.end(), std::back_inserter(thetaXPrev));
    
    vdouble gradDeltaYPrev, gradDeltaZPrev, gradThetaXPrev;
    
    std::copy(gradDeltaY.begin(), gradDeltaY.end(), std::back_inserter(gradDeltaYPrev));
    std::copy(gradDeltaZ.begin(), gradDeltaZ.end(), std::back_inserter(gradDeltaZPrev));
    std::copy(gradThetaX.begin(), gradThetaX.end(), std::back_inserter(gradThetaXPrev));
    
    while (std::abs(gamma_max - gamma_min) > gamma_precision) {
		
		if (step == 0) gamma = gamma_min;
		else if (step == 1) gamma = gamma_max;
		else if (step == 2) {gamma = gamma_max - (gamma_max - gamma_min) / golden_ratio; point_c = gamma;}
		else {gamma = gamma_min + (gamma_max - gamma_min) / golden_ratio; point_d = gamma;}
		
		std::cout << "step: " << step << " gamma: " << gamma << std::endl;
		
		makeGradStep(deltaY, deltaZ, thetaX, gradDeltaY, gradDeltaZ, gradThetaX, gamma);
		
		for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
			std::cout << "DeltaY[" << i << "]: " << deltaY.at(i) << std::endl;
			std::cout << "DeltaZ[" << i << "]: " << deltaZ.at(i) << std::endl;
			std::cout << "ThetaX[" << i << "]: " << thetaX.at(i) << std::endl;
		}
		
		N1GoodEvent = 0;
		chi2Sum = 0.;
    
		for (std::size_t event{0}; event < sampleSize; ++event) {
			
			clustersTree->GetEntry(randGen.Integer(nEntries));
			
			for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
			
			it = std::find_if(len.begin(), len.end(), notSingle);
				
			it = std::min_element(len.begin(), len.end());
				
			if (!PRenabled) {
				it = std::find_if(len.begin(), len.end(), notSingle);
				if (it == len.end()) nTracks = 1;
				else continue;
			}
			else nTracks = *it;
				
			for (std::size_t tr{0}; tr < nTracks; ++tr) {
				
				newDetectorPos.clear();
                newDetectorPos.shrink_to_fit();
                if (june2022BT) {
                    updateVcogs(vcogs, vcogsAligned, tr);
                    newDetectorPos = updateDetPos(vcogsAligned, deltaZ, thetaX, tr);
                }
                else newDetectorPos = updateDetPos(vcogs, deltaZ, thetaX, tr);
                updateCoGs(vcogs, vcogsAligned, deltaY, thetaX, tr);
                applyCorrections(vcogsAligned, correctionsY, tr);
                
                delete fitFunc;
                fitFunc = nullptr;
                fitFunc = new TF1("fitFunc","pol1(0)", newDetectorPos.front() * 1.05, newDetectorPos.back() * 1.05);
                
                fitFunc->SetParameter(0, ((vcogsAligned[0]->at(tr)  + vcogsAligned[NBOARD * NLAD - 1]->at(tr)) / 2. - 1023.5) * conversionCoef);
                fitFunc->SetParameter(1, (-vcogsAligned[0]->at(tr) + vcogsAligned[NBOARD * NLAD - 1]->at(tr)) * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()));
                fitFunc->SetParLimits(0, -1024.0 * conversionCoef, 1024.0 * conversionCoef);
                fitFunc->SetParLimits(1, -2048.0 * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()), 2048.0 * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()));
                
                yCoord = new TVectorD(NLAD * NBOARD, &transformVectorsDouble(vcogsAligned, vsoverns, tr)[0]);
                yErr = new TVectorD(NLAD * NBOARD, &transformVectorsInt(vlengths, vsoverns, tr, false)[0]);
                zCoord = new TVectorD(NLAD * NBOARD, &newDetectorPos[0]);
                
                gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
                fitFunc->SetRange(newDetectorPos.front() * 1.05, newDetectorPos.back() * 1.05);
                r = gr->Fit(fitFunc, "QRNSEM");
				
				evaluateUnbiasedResiduals(yErrors, yCoord, yErr, zCoord, zErr, fitFunc);
				
				errorCut = std::find_if(yErrors.begin(), yErrors.end(), [](float i){ return std::abs(i) > 0.05; }); // 500 um cut on residuals
				
				if (errorCut == std::end(yErrors)) {
					chi2Sum += r->Chi2();
				}
            
				if (errorCut == std::end(yErrors)) N1GoodEvent++;
				
				delete yCoord;
				yCoord = nullptr;
				delete yErr;
				yErr = nullptr;
				delete gr;
				gr = nullptr;
			}
			
			for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
				vcogsAligned[i]->clear();
				vcogsAligned[i]->shrink_to_fit();
			}
		}
		
		if (N1GoodEvent > 1000) chi2Values.push_back(chi2Sum / N1GoodEvent);
		else chi2Values.push_back(-1.);
		
		if (step > 2) {
			std::cout << "chi2Values.at(2): " << chi2Values.at(2) << " chi2Values.at(3): " << chi2Values.at(3) << std::endl;
			if (chi2Values.at(3) < 0.) {
				gamma_max = point_d;
			}
			else {
				if (chi2Values.at(2) < chi2Values.at(3) && chi2Values.at(2) > 0.) gamma_max = point_d;
				else gamma_min = point_c;
			}
			step = 1;
			chi2Values.pop_back();
			chi2Values.pop_back();
			std::cout << "gamma_max: " << gamma_max << " gamma_min: " << gamma_min << std::endl;
		}
		
		std::copy(deltaYPrev.begin(), deltaYPrev.end(), deltaY.begin());
		std::copy(deltaZPrev.begin(), deltaZPrev.end(), deltaZ.begin());
		std::copy(thetaXPrev.begin(), thetaXPrev.end(), thetaX.begin());
		
		std::copy(gradDeltaYPrev.begin(), gradDeltaYPrev.end(), gradDeltaY.begin());
		std::copy(gradDeltaZPrev.begin(), gradDeltaZPrev.end(), gradDeltaZ.begin());
		std::copy(gradThetaXPrev.begin(), gradThetaXPrev.end(), gradThetaX.begin());
		
		step++;
	}
	
	gamma = (gamma_max + gamma_min) / 2.;
	double gamma_optimal{gamma};
	std::cout << "Optimal gamma for the first step: " << gamma << std::endl;
	chi2Sum = 0.;
	chi2Values.clear();
	chi2Values.shrink_to_fit();
	
	outFile->cd();
    tracksTree->Write();
    outFile->Close();
    // delete tracksTree;
    
    //*
    string alignedFileName(outputFile.begin(), outputFile.end() - 5);
    alignedFileName += "_aligned.root";
    TFile *alignedFile = TFile::Open(alignedFileName.c_str(), "recreate");
    TTree *alignedTracksTree = new TTree("aligned_Tracks_tree", "Fitted aligned tracks");
    
    float chi2_aligned, pvalue_aligned, rmse_y_aligned;
    float p0_aligned, p1_aligned;
    int hnmeas_aligned;
    std::vector<float> yErrors_aligned;
    TMatrixDSym covMatrix_aligned, correlMatrix_aligned;
    //*
    auto chi2br_aligned = alignedTracksTree->Branch("chi2br", &chi2_aligned, "chi2_aligned/F");
    auto pvaluebr_aligned = alignedTracksTree->Branch("pvaluebr", &pvalue_aligned, "pvalue_aligned/F");
    auto p0br_aligned = alignedTracksTree->Branch("p0br", &p0_aligned, "p0_aligned/F");
    auto p1br_aligned = alignedTracksTree->Branch("p1br", &p1_aligned, "p1_aligned/F");
    auto covMatrixbr_aligned = alignedTracksTree->Branch("covMatrixbr", &covMatrix_aligned);
    auto corelMatrixbr_aligned = alignedTracksTree->Branch("correlMatrixbr", &correlMatrix_aligned);
    auto rmse_ybr_aligned = alignedTracksTree->Branch("rmse_ybr", &rmse_y_aligned, "rmse_y_aligned/F");
    auto hnmeasbr_aligned = alignedTracksTree->Branch("hnmeasbr", &hnmeas_aligned, "hnmeas_aligned/I");
    auto yerrbr_aligned = alignedTracksTree->Branch("yerrbr", &yErrors_aligned);
    
    TCanvas *ac = nullptr;
    subCanvasIt = 1;
    N1GoodEvent = 0;
    N1event = 0;
    int nAlignmentIterations = nIter;
    chi2array.ResizeTo(nAlignmentIterations + 1);
    
    int iterAlign{0};
    
    std::vector<TGeoPhysicalNode*> physicalNodes(NLAD * NBOARD);
    physicalNodes.assign(NLAD * NBOARD, nullptr);
    genfit::AbsTrackRep* rep{nullptr};
    bool fitResult{false};
    uint gradDirResult{0};
    
    // if (kalmanFitterEnabled) physicalNodes = makePhysicalNodes();
    
    while (iterAlign < nAlignmentIterations) {
        
        makeGradStep(deltaY, deltaZ, thetaX, gradDeltaY, gradDeltaZ, gradThetaX, gamma);
        
        if (kalmanFitterEnabled && iterAlign > 5) alignGeometry(deltaY, deltaZ, thetaX, physicalNodes);
        if (iterAlign >= nAlignmentIterations - 10) sampleSize = nEntries;
        
        N1event = 0;
    
        for (std::size_t event{0}; event < sampleSize; ++event) {
            
            clustersTree->GetEntry(randGen.Integer(nEntries));
            if (iterAlign >= nAlignmentIterations - 10) clustersTree->GetEntry(event);
            
            for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
            
            it = std::find_if(len.begin(), len.end(), notSingle);
            
            it = std::min_element(len.begin(), len.end());
            
            if (!PRenabled) {
                it = std::find_if(len.begin(), len.end(), notSingle);
                if (it == len.end()) nTracks = 1;
                else continue;
            }
            else nTracks = *it;
            
            for (std::size_t tr{0}; tr < nTracks; ++tr) {
                
                N1event++;
                
                // std::cout << "event: " << event << std::endl;
                newDetectorPos.clear();
                newDetectorPos.shrink_to_fit();
                if (june2022BT) {
                    updateVcogs(vcogs, vcogsAligned, tr);
                    newDetectorPos = updateDetPos(vcogsAligned, deltaZ, thetaX, tr);
                }
                else newDetectorPos = updateDetPos(vcogs, deltaZ, thetaX, tr);
                updateCoGs(vcogs, vcogsAligned, deltaY, thetaX, tr);
                applyCorrections(vcogsAligned, correctionsY, tr);
                
                delete fitFunc;
                fitFunc = nullptr;
                fitFunc = new TF1("fitFunc","pol1(0)", newDetectorPos.front() * 1.05, newDetectorPos.back() * 1.05);
                
                fitFunc->SetParameter(0, ((vcogsAligned[0]->at(tr)  + vcogsAligned[NBOARD * NLAD - 1]->at(tr)) / 2. - 1023.5) * conversionCoef);
                fitFunc->SetParameter(1, (-vcogsAligned[0]->at(tr) + vcogsAligned[NBOARD * NLAD - 1]->at(tr)) * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()));
                fitFunc->SetParLimits(0, -1024.0 * conversionCoef, 1024.0 * conversionCoef);
                fitFunc->SetParLimits(1, -2048.0 * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()), 2048.0 * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()));
                
                yCoord = new TVectorD(NLAD * NBOARD, &transformVectorsDouble(vcogsAligned, vsoverns, tr)[0]);
                yErr = new TVectorD(NLAD * NBOARD, &transformVectorsInt(vlengths, vsoverns, tr, false)[0]);
                zCoord = new TVectorD(NLAD * NBOARD, &newDetectorPos[0]);
                
                gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
                fitFunc->SetRange(newDetectorPos.front() * 1.05, newDetectorPos.back() * 1.05);
                r = gr->Fit(fitFunc, "QRNSEM");
                fitResult = false;
                
                if (kalmanFitterEnabled && iterAlign > 5) {
                    delete fitTrack;
                    fitTrack = nullptr;
                    
                    double sign{1.};
                    rep = new genfit::RKTrackRep(sign*pdg);
                    genfit::MeasuredStateOnPlane stateRef(rep);
                    pos = TVector3(0, 0, newDetectorPos.front());
                    mom = TVector3(0, 0, 10);
                    for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution;
                    for (int i = 3; i < 6; ++i) covM(i,i) = pow(detectorResolution / 6 / sqrt(3), 2);
                    rep->setPosMomCov(stateRef, pos, mom, covM);
                    
                    // create track
                    TVectorD seedState(6);
                    TMatrixDSym seedCov(6);
                    rep->get6DStateCov(stateRef, seedState, seedCov);
                    fitTrack = new genfit::Track(rep, seedState, seedCov);
                    
                    fitResult = performFitting(*zCoord, *yCoord, *zErr, *yErr, thetaX, deltaZ, fitTrack, fitter);
                    
                    cardRep = fitTrack->getCardinalRep();
                    
                    fitStatus = fitTrack->getFitStatus(cardRep);
                    chi2_aligned = fitStatus->getChi2() / fitStatus->getNdf();
                    pvalue_aligned = fitStatus->getPVal();
                    
                    nTrackPoints = fitTrack->getNumPoints();
                    
                }
                else {
                    chi2_aligned = r->Chi2() / r->Ndf();
                    pvalue_aligned = r->Prob();
                    covMatrix_aligned.ResizeTo(r->GetCovarianceMatrix());
                    covMatrix_aligned = r->GetCovarianceMatrix();
                }
                
                chi2Values.push_back(chi2_aligned);
                correlMatrix_aligned.ResizeTo(r->GetCorrelationMatrix());
                correlMatrix_aligned = r->GetCorrelationMatrix();
                p0_aligned = r->Parameter(0);
                p1_aligned = r->Parameter(1);
                // r->Print("V");
                if (iterAlign == nAlignmentIterations - 1 && event % 100 == 0) {
                    if (!ac) {
                        ac = new TCanvas("ac", "A fitted aligned track", 1280, 800);
                        ac->Divide(5,2);
                    }
                    if (subCanvasIt < 11) {
                        ac->cd(subCanvasIt);
                        gr->SetTitle(Form("Event %lu", event));
                        gr->GetXaxis()->SetTitle("Layer position Z, cm");
                        gr->GetYaxis()->SetTitle("Channel number");
                        gr->Draw("AP");
                        fitFunc->Draw("same");
                        ac->Update();
                        subCanvasIt++;
                        yCoord->Print();
                        zCoord->Print();
                        r->Print("V");
                    }
                }
                
                hnmeas_aligned = yCoord->GetNrows();
                
                if (kalmanFitterEnabled && iterAlign > 5) {
                    
                    yErrors_aligned.clear();
                    yErrors_aligned.shrink_to_fit();
                    
                    if (fitResult && fitStatus->isFitConverged()) {
                        
                        fittedState = fitTrack->getFittedState();
                        covMatrix_aligned.ResizeTo(fittedState.getCov());
                        covMatrix_aligned = fittedState.getCov();
                        
                        for (std::size_t i = 0; i < nTrackPoints; ++i) {
                            trackPoint = fitTrack->getPointWithMeasurementAndFitterInfo(i, cardRep);
                            rawMeas = trackPoint->getRawMeasurement();
                            fittedState = fitTrack->getFittedState(i, cardRep);
                            detId = rawMeas->getDetId();
                            fitterInfo = trackPoint->getKalmanFitterInfo();
                            residual = fitterInfo->getResidual();
                            scdState = residual.getState();
                            yErrors_aligned.push_back(scdState[1]);
                        }
                        
                        // for (std::vector<float>::iterator yError = yErrors_aligned.begin(); yError < yErrors_aligned.end(); yError++) std::cout << "yError[" << std::distance(yErrors_aligned.begin(), yError) << "]: " << *yError << std::endl;
                        
                    }
                    
                    fitTrack->prune();
                }
                else{
                    evaluateUnbiasedResiduals(yErrors_aligned, yCoord, yErr, zCoord, zErr, fitFunc);
                    fitResult = true;
                    fitFunc->SetRange(newDetectorPos.front() * 1.05, newDetectorPos.back() * 1.05);
                }
                
                if (fitResult && !yErrors_aligned.empty()) {
                
                    errorCut = std::find_if(yErrors_aligned.begin(), yErrors_aligned.end(), [](float i){ return std::abs(i) > 0.03; }); // 300 um cut on residuals
                    
                    if (errorCut == std::end(yErrors_aligned) && chi2_aligned < chi2Cut) { // && chi2_aligned < 5 * chi2Cut
                        if (kalmanFitterEnabled  && iterAlign > 5) chi2Sum += fitStatus->getChi2();
                        else chi2Sum += r->Chi2();
                    }
                    
                    if (errorCut == std::end(yErrors_aligned) && chi2_aligned < chi2Cut) N1GoodEvent++;
                    
                    if (errorCut == std::end(yErrors_aligned) && chi2_aligned < chi2Cut) {
                        calculateGrad(fitFunc, gradDeltaY, yCoord, yErr, zCoord, zErr, 0, deltaY);
                        calculateGrad(fitFunc, gradDeltaZ, yCoord, yErr, zCoord, zErr, 1, deltaY);
                        calculateGrad(fitFunc, gradThetaX, yCoord, yErr, zCoord, zErr, 2, deltaY);
                        // gradDeltaY.at(i) += calculateGradDeltaY(fitFunc, (*yCoord)[i], (*yErr)[i], (*zCoord)[i], deltaY.at(i), deltaZ.at(i), thetaX.at(i), yErrors_aligned[i], 0.);
                        // gradDeltaZ.at(i) += calculateGradDeltaZ(fitFunc, (*yCoord)[i], (*yErr)[i], (*zCoord)[i], deltaY.at(i), deltaZ.at(i), thetaX.at(i), yErrors_aligned[i], 0.);
                        // gradThetaX.at(i) += calculateGradThetaX(fitFunc, (*yCoord)[i], (*yErr)[i], (*zCoord)[i], deltaY.at(i), deltaZ.at(i), thetaX.at(i), yErrors_aligned[i], 0.);
                    }
                    
                    rmse_y_aligned = TMath::RMS(yErrors_aligned.begin(), yErrors_aligned.end());
                    
                }
                
                if (iterAlign == nAlignmentIterations - 1) alignedTracksTree->Fill();
                
                fitResult = false;
                
                delete rep;
                rep = nullptr;
                delete yCoord;
                yCoord = nullptr;
                delete zCoord;
                zCoord = nullptr;
                delete yErr;
                yErr = nullptr;
                delete gr;
                gr = nullptr;
            }
            
            for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
                vcogsAligned[i]->clear();
                vcogsAligned[i]->shrink_to_fit();
            }
            
        }
        
        std::cout << "N1GoodEvent: " << N1GoodEvent << std::endl;
        std::cout << "chi2Sum: " << chi2Sum << std::endl;
        
        if (N1GoodEvent != 0) chi2array[iterAlign + 1] = chi2Sum / N1GoodEvent;
        else chi2array[iterAlign + 1] = -1.;
        chi2Cut = TMath::Mean(chi2Values.begin(), chi2Values.end()) + TMath::StdDev(chi2Values.begin(), chi2Values.end());
        if (isnan(chi2Cut)) chi2Cut = chi2CutOld;
        else chi2CutOld = chi2Cut;
        chi2Values.clear();
        chi2Values.shrink_to_fit();
        chi2Sum = 0.;
        
        std::cout << "Chi2 without alignment: " << chi2array[0] << std::endl;
        std::cout << "Chi2 prev: " << chi2array[iterAlign] << std::endl;
        std::cout << "Chi2 new: " << chi2array[iterAlign + 1] << std::endl;
        std::cout << "N1GoodEvent " << N1GoodEvent << " out of " << N1event << std::endl;
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
			if (N1GoodEvent != 0)  {
				gradDeltaY.at(i) /= N1GoodEvent;
				gradDeltaZ.at(i) /= N1GoodEvent;
				gradThetaX.at(i) /= N1GoodEvent;
			}
            std::cout << "gradDeltaY[" << i << "]: " << gradDeltaY.at(i) << std::endl;
            std::cout << "gradDeltaZ[" << i << "]: " << gradDeltaZ.at(i) << std::endl;
            std::cout << "gradThetaX[" << i << "]: " << gradThetaX.at(i) << std::endl;
        }
        
        if (N1GoodEvent != 0) gradDirResult = checkGradDir(gradDeltaY, gradDeltaZ, gradThetaX, gradDeltaYPrev, gradDeltaZPrev, gradThetaXPrev, epsilon);
        else gradDirResult = 1;
        
        if (gradDirResult > 0 || chi2array[iterAlign + 1] < 0. || N1GoodEvent < 1000) {
            std::cout<< "Bad direction of the next gradient " << iterAlign << std::endl;
            std::cout<< "Current gamma: " << gamma << std::endl;
            
            //*
            if (chi2array[iterAlign + 1] < 0. || checkDeltaZ(deltaZ)) {           
                iterAlign--;
                badChi2Iter = 0;
				std::copy(deltaYPrev.begin(), deltaYPrev.end(), deltaY.begin());
				std::copy(deltaZPrev.begin(), deltaZPrev.end(), deltaZ.begin());
				std::copy(thetaXPrev.begin(), thetaXPrev.end(), thetaX.begin());
						
				std::copy(gradDeltaYPrev.begin(), gradDeltaYPrev.end(), gradDeltaY.begin());
				std::copy(gradDeltaZPrev.begin(), gradDeltaZPrev.end(), gradDeltaZ.begin());
				std::copy(gradThetaXPrev.begin(), gradThetaXPrev.end(), gradThetaX.begin());
            }
            else {
				std::copy(deltaY.begin(), deltaY.end(), deltaYPrev.begin());
				std::copy(deltaZ.begin(), deltaZ.end(), deltaZPrev.begin());
				std::copy(thetaX.begin(), thetaX.end(), thetaXPrev.begin());
            
				std::copy(gradDeltaY.begin(), gradDeltaY.end(), gradDeltaYPrev.begin());
				std::copy(gradDeltaZ.begin(), gradDeltaZ.end(), gradDeltaZPrev.begin());
				std::copy(gradThetaX.begin(), gradThetaX.end(), gradThetaXPrev.begin());
			}
            //*/
            
            goodChi2Iter = 0;
            badChi2Iter++;
            if (gamma > 1.e-3 || iterAlign >= nAlignmentIterations - 10) gamma *= reducingFactor;
            if(badChi2Iter > patience) {
                std::cout << "Ran out of patience" << std::endl;
                if (gradDirResult == 2) gamma /= pow(reducingFactor, patience + 2);
                else gamma /= pow(reducingFactor, patience + 1);
                // if (gamma > 2. * gamma_optimal) gamma = 2. * gamma_optimal;
                std::cout<< "New gamma: " << gamma << std::endl;
                badChi2Iter = 0;
            }
        }
        else {
            std::cout<< "Good direction of the next gradient " << iterAlign << std::endl;
            std::cout<< "gamma: " << gamma << std::endl;
            std::copy(deltaY.begin(), deltaY.end(), deltaYPrev.begin());
            std::copy(deltaZ.begin(), deltaZ.end(), deltaZPrev.begin());
            std::copy(thetaX.begin(), thetaX.end(), thetaXPrev.begin());
            
            std::copy(gradDeltaY.begin(), gradDeltaY.end(), gradDeltaYPrev.begin());
            std::copy(gradDeltaZ.begin(), gradDeltaZ.end(), gradDeltaZPrev.begin());
            std::copy(gradThetaX.begin(), gradThetaX.end(), gradThetaXPrev.begin());
            badChi2Iter = 0;
            goodChi2Iter++;
            if(goodChi2Iter > 5 * patience) {
                std::cout << "Ran out of patience" << std::endl;
                gamma /= reducingFactor;
                std::cout<< "New gamma: " << gamma << std::endl;
                goodChi2Iter = 0;
            }
        }
        
        N1GoodEvent = 0;
        iterAlign++;
    }
    
    alignedFile->cd();
    alignedTracksTree->Write();
    alignedFile->Close();
    // delete alignedTracksTree;
    
    TVectorD iterArray(nAlignmentIterations + 1);
    
    for (std::size_t i{0}; i < iterArray.GetNrows(); ++i) iterArray[i] = i;
    
    TCanvas *ic = new TCanvas("ic", "Number of alignment iterations", 1280, 800);
    TGraph *grAlign = new TGraph(iterArray, chi2array);
    grAlign->SetTitle("Alignment convergence");
    grAlign->GetXaxis()->SetTitle("N iterations");
    grAlign->GetYaxis()->SetTitle("Global #chi^{2}/Ntracks");
    
    grAlign->Draw("ALP");
    ic->Update();
    
    //*/
    inFile->Close();
    
    std::cout << "Total number of events: " << nEntries << std::endl;
    std::cout << "Number of events with 1 cluster in every plane: " << N1event << std::endl;
    std::cout << "Number of events with 2 clusters in every plane: " << N2events << std::endl;
    std::cout << "Number of events with 3 clusters in every plane: " << N3events << std::endl;
    std::cout << "Number of events with 4 clusters in every plane: " << N4events << std::endl;
    std::cout << "Number of events with 5+ clusters in every plane: " << N5plusevents << std::endl;
    
    std::cout << "Efficiency, layer 0: " << N1event * 100.0 / Nnot0Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 1: " << N1event * 100.0 / Nnot1Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 2: " << N1event * 100.0 / Nnot2Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 3: " << N1event * 100.0 / Nnot3Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 4: " << N1event * 100.0 / Nnot4Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 5: " << N1event * 100.0 / Nnot5Layer << " %" << std::endl;
    
    double totalEff = (N1event * 1.0 / Nnot0Layer) * (N1event * 1.0 / Nnot1Layer) * (N1event * 1.0 / Nnot2Layer) * (N1event * 1.0 / Nnot3Layer) * (N1event * 1.0 / Nnot4Layer) * (N1event * 1.0 / Nnot5Layer);
    
    std::cout << "Tracking efficiency: " << totalEff * 100.0 << " %" << std::endl;
    
    if (nov2021BT) std::cout << "MCS contribution per layer for 10 GeV/c pions" << std::endl;
    if (june2022BT) std::cout << "MCS contribution per layer for 15 GeV/c protons" << std::endl;
    if (august2022BT) std::cout << "MCS contribution per layer for 180 GeV/c pions" << std::endl;
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) std::cout << "Layer " << i << " sigmaMCS: " << MCSSigma(i) * 10000.0 << " um" << std::endl;
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        std::cout << "layer: " << i << std::endl;
        std::cout << "deltaY: " << (deltaY[i] + correctionsY.at(i)) * 1E+04 << " um" << std::endl;
        std::cout << "deltaZ: " << deltaZ[i] * 1E+04 << " um" << std::endl;
        std::cout << "thetaX: " << thetaX[i] << " " << std::endl;
        // delete vYErrors.at(i);
    }
    
}
