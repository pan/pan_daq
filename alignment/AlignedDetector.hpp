//
//  AlignedDetector.hpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 13.10.22.
//

#ifndef AlignedDetector_hpp
#define AlignedDetector_hpp

#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <limits>
#include <string>
#include <cmath>

class AlignedDetector {
public:
    AlignedDetector();
    AlignedDetector(std::size_t nDetectors, double g = 1.0);
    ~AlignedDetector();
    
    void setAlignmentParameters(const std::vector<std::array<double, 6>> &pars);
    void setGradientValues(const std::vector<std::array<double, 6>> &grads);
    void setGamma(double g) {gamma = g;}
    friend std::ifstream& operator>>(std::ifstream &inFile, AlignedDetector &ad);
    
    void getAlignmentParameters(std::vector<std::array<double, 6>> &pars) const;
    void getGradientValues(std::vector<std::array<double, 6>> &grads) const;
    double getGamma() const {return gamma;}
    friend std::ofstream& operator<<(std::ofstream &outFile, AlignedDetector &ad);
    
    void print() const;
    
    void makeGradStep();
    
private:
    std::vector<std::array<double, 6>> parameters;
    std::vector<std::array<double, 6>> gradients;
    double gamma;
};

#endif /* AlignedDetector_hpp */
