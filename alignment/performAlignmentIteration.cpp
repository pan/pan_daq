//
//  performAlignmentIteration.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 25.10.22.
//

#include "TString.h"

#include "AlignedDetector.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <array>
#include <limits>
#include <cmath>

constexpr float epsilon{0.5};

void sumGradients(std::vector<std::array<double, 6>> &gradients, const std::vector<std::array<double, 6>> &gradientsIter, std::size_t nDetectors) {
    for (std::size_t i{0}; i < nDetectors; ++i) {
        for (std::size_t j{0}; j < 6; ++j) (gradients.at(i))[j] += (gradientsIter.at(i))[j];
    }
}

unsigned int checkGradDir(const std::vector<std::array<double, 6>> &gradients, const std::vector<std::array<double, 6>> &gradientsPrev, std::size_t nDetectors) {
    
    unsigned int result{0};
    double scalarProduct{0.0};
    double norm{0.}, normPrev{0.};
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        for (std::size_t j{0}; j < 6; ++j) {
            scalarProduct += (gradients.at(i))[j] * (gradientsPrev.at(i))[j];
            norm += (gradients.at(i))[j] * (gradients.at(i))[j];
            normPrev += (gradientsPrev.at(i))[j] * (gradientsPrev.at(i))[j];
        }
    }
    
    if (normPrev <= std::numeric_limits<double>::min() && normPrev >= 0.) {
        result = 0;
        return result;
    }
    
    scalarProduct /= (std::sqrt(norm) * std::sqrt(normPrev));
    
    std::cout << "eps: " << epsilon << " scalarProduct: " << scalarProduct << std::endl;
    
    if (scalarProduct < epsilon) result = 1;
    if (scalarProduct < epsilon && scalarProduct > -epsilon) result = 2;
    
    return result;
}

bool checkDeltaZ(const std::vector<std::array<double, 6>> &parameters, std::size_t nDetectors) {
    bool result{false};
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        if (fabs((parameters.at(i))[2]) > 0.5) result = true;
    }
    
    return result;
}

int main(int argc, char** argv) {
    //argv[1] the number of files to read
    //argv[2] path for the files
    //argv[3] is a ROOT file name
    //argv[4] is the number of detectors
    
    if (argc != 5) {
        std::cerr << "Error: The number of arguments should be 4" << std::flush;
        return 1;
    }
    
    TString path(argv[2]), inputFileName(argv[3]);
    inputFileName = inputFileName(0, inputFileName.Length() - 5);
    inputFileName = inputFileName(15, inputFileName.Length());
    
    std::size_t nDetectors = std::atoi(argv[4]);
    
    AlignedDetector ad(nDetectors);
    
    std::ifstream globalInFile(path + "global_pars/parameters" + inputFileName + ".txt");
    std::size_t N1GoodEventPrev{0}, goodChi2Iter{0}, badChi2Iter{0}, iterAlign{0};
    float reducingFactor{0.5};
    std::size_t patience{10};
    
    if (globalInFile.is_open()) {
        globalInFile >> ad;
        globalInFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        globalInFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        globalInFile >> N1GoodEventPrev;
        globalInFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        globalInFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        globalInFile >> goodChi2Iter;
        globalInFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        globalInFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        globalInFile >> badChi2Iter;
        globalInFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        globalInFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        globalInFile >> iterAlign;
    }
    
    globalInFile.close();
    
    std::vector<std::array<double, 6>> parametersPrev;
    std::vector<std::array<double, 6>> gradientsPrev;
    std::array<double, 6> dummy;
    dummy.fill(0.);
    parametersPrev.assign(nDetectors, dummy);
    gradientsPrev.assign(nDetectors, dummy);
    
    std::vector<std::array<double, 6>> parametersIter;
    std::vector<std::array<double, 6>> gradientsIter;
    parametersIter.assign(nDetectors, dummy);
    gradientsIter.assign(nDetectors, dummy);
    
    std::vector<std::array<double, 6>> parameters;
    std::vector<std::array<double, 6>> gradients;
    parameters.assign(nDetectors, dummy);
    gradients.assign(nDetectors, dummy);
    
    ad.getAlignmentParameters(parametersPrev);
    ad.getGradientValues(gradientsPrev);
    
    AlignedDetector* adIter{nullptr};
    std::size_t N1GoodEventIter{0}, N1GoodEvent{0};
    double chi2sum{0.}, chi2sample{0.};
    TString pathToLocalFiles(path + "local_pars" + inputFileName);
    
    for (int i{0}; i < std::atoi(argv[1]); ++i) {
        
        std::ifstream inFile(Form("%s_%d_%d/parameters_%d.txt", pathToLocalFiles.Data(), (i / 500) * 500, ((i / 500) + 1) * 500, i));
        
        adIter = new AlignedDetector(nDetectors);
        
        inFile >> *adIter;
        
        adIter->getAlignmentParameters(parametersIter);
        adIter->getGradientValues(gradientsIter);
        
        inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        inFile >> N1GoodEventIter;
        inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        inFile >> chi2sample;
        
        sumGradients(gradients, gradientsIter, nDetectors);
        N1GoodEvent += N1GoodEventIter;
        chi2sum += chi2sample;
        
        inFile.close();
        
        delete adIter;
        adIter = nullptr;
        
    }
    
    if (N1GoodEvent > 1000) {
        
        unsigned int gradDirResult{0};
    
        for (std::size_t i{0}; i < nDetectors; ++i) {
            for (std::size_t j{0}; j < 6; ++j) (gradients.at(i))[j] /= N1GoodEvent;
        }
        
        ad.setGradientValues(gradients);
        ad.makeGradStep();
        ad.getAlignmentParameters(parameters);
        
        ad.print();
        
        gradDirResult = checkGradDir(gradients, gradientsPrev, nDetectors);
        
        if (gradDirResult > 0) {
            std::cout<< "Bad direction of the next gradient " << iterAlign << std::endl;
            std::cout<< "Current gamma: " << ad.getGamma() << std::endl;
            
            //*
            if (checkDeltaZ(parameters, nDetectors)) {
                iterAlign--;
                badChi2Iter = 0;
                
                ad.setGradientValues(gradientsPrev);
                ad.setAlignmentParameters(parametersPrev);
            }
            //*/
            
            goodChi2Iter = 0;
            badChi2Iter++;
            ad.setGamma(ad.getGamma() * reducingFactor);
            if(badChi2Iter > patience) {
                std::cout << "Ran out of patience" << std::endl;
                if (gradDirResult == 2) ad.setGamma(ad.getGamma() / pow(reducingFactor, patience + 2));
                else ad.setGamma(ad.getGamma() / pow(reducingFactor, patience + 1));
                std::cout<< "New gamma: " << ad.getGamma() << std::endl;
                badChi2Iter = 0;
            }
        }
        else {
            std::cout<< "Good direction of the next gradient " << iterAlign << std::endl;
            std::cout<< "gamma: " << ad.getGamma() << std::endl;
            
            badChi2Iter = 0;
            goodChi2Iter++;
            if(goodChi2Iter > 5 * patience) {
                std::cout << "Ran out of patience" << std::endl;
                ad.setGamma(ad.getGamma() / reducingFactor);
                std::cout<< "New gamma: " << ad.getGamma() << std::endl;
                goodChi2Iter = 0;
            }
        }
        
        iterAlign++;
    
    }
    else {
        ad.setGradientValues(gradientsPrev);
        ad.setAlignmentParameters(parametersPrev);
        
        goodChi2Iter = 0;
        badChi2Iter++;
        
        ad.setGamma(ad.getGamma() * reducingFactor);
        if(badChi2Iter > patience) {
            std::cout << "Ran out of patience" << std::endl;
            ad.setGamma(ad.getGamma() / pow(reducingFactor, patience + 1));
            std::cout<< "New gamma: " << ad.getGamma() << std::endl;
            badChi2Iter = 0;
        }
    }
    
    std::ofstream outFile(path + "global_pars/parameters" + inputFileName + ".txt");
    
    outFile << ad;
    outFile << "N1GoodEvent:\n";
    if (N1GoodEvent <= 1000 || checkDeltaZ(parameters, nDetectors)) {
        outFile << N1GoodEventPrev << '\n';
        chi2sum /= N1GoodEventPrev;
    }
    else {
        outFile << N1GoodEvent << '\n';
        chi2sum /= N1GoodEvent;
    }
    
    std::cout << "chi2sum: " << chi2sum << std::endl;
    
    outFile << "goodChi2Iter:\n";
    outFile << goodChi2Iter << '\n';
    outFile << "badChi2Iter:\n";
    outFile << badChi2Iter << '\n';
    outFile << "iterAlign:\n";
    outFile << iterAlign << '\n';
    outFile << "chi2sum:\n";
    outFile << chi2sum << '\n';
    
    outFile.close();
    
    std::ofstream outIndicator(path + "indicators/parameters_indicator" + inputFileName + ".txt");
    
    outIndicator << "dummy";
    
    outIndicator.close();
    
    return 0;
}
