//
//  generateTrackingDataPixel.cpp
//  
//
//  Created by Daniil Sukhonos on 15.06.23.
//

#include <iostream>
#include <string>
#include <cmath>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TVectorD.h"
#include "TFitResult.h"
#include "TGeoManager.h"
#include "TGeoPhysicalNode.h"
#include "TRandom2.h"
#include "TStyle.h"
#include "TMath.h"
#include "TApplication.h"

constexpr std::size_t nDetectors{8};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double conversionCoefPixel{0.0055}; // cm / ch
constexpr double stripLength{5.12}; // cm
bool enableVerbose{false};
bool enableMCS{true};
bool enableMisalignment{false};
bool threeAngles{false};
bool sevenAngles{false};

constexpr double p = 15000.; // MeV/c 180000
constexpr double mass = 938.27; // MeV/c^2 for pi+- 139.57 for p 938.27

std::vector<double> detectorPos, newDetectorPos;
TRandom2 r(42);

double MCSSigma(std::size_t i, const std::vector<double> &detectorPosLocal, double angle) {
    
    if (i == 0) return 0.;
    double gammaFactor = sqrt(1 + (p / mass) * (p / mass));
    double velocity = p / (mass * gammaFactor);
    double xSi = 9.37; // cm for silicon
    double s = 0.0150; // cm thickness of each tracking plane
    double s_pixel = 0.1; // cm thickness of pixel detector 300 um silicon + 700 um timepix3 asic
    double gammaAngle{TMath::Pi() / 2.};
    double phiAngle{TMath::Pi() / 2.};
    double relThickness = s / xSi;
    double theta0{0.};
    double sigma = 0.0;
    for (std::size_t j{0}; j < i; ++j) {
        gammaAngle = TMath::Pi() / 2.;
        phiAngle = gammaAngle - angle;
        s *= 1. / fabs(sin(phiAngle));
        relThickness = s / xSi;
        if (j == 0) {
            s_pixel *= 1. / fabs(sin(phiAngle));
            relThickness = s_pixel / xSi;
        }
        theta0 = (13.6 / (p * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
        sigma += (detectorPosLocal.at(i) - detectorPosLocal.at(j)) * (detectorPosLocal.at(i) - detectorPosLocal.at(j)) * theta0 * theta0;
        s = 0.0150;
        s_pixel = 0.08;
    }
    gammaAngle = TMath::Pi() / 2.;
    phiAngle = gammaAngle - angle;
    s *= 1. / fabs(sin(phiAngle));
    relThickness = s / xSi;
    theta0 = (13.6 / (p * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
    if (i > 0) sigma += (detectorPosLocal.at(i) + 6.7) * (detectorPosLocal.at(i) + 6.7) * theta0 * theta0; // 1st StripY layer
    //if (i > 3) sigma += (detectorPosLocal.at(i) + 0.006) * (detectorPosLocal.at(i) + 0.006) * theta0 * theta0; // middle StripY layer
    if (i > 6) sigma += (detectorPosLocal.at(i) - 6.7) * (detectorPosLocal.at(i) - 6.7) * theta0 * theta0; // last StripY layer
    std::cout << "MCSSigma, Layer " << i << " equal " << sqrt(sigma) << std::endl;
    return sqrt(sigma);
}

TVectorD removeElement(std::size_t i, TVectorD * vecOrig) {
    TVectorD outVec, tempVecFirst(*vecOrig), tempVecSecond(*vecOrig);
    
    tempVecFirst.ResizeTo(vecOrig->GetLwb(), i - 1);
    tempVecSecond.ResizeTo(i + 1, vecOrig->GetUpb());
    outVec.ResizeTo(vecOrig->GetNrows() - 1);
    outVec.SetSub(0, tempVecFirst);
    outVec.SetSub(tempVecFirst.GetNrows(), tempVecSecond);
    
    return outVec;
}

void evaluateUnbiasedResiduals(std::vector<double> &yErrors, TVectorD * yCoord, TVectorD * yErr, TVectorD * zCoord, TVectorD * zErr, TF1 * fitFunc) {
    
    TGraphErrors *gr{nullptr};
    TVectorD yCoordMod, yErrMod, zCoordMod, zErrMod;
    yErrors.clear();
    yErrors.shrink_to_fit();
    
    yCoordMod.ResizeTo(nDetectors - 1);
    yErrMod.ResizeTo(nDetectors - 1);
    zCoordMod.ResizeTo(nDetectors - 1);
    zErrMod.ResizeTo(nDetectors - 1);
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        
        yCoordMod = removeElement(i, yCoord);
        yErrMod = removeElement(i, yErr);
        zCoordMod = removeElement(i, zCoord);
        zErrMod = removeElement(i, zErr);
        
        fitFunc->SetRange(zCoordMod[yCoordMod.GetLwb()] * 1.05, zCoordMod[yCoordMod.GetUpb()] * 1.05);
        
        gr = new TGraphErrors(zCoordMod, yCoordMod, zErrMod, yErrMod);
        fitFunc->SetParameter(0, (yCoordMod.Max() + yCoordMod.Min()) / 2.);
        fitFunc->SetParameter(1, (yCoordMod[yCoordMod.GetUpb()] - yCoordMod[yCoordMod.GetLwb()]) / (zCoordMod[yCoordMod.GetUpb()] - zCoordMod[yCoordMod.GetLwb()]));
        gr->Fit(fitFunc,"QRN EX0"); // default: "QRNEMC"
        
        yErrors.push_back(fitFunc->Eval((*zCoord)(i)) - (*yCoord)(i));
        
        delete gr;
        gr = nullptr;
    }
    
    fitFunc->SetRange(detectorPos.front() * 1.05, detectorPos.back() * 1.05);
}

int main(int argc, char* argv[]) {
    
    if(argc != 2) {
        std::cout << "To use the script. Do: " << std::endl;
        std::cout << "./GenerateTrackingDataPixel <angle-in-deg>" << std::endl;
        return 1;
    }
    
    TApplication app("app", &argc, argv);
    TString path("/Users/dasukhon/Documents/Documents_newMac/pan_daq/fitted_rootfiles/");
    
    double tracks_angle{TMath::Pi() * std::stod(argv[1]) / 180.};
    double tracks_angle_basic{tracks_angle};
    double track_intercept{0.};
    
    TString inputFileName(Form("generated_clusters_protons_15GeV_%u_deg", static_cast<unsigned int>(fabs(std::stod(argv[1])))));
    
    detectorPos = {-7.8825, -6.645, -5.83, -0.395, 0.4050, 5.83, 6.645, 7.8825}; // april 2023
    
    TF1 *fitFunc = new TF1("fitFunc","pol1(0)", detectorPos.front() * 1.05, detectorPos.back() * 1.05);
    std::vector<double> yErrors;
    TVectorD *yCoord, *zCoord, *yErr, *zErr;
    yErr = new TVectorD(nDetectors);
    zErr = new TVectorD(nDetectors);
    yCoord = new TVectorD(nDetectors);
    zCoord = new TVectorD(nDetectors, &detectorPos[0]);
    TGraphErrors *gr{nullptr};
    
    gStyle->SetOptFit(1);
    TCanvas** c_arr = new TCanvas*[nDetectors];
    TH1D **hy_arr = new TH1D*[nDetectors];
    double* std_dev = new double[nDetectors];
    double* std_dev_err = new double[nDetectors];
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        if (i == 0 || i == nDetectors - 1) (*yErr)[i] = conversionCoefPixel / sqrt(12.);
        else (*yErr)[i] = conversionCoef / sqrt(12.);
        (*zErr)[i] = 0.;
        c_arr[i] = new TCanvas(Form("c%lu", i), Form("Residuals, Layer %lu", i), 0, 0, 1280, 800);
        hy_arr[i] = new TH1D(Form("hy%lu", i), "", 500, -1.e-2, 1.e-2);
        hy_arr[i]->GetXaxis()->SetTitle("cm");
        hy_arr[i]->SetTitle(Form("Unbiased Residuals, Layer %lu", i));
    }
    
    for (std::size_t event{0}; event < 100000; ++event) {
        
        track_intercept = r.Gaus(0.,1.);
        
        for (std::size_t i{0}; i < nDetectors; ++i) {
            (*yCoord)[i] = detectorPos.at(i) * tan(tracks_angle) + track_intercept;
            if (i > 0) (*yCoord)[i] += r.Gaus(0., MCSSigma(i, detectorPos, tracks_angle) * (cos(tracks_angle) + sin(tracks_angle) * tan(tracks_angle))); // assuming theta0/2 is very small
        }
        
        delete fitFunc;
        fitFunc = nullptr;
        fitFunc = new TF1("fitFunc","pol1(0)", detectorPos.front() * 1.05, detectorPos.back() * 1.05);
        fitFunc->SetParameter(0, ((*yCoord)[0] * detectorPos.back() - (*yCoord)[nDetectors - 1] * detectorPos.front()) / (detectorPos.back() - detectorPos.front()));
        fitFunc->SetParameter(1, ((*yCoord)[nDetectors - 1] - (*yCoord)[0]) / (detectorPos.back() - detectorPos.front()));
        
        delete gr;
        gr = nullptr;
        gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
        gr->Fit(fitFunc, "QRN EX0");
        
        evaluateUnbiasedResiduals(yErrors, yCoord, yErr, zCoord, zErr, fitFunc);
        
        for (std::size_t i{0}; i < nDetectors; ++i) {
            hy_arr[i]->Fill(yErrors.at(i));
        }
        
    }
    
    unsigned int nEntries{0};
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        c_arr[i]->cd();
        hy_arr[i]->Draw();
        nEntries = hy_arr[i]->GetEntries();
        gPad->SetLogy();
        std_dev[i] = hy_arr[i]->GetStdDev();
        std_dev_err[i] = hy_arr[i]->GetStdDevError();
        c_arr[i]->Modified();
        c_arr[i]->Update();
        c_arr[i]->SaveAs(path + "../plots/" + Form("resid_layer%lu", i) + inputFileName + ".pdf");
    }
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        std::cout << "Layer " << i << ", std dev = " << std_dev[i] * 1.e+04 << " +- " << std_dev_err[i] * 1.e+04 << " um" << std::endl;
    }
    
    app.Run();
    
    delete [] c_arr;
    delete [] std_dev;
    delete [] std_dev_err;
    
    return 0;
    
}
