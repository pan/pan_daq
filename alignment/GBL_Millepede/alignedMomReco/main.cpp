//
//  main.cpp
//  alignedMomReco
//
//  Created by Daniil Sukhonos on 02.11.23.
//

#include <boost/program_options.hpp>
#include <unordered_map>

#include "GFGbl.h"
#include "FieldManager.h"
#include "ConstField.h"
#include "MaterialEffects.h"
#include "TGeoMaterialInterface.h"
#include "EventDisplay.h"
#include "GblFitterInfo.h"
#include "PlanarMeasurement.h"
#include "RectangularFinitePlane.h"
#include "SharedPlanePtr.h"
#include "KalmanFitterInfo.h"
#include "KalmanFitterRefTrack.h"

#include "TGeoManager.h"
#include "TDatabasePDG.h"
#include "TGraphErrors.h"
#include "TRandom3.h"
#include "TH1D.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TObjString.h"

#include "miniPanMagField.hpp"

// #define SHOW_TRACKS

namespace po = boost::program_options;

constexpr std::size_t NLAD{2};
constexpr std::size_t NBOARD{3};
constexpr std::size_t NBRANCH{6};
constexpr std::size_t NBRANCHPIX{3};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double conversionCoefPixel{0.0055}; // cm / ch
constexpr float detectorResolution{0.0025}; // resolution of scd detectors in cm
constexpr float stripLength{5.12}; // stripx strip length in cm
float pTruth{10.0}; // beamtest energy 10 GeV/c pi-
float kinEnergyTruth{0.2};
constexpr bool nov2021BT{false};
constexpr bool june2022BT{false};
constexpr bool sep2022BT{false};
constexpr bool nov2022BT{false};
constexpr bool apr2023BT{true};
constexpr bool summer2023{false};

std::vector<double> detectorPos;
std::vector<double> detectorPosErr = {0.01, 0.01, 0.01, 0.01, 0.01, 0.01};
std::array<std::vector<double>, NLAD * NBOARD> deltas;
// genfit::AbsFinitePlane* finiteStripX = new genfit::StripXFinitePlane(2.55, 2.56);
std::unique_ptr<genfit::AbsFinitePlane> finiteStripX = std::make_unique<genfit::RectangularFinitePlane>(-2.57, 2.57, -2.57, 2.57);
std::unique_ptr<genfit::AbsFinitePlane> finitePixel = std::make_unique<genfit::RectangularFinitePlane>(-1.415, 1.415, -1.415, 1.415);
std::vector<double> detectorPosPixel = {-7.8695, 7.8845};

double mass{0.0};
TRandom3 r{42};

double MCSSigma(std::size_t i, const TVectorD &detectorPosLocal, const std::vector<double> &slopes, double angle) {
    
    if (i == 0) return 0.;
    double gammaFactor = sqrt(1 + (fabs(pTruth) / mass) * (fabs(pTruth) / mass));
    double velocity = fabs(pTruth) / (mass * gammaFactor);
    double xSi = 9.37; // cm for silicon
    double s = 0.0150; // cm thickness of each tracking plane
    double gammaAngle{TMath::Pi() / 2.};
    double phiAngle{TMath::Pi() / 2.};
    double relThickness = s / xSi;
    double theta0{0.};
    double sigma{0.};
    for (std::size_t j{0}; j < i; ++j) {
        if (fabs(slopes.at(i)) > 1.e-12) gammaAngle = atan(slopes.at(i));
        else gammaAngle = TMath::Pi() / 2.;
        phiAngle = gammaAngle - angle;
        s *= 1. / fabs(sin(phiAngle));
        relThickness = s / xSi;
        theta0 = (13.6 / (fabs(pTruth) * 1.e+03 * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
        sigma += (detectorPosLocal[i] - detectorPosLocal[j]) * (detectorPosLocal[i] - detectorPosLocal[j]) * theta0 * theta0;
        s = 0.0150;
    }
    gammaAngle = TMath::Pi() / 2.;
    phiAngle = gammaAngle - angle;
    s *= 1. / fabs(sin(phiAngle));
    relThickness = s / xSi;
    theta0 = (13.6 / (fabs(pTruth) * 1.e+03 * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
    // if (i > 2) sigma += (detectorPosLocal[i] + 0.006) * (detectorPosLocal[i] + 0.006) * theta0 * theta0; // middle StripY layer
    std::cout << "MCSSigma, Layer " << i << " equal " << sqrt(sigma) << std::endl;
    return sqrt(sigma);
}

void correctStripPositions(const std::unordered_map<std::size_t, std::vector<double>*> &vec, std::unordered_map<std::size_t, std::vector<double>*> &vecAligned, bool generatedDataFlag, std::size_t j) {
    std::size_t key{0};
    for (std::size_t i{0}; i < vecAligned.size(); ++i) {
        key = 10 * i + 12;
        if (generatedDataFlag) vecAligned.at(key)->push_back(vec.at(key)->at(j));
        else {
            if (vec.at(key)->at(j) >= 0 and vec.at(key)->at(j) < 256) {
                vecAligned.at(key)->push_back(vec.at(key)->at(j) - 1);
                if (vec.at(key)->at(j) < (0 + 64)) vecAligned.at(key)->back() -= 1;
                else if (vec.at(key)->at(j) < (0 + 128)) vecAligned.at(key)->back() -= 0;
                else if (vec.at(key)->at(j) < (0 + 192)) vecAligned.at(key)->back() += 1;
                else if (vec.at(key)->at(j) < (0 + 256)) vecAligned.at(key)->back() += 2;
            }
            else if (vec.at(key)->at(j) >= 256 and vec.at(key)->at(j) < 512) {
                vecAligned.at(key)->push_back(vec.at(key)->at(j) + 1);
                if (vec.at(key)->at(j) < (256 + 64)) vecAligned.at(key)->back() -= 2;
                else if (vec.at(key)->at(j) < (256 + 128)) vecAligned.at(key)->back() -= 1;
                else if (vec.at(key)->at(j) < (256 + 192)) vecAligned.at(key)->back() += 0;
                else if (vec.at(key)->at(j) < (256 + 256)) vecAligned.at(key)->back() += 1;
            }
            else if (vec.at(key)->at(j) >= 512 and vec.at(key)->at(j) < 768) {
                vecAligned.at(key)->push_back(vec.at(key)->at(j) - 1);
                if (vec.at(key)->at(j) < (512 + 64)) vecAligned.at(key)->back() -= 1;
                else if (vec.at(key)->at(j) < (512 + 128)) vecAligned.at(key)->back() -= 0;
                else if (vec.at(key)->at(j) < (512 + 192)) vecAligned.at(key)->back() += 1;
                else if (vec.at(key)->at(j) < (512 + 256)) vecAligned.at(key)->back() += 2;
            }
            else if (vec.at(key)->at(j) >= 768 and vec.at(key)->at(j) < 1024) {
                vecAligned.at(key)->push_back(vec.at(key)->at(j) + 1);
                if (vec.at(key)->at(j) < (768 + 64)) vecAligned.at(key)->back() -= 2;
                else if (vec.at(key)->at(j) < (768 + 128)) vecAligned.at(key)->back() -= 1;
                else if (vec.at(key)->at(j) < (768 + 192)) vecAligned.at(key)->back() += 0;
                else if (vec.at(key)->at(j) < (768 + 256)) vecAligned.at(key)->back() += 1;
            }
            else if (vec.at(key)->at(j) >= 1024 and vec.at(key)->at(j) < 1280) {
                vecAligned.at(key)->push_back(vec.at(key)->at(j) - 1);
                if (vec.at(key)->at(j) < (1024 + 64)) vecAligned.at(key)->back() -= 1;
                else if (vec.at(key)->at(j) < (1024 + 128)) vecAligned.at(key)->back() -= 0;
                else if (vec.at(key)->at(j) < (1024 + 192)) vecAligned.at(key)->back() += 1;
                else if (vec.at(key)->at(j) < (1024 + 256)) vecAligned.at(key)->back() += 2;
            }
            else if (vec.at(key)->at(j) >= 1280 and vec.at(key)->at(j) < 1536) {
                vecAligned.at(key)->push_back(vec.at(key)->at(j) + 1);
                if (vec.at(key)->at(j) < (1280 + 64)) vecAligned.at(key)->back() -= 2;
                else if (vec.at(key)->at(j) < (1280 + 128)) vecAligned.at(key)->back() -= 1;
                else if (vec.at(key)->at(j) < (1280 + 192)) vecAligned.at(key)->back() += 0;
                else if (vec.at(key)->at(j) < (1280 + 256)) vecAligned.at(key)->back() += 1;
            }
            else if (vec.at(key)->at(j) >= 1536 and vec.at(key)->at(j) < 1792) {
                vecAligned.at(key)->push_back(vec.at(key)->at(j) - 1);
                if (vec.at(key)->at(j) < (1536 + 64)) vecAligned.at(key)->back() -= 1;
                else if (vec.at(key)->at(j) < (1536 + 128)) vecAligned.at(key)->back() -= 0;
                else if (vec.at(key)->at(j) < (1536 + 192)) vecAligned.at(key)->back() += 1;
                else if (vec.at(key)->at(j) < (1536 + 256)) vecAligned.at(key)->back() += 2;
            }
            else if (vec.at(key)->at(j) >= 1792 and vec.at(key)->at(j) < 2048) {
                vecAligned.at(key)->push_back(vec.at(key)->at(j) + 1);
                if (vec.at(key)->at(j) < (1792 + 64)) vecAligned.at(key)->back() -= 2;
                else if (vec.at(key)->at(j) < (1792 + 128)) vecAligned.at(key)->back() -= 1;
                else if (vec.at(key)->at(j) < (1792 + 192)) vecAligned.at(key)->back() += 0;
                else if (vec.at(key)->at(j) < (1792 + 256)) vecAligned.at(key)->back() += 1;
            }
        }
    }
}

void updateVcogs(const std::unordered_map<std::size_t, std::vector<double>*> &vec, std::unordered_map<std::size_t, std::vector<double>*> &vecAligned, std::size_t j, bool enablePixels = false) {
    std::size_t key{0};
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        key = 10 * i + 12;
        if (i % 2 == 0 && !enablePixels) vecAligned[key]->at(j) = 2047.0 - vec.at(key)->at(j);
        else vecAligned[key]->at(j) = vec.at(key)->at(j);
    }
}

std::vector<double> convertErrors(const std::vector<double> &meas_errors, const std::vector<int> &meas_labels, bool yFlag = false) {
    std::vector<double> errors;
    for (int i{0}; i < meas_errors.size(); ++i) {
        if (yFlag) {
            if (meas_labels.at(i) % 2 == 0) {
                errors.push_back(meas_errors.at(i));
//                errors.push_back(conversionCoef * conversionCoef / 12.);
            }
        }
        else {
            if (meas_labels.at(i) % 2 > 0) {
                errors.push_back(meas_errors.at(i));
            }
        }
    }
    return errors;
}

std::vector<double> applyAlignmentCorrections(const std::unordered_map<std::size_t, std::vector<double>*> &vecAligned, TVectorD &zCoord, const std::vector<int> &meas_labels, const std::vector<std::vector<double>> &vglder, const std::vector<std::vector<unsigned int>> &vparamlabels, TObjString* parametersString, TObjString* prefitResidualsString, bool yFlag = false) {
    
    std::istringstream iss(parametersString->GetString().Data());
    std::string line;
    std::unordered_map<int, std::pair<double, double>> data; // To store the parsed data

    while (std::getline(iss, line)) {
        int key;
        double value1, value2;
        if (std::istringstream(line) >> key >> value1 >> value2) {
            data[key] = std::make_pair(value1, value2);
        }
    }
    
    std::istringstream iss2(prefitResidualsString->GetString().Data());
    std::unordered_map<int, double> prefitResiduals;
    
    while (std::getline(iss2, line)) {
        int key;
        double value;
        if (std::istringstream(line) >> key >> value) {
            prefitResiduals[key] = value;
        }
    }
    
    double correction{0.0};
    double correctionZ{0.0};
    
    int k{0};
    std::vector<double> aligned_cogs;
    std::vector<double> zeroSlopes;
    zeroSlopes.assign(vecAligned.size(), 0);
    
    double random_shift{(r.Rndm(2) - 1.0)};
    
    for (int i{0}; i < meas_labels.size(); ++i) {
        if (yFlag) {
            if (vecAligned.count(meas_labels.at(i)) > 0) {
                if (summer2023) {
                    aligned_cogs.push_back((2047.0 - vecAligned.at(meas_labels.at(i))->front() - 1023.5) * conversionCoef);
                }
                else aligned_cogs.push_back((vecAligned.at(meas_labels.at(i))->front() - 1023.5) * conversionCoef);
                aligned_cogs.back() -= prefitResiduals.at(meas_labels.at(i));
                
                for (int j{0}; j < vglder.at(i).size(); ++j) {
                    if (data.count(vparamlabels.at(i).at(j)) > 0 && vparamlabels.at(i).at(j) > 10) {
                        if (vparamlabels.at(i).at(j) % 10 == 2) correction += data[vparamlabels.at(i).at(j)].first;
                        else if (vparamlabels.at(i).at(j) % 10 == 3) correctionZ += data[vparamlabels.at(i).at(j)].first;
                        else if (vparamlabels.at(i).at(j) % 10 == 4) correctionZ -= aligned_cogs.back() * data[vparamlabels.at(i).at(j)].first;
                    }
                }
                
                aligned_cogs.back() += correction;
                zCoord[k] += correctionZ;
                correction = 0.0;
                correctionZ = 0.0;
                k++;
            }
        }
        else {
            if (meas_labels.at(i) % 2 > 0) {
                aligned_cogs.push_back(0.0);
                for (int j{0}; j < vglder.at(i).size(); ++j) {
                    if (data.count(vparamlabels.at(i).at(j)) > 0 && vparamlabels.at(i).at(j) > 10) {
                        if (vparamlabels.at(i).at(j) % 10 == 2) correction += data[vparamlabels.at(i).at(j)].first;
                    }
                }
                aligned_cogs.back() += correction;
                correction = 0.0;
            }
        }
    }
    return aligned_cogs;
}

bool performFitting(const TVectorD &zCoord, const TVectorD &yCoord, const TVectorD &zErr, const TVectorD &yErr, const TVectorD &xCoord, const TVectorD &xErr, genfit::Track* fitTrack, std::shared_ptr<genfit::KalmanFitterRefTrack> fitter) {
    
    int detId{0}; // detector ID
    int planeId{0};
    int hitId{0}; // hit ID
    
    genfit::PlanarMeasurement* measurement{nullptr};
    //*
    TMatrixDSym hitCov(2);
    hitCov.UnitMatrix();
    hitCov(1,1) = detectorResolution*detectorResolution / 12; // include misalignment factor?
    hitCov(0,0) = stripLength * stripLength / 12; // 12 - classic approach

    TVectorD hitCoords(2);
    hitCoords[0] = 0.;
    hitCoords[1] = 0.;
    //*/
    /*
    TMatrixDSym hitCov(1);
    hitCov.UnitMatrix();
    hitCov *= detectorResolution*detectorResolution / 12;

    TVectorD hitCoords(1);
    hitCoords[0] = 0;
    //*/
    TVector3 detPlaneN, detPlaneU, detPlaneV, detPlane0;
    genfit::AbsTrackRep* cardRep{nullptr};
    genfit::FitStatus* fitStatus{nullptr};
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        detPlaneN.SetXYZ(0,0,1);
        detPlaneN.SetMag(1);
        detPlane0.SetXYZ(0, 0, zCoord[i]);
        hitCoords[0] = xCoord[i];
        hitCoords[1] = yCoord[i];
        hitCov(1,1) = yErr[i] * yErr[i];
        hitCov(0,0) = xErr[i] * xErr[i];
        // hitCov(1,1) = yErr[i] * yErr[i] + detectorResolution * detectorResolution * 16 / 25; // 16 / 25 default; 5 / 25 for 1 GeV/c
        detPlaneU = detPlaneN.Orthogonal();
        detPlaneU.SetMag(1);
        detPlaneV = detPlaneN.Cross(detPlaneU);
        detPlaneV.SetMag(1);
//            if (useAlignedGeom) {
//                detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
//            }
//            else {
//                detPlaneU.SetXYZ(fabs(detPlaneU.X()), fabs(detPlaneU.Y()), fabs(detPlaneU.Z()));
//                detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
//            }
        detPlaneU.SetXYZ(fabs(detPlaneU.X()), fabs(detPlaneU.Y()), fabs(detPlaneU.Z()));
        detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
        measurement = new genfit::PlanarMeasurement(hitCoords, hitCov, ++detId, hitId, nullptr);
//            if (i == 0 || i == NLAD * (NBOARD + 1) - 1) {
//                measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finitePixel->clone())), ++planeId);
//            }
//            else {
//                measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finiteStripX->clone())), ++planeId);
//            }
//            if (!useAlignedGeom) measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finiteStripX->clone())), ++planeId);
//            else measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneV, detPlaneU, finiteStripX->clone())), ++planeId);
        measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finiteStripX->clone())), ++planeId);
        // measurement->setStripV();
        fitTrack->insertPoint(new genfit::TrackPoint(measurement->clone(), fitTrack));
        delete measurement;
        measurement = nullptr;
    }
    
    if (fitTrack->getNumPoints() == 0) return false;
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track before fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    // do the fit
    try {
        fitter->processTrack(fitTrack);
    }
    catch (genfit::Exception& e) {
        std::cout << "genfit failed to fit track" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track after fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->determineCardinalRep();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with cardinal rep determination after fit" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->getFittedState();
        // print fit result
        // (fitTrack->getFittedState()).Print();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with fittedstate" << std:: endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    cardRep = fitTrack->getCardinalRep();
    
    try {
        fitStatus = fitTrack->getFitStatus(cardRep);
        std::cout << "N Failed Points: " << fitStatus->getNFailedPoints() << std::endl;
        if (!fitStatus->isFitted()) return false;
        auto nmeas = fitStatus->getNdf();
        if (nmeas < 1) return false;
        auto chi2Value  = fitStatus->getChi2()/nmeas;
        std::cout << "N measurements: " << nmeas << std::endl;
        std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
        if (chi2Value < 0) return false;
        if (!fitStatus->isFitConverged()) {
            try {
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->determineCardinalRep();
                fitTrack->getFittedState();
                cardRep = fitTrack->getCardinalRep();
                fitStatus = fitTrack->getFitStatus(cardRep);
                nmeas = fitStatus->getNdf();
                if (nmeas < 1) return false;
                chi2Value  = fitStatus->getChi2()/nmeas;
                std::cout << "N measurements: " << nmeas << std::endl;
                std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
                if (chi2Value < 0) return false;
            } catch (genfit::Exception& e) {
                std::cerr << "Exception" << std::endl;
                std::cerr << e.what();
                throw e;
            }
        }
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with Fit Status" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    return true;
}


int main(int argc, char * argv[]) {
    
    std::string inputFile, inputDerFile, outputFile, geomFile, pathToFieldMap;
    bool beamTestData, useFieldMap, enablePixels, enableSecondRep, enableThirdRep;
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "Produce help message")
        ("input-file,i", po::value<std::string>(&inputFile)->required(), "Input file name")
        ("input-der-file,d", po::value<std::string>(&inputDerFile)->required(), "Input file with derivatives name")
        ("output-file,o", po::value<std::string>(&outputFile)->required(), "Output file name")
        ("geom-file,g", po::value<std::string>(&geomFile)->required(), "ROOT geometry file name")
        ("beamtest,b", po::value<bool>(&beamTestData)->default_value(true), "An input file is beamtest data")
        ("field-map,f", po::value<bool>(&useFieldMap)->default_value(true), "Use a simulated field map instead of a const field value")
        ("path-to-map,m", po::value<std::string>(&pathToFieldMap)->required(), "Path to field map files")
        ("enable-second-rep,r", po::bool_switch(&enableSecondRep)->default_value(false), "Add second particle representation for better fitting")
        ("enable-third-rep,t", po::bool_switch(&enableThirdRep)->default_value(false), "Add third particle representation for better fitting")
        ("enable-pixels,p", po::bool_switch(&enablePixels)->default_value(false), "An input file contains pixels data as well");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    try {
        po::notify(vm);
    }
    catch (const po::required_option& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        std::cerr << desc << std::endl;
        return 1;
    }
    
//    TApplication* myApp = new TApplication("myApp", &argc, argv);
    
    std::vector<double>* resid_intermediate{nullptr};
    std::vector<double>* meas_errors{nullptr};
    std::vector<int>* meas_labels{nullptr};
    std::vector<std::vector<double>>* vglder{nullptr};
    std::vector<std::vector<unsigned int>>* vparamlabels{nullptr};
    int milleEvent;
    std::unordered_map<std::size_t, std::vector<double>*> vcogs, vcogsAligned, vquadX, vquadY;
    std::unordered_map<std::size_t, std::vector<int>*> vpixelSizes;
    TObjString* parametersString;
    TObjString* prefitResidualsString;
    
    if (nov2021BT) detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81 cm // November 2021 beamtest
    // if (june2022BT) detectorPos = {-7.006, -6.206, -0.470, 0.330, 5.870, 6.670}; // old
    else if (june2022BT) detectorPos = {-7.006, -6.191, -0.470, 0.330, 5.870, 6.685}; // 0, 8.0, 65.36, 73.36, 128.76, 136.76 mm (sens. surf) // June/August 2022 beamtest
    else if (sep2022BT) {
        detectorPosPixel = {-7.8985, 7.8985};
        detectorPos = {-6.699, -5.884, -0.316, 0.484, 5.884, 6.699};
    }
    else if (nov2022BT) {
        detectorPosPixel = {-7.807, 7.807};
        detectorPos = {-6.6075, -5.7925, -0.4075, 0.3925, 5.7925, 6.6075};
    }
    else if (apr2023BT || summer2023) {
        detectorPosPixel = {-7.8825, 7.8825};
        detectorPos = {-6.645, -5.83, -0.395, 0.4050, 5.83, 6.645};
    }
    
    double edge_widthX_quad1{2.}, edge_widthY_quad1{2.}, edge_widthX_quad2{2.}, edge_widthY_quad2{2.};

    int pdg;
    if (!beamTestData) pdg = 13; // particle pdg code for cosmics (mu-)
    else {
        pdg = -211; // pi- during beamtest
        if (june2022BT) {
            pdg = 11; // e-
            pTruth = 0.5; // 1 GeV/c, 0.5 GeV/c
            if (enablePixels) {
                pdg = 2212; // proton
                pTruth = 180; // 180 GeV/c
            }
        }
        else if (sep2022BT) {
            pdg = 11; // e- is 11, protons 2212
            pTruth = 1; // GeV/c
        }
        else if (nov2022BT) {
            pdg = (TDatabasePDG::Instance())->GetParticle("alpha")->PdgCode();
            pTruth = 150; // 150 GeV/c
        }
        else if (apr2023BT) {
            pdg = -11; // pi+ 211, proton 2212, positron -11
            pTruth = 0.25; // GeV/c
        }
        else if (summer2023) {
            pdg = 2212;
            mass = (TDatabasePDG::Instance())->GetParticle(pdg)->Mass();
            pTruth = kinEnergyTruth * sqrt(1. + (2. * mass / kinEnergyTruth));
        }
    }

    mass = (TDatabasePDG::Instance())->GetParticle(pdg)->Mass();
    
    kinEnergyTruth = mass * (sqrt(1. + (pTruth / mass) * (pTruth / mass)) - 1.);
    
    std::shared_ptr<genfit::KalmanFitterRefTrack> fitter{nullptr};
    fitter = std::make_shared<genfit::KalmanFitterRefTrack>();
    
    TGeoManager* geoMan = new TGeoManager("Geometry", "miniPAN geometry");
    geoMan->Import(geomFile.c_str());

    if (!useFieldMap) {
        // genfit::FieldManager::getInstance()->init(new genfit::ConstField(0, 0, 0)); // in kGauss 1 T = 10 kG here we set the mag field of the Earth
        genfit::FieldManager::getInstance()->init(new genfit::ConstField(-4.0, 0., 0.));
    } else {
        if (nov2021BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("30mm", pathToFieldMap + "2-magnets-30mm-N48.txt"));
        else if (june2022BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("june2022", pathToFieldMap + "magnet_map_june_august_2022.txt"));
        else if (sep2022BT || nov2022BT || apr2023BT || summer2023) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("latest", pathToFieldMap + "magnet_map_sep_2022_onwards.txt"));
        // genfit::FieldManager::getInstance()->useCache(true, 8);
    }
    
    genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
        
    // const double charge{TDatabasePDG::Instance()->GetParticle(pdg)->Charge()/(3.)};

    #ifdef SHOW_TRACKS
        std::unique_ptr<genfit::EventDisplay> display(genfit::EventDisplay::getInstance());
        display->reset();
    #endif
    
    std::unique_ptr<TGraphErrors> gr{nullptr};
        
    std::unique_ptr<TFile> inFile(TFile::Open(inputFile.c_str(), "read"));
    std::unique_ptr<TFile> inDerFile(TFile::Open(inputDerFile.c_str(), "read"));
    parametersString = inDerFile->Get<TObjString>("Metadata/ParameterMap");
    prefitResidualsString = inDerFile->Get<TObjString>("Metadata/ResidualsMap");
    TTree* clustersTree;
    if (enablePixels) clustersTree = static_cast<TTree*>(inFile->Get("CoincidentClusters"));
    else clustersTree = static_cast<TTree*>(inFile->Get("sync_clusters_tree"));
    TTree* milleTree = static_cast<TTree*>(inDerFile->Get("MilleData"));
        
    std::array<Int_t, NBRANCH> branchesMille;
    std::vector<Int_t> branches;
    std::vector<std::array<Int_t, NBRANCHPIX>> branchesPixel;
    std::array<Int_t, NBRANCHPIX> temp_arr_pix;
    branchesPixel.assign(NLAD, temp_arr_pix);
        
    const auto &nEntries = milleTree->GetEntries();
    
    gInterpreter->GenerateDictionary("vector<vector<unsigned int>>");
    
    branchesMille.at(0) = milleTree->SetBranchAddress("resid_value", &resid_intermediate);
    branchesMille.at(1) = milleTree->SetBranchAddress("meas_error", &meas_errors);
    branchesMille.at(2) = milleTree->SetBranchAddress("meas_labels", &meas_labels);
    branchesMille.at(3) = milleTree->SetBranchAddress("glder", &vglder);
    branchesMille.at(4) = milleTree->SetBranchAddress("labels", &vparamlabels);
    branchesMille.at(5) = milleTree->SetBranchAddress("event", &milleEvent);
    
    std::size_t key{0};
    if (!enablePixels) {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            key = i * 10 + 12;
            vcogs[key] = nullptr;
            vcogsAligned[key] = new std::vector<double>;
            branches.push_back(clustersTree->SetBranchAddress(Form("cog%lu", i), &(vcogs.at(key))));
        }
    }
    else {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            key = i * 10 + 12;
            vcogs[key] = nullptr;
            vcogsAligned[key] = new std::vector<double>;
            branches.push_back(clustersTree->SetBranchAddress(Form("stripx%lu_cog", i + 1), &(vcogs.at(key))));
            if (i < NLAD) {
                vquadX[i] = nullptr;
                vquadY[i] = nullptr;
                vpixelSizes[i] = nullptr;
                (branchesPixel.at(i)).at(0) = clustersTree->SetBranchAddress(Form("quad%lu_x", i + 1), &(vquadX.at(i)));
                (branchesPixel.at(i)).at(1) = clustersTree->SetBranchAddress(Form("quad%lu_y", i + 1), &(vquadY.at(i)));
                (branchesPixel.at(i)).at(1) = clustersTree->SetBranchAddress(Form("quad%lu_size", i + 1), &(vpixelSizes.at(i)));
            }
        }
    }

    // branch check
    
    if (enablePixels) {
        for (std::size_t i{0}; i < NLAD; ++i) {
            for (uint br{0}; br < branchesPixel.at(i).size(); br++) {
                std::cout << "ladder " << i << " branch " << br << " status has value " << branchesPixel.at(i).at(br) << std::endl;
            }
        }
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            std::cout << "ladder " << i << " branch status has value " << branches.at(i) << std::endl;
        }
    }
    else {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            std::cout << "ladder " << i << " branch status has value " << branches.at(i) << std::endl;
        }
    }
    
    
    for (uint br{0}; br < branches.size(); br++) {
        std::cout << " branch " << br << " has value " << branches.at(br) << std::endl;
    }
    
    TFile *outFile = TFile::Open(outputFile.c_str(), "recreate");
    TTree *tracksTree = new TTree("tracks_tree", "Fitted tracks");
    //*/
    float chi2, pvalue, rmse_x, rmse_y, rmse_z, momReco, momX, momY, momZ, momErr, curvatureReco, curvatureRecoErr;
    float xInitDir, yInitDir, zInitDir, momInit, p0, p1, kinEnergy, kinEnergyErr;
    int hnmeas, hPDG, hConvTracks;
    UInt_t conversionDepth;
    std::vector<float> xErrors, yErrors, zErrors;
    std::vector<int> lengthOut;
    std::vector<double> integralOut;
    std::vector<std::vector<double>> sovernsPerChannelOut;
    TMatrixDSym covMatrix, correlMatrix;
    //*
    auto chi2br = tracksTree->Branch("chi2br", &chi2, "chi2/F");
    auto pvaluebr = tracksTree->Branch("pvaluebr", &pvalue, "pvalue/F");
    auto p0br = tracksTree->Branch("p0br", &p0, "p0/F");
    auto p1br = tracksTree->Branch("p1br", &p1, "p1/F");
    auto covMatrixbr = tracksTree->Branch("covMatrixbr", &covMatrix);
    auto corelMatrixbr = tracksTree->Branch("correlMatrixbr", &correlMatrix);
    auto rmse_ybr = tracksTree->Branch("rmse_ybr", &rmse_y, "rmse_y/F");
    //auto xdirbr = tracksTree->Branch("xdirbr", &xdir, "xdir/F");
    //auto ydirbr = tracksTree->Branch("ydirbr", &ydir, "ydir/F");
    //auto zdirbr = tracksTree->Branch("zdirbr", &zdir, "zdir/F");
    auto hnmeasbr = tracksTree->Branch("hnmeasbr", &hnmeas, "hnmeas/I");
    auto yerrbr = tracksTree->Branch("yerrbr", &yErrors);
    auto momRecobr = tracksTree->Branch("momRecobr", &momReco, "momReco/F");
    auto curvatureRecobr = tracksTree->Branch("curvatureRecobr", &curvatureReco, "curvatureReco/F");
    auto curvatureRecoErrbr = tracksTree->Branch("curvatureRecoErrbr", &curvatureRecoErr, "curvatureRecoErr/F");
    auto momXbr = tracksTree->Branch("momXbr", &momX, "momX/F");
    auto momYbr = tracksTree->Branch("momYbr", &momY, "momY/F");
    auto momZbr = tracksTree->Branch("momZbr", &momZ, "momZ/F");
    auto momErrbr = tracksTree->Branch("momErrbr", &momErr, "momErr/F");
    auto kinEnergybr = tracksTree->Branch("kinEnergybr", &kinEnergy, "kinEnergy/F");
    auto kinEnergyErrbr = tracksTree->Branch("kinEnergyErrbr", &kinEnergyErr, "kinEnergyErr/F");
    auto hPDGbr = tracksTree->Branch("hPDGbr", &hPDG, "hPDG/I");
    auto lengthbr = tracksTree->Branch("clusterlengthbr", &lengthOut);
    auto integralbr = tracksTree->Branch("clusterintegralbr", &integralOut);
    auto sovernsbr = tracksTree->Branch("sovernsbr", &sovernsPerChannelOut);
    // auto hConvTracksbr = tracksTree->Branch("hConvTracksbr", &hConvTracks, "hConvTracks/I");

    std::shared_ptr<TVectorD> xCoord, yCoord, zCoord, xErr, yErr, zErr;
    std::shared_ptr<TVectorD> xPixel, yPixel, xPixelErr, yPixelErr;
    zCoord = std::make_shared<TVectorD>(NLAD * NBOARD, &detectorPos[0]);
    zErr = std::make_shared<TVectorD>(NLAD * NBOARD, &detectorPosErr[0]);
    xPixel = std::make_shared<TVectorD>(NLAD);
    yPixel = std::make_shared<TVectorD>(NLAD);
    xPixelErr = std::make_shared<TVectorD>(NLAD);
    yPixelErr = std::make_shared<TVectorD>(NLAD);
    TVectorD chi2array;
    double chi2Sum{0};
    
    unsigned int N1event{0}, N2events{0}, N3events{0}, N4events{0}, N5plusevents{0};
    unsigned int N1GoodEvent{0};
    unsigned int Nnot0Layer{0}, Nnot1Layer{0}, Nnot2Layer{0}, Nnot3Layer{0}, Nnot4Layer{0}, Nnot5Layer{0};

    
    genfit::Track* fitTrack{nullptr};
    TVector3 pos(0, 0, detectorPos.front());
    TVector3 mom;
    if (!beamTestData) mom = TVector3(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
    else mom = TVector3(0, 0, pTruth); // 10 GeV/c
    TMatrixDSym covM(6);
    for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
    for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
    genfit::AbsTrackRep* rep{nullptr};
    genfit::AbsTrackRep* secondRep{nullptr};
    genfit::AbsTrackRep* thirdRep{nullptr};
    
    genfit::MeasuredStateOnPlane fittedState;
    genfit::ReferenceStateOnPlane* refState{nullptr};
    genfit::FitStatus* fitStatus{nullptr};
    TVector3 fittedTrackDir, primaryPartDir;
    genfit::TrackPoint* trackPoint{nullptr};
    genfit::AbsMeasurement* rawMeas{nullptr};
    genfit::KalmanFitterInfo* fitterInfo{nullptr};
    genfit::MeasurementOnPlane residual;
    genfit::AbsTrackRep* cardRep{nullptr};
    unsigned int nTrackPoints;
    int detId;
    TVectorD scdState(2);
    
    auto notSingle = [](std::size_t n) -> bool {
        return n != 1;
    };
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        milleTree->GetEntry(event);
        clustersTree->GetEntry(milleEvent);
        
        N1event++;
        
        std::cout << "event: " << event << std::endl;
    
        correctStripPositions(vcogs, vcogsAligned, true, 0);
        
        if (!nov2021BT) updateVcogs(vcogs, vcogsAligned, 0, enablePixels);
        else {
            for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
                key = i * 10 + 12;
                vcogsAligned[key]->push_back(vcogs.at(key)->at(0));
            }
        }
        
        if (enablePixels) {
            for (std::size_t i{0}; i < NLAD; ++i) {
                if (i == 0) {
                    if (vquadX.at(i)->at(0) <= 254) {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - edge_widthY_quad1 + 1.0) * conversionCoefPixel;
                    }
                    else if (vquadX.at(i)->at(0) >= 257) {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + edge_widthY_quad1 - 1.0) * conversionCoefPixel;
                    }
                    else if (vquadX.at(i)->at(0) <= 255) {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - (edge_widthY_quad1 - 1.0) / 2.) * conversionCoefPixel;
                    }
                    else if (vquadX.at(i)->at(0) >= 256) {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + (edge_widthY_quad1 - 1.0) / 2.) * conversionCoefPixel;
                    }
                    else
                    {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthY_quad1;
                    }
                    if (vquadY.at(i)->at(0) <= 254) {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - edge_widthX_quad1 + 1.0) * conversionCoefPixel;
                    }
                    else if (vquadY.at(i)->at(0) >= 257) {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + edge_widthX_quad1 - 1.0) * conversionCoefPixel;
                    }
                    else if (vquadY.at(i)->at(0) <= 255) {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - (edge_widthX_quad1 - 1.0) / 2.) * conversionCoefPixel;
                    }
                    else if (vquadY.at(i)->at(0) >= 256) {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + (edge_widthX_quad1 - 1.0) / 2.) * conversionCoefPixel;
                    }
                    else {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthX_quad1;
                    }
                }
                else {
                    if (vquadX.at(i)->at(0) <= 254) {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - edge_widthY_quad2 + 1.0) * conversionCoefPixel;
                    }
                    else if (vquadX.at(i)->at(0) >= 257) {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + edge_widthY_quad2 - 1.0) * conversionCoefPixel;
                    }
                    else if (vquadX.at(i)->at(0) <= 255) {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - (edge_widthY_quad2 - 1.0) / 2.) * conversionCoefPixel;
                    }
                    else if (vquadX.at(i)->at(0) >= 256) {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + (edge_widthY_quad2 - 1.0) / 2.) * conversionCoefPixel;
                    }
                    else
                    {
                        (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthY_quad2;
                    }
                    if (vquadY.at(i)->at(0) <= 254) {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - edge_widthX_quad2 + 1.0) * conversionCoefPixel;
                    }
                    else if (vquadY.at(i)->at(0) >= 257) {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + edge_widthX_quad2 - 1.0) * conversionCoefPixel;
                    }
                    else if (vquadY.at(i)->at(0) <= 255) {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - (edge_widthX_quad2 - 1.0) / 2.) * conversionCoefPixel;
                    }
                    else if (vquadY.at(i)->at(0) >= 256) {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + (edge_widthX_quad2 - 1.0) / 2.) * conversionCoefPixel;
                    }
                    else {
                        (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthX_quad2;
                    }
                }
                (*xPixelErr)[i] = conversionCoefPixel / sqrt(12. * vpixelSizes.at(i)->at(0));
                (*yPixelErr)[i] = conversionCoefPixel / sqrt(12. * vpixelSizes.at(i)->at(0));
            }
        }
        
        xErr = std::make_shared<TVectorD>(NLAD * NBOARD, &convertErrors(*meas_errors, *meas_labels, false)[0]);

        yErr = std::make_shared<TVectorD>(NLAD * NBOARD, &convertErrors(*meas_errors, *meas_labels, true)[0]);
        
        xCoord = std::make_shared<TVectorD>(NLAD * NBOARD, &applyAlignmentCorrections(vcogsAligned, *zCoord, *meas_labels, *vglder, *vparamlabels, parametersString, prefitResidualsString, false)[0]);
                        
        yCoord = std::make_shared<TVectorD>(NLAD * NBOARD, &applyAlignmentCorrections(vcogsAligned, *zCoord, *meas_labels, *vglder, *vparamlabels, parametersString, prefitResidualsString, true)[0]);
        
        // yCoord->Print();
        // TVector3 currentFieldVal = genfit::FieldManager::getInstance()->getFieldVal(TVector3(0.,0.,0.));
        // currentFieldVal.Print();
            
        int sign{1};
        rep = new genfit::RKTrackRep(sign*pdg);
        genfit::MeasuredStateOnPlane stateRef(rep);
        if (!beamTestData) mom.SetXYZ(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
        else mom.SetXYZ(0, 0, pTruth); // 10 GeV/c
        pos.SetXYZ((*xCoord)[0], (*yCoord)[0], (*zCoord)[0]);
        if (enablePixels) pos.SetXYZ((*xCoord)[0], (*yCoord)[0], detectorPosPixel.front());
        for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
        for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
        rep->setPosMomCov(stateRef, pos, mom, covM);
        
        //second rep
        if (!beamTestData || enableSecondRep) {
            sign = -1;
            if (beamTestData) { sign = 1; pdg = 211; }
            secondRep = new genfit::RKTrackRep(sign*pdg);
            genfit::MeasuredStateOnPlane stateSecondRef(secondRep);
            if (beamTestData) mom.SetXYZ(0, 0, pTruth);
            mom.SetXYZ(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
            pos.SetXYZ((*xCoord)[0], (*yCoord)[0], (*zCoord)[0]);
            for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
            for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
            secondRep->setPosMomCov(stateSecondRef, pos, mom, covM);
            if (enableThirdRep) {
                sign = -1;
                pdg = 11;
                thirdRep = new genfit::RKTrackRep(sign*pdg);
                genfit::MeasuredStateOnPlane stateThirdRef(thirdRep);
                mom.SetXYZ(0, 0, pTruth);
                pos.SetXYZ((*xCoord)[0], (*yCoord)[0], (*zCoord)[0]);
                for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
                for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
                thirdRep->setPosMomCov(stateThirdRef, pos, mom, covM);
            }
        }
            
        // create track
        TVectorD seedState(6);
        TMatrixDSym seedCov(6);
        rep->get6DStateCov(stateRef, seedState, seedCov);
        fitTrack = new genfit::Track(rep, seedState, seedCov); //initialized with smeared rep
        if (!beamTestData || enableSecondRep) {
            fitTrack->addTrackRep(secondRep);
            if (enableThirdRep) fitTrack->addTrackRep(thirdRep);
        }
        
        std::cout << "after fitTrack definition: " << std::endl;
        
        std::cout << "before performFitting: " << std::endl;
        
        bool fitResult = performFitting(*zCoord, *yCoord, *zErr, *yErr, *xCoord, *xErr, fitTrack, fitter);
        
        std::cout << "after performFitting: " << std::endl;
        
        cardRep = fitTrack->getCardinalRep();
        
        fitStatus = fitTrack->getFitStatus(cardRep);
        chi2 = fitStatus->getChi2() / fitStatus->getNdf();
        pvalue = fitStatus->getPVal();
        if (pvalue > 1.0E-33) chi2Sum += fitStatus->getChi2();

        hnmeas = yCoord->GetNrows();
    
        yErrors.clear();
        yErrors.shrink_to_fit();
        
        std::cout << "before residual calculation: " << std::endl;
        
        nTrackPoints = fitTrack->getNumPoints();
        
        std::cout << "nTrackPoints: " << nTrackPoints << std::endl;

        if (fitResult && fitStatus->isFitConverged()) {
            
            for (std::size_t i = 0; i < nTrackPoints; ++i) {
                trackPoint = fitTrack->getPointWithMeasurementAndFitterInfo(i, cardRep);
                rawMeas = trackPoint->getRawMeasurement();
                std::unique_ptr<const genfit::AbsHMatrix> HitHMatrix(rawMeas->constructHMatrix(cardRep));
                detId = rawMeas->getDetId();
                fitterInfo = trackPoint->getKalmanFitterInfo();
                residual = fitterInfo->getResidual();
                refState = fitterInfo->getReferenceState();
                scdState = residual.getState();
//                TVectorD raw_coords = rawMeas->getRawHitCoords();
//                TVectorD updated_ref_coords = HitHMatrix->Hv(refState->getState());
//                scdState = raw_coords - updated_ref_coords;
                yErrors.push_back(scdState[1]);
            }
            
            if (pvalue > 1.0E-33) N1GoodEvent++;
            
            rmse_y = TMath::RMS(yErrors.begin(), yErrors.end());
            
            fittedState = fitTrack->getFittedState();
            covMatrix.ResizeTo(fittedState.getCov());
            covMatrix = fittedState.getCov();
            
            momReco = fittedState.getMomMag();
            momErr = (momReco - fabs(pTruth)) / fabs(pTruth);
            
            kinEnergy = mass * (sqrt(1. + (momReco / mass) * (momReco / mass)) - 1.);
            kinEnergyErr = (kinEnergy - kinEnergyTruth) / kinEnergyTruth;
            
            curvatureReco = 1. / momReco;
            curvatureRecoErr = (curvatureReco - 1. / fabs(pTruth)) * fabs(pTruth);
            
            std::cout << "Reconstructed momentum: " << momReco << " GeV/c" << std::endl;
            std::cout << "Reconstructed momentum error: " << momErr << " GeV/c" << std::endl;
            
            momX = (fittedState.getMom()).X();
            momY = (fittedState.getMom()).Y();
            momZ = (fittedState.getMom()).Z();
            
            hPDG = fittedState.getPDG();
            
            tracksTree->Fill();
            
            #ifdef SHOW_TRACKS
                display->addEvent(fitTrack);
            #endif
            
        }
        
        delete fitTrack;
        fitTrack = nullptr;
        
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            key = i * 10 + 12;
            vcogsAligned[key]->clear();
            vcogsAligned[key]->shrink_to_fit();
            (*zCoord)[i] = detectorPos.at(i);
        }
        
        std::cout << "after residual calculation: " << std::endl;

    }
    
    chi2array.ResizeTo(1);
    chi2array[0] = chi2Sum / N1GoodEvent;
    chi2Sum = 0.;
    
    outFile->cd();
    tracksTree->Write();
    outFile->Close();
    
    inFile->Close();
    
    std::cout << "Total number of events: " << nEntries << std::endl;
    std::cout << "Number of events with 1 cluster in every plane: " << N1event << std::endl;
    std::cout << "Number of events with 2 clusters in every plane: " << N2events << std::endl;
    std::cout << "Number of events with 3 clusters in every plane: " << N3events << std::endl;
    std::cout << "Number of events with 4 clusters in every plane: " << N4events << std::endl;
    std::cout << "Number of events with 5+ clusters in every plane: " << N5plusevents << std::endl;
    
    std::cout << "Efficiency, layer 0: " << N1event * 100.0 / Nnot0Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 1: " << N1event * 100.0 / Nnot1Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 2: " << N1event * 100.0 / Nnot2Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 3: " << N1event * 100.0 / Nnot3Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 4: " << N1event * 100.0 / Nnot4Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 5: " << N1event * 100.0 / Nnot5Layer << " %" << std::endl;
    
    double totalEff = (N1event * 1.0 / Nnot0Layer) * (N1event * 1.0 / Nnot1Layer) * (N1event * 1.0 / Nnot2Layer) * (N1event * 1.0 / Nnot3Layer) * (N1event * 1.0 / Nnot4Layer) * (N1event * 1.0 / Nnot5Layer);
    
    std::cout << "Tracking efficiency: " << totalEff * 100.0 << " %" << std::endl;
    
    #ifdef SHOW_TRACKS
        display->setOptions("ABDEFGHMPT"); // G show geometry
        display->open();
    #endif
    
//    myApp->Run();
//    myApp->Delete();
    
    return 0;
}
