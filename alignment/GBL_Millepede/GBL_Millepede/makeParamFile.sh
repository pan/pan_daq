#!/bin/bash

# Your input file
input_file="millepede.res"

# Output file to store the filtered data
output_file="mp2param.txt"

# Search for the "Parameter" line and store lines starting from there in the temporary file
awk '/Parameter/,0' "$input_file" > temp_filtered_data.txt

# Use awk to print only the first 3 columns from the temporary file
awk '{ print $1, $2, $3 }' temp_filtered_data.txt > "$output_file"

# Remove the temporary file
rm temp_filtered_data.txt

echo "Filtered data saved to $output_file"
