//
//  main.cpp
//  GBL_Millepede
//
//  Created by Daniil Sukhonos on 27.07.23.
//

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <unordered_map>

#include "GFGbl.h"
#include "FieldManager.h"
#include "ConstField.h"
#include "MaterialEffects.h"
#include "TGeoMaterialInterface.h"
#include "EventDisplay.h"
#include "GblFitterInfo.h"
#include "PlanarMeasurement.h"
#include "RectangularFinitePlane.h"
#include "SharedPlanePtr.h"
#include "KalmanFitterInfo.h"
#include "KalmanFitterRefTrack.h"

#include "TGeoManager.h"
#include "TDatabasePDG.h"
#include "TGraphErrors.h"
#include "TRandom3.h"
#include "TH1D.h"
#include "TApplication.h"
#include "TCanvas.h"

#include "miniPanMagField.hpp"

// #define SHOW_TRACKS

namespace po = boost::program_options;
namespace fs = boost::filesystem;

constexpr std::size_t NLAD{2};
constexpr std::size_t NBOARD{3};
constexpr std::size_t NBRANCH{5};
constexpr std::size_t NBRANCHPIX{3};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double conversionCoefPixel{0.0055}; // cm / ch
constexpr float detectorResolution{0.0025}; // resolution of scd detectors in cm
constexpr float stripLength{5.12}; // stripx strip length in cm
float pTruth{10.0}; // beamtest energy 10 GeV/c pi-
double mass{0.0};
constexpr bool nov2021BT{false};
constexpr bool june2022BT{false};
constexpr bool sep2022BT{false};
constexpr bool nov2022BT{false};
constexpr bool apr2023BT{true};
constexpr bool summer2023BT{false};

std::vector<double> detectorPos;
std::vector<double> detectorPosErr = {0.01, 0.01, 0.01, 0.01, 0.01, 0.01};
std::array<std::vector<double>, NLAD * NBOARD> deltas;
// genfit::AbsFinitePlane* finiteStripX = new genfit::StripXFinitePlane(2.55, 2.56);
std::unique_ptr<genfit::AbsFinitePlane> finiteStripX = std::make_unique<genfit::RectangularFinitePlane>(-2.57, 2.57, -2.57, 2.57);
std::unique_ptr<genfit::AbsFinitePlane> finitePixel = std::make_unique<genfit::RectangularFinitePlane>(-1.414, 1.414, -1.414, 1.414);
std::vector<double> detectorPosPixel = {-7.8695, 7.8845};

TRandom3 r{42};

bool multipleClusterLength(const std::unordered_map<std::size_t, std::vector<int>*> &vlengths) {
    bool result{true};
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (vlengths.at(i)->front() == 1) result = false;
    }

    return result;
}

double MCSSigma(std::size_t i, const TVectorD &detectorPosLocal, const std::vector<double> &slopes, double angle) {
    
    if (i == 0) return 0.;
    double gammaFactor = sqrt(1 + (fabs(pTruth) / mass) * (fabs(pTruth) / mass));
    double velocity = fabs(pTruth) / (mass * gammaFactor);
    double xSi = 9.37; // cm for silicon
    double s = 0.0150; // cm thickness of each tracking plane
    double gammaAngle{TMath::Pi() / 2.};
    double phiAngle{TMath::Pi() / 2.};
    double relThickness = s / xSi;
    double theta0{0.};
    double sigma = 0.;
    for (std::size_t j{0}; j < i; ++j) {
        if (fabs(slopes.at(i)) > 1.e-12) gammaAngle = atan(slopes.at(i));
        else gammaAngle = TMath::Pi() / 2.;
        phiAngle = gammaAngle - angle;
        s *= 1. / fabs(sin(phiAngle));
        relThickness = s / xSi;
        theta0 = (13.6 / (fabs(pTruth) * 1.e+03 * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
        sigma += (detectorPosLocal[i] - detectorPosLocal[j]) * (detectorPosLocal[i] - detectorPosLocal[j]) * theta0 * theta0;
        s = 0.0150;
    }
    gammaAngle = TMath::Pi() / 2.;
    phiAngle = gammaAngle - angle;
    s *= 1. / fabs(sin(phiAngle));
    relThickness = s / xSi;
    theta0 = (13.6 / (fabs(pTruth) * 1.e+03 * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
    // if (i > 2) sigma += (detectorPosLocal[i] + 0.006) * (detectorPosLocal[i] + 0.006) * theta0 * theta0; // middle StripY layer
    std::cout << "MCSSigma, Layer " << i << " equal " << sqrt(sigma) << std::endl;
    return sqrt(sigma);
}

void correctStripPositions(const std::unordered_map<std::size_t, std::vector<double>*> &vec, std::unordered_map<std::size_t, std::vector<double>*> &vecAligned, bool generatedDataFlag, std::size_t j) {
    for (std::size_t i{0}; i < vecAligned.size(); ++i) {
        if (generatedDataFlag) vecAligned.at(i)->push_back(vec.at(i)->at(j));
        else {
            if (vec.at(i)->at(j) >= 0 and vec.at(i)->at(j) < 256) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) - 1);
                if (vec.at(i)->at(j) < (0 + 64)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (0 + 128)) vecAligned.at(i)->back() -= 0;
                else if (vec.at(i)->at(j) < (0 + 192)) vecAligned.at(i)->back() += 1;
                else if (vec.at(i)->at(j) < (0 + 256)) vecAligned.at(i)->back() += 2;
            }
            else if (vec.at(i)->at(j) >= 256 and vec.at(i)->at(j) < 512) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) + 1);
                if (vec.at(i)->at(j) < (256 + 64)) vecAligned.at(i)->back() -= 2;
                else if (vec.at(i)->at(j) < (256 + 128)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (256 + 192)) vecAligned.at(i)->back() += 0;
                else if (vec.at(i)->at(j) < (256 + 256)) vecAligned.at(i)->back() += 1;
            }
            else if (vec.at(i)->at(j) >= 512 and vec.at(i)->at(j) < 768) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) - 1);
                if (vec.at(i)->at(j) < (512 + 64)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (512 + 128)) vecAligned.at(i)->back() -= 0;
                else if (vec.at(i)->at(j) < (512 + 192)) vecAligned.at(i)->back() += 1;
                else if (vec.at(i)->at(j) < (512 + 256)) vecAligned.at(i)->back() += 2;
            }
            else if (vec.at(i)->at(j) >= 768 and vec.at(i)->at(j) < 1024) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) + 1);
                if (vec.at(i)->at(j) < (768 + 64)) vecAligned.at(i)->back() -= 2;
                else if (vec.at(i)->at(j) < (768 + 128)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (768 + 192)) vecAligned.at(i)->back() += 0;
                else if (vec.at(i)->at(j) < (768 + 256)) vecAligned.at(i)->back() += 1;
            }
            else if (vec.at(i)->at(j) >= 1024 and vec.at(i)->at(j) < 1280) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) - 1);
                if (vec.at(i)->at(j) < (1024 + 64)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (1024 + 128)) vecAligned.at(i)->back() -= 0;
                else if (vec.at(i)->at(j) < (1024 + 192)) vecAligned.at(i)->back() += 1;
                else if (vec.at(i)->at(j) < (1024 + 256)) vecAligned.at(i)->back() += 2;
            }
            else if (vec.at(i)->at(j) >= 1280 and vec.at(i)->at(j) < 1536) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) + 1);
                if (vec.at(i)->at(j) < (1280 + 64)) vecAligned.at(i)->back() -= 2;
                else if (vec.at(i)->at(j) < (1280 + 128)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (1280 + 192)) vecAligned.at(i)->back() += 0;
                else if (vec.at(i)->at(j) < (1280 + 256)) vecAligned.at(i)->back() += 1;
            }
            else if (vec.at(i)->at(j) >= 1536 and vec.at(i)->at(j) < 1792) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) - 1);
                if (vec.at(i)->at(j) < (1536 + 64)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (1536 + 128)) vecAligned.at(i)->back() -= 0;
                else if (vec.at(i)->at(j) < (1536 + 192)) vecAligned.at(i)->back() += 1;
                else if (vec.at(i)->at(j) < (1536 + 256)) vecAligned.at(i)->back() += 2;
            }
            else if (vec.at(i)->at(j) >= 1792 and vec.at(i)->at(j) < 2048) {
                vecAligned.at(i)->push_back(vec.at(i)->at(j) + 1);
                if (vec.at(i)->at(j) < (1792 + 64)) vecAligned.at(i)->back() -= 2;
                else if (vec.at(i)->at(j) < (1792 + 128)) vecAligned.at(i)->back() -= 1;
                else if (vec.at(i)->at(j) < (1792 + 192)) vecAligned.at(i)->back() += 0;
                else if (vec.at(i)->at(j) < (1792 + 256)) vecAligned.at(i)->back() += 1;
            }
        }
    }
}

void updateVcogs(const std::unordered_map<std::size_t, std::vector<double>*> &vec, std::unordered_map<std::size_t, std::vector<double>*> &vecAligned, std::size_t j, bool enablePixels = false) {
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (i % 2 == 0 && !enablePixels) vecAligned[i]->at(j) = 2047.0 - vec.at(i)->at(j);
        else vecAligned[i]->at(j) = vec.at(i)->at(j);
    }
}

template <typename T>
std::vector<double> transformVectors(const std::unordered_map<std::size_t, std::vector<T>*> &vec, const std::unordered_map<std::size_t, std::vector<double>*> &vsoverns, std::size_t j, bool enablePixels = false) {
    
    std::vector<double> outVec;
    double alpha{0.0};
    
    for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
        if (std::is_integral<T>::value) {
            alpha = sqrt(1.5 + vec.at(i)->at(j));
            if (enablePixels) {
                outVec.push_back(conversionCoef / sqrt(12.0));
            }
            else {
                if (vec.at(i)->at(j) == 1) outVec.push_back(conversionCoef / sqrt(12.0));
                else outVec.push_back(alpha * conversionCoef / vsoverns.at(i)->at(j)); // due to CoG calculation
            }
        }
        else {
            if (summer2023BT) {
                outVec.push_back((2047.0 - vec.at(i)->at(j) - 1023.5) * conversionCoef);
            }
            else outVec.push_back((vec.at(i)->at(j) - 1023.5) * conversionCoef);
        }
    }
    
    return outVec;
}

bool performFitting(const TVectorD &zCoord, const TVectorD &yCoord, const TVectorD &zErr, const TVectorD &yErr, const TVectorD &xPixel, const TVectorD &yPixel, const TVectorD &xPixelErr, const TVectorD &yPixelErr, bool enablePixels, bool useAlignedGeom, genfit::Track* fitTrack, std::shared_ptr<genfit::GFGbl> fitterMillePede, std::shared_ptr<genfit::KalmanFitterRefTrack> fitter, bool doGBLFit, std::size_t currentEntry) {
    
    int detId{0}; // detector ID
    int planeId{0};
    int hitId{0}; // hit ID
    
    genfit::PlanarMeasurement* measurement{nullptr};
    //*
    TMatrixDSym hitCov(2);
    hitCov.UnitMatrix();
    hitCov(1, 1) = detectorResolution*detectorResolution / 12; // include misalignment factor?
    hitCov(0, 0) = stripLength * stripLength / 12; // 12 - classic approach

    TVectorD hitCoords(2);
    hitCoords[0] = 0.;
    hitCoords[1] = 0.;
    //*/
    /*
    TMatrixDSym hitCov(1);
    hitCov.UnitMatrix();
    hitCov *= detectorResolution*detectorResolution / 12;

    TVectorD hitCoords(1);
    hitCoords[0] = 0;
    //*/
    TVector3 detPlaneN, detPlaneU, detPlaneV, detPlane0;
    genfit::AbsTrackRep* cardRep{nullptr};
    genfit::FitStatus* fitStatus{nullptr};
    
    if (enablePixels) {
        for (std::size_t i{0}; i < NLAD * (NBOARD + 1); ++i) {
            detPlaneN.SetXYZ(0, 0, 1);
            detPlaneN.SetMag(1);
            if (i == 0) {
                hitCoords[0] = xPixel[i];
                detPlane0.SetXYZ(0, 0, detectorPosPixel.front());
                hitCov(0, 0) = xPixelErr[i] * xPixelErr[i];
                hitCoords[1] = yPixel[i];
                hitCov(1, 1) = yPixelErr[i] * yPixelErr[i];
            }
            else if (i == NLAD * (NBOARD + 1) - 1) {
                hitCoords[0] = xPixel[1];
                detPlane0.SetXYZ(0, 0, detectorPosPixel.back());
                hitCov(0, 0) = xPixelErr[1];
                hitCoords[1] = yPixel[1];
                hitCov(1, 1) = yPixelErr[1] * yPixelErr[1];
            }
            else {
                hitCoords[0] = 0.;
                detPlane0.SetXYZ(0, 0, zCoord[i - 1]);
                hitCov(0, 0) = stripLength * stripLength / 4;
                hitCoords[1] = yCoord[i - 1];
                hitCov(1, 1) = yErr[i - 1] * yErr[i - 1] + detectorResolution * detectorResolution * 16 / 25;
            }
            std::cout << "hitCoords[0]: " << hitCoords[0] << std::endl;
            std::cout << "hitCoords[1]: " << hitCoords[1] << std::endl;
            detPlaneU = detPlaneN.Orthogonal();
            detPlaneU.SetMag(1);
            detPlaneV = detPlaneN.Cross(detPlaneU);
            detPlaneV.SetMag(1);
            detPlaneU.SetXYZ(fabs(detPlaneU.X()), fabs(detPlaneU.Y()), fabs(detPlaneU.Z()));
            detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
            measurement = new genfit::PlanarMeasurement(hitCoords, hitCov, ++detId, hitId, nullptr);
            if (i == 0 || i == NLAD * (NBOARD + 1) - 1) {
                measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finitePixel->clone())), ++planeId);
            }
            else {
                measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finiteStripX->clone())), ++planeId);
            }
            // measurement->setStripV();
            fitTrack->insertPoint(new genfit::TrackPoint(measurement->clone(), fitTrack));
            delete measurement;
            measurement = nullptr;
        }
    }
    else {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            detPlaneN.SetXYZ(0, 0, 1);
            detPlaneN.SetMag(1);
            hitCoords[0] = 0.0;
            if (useAlignedGeom) {
                // detPlaneN.RotateX(TMath::DegToRad() * alignPars.at(i)[2]);
                // detPlane0.SetXYZ(0, 0, detectorPos[i] + alignPars.at(i)[1]);
                // hitCoords[1] = yCoord[i] / TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]);
                // hitCov(1,1) = yErr[i] * yErr[i] / (TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]) * TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]));
                // hitCoords[0] = yCoord[i] / TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]);
                // hitCov(0,0) = yErr[i] * yErr[i] / (TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]) * TMath::Cos(TMath::DegToRad() * alignPars.at(i)[2]));
            }
            else {
                detPlane0.SetXYZ(0, 0, zCoord[i]);
                hitCoords[1] = yCoord[i];
                hitCov(1, 1) = yErr[i] * yErr[i];
                // hitCov(1,1) = yErr[i] * yErr[i] + detectorResolution * detectorResolution * 16 / 25; // 16 / 25 default; 5 / 25 for 1 GeV/c
                // hitCoords[0] = yCoord[i];
                // hitCov(0,0) = yErr[i] * yErr[i];
            }
            detPlaneU = detPlaneN.Orthogonal();
            detPlaneU.SetMag(1);
            detPlaneV = detPlaneN.Cross(detPlaneU);
            detPlaneV.SetMag(1);
            if (useAlignedGeom) {
                detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
            }
            else {
                detPlaneU.SetXYZ(fabs(detPlaneU.X()), fabs(detPlaneU.Y()), fabs(detPlaneU.Z()));
                detPlaneV.SetXYZ(fabs(detPlaneV.X()), fabs(detPlaneV.Y()), fabs(detPlaneV.Z()));
            }
            measurement = new genfit::PlanarMeasurement(hitCoords, hitCov, ++detId, hitId, nullptr);
            if (!useAlignedGeom) measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneU, detPlaneV, finiteStripX->clone())), ++planeId);
            else measurement->setPlane(genfit::SharedPlanePtr(new genfit::DetPlane(detPlane0, detPlaneV, detPlaneU, finiteStripX->clone())), ++planeId);
            // measurement->setStripV();
            fitTrack->insertPoint(new genfit::TrackPoint(measurement->clone(), fitTrack));
            delete measurement;
            measurement = nullptr;
        }
    }
    
    if (fitTrack->getNumPoints() == 0) return false;
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track before fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    // do the fit
    try {
        fitter->processTrack(fitTrack);
    }
    catch (genfit::Exception& e) {
        std::cout << "genfit failed to fit track" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->checkConsistency();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with track after fit, not consistent" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->determineCardinalRep();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with cardinal rep determination after fit" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    try {
        fitTrack->getFittedState();
        // print fit result
        // (fitTrack->getFittedState()).Print();
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with fittedstate" << std:: endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    cardRep = fitTrack->getCardinalRep();
    
    try {
        fitStatus = fitTrack->getFitStatus(cardRep);
        std::cout << "N Failed Points: " << fitStatus->getNFailedPoints() << std::endl;
        if (!fitStatus->isFitted()) return false;
        auto nmeas = fitStatus->getNdf();
        if (nmeas < 1) return false;
        auto chi2Value  = fitStatus->getChi2()/nmeas;
        std::cout << "N measurements: " << nmeas << std::endl;
        std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
        if (chi2Value < 0) return false;
        if (!fitStatus->isFitConverged()) {
            try {
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->udpateSeed(0);
                fitter->processTrack(fitTrack);
                fitTrack->checkConsistency();
                fitTrack->determineCardinalRep();
                fitTrack->getFittedState();
                cardRep = fitTrack->getCardinalRep();
                fitStatus = fitTrack->getFitStatus(cardRep);
                nmeas = fitStatus->getNdf();
                if (nmeas < 1) return false;
                chi2Value  = fitStatus->getChi2()/nmeas;
                std::cout << "N measurements: " << nmeas << std::endl;
                std::cout << "Chi2 / nmeas: " << chi2Value << std::endl;
                if (chi2Value < 0) return false;
            } catch (genfit::Exception& e) {
                std::cout << "Double refit failed" << std::endl;
                std::cerr << "Exception" << std::endl;
                std::cerr << e.what();
                throw e;
            }
        }
    }
    catch (genfit::Exception& e) {
        std::cout << "Problem with Fit Status" << std::endl;
        std::cerr << "Exception, next permutation" << std::endl;
        std::cerr << e.what();
        return false;
    }
    
    if (doGBLFit) {
        fitterMillePede->setCurrentEntry(currentEntry);
        fitterMillePede->processTrack(fitTrack);
    }
    
    return true;
}


int main(int argc, char * argv[]) {
    
    std::string inputFile, geomFile, pathToFieldMap;
    bool useAlignedGeom, beamTestData, useFieldMap, enablePixels, excludeSingleStripClusters, enableSecondRep, enableThirdRep;
    unsigned nIterations;
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "Produce help message")
        ("input-file,i", po::value<std::string>(&inputFile)->required(), "Input file name")
        ("geom-file,g", po::value<std::string>(&geomFile)->required(), "ROOT geometry file name")
        ("aligned-geom,a", po::bool_switch(&useAlignedGeom)->default_value(false), "Use file with aligned geometry")
        ("beamtest,b", po::value<bool>(&beamTestData)->default_value(true), "An input file is beamtest data")
        ("field-map,f", po::value<bool>(&useFieldMap)->default_value(true), "Use a simulated field map instead of a const field value")
        ("path-to-map,m", po::value<std::string>(&pathToFieldMap)->required(), "Path to field map files")
        ("enable-second-rep,r", po::bool_switch(&enableSecondRep)->default_value(false), "Add second particle representation for better fitting")
        ("enable-third-rep,t", po::bool_switch(&enableThirdRep)->default_value(false), "Add third particle representation for better fitting")
        ("enable-pixels,p", po::bool_switch(&enablePixels)->default_value(false), "An input file contains pixels data as well")
        ("exclude-single-clusters,s", po::bool_switch(&excludeSingleStripClusters)->default_value(false), "If true only clusters with length > 1 are considered")
        ("n-iterations,n", po::value<unsigned>(&nIterations)->default_value(1), "The number of iterations for pre-alignment");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    try {
        po::notify(vm);
    }
    catch (const po::required_option& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        std::cerr << desc << std::endl;
        return 1;
    }
    
    // Get the current working directory
    fs::path currentDir = fs::current_path();
    // Extract the directory path from argv[0]
    fs::path executablePath(argv[0]);
    fs::path directoryPath = executablePath.parent_path();
    std::string directoryPathStr = directoryPath.string();
    if (directoryPathStr.size() > 1 && directoryPathStr[0] == '.') {
        directoryPathStr = directoryPathStr.substr(1);
    }
    else if (directoryPathStr == ".") directoryPathStr = "";
    else if (directoryPathStr[0] != '/') directoryPathStr = "/" + directoryPathStr;
    
    TApplication* myApp = new TApplication("myApp", &argc, argv);
    
    std::unordered_map<std::size_t, std::vector<double>*> vintegrals, vcogs, vsoverns, vcogsAligned, vquadX, vquadY;
    std::unordered_map<std::size_t, std::vector<std::vector<double>>*> vsovernsPerChannel;
    std::unordered_map<std::size_t, std::vector<int>*> vlengths, vpixelSizes;
    
    if (nov2021BT) detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81 cm // November 2021 beamtest
    // if (june2022BT) detectorPos = {-7.006, -6.206, -0.470, 0.330, 5.870, 6.670}; // old
    if (june2022BT) detectorPos = {-7.006, -6.191, -0.470, 0.330, 5.870, 6.685}; // 0, 8.0, 65.36, 73.36, 128.76, 136.76 mm (sens. surf) // June/August 2022 beamtest
    if (sep2022BT) {
        detectorPosPixel = {-7.8985, 7.8985};
        detectorPos = {-6.699, -5.884, -0.316, 0.484, 5.884, 6.699};
    }
    if (nov2022BT) {
        detectorPosPixel = {-7.807, 7.807};
        detectorPos = {-6.6075, -5.7925, -0.4075, 0.3925, 5.7925, 6.6075};
    }
    if (apr2023BT || summer2023BT) {
        detectorPosPixel = {-7.8825, 7.8825};
        detectorPos = {-6.645, -5.83, -0.395, 0.4050, 5.83, 6.645};
    }
    
    double edge_widthX_quad1{2.}, edge_widthY_quad1{2.}, edge_widthX_quad2{2.}, edge_widthY_quad2{2.};

    int pdg;
    if (!beamTestData) pdg = 13; // particle pdg code for cosmics (mu-)
    else {
        pdg = -211; // pi- during beamtest
        if (june2022BT) {
            pdg = 11; // e-
            pTruth = 0.5; // 1 GeV/c, 0.5 GeV/c
            if (enablePixels) {
                pdg = 2212; // proton
                pTruth = 180; // 180 GeV/c
            }
        }
        else if (sep2022BT) {
            pdg = 11; // e- is 11, protons 2212
            pTruth = 1; // GeV/c
        }
        else if (nov2022BT) {
            pdg = (TDatabasePDG::Instance())->GetParticle("alpha")->PdgCode();
            pTruth = 150; // 150 GeV/c
        }
        else if (apr2023BT) {
            pdg = -11; // pi+ 211, proton 2212, positron -11
            pTruth = 0.25; // GeV/c
        }
        else if (summer2023BT) {
            pdg = 2212;
            pTruth = 0.5; // GeV/c
        }
    }
    
    mass = (TDatabasePDG::Instance())->GetParticle(pdg)->Mass();
    
    std::shared_ptr<genfit::GFGbl> fitterMillePede{nullptr};
    std::shared_ptr<genfit::KalmanFitterRefTrack> fitter{nullptr};
    fitterMillePede = std::make_shared<genfit::GFGbl>();
    fitter = std::make_shared<genfit::KalmanFitterRefTrack>();
//    fitterMillePede->setGBLOptions("THC", false);
    fitterMillePede->setMP2Options(0., 1, currentDir.string() + directoryPathStr + "/../GBL_Millepede/millefile.dat");
    
    TGeoManager* geoMan = new TGeoManager("Geometry", "miniPAN geometry");
    geoMan->Import(geomFile.c_str());

    if (!useFieldMap) {
        // genfit::FieldManager::getInstance()->init(new genfit::ConstField(0, 0, 0)); // in kGauss 1 T = 10 kG here we set the mag field of the Earth
        genfit::FieldManager::getInstance()->init(new genfit::ConstField(-4.0, 0., 0.));
    } else {
        if (nov2021BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("30mm", pathToFieldMap + "2-magnets-30mm-N48.txt"));
        else if (june2022BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("june2022", pathToFieldMap + "magnet_map_june_august_2022.txt"));
        else if (sep2022BT || nov2022BT || apr2023BT || summer2023BT) genfit::FieldManager::getInstance()->init(new genfit::PanBFieldMap("latest", pathToFieldMap + "magnet_map_sep_2022_onwards.txt"));
        // genfit::FieldManager::getInstance()->useCache(true, 8);
    }
    
    genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
        
    // const double charge{TDatabasePDG::Instance()->GetParticle(pdg)->Charge()/(3.)};

    #ifdef SHOW_TRACKS
        std::unique_ptr<genfit::EventDisplay> display(genfit::EventDisplay::getInstance());
        display->reset();
    #endif
    
    std::unique_ptr<TGraphErrors> gr{nullptr};
        
    std::unique_ptr<TFile> inFile(TFile::Open(inputFile.c_str(), "read"));
    TTree* clustersTree;
    if (enablePixels) clustersTree = static_cast<TTree*>(inFile->Get("CoincidentClusters"));
    else clustersTree = static_cast<TTree*>(inFile->Get("sync_clusters_tree"));
        
    std::vector<std::array<Int_t, NBRANCH>> branches;
    std::vector<std::array<Int_t, NBRANCHPIX>> branchesPixel;
    std::array<Int_t, NBRANCH> temp_arr;
    std::array<Int_t, NBRANCHPIX> temp_arr_pix;
    branches.assign(NLAD * NBOARD, temp_arr);
    branchesPixel.assign(NLAD, temp_arr_pix);
        
    const auto &nEntries = clustersTree->GetEntries();
    
    if (!enablePixels) {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            vintegrals[i] = nullptr;
            vcogs[i] = nullptr;
            vsoverns[i] = nullptr;
            vsovernsPerChannel[i] = nullptr;
            vlengths[i] = nullptr;
            vcogsAligned[i] = new std::vector<double>;
            (branches.at(i)).at(0) = clustersTree->SetBranchAddress(Form("integral%lu", i), &(vintegrals.at(i)));
            (branches.at(i)).at(2) = clustersTree->SetBranchAddress(Form("cog%lu", i), &(vcogs.at(i)));
            (branches.at(i)).at(3) = clustersTree->SetBranchAddress(Form("sovern%lu", i), &(vsoverns.at(i)));
            (branches.at(i)).at(4) = clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", i), &(vsovernsPerChannel.at(i)));
            (branches.at(i)).at(1) = clustersTree->SetBranchAddress(Form("length%lu", i), &(vlengths.at(i)));
        }
    }
    else {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            vintegrals[i] = nullptr;
            vcogs[i] = nullptr;
            vsoverns[i] = nullptr;
            vsovernsPerChannel[i] = nullptr;
            vlengths[i] = nullptr;
            vcogsAligned[i] = new std::vector<double>;
            (branches.at(i)).at(0) = clustersTree->SetBranchAddress(Form("stripx%lu_integral", i + 1), &(vintegrals.at(i)));
            (branches.at(i)).at(2) = clustersTree->SetBranchAddress(Form("stripx%lu_cog", i + 1), &(vcogs.at(i)));
            // clustersTree->SetBranchAddress(Form("sovern%lu", i), &vsoverns.at(i), &((branches.at(i)).at(3)));
            // clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", i), &vsovernsPerChannel.at(i), &((branches.at(i)).at(4)));
            (branches.at(i)).at(1) = clustersTree->SetBranchAddress(Form("stripx%lu_len", i + 1), &(vlengths.at(i)));
            if (i < NLAD) {
                vquadX[i] = nullptr;
                vquadY[i] = nullptr;
                vpixelSizes[i] = nullptr;
                (branchesPixel.at(i)).at(0) = clustersTree->SetBranchAddress(Form("quad%lu_x", i + 1), &(vquadX.at(i)));
                (branchesPixel.at(i)).at(1) = clustersTree->SetBranchAddress(Form("quad%lu_y", i + 1), &(vquadY.at(i)));
                (branchesPixel.at(i)).at(1) = clustersTree->SetBranchAddress(Form("quad%lu_size", i + 1), &(vpixelSizes.at(i)));
            }
        }
    }

    // branch check
        
    if (enablePixels) {
        for (std::size_t i{0}; i < NLAD; ++i) {
            for (uint br{0}; br < branchesPixel.at(i).size(); br++) {
                std::cout << "ladder " << i << " branch " << br << " has value " << branchesPixel.at(i).at(br) << std::endl;
            }
        }
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            for (uint br{0}; br < 3; br++) {
                std::cout << "ladder " << i << " branch " << br << " has value " << branches.at(i).at(br) << std::endl;
            }
        }
    }
    else {
        for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
            for (uint br{0}; br < branches.at(i).size(); br++) {
                std::cout << "ladder " << i << " branch " << br << " has value " << branches.at(i).at(br) << std::endl;
            }
        }
    }
    
    double chi2, pvalue, momReco, momErr, momX, momY, momZ;
    int hnmeas, hPDG;

    std::shared_ptr<TVectorD> yCoord, zCoord, yErr, zErr;
    std::shared_ptr<TVectorD> xPixel, yPixel, xPixelErr, yPixelErr;
    zCoord = std::make_shared<TVectorD>(NLAD * NBOARD, &detectorPos[0]);
    zErr = std::make_shared<TVectorD>(NLAD * NBOARD, &detectorPosErr[0]);
    xPixel = std::make_shared<TVectorD>(NLAD);
    yPixel = std::make_shared<TVectorD>(NLAD);
    xPixelErr = std::make_shared<TVectorD>(NLAD);
    yPixelErr = std::make_shared<TVectorD>(NLAD);
    TVectorD chi2array;
    double chi2Sum{0};
    
    unsigned int N1event{0}, N2events{0}, N3events{0}, N4events{0}, N5plusevents{0};
    unsigned int N1GoodEvent{0};
    unsigned int Nnot0Layer{0}, Nnot1Layer{0}, Nnot2Layer{0}, Nnot3Layer{0}, Nnot4Layer{0}, Nnot5Layer{0};

    
    genfit::Track* fitTrack{nullptr};
    TVector3 pos(0, 0, detectorPos.front());
    TVector3 mom;
    if (!beamTestData) mom = TVector3(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
    else mom = TVector3(0, 0, pTruth); // 10 GeV/c
    TMatrixDSym covM(6);
    for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
    for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
    genfit::AbsTrackRep* rep{nullptr};
    genfit::AbsTrackRep* secondRep{nullptr};
    genfit::AbsTrackRep* thirdRep{nullptr};
    
    genfit::MeasuredStateOnPlane fittedState;
    genfit::ReferenceStateOnPlane* refState{nullptr};
    genfit::FitStatus* fitStatus{nullptr};
    TVector3 fittedTrackDir, primaryPartDir;
    genfit::TrackPoint* trackPoint{nullptr};
    genfit::AbsMeasurement* rawMeas{nullptr};
    genfit::KalmanFitterInfo* fitterInfo{nullptr};
    genfit::MeasurementOnPlane residual;
    genfit::AbsTrackRep* cardRep{nullptr};
    unsigned int nTrackPoints;
    int detId;
    TVectorD scdState(2);
    
    auto notSingle = [](std::size_t n) -> bool {
        return n != 1;
    };
    
    fitterMillePede->beginRun();

    bool doGBLFit{false};
    
    std::vector<double> zeroSlopes;
    zeroSlopes.assign(NLAD * NBOARD, 0.0);
    std::vector<double> randomShifts;
    
    std::vector<double> residualsYMean;
    std::vector<std::shared_ptr<TCanvas>> canvasArr;
    
    for (std::size_t iter{0}; iter < nIterations; ++iter) {
        
        std::vector<std::unique_ptr<TH1D>> residualsY;
        std::unique_ptr<TH1D> momentumReco = std::make_unique<TH1D>("mom", Form("Momentum %lu", iter), 500, 0., 10.);
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        clustersTree->GetEntry(event);
        std::array<std::size_t, NBOARD * NLAD> len;
        std::array<std::size_t, NBOARD * NLAD>::iterator it;
        
        for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
        it = std::find_if(len.begin(), len.end(), [](std::size_t n) -> bool {return n != 2;});
        if (it == len.end()) N2events++;
        it = std::find_if(len.begin(), len.end(), [](std::size_t n) -> bool {return n != 3;});
        if (it == len.end()) N3events++;
        it = std::find_if(len.begin(), len.end(), [](std::size_t n) -> bool {return n != 4;});
        if (it == len.end()) N4events++;
        it = std::find_if(len.begin(), len.end(), [](std::size_t n) -> bool {return n < 5;});
        if (it == len.end()) N5plusevents++;
        
        it = std::find_if(len.begin() + 1, len.end(), notSingle);
        if (it == len.end()) Nnot0Layer++;
        
        if (len[0] == 1) {
            it = std::find_if(len.begin() + 2, len.end(), notSingle);
            if (it == len.end()) Nnot1Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 2, notSingle);
        if (it == len.begin() + 2) {
            it = std::find_if(len.begin() + 3, len.end(), notSingle);
            if (it == len.end()) Nnot2Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 3, notSingle);
        if (it == len.begin() + 3) {
            it = std::find_if(len.begin() + 4, len.end(), notSingle);
            if (it == len.end()) Nnot3Layer++;
        }
        
        it = std::find_if(len.begin(), len.begin() + 4, notSingle);
        if (it == len.begin() + 4) {
            if (len[5] == 1) Nnot4Layer++;
        }
        
        it = std::find_if(len.begin(), len.end() - 1, notSingle);
        if (it == len.end() - 1) Nnot5Layer++;
        
        it = std::find_if(len.begin(), len.end(), notSingle);
        
        if (it == len.end() && (enablePixels ? vpixelSizes.at(0)->size() == 1 && vpixelSizes.at(1)->size() == 1 : true)) {
            
            if (!multipleClusterLength(vlengths) && excludeSingleStripClusters) continue;
                        
            N1event++;
            
            std::cout << "event: " << event << std::endl;
            
            correctStripPositions(vcogs, vcogsAligned, true, 0);
            
            if (!nov2021BT) updateVcogs(vcogs, vcogsAligned, 0, enablePixels);
            else {
                for (std::size_t i{0}; i < NLAD * NBOARD; ++i) vcogsAligned[i]->push_back(vcogs.at(i)->at(0));
            }

            if (enablePixels) {
                for (std::size_t i{0}; i < NLAD; ++i) {
                    if (i == 0) {
                        if (vquadX.at(i)->at(0) <= 254) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - edge_widthY_quad1 + 1.0) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) >= 257) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + edge_widthY_quad1 - 1.0) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) <= 255) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - (edge_widthY_quad1 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) >= 256) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + (edge_widthY_quad1 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else
                        {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthY_quad1;
                        }
                        if (vquadY.at(i)->at(0) <= 254) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - edge_widthX_quad1 + 1.0) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) >= 257) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + edge_widthX_quad1 - 1.0) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) <= 255) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - (edge_widthX_quad1 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) >= 256) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + (edge_widthX_quad1 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthX_quad1;
                        }
                    }
                    else {
                        if (vquadX.at(i)->at(0) <= 254) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - edge_widthY_quad2 + 1.0) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) >= 257) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + edge_widthY_quad2 - 1.0) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) <= 255) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - (edge_widthY_quad2 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) >= 256) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + (edge_widthY_quad2 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else
                        {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthY_quad2;
                        }
                        if (vquadY.at(i)->at(0) <= 254) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - edge_widthX_quad2 + 1.0) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) >= 257) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + edge_widthX_quad2 - 1.0) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) <= 255) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - (edge_widthX_quad2 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) >= 256) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + (edge_widthX_quad2 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthX_quad2;
                        }
                    }
                    (*xPixelErr)[i] = conversionCoefPixel / sqrt(12. * vpixelSizes.at(i)->at(0));
                    (*yPixelErr)[i] = conversionCoefPixel / sqrt(12. * vpixelSizes.at(i)->at(0));
                }
            }

            yErr = std::make_shared<TVectorD>(NLAD * NBOARD, &transformVectors(vlengths, vsoverns, 0, enablePixels)[0]);
                        
            if (useAlignedGeom) {
                // yCoord = new TVectorD(NLAD * NBOARD, &updateCoGs(vcogsAligned)[0]);
                // zCoord = new TVectorD(NLAD * NBOARD, &updateDetPos(vcogsAligned)[0]);
            }
            else {
                yCoord = std::make_shared<TVectorD>(NLAD * NBOARD, &transformVectors(vcogsAligned, vsoverns, 0)[0]);
                for (std::size_t i{0}; i < yCoord->GetNrows(); ++i) {
                    if (iter > 0) (*yCoord)[i] -= residualsYMean.at(i);
                }
            }
            
//            randomShifts.push_back(r.Rndm(2) - 1.0);
            
//            for (std::size_t i{0}; i < yCoord->GetNrows(); ++i) {
//                (*yCoord)[i] = 834. - sqrt(834. * 834. - ((*zCoord)[i] - (*zCoord)[0]) * ((*zCoord)[i] - (*zCoord)[0])) + randomShifts.back();
//                (*yCoord)[i] += r.Gaus(0.0, MCSSigma(i, *zCoord, zeroSlopes, 0.0) / 10.);
//                if (i == 0) (*yCoord)[i] += 0.01;
//                else if (i == 1) (*yCoord)[i] -= 0.012;
//                else if (i == 2) (*yCoord)[i] += 0.015;
//                else if (i == 3) (*yCoord)[i] -= 0.02;
//                else if (i == 4) (*yCoord)[i] += 0.013;
//                else if (i == 5) (*yCoord)[i] -= 0.019;
//                (*yErr)[i] = conversionCoef / sqrt(12.);
//            }
            
            int sign{1};
            rep = new genfit::RKTrackRep(sign * pdg);
            genfit::MeasuredStateOnPlane stateRef(rep);
            if (!beamTestData) mom.SetXYZ(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
            else mom.SetXYZ(0, 0, pTruth); // 10 GeV/c
            if (!useAlignedGeom) pos.SetXYZ(0, (*yCoord)[0], detectorPos.front());
            else pos.SetXYZ(0, (*yCoord)[0], (*zCoord)[0]);
            if (enablePixels) pos.SetXYZ(0, (*yCoord)[0], detectorPosPixel.front());
            for (int i = 0; i < 3; ++i) covM(i, i) = detectorResolution * detectorResolution * 100.;
            for (int i = 3; i < 6; ++i) covM(i, i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
            rep->setPosMomCov(stateRef, pos, mom, covM);
            
            //second rep
            if (!beamTestData || enableSecondRep) {
                sign = -1;
                if (beamTestData) { sign = 1; pdg = 211; }
                secondRep = new genfit::RKTrackRep(sign * pdg);
                genfit::MeasuredStateOnPlane stateSecondRef(secondRep);
                if (beamTestData) mom.SetXYZ(0, 0, pTruth);
                mom.SetXYZ(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
                if (!useAlignedGeom) pos.SetXYZ(0, (*yCoord)[0], detectorPos.front());
                else pos.SetXYZ(0, (*yCoord)[0], (*zCoord)[0]);
                for (int i = 0; i < 3; ++i) covM(i, i) = detectorResolution * detectorResolution * 100.;
                for (int i = 3; i < 6; ++i) covM(i, i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
                secondRep->setPosMomCov(stateSecondRef, pos, mom, covM);
                if (enableThirdRep) {
                    sign = -1;
                    pdg = 11;
                    thirdRep = new genfit::RKTrackRep(sign * pdg);
                    genfit::MeasuredStateOnPlane stateThirdRef(thirdRep);
                    mom.SetXYZ(0, 0, pTruth);
                    if (!useAlignedGeom) pos.SetXYZ(0, (*yCoord)[0], detectorPos.front());
                    else pos.SetXYZ(0, (*yCoord)[0], (*zCoord)[0]);
                    for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
                    for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
                    thirdRep->setPosMomCov(stateThirdRef, pos, mom, covM);
                }
            }
                
            // create track
            TVectorD seedState(6);
            TMatrixDSym seedCov(6);
            rep->get6DStateCov(stateRef, seedState, seedCov);
            fitTrack = new genfit::Track(rep, seedState, seedCov); //initialized with smeared rep
            if (!beamTestData || enableSecondRep) {
                fitTrack->addTrackRep(secondRep);
                if (enableThirdRep) fitTrack->addTrackRep(thirdRep);
            }
            
            std::cout << "after fitTrack definition: " << std::endl;
            
            std::cout << "before performFitting: " << std::endl;
            
            bool fitResult = performFitting(*zCoord, *yCoord, *zErr, *yErr, *xPixel, *yPixel, *xPixelErr, *yPixelErr, enablePixels, useAlignedGeom, fitTrack, fitterMillePede, fitter, doGBLFit, event);
            
            std::cout << "after performFitting: " << std::endl;
            
            cardRep = fitTrack->getCardinalRep();
            
            fitStatus = fitTrack->getFitStatus(cardRep);
            chi2 = fitStatus->getChi2() / fitStatus->getNdf();
            pvalue = fitStatus->getPVal();
            if (pvalue > 1.0E-33) chi2Sum += fitStatus->getChi2();

            hnmeas = yCoord->GetNrows();
            
            std::cout << "before residual calculation: " << std::endl;
            
            nTrackPoints = fitTrack->getNumPoints();
            
            std::cout << "nTrackPoints: " << nTrackPoints << std::endl;

            if (fitResult && fitStatus->isFitConverged()) {
                
                for (std::size_t i = 0; i < nTrackPoints; ++i) {
                    trackPoint = fitTrack->getPointWithMeasurementAndFitterInfo(i, cardRep);
                    rawMeas = trackPoint->getRawMeasurement();
                    std::unique_ptr<const genfit::AbsHMatrix> HitHMatrix(rawMeas->constructHMatrix(cardRep));
                    detId = rawMeas->getDetId();
                    fitterInfo = trackPoint->getKalmanFitterInfo();
                    residual = fitterInfo->getResidual();
                    refState = fitterInfo->getReferenceState();
                    scdState = residual.getState();
//                    scdState = rawMeas->getRawHitCoords() - HitHMatrix->Hv(refState->getState());
                    if (residualsY.size() < nTrackPoints) residualsY.push_back(std::make_unique<TH1D>(Form("h_%lu", i), Form("Residuals Y, iteration %lu. Layer %lu", iter, i), 500, -0.2, 0.2));
                    residualsY.at(i)->Fill(scdState[1]);
                }
                
                if (pvalue > 1.0E-33) N1GoodEvent++;
                
                fittedState = fitTrack->getFittedState();
                
                momReco = fittedState.getMomMag();
                momentumReco->Fill(momReco);
                momErr = (momReco - fabs(pTruth)) / fabs(pTruth);
                
                std::cout << "Reconstructed momentum: " << momReco << " GeV/c" << std::endl;
                std::cout << "Reconstructed momentum error: " << momErr << " GeV/c" << std::endl;
                
                momX = (fittedState.getMom()).X();
                momY = (fittedState.getMom()).Y();
                momZ = (fittedState.getMom()).Z();
                
                hPDG = fittedState.getPDG();
                
                #ifdef SHOW_TRACKS
                    display->addEvent(fitTrack);
                #endif
                
                delete fitTrack;
                fitTrack = nullptr;
            }
            
            for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
                vcogsAligned[i]->clear();
                vcogsAligned[i]->shrink_to_fit();
            }
            
            std::cout << "after residual calculation: " << std::endl;

        }

    }
    
    for (std::size_t i{0}; i < residualsY.size(); ++i) {
        if (iter == 0) residualsYMean.push_back(residualsY.at(i)->GetMean() / 2.);
        else residualsYMean.at(i) += residualsY.at(i)->GetMean() / 2.;
//        residualsYMean.push_back(0.0);
        std::cout << "Mean of Y residuals: " << residualsYMean.at(i) << " cm" << std::endl;
        std::cout << "Std. dev. of Y residuals: " << residualsY.at(i)->GetStdDev() << " cm" << std::endl;
        canvasArr.push_back(std::make_shared<TCanvas>());
        residualsY.at(i)->Draw();
        gPad->SetLogy();
        canvasArr.back()->Modified();
        canvasArr.back()->Update();
    }
    
    std::shared_ptr<TCanvas> canvasMom = std::make_shared<TCanvas>();
    momentumReco->Draw();
    canvasMom->Modified();
    canvasMom->Update();
        
    }
    
    std::string outputResidualsFileName = currentDir.string() + directoryPathStr + "/../GBL_Millepede/residuals.txt";

    // Open the output file
    std::ofstream outputResidualsFile(outputResidualsFileName);

    // Check if the file is open
    int i{12};
    if (outputResidualsFile) {
        // Iterate over the vector and write each value to the file
        for (const double& value : residualsYMean) {
            outputResidualsFile << i << " " << value << std::endl;
            i += 10;
        }

        // Close the file
        outputResidualsFile.close();

        std::cout << "Data saved to " << outputResidualsFileName << std::endl;
    } else {
        std::cerr << "Failed to open the output file: " << outputResidualsFileName << std::endl;
    }
    
    std::vector<std::unique_ptr<TH1D>> residualsY;
    std::unique_ptr<TH1D> momentumReco2 = std::make_unique<TH1D>("mom2", "Momentum Last Iteration", 500, 0., 10.);
    doGBLFit = true;
    N1event = 0;

    for (std::size_t event{0}; event < nEntries; ++event) {
        clustersTree->GetEntry(event);
        std::array<std::size_t, NBOARD * NLAD> len;
        std::array<std::size_t, NBOARD * NLAD>::iterator it;

        for (std::size_t i{0}; i < NBOARD * NLAD; ++i) len[i] = vintegrals.at(i)->size();
        it = std::find_if(len.begin(), len.end(), [](std::size_t n) -> bool {return n != 2;});
        if (it == len.end()) N2events++;
        it = std::find_if(len.begin(), len.end(), [](std::size_t n) -> bool {return n != 3;});
        if (it == len.end()) N3events++;
        it = std::find_if(len.begin(), len.end(), [](std::size_t n) -> bool {return n != 4;});
        if (it == len.end()) N4events++;
        it = std::find_if(len.begin(), len.end(), [](std::size_t n) -> bool {return n < 5;});
        if (it == len.end()) N5plusevents++;

        it = std::find_if(len.begin() + 1, len.end(), notSingle);
        if (it == len.end()) Nnot0Layer++;

        if (len[0] == 1) {
            it = std::find_if(len.begin() + 2, len.end(), notSingle);
            if (it == len.end()) Nnot1Layer++;
        }

        it = std::find_if(len.begin(), len.begin() + 2, notSingle);
        if (it == len.begin() + 2) {
            it = std::find_if(len.begin() + 3, len.end(), notSingle);
            if (it == len.end()) Nnot2Layer++;
        }

        it = std::find_if(len.begin(), len.begin() + 3, notSingle);
        if (it == len.begin() + 3) {
            it = std::find_if(len.begin() + 4, len.end(), notSingle);
            if (it == len.end()) Nnot3Layer++;
        }

        it = std::find_if(len.begin(), len.begin() + 4, notSingle);
        if (it == len.begin() + 4) {
            if (len[5] == 1) Nnot4Layer++;
        }

        it = std::find_if(len.begin(), len.end() - 1, notSingle);
        if (it == len.end() - 1) Nnot5Layer++;

        it = std::find_if(len.begin(), len.end(), notSingle);

        if (it == len.end() && (enablePixels ? vpixelSizes.at(0)->size() == 1 && vpixelSizes.at(1)->size() == 1 : true)) {

            if (!multipleClusterLength(vlengths) && excludeSingleStripClusters) continue;

            N1event++;

            std::cout << "event: " << event << std::endl;

            correctStripPositions(vcogs, vcogsAligned, true, 0);

            if (!nov2021BT) updateVcogs(vcogs, vcogsAligned, 0, enablePixels);
            else {
                for (std::size_t i{0}; i < NLAD * NBOARD; ++i) vcogsAligned[i]->push_back(vcogs.at(i)->at(0));
            }

            if (enablePixels) {
                for (std::size_t i{0}; i < NLAD; ++i) {
                    if (i == 0) {
                        if (vquadX.at(i)->at(0) <= 254) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - edge_widthY_quad1 + 1.0) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) >= 257) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + edge_widthY_quad1 - 1.0) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) <= 255) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - (edge_widthY_quad1 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) >= 256) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + (edge_widthY_quad1 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else
                        {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthY_quad1;
                        }
                        if (vquadY.at(i)->at(0) <= 254) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - edge_widthX_quad1 + 1.0) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) >= 257) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + edge_widthX_quad1 - 1.0) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) <= 255) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - (edge_widthX_quad1 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) >= 256) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + (edge_widthX_quad1 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthX_quad1;
                        }
                    }
                    else {
                        if (vquadX.at(i)->at(0) <= 254) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - edge_widthY_quad2 + 1.0) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) >= 257) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + edge_widthY_quad2 - 1.0) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) <= 255) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 - (edge_widthY_quad2 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else if (vquadX.at(i)->at(0) >= 256) {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5 + (edge_widthY_quad2 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else
                        {
                            (*yPixel)[i] = (vquadX.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthY_quad2;
                        }
                        if (vquadY.at(i)->at(0) <= 254) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - edge_widthX_quad2 + 1.0) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) >= 257) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + edge_widthX_quad2 - 1.0) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) <= 255) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 - (edge_widthX_quad2 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else if (vquadY.at(i)->at(0) >= 256) {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5 + (edge_widthX_quad2 - 1.0) / 2.) * conversionCoefPixel;
                        }
                        else {
                            (*xPixel)[i] = (vquadY.at(i)->at(0) - 255.5) * conversionCoefPixel * edge_widthX_quad2;
                        }
                    }
                    (*xPixelErr)[i] = conversionCoefPixel / sqrt(12. * vpixelSizes.at(i)->at(0));
                    (*yPixelErr)[i] = conversionCoefPixel / sqrt(12. * vpixelSizes.at(i)->at(0));
                }
            }

            yErr = std::make_shared<TVectorD>(NLAD * NBOARD, &transformVectors(vlengths, vsoverns, 0, enablePixels)[0]);

            if (useAlignedGeom) {
                // yCoord = new TVectorD(NLAD * NBOARD, &updateCoGs(vcogsAligned)[0]);
                // zCoord = new TVectorD(NLAD * NBOARD, &updateDetPos(vcogsAligned)[0]);
            }
            else {
                yCoord = std::make_shared<TVectorD>(NLAD * NBOARD, &transformVectors(vcogsAligned, vsoverns, 0)[0]);
                if (doGBLFit) {
                    for (std::size_t i{0}; i < yCoord->GetNrows(); ++i) {
//                        (*yCoord)[i] = 834. - sqrt(834. * 834. - ((*zCoord)[i] - (*zCoord)[0]) * ((*zCoord)[i] - (*zCoord)[0])) + randomShifts.at(N1event - 1);
//                        (*yCoord)[i] += r.Gaus(0.0, MCSSigma(i, *zCoord, zeroSlopes, 0.0));
//                        if (i == 0) (*yCoord)[i] += 0.01;
//                        else if (i == 1) (*yCoord)[i] -= 0.012;
//                        else if (i == 2) (*yCoord)[i] += 0.015;
//                        else if (i == 3) (*yCoord)[i] -= 0.02;
//                        else if (i == 4) (*yCoord)[i] += 0.013;
//                        else if (i == 5) (*yCoord)[i] -= 0.019;
                        (*yCoord)[i] -= residualsYMean.at(i);
//                        (*yErr)[i] = conversionCoef / sqrt(12.);
                    }
                }
            }

            int sign{1};
            rep = new genfit::RKTrackRep(sign * pdg);
            genfit::MeasuredStateOnPlane stateRef(rep);
            if (!beamTestData) mom.SetXYZ(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
            else mom.SetXYZ(0, 0, pTruth); // 10 GeV/c
            if (!useAlignedGeom) pos.SetXYZ(0, (*yCoord)[0], detectorPos.front());
            else pos.SetXYZ(0, (*yCoord)[0], (*zCoord)[0]);
            if (enablePixels) pos.SetXYZ(0, (*yCoord)[0], detectorPosPixel.front());
            for (int i = 0; i < 3; ++i) covM(i, i) = detectorResolution * detectorResolution * 100.;
            for (int i = 3; i < 6; ++i) covM(i, i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
            rep->setPosMomCov(stateRef, pos, mom, covM);

            //second rep
            if (!beamTestData || enableSecondRep) {
                sign = -1;
                if (beamTestData) { sign = 1; pdg = 211; }
                secondRep = new genfit::RKTrackRep(sign * pdg);
                genfit::MeasuredStateOnPlane stateSecondRef(secondRep);
                if (beamTestData) mom.SetXYZ(0, 0, pTruth);
                mom.SetXYZ(0, 0, 0.1); // 100 MeV/c the llimit for Genfit is 4 MeV/c
                if (!useAlignedGeom) pos.SetXYZ(0, (*yCoord)[0], detectorPos.front());
                else pos.SetXYZ(0, (*yCoord)[0], (*zCoord)[0]);
                for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution * detectorResolution * 100.;
                for (int i = 3; i < 6; ++i) covM(i,i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
                secondRep->setPosMomCov(stateSecondRef, pos, mom, covM);
                if (enableThirdRep) {
                    sign = -1;
                    pdg = 11;
                    thirdRep = new genfit::RKTrackRep(sign * pdg);
                    genfit::MeasuredStateOnPlane stateThirdRef(thirdRep);
                    mom.SetXYZ(0, 0, pTruth);
                    if (!useAlignedGeom) pos.SetXYZ(0, (*yCoord)[0], detectorPos.front());
                    else pos.SetXYZ(0, (*yCoord)[0], (*zCoord)[0]);
                    for (int i = 0; i < 3; ++i) covM(i, i) = detectorResolution * detectorResolution * 100.;
                    for (int i = 3; i < 6; ++i) covM(i, i) = pow(0.3 * fabs(pTruth), 2); // 6 is set arbitrary ~ nTrackpoints
                    thirdRep->setPosMomCov(stateThirdRef, pos, mom, covM);
                }
            }

            // create track
            TVectorD seedState(6);
            TMatrixDSym seedCov(6);
            rep->get6DStateCov(stateRef, seedState, seedCov);
            fitTrack = new genfit::Track(rep, seedState, seedCov); //initialized with smeared rep
            if (!beamTestData || enableSecondRep) {
                fitTrack->addTrackRep(secondRep);
                if (enableThirdRep) fitTrack->addTrackRep(thirdRep);
            }

            std::cout << "after fitTrack definition: " << std::endl;

            std::cout << "before performFitting: " << std::endl;

            bool fitResult = performFitting(*zCoord, *yCoord, *zErr, *yErr, *xPixel, *yPixel, *xPixelErr, *yPixelErr, enablePixels, useAlignedGeom, fitTrack, fitterMillePede, fitter, doGBLFit, event);

            std::cout << "after performFitting: " << std::endl;

            cardRep = fitTrack->getCardinalRep();

            fitStatus = fitTrack->getFitStatus(cardRep);
            chi2 = fitStatus->getChi2() / fitStatus->getNdf();
            pvalue = fitStatus->getPVal();
            if (pvalue > 1.0E-33) chi2Sum += fitStatus->getChi2();

            hnmeas = yCoord->GetNrows();

            std::cout << "before residual calculation: " << std::endl;

            nTrackPoints = fitTrack->getNumPoints();

            std::cout << "nTrackPoints: " << nTrackPoints << std::endl;

            if (fitResult && fitStatus->isFitConverged()) {

                for (std::size_t i = 0; i < nTrackPoints; ++i) {
                    trackPoint = fitTrack->getPointWithMeasurementAndFitterInfo(i, cardRep);
                    rawMeas = trackPoint->getRawMeasurement();
                    std::unique_ptr<const genfit::AbsHMatrix> HitHMatrix(rawMeas->constructHMatrix(cardRep));
                    detId = rawMeas->getDetId();
                    fitterInfo = trackPoint->getKalmanFitterInfo();
                    refState = fitterInfo->getReferenceState();
                    residual = fitterInfo->getResidual();
                    scdState = residual.getState();
//                    scdState = rawMeas->getRawHitCoords() - HitHMatrix->Hv(refState->getState());
                    if (residualsY.size() < nTrackPoints) residualsY.push_back(std::make_unique<TH1D>(Form("h2_%lu", i), Form("Residuals Y. Last Iteration. Layer %lu", i), 500, -0.2, 0.2));
                    residualsY.at(i)->Fill(scdState[1]);
                }

                if (pvalue > 1.0E-33) N1GoodEvent++;
                
                fittedState = fitTrack->getFittedState();

                momReco = fittedState.getMomMag();
                momentumReco2->Fill(momReco);
                momErr = (momReco - fabs(pTruth)) / fabs(pTruth);

                std::cout << "Reconstructed momentum: " << momReco << " GeV/c" << std::endl;
                std::cout << "Reconstructed momentum error: " << momErr << " GeV/c" << std::endl;

                momX = (fittedState.getMom()).X();
                momY = (fittedState.getMom()).Y();
                momZ = (fittedState.getMom()).Z();

                hPDG = fittedState.getPDG();

                #ifdef SHOW_TRACKS
                    display->addEvent(fitTrack);
                #endif

                delete fitTrack;
                fitTrack = nullptr;
            }

            for (std::size_t i{0}; i < NLAD * NBOARD; ++i) {
                vcogsAligned[i]->clear();
                vcogsAligned[i]->shrink_to_fit();
            }

            std::cout << "after residual calculation: " << std::endl;

        }

    }

    for (const auto &hist : residualsY) {
        std::cout << "Mean of Y residuals: " << hist->GetMean() << " cm" << std::endl;
        std::cout << "Std. dev. of Y residuals: " << hist->GetStdDev() << " cm" << std::endl;
        canvasArr.push_back(std::make_shared<TCanvas>());
        hist->Draw();
        gPad->SetLogy();
        canvasArr.back()->Modified();
        canvasArr.back()->Update();
    }
    
    std::unique_ptr<TCanvas> canvasMom2 = std::make_unique<TCanvas>();
    momentumReco2->Draw();
    canvasMom2->Modified();
    canvasMom2->Update();

    residualsY.clear();
    
    fitterMillePede->endRun();
    
    std::vector<unsigned int> gblEntriesList = fitterMillePede->getWrittenEntriesToMilleFile();

    std::string outputEntriesFileName = currentDir.string() + directoryPathStr + "/../GBL_Millepede/entriesMille.txt";

    // Open the output file
    std::ofstream outputEntriesFile(outputEntriesFileName);

    // Check if the file is open
    if (outputEntriesFile) {
        // Iterate over the vector and write each value to the file
        for (const unsigned int& value : gblEntriesList) {
            outputEntriesFile << value << std::endl;
        }

        // Close the file
        outputEntriesFile.close();

        std::cout << "Data saved to " << outputEntriesFileName << std::endl;
    } else {
        std::cerr << "Failed to open the output file: " << outputEntriesFileName << std::endl;
    }
    
    chi2array.ResizeTo(1);
    chi2array[0] = chi2Sum / N1GoodEvent;
    chi2Sum = 0.;
    
    inFile->Close();
    
    std::cout << "Total number of events: " << nEntries << std::endl;
    std::cout << "Number of events with 1 cluster in every plane: " << N1event << std::endl;
    std::cout << "Number of events with 2 clusters in every plane: " << N2events << std::endl;
    std::cout << "Number of events with 3 clusters in every plane: " << N3events << std::endl;
    std::cout << "Number of events with 4 clusters in every plane: " << N4events << std::endl;
    std::cout << "Number of events with 5+ clusters in every plane: " << N5plusevents << std::endl;
    
    std::cout << "Efficiency, layer 0: " << N1event * 100.0 / Nnot0Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 1: " << N1event * 100.0 / Nnot1Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 2: " << N1event * 100.0 / Nnot2Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 3: " << N1event * 100.0 / Nnot3Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 4: " << N1event * 100.0 / Nnot4Layer << " %" << std::endl;
    std::cout << "Efficiency, layer 5: " << N1event * 100.0 / Nnot5Layer << " %" << std::endl;
    
    double totalEff = (N1event * 1.0 / Nnot0Layer) * (N1event * 1.0 / Nnot1Layer) * (N1event * 1.0 / Nnot2Layer) * (N1event * 1.0 / Nnot3Layer) * (N1event * 1.0 / Nnot4Layer) * (N1event * 1.0 / Nnot5Layer);
    
    std::cout << "Tracking efficiency: " << totalEff * 100.0 << " %" << std::endl;
    
    #ifdef SHOW_TRACKS
        display->setOptions("ABDEFGHMPT"); // G show geometry
        display->open();
    #endif
    
    myApp->Run();
    myApp->Delete();
    
    return 0;
}
