#!/usr/bin32/python

## \file
# Read millepede binary file and print records
#
# \author Claus Kleinwort, DESY, 2009-2022 (Claus.Kleinwort@desy.de)
#
#  \copyright
#  Copyright (c) 2009 - 2018 Deutsches Elektronen-Synchroton,
#  Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY \n\n
#  This library is free software; you can redistribute it and/or modify
#  it under the terms of the GNU Library General Public License as
#  published by the Free Software Foundation; either version 2 of the
#  License, or (at your option) any later version. \n\n
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Library General Public License for more details. \n\n
#  You should have received a copy of the GNU Library General Public
#  License along with this program (see the file COPYING.LIB for more
#  details); if not, write to the Free Software Foundation, Inc.,
#  675 Mass Ave, Cambridge, MA 02139, USA.
#
# Hardcoded defaults can be replaced by command line arguments for
#    -  Name of binary file
#    -  Number of records to print (-1: all; <-1: all, record headers only)
#    -  Number of records to skip (optional)
#    -  Mininum value to print derivative
#
# Description of the output from readMilleBinary.py
#    -  Records (tracks) start with \c '===' followed by record number and length 
#       (<0 for binary files containing doubles)
#    -  Measurements: A measurement with global derivatives is called a 'global measurement', 
#       otherwise 'local measurement'. Usually the real measurements from the detectors are 'global'
#       ones and virtual measurements e.g. to describe multiple scattering are 'local'.
#    -  'Global' measurements start with \c '-g-' followed by measurement number, first global label,
#       number of local and global derivatives, measurement value and error. The next lines contain 
#       local and global labels (array('i')) and derivatives (array('f') or array('d')).
#    -  'Local' measurements start with \c '-l-' followed by measurement number, first local label, 
#       number of local and global derivatives, measurement value and error. The next lines contain
#       local labels (array('i')) and derivatives (array('f') or array('d')).
#
# Tested with SL4, SL5, SL6

import array
import sys, os
import ROOT

# ############### read millepede binary file #################
#
## Binary file type (C or Fortran)
# Cfiles = 1  # Cfiles
# Cfiles = 0 # Fortran files
Cfiles = -1 # autodetect
#
## Integer format
intfmt = 'i'  # SL5, gcc-4
# intfmt = 'l' # SL4, gcc-3
#
## Binary file name
fname = "milleBinaryISN.dat"
#
## number of records (tracks) to show
mrec = 10
## number of records (track) to skip before
skiprec = 0
## minimum value to print derivatives
minval = None  # allows for NaNs
#
# ## C. Kleinwort - DESY ########################

doNotPrint = True

# ## use command line arguments ?
narg = len(sys.argv)
if narg > 1:
  if narg < 3:
    print("usage: readMilleBinary.py <file name> <number of records> [<number of records to skip> <minimum value to print derivative>]")
    sys.exit(2)
  else:
    fname = sys.argv[1]
    mrec = int(sys.argv[2])
    if narg > 3:
      skiprec = int(sys.argv[3])
    if narg > 4:
      minval = float(sys.argv[4])

#print " input ", fname, mrec, skiprec

# autodetect?
if Cfiles < 0:
  f = open(fname, "rb")
  len2 = array.array(intfmt)
  len2.fromfile(f, 2)
  f.close()
  Cfiles = 1 # C
  if len2[0] == 4*(len2[1]+1):
    Cfiles = 0 # Fortran
    print("Detected Fortran binary file")

path = os.path.abspath(os.path.dirname(__file__))
print("path to file: ", path)
path += "/../GBL_Millepede/"
print("output path: ", path)

# Open the ROOT file for writing
root_file = ROOT.TFile(path + "millefile.root", "RECREATE")

# Create a ROOT TTree to store the data
tree = ROOT.TTree("MilleData", "Millepede Data")

# Define TBranches for your data
branch_meas_value = ROOT.std.vector("double")()
branch_meas_error = ROOT.std.vector("double")()
branch_meas_labels = ROOT.std.vector("int")()
branch_labels = ROOT.std.vector(ROOT.std.vector('unsigned int'))()
branch_glder = ROOT.std.vector(ROOT.std.vector('double'))()
branch_event = array.array('i', [0]);

tree.Branch("resid_value", branch_meas_value)
tree.Branch("meas_error", branch_meas_error)
tree.Branch("meas_labels", branch_meas_labels)
tree.Branch("glder", branch_glder)
tree.Branch("labels", branch_labels)
tree.Branch("event", branch_event, "branch_event/I")

# Read millepede.res text file
millepede_res_filename = path + "millepede.res"
parameter_map = {}  # Unordered map to store data

try:
    with open(millepede_res_filename, "r") as mille_file:
        next(mille_file) # Skip the first line
        for line in mille_file:
            columns = line.split()
            if len(columns) < 5:
                continue

            parameter = int(columns[0])
            value = float(columns[1])
            sigma = float(columns[2])
            error = float(columns[4])
            if sigma >= 0.0:
                parameter_map[parameter] = (value, error)

    # Save parameter_map as a TMap object in the ROOT file
    parameter_map_string = "\n".join(["{} {} {}".format(param, value[0], value[1]) for param, value in parameter_map.items()])
    parameter_map_objstring = ROOT.TObjString(parameter_map_string)
    # Create a separate directory for metadata
    metadata_dir = root_file.mkdir("Metadata")
    metadata_dir.cd()
    # Write the TObjString to the metadata directory
    parameter_map_objstring.Write("ParameterMap")

    # Return to the top directory
    root_file.cd()

except IOError as e:
    print("Error reading millepede.res:", str(e))
    
millepede_residuals_filename = path + "residuals.txt"
residuals_map = {}  # Unordered map to store data

try:
    with open(millepede_residuals_filename, "r") as mille_residuals_file:
        for line in mille_residuals_file:
            columns = line.split()
            parameter = int(columns[0])
            value = float(columns[1])
            residuals_map[parameter] = value

    # Save parameter_map as a TMap object in the ROOT file
    residuals_map_string = "\n".join(["{} {}".format(param, value) for param, value in residuals_map.items()])
    residuals_map_objstring = ROOT.TObjString(residuals_map_string)
    root_file.cd("Metadata")
    # Write the TObjString to the metadata directory
    residuals_map_objstring.Write("ResidualsMap")
    # Return to the top directory
    root_file.cd()

except IOError as e:
    print("Error reading residuals.txt:", str(e))

entries_filename = path + "entriesMille.txt"
entries_list = []

try:
    with open(entries_filename, "r") as entries_file:
        for line in entries_file:
            entries_list.append(int(line))

except IOError as e:
    print("Error reading entriesMille.txt:", str(e))

# read file
f = open(fname, "rb")

nrec = 0
try:
    while (nrec < mrec + skiprec) or (mrec < 0):
# read 1 record
        nr = 0
        if (Cfiles == 0):
            lenf = array.array(intfmt)
            lenf.fromfile(f, 1)

        length = array.array(intfmt)
        length.fromfile(f, 1)
        nr = abs(length[0] // 2)
        nrec += 1

        if length[0] > 0:
            glder = array.array('f')
        else:
            glder = array.array('d')
        glder.fromfile(f, nr)

        inder = array.array(intfmt)
        inder.fromfile(f, nr)

        if (Cfiles == 0):
            lenf = array.array(intfmt)
            lenf.fromfile(f, 1)

        if (nrec <= skiprec):  # must be after last fromfile
            continue

        if not doNotPrint: print(" === NR", nrec, length[0] // 2)

        # no details, only header
        if (mrec < -1):
            continue

        i = 0
        nh = 0
        ja = 0
        jb = 0
        jsp = 0
        nsp = 0
        while (i < (nr - 1)):
            i += 1
            while (i < nr) and (inder[i] != 0):
                i += 1
            ja = i
            i += 1
            while (i < nr) and (inder[i] != 0):
                i += 1
            jb = i
            i += 1
            # special data?
            if (ja + 1 == jb) and (glder[jb] < 0.):
                jsp = jb
                nsp = int(-glder[jb])
                i += nsp - 1
                if not doNotPrint: print(' ### spec.', nsp, inder[jsp + 1:i + 1], glder[jsp + 1:i + 1])
                continue
            while (i < nr) and (inder[i] != 0):
                i += 1
            i -= 1
            nh += 1
            if (jb < i):
                # measurement with global derivatives
                if not doNotPrint: print(' -g- meas.', nh, inder[jb + 1], jb - ja - 1, i - jb, glder[ja], glder[jb])
                branch_meas_value.push_back(glder[ja])
                branch_meas_error.push_back(glder[jb])
                branch_meas_labels.push_back(inder[jb + 1])
            else:
                # measurement without global derivatives
                if not doNotPrint: print(' -l- meas.', nh, inder[ja + 1], jb - ja - 1, i - jb, glder[ja], glder[jb])
            if (ja + 1 < jb):
                lab = []
                val = []
                for k in range(ja + 1, jb):
                    if minval is None:
                        lab.append(inder[k])
                        val.append(glder[k])
                    elif abs(glder[k]) >= minval:
                        lab.append(inder[k])
                        val.append(glder[k])
                        
                if not doNotPrint: print(" local", lab)
                if not doNotPrint: print(" local", val)
            if (jb + 1 < i + 1):
                lab = []
                val = []
                labels_vec = ROOT.std.vector('unsigned int')()
                glder_vec = ROOT.std.vector('double')()
                for k in range(jb + 1, i + 1):
                    if minval is None:
                        lab.append(inder[k])
                        val.append(glder[k])
                        glder_vec.push_back(glder[k])
                        labels_vec.push_back(inder[k])
                    elif abs(glder[k]) >= minval:
                        lab.append(inder[k])
                        val.append(glder[k])
                        glder_vec.push_back(glder[k])
                        labels_vec.push_back(inder[k])
                if not doNotPrint: print(" global", lab)
                if not doNotPrint: print(" global", val)
                branch_glder.push_back(glder_vec)
                branch_labels.push_back(labels_vec)
                glder_vec.clear()
                labels_vec.clear()
        
        branch_event[0] = entries_list[nrec - 1]
        tree.Fill()
        branch_labels.clear()
        branch_glder.clear()
        branch_meas_value.clear()
        branch_meas_error.clear()
        branch_meas_labels.clear()

except EOFError:
    print()
    if (nr > 0):
        print(" >>> error: end of file before end of record", nrec)
    else:
        print("end of file after", nrec, "records")

f.close()

# Write the ROOT tree to the file
root_file.Write()

# Close the ROOT file
root_file.Close()
