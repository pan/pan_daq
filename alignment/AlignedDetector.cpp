//
//  AlignedDetector.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 13.10.22.
//

#include "AlignedDetector.hpp"

AlignedDetector::AlignedDetector() {
    std::array<double, 6> dummy;
    dummy.fill(0.);
    parameters.assign(6, dummy);
    gradients.assign(6, dummy);
    gamma = 1.0;
}

AlignedDetector::AlignedDetector(std::size_t nDetectors, double g) {
    std::array<double, 6> dummy;
    dummy.fill(0.);
    parameters.assign(nDetectors, dummy);
    gradients.assign(nDetectors, dummy);
    gamma = g;
}

AlignedDetector::~AlignedDetector() {
    parameters.clear();
    gradients.clear();
    parameters.shrink_to_fit();
    gradients.shrink_to_fit();
}

void AlignedDetector::setAlignmentParameters(const std::vector<std::array<double, 6>> &pars) {
    for (std::size_t i = 0; i < parameters.size(); ++i) parameters.at(i) = pars.at(i);
}

void AlignedDetector::setGradientValues(const std::vector<std::array<double, 6>> &grads) {
    for (std::size_t i = 0; i < gradients.size(); ++i) gradients.at(i) = grads.at(i);
}

void AlignedDetector::getAlignmentParameters(std::vector<std::array<double, 6>> &pars) const {
    for (std::size_t i = 0; i < parameters.size(); ++i) pars.at(i) = parameters.at(i);
}

void AlignedDetector::getGradientValues(std::vector<std::array<double, 6>> &grads) const {
    for (std::size_t i = 0; i < gradients.size(); ++i) grads.at(i) = gradients.at(i);
}

void AlignedDetector::makeGradStep() {
    for (std::size_t i = 0; i < parameters.size(); ++i) {
        for (std::size_t j = 0; j < 6; ++j) {
            (parameters.at(i)).at(j) = (parameters.at(i)).at(j) - gamma * (gradients.at(i)).at(j);
        }
    }
}

std::ifstream& operator>>(std::ifstream &inFile, AlignedDetector &ad) {
    inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    std::array<double, 6> words;
    inFile >> words.at(0);
    ad.gamma = words.at(0);
    inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    for (std::size_t i = 0; i < (ad.parameters).size(); ++i) {
        for (std::size_t j = 0; j < 6; ++j) {
            inFile >> words.at(j);
            ((ad.parameters).at(i)).at(j) = words.at(j);
        }
    }
    inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    inFile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    for (std::size_t i = 0; i < (ad.gradients).size(); ++i) {
        for (std::size_t j = 0; j < 6; ++j) {
            inFile >> words.at(j);
            ((ad.gradients).at(i)).at(j) = words.at(j);
        }
    }
    return inFile;
}

std::ofstream& operator<<(std::ofstream &outFile, AlignedDetector &ad) {
    outFile << "Gamma\n";
    outFile << ad.gamma << '\n';
    outFile << "DeltaX DeltaY DeltaZ ThetaX ThetaY ThetaZ\n";
    for (std::size_t i = 0; i < (ad.parameters).size(); ++i) {
        for (std::size_t j = 0; j < 6; ++j) {
            outFile.setf(std::ios_base::scientific);
            outFile.width(6);
            outFile.precision(10);
            if (fabs(((ad.parameters).at(i)).at(j)) < 1.e-12) ((ad.parameters).at(i)).at(j) = 0;
            outFile << ((ad.parameters).at(i)).at(j) << ' ';
        }
        outFile << '\n';
    }
    outFile << "GradDeltaX GradDeltaY GradDeltaZ GradThetaX GradThetaY GradThetaZ\n";
    for (std::size_t i = 0; i < (ad.gradients).size(); ++i) {
        for (std::size_t j = 0; j < 6; ++j) {
            outFile.setf(std::ios_base::scientific);
            outFile.width(10);
            outFile.precision(10);
            outFile << ((ad.gradients).at(i)).at(j) << ' ';
        }
        outFile << '\n';
    }
    return outFile;
}

void AlignedDetector::print() const {
    std::cout << "DeltaX DeltaY DeltaZ ThetaX ThetaY ThetaZ\n";
    for (std::size_t i = 0; i < (parameters).size(); ++i) {
        for (std::size_t j = 0; j < 6; ++j) {
            std::cout.setf(std::ios_base::scientific);
            std::cout.width(6);
            std::cout.precision(10);
            std::cout << ((parameters).at(i)).at(j) << ' ';
        }
        std::cout << '\n';
    }
    std::cout << "GradDeltaX GradDeltaY GradDeltaZ GradThetaX GradThetaY GradThetaZ\n";
    for (std::size_t i = 0; i < (gradients).size(); ++i) {
        for (std::size_t j = 0; j < 6; ++j) {
            std::cout.setf(std::ios_base::scientific);
            std::cout.width(10);
            std::cout.precision(10);
            std::cout << ((gradients).at(i)).at(j) << ' ';
        }
        std::cout << '\n';
    }
}
