//
//  generateTrackingData.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 30.01.23.
//

#include <iostream>
#include <string>
#include <cmath>

#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TMath.h"
#include "TRandom2.h"

#include "StripsTree.hpp"
#include "AlignedDetector.hpp"

constexpr std::size_t nDetectors{6};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double stripLength{5.12}; // cm
bool enableVerbose{false};
bool enableMCS{true};
bool enableMisalignment{false};
bool threeAngles{false};
bool sevenAngles{false};

constexpr double p = 15000.; // MeV/c 180000
constexpr double mass = 938.27; // MeV/c^2 for pi+- 139.57 for proton 938.27

std::size_t nTracks{1};
std::vector<double> detectorPos, newDetectorPos;
TRandom2 r(42);

bool notSingle(std::size_t n) {
    return (n != 1);
}

double MCSSigma(std::size_t i, const std::vector<double> &detectorPosLocal, const std::vector<double> &slopes, double angle) {
    
    if (i == 0) return 0.;
    double gammaFactor = sqrt(1 + (p / mass) * (p / mass));
    double velocity = p / (mass * gammaFactor);
    double xSi = 9.37; // cm for silicon
    double s = 0.0150; // cm thickness of each tracking plane
    double gammaAngle{TMath::Pi() / 2.};
    double phiAngle{TMath::Pi() / 2.};
    double relThickness = s / xSi;
    double theta0{0.};
    double sigma = 0.;
    for (std::size_t j{0}; j < i; ++j) {
        if (fabs(slopes.at(i)) > 1.e-12) gammaAngle = atan(slopes.at(i));
        else gammaAngle = TMath::Pi() / 2.;
        phiAngle = gammaAngle - angle;
        s *= 1. / fabs(sin(phiAngle));
        relThickness = s / xSi;
        theta0 = (13.6 / (p * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
        sigma += (detectorPosLocal.at(i) - detectorPosLocal.at(j)) * (detectorPosLocal.at(i) - detectorPosLocal.at(j)) * theta0 * theta0;
        s = 0.0150;
    }
    gammaAngle = TMath::Pi() / 2.;
    phiAngle = gammaAngle - angle;
    s *= 1. / fabs(sin(phiAngle));
    relThickness = s / xSi;
    theta0 = (13.6 / (p * velocity)) * sqrt(relThickness) * (1. + 0.038 * log(relThickness));
    if (i > 2) sigma += (detectorPosLocal.at(i) + 0.006) * (detectorPosLocal.at(i) + 0.006) * theta0 * theta0; // middle StripY layer
    std::cout << "MCSSigma, Layer " << i << " equal " << sqrt(sigma) << std::endl;
    return sqrt(sigma);
}

void updateVcogs(const StripsTree &inTree, StripsTree &outTree, double angle, double intercept, std::size_t j) {
    double new_position{0};
    std::vector<double> empty_vec, zero_slopes;
    zero_slopes.assign(nDetectors, 0);
    for (std::size_t i{0}; i < nDetectors; ++i) {
        if (i == 0) outTree.setPos(i, inTree.getPos(i));
        else {
            new_position = detectorPos.at(i) * tan(angle) / conversionCoef + intercept;
            if (new_position < 0 || new_position > 2047) {outTree.setPos(i, empty_vec); continue;}
            if (i % 2 == 0) {
                if (enableMCS) outTree.setPosLocal(i, j, 2047. - (new_position + r.Gaus(0., MCSSigma(i, detectorPos, zero_slopes, angle) * (cos(angle) + sin(angle) * tan(angle)) / conversionCoef)));
                else outTree.setPosLocal(i, j, 2047. - (new_position));
            }
            else {
                if (enableMCS) outTree.setPosLocal(i, j, (new_position + r.Gaus(0., MCSSigma(i, detectorPos, zero_slopes, angle) * (cos(angle) + sin(angle) * tan(angle)) / conversionCoef)));
                else outTree.setPosLocal(i, j, new_position);
            }
        }
        outTree.setIntegral(i, inTree.getIntegral(i));
        outTree.setSovern(i, inTree.getSovern(i));
        outTree.setSovernsPerChannel(i, inTree.getSovernsPerChannel(i));
        outTree.setLength(i, inTree.getLength(i));
        outTree.setEventNumber(i, inTree.getEventNumber(i));
        outTree.setFrameCounter(i, inTree.getFrameCounter(i));
    }
}

void applyZMisalignment(StripsTree &outTree, double angle, double intercept, const std::vector<double> &slopes, std::size_t j) {
    double new_position{0}, MCS_contribution{0};
    std::vector<double> empty_vec;
    for (std::size_t i{0}; i < nDetectors; ++i) {
        new_position = newDetectorPos.at(i) * tan(angle) / conversionCoef + intercept;
        if (enableVerbose) std::cout << "Layer: " << i << " Intersection Y: " << new_position << std::endl;
        if (new_position < 0 || new_position > 2047) {
            outTree.setPos(i, empty_vec);
            outTree.setIntegral(i, empty_vec);
            continue;
        }
        outTree.setPosLocal(i, j, new_position);
        if (enableMCS && i > 0) {
            MCS_contribution = r.Gaus(0, MCSSigma(i, newDetectorPos, slopes, angle) * (cos(angle) + sin(angle) * tan(angle)) / conversionCoef);
            outTree.setPosLocal(i, j, new_position + MCS_contribution);
            if (fabs(slopes.at(i)) > 1.e-12) newDetectorPos.at(i) += MCS_contribution * conversionCoef / slopes.at(i);
        }
    }
}

void updateDetPos(std::vector<double> &slopes, std::vector<double> &intercepts, double angle, double intercept, std::size_t j = 0) {
    for (std::size_t i{0}; i < nDetectors; ++i) {
        if (fabs(slopes.at(i)) < 1.e-12) {
            newDetectorPos.at(i) = intercepts.at(i);
        }
        else {
            newDetectorPos.at(i) = (intercept - intercepts.at(i)) * conversionCoef / (slopes.at(i) - tan(angle));
        }
        if (enableVerbose) std::cout << "Layer: " << i << " Intersection Z: " << newDetectorPos.at(i) << std::endl;
    }
}

void updateCoGs(StripsTree &outTree, std::vector<double> &lowerLimit, std::vector<double> &lowerZLimit, std::size_t j) {
    double new_position{0};
    std::vector<double> empty_vec;
    for (std::size_t i{0}; i < nDetectors; ++i) {
        new_position = sqrt((outTree.getPosLocal(i, j) - lowerLimit.at(i)) * (outTree.getPosLocal(i, j) - lowerLimit.at(i)) + (newDetectorPos.at(i) - lowerZLimit.at(i)) * (newDetectorPos.at(i) - lowerZLimit.at(i)));
        if (enableVerbose) std::cout << "Layer: " << i << " Local Y: " << new_position << std::endl;
        if (i % 2 == 0) {
            outTree.setPosLocal(i, j, 2047. - new_position);
        }
        else {
            outTree.setPosLocal(i, j, new_position);
        }
    }
}

void updateLimits(std::vector<double> &lowerLimit, std::vector<double> &upperLimit, std::vector<double> &lowerZLimit, std::vector<double> &upperZLimit, std::vector<std::array<double, 6>> &parameters, std::size_t j = 0) {
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        lowerLimit.at(i) = lowerLimit.at(i) * cos(2. * (parameters.at(i))[3] / stripLength) + (parameters.at(i))[1] / conversionCoef;
        upperLimit.at(i) = upperLimit.at(i) * cos(2. * (parameters.at(i))[3] / stripLength) + (parameters.at(i))[1] / conversionCoef;
        lowerZLimit.at(i) = detectorPos.at(i) + (parameters.at(i))[2] + lowerLimit.at(i) * conversionCoef * sin(2. * (parameters.at(i))[3] / stripLength);
        upperZLimit.at(i) = detectorPos.at(i) + (parameters.at(i))[2] + upperLimit.at(i) * conversionCoef * sin(2. * (parameters.at(i))[3] / stripLength);
        if (enableVerbose) {
            std::cout << "Layer: " << i << " Detector pos. Z: " << detectorPos.at(i) << std::endl;
            std::cout << "Layer: " << i << " Delta Y: " << (parameters.at(i))[1] << " Delta Z: " << (parameters.at(i))[2] << " Theta X: " << (parameters.at(i))[3] << std::endl;
            std::cout << "Layer: " << i << " Lower Limit: " << lowerLimit.at(i) << std::endl;
            std::cout << "Layer: " << i << " Upper Limit: " << upperLimit.at(i) << std::endl;
        }
    }
    
}

void updateSlopesIntercepts(std::vector<double> &lowerLimit, std::vector<double> &upperLimit, std::vector<double> &lowerZLimit, std::vector<double> &upperZLimit, std::vector<double> &slopes, std::vector<double> &intercepts, std::size_t j = 0) {
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        if (fabs(upperZLimit.at(i) - lowerZLimit.at(i)) < 1.e-12) {
            slopes.at(i) = 0;
            intercepts.at(i) = (upperZLimit.at(i) + lowerZLimit.at(i)) / 2.;
        }
        else {
            slopes.at(i) = (upperLimit.at(i) - lowerLimit.at(i)) * conversionCoef / (upperZLimit.at(i) - lowerZLimit.at(i));
            intercepts.at(i) = lowerLimit.at(i) - slopes.at(i) * lowerZLimit.at(i) / conversionCoef;
        }
        if (enableVerbose) {
            std::cout << "Layer: " << i << " Slope Y: " << slopes.at(i) << std::endl;
            std::cout << "Layer: " << i << " Intercept Y: " << intercepts.at(i) << std::endl;
        }
    }
    
}

int main(int argc, char* argv[]) {
    
    if(argc != 5){
        std::cout << "To use the script. Do: " << std::endl;
        std::cout << "./GenerateTrackingData <Strip-ROOT-file> <angle-in-deg> <parameters-file-name> <output-filename>" << std::endl;
        return 1;
      }

    std::string strip_filename{argv[1]};
    std::string root_filename{argv[4]};
    
    double tracks_angle{TMath::Pi() * std::stod(argv[2]) / 180.};
    double tracks_angle_basic{tracks_angle};
    double track_intercept{0.};
    
    StripsTree stripData(strip_filename);
    
    TFile *f_generated = new TFile(root_filename.c_str(), "RECREATE");
    if (f_generated == NULL) { std::cout << "could not open root file " << std::endl; return 2; }

    // f_generated->cd();
    int nEntries = stripData.getTree()->GetEntries();
    TTree* generatedStripTree = new TTree("sync_clusters_tree", "Synchronised strip layers");

    if (enableVerbose) generatedStripTree->Print();
    
    StripsTree stripGeneratedData;
    stripGeneratedData.setTree(generatedStripTree);
    stripGeneratedData.createBranches();
    
    if (enableVerbose) std::cout << "stripGeneratedData object initialised!" << std::endl;

    std::vector<std::array<double, 6>> parameters;
    std::vector<std::array<double, 6>> gradients;
    std::array<double, 6> dummy;
    dummy.fill(0.);
    parameters.assign(6, dummy);
    gradients.assign(6, dummy);
    
    // detectorPos = {-7.006, -6.206, -0.470, 0.330, 5.870, 6.670}; // old
    // detectorPos = {-7.006, -6.191, -0.470, 0.330, 5.870, 6.685}; // june 2022
    detectorPos = {-6.645, -5.83, -0.395, 0.4050, 5.83, 6.645}; // april 2023
    newDetectorPos.assign(nDetectors, 0.);
    
    AlignedDetector ad(nDetectors);
    
    std::ifstream inputFile(argv[3]);
    
    if (inputFile.is_open()) {
        inputFile >> ad;
    }
    
    ad.getAlignmentParameters(parameters);
    ad.getGradientValues(gradients);
    
    ad.print();
    
    inputFile.close();
    
    std::array<std::size_t, nDetectors> len;
    std::array<std::size_t, nDetectors>::iterator it;
    
    std::vector<double> stripLayerLowerLimit, stripLayerUpperLimit, stripZLowerLimit, stripZUpperLimit;
    std::vector<double> stripLayerSlope, stripLayerIntercept;
    stripLayerLowerLimit.assign(nDetectors, 0.);
    stripLayerUpperLimit.assign(nDetectors, 2047.);
    stripZLowerLimit.assign(nDetectors, 0.);
    stripZUpperLimit.assign(nDetectors, 0.);
    stripLayerSlope.assign(nDetectors, 0.);
    stripLayerIntercept.assign(nDetectors, 0.);
    
    std::size_t eventResidual{0};
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        stripData.getTree()->GetEntry(event);
        for (std::size_t i{0}; i < nDetectors; ++i) len[i] = (stripData.getLength(i)).size();
        
        it = std::find_if(len.begin(), len.end(), notSingle);
        if (it == len.end()) nTracks = 1;
        else continue;
        
        if (threeAngles) {
            if (event - eventResidual == 4) eventResidual = event - 1;
        }
        else if (sevenAngles) {
            if (event - eventResidual == 8) eventResidual = event - 1;
        }
        
        for (std::size_t tr{0}; tr < nTracks; ++tr) {
            
            if (event % 100 == 0) std::cout << "event: " << event << std::endl;
            
            if (threeAngles || sevenAngles) {
                if (threeAngles) {
                    if ((event - eventResidual) % 3 == 0) {
                        tracks_angle = 0;
                        track_intercept = stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                    }
                    else {
                        if ((event - eventResidual) % 2 == 0) {
                            tracks_angle = (-1.) * tracks_angle_basic;
                            track_intercept = stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                        }
                        else {
                            tracks_angle = tracks_angle_basic;
                            track_intercept = 2047. - stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                        }
                    }
                }
                else if (sevenAngles) {
                    if ((event - eventResidual) % 7 == 0) {
                        tracks_angle = 0;
                        track_intercept = stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                    }
                    else {
                        if ((event - eventResidual) % 6 == 0) {
                            tracks_angle = (-1.) * tracks_angle_basic * 3. / 4.;
                            track_intercept = stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                        }
                        else {
                            if ((event - eventResidual) % 5 == 0) {
                                tracks_angle = tracks_angle_basic;
                                track_intercept = 2047. - stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                            }
                            else {
                                if ((event - eventResidual) % 4 == 0) {
                                    tracks_angle = (-1.) * tracks_angle_basic / 2.;
                                    track_intercept = stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                                }
                                else {
                                    if ((event - eventResidual) % 3 == 0) {
                                        tracks_angle = tracks_angle_basic * 2. / 3.;
                                        track_intercept = 2047. - stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                                    }
                                    else {
                                        if ((event - eventResidual) % 2 == 0) {
                                            tracks_angle = (-1.) * tracks_angle_basic / 4.;
                                            track_intercept = stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                                        }
                                        else {
                                            tracks_angle = tracks_angle_basic / 3.;
                                            track_intercept = 2047. - stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else track_intercept = 2047. - stripData.getPosLocal(0, tr) - detectorPos.at(0) * tan(tracks_angle) / conversionCoef;
            
            updateVcogs(stripData, stripGeneratedData, tracks_angle, track_intercept, tr);
            
            if (enableVerbose) {
                std::cout << "Original: " << std::endl;
                
                for (std::size_t i{0}; i < nDetectors; ++i) std::cout << "Layer: " << i << " cog: " << (stripGeneratedData.getPos(i)).front() << std::endl;
            }
            
            for (std::size_t i{0}; i < nDetectors; ++i) len[i] = (stripGeneratedData.getPos(i)).size();
            
            it = std::find_if(len.begin(), len.end(), notSingle);
            if (!(it == len.end())) continue;
            
            if (enableMisalignment) {
                
                stripLayerLowerLimit.assign(nDetectors, 0.);
                stripLayerUpperLimit.assign(nDetectors, 2047.);
                stripZLowerLimit.assign(nDetectors, 0.);
                stripZUpperLimit.assign(nDetectors, 0.);
                stripLayerSlope.assign(nDetectors, 0.);
                stripLayerIntercept.assign(nDetectors, 0.);
                
                updateLimits(stripLayerLowerLimit, stripLayerUpperLimit, stripZLowerLimit, stripZUpperLimit, parameters);
                updateSlopesIntercepts(stripLayerLowerLimit, stripLayerUpperLimit, stripZLowerLimit, stripZUpperLimit, stripLayerSlope, stripLayerIntercept);
                newDetectorPos.assign(nDetectors, 0.);
                updateDetPos(stripLayerSlope, stripLayerIntercept, tracks_angle, track_intercept);
                applyZMisalignment(stripGeneratedData, tracks_angle, track_intercept, stripLayerSlope, tr);
                
                for (std::size_t i{0}; i < nDetectors; ++i) len[i] = (stripGeneratedData.getPos(i)).size();
                it = std::find_if(len.begin(), len.end(), notSingle);
                if (!(it == len.end())) continue;
                
                updateCoGs(stripGeneratedData, stripLayerLowerLimit, stripZLowerLimit, tr);
                
                if (enableVerbose) {
                    std::cout << "Misaligned: " << std::endl;
                    
                    for (std::size_t i{0}; i < nDetectors; ++i) std::cout << "Layer: " << i << " cog: " << (stripGeneratedData.getPos(i)).front() << std::endl;
                }
            }
            
            stripGeneratedData.getTree()->Fill();
            
        }
        
    }
    f_generated->cd();
    f_generated->Write("", TObject::kOverwrite);
    f_generated->Close();
    delete f_generated;
    
    return 0;
}
