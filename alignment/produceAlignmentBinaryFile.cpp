//
//  produceAlignmentPlots.cpp
//  PAN_DAQ
//
//  Created by Daniil Sukhonos on 25.10.22.
//

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TVectorD.h"
#include "TFitResult.h"
#include "TGeoManager.h"
#include "TGeoPhysicalNode.h"
#include "TRandom.h"
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <unordered_map>
#include <algorithm>
#include <numeric>

#include "AlignedDetector.hpp"
#include "Mille.h"

typedef unsigned int uint;
typedef std::vector<double> vdouble;
typedef std::vector<int> vint;
typedef std::vector<std::string> vstring;

constexpr std::size_t nDetectors{6};
constexpr std::size_t NBRANCH{5};
constexpr double conversionCoef{0.0025}; // cm / ch
constexpr double stripLength(5.12);
uint sampleSize{2000};
constexpr bool nov2021BT{false};
constexpr bool june2022BT{true};
constexpr bool PRenabled{false};
constexpr bool sep2022BT{false};

vdouble detectorPos;
vdouble detectorPosErr = {0.01, 0.01, 0.01, 0.01, 0.01, 0.01}; // Let's put higher machining accuracy: 0.02 cm
std::array<vdouble, nDetectors> deltas;
std::size_t nTracks{1};

bool notSingle(std::size_t n) {
    return (n != 1);
}

void correctStripPositions(std::unordered_map<int, vdouble *> &vec, std::unordered_map<int, vdouble *> &vecAligned, bool generatedDataFlag, std::size_t j) {
    for (std::size_t i{0}; i < vecAligned.size(); ++i) {
        if (generatedDataFlag) vecAligned[i]->push_back(vec[i]->at(j));
        else {
            if (vec[i]->at(j) >= 0 and vec[i]->at(j) < 256) {
                vecAligned[i]->push_back(vec[i]->at(j) - 1);
                if (vec[i]->at(j) < (0 + 64)) vecAligned[i]->back() -= 1;
                else if (vec[i]->at(j) < (0 + 128)) vecAligned[i]->back() -= 0;
                else if (vec[i]->at(j) < (0 + 192)) vecAligned[i]->back() += 1;
                else if (vec[i]->at(j) < (0 + 256)) vecAligned[i]->back() += 2;
            }
            else if (vec[i]->at(j) >= 256 and vec[i]->at(j) < 512) {
                vecAligned[i]->push_back(vec[i]->at(j) + 1);
                if (vec[i]->at(j) < (256 + 64)) vecAligned[i]->back() -= 2;
                else if (vec[i]->at(j) < (256 + 128)) vecAligned[i]->back() -= 1;
                else if (vec[i]->at(j) < (256 + 192)) vecAligned[i]->back() += 0;
                else if (vec[i]->at(j) < (256 + 256)) vecAligned[i]->back() += 1;
            }
            else if (vec[i]->at(j) >= 512 and vec[i]->at(j) < 768) {
                vecAligned[i]->push_back(vec[i]->at(j) - 1);
                if (vec[i]->at(j) < (512 + 64)) vecAligned[i]->back() -= 1;
                else if (vec[i]->at(j) < (512 + 128)) vecAligned[i]->back() -= 0;
                else if (vec[i]->at(j) < (512 + 192)) vecAligned[i]->back() += 1;
                else if (vec[i]->at(j) < (512 + 256)) vecAligned[i]->back() += 2;
            }
            else if (vec[i]->at(j) >= 768 and vec[i]->at(j) < 1024) {
                vecAligned[i]->push_back(vec[i]->at(j) + 1);
                if (vec[i]->at(j) < (768 + 64)) vecAligned[i]->back() -= 2;
                else if (vec[i]->at(j) < (768 + 128)) vecAligned[i]->back() -= 1;
                else if (vec[i]->at(j) < (768 + 192)) vecAligned[i]->back() += 0;
                else if (vec[i]->at(j) < (768 + 256)) vecAligned[i]->back() += 1;
            }
            else if (vec[i]->at(j) >= 1024 and vec[i]->at(j) < 1280) {
                vecAligned[i]->push_back(vec[i]->at(j) - 1);
                if (vec[i]->at(j) < (1024 + 64)) vecAligned[i]->back() -= 1;
                else if (vec[i]->at(j) < (1024 + 128)) vecAligned[i]->back() -= 0;
                else if (vec[i]->at(j) < (1024 + 192)) vecAligned[i]->back() += 1;
                else if (vec[i]->at(j) < (1024 + 256)) vecAligned[i]->back() += 2;
            }
            else if (vec[i]->at(j) >= 1280 and vec[i]->at(j) < 1536) {
                vecAligned[i]->push_back(vec[i]->at(j) + 1);
                if (vec[i]->at(j) < (1280 + 64)) vecAligned[i]->back() -= 2;
                else if (vec[i]->at(j) < (1280 + 128)) vecAligned[i]->back() -= 1;
                else if (vec[i]->at(j) < (1280 + 192)) vecAligned[i]->back() += 0;
                else if (vec[i]->at(j) < (1280 + 256)) vecAligned[i]->back() += 1;
            }
            else if (vec[i]->at(j) >= 1536 and vec[i]->at(j) < 1792) {
                vecAligned[i]->push_back(vec[i]->at(j) - 1);
                if (vec[i]->at(j) < (1536 + 64)) vecAligned[i]->back() -= 1;
                else if (vec[i]->at(j) < (1536 + 128)) vecAligned[i]->back() -= 0;
                else if (vec[i]->at(j) < (1536 + 192)) vecAligned[i]->back() += 1;
                else if (vec[i]->at(j) < (1536 + 256)) vecAligned[i]->back() += 2;
            }
            else if (vec[i]->at(j) >= 1792 and vec[i]->at(j) < 2048) {
                vecAligned[i]->push_back(vec[i]->at(j) + 1);
                if (vec[i]->at(j) < (1792 + 64)) vecAligned[i]->back() -= 2;
                else if (vec[i]->at(j) < (1792 + 128)) vecAligned[i]->back() -= 1;
                else if (vec[i]->at(j) < (1792 + 192)) vecAligned[i]->back() += 0;
                else if (vec[i]->at(j) < (1792 + 256)) vecAligned[i]->back() += 1;
            }
        }
    }
}

void updateVcogs(std::unordered_map<int, vdouble *> &vecAligned, std::size_t j) {
    for (std::size_t i{0}; i < vecAligned.size(); ++i) {
        if (i % 2 == 0) vecAligned[i]->at(j) = 2047. - vecAligned[i]->at(j);
    }
}

vdouble updateDetPos(std::unordered_map<int, vdouble *> &vec, std::vector<std::array<double, 6>> &parameters, std::size_t j) {
    vdouble newDetPos(nDetectors);
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        newDetPos.at(i) = detectorPos.at(i) + (parameters.at(i))[2] + 2. * (vec[i]->at(j) - 1023.5) * conversionCoef * (parameters.at(i))[3] / stripLength;
    }
    return newDetPos;
}

void updateCoGs(std::unordered_map<int, vdouble *> &vecAligned, std::vector<std::array<double, 6>> &parameters, std::size_t j) {
    for (std::size_t i{0}; i < nDetectors; ++i) {
        vecAligned[i]->at(j) = vecAligned[i]->at(j) + (parameters.at(i))[1] / conversionCoef;
    }
}

vdouble transformVectorsDouble(std::unordered_map<int, vdouble *> &vec, std::size_t j) {
    vdouble outVec;
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        outVec.push_back((vec[i]->at(j) - 1023.5) * conversionCoef);
    }
    
    return outVec;
}

vdouble transformVectorsInt(std::unordered_map<int, vint *> &vec, std::unordered_map<int, vdouble *> &vsoverns, std::size_t j, bool firstStep = false) {
    
    vdouble outVec;
    double alpha{0.0};
    for (std::size_t i{0}; i < nDetectors; ++i) {
        alpha = sqrt(1.5 + vec[i]->at(j));
        if (vec[i]->at(j) == 1) outVec.push_back(conversionCoef / sqrt(12.0));
        else outVec.push_back(alpha * conversionCoef / vsoverns[i]->at(j)); // due to CoG calculation
        if (firstStep) outVec.back() = std::sqrt(outVec.back() * outVec.back() + 0.01 * 0.01); // artificially increase the errors by 100 um for the first few grad steps to increase statistics
    }
    
    return outVec;
}

TVectorD removeElement(std::size_t i, TVectorD * vecOrig) {
    TVectorD outVec, tempVecFirst(*vecOrig), tempVecSecond(*vecOrig);
    
    tempVecFirst.ResizeTo(vecOrig->GetLwb(), i - 1);
    tempVecSecond.ResizeTo(i + 1, vecOrig->GetUpb());
    outVec.ResizeTo(vecOrig->GetNrows() - 1);
    outVec.SetSub(0, tempVecFirst);
    outVec.SetSub(tempVecFirst.GetNrows(), tempVecSecond);
    
    return outVec;
}

void evaluateUnbiasedResiduals(std::vector<double> &yErrors, TVectorD * yCoord, TVectorD * yErr, TVectorD * zCoord, TVectorD * zErr, TF1 * fitFunc) {
    
    TGraphErrors *gr{nullptr};
    TVectorD yCoordMod, yErrMod, zCoordMod, zErrMod;
    yErrors.clear();
    yErrors.shrink_to_fit();
    
    yCoordMod.ResizeTo(nDetectors - 1);
    yErrMod.ResizeTo(nDetectors - 1);
    zCoordMod.ResizeTo(nDetectors - 1);
    zErrMod.ResizeTo(nDetectors - 1);
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        
        yCoordMod = removeElement(i, yCoord);
        yErrMod = removeElement(i, yErr);
        zCoordMod = removeElement(i, zCoord);
        zErrMod = removeElement(i, zErr);
        
        fitFunc->SetRange(zCoordMod[yCoordMod.GetLwb()] * 1.05, zCoordMod[yCoordMod.GetUpb()] * 1.05);
        
        gr = new TGraphErrors(zCoordMod, yCoordMod, zErrMod, yErrMod);
        fitFunc->SetParameter(0, (yCoordMod.Max() + yCoordMod.Min()) / 2.);
        fitFunc->SetParameter(1, (yCoordMod[yCoordMod.GetUpb()] - yCoordMod[yCoordMod.GetLwb()]) / (zCoordMod[yCoordMod.GetUpb()] - zCoordMod[yCoordMod.GetLwb()]));
        gr->Fit(fitFunc,"QRN EX0"); // default: "QRNEMC"
        
        yErrors.push_back((*yCoord)(i) - fitFunc->Eval((*zCoord)(i)));
        
        delete gr;
        gr = nullptr;
    }
    
    fitFunc->SetRange(detectorPos.front() * 1.05, detectorPos.back() * 1.05);
}

int main(int argc, char** argv) {
    // argv[1] is a ROOT file name
    // argv[2] is alignment flag (true/false)
    // argv[3] path for the files
    // argv[4] generated data flag
    
    gStyle->SetOptFit(1);
    
    std::unordered_map<int, vdouble *> vintegrals, vcogs, vsoverns, vcogsAligned;
    std::unordered_map<int, std::vector<vdouble> *> vsovernsPerChannel;
    std::unordered_map<int, vint *> vlengths;
    
    std::vector<std::array<double, 6>> parameters;
    std::vector<std::array<double, 6>> gradients;
    std::array<double, 6> dummy;
    dummy.fill(0.);
    parameters.assign(6, dummy);
    gradients.assign(6, dummy);
    
    if (nov2021BT) detectorPos = {-10.905, -9.075, -0.915, 0.915, 9.075, 10.905}; // 0, 1.83, 9.99, 11.82, 19.98, 21.81 cm // November 2021 beamtest
    if (june2022BT) detectorPos = {-7.006, -6.191, -0.470, 0.330, 5.870, 6.685}; // 0, 8.0, 65.36, 73.36, 128.76, 136.76 mm (sens. surf) // June/August 2022 beamtest
    if (sep2022BT) detectorPos = {-6.6075, -5.7925, -0.4075, 0.3925, 5.7925, 6.6075};
    
    TString generatedDataFlagStr{argv[4]};
    generatedDataFlagStr.ToLower();
    bool generatedDataFlag = (generatedDataFlagStr == "true" ? true : false);
    
    TGraphErrors *gr;
    
    TString path(argv[3]);
    
    TFile *inFile = TFile::Open(path + "synced_rootfiles/" + argv[1], "read");
    TTree *clustersTree = (TTree *) inFile->Get("sync_clusters_tree");
    
    std::vector<std::array<TBranch *, NBRANCH>> branches;
    std::array<TBranch *, NBRANCH> temp_arr;
    branches.assign(nDetectors, temp_arr);
    
    TString binaryFileName(argv[1]);
    binaryFileName = binaryFileName(0, binaryFileName.Length() - 5);
    binaryFileName += ".bin";
    // Mille* milleObject = new Mille(binaryFileName, false, true);
    Mille* milleObject = new Mille(binaryFileName);
    
    int nEntries = clustersTree->GetEntries();
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        vintegrals[i] = new vdouble;
        vcogs[i] = new vdouble;
        vsoverns[i] = new vdouble;
        vlengths[i] = new vint;
        vsovernsPerChannel[i] = new std::vector<vdouble>;
        vcogsAligned[i] = new vdouble;
        clustersTree->SetBranchAddress(Form("integral%lu", i), &vintegrals.at(i), &((branches.at(i)).at(0)));
        clustersTree->SetBranchAddress(Form("cog%lu", i), &vcogs.at(i), &((branches.at(i)).at(2)));
        clustersTree->SetBranchAddress(Form("sovern%lu", i), &vsoverns.at(i), &((branches.at(i)).at(3)));
        clustersTree->SetBranchAddress(Form("sovernPerChannel%lu", i), &vsovernsPerChannel.at(i), &((branches.at(i)).at(4)));
        clustersTree->SetBranchAddress(Form("length%lu", i), &vlengths.at(i), &((branches.at(i)).at(1)));
    }
    
    // branch check
    for (std::size_t i{0}; i < nDetectors; ++i) {
        for (uint br{0}; br < branches.at(i).size(); br++)
            std::cout << "ladder " << i << " branch " << br << " has name " << branches.at(i).at(br)->GetName() << std::endl;
    }
    
    std::array<vdouble, nDetectors> arrHistos;
    TString inputFileName(argv[1]);
    inputFileName = inputFileName(15, inputFileName.Length());
    TString alignedFlag(argv[2]);
    alignedFlag.ToLower();
    bool flag = (alignedFlag == "true" ? true : false);
    TString outputFile(flag ? "tracking_histos_aligned" + inputFileName : "tracking_histos" + inputFileName);
    // TFile *outFile = TFile::Open(path + "fitted_rootfiles/" + outputFile, "recreate");
    // TTree *tracksTree = new TTree("tracks_tree", "Fitted tracks");
    
    double chi2, pvalue, rmse_x, rmse_y, rmse_z;
    double xInitDir, yInitDir, zInitDir, momInit, p0, p1;
    int hnmeas, hPDG, hConvTracks;
    std::vector<double> xErrors, yErrors, zErrors, yErrors_aligned;
    TMatrixDSym covMatrix, correlMatrix;
    TVectorD yCoGs(nDetectors);
    vint lengthOut;
    vdouble integralOut;
    std::vector<vdouble> sovernsPerChannelOut;
    
    /*
    auto ycogsbr = tracksTree->Branch("ycogsbr", &yCoGs);
    auto chi2br = tracksTree->Branch("chi2br", &chi2, "chi2/D");
    auto pvaluebr = tracksTree->Branch("pvaluebr", &pvalue, "pvalue/D");
    auto p0br = tracksTree->Branch("p0br", &p0, "p0/D");
    auto p1br = tracksTree->Branch("p1br", &p1, "p1/D");
    auto covMatrixbr = tracksTree->Branch("covMatrixbr", &covMatrix);
    auto corelMatrixbr = tracksTree->Branch("correlMatrixbr", &correlMatrix);
    auto rmse_ybr = tracksTree->Branch("rmse_ybr", &rmse_y, "rmse_y/D");
    auto hnmeasbr = tracksTree->Branch("hnmeasbr", &hnmeas, "hnmeas/I");
    auto yerrbr = tracksTree->Branch("yerrbr", &yErrors);
    auto yerr_biasedbr = tracksTree->Branch("yerr_biasedbr", &yErrors_aligned);
    auto lengthbr = tracksTree->Branch("clusterlengthbr", &lengthOut);
    auto integralbr = tracksTree->Branch("clusterintegralbr", &integralOut);
    auto sovernsbr = tracksTree->Branch("sovernsbr", &sovernsPerChannelOut);
    */
     
    TF1 *fitFunc = new TF1("fitFunc","pol1(0)", detectorPos.front() * 1.05, detectorPos.back() * 1.05);
    TVectorD *yCoord, *zCoord, *yErr, *zErr;
    zCoord = new TVectorD(nDetectors, &detectorPos[0]);
    zErr = new TVectorD(nDetectors, &detectorPosErr[0]);
    TVectorD chi2array;
    double chi2Sum{0};
    vdouble newDetectorPos(nDetectors);
    
    float* derLc = new float[fitFunc->GetNpar()];
    derLc[0] = 1.;
    float* derGl = new float[1];
    derGl[0] = -1.;
    int* label = new int[1];
    
    uint N1event{0};
    uint N1GoodEvent{0};
    
    std::array<std::size_t, nDetectors> len;
    std::array<std::size_t, nDetectors>::iterator it;
    std::vector<double>::iterator errorCut;
    TFitResultPtr r{nullptr};
    
    AlignedDetector ad(nDetectors);
    inputFileName = inputFileName(0, inputFileName.Length() - 5);
    
    std::ifstream inputFile(path + "global_pars/parameters" + inputFileName + ".txt");
    std::cout << path + "global_pars/parameters" + inputFileName + ".txt" << std::endl;
    
    if (inputFile.is_open() && flag) {
        inputFile >> ad;
    }
    
    ad.getAlignmentParameters(parameters);
    ad.getGradientValues(gradients);
    
    ad.print();
    
    inputFile.close();
    
    for (std::size_t event{0}; event < nEntries; ++event) {
        clustersTree->GetEntry(event);
        for (std::size_t i{0}; i < nDetectors; ++i) len[i] = vintegrals.at(i)->size();
        if (!PRenabled) {
            it = std::find_if(len.begin(), len.end(), notSingle);
            if (it == len.end()) nTracks = 1;
            else continue;
        }
        else nTracks = *it;
        
        for (std::size_t tr{0}; tr < nTracks; ++tr) {
            N1event++;
            
            if (event % 100 == 0) std::cout << "event: " << event << std::endl;
            
            newDetectorPos.clear();
            newDetectorPos.shrink_to_fit();
            
            correctStripPositions(vcogs, vcogsAligned, generatedDataFlag, tr);
            if (june2022BT || sep2022BT) {
                updateVcogs(vcogsAligned, tr);
                newDetectorPos = updateDetPos(vcogsAligned, parameters, tr);
            }
            else newDetectorPos = updateDetPos(vcogsAligned, parameters, tr);
            updateCoGs(vcogsAligned, parameters, tr);
            
            delete fitFunc;
            fitFunc = nullptr;
            fitFunc = new TF1("fitFunc","pol1(0)", newDetectorPos.front() * 1.05, newDetectorPos.back() * 1.05);
            
            fitFunc->SetParameter(0, ((vcogsAligned[0]->at(tr)  + vcogsAligned[nDetectors - 1]->at(tr)) / 2. - 1023.5) * conversionCoef);
            fitFunc->SetParameter(1, (-vcogsAligned[0]->at(tr) + vcogsAligned[nDetectors - 1]->at(tr)) * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()));
            // fitFunc->SetParLimits(0, -1024.0 * conversionCoef, 1024.0 * conversionCoef);
            // fitFunc->SetParLimits(1, -2048.0 * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()), 2048.0 * conversionCoef / (newDetectorPos.back() - newDetectorPos.front()));
            
            yCoord = new TVectorD(nDetectors, &transformVectorsDouble(vcogsAligned, tr)[0]);
            yErr = new TVectorD(nDetectors, &transformVectorsInt(vlengths, vsoverns, tr, false)[0]);
            zCoord = new TVectorD(nDetectors, &newDetectorPos[0]);
            
            gr = new TGraphErrors(*zCoord, *yCoord, *zErr, *yErr);
            fitFunc->SetRange(newDetectorPos.front() * 1.05, newDetectorPos.back() * 1.05);
            r = gr->Fit(fitFunc, "QRNS EX0"); // default: "QRNSEM"
            
            chi2 = r->Chi2() / r->Ndf();
            pvalue = r->Prob();
            covMatrix.ResizeTo(r->GetCovarianceMatrix());
            covMatrix = r->GetCovarianceMatrix();
            
            correlMatrix.ResizeTo(r->GetCorrelationMatrix());
            correlMatrix = r->GetCorrelationMatrix();
            p0 = r->Parameter(0);
            p1 = r->Parameter(1);
            
            lengthOut.clear();
            lengthOut.shrink_to_fit();
            integralOut.clear();
            integralOut.shrink_to_fit();
            sovernsPerChannelOut.clear();
            sovernsPerChannelOut.shrink_to_fit();
            
            hnmeas = yCoord->GetNrows();
            
            yCoGs = *yCoord;
            
            evaluateUnbiasedResiduals(yErrors, yCoord, yErr, zCoord, zErr, fitFunc);
            fitFunc->SetRange(newDetectorPos.front() * 1.05, newDetectorPos.back() * 1.05);
            
            yErrors_aligned.clear();
            yErrors_aligned.shrink_to_fit();
            
            for (std::size_t i{0}; i < nDetectors; ++i) {
                yErrors_aligned.push_back((*yCoord)(i) - fitFunc->Eval((*zCoord)(i)));
            }
            
            if (!yErrors.empty()) {
            
                errorCut = std::find_if(yErrors.begin(), yErrors.end(), [](double i){ return fabs(i) > 0.03; }); // 300 um cut on residuals
                
                if (errorCut == std::end(yErrors)) { // && chi2_aligned < 5 * chi2Cut
                    chi2Sum += r->Chi2();
                    for (std::size_t i{0}; i < nDetectors; ++i) {
                        derLc[1] = newDetectorPos.at(i);
                        // derGl[1] = p1;
                        // derGl[2] = 2. * p1 / stripLength;
                        label[0] = 100 * (i + 1);
                        // label[1] = label[0] + 1;
                        // label[2] = label[1] + 1;
                        milleObject->mille(fitFunc->GetNpar(), derLc, 1, derGl, label, yErrors_aligned.at(i), (*yErr)(i));
                    }
                    milleObject->end();
                    milleObject->kill();
                }
                
                if (errorCut == std::end(yErrors)) N1GoodEvent++;
                
                rmse_y = TMath::RMS(yErrors.begin(), yErrors.end());
                
                for (std::size_t i{0}; i < nDetectors; ++i) {
                    lengthOut.push_back((vlengths.at(i))->at(tr));
                    integralOut.push_back((vintegrals.at(i))->at(tr));
                    sovernsPerChannelOut.push_back((vsovernsPerChannel.at(i))->at(tr));
                }
                
            }
            
            delete yCoord;
            yCoord = nullptr;
            delete zCoord;
            zCoord = nullptr;
            delete yErr;
            yErr = nullptr;
            delete gr;
            gr = nullptr;
        }
        
        for (std::size_t i{0}; i < nDetectors; ++i) {
            vcogsAligned[i]->clear();
            vcogsAligned[i]->shrink_to_fit();
        }
    }
    
    std::cout << "N1GoodEvent: " << N1GoodEvent << std::endl;
    std::cout << "chi2Sum / N1GoodEvent: " << chi2Sum / N1GoodEvent << std::endl;
    
    // outFile->cd();
    // tracksTree->Write();
    // outFile->Close();
    
    delete milleObject;
    delete derLc;
    delete derGl;
    delete label;
    
    inFile->Close();
    
    return 0;
}
